(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[7],{

/***/ 745:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 100).then(__webpack_require__.bind(null, 755)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 750:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// CONCATENATED MODULE: ./components/ui/loading-placeholder/initialize.tsx
var DEFAULT_PROPS = {
    style: {}
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixLQUFLLEVBQUUsRUFBRTtDQUNPLENBQUMifQ==
// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/ui/loading-placeholder/style.tsx

/* harmony default export */ var loading_placeholder_style = ({
    width: '100%',
    height: 100,
    backgroundColor: variable["colorF7"],
    backgroundRepeat: 'none',
    opacity: .4,
    display: variable["display"].block
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxlQUFlO0lBQ2IsS0FBSyxFQUFFLE1BQU07SUFDYixNQUFNLEVBQUUsR0FBRztJQUNYLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztJQUNqQyxnQkFBZ0IsRUFBRSxNQUFNO0lBQ3hCLE9BQU8sRUFBRSxFQUFFO0lBQ1gsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztDQUNoQyxDQUFDIn0=
// CONCATENATED MODULE: ./components/ui/loading-placeholder/view.tsx


var renderView = function (_a) {
    var style = _a.style;
    var formatedStyle = Array.isArray(style)
        ? style.slice() : style;
    return (react["createElement"]("div", { className: 'ani-bg', style: [loading_placeholder_style, formatedStyle] }));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRS9CLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixJQUFNLFVBQVUsR0FBRyxVQUFDLEVBQVM7UUFBUCxnQkFBSztJQUN6QixJQUFNLGFBQWEsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztRQUN4QyxDQUFDLENBQUssS0FBSyxTQUNYLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFFVixNQUFNLENBQUMsQ0FDTCw2QkFBSyxTQUFTLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxDQUFDLEtBQUssRUFBRSxhQUFhLENBQUMsR0FBUSxDQUNoRSxDQUFBO0FBQ0gsQ0FBQyxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// CONCATENATED MODULE: ./components/ui/loading-placeholder/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_LoadingPlaceholder = /** @class */ (function (_super) {
    __extends(LoadingPlaceholder, _super);
    function LoadingPlaceholder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LoadingPlaceholder.prototype.render = function () {
        return view({ style: this.props.style });
    };
    LoadingPlaceholder.defaultProps = DEFAULT_PROPS;
    LoadingPlaceholder = __decorate([
        radium
    ], LoadingPlaceholder);
    return LoadingPlaceholder;
}(react["PureComponent"]));
;
/* harmony default export */ var component = (component_LoadingPlaceholder);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBRTdDLE9BQU8sVUFBVSxNQUFNLFFBQVEsQ0FBQztBQUdoQztJQUFpQyxzQ0FBaUM7SUFBbEU7O0lBT0EsQ0FBQztJQUhDLG1DQUFNLEdBQU47UUFDRSxNQUFNLENBQUMsVUFBVSxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBTE0sK0JBQVksR0FBa0IsYUFBYSxDQUFDO0lBRC9DLGtCQUFrQjtRQUR2QixNQUFNO09BQ0Qsa0JBQWtCLENBT3ZCO0lBQUQseUJBQUM7Q0FBQSxBQVBELENBQWlDLGFBQWEsR0FPN0M7QUFBQSxDQUFDO0FBRUYsZUFBZSxrQkFBa0IsQ0FBQyJ9
// CONCATENATED MODULE: ./components/ui/loading-placeholder/index.tsx

/* harmony default export */ var loading_placeholder = __webpack_exports__["a"] = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxrQkFBa0IsTUFBTSxhQUFhLENBQUM7QUFDN0MsZUFBZSxrQkFBa0IsQ0FBQyJ9

/***/ }),

/***/ 753:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 97).then(__webpack_require__.bind(null, 771)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 762:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// CONCATENATED MODULE: ./api/magazine.ts

;
var fetchMagazineList = function (_a) {
    var _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c, _d = _a.type, type = _d === void 0 ? 'default' : _d;
    var query = "?page=" + page + "&per_page=" + perPage + "&filter[post_type]=" + type;
    return Object(restful_method["b" /* get */])({
        path: "/magazines" + query,
        description: 'Get magazine list',
        errorMesssage: "Can't get magazine list. Please try again",
    });
};
var fetchMagazineDashboard = function () { return Object(restful_method["b" /* get */])({
    path: "/magazines/dashboard",
    description: 'Get magazine dashboard list',
    errorMesssage: "Can't get magazine dashboard list. Please try again",
}); };
/**
 * Fetch magazine category by slug (id)
 */
var fetchMagazineCategory = function (_a) {
    var slug = _a.slug, page = _a.page, perPage = _a.perPage;
    var query = slug + "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/category/" + query,
        description: 'Get magazine category by slug (id)',
        errorMesssage: "Can't get magazine category by slug (id). Please try again",
    });
};
/**
* Fetch magazine by slug (id)
*/
var fetchMagazineBySlug = function (_a) {
    var slug = _a.slug;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/" + slug,
        description: 'Get magazine by slug (id)',
        errorMesssage: "Can't get magazine by slug (id). Please try again",
    });
};
/**
* Fetch magazine related blogs by slug (id)
*/
var fetchMagazineRelatedBlog = function (_a) {
    var slug = _a.slug;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/" + slug + "/related_blogs",
        description: 'Get magazine related blogs by slug (id)',
        errorMesssage: "Can't get magazine related blogs by slug (id). Please try again",
    });
};
/**
* Fetch magazine related boxes by slug (id)
*/
var fetchMagazineRelatedBox = function (_a) {
    var slug = _a.slug, limit = _a.limit;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/" + slug + "/related_boxes?limit=" + limit,
        description: 'Get magazine related boxes by slug (id)',
        errorMesssage: "Can't get magazine related boxes by slug (id). Please try again",
    });
};
/**
* Fetch magazine by tag name
*/
var fetchMagazineByTagName = function (_a) {
    var slug = _a.slug, page = _a.page, perPage = _a.perPage;
    var query = slug + "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/tag/" + query,
        description: 'Get magazine by tag name',
        errorMesssage: "Can't get magazine by tag name. Please try again",
    });
};
/**
* Fetch search magazine by keyword
*/
var fetchMagazineByKeyword = function (_a) {
    var keyword = _a.keyword, page = _a.page, perPage = _a.perPage;
    var query = keyword + "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/search/" + query,
        description: 'Get search magazine by keyword',
        errorMesssage: "Can't get search magazine by keyword. Please try again",
    });
};
/**
* Fetch search suggestion magazine by keyword
*/
var fetchMagazineSuggestionByKeyword = function (_a) {
    var keyword = _a.keyword, limit = _a.limit;
    var query = keyword + "?limit=" + limit;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/search_suggestion/" + query,
        description: 'Get search suggestion magazine by keyword',
        errorMesssage: "Can't get search suggestion magazine by keyword. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFnYXppbmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtYWdhemluZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFNOUMsQ0FBQztBQUVGLE1BQU0sQ0FBQyxJQUFNLGlCQUFpQixHQUM1QixVQUFDLEVBQXFFO1FBQW5FLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWSxFQUFFLFlBQWdCLEVBQWhCLHFDQUFnQjtJQUN6QyxJQUFNLEtBQUssR0FBRyxXQUFTLElBQUksa0JBQWEsT0FBTywyQkFBc0IsSUFBTSxDQUFDO0lBRTVFLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsZUFBYSxLQUFPO1FBQzFCLFdBQVcsRUFBRSxtQkFBbUI7UUFDaEMsYUFBYSxFQUFFLDJDQUEyQztLQUMzRCxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSixNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FBRyxjQUFNLE9BQUEsR0FBRyxDQUFDO0lBQzlDLElBQUksRUFBRSxzQkFBc0I7SUFDNUIsV0FBVyxFQUFFLDZCQUE2QjtJQUMxQyxhQUFhLEVBQUUscURBQXFEO0NBQ3JFLENBQUMsRUFKMEMsQ0FJMUMsQ0FBQztBQUVIOztHQUVHO0FBQ0gsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQ2hDLFVBQUMsRUFBdUI7UUFBckIsY0FBSSxFQUFFLGNBQUksRUFBRSxvQkFBTztJQUNwQixJQUFNLEtBQUssR0FBTSxJQUFJLGNBQVMsSUFBSSxrQkFBYSxPQUFTLENBQUM7SUFDekQsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSx5QkFBdUIsS0FBTztRQUNwQyxXQUFXLEVBQUUsb0NBQW9DO1FBQ2pELGFBQWEsRUFBRSw0REFBNEQ7S0FDNUUsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUo7O0VBRUU7QUFDRixNQUFNLENBQUMsSUFBTSxtQkFBbUIsR0FBRyxVQUFDLEVBQVE7UUFBTixjQUFJO0lBQU8sT0FBQSxHQUFHLENBQUM7UUFDbkQsSUFBSSxFQUFFLGdCQUFjLElBQU07UUFDMUIsV0FBVyxFQUFFLDJCQUEyQjtRQUN4QyxhQUFhLEVBQUUsbURBQW1EO0tBQ25FLENBQUM7QUFKK0MsQ0FJL0MsQ0FBQztBQUVIOztFQUVFO0FBQ0YsTUFBTSxDQUFDLElBQU0sd0JBQXdCLEdBQUcsVUFBQyxFQUFRO1FBQU4sY0FBSTtJQUFPLE9BQUEsR0FBRyxDQUFDO1FBQ3hELElBQUksRUFBRSxnQkFBYyxJQUFJLG1CQUFnQjtRQUN4QyxXQUFXLEVBQUUseUNBQXlDO1FBQ3RELGFBQWEsRUFBRSxpRUFBaUU7S0FDakYsQ0FBQztBQUpvRCxDQUlwRCxDQUFDO0FBRUg7O0VBRUU7QUFDRixNQUFNLENBQUMsSUFBTSx1QkFBdUIsR0FBRyxVQUFDLEVBQWU7UUFBYixjQUFJLEVBQUUsZ0JBQUs7SUFBTyxPQUFBLEdBQUcsQ0FBQztRQUM5RCxJQUFJLEVBQUUsZ0JBQWMsSUFBSSw2QkFBd0IsS0FBTztRQUN2RCxXQUFXLEVBQUUseUNBQXlDO1FBQ3RELGFBQWEsRUFBRSxpRUFBaUU7S0FDakYsQ0FBQztBQUowRCxDQUkxRCxDQUFDO0FBRUg7O0VBRUU7QUFDRixNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FDakMsVUFBQyxFQUF1QjtRQUFyQixjQUFJLEVBQUUsY0FBSSxFQUFFLG9CQUFPO0lBQ3BCLElBQU0sS0FBSyxHQUFNLElBQUksY0FBUyxJQUFJLGtCQUFhLE9BQVMsQ0FBQztJQUN6RCxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLG9CQUFrQixLQUFPO1FBQy9CLFdBQVcsRUFBRSwwQkFBMEI7UUFDdkMsYUFBYSxFQUFFLGtEQUFrRDtLQUNsRSxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSjs7RUFFRTtBQUNGLE1BQU0sQ0FBQyxJQUFNLHNCQUFzQixHQUNqQyxVQUFDLEVBQTBCO1FBQXhCLG9CQUFPLEVBQUUsY0FBSSxFQUFFLG9CQUFPO0lBQ3ZCLElBQU0sS0FBSyxHQUFNLE9BQU8sY0FBUyxJQUFJLGtCQUFhLE9BQVMsQ0FBQztJQUM1RCxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLHVCQUFxQixLQUFPO1FBQ2xDLFdBQVcsRUFBRSxnQ0FBZ0M7UUFDN0MsYUFBYSxFQUFFLHdEQUF3RDtLQUN4RSxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSjs7RUFFRTtBQUNGLE1BQU0sQ0FBQyxJQUFNLGdDQUFnQyxHQUMzQyxVQUFDLEVBQWtCO1FBQWhCLG9CQUFPLEVBQUUsZ0JBQUs7SUFDZixJQUFNLEtBQUssR0FBTSxPQUFPLGVBQVUsS0FBTyxDQUFDO0lBQzFDLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsa0NBQWdDLEtBQU87UUFDN0MsV0FBVyxFQUFFLDJDQUEyQztRQUN4RCxhQUFhLEVBQUUsbUVBQW1FO0tBQ25GLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQyJ9
// EXTERNAL MODULE: ./constants/api/magazine.ts
var magazine = __webpack_require__(22);

// CONCATENATED MODULE: ./action/magazine.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return fetchMagazineListAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return fetchMagazineDashboardAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return fetchMagazineCategoryAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fetchMagazineBySlugAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return fetchMagazineRelatedBlogAction; });
/* unused harmony export fetchMagazineRelatedBoxAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fetchMagazineByTagNameAction; });
/* unused harmony export showHideMagazineSearchAction */
/* unused harmony export fetchMagazineByKeywordAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return fetchMagazineSuggestionByKeywordAction; });


/**
 * Fetch love list by filter paramsx
 *
 * @param {number} page ex: 1, 2, 3, 4
 * @param {number} perPage ex: 10, 15, 20
 * @param {'default' | 'video-post' | 'quote-post'} type
 */
var fetchMagazineListAction = function (_a) {
    var page = _a.page, perPage = _a.perPage, type = _a.type;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["f" /* FETCH_MAGAZINE_LIST */],
            payload: { promise: fetchMagazineList({ page: page, perPage: perPage, type: type }).then(function (res) { return res; }) },
            meta: { page: page, perPage: perPage, type: type }
        });
    };
};
var fetchMagazineDashboardAction = function () {
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["e" /* FETCH_MAGAZINE_DASHBOARD */],
            payload: { promise: fetchMagazineDashboard().then(function (res) { return res; }) },
            meta: {}
        });
    };
};
/**
* Fetch magazine category by slug (id)
*
* @param {string} slug ex: makeup
* @param {number} page ex: 1, 2, 3, 4
* @param {number} perPage ex: 10, 15, 20
*/
var fetchMagazineCategoryAction = function (_a) {
    var slug = _a.slug, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["d" /* FETCH_MAGAZINE_CATEGORY */],
            payload: { promise: fetchMagazineCategory({ slug: slug, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { slug: slug, page: page, perPage: perPage }
        });
    };
};
/**
* Fetch magazine by slug (id)
*
* @param {string} slug ex: makeup
*/
var fetchMagazineBySlugAction = function (_a) {
    var slug = _a.slug;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["c" /* FETCH_MAGAZINE_BY_SLUG */],
            payload: { promise: fetchMagazineBySlug({ slug: slug }).then(function (res) { return res; }) },
            meta: { slug: slug }
        });
    };
};
/**
* Fetch magazine related blogs by slug (id)
*
* @param {string} slug ex: makeup
*/
var fetchMagazineRelatedBlogAction = function (_a) {
    var slug = _a.slug;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["g" /* FETCH_MAGAZINE_RELATED_BLOGS */],
            payload: { promise: fetchMagazineRelatedBlog({ slug: slug }).then(function (res) { return res; }) },
            meta: { slug: slug }
        });
    };
};
/**
* Fetch magazine related boxes by slug (id)
*
* @param {string} slug ex: makeup
* @param {number} limit ex: 1, 2, 3, 4
*/
var fetchMagazineRelatedBoxAction = function (_a) {
    var slug = _a.slug, _b = _a.limit, limit = _b === void 0 ? 6 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["h" /* FETCH_MAGAZINE_RELATED_BOXES */],
            payload: { promise: fetchMagazineRelatedBox({ slug: slug, limit: limit }).then(function (res) { return res; }) },
            meta: { slug: slug }
        });
    };
};
/**
* Fetch magazine by tag name
*
* @param {string} tagName ex: halio
* @param {number} page ex: 1, 2, 3, 4
* @param {number} perPage ex: 10, 15, 20
*/
var fetchMagazineByTagNameAction = function (_a) {
    var slug = _a.slug, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["j" /* FETCH_MAGAZINE_TAG */],
            payload: { promise: fetchMagazineByTagName({ slug: slug, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { slug: slug, page: page, perPage: perPage }
        });
    };
};
/**
* Show / Hide search suggest with state param
*
* @param {boolean} state
*/
var showHideMagazineSearchAction = function (state) {
    if (state === void 0) { state = false; }
    return ({
        type: magazine["a" /* DISPLAY_MAGAZINE_SEARCH_SUGGESTION */],
        payload: state
    });
};
/**
* Fetch magazine by keyword
*
* @param {string} keyword ex: halio
* @param {number} page ex: 1, 2, 3, 4
* @param {number} perPage ex: 10, 15, 20
*/
var fetchMagazineByKeywordAction = function (_a) {
    var keyword = _a.keyword, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["b" /* FETCH_MAGAZINE_BY_KEWORD */],
            payload: { promise: fetchMagazineByKeyword({ keyword: keyword, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { keyword: keyword }
        });
    };
};
/**
* Fetch magazine suggestion by keyword
*
* @param {string} keyword ex: halio
* @param {number} limit ex: 1, 2, 3, 4
*/
var fetchMagazineSuggestionByKeywordAction = function (_a) {
    var keyword = _a.keyword, _b = _a.limit, limit = _b === void 0 ? 10 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["i" /* FETCH_MAGAZINE_SUGGESTION_BY_KEWORD */],
            payload: { promise: fetchMagazineSuggestionByKeyword({ keyword: keyword, limit: limit }).then(function (res) { return res; }) },
            meta: { keyword: keyword }
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFnYXppbmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtYWdhemluZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBRUwsaUJBQWlCLEVBQ2pCLHNCQUFzQixFQUN0QixxQkFBcUIsRUFDckIsbUJBQW1CLEVBQ25CLHdCQUF3QixFQUN4Qix1QkFBdUIsRUFDdkIsc0JBQXNCLEVBQ3RCLHNCQUFzQixFQUN0QixnQ0FBZ0MsRUFDakMsTUFBTSxpQkFBaUIsQ0FBQztBQUV6QixPQUFPLEVBQ0wsbUJBQW1CLEVBQ25CLHdCQUF3QixFQUN4Qix1QkFBdUIsRUFDdkIsc0JBQXNCLEVBQ3RCLDRCQUE0QixFQUM1Qiw0QkFBNEIsRUFDNUIsa0JBQWtCLEVBQ2xCLGtDQUFrQyxFQUNsQyx3QkFBd0IsRUFDeEIsbUNBQW1DLEVBQ3BDLE1BQU0sMkJBQTJCLENBQUM7QUFFbkM7Ozs7OztHQU1HO0FBQ0gsTUFBTSxDQUFDLElBQU0sdUJBQXVCLEdBQ2xDLFVBQUMsRUFBZ0Q7UUFBOUMsY0FBSSxFQUFFLG9CQUFPLEVBQUUsY0FBSTtJQUNwQixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsbUJBQW1CO1lBQ3pCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDakYsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUU7U0FDOUIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVCxNQUFNLENBQUMsSUFBTSw0QkFBNEIsR0FDdkM7SUFDRSxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsd0JBQXdCO1lBQzlCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUMvRCxJQUFJLEVBQUUsRUFBRTtTQUNULENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7OztFQU1FO0FBQ0YsTUFBTSxDQUFDLElBQU0sMkJBQTJCLEdBQ3RDLFVBQUMsRUFBZ0M7UUFBOUIsY0FBSSxFQUFFLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUM3QixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsdUJBQXVCO1lBQzdCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxxQkFBcUIsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDckYsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDOUIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0seUJBQXlCLEdBQ3BDLFVBQUMsRUFBUTtRQUFOLGNBQUk7SUFDTCxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsc0JBQXNCO1lBQzVCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDcEUsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUU7U0FDZixDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUdUOzs7O0VBSUU7QUFDRixNQUFNLENBQUMsSUFBTSw4QkFBOEIsR0FDekMsVUFBQyxFQUFRO1FBQU4sY0FBSTtJQUNMLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSw0QkFBNEI7WUFDbEMsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLHdCQUF3QixDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN6RSxJQUFJLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRTtTQUNmLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7O0VBS0U7QUFDRixNQUFNLENBQUMsSUFBTSw2QkFBNkIsR0FDeEMsVUFBQyxFQUFtQjtRQUFqQixjQUFJLEVBQUUsYUFBUyxFQUFULDhCQUFTO0lBQ2hCLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSw0QkFBNEI7WUFDbEMsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLHVCQUF1QixDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUMvRSxJQUFJLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRTtTQUNmLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBR1Q7Ozs7OztFQU1FO0FBQ0YsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsRUFBZ0M7UUFBOUIsY0FBSSxFQUFFLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUM3QixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsa0JBQWtCO1lBQ3hCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxzQkFBc0IsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDdEYsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDOUIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsS0FBYTtJQUFiLHNCQUFBLEVBQUEsYUFBYTtJQUFLLE9BQUEsQ0FBQztRQUNsQixJQUFJLEVBQUUsa0NBQWtDO1FBQ3hDLE9BQU8sRUFBRSxLQUFLO0tBQ2YsQ0FBQztBQUhpQixDQUdqQixDQUFDO0FBRUw7Ozs7OztFQU1FO0FBQ0YsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsRUFBbUM7UUFBakMsb0JBQU8sRUFBRSxZQUFRLEVBQVIsNkJBQVEsRUFBRSxlQUFZLEVBQVosaUNBQVk7SUFDaEMsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLHdCQUF3QjtZQUM5QixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsc0JBQXNCLENBQUMsRUFBRSxPQUFPLFNBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQ3pGLElBQUksRUFBRSxFQUFFLE9BQU8sU0FBQSxFQUFFO1NBQ2xCLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7O0VBS0U7QUFDRixNQUFNLENBQUMsSUFBTSxzQ0FBc0MsR0FDakQsVUFBQyxFQUF1QjtRQUFyQixvQkFBTyxFQUFFLGFBQVUsRUFBViwrQkFBVTtJQUNwQixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsbUNBQW1DO1lBQ3pDLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxnQ0FBZ0MsQ0FBQyxFQUFFLE9BQU8sU0FBQSxFQUFFLEtBQUssT0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDM0YsSUFBSSxFQUFFLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDbEIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUMifQ==

/***/ }),

/***/ 787:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 93).then(__webpack_require__.bind(null, 804)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 835:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./utils/encode.ts
var encode = __webpack_require__(7);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./components/magazine/item/style.tsx


/* harmony default export */ var item_style = ({
    infoContainer: {
        display: variable["display"].flex,
        flexDirection: 'column',
        title: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    paddingLeft: 10,
                    paddingRight: 10,
                    fontSize: 14,
                }],
            DESKTOP: [{
                    paddingLeft: 0,
                    paddingRight: 0,
                    fontSize: 15,
                }],
            GENERAL: [{
                    lineHeight: '22px',
                    color: variable["colorBlack08"],
                    fontFamily: variable["fontAvenirMedium"],
                    width: '100%',
                    fontWeight: 900,
                    display: variable["display"].block,
                    marginBottom: 10,
                    textTransform: 'capitalize',
                    textAlign: 'justify',
                    maxHeight: 66,
                    overflow: 'hidden'
                }]
        }),
        description: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{ fontSize: 12 }],
            DESKTOP: [{ fontSize: 14 }],
            GENERAL: [{
                    lineHeight: '22px',
                    maxHeight: 66,
                    overflow: 'hidden',
                    color: variable["colorBlack08"],
                    textAlign: 'justify',
                    width: '100%',
                    marginBottom: 10,
                }]
        })
    },
    itemContainer: {
        width: '100%',
        display: variable["display"].flex,
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        marginBottom: 20,
        itemWrap: {
            container: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{ marginBottom: 10 }],
                DESKTOP: [{ marginBottom: 20 }],
                GENERAL: [{ width: 'calc(50% - 10px)', display: variable["display"].block }]
            }),
            itemImage: {
                width: '100%',
                paddingTop: '65%',
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                marginBottom: 10,
                posicontion: variable["position"].relative,
                transition: variable["transitionNormal"],
            }
        }
    },
    mobile: {
        itemSlider: function (imgUrl) { return Object(responsive["a" /* combineStyle */])({
            MOBILE: [{ margin: 5 }],
            DESKTOP: [{ marginBottom: 20 }],
            GENERAL: [{
                    width: '100%',
                    display: variable["display"].inlineBlock,
                    borderRadius: 5,
                    overflow: 'hidden',
                    boxShadow: variable["shadowBlur"],
                    backgroundColor: variable["colorF7"],
                    backgroundImage: "url(" + imgUrl + ")",
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    position: variable["position"].relative,
                }]
        }); },
        smallItemSlider: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{ margin: 5, width: 'calc(50% - 10px)', }],
            DESKTOP: [{ width: 'calc(50% - 10px)', marginBottom: 20 }],
            GENERAL: [{
                    display: variable["display"].inlineBlock,
                    borderRadius: 5,
                    overflow: 'hidden',
                    boxShadow: variable["shadowBlur"],
                    backgroundColor: variable["colorWhite"],
                    backgroundPosition: 'top center',
                    backgroundSize: 'cover',
                    position: variable["position"].relative,
                }]
        }),
        itemSliderPanel: {
            wdith: '100%',
            paddingTop: '62.5%',
            position: variable["position"].relative,
        },
        smallItemSliderPanel: function (imgUrl) { return ({
            wdith: '100%',
            paddingTop: '62.5%',
            position: variable["position"].relative,
            backgroundImage: "url(" + imgUrl + ")",
            backgroundColor: variable["colorF7"],
            backgroundPosition: 'top center',
            backgroundSize: 'cover',
        }); },
        info: {
            width: '100%',
            padding: 'DESKTOP' === window.DEVICE_VERSION ? 30 : 12,
            position: variable["position"].absolute,
            bottom: 0,
            left: 0,
            background: variable["colorBlack02"],
            height: '100%',
            display: variable["display"].flex,
            flexDirection: 'column',
            justifyContent: 'flex-end',
            alignItems: 'flex-start',
            category: {
                background: variable["colorWhite"],
                display: variable["display"].block,
                height: 22,
                lineHeight: '24px',
                padding: '0 6px',
                float: 'left',
                color: variable["colorBlack"],
                fontSize: 12,
                marginBottom: 5,
            },
            title: {
                color: variable["colorWhite"],
                whiteSpace: 'pre-wrap',
                fontFamily: variable["fontAvenirMedium"],
                fontSize: 'DESKTOP' === window.DEVICE_VERSION ? 40 : 20,
                lineHeight: 'DESKTOP' === window.DEVICE_VERSION ? '50px' : '26px',
                maxHeight: 'DESKTOP' === window.DEVICE_VERSION ? '150px' : '78px',
                maxWidth: 'DESKTOP' === window.DEVICE_VERSION ? '80%' : '100%',
                textShadow: variable["shadowTextBlur"],
                overflow: 'hidden',
            },
            tagList: {
                width: '100%',
                display: variable["display"].flex,
                flexWrap: 'wrap',
                height: '18px',
                maxHeight: '18px',
                overflow: 'hidden'
            },
            tagItem: {
                fontSize: 10,
                lineHeight: '18px',
                color: variable["colorBlack"],
                fontFamily: variable["fontAvenirMedium"],
                opacity: .7,
                marginRight: 10,
                whiteSpace: 'pre-wrap'
            },
        },
        smallInfo: {
            width: '100%',
            padding: 12,
            position: variable["position"].relative,
            background: variable["colorWhite"],
            display: variable["display"].flex,
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
            title: {
                color: variable["colorBlack"],
                whiteSpace: 'pre-wrap',
                fontFamily: variable["fontAvenirMedium"],
                fontSize: 14,
                lineHeight: '22px',
                maxHeight: '66px',
                overflow: 'hidden',
                marginBottom: 5
            },
            description: {
                fontSize: 12,
                color: variable["colorBlack"],
                fontFamily: variable["fontAvenirMedium"],
                opacity: .7,
                marginRight: 10,
                whiteSpace: 'pre-wrap',
                lineHeight: '18px',
                maxHeight: 36,
                overflow: 'hidden',
            },
        },
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFekQsZUFBZTtJQUViLGFBQWEsRUFBRTtRQUNiLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7UUFDOUIsYUFBYSxFQUFFLFFBQVE7UUFFdkIsS0FBSyxFQUFFLFlBQVksQ0FBQztZQUNsQixNQUFNLEVBQUUsQ0FBQztvQkFDUCxXQUFXLEVBQUUsRUFBRTtvQkFDZixZQUFZLEVBQUUsRUFBRTtvQkFDaEIsUUFBUSxFQUFFLEVBQUU7aUJBQ2IsQ0FBQztZQUVGLE9BQU8sRUFBRSxDQUFDO29CQUNSLFdBQVcsRUFBRSxDQUFDO29CQUNkLFlBQVksRUFBRSxDQUFDO29CQUNmLFFBQVEsRUFBRSxFQUFFO2lCQUNiLENBQUM7WUFFRixPQUFPLEVBQUUsQ0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxZQUFZO29CQUM1QixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsS0FBSyxFQUFFLE1BQU07b0JBQ2IsVUFBVSxFQUFFLEdBQUc7b0JBQ2YsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztvQkFDL0IsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLGFBQWEsRUFBRSxZQUFZO29CQUMzQixTQUFTLEVBQUUsU0FBUztvQkFDcEIsU0FBUyxFQUFFLEVBQUU7b0JBQ2IsUUFBUSxFQUFFLFFBQVE7aUJBQ25CLENBQUM7U0FDSCxDQUFDO1FBRUYsV0FBVyxFQUFFLFlBQVksQ0FBQztZQUN4QixNQUFNLEVBQUUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUUsQ0FBQztZQUMxQixPQUFPLEVBQUUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUUsQ0FBQztZQUUzQixPQUFPLEVBQUUsQ0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsU0FBUyxFQUFFLEVBQUU7b0JBQ2IsUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTtvQkFDNUIsU0FBUyxFQUFFLFNBQVM7b0JBQ3BCLEtBQUssRUFBRSxNQUFNO29CQUNiLFlBQVksRUFBRSxFQUFFO2lCQUNqQixDQUFDO1NBQ0gsQ0FBQztLQUNIO0lBRUQsYUFBYSxFQUFFO1FBQ2IsS0FBSyxFQUFFLE1BQU07UUFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1FBQzlCLGNBQWMsRUFBRSxlQUFlO1FBQy9CLFFBQVEsRUFBRSxNQUFNO1FBQ2hCLFlBQVksRUFBRSxFQUFFO1FBRWhCLFFBQVEsRUFBRTtZQUNSLFNBQVMsRUFBRSxZQUFZLENBQUM7Z0JBQ3RCLE1BQU0sRUFBRSxDQUFDLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxDQUFDO2dCQUM5QixPQUFPLEVBQUUsQ0FBQyxFQUFFLFlBQVksRUFBRSxFQUFFLEVBQUUsQ0FBQztnQkFDL0IsT0FBTyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsa0JBQWtCLEVBQUUsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDMUUsQ0FBQztZQUVGLFNBQVMsRUFBRTtnQkFDVCxLQUFLLEVBQUUsTUFBTTtnQkFDYixVQUFVLEVBQUUsS0FBSztnQkFDakIsa0JBQWtCLEVBQUUsUUFBUTtnQkFDNUIsY0FBYyxFQUFFLE9BQU87Z0JBQ3ZCLFlBQVksRUFBRSxFQUFFO2dCQUNoQixXQUFXLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUN2QyxVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjthQUN0QztTQUNGO0tBQ0Y7SUFFRCxNQUFNLEVBQUU7UUFDTixVQUFVLEVBQUUsVUFBQyxNQUFNLElBQUssT0FBQSxZQUFZLENBQUM7WUFDbkMsTUFBTSxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUM7WUFFdkIsT0FBTyxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLENBQUM7WUFFL0IsT0FBTyxFQUFFLENBQUM7b0JBQ1IsS0FBSyxFQUFFLE1BQU07b0JBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztvQkFDckMsWUFBWSxFQUFFLENBQUM7b0JBQ2YsUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDOUIsZUFBZSxFQUFFLFFBQVEsQ0FBQyxPQUFPO29CQUNqQyxlQUFlLEVBQUUsU0FBTyxNQUFNLE1BQUc7b0JBQ2pDLGtCQUFrQixFQUFFLFFBQVE7b0JBQzVCLGNBQWMsRUFBRSxPQUFPO29CQUN2QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2lCQUNyQyxDQUFDO1NBQ0gsQ0FBQyxFQWpCc0IsQ0FpQnRCO1FBR0YsZUFBZSxFQUFFLFlBQVksQ0FBQztZQUM1QixNQUFNLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLGtCQUFrQixHQUFHLENBQUM7WUFFbkQsT0FBTyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsa0JBQWtCLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxDQUFDO1lBRTFELE9BQU8sRUFBRSxDQUFDO29CQUNSLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLFdBQVc7b0JBQ3JDLFlBQVksRUFBRSxDQUFDO29CQUNmLFFBQVEsRUFBRSxRQUFRO29CQUNsQixTQUFTLEVBQUUsUUFBUSxDQUFDLFVBQVU7b0JBQzlCLGVBQWUsRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDcEMsa0JBQWtCLEVBQUUsWUFBWTtvQkFDaEMsY0FBYyxFQUFFLE9BQU87b0JBQ3ZCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7aUJBQ3JDLENBQUM7U0FDSCxDQUFDO1FBRUYsZUFBZSxFQUFFO1lBQ2YsS0FBSyxFQUFFLE1BQU07WUFDYixVQUFVLEVBQUUsT0FBTztZQUNuQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1NBQ3JDO1FBRUQsb0JBQW9CLEVBQUUsVUFBQyxNQUFNLElBQUssT0FBQSxDQUFDO1lBQ2pDLEtBQUssRUFBRSxNQUFNO1lBQ2IsVUFBVSxFQUFFLE9BQU87WUFDbkIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxlQUFlLEVBQUUsU0FBTyxNQUFNLE1BQUc7WUFDakMsZUFBZSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1lBQ2pDLGtCQUFrQixFQUFFLFlBQVk7WUFDaEMsY0FBYyxFQUFFLE9BQU87U0FDeEIsQ0FBQyxFQVJnQyxDQVFoQztRQUVGLElBQUksRUFBRTtZQUNKLEtBQUssRUFBRSxNQUFNO1lBQ2IsT0FBTyxFQUFFLFNBQVMsS0FBSyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDdEQsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxNQUFNLEVBQUUsQ0FBQztZQUNULElBQUksRUFBRSxDQUFDO1lBQ1AsVUFBVSxFQUFFLFFBQVEsQ0FBQyxZQUFZO1lBQ2pDLE1BQU0sRUFBRSxNQUFNO1lBQ2QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixhQUFhLEVBQUUsUUFBUTtZQUN2QixjQUFjLEVBQUUsVUFBVTtZQUMxQixVQUFVLEVBQUUsWUFBWTtZQUV4QixRQUFRLEVBQUU7Z0JBQ1IsVUFBVSxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMvQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO2dCQUMvQixNQUFNLEVBQUUsRUFBRTtnQkFDVixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsT0FBTyxFQUFFLE9BQU87Z0JBQ2hCLEtBQUssRUFBRSxNQUFNO2dCQUNiLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDMUIsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osWUFBWSxFQUFFLENBQUM7YUFDaEI7WUFDRCxLQUFLLEVBQUU7Z0JBQ0wsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMxQixVQUFVLEVBQUUsVUFBVTtnQkFDdEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLFFBQVEsRUFBRSxTQUFTLEtBQUssTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUN2RCxVQUFVLEVBQUUsU0FBUyxLQUFLLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTTtnQkFDakUsU0FBUyxFQUFFLFNBQVMsS0FBSyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLE1BQU07Z0JBQ2pFLFFBQVEsRUFBRSxTQUFTLEtBQUssTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNO2dCQUM5RCxVQUFVLEVBQUUsUUFBUSxDQUFDLGNBQWM7Z0JBQ25DLFFBQVEsRUFBRSxRQUFRO2FBQ25CO1lBQ0QsT0FBTyxFQUFFO2dCQUNQLEtBQUssRUFBRSxNQUFNO2dCQUNiLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLFFBQVEsRUFBRSxNQUFNO2dCQUNoQixNQUFNLEVBQUUsTUFBTTtnQkFDZCxTQUFTLEVBQUUsTUFBTTtnQkFDakIsUUFBUSxFQUFFLFFBQVE7YUFDbkI7WUFFRCxPQUFPLEVBQUU7Z0JBQ1AsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osVUFBVSxFQUFFLE1BQU07Z0JBQ2xCLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLE9BQU8sRUFBRSxFQUFFO2dCQUNYLFdBQVcsRUFBRSxFQUFFO2dCQUNmLFVBQVUsRUFBRSxVQUFVO2FBQ3ZCO1NBQ0Y7UUFFRCxTQUFTLEVBQUU7WUFDVCxLQUFLLEVBQUUsTUFBTTtZQUNiLE9BQU8sRUFBRSxFQUFFO1lBQ1gsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDL0IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixhQUFhLEVBQUUsUUFBUTtZQUN2QixjQUFjLEVBQUUsWUFBWTtZQUM1QixVQUFVLEVBQUUsWUFBWTtZQUV4QixLQUFLLEVBQUU7Z0JBQ0wsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMxQixVQUFVLEVBQUUsVUFBVTtnQkFDdEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxNQUFNO2dCQUNsQixTQUFTLEVBQUUsTUFBTTtnQkFDakIsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFlBQVksRUFBRSxDQUFDO2FBQ2hCO1lBRUQsV0FBVyxFQUFFO2dCQUNYLFFBQVEsRUFBRSxFQUFFO2dCQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLE9BQU8sRUFBRSxFQUFFO2dCQUNYLFdBQVcsRUFBRSxFQUFFO2dCQUNmLFVBQVUsRUFBRSxVQUFVO2dCQUN0QixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsU0FBUyxFQUFFLEVBQUU7Z0JBQ2IsUUFBUSxFQUFFLFFBQVE7YUFDbkI7U0FDRjtLQUNGO0NBQ00sQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/item/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};





var renderView = function (props) {
    var _a = props, item = _a.item, showViewGroup = _a.showViewGroup, showDescription = _a.showDescription, style = _a.style, size = _a.size;
    var linkProps = {
        key: "item-" + item.id,
        to: routing["Ma" /* ROUTING_MAGAZINE_DETAIL_PATH */] + "/" + item.slug,
        style: Object.assign({}, 'large' === size
            ? item_style.mobile.itemSlider(Object(encode["f" /* decodeEntities */])(item.cover_image.original_url))
            : item_style.mobile.smallItemSlider, style)
    };
    var tags = 0 === item.tags.length ? [item.description] : item.tags;
    tags = tags.map(function (item, index) {
        if ('#' === item.charAt(0)) {
            item = item.slice(1).toLowerCase();
        }
        return item;
    }).filter(function (item, index) { return index < 5; });
    var switchView = {
        'large': function () { return (react["createElement"](react_router_dom["NavLink"], __assign({}, linkProps),
            react["createElement"]("div", { style: item_style.mobile.itemSliderPanel },
                react["createElement"]("div", { style: item_style.mobile.info },
                    react["createElement"]("div", { style: item_style.mobile.info.title }, item.title))))); },
        'normal': function () { return (react["createElement"](react_router_dom["NavLink"], __assign({}, linkProps),
            react["createElement"]("div", { style: item_style.mobile.smallItemSliderPanel(Object(encode["f" /* decodeEntities */])(item.cover_image.original_url)) }),
            react["createElement"]("div", { style: item_style.mobile.smallInfo },
                react["createElement"]("div", { style: item_style.mobile.smallInfo.title }, item.title),
                react["createElement"]("div", { style: item_style.mobile.smallInfo.description }, item.description)))); }
    };
    return switchView[size]();
    // (
    //   <NavLink {...linkProps}>
    //     <div style={[
    //       STYLE.itemContainer.itemWrap.itemImage,
    //       { backgroundImage: `url(${})`, }
    //     ]} />
    //     <div style={STYLE.infoContainer}>
    //       <div style={STYLE.infoContainer.title}>{item.title.toLowerCase()}</div>
    //       {true === showDescription && <div style={STYLE.infoContainer.description}>{item.description}</div>}
    //       {true === showViewGroup && <ViewGroup txtView={item.views} txtTime={convertUnixTime(item.created_at, DATETIME_TYPE_FORMAT.SHORT_DATE)} />}
    //     </div>
    //   </NavLink>
    // );
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sRUFBbUIsY0FBYyxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFFeEUsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFLdEYsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRTVCLElBQU0sVUFBVSxHQUFHLFVBQUMsS0FBSztJQUNqQixJQUFBLFVBQXVFLEVBQXJFLGNBQUksRUFBRSxnQ0FBYSxFQUFFLG9DQUFlLEVBQUUsZ0JBQUssRUFBRSxjQUFJLENBQXFCO0lBRTlFLElBQU0sU0FBUyxHQUFHO1FBQ2hCLEdBQUcsRUFBRSxVQUFRLElBQUksQ0FBQyxFQUFJO1FBQ3RCLEVBQUUsRUFBSyw0QkFBNEIsU0FBSSxJQUFJLENBQUMsSUFBTTtRQUNsRCxLQUFLLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsT0FBTyxLQUFLLElBQUk7WUFDdkMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3hFLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLGVBQWUsRUFBRSxLQUFLLENBQUM7S0FDekMsQ0FBQztJQUVGLElBQUksSUFBSSxHQUFHLENBQUMsS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7SUFFbkUsSUFBSSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFJLEVBQUUsS0FBSztRQUMxQixFQUFFLENBQUMsQ0FBQyxHQUFHLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQTtRQUFDLENBQUM7UUFDbEUsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNkLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLLElBQUssT0FBQSxLQUFLLEdBQUcsQ0FBQyxFQUFULENBQVMsQ0FBQyxDQUFDO0lBRXRDLElBQU0sVUFBVSxHQUFHO1FBQ2pCLE9BQU8sRUFBRSxjQUFNLE9BQUEsQ0FDYixvQkFBQyxPQUFPLGVBQUssU0FBUztZQUNwQiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxlQUFlO2dCQUN0Qyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJO29CQUMzQiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFHLElBQUksQ0FBQyxLQUFLLENBQU8sQ0FDbkQsQ0FDRixDQUNFLENBQ1gsRUFSYyxDQVFkO1FBRUQsUUFBUSxFQUFFLGNBQU0sT0FBQSxDQUNkLG9CQUFDLE9BQU8sZUFBSyxTQUFTO1lBQ3BCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLG9CQUFvQixDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFDLEdBQVE7WUFDcEcsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUztnQkFFaEMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssSUFBRyxJQUFJLENBQUMsS0FBSyxDQUFPO2dCQUM1RCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsV0FBVyxJQUFHLElBQUksQ0FBQyxXQUFXLENBQU8sQ0FDcEUsQ0FDRSxDQUNYLEVBVGUsQ0FTZjtLQUNGLENBQUE7SUFFRCxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUM7SUFDMUIsSUFBSTtJQUNKLDZCQUE2QjtJQUM3QixvQkFBb0I7SUFDcEIsZ0RBQWdEO0lBQ2hELHlDQUF5QztJQUN6QyxZQUFZO0lBQ1osd0NBQXdDO0lBQ3hDLGdGQUFnRjtJQUNoRiw0R0FBNEc7SUFDNUcsbUpBQW1KO0lBQ25KLGFBQWE7SUFDYixlQUFlO0lBQ2YsS0FBSztBQUNQLENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/item/initialize.tsx
var DEFAULT_PROPS = {
    item: {},
    showViewGroup: true,
    showDescription: true,
    style: {},
    size: 'normal'
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLGFBQWEsRUFBRSxJQUFJO0lBQ25CLGVBQWUsRUFBRSxJQUFJO0lBQ3JCLEtBQUssRUFBRSxFQUFFO0lBQ1QsSUFBSSxFQUFFLFFBQVE7Q0FDZixDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/item/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_Item = /** @class */ (function (_super) {
    __extends(Item, _super);
    function Item(props) {
        return _super.call(this, props) || this;
    }
    Item.prototype.render = function () {
        return view(this.props);
    };
    ;
    Item.defaultProps = DEFAULT_PROPS;
    Item = __decorate([
        radium
    ], Item);
    return Item;
}(react["PureComponent"]));
/* harmony default export */ var component = (component_Item);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFFaEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUc3QztJQUFtQix3QkFBNkI7SUFFOUMsY0FBWSxLQUFhO2VBQ3ZCLGtCQUFNLEtBQUssQ0FBQztJQUNkLENBQUM7SUFFRCxxQkFBTSxHQUFOO1FBQ0UsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUFBLENBQUM7SUFQSyxpQkFBWSxHQUFHLGFBQWEsQ0FBQztJQURoQyxJQUFJO1FBRFQsTUFBTTtPQUNELElBQUksQ0FTVDtJQUFELFdBQUM7Q0FBQSxBQVRELENBQW1CLGFBQWEsR0FTL0I7QUFFRCxlQUFlLElBQUksQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/item/index.tsx

/* harmony default export */ var magazine_item = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxJQUFJLE1BQU0sYUFBYSxDQUFDO0FBQy9CLGVBQWUsSUFBSSxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/item-list/style.tsx


/* harmony default export */ var item_list_style = ({
    container: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{ marginBottom: 10, paddingLeft: 5, paddingRight: 5 }],
        DESKTOP: [{ paddingLeft: 0, paddingRight: 0 }],
        GENERAL: [{
                width: '100%',
                display: variable["display"].flex,
                justifyContent: 'space-between',
                flexWrap: 'wrap',
            }]
    }),
    searchList: {
        marginBottom: 50
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFekQsZUFBZTtJQUNiLFNBQVMsRUFBRSxZQUFZLENBQUM7UUFDdEIsTUFBTSxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLFdBQVcsRUFBRSxDQUFDLEVBQUUsWUFBWSxFQUFFLENBQUMsRUFBRSxDQUFDO1FBRS9ELE9BQU8sRUFBRSxDQUFDLEVBQUUsV0FBVyxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsQ0FBQyxFQUFFLENBQUM7UUFFOUMsT0FBTyxFQUFFLENBQUM7Z0JBQ1IsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsY0FBYyxFQUFFLGVBQWU7Z0JBQy9CLFFBQVEsRUFBRSxNQUFNO2FBQ2pCLENBQUM7S0FDSCxDQUFDO0lBRUYsVUFBVSxFQUFFO1FBQ1YsWUFBWSxFQUFFLEVBQUU7S0FDakI7Q0FDTSxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/item-list/view.tsx
var view_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};



var view_renderView = function (props) {
    var _a = props, list = _a.list, showViewGroup = _a.showViewGroup, showDescription = _a.showDescription, style = _a.style, isSearchList = _a.isSearchList, size = _a.size;
    var num = list.length - 1;
    var renderItem = function (item, index) {
        var itemProps = {
            item: item,
            showDescription: showDescription,
            showViewGroup: showViewGroup,
            size: 0 < size.length ? size : (0 === index || 7 === index ? 'large' : 'normal'),
            style: Object.assign({}, true === isSearchList && index === num && item_list_style.searchList, style)
        };
        return react["createElement"](magazine_item, view_assign({ key: "item-" + item.id }, itemProps));
    };
    return (react["createElement"]("div", { style: item_list_style.container }, Array.isArray(list) && list.map(renderItem)));
};
/* harmony default export */ var item_list_view = (view_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxJQUFJLE1BQU0sU0FBUyxDQUFDO0FBRTNCLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixJQUFNLFVBQVUsR0FBRyxVQUFDLEtBQUs7SUFDakIsSUFBQSxVQUFxRixFQUFuRixjQUFJLEVBQUUsZ0NBQWEsRUFBRSxvQ0FBZSxFQUFFLGdCQUFLLEVBQUUsOEJBQVksRUFBRSxjQUFJLENBQXFCO0lBQzVGLElBQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBRTVCLElBQU0sVUFBVSxHQUFHLFVBQUMsSUFBSSxFQUFFLEtBQUs7UUFDN0IsSUFBTSxTQUFTLEdBQUc7WUFDaEIsSUFBSSxFQUFFLElBQUk7WUFDVixlQUFlLEVBQUUsZUFBZTtZQUNoQyxhQUFhLEVBQUUsYUFBYTtZQUM1QixJQUFJLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDO1lBQ2hGLEtBQUssRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxJQUFJLEtBQUssWUFBWSxJQUFJLEtBQUssS0FBSyxHQUFHLElBQUksS0FBSyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUM7U0FDNUYsQ0FBQztRQUNGLE1BQU0sQ0FBQyxvQkFBQyxJQUFJLGFBQUMsR0FBRyxFQUFFLFVBQVEsSUFBSSxDQUFDLEVBQUksSUFBTSxTQUFTLEVBQUksQ0FBQztJQUN6RCxDQUFDLENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFNBQVMsSUFDeEIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUN4QyxDQUNQLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/item-list/initialize.tsx
var SWITCH_DEFAULT_PROPS = {
    MOBILE: { showViewGroup: false, showDescription: false },
    DESKTOP: { showViewGroup: true, showDescription: true }
};
var initialize_DEFAULT_PROPS = Object.assign({}, SWITCH_DEFAULT_PROPS[window.DEVICE_VERSION], {
    list: [],
    style: {},
    isSearchList: false,
    size: ''
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLElBQU0sb0JBQW9CLEdBQUc7SUFDM0IsTUFBTSxFQUFFLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFO0lBQ3hELE9BQU8sRUFBRSxFQUFFLGFBQWEsRUFBRSxJQUFJLEVBQUUsZUFBZSxFQUFFLElBQUksRUFBRTtDQUN4RCxDQUFDO0FBRUYsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUMzQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQzNDO0lBQ0UsSUFBSSxFQUFFLEVBQUU7SUFDUixLQUFLLEVBQUUsRUFBRTtJQUNULFlBQVksRUFBRSxLQUFLO0lBQ25CLElBQUksRUFBRSxFQUFFO0NBQ1QsQ0FDRixDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/item-list/component.tsx
var component_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var component_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_ItemList = /** @class */ (function (_super) {
    component_extends(ItemList, _super);
    function ItemList(props) {
        return _super.call(this, props) || this;
    }
    ItemList.prototype.render = function () {
        return item_list_view(this.props);
    };
    ;
    ItemList.defaultProps = initialize_DEFAULT_PROPS;
    ItemList = component_decorate([
        radium
    ], ItemList);
    return ItemList;
}(react["PureComponent"]));
/* harmony default export */ var item_list_component = (component_ItemList);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFFaEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUc3QztJQUF1Qiw0QkFBNkI7SUFFbEQsa0JBQVksS0FBYTtlQUN2QixrQkFBTSxLQUFLLENBQUM7SUFDZCxDQUFDO0lBRUQseUJBQU0sR0FBTjtRQUNFLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFBQSxDQUFDO0lBUEsscUJBQVksR0FBRyxhQUFhLENBQUM7SUFEaEMsUUFBUTtRQURiLE1BQU07T0FDRCxRQUFRLENBU2I7SUFBRCxlQUFDO0NBQUEsQUFURCxDQUF1QixhQUFhLEdBU25DO0FBRUQsZUFBZSxRQUFRLENBQUMifQ==
// CONCATENATED MODULE: ./components/magazine/item-list/index.tsx

/* harmony default export */ var item_list = __webpack_exports__["a"] = (item_list_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxRQUFRLE1BQU0sYUFBYSxDQUFDO0FBQ25DLGVBQWUsUUFBUSxDQUFDIn0=

/***/ }),

/***/ 842:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MAGAZINE_WIDGET_TYPE; });
var MAGAZINE_WIDGET_TYPE = {
    ONE: {
        type: 1,
        title: '',
        url: ''
    },
    TWO: {
        type: 2,
        title: '',
        url: ''
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFnYXppbmUtd2lkZ2V0LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibWFnYXppbmUtd2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE1BQU0sQ0FBQyxJQUFNLG9CQUFvQixHQUFHO0lBQ2xDLEdBQUcsRUFBRTtRQUNILElBQUksRUFBRSxDQUFDO1FBQ1AsS0FBSyxFQUFFLEVBQUU7UUFDVCxHQUFHLEVBQUUsRUFBRTtLQUNSO0lBRUQsR0FBRyxFQUFFO1FBQ0gsSUFBSSxFQUFFLENBQUM7UUFDUCxLQUFLLEVBQUUsRUFBRTtRQUNULEdBQUcsRUFBRSxFQUFFO0tBQ1I7Q0FDRixDQUFDIn0=

/***/ }),

/***/ 845:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./constants/application/magazine-widget.ts
var magazine_widget = __webpack_require__(842);

// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./utils/encode.ts
var encode = __webpack_require__(7);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./container/layout/aside-block/index.tsx
var aside_block = __webpack_require__(787);

// EXTERNAL MODULE: ./components/ui/loading-placeholder/index.tsx + 4 modules
var loading_placeholder = __webpack_require__(750);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./components/magazine/category-widget/style.tsx



/* harmony default export */ var style = ({
    navigationBlock: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{ paddingLeft: 10, paddingRight: 10 }],
        DESKTOP: [{ paddingLeft: 0 }],
        GENERAL: [{ display: variable["display"].block, marginBottom: 20 }]
    }),
    categoryWidgetTwo: {
        listContent: Object.assign({}, layout["a" /* flexContainer */].center, layout["a" /* flexContainer */].wrap, layout["a" /* flexContainer */].justify)
    },
    categoryWidgetOne: {
        item: {
            display: variable["display"].flex,
            padding: '15px 0',
            borderBottom: "1px solid " + variable["colorE5"],
            imageWrap: {
                display: variable["display"].flex,
                alignItems: 'center',
                justifyContent: 'center',
                width: 70,
                marginRight: 10
            },
            itemImage: {
                display: variable["display"].flex,
                width: 100,
                paddingTop: '62.5%',
                backgroundSize: 'cover',
                backgroundPosition: 'top center',
                transition: variable["transitionNormal"],
            },
            itemInfo: {
                display: variable["display"].flex,
                flex: 10,
                flexDirection: 'column',
                infoCategory: {
                    fontSize: 12,
                    textTransform: 'uppercase',
                    color: variable["colorBlack06"],
                    lineHeight: '14px',
                    marginBottom: 16,
                    display: 'block',
                    width: '100%',
                },
                infoTitle: {
                    fontSize: 16,
                    lineHeight: '22px',
                    color: variable["colorBlack"],
                    whiteSpace: 'pre-wrap',
                    fontFamily: variable["fontAvenirMedium"],
                    width: '100%',
                    display: variable["display"].block,
                    textTransform: 'capitalize',
                    maxHeight: 44,
                    overflow: 'hidden'
                },
            }
        },
        lastItem: {
            borderBottom: 'none'
        }
    },
    placeholder: {
        width: '100%',
        paddingTop: 20,
        product: {
            display: variable["display"].flex,
            flexWrap: 'wrap',
            justifyContent: 'space-between',
            marginTop: 15,
            marginBottom: 15,
            borderBottom: "1px solid " + variable["colorE5"],
            paddingBottom: 15,
            image: {
                width: 70,
                height: 50,
                marginRight: 10
            },
            textGroup: {
                flex: 1,
                display: variable["display"].flex,
                flexDirection: 'column',
                text: {
                    width: '100%',
                    height: 20,
                    marginBottom: 10
                }
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEtBQUssTUFBTSxNQUFNLHVCQUF1QixDQUFDO0FBQ2hELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUV6RCxlQUFlO0lBQ2IsZUFBZSxFQUFFLFlBQVksQ0FBQztRQUM1QixNQUFNLEVBQUUsQ0FBQyxFQUFFLFdBQVcsRUFBRSxFQUFFLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxDQUFDO1FBQy9DLE9BQU8sRUFBRSxDQUFDLEVBQUUsV0FBVyxFQUFFLENBQUMsRUFBRSxDQUFDO1FBQzdCLE9BQU8sRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLFlBQVksRUFBRSxFQUFFLEVBQUUsQ0FBQztLQUNqRSxDQUFDO0lBRUYsaUJBQWlCLEVBQUU7UUFDakIsV0FBVyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUMzQixNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFDM0IsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQ3pCLE1BQU0sQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUM3QjtLQUNGO0lBRUQsaUJBQWlCLEVBQUU7UUFDakIsSUFBSSxFQUFFO1lBQ0osT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixPQUFPLEVBQUUsUUFBUTtZQUNqQixZQUFZLEVBQUUsZUFBYSxRQUFRLENBQUMsT0FBUztZQUU3QyxTQUFTLEVBQUU7Z0JBQ1QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsVUFBVSxFQUFFLFFBQVE7Z0JBQ3BCLGNBQWMsRUFBRSxRQUFRO2dCQUN4QixLQUFLLEVBQUUsRUFBRTtnQkFDVCxXQUFXLEVBQUUsRUFBRTthQUNoQjtZQUVELFNBQVMsRUFBRTtnQkFDVCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixLQUFLLEVBQUUsR0FBRztnQkFDVixVQUFVLEVBQUUsT0FBTztnQkFDbkIsY0FBYyxFQUFFLE9BQU87Z0JBQ3ZCLGtCQUFrQixFQUFFLFlBQVk7Z0JBQ2hDLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO2FBQ3RDO1lBRUQsUUFBUSxFQUFFO2dCQUNSLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLElBQUksRUFBRSxFQUFFO2dCQUNSLGFBQWEsRUFBRSxRQUFRO2dCQUV2QixZQUFZLEVBQUU7b0JBQ1osUUFBUSxFQUFFLEVBQUU7b0JBQ1osYUFBYSxFQUFFLFdBQVc7b0JBQzFCLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTtvQkFDNUIsVUFBVSxFQUFFLE1BQU07b0JBQ2xCLFlBQVksRUFBRSxFQUFFO29CQUNoQixPQUFPLEVBQUUsT0FBTztvQkFDaEIsS0FBSyxFQUFFLE1BQU07aUJBRWQ7Z0JBRUQsU0FBUyxFQUFFO29CQUNULFFBQVEsRUFBRSxFQUFFO29CQUNaLFVBQVUsRUFBRSxNQUFNO29CQUNsQixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7b0JBQzFCLFVBQVUsRUFBRSxVQUFVO29CQUN0QixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsS0FBSyxFQUFFLE1BQU07b0JBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztvQkFDL0IsYUFBYSxFQUFFLFlBQVk7b0JBQzNCLFNBQVMsRUFBRSxFQUFFO29CQUNiLFFBQVEsRUFBRSxRQUFRO2lCQUNuQjthQUNGO1NBQ0Y7UUFDRCxRQUFRLEVBQUU7WUFDUixZQUFZLEVBQUUsTUFBTTtTQUNyQjtLQUNGO0lBRUQsV0FBVyxFQUFFO1FBQ1gsS0FBSyxFQUFFLE1BQU07UUFDYixVQUFVLEVBQUUsRUFBRTtRQUVkLE9BQU8sRUFBRTtZQUNQLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsUUFBUSxFQUFFLE1BQU07WUFDaEIsY0FBYyxFQUFFLGVBQWU7WUFDL0IsU0FBUyxFQUFFLEVBQUU7WUFDYixZQUFZLEVBQUUsRUFBRTtZQUNoQixZQUFZLEVBQUUsZUFBYSxRQUFRLENBQUMsT0FBUztZQUM3QyxhQUFhLEVBQUUsRUFBRTtZQUVqQixLQUFLLEVBQUU7Z0JBQ0wsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsV0FBVyxFQUFFLEVBQUU7YUFDaEI7WUFFRCxTQUFTLEVBQUU7Z0JBQ1QsSUFBSSxFQUFFLENBQUM7Z0JBQ1AsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsYUFBYSxFQUFFLFFBQVE7Z0JBRXZCLElBQUksRUFBRTtvQkFDSixLQUFLLEVBQUUsTUFBTTtvQkFDYixNQUFNLEVBQUUsRUFBRTtvQkFDVixZQUFZLEVBQUUsRUFBRTtpQkFDakI7YUFDRjtTQUNGO0tBQ0Y7Q0FDTSxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/category-widget/view-one.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};







var categoryWidgetStyle = style.categoryWidgetOne.item;
var renderItemPlaceholder = function (item) { return (react["createElement"]("div", { style: style.placeholder.product, key: "loading-item-" + item },
    react["createElement"](loading_placeholder["a" /* default */], { style: style.placeholder.product.image }),
    react["createElement"]("div", { style: style.placeholder.product.textGroup },
        react["createElement"](loading_placeholder["a" /* default */], { style: style.placeholder.product.textGroup.text }),
        react["createElement"](loading_placeholder["a" /* default */], { style: style.placeholder.product.textGroup.text })))); };
var renderLoadingPlaceholder = function () { return [1, 2, 3, 4, 5, 6].map(function (item) { return renderItemPlaceholder(item); }); };
var renderContent = function (_a) {
    var list = _a.list;
    return (react["createElement"]("category-widget", null, Array.isArray(list)
        && list.map(function (item, index) {
            var navLinkProps = {
                key: "category-widget-item-" + index,
                style: Object.assign({}, categoryWidgetStyle, index === list.length - 1 && style.categoryWidgetOne.lastItem),
                to: routing["Ma" /* ROUTING_MAGAZINE_DETAIL_PATH */] + "/" + item.slug
            };
            return (react["createElement"](react_router_dom["NavLink"], __assign({}, navLinkProps),
                react["createElement"]("div", { style: categoryWidgetStyle.imageWrap },
                    react["createElement"]("div", { style: [categoryWidgetStyle.itemImage, { backgroundImage: "url(" + Object(encode["f" /* decodeEntities */])(item.cover_image.original_url) + ")" }] })),
                react["createElement"]("div", { style: categoryWidgetStyle.itemInfo },
                    react["createElement"]("div", { style: categoryWidgetStyle.itemInfo.infoTitle }, item.title.toLowerCase()))));
        })));
};
var renderView = function (props) {
    var _a = props, title = _a.title, list = _a.list;
    var asideBlockProps = {
        title: title,
        style: style.navigationBlock,
        content: Array.isArray(list) && 0 === list.length ? renderLoadingPlaceholder() : renderContent({ list: list })
    };
    return react["createElement"](aside_block["a" /* default */], __assign({}, asideBlockProps));
};
/* harmony default export */ var view_one = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1vbmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LW9uZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUUvQixPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFFM0MsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3ZELE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQ3RGLE9BQU8sVUFBVSxNQUFNLHVDQUF1QyxDQUFDO0FBQy9ELE9BQU8sa0JBQWtCLE1BQU0sOEJBQThCLENBQUM7QUFFOUQsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRTVCLElBQU0sbUJBQW1CLEdBQUcsS0FBSyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQztBQUV6RCxJQUFNLHFCQUFxQixHQUFHLFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FDdEMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLEdBQUcsRUFBRSxrQkFBZ0IsSUFBTTtJQUNoRSxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFJO0lBQzlELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxTQUFTO1FBQzdDLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFJO1FBQ3ZFLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFJLENBQ25FLENBQ0YsQ0FDUCxFQVJ1QyxDQVF2QyxDQUFDO0FBRUYsSUFBTSx3QkFBd0IsR0FBRyxjQUFNLE9BQUEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksSUFBSyxPQUFBLHFCQUFxQixDQUFDLElBQUksQ0FBQyxFQUEzQixDQUEyQixDQUFDLEVBQTdELENBQTZELENBQUM7QUFFckcsSUFBTSxhQUFhLEdBQUcsVUFBQyxFQUFRO1FBQU4sY0FBSTtJQUMzQixNQUFNLENBQUMsQ0FDTCw2Q0FFSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztXQUNoQixJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBSSxFQUFFLEtBQUs7WUFFdEIsSUFBTSxZQUFZLEdBQUc7Z0JBQ25CLEdBQUcsRUFBRSwwQkFBd0IsS0FBTztnQkFDcEMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLG1CQUFtQixFQUFFLEtBQUssS0FBSyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDO2dCQUM1RyxFQUFFLEVBQUssNEJBQTRCLFNBQUksSUFBSSxDQUFDLElBQU07YUFDbkQsQ0FBQTtZQUVELE1BQU0sQ0FBQyxDQUNMLG9CQUFDLE9BQU8sZUFBSyxZQUFZO2dCQUN2Qiw2QkFBSyxLQUFLLEVBQUUsbUJBQW1CLENBQUMsU0FBUztvQkFDdkMsNkJBQUssS0FBSyxFQUFFLENBQUMsbUJBQW1CLENBQUMsU0FBUyxFQUFFLEVBQUUsZUFBZSxFQUFFLFNBQU8sY0FBYyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQUcsRUFBRSxDQUFDLEdBQVEsQ0FDN0g7Z0JBQ04sNkJBQUssS0FBSyxFQUFFLG1CQUFtQixDQUFDLFFBQVE7b0JBRXRDLDZCQUFLLEtBQUssRUFBRSxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsU0FBUyxJQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQU8sQ0FDaEYsQ0FDRSxDQUNYLENBQUM7UUFDSixDQUFDLENBQUMsQ0FFWSxDQUNuQixDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsSUFBTSxVQUFVLEdBQUcsVUFBQyxLQUFhO0lBQ3pCLElBQUEsVUFBaUMsRUFBL0IsZ0JBQUssRUFBRSxjQUFJLENBQXFCO0lBQ3hDLElBQU0sZUFBZSxHQUFHO1FBQ3RCLEtBQUssRUFBRSxLQUFLO1FBQ1osS0FBSyxFQUFFLEtBQUssQ0FBQyxlQUFlO1FBQzVCLE9BQU8sRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyx3QkFBd0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDO0tBQ3pHLENBQUM7SUFFRixNQUFNLENBQUMsb0JBQUMsVUFBVSxlQUFLLGVBQWUsRUFBSSxDQUFDO0FBQzdDLENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// EXTERNAL MODULE: ./components/magazine/item-list/index.tsx + 9 modules
var item_list = __webpack_require__(835);

// CONCATENATED MODULE: ./components/magazine/category-widget/view-two.tsx
var view_two_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};




var view_two_renderContent = function (_a) {
    var list = _a.list;
    var itemListProps = {
        list: list,
        showDescription: false,
        showViewGroup: false,
        style: { width: '100%', margin: '0 0 20px' },
        size: 'normal'
    };
    return (react["createElement"]("category-widget", { style: style.categoryWidgetTwo },
        react["createElement"]("div", { style: style.categoryWidgetTwo.listContent },
            react["createElement"](item_list["a" /* default */], view_two_assign({}, itemListProps)))));
};
var view_two_renderView = function (props) {
    var _a = props, title = _a.title, list = _a.list;
    var asideBlockProps = {
        title: title,
        style: style.navigationBlock,
        content: view_two_renderContent({ list: list })
    };
    return react["createElement"](aside_block["a" /* default */], view_two_assign({}, asideBlockProps));
};
/* harmony default export */ var view_two = (view_two_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy10d28uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LXR3by50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUcvQixPQUFPLFVBQVUsTUFBTSx1Q0FBdUMsQ0FBQztBQUUvRCxPQUFPLFFBQVEsTUFBTSxjQUFjLENBQUM7QUFFcEMsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRTVCLElBQU0sYUFBYSxHQUFHLFVBQUMsRUFBUTtRQUFOLGNBQUk7SUFDM0IsSUFBTSxhQUFhLEdBQUc7UUFDcEIsSUFBSSxFQUFFLElBQUk7UUFDVixlQUFlLEVBQUUsS0FBSztRQUN0QixhQUFhLEVBQUUsS0FBSztRQUNwQixLQUFLLEVBQUUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUU7UUFDNUMsSUFBSSxFQUFFLFFBQVE7S0FDZixDQUFDO0lBQ0YsTUFBTSxDQUFDLENBQ0wseUNBQWlCLEtBQUssRUFBRSxLQUFLLENBQUMsaUJBQWlCO1FBQzdDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsaUJBQWlCLENBQUMsV0FBVztZQUM3QyxvQkFBQyxRQUFRLGVBQUssYUFBYSxFQUFJLENBQzNCLENBQ1UsQ0FDbkIsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLElBQU0sVUFBVSxHQUFHLFVBQUMsS0FBYTtJQUN6QixJQUFBLFVBQWlDLEVBQS9CLGdCQUFLLEVBQUUsY0FBSSxDQUFxQjtJQUN4QyxJQUFNLGVBQWUsR0FBRztRQUN0QixLQUFLLEVBQUUsS0FBSztRQUNaLEtBQUssRUFBRSxLQUFLLENBQUMsZUFBZTtRQUM1QixPQUFPLEVBQUUsYUFBYSxDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQztLQUNqQyxDQUFDO0lBRUYsTUFBTSxDQUFDLG9CQUFDLFVBQVUsZUFBSyxlQUFlLEVBQUksQ0FBQztBQUM3QyxDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/category-widget/view.tsx



var view_renderView = function (props) {
    var type = props.type;
    switch (type) {
        case magazine_widget["a" /* MAGAZINE_WIDGET_TYPE */].ONE.type: return view_one(props);
        case magazine_widget["a" /* MAGAZINE_WIDGET_TYPE */].TWO.type: return view_two(props);
        default: return view_one(props);
    }
};
/* harmony default export */ var view = (view_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBR3RGLE9BQU8sYUFBYSxNQUFNLFlBQVksQ0FBQztBQUN2QyxPQUFPLGFBQWEsTUFBTSxZQUFZLENBQUM7QUFFdkMsSUFBTSxVQUFVLEdBQUcsVUFBQyxLQUFhO0lBQ3ZCLElBQUEsaUJBQUksQ0FBcUI7SUFDakMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNiLEtBQUssb0JBQW9CLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2hFLEtBQUssb0JBQW9CLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRWhFLFNBQVMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN2QyxDQUFDO0FBQ0gsQ0FBQyxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// CONCATENATED MODULE: ./components/magazine/category-widget/initialize.tsx
var DEFAULT_PROPS = {
    list: []
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtDQUNULENBQUMifQ==
// CONCATENATED MODULE: ./components/magazine/category-widget/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_MagazineCategoryWidget = /** @class */ (function (_super) {
    __extends(MagazineCategoryWidget, _super);
    function MagazineCategoryWidget(props) {
        return _super.call(this, props) || this;
    }
    MagazineCategoryWidget.prototype.render = function () {
        return view(this.props);
    };
    ;
    MagazineCategoryWidget.defaultProps = DEFAULT_PROPS;
    MagazineCategoryWidget = __decorate([
        radium
    ], MagazineCategoryWidget);
    return MagazineCategoryWidget;
}(react["PureComponent"]));
/* harmony default export */ var component = (component_MagazineCategoryWidget);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFFaEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUc3QztJQUFxQywwQ0FBNkI7SUFFaEUsZ0NBQVksS0FBYTtlQUN2QixrQkFBTSxLQUFLLENBQUM7SUFDZCxDQUFDO0lBRUQsdUNBQU0sR0FBTjtRQUNFLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFBQSxDQUFDO0lBUEssbUNBQVksR0FBVyxhQUFhLENBQUM7SUFEeEMsc0JBQXNCO1FBRDNCLE1BQU07T0FDRCxzQkFBc0IsQ0FTM0I7SUFBRCw2QkFBQztDQUFBLEFBVEQsQ0FBcUMsYUFBYSxHQVNqRDtBQUVELGVBQWUsc0JBQXNCLENBQUMifQ==
// CONCATENATED MODULE: ./components/magazine/category-widget/index.tsx

/* harmony default export */ var category_widget = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxzQkFBc0IsTUFBTSxhQUFhLENBQUM7QUFDakQsZUFBZSxzQkFBc0IsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/right-widget-group/view.tsx
var view_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};


var right_widget_group_view_renderView = function (props) {
    var dataList = props.dataList;
    return (react["createElement"]("magazine-right-widget-group", null, Array.isArray(dataList)
        && dataList.map(function (item, index) {
            var categoryWidgetProps = {
                list: item.list,
                type: item.type,
                title: item.title,
                key: "category-widget-item-" + index
            };
            return item.list && item.list.length > 0 ? react["createElement"](category_widget, view_assign({}, categoryWidgetProps)) : null;
        })));
};
/* harmony default export */ var right_widget_group_view = (right_widget_group_view_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxjQUFjLE1BQU0sb0JBQW9CLENBQUM7QUFJaEQsSUFBTSxVQUFVLEdBQUcsVUFBQyxLQUFhO0lBQ3ZCLElBQUEseUJBQVEsQ0FBcUI7SUFFckMsTUFBTSxDQUFDLENBQ0wseURBRUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUM7V0FDcEIsUUFBUSxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLO1lBQzFCLElBQU0sbUJBQW1CLEdBQUc7Z0JBQzFCLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtnQkFDZixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7Z0JBQ2YsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO2dCQUNqQixHQUFHLEVBQUUsMEJBQXdCLEtBQU87YUFDckMsQ0FBQztZQUVGLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsb0JBQUMsY0FBYyxlQUFLLG1CQUFtQixFQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUNoRyxDQUFDLENBQUMsQ0FFd0IsQ0FDL0IsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/right-widget-group/initialize.tsx
var initialize_DEFAULT_PROPS = {
    dataList: []
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixRQUFRLEVBQUUsRUFBRTtDQUNILENBQUMifQ==
// CONCATENATED MODULE: ./components/magazine/right-widget-group/component.tsx
var component_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var component_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_RightWidgetGroupComponent = /** @class */ (function (_super) {
    component_extends(RightWidgetGroupComponent, _super);
    function RightWidgetGroupComponent(props) {
        return _super.call(this, props) || this;
    }
    RightWidgetGroupComponent.prototype.render = function () {
        return right_widget_group_view(this.props);
    };
    ;
    RightWidgetGroupComponent.defaultProps = initialize_DEFAULT_PROPS;
    RightWidgetGroupComponent = component_decorate([
        radium
    ], RightWidgetGroupComponent);
    return RightWidgetGroupComponent;
}(react["PureComponent"]));
/* harmony default export */ var right_widget_group_component = (component_RightWidgetGroupComponent);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFFaEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUc3QztJQUF3Qyw2Q0FBNkI7SUFFbkUsbUNBQVksS0FBYTtlQUN2QixrQkFBTSxLQUFLLENBQUM7SUFDZCxDQUFDO0lBRUQsMENBQU0sR0FBTjtRQUNFLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFBQSxDQUFDO0lBUEssc0NBQVksR0FBVyxhQUFhLENBQUM7SUFEeEMseUJBQXlCO1FBRDlCLE1BQU07T0FDRCx5QkFBeUIsQ0FTOUI7SUFBRCxnQ0FBQztDQUFBLEFBVEQsQ0FBd0MsYUFBYSxHQVNwRDtBQUVELGVBQWUseUJBQXlCLENBQUMifQ==
// CONCATENATED MODULE: ./components/magazine/right-widget-group/index.tsx

/* harmony default export */ var right_widget_group = __webpack_exports__["a"] = (right_widget_group_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyx5QkFBeUIsTUFBTSxhQUFhLENBQUM7QUFDcEQsZUFBZSx5QkFBeUIsQ0FBQyJ9

/***/ })

}]);