(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[85],{

/***/ 754:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return updateMetaInfoAction; });
/* harmony import */ var _constants_api_meta__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(352);

var updateMetaInfoAction = function (data) { return ({
    type: _constants_api_meta__WEBPACK_IMPORTED_MODULE_0__[/* UPDATE_META_INFO */ "a"],
    payload: data
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWV0YS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1ldGEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLGdCQUFnQixFQUNqQixNQUFNLHVCQUF1QixDQUFDO0FBRS9CLE1BQU0sQ0FBQyxJQUFNLG9CQUFvQixHQUFHLFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FBQztJQUM3QyxJQUFJLEVBQUUsZ0JBQWdCO0lBQ3RCLE9BQU8sRUFBRSxJQUFJO0NBQ2QsQ0FBQyxFQUg0QyxDQUc1QyxDQUFDIn0=

/***/ }),

/***/ 808:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// CONCATENATED MODULE: ./api/game.ts

var loadGame = function () { return Object(restful_method["d" /* post */])({
    path: "/games/load_game",
    description: 'Load game',
    errorMesssage: "Can't load game. Please try again",
}); };
var getUserGift = function () { return Object(restful_method["b" /* get */])({
    path: "/games/user_gifts",
    description: 'load user gift',
    errorMesssage: "Can't load game. Please try again",
}); };
var getTodayGift = function () { return Object(restful_method["b" /* get */])({
    path: "/games/today_gifts",
    description: 'load today gift',
    errorMesssage: "Can't load game. Please try again",
}); };
var playGame = function (_a) {
    var id = _a.id;
    return Object(restful_method["c" /* patch */])({
        path: "/games/" + id + "/play",
        description: 'load today gift',
        errorMesssage: "Can't load game. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2FtZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImdhbWUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFNUQsTUFBTSxDQUFDLElBQU0sUUFBUSxHQUFHLGNBQU8sT0FBQSxJQUFJLENBQUM7SUFDbEMsSUFBSSxFQUFFLGtCQUFrQjtJQUN4QixXQUFXLEVBQUUsV0FBVztJQUN4QixhQUFhLEVBQUUsbUNBQW1DO0NBQ25ELENBQUMsRUFKNkIsQ0FJN0IsQ0FBQztBQUVILE1BQU0sQ0FBQyxJQUFNLFdBQVcsR0FBRyxjQUFPLE9BQUEsR0FBRyxDQUFDO0lBQ3BDLElBQUksRUFBRSxtQkFBbUI7SUFDekIsV0FBVyxFQUFFLGdCQUFnQjtJQUM3QixhQUFhLEVBQUUsbUNBQW1DO0NBQ25ELENBQUMsRUFKZ0MsQ0FJaEMsQ0FBQztBQUVILE1BQU0sQ0FBQyxJQUFNLFlBQVksR0FBRyxjQUFPLE9BQUEsR0FBRyxDQUFDO0lBQ3JDLElBQUksRUFBRSxvQkFBb0I7SUFDMUIsV0FBVyxFQUFFLGlCQUFpQjtJQUM5QixhQUFhLEVBQUUsbUNBQW1DO0NBQ25ELENBQUMsRUFKaUMsQ0FJakMsQ0FBQztBQUVILE1BQU0sQ0FBQyxJQUFNLFFBQVEsR0FBRyxVQUFDLEVBQU07UUFBSixVQUFFO0lBQVEsT0FBQSxLQUFLLENBQUM7UUFDekMsSUFBSSxFQUFFLFlBQVUsRUFBRSxVQUFPO1FBQ3pCLFdBQVcsRUFBRSxpQkFBaUI7UUFDOUIsYUFBYSxFQUFFLG1DQUFtQztLQUNuRCxDQUFDO0FBSm1DLENBSW5DLENBQUMifQ==
// EXTERNAL MODULE: ./constants/api/game.ts
var game = __webpack_require__(50);

// CONCATENATED MODULE: ./action/game.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return loadGameAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getUserGiftAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getTodayGiftAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return playGameAction; });


var loadGameAction = function () {
    return function (dispatch, getState) {
        return dispatch({
            type: game["c" /* LOAD_GAME */],
            payload: { promise: loadGame().then(function (res) { return res; }) },
        });
    };
};
var getUserGiftAction = function () {
    return function (dispatch, getState) {
        return dispatch({
            type: game["b" /* GET_USER_GIFT */],
            payload: { promise: getUserGift().then(function (res) { return res; }) },
        });
    };
};
var getTodayGiftAction = function () {
    return function (dispatch, getState) {
        return dispatch({
            type: game["a" /* GET_TODAY_GIFT */],
            payload: { promise: getTodayGift().then(function (res) { return res; }) },
        });
    };
};
var playGameAction = function (_a) {
    var id = _a.id;
    return function (dispatch, getState) {
        return dispatch({
            type: game["d" /* PLAY_GAME */],
            payload: { promise: playGame({ id: id }).then(function (res) { return res; }) },
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2FtZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImdhbWUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUEsT0FBTyxFQUNMLFFBQVEsRUFDUixXQUFXLEVBQ1gsWUFBWSxFQUNaLFFBQVEsRUFDVCxNQUFNLGFBQWEsQ0FBQztBQUVyQixPQUFPLEVBQ0wsU0FBUyxFQUNULGFBQWEsRUFDYixjQUFjLEVBQ2QsU0FBUyxFQUNWLE1BQU0sdUJBQXVCLENBQUM7QUFFL0IsTUFBTSxDQUFDLElBQU0sY0FBYyxHQUFHO0lBQzVCLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxTQUFTO1lBQ2YsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtTQUNsRCxDQUFDO0lBSEYsQ0FHRTtBQUpKLENBSUksQ0FBQztBQUVQLE1BQU0sQ0FBQyxJQUFNLGlCQUFpQixHQUFHO0lBQy9CLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxhQUFhO1lBQ25CLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7U0FDckQsQ0FBQztJQUhGLENBR0U7QUFKSixDQUlJLENBQUM7QUFFUCxNQUFNLENBQUMsSUFBTSxrQkFBa0IsR0FBRztJQUNoQyxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsY0FBYztZQUNwQixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsWUFBWSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1NBQ3RELENBQUM7SUFIRixDQUdFO0FBSkosQ0FJSSxDQUFDO0FBRVAsTUFBTSxDQUFDLElBQU0sY0FBYyxHQUFHLFVBQUMsRUFBSTtRQUFILFVBQUU7SUFDOUIsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLFNBQVM7WUFDZixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDLEVBQUMsRUFBRSxJQUFBLEVBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtTQUN0RCxDQUFDO0lBSEYsQ0FHRTtBQUpKLENBSUksQ0FBQyJ9

/***/ }),

/***/ 864:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./utils/uri.ts
var uri = __webpack_require__(347);

// EXTERNAL MODULE: ./action/game.ts + 1 modules
var game = __webpack_require__(808);

// EXTERNAL MODULE: ./action/meta.ts
var meta = __webpack_require__(754);

// CONCATENATED MODULE: ./container/game/beauty-hunter/step-2/store.tsx


var mapStateToProps = function (state) { return ({
    gameStore: state.game,
    shopStore: state.shop,
    userStore: state.user,
    likeStore: state.like,
    cartStore: state.cart,
    authStore: state.auth,
    provinceStore: state.province,
    trackingStore: state.tracking,
    listLikedId: state.like.liked.id,
    signInStatus: state.auth.signInStatus
}); };
var mapDispatchToProps = function (dispatch) { return ({
    playGameAction: function (_a) {
        var id = _a.id;
        return dispatch(Object(game["d" /* playGameAction */])({ id: id }));
    },
    updateMetaInfoAction: function (data) { return dispatch(Object(meta["a" /* updateMetaInfoAction */])(data)); },
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLGNBQWMsRUFDZixNQUFNLHlCQUF5QixDQUFDO0FBQ2pDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQy9ELE1BQU0sQ0FBQyxJQUFNLGVBQWUsR0FBRyxVQUFDLEtBQUssSUFBSyxPQUFBLENBQUM7SUFDekMsU0FBUyxFQUFFLEtBQUssQ0FBQyxJQUFJO0lBQ3JCLFNBQVMsRUFBRSxLQUFLLENBQUMsSUFBSTtJQUNyQixTQUFTLEVBQUUsS0FBSyxDQUFDLElBQUk7SUFDckIsU0FBUyxFQUFFLEtBQUssQ0FBQyxJQUFJO0lBQ3JCLFNBQVMsRUFBRSxLQUFLLENBQUMsSUFBSTtJQUNyQixTQUFTLEVBQUUsS0FBSyxDQUFDLElBQUk7SUFDckIsYUFBYSxFQUFFLEtBQUssQ0FBQyxRQUFRO0lBQzdCLGFBQWEsRUFBRSxLQUFLLENBQUMsUUFBUTtJQUM3QixXQUFXLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtJQUNoQyxZQUFZLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZO0NBQ3RDLENBQUMsRUFYd0MsQ0FXeEMsQ0FBQztBQUVILE1BQU0sQ0FBQyxJQUFNLGtCQUFrQixHQUFHLFVBQUMsUUFBUSxJQUFLLE9BQUEsQ0FBQztJQUMvQyxjQUFjLEVBQUUsVUFBQyxFQUFJO1lBQUgsVUFBRTtRQUFNLE9BQUEsUUFBUSxDQUFDLGNBQWMsQ0FBQyxFQUFDLEVBQUUsSUFBQSxFQUFDLENBQUMsQ0FBQztJQUE5QixDQUE4QjtJQUN4RCxvQkFBb0IsRUFBRSxVQUFDLElBQUksSUFBSyxPQUFBLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFwQyxDQUFvQztDQUNyRSxDQUFDLEVBSDhDLENBRzlDLENBQUMifQ==
// CONCATENATED MODULE: ./container/game/beauty-hunter/step-2/initialize.tsx
var DEFAULT_PROPS = {};
var INITIAL_STATE = {
    countDown: 13,
    isShaking: false,
    countShaking: 0,
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRyxFQUFZLENBQUM7QUFDMUMsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHO0lBQ3pCLFNBQVMsRUFBRSxFQUFFO0lBQ2IsU0FBUyxFQUFFLEtBQUs7SUFDaEIsWUFBWSxFQUFFLENBQUM7Q0FDbEIsQ0FBQyJ9
// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./container/game/beauty-hunter/step-2/style.tsx


var pattern = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/pattern.png';
var style_top = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/top.png';
var banner = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/banner.png';
var bgr2 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/bgr-2.png';
var diamond = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/diamond.png';
var s1Phone = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s1-phone.png';
var s1Play = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s1-play.png';
var s2Flora = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s2-flora.png';
var s2Info = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s2-info.png';
var s2Text = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s2-text.png';
var s2Text2 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s2-text-2.png';
var s2Time = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s2-time.png';
var s2p1 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s2-p-1.png';
var s2p2 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s2-p-2.png';
var s2p3 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s2-p-3.png';
var s2p4 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s2-p-4.png';
var s2p5 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s2-p-5.png';
/* harmony default export */ var style = ({
    container: {
        position: 'relative',
        background: '#FFFFFF',
        minHeight: '100vh',
        height: '100vh',
        width: '100vw',
        display: 'flex',
        flexDirection: 'column'
    },
    bg: {
        backgroundImage: "url(" + pattern + ")",
        backgroundPosition: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        backgroundSize: '100px 100px',
        opacity: .05,
        zIndex: -1,
    },
    top: {
        backgroundImage: "url(" + style_top + ")",
        backgroundPosition: 'center',
        width: '100%',
        paddingTop: '33.33%',
        backgroundSize: 'cover'
    },
    content: {
        padding: 30
    },
    banner: {
        backgroundImage: "url(" + banner + ")",
        backgroundPosition: 'center',
        width: '100%',
        paddingTop: '46.666%',
        backgroundSize: 'cover',
        marginBottom: 30
    },
    countdown: {
        margin: '0 auto',
        width: 130,
        height: 30,
        textAlign: 'center',
        lineHeight: '30px',
        background: '#FFF',
        marginBottom: 30,
        fontSize: 24,
        fontFamily: 'arial',
        fontWeight: 'bold',
        color: 'orange',
        border: '1px solid orange',
    },
    button: {
        background: '#ffab00',
        color: '#FFF',
        height: 40,
        lineHeight: '38px',
        fontSize: 18,
        textTransform: 'uppercase',
        textAlign: 'center',
        fontWeight: '800',
        borderRadius: 50,
        border: "2px dotted #f24a00",
        width: 220,
        display: 'block',
        margin: "0 auto 10px"
    },
    info: {
        flex: 1,
        position: 'relative',
        zIndex: 0,
        display: 'flex',
        flexDirection: 'column',
        bgLight: {
            backgroundImage: "url(" + bgr2 + ")",
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            position: 'fixed',
            width: '120vh',
            height: '120vh',
            top: '42%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            marginLeft: '-60vh',
            marginTop: '-60vh',
        },
        bgGradient: {
            width: '100%',
            height: '100%',
            position: 'absolute',
        },
        bgFlora: {
            backgroundImage: "url(" + s2Flora + ")",
            backgroundSize: 'contain',
            width: '100%',
            paddingTop: '44%',
            position: 'relative',
            zIndex: 5,
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'bottom center',
        },
        countdown: {
            backgroundImage: "url(" + s2Time + ")",
            backgroundSize: 'contain',
            width: 200,
            height: 70,
            position: 'absolute',
            zIndex: 10,
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'bottom center',
            top: 0,
            left: '50%',
            marginLeft: -100,
            textAlign: 'center',
            color: '#FFF',
            lineHeight: '80px',
            fontSize: '29px',
            fontWeight: '800',
            textShadow: '1px 1px 0px rgba(0,0,0,.25), 1px 1px 2px rgba(0,0,0,.25)'
        },
        content: {
            width: '100vw',
            flex: 1,
            position: 'relative',
            zIndex: 10,
            display: 'flex',
            justifyContent: 'center',
            alignitems: 'center'
        },
        contentInfo: {
            backgroundImage: "url(" + s2Info + ")",
            width: '100%',
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        },
        contentInfoPanel: {
            width: '100%',
            paddingTop: '90%',
            position: 'relative',
            top: 70,
        },
        phone: {
            backgroundImage: "url(" + s1Phone + ")",
            width: 110,
            height: 110,
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            position: 'absolute',
            top: '34%',
            left: '50%',
            marginLeft: -55,
            marginTop: -55
        },
        slogan: {
            backgroundImage: "url(" + s2Text + ")",
            width: 200,
            height: 110,
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            position: 'absolute',
            top: '89%',
            left: '50%',
            marginLeft: -100,
            marginTop: -55
        },
        emptySlogan: {
            backgroundImage: "url(" + s2Text2 + ")",
            width: 200,
            height: 110,
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            position: 'absolute',
            top: '95%',
            left: '50%',
            marginLeft: -100,
            marginTop: -55
        },
        pig: {
            width: 300,
            height: 300,
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)'
        },
        pig1: {
            backgroundImage: "url(" + s2p1 + ")",
        },
        pig2: {
            backgroundImage: "url(" + s2p2 + ")",
        },
        pig3: {
            backgroundImage: "url(" + s2p3 + ")",
        },
        pig4: {
            backgroundImage: "url(" + s2p4 + ")",
        },
        pig5: {
            backgroundImage: "url(" + s2p5 + ")",
        },
        playOverlay: {
            position: 'fixed',
            background: 'rgba(0,0,0,.9)',
            width: '100%',
            height: '100%',
            zIndex: variable["zIndexMax"],
            top: 0,
            left: 0,
        },
        play: {
            backgroundImage: "url(" + s1Play + ")",
            width: 150,
            height: 50,
            fontSize: 32,
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            position: 'absolute',
            bottom: '50%',
            left: '50%',
            zIndex: variable["zIndexMax"],
            marginLeft: -75,
            marginTop: -55,
            textAlign: 'center',
            color: '#FFF',
            lineHeight: '50px',
            textTransform: 'uppercase',
            fontWeight: 'bolder',
            letterSpacing: '1px',
            textShadow: '1px 1px 0px rgba(0,0,0,0.75), 1px 1px 1px rgba(0,0,0,0.15)',
        },
        playAgain: {
            backgroundImage: "url(" + s1Play + ")",
            width: 150,
            height: 50,
            fontSize: 15,
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            position: 'absolute',
            bottom: '-24%',
            left: '50%',
            zIndex: variable["zIndexMax"],
            marginLeft: -75,
            marginTop: -55,
            textAlign: 'center',
            color: '#FFF',
            lineHeight: '50px',
            textTransform: 'uppercase',
            fontWeight: 'bolder',
            letterSpacing: '1px',
            textShadow: '1px 1px 0px rgba(0,0,0,0.75), 1px 1px 1px rgba(0,0,0,0.15)',
        },
        diamond: {
            position: 'absolute',
            backgroundImage: "url(" + diamond + ")",
            width: 50,
            height: 50,
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            zIndex: 10,
        },
        diamond1: {
            top: '14%',
            right: '13%',
            transform: 'scale(.8)'
        },
        diamond2: {
            top: '16%',
            left: '14%',
            transform: 'scale(.8) rotate(-45deg)',
        },
        diamond3: {
            bottom: '10%',
            right: '61%',
            transform: 'scale(1) rotate(-45deg)',
        },
        diamond4: {
            top: '50%',
            left: '10%',
            transform: 'scale(1.2)',
        },
        diamond5: {
            right: '10%',
            bottom: '24%',
            transform: 'scale(1.3) rotate(10deg)'
        },
    },
    gift: {
        background: '#FFF',
        height: 90,
        position: 'relative',
        zIndex: 5,
        tab: {
            position: 'absolute',
            top: -40,
            left: 0,
            width: '100%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            paddingLeft: 10,
            paddingRight: 10,
        },
        tabItem: {
            height: 40,
            display: 'flex',
            flex: 1,
            marginLeft: 2,
            marginRight: 2,
            justifyContent: 'center',
            alignItems: 'center',
            border: "2px solid #e7c1d1",
            borderRadius: '6px 6px 0 0',
            background: '#f1d5e1',
            position: 'relative',
            zIndex: 1,
            boxShadow: '1px 1px 0 .5px rgba(255,255,255,.5) inset'
        },
        tabActive: {
            boxShadow: 'none',
            background: '#FFFFFF',
            borderBottom: "none",
        },
        tabImage: {
            height: 20,
            width: 'auto',
        },
        tabLine: {
            zIndex: 0,
            background: '#e7c1d1',
            left: 0,
            bottom: 0,
            position: 'absolute',
            width: '100%',
            height: 2,
        },
        list: {
            width: '100%',
            overflowX: 'scroll',
            overflowY: 'hidden',
        },
        listPanel: {
            whiteSpace: 'nowrap',
            padding: 10,
        },
        listItem: {
            display: 'inline-block',
            width: 50,
            height: 50,
            margin: 10,
            boxShadow: '1px 1px 0 .5px inset #ffffff, 0 0 0px 2px #f1d5e1, 1px 1px 0 2px #e7c1d1',
            borderRadius: 4,
            background: '#f7e6ed',
        },
        lastItem: {
            marginRight: 20
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSw0QkFBNEIsQ0FBQztBQUd2RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUUxRCxJQUFNLE9BQU8sR0FBRyxpQkFBaUIsR0FBRyxzREFBc0QsQ0FBQztBQUMzRixJQUFNLEdBQUcsR0FBRyxpQkFBaUIsR0FBRyxrREFBa0QsQ0FBQztBQUNuRixJQUFNLE1BQU0sR0FBRyxpQkFBaUIsR0FBRyxxREFBcUQsQ0FBQztBQUV6RixJQUFNLElBQUksR0FBRyxpQkFBaUIsR0FBRyxvREFBb0QsQ0FBQztBQUN0RixJQUFNLE9BQU8sR0FBRyxpQkFBaUIsR0FBRyxzREFBc0QsQ0FBQztBQUMzRixJQUFNLE9BQU8sR0FBRyxpQkFBaUIsR0FBRyx1REFBdUQsQ0FBQztBQUM1RixJQUFNLE1BQU0sR0FBRyxpQkFBaUIsR0FBRyxzREFBc0QsQ0FBQztBQUMxRixJQUFNLE9BQU8sR0FBRyxpQkFBaUIsR0FBRyx1REFBdUQsQ0FBQztBQUM1RixJQUFNLE1BQU0sR0FBRyxpQkFBaUIsR0FBRyxzREFBc0QsQ0FBQztBQUMxRixJQUFNLE1BQU0sR0FBRyxpQkFBaUIsR0FBRyxzREFBc0QsQ0FBQztBQUMxRixJQUFNLE9BQU8sR0FBRyxpQkFBaUIsR0FBRyx3REFBd0QsQ0FBQztBQUM3RixJQUFNLE1BQU0sR0FBRyxpQkFBaUIsR0FBRyxzREFBc0QsQ0FBQztBQUMxRixJQUFNLElBQUksR0FBRyxpQkFBaUIsR0FBRyxxREFBcUQsQ0FBQztBQUN2RixJQUFNLElBQUksR0FBRyxpQkFBaUIsR0FBRyxxREFBcUQsQ0FBQztBQUN2RixJQUFNLElBQUksR0FBRyxpQkFBaUIsR0FBRyxxREFBcUQsQ0FBQztBQUN2RixJQUFNLElBQUksR0FBRyxpQkFBaUIsR0FBRyxxREFBcUQsQ0FBQztBQUN2RixJQUFNLElBQUksR0FBRyxpQkFBaUIsR0FBRyxxREFBcUQsQ0FBQztBQUV2RixlQUFlO0lBQ2IsU0FBUyxFQUFFO1FBQ1QsUUFBUSxFQUFFLFVBQVU7UUFDcEIsVUFBVSxFQUFFLFNBQVM7UUFDckIsU0FBUyxFQUFFLE9BQU87UUFDbEIsTUFBTSxFQUFFLE9BQU87UUFDZixLQUFLLEVBQUUsT0FBTztRQUNkLE9BQU8sRUFBRSxNQUFNO1FBQ2YsYUFBYSxFQUFFLFFBQVE7S0FDeEI7SUFFRCxFQUFFLEVBQUU7UUFDRixlQUFlLEVBQUUsU0FBTyxPQUFPLE1BQUc7UUFDbEMsa0JBQWtCLEVBQUUsUUFBUTtRQUM1QixRQUFRLEVBQUUsVUFBVTtRQUNwQixHQUFHLEVBQUUsQ0FBQztRQUNOLElBQUksRUFBRSxDQUFDO1FBQ1AsS0FBSyxFQUFFLE1BQU07UUFDYixNQUFNLEVBQUUsTUFBTTtRQUNkLGNBQWMsRUFBRSxhQUFhO1FBQzdCLE9BQU8sRUFBRSxHQUFHO1FBQ1osTUFBTSxFQUFFLENBQUMsQ0FBQztLQUNYO0lBRUQsR0FBRyxFQUFFO1FBQ0gsZUFBZSxFQUFFLFNBQU8sR0FBRyxNQUFHO1FBQzlCLGtCQUFrQixFQUFFLFFBQVE7UUFDNUIsS0FBSyxFQUFFLE1BQU07UUFDYixVQUFVLEVBQUUsUUFBUTtRQUNwQixjQUFjLEVBQUUsT0FBTztLQUN4QjtJQUVELE9BQU8sRUFBRTtRQUNMLE9BQU8sRUFBRSxFQUFFO0tBQ2Q7SUFFRCxNQUFNLEVBQUU7UUFDTixlQUFlLEVBQUUsU0FBTyxNQUFNLE1BQUc7UUFDakMsa0JBQWtCLEVBQUUsUUFBUTtRQUM1QixLQUFLLEVBQUUsTUFBTTtRQUNiLFVBQVUsRUFBRSxTQUFTO1FBQ3JCLGNBQWMsRUFBRSxPQUFPO1FBQ3ZCLFlBQVksRUFBRSxFQUFFO0tBQ2pCO0lBRUQsU0FBUyxFQUFFO1FBQ1QsTUFBTSxFQUFFLFFBQVE7UUFDaEIsS0FBSyxFQUFFLEdBQUc7UUFDVixNQUFNLEVBQUUsRUFBRTtRQUNWLFNBQVMsRUFBRSxRQUFRO1FBQ25CLFVBQVUsRUFBRSxNQUFNO1FBQ2xCLFVBQVUsRUFBRSxNQUFNO1FBQ2xCLFlBQVksRUFBRSxFQUFFO1FBQ2hCLFFBQVEsRUFBRSxFQUFFO1FBQ1osVUFBVSxFQUFFLE9BQU87UUFDbkIsVUFBVSxFQUFFLE1BQU07UUFDbEIsS0FBSyxFQUFFLFFBQVE7UUFDZixNQUFNLEVBQUUsa0JBQWtCO0tBQzNCO0lBRUQsTUFBTSxFQUFFO1FBQ04sVUFBVSxFQUFFLFNBQVM7UUFDckIsS0FBSyxFQUFFLE1BQU07UUFDYixNQUFNLEVBQUUsRUFBRTtRQUNWLFVBQVUsRUFBRSxNQUFNO1FBQ2xCLFFBQVEsRUFBRSxFQUFFO1FBQ1osYUFBYSxFQUFFLFdBQVc7UUFDMUIsU0FBUyxFQUFFLFFBQVE7UUFDbkIsVUFBVSxFQUFFLEtBQUs7UUFDakIsWUFBWSxFQUFFLEVBQUU7UUFDaEIsTUFBTSxFQUFFLG9CQUFvQjtRQUM1QixLQUFLLEVBQUUsR0FBRztRQUNWLE9BQU8sRUFBRSxPQUFPO1FBQ2hCLE1BQU0sRUFBRSxhQUFhO0tBQ3RCO0lBRUQsSUFBSSxFQUFFO1FBQ0osSUFBSSxFQUFFLENBQUM7UUFDUCxRQUFRLEVBQUUsVUFBVTtRQUNwQixNQUFNLEVBQUUsQ0FBQztRQUNULE9BQU8sRUFBRSxNQUFNO1FBQ2YsYUFBYSxFQUFFLFFBQVE7UUFFdkIsT0FBTyxFQUFFO1lBQ1AsZUFBZSxFQUFFLFNBQU8sSUFBSSxNQUFHO1lBQy9CLGtCQUFrQixFQUFFLFFBQVE7WUFDNUIsY0FBYyxFQUFFLE9BQU87WUFDdkIsUUFBUSxFQUFFLE9BQU87WUFDakIsS0FBSyxFQUFFLE9BQU87WUFDZCxNQUFNLEVBQUUsT0FBTztZQUNmLEdBQUcsRUFBRSxLQUFLO1lBQ1YsSUFBSSxFQUFFLEtBQUs7WUFDWCxTQUFTLEVBQUUsdUJBQXVCO1lBQ2xDLFVBQVUsRUFBRSxPQUFPO1lBQ25CLFNBQVMsRUFBRSxPQUFPO1NBQ25CO1FBRUQsVUFBVSxFQUFFO1lBQ1YsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsTUFBTTtZQUNkLFFBQVEsRUFBRSxVQUFVO1NBQ3JCO1FBRUQsT0FBTyxFQUFFO1lBQ1AsZUFBZSxFQUFFLFNBQU8sT0FBTyxNQUFHO1lBQ2xDLGNBQWMsRUFBRSxTQUFTO1lBQ3pCLEtBQUssRUFBRSxNQUFNO1lBQ2IsVUFBVSxFQUFFLEtBQUs7WUFDakIsUUFBUSxFQUFFLFVBQVU7WUFDcEIsTUFBTSxFQUFFLENBQUM7WUFDVCxnQkFBZ0IsRUFBRSxXQUFXO1lBQzdCLGtCQUFrQixFQUFFLGVBQWU7U0FDcEM7UUFFRCxTQUFTLEVBQUU7WUFDVCxlQUFlLEVBQUUsU0FBTyxNQUFNLE1BQUc7WUFDakMsY0FBYyxFQUFFLFNBQVM7WUFDekIsS0FBSyxFQUFFLEdBQUc7WUFDVixNQUFNLEVBQUUsRUFBRTtZQUNWLFFBQVEsRUFBRSxVQUFVO1lBQ3BCLE1BQU0sRUFBRSxFQUFFO1lBQ1YsZ0JBQWdCLEVBQUUsV0FBVztZQUM3QixrQkFBa0IsRUFBRSxlQUFlO1lBQ25DLEdBQUcsRUFBRSxDQUFDO1lBQ04sSUFBSSxFQUFFLEtBQUs7WUFDWCxVQUFVLEVBQUUsQ0FBQyxHQUFHO1lBQ2hCLFNBQVMsRUFBRSxRQUFRO1lBQ25CLEtBQUssRUFBRSxNQUFNO1lBQ2IsVUFBVSxFQUFFLE1BQU07WUFDbEIsUUFBUSxFQUFFLE1BQU07WUFDaEIsVUFBVSxFQUFFLEtBQUs7WUFDakIsVUFBVSxFQUFFLDBEQUEwRDtTQUN2RTtRQUVELE9BQU8sRUFBRTtZQUNQLEtBQUssRUFBRSxPQUFPO1lBQ2QsSUFBSSxFQUFFLENBQUM7WUFDUCxRQUFRLEVBQUUsVUFBVTtZQUNwQixNQUFNLEVBQUUsRUFBRTtZQUNWLE9BQU8sRUFBRSxNQUFNO1lBQ2YsY0FBYyxFQUFFLFFBQVE7WUFDeEIsVUFBVSxFQUFFLFFBQVE7U0FDckI7UUFFRCxXQUFXLEVBQUU7WUFDWCxlQUFlLEVBQUUsU0FBTyxNQUFNLE1BQUc7WUFDakMsS0FBSyxFQUFFLE1BQU07WUFDYixjQUFjLEVBQUUsU0FBUztZQUN6QixnQkFBZ0IsRUFBRSxXQUFXO1lBQzdCLGtCQUFrQixFQUFFLFFBQVE7WUFDNUIsT0FBTyxFQUFFLE1BQU07WUFDZixjQUFjLEVBQUUsUUFBUTtZQUN4QixVQUFVLEVBQUUsUUFBUTtTQUNyQjtRQUVELGdCQUFnQixFQUFFO1lBQ2hCLEtBQUssRUFBRSxNQUFNO1lBQ2IsVUFBVSxFQUFFLEtBQUs7WUFDakIsUUFBUSxFQUFFLFVBQVU7WUFDcEIsR0FBRyxFQUFFLEVBQUU7U0FDUjtRQUVELEtBQUssRUFBRTtZQUNMLGVBQWUsRUFBRSxTQUFPLE9BQU8sTUFBRztZQUNsQyxLQUFLLEVBQUUsR0FBRztZQUNWLE1BQU0sRUFBRSxHQUFHO1lBQ1gsY0FBYyxFQUFFLFNBQVM7WUFDekIsZ0JBQWdCLEVBQUUsV0FBVztZQUM3QixrQkFBa0IsRUFBRSxRQUFRO1lBQzVCLFFBQVEsRUFBRSxVQUFVO1lBQ3BCLEdBQUcsRUFBRSxLQUFLO1lBQ1YsSUFBSSxFQUFFLEtBQUs7WUFDWCxVQUFVLEVBQUUsQ0FBQyxFQUFFO1lBQ2YsU0FBUyxFQUFFLENBQUMsRUFBRTtTQUNmO1FBRUQsTUFBTSxFQUFFO1lBQ04sZUFBZSxFQUFFLFNBQU8sTUFBTSxNQUFHO1lBQ2pDLEtBQUssRUFBRSxHQUFHO1lBQ1YsTUFBTSxFQUFFLEdBQUc7WUFDWCxjQUFjLEVBQUUsU0FBUztZQUN6QixnQkFBZ0IsRUFBRSxXQUFXO1lBQzdCLGtCQUFrQixFQUFFLFFBQVE7WUFDNUIsUUFBUSxFQUFFLFVBQVU7WUFDcEIsR0FBRyxFQUFFLEtBQUs7WUFDVixJQUFJLEVBQUUsS0FBSztZQUNYLFVBQVUsRUFBRSxDQUFDLEdBQUc7WUFDaEIsU0FBUyxFQUFFLENBQUMsRUFBRTtTQUNmO1FBRUQsV0FBVyxFQUFFO1lBQ1gsZUFBZSxFQUFFLFNBQU8sT0FBTyxNQUFHO1lBQ2xDLEtBQUssRUFBRSxHQUFHO1lBQ1YsTUFBTSxFQUFFLEdBQUc7WUFDWCxjQUFjLEVBQUUsU0FBUztZQUN6QixnQkFBZ0IsRUFBRSxXQUFXO1lBQzdCLGtCQUFrQixFQUFFLFFBQVE7WUFDNUIsUUFBUSxFQUFFLFVBQVU7WUFDcEIsR0FBRyxFQUFFLEtBQUs7WUFDVixJQUFJLEVBQUUsS0FBSztZQUNYLFVBQVUsRUFBRSxDQUFDLEdBQUc7WUFDaEIsU0FBUyxFQUFFLENBQUMsRUFBRTtTQUNmO1FBRUQsR0FBRyxFQUFFO1lBQ0gsS0FBSyxFQUFFLEdBQUc7WUFDVixNQUFNLEVBQUUsR0FBRztZQUNYLGNBQWMsRUFBRSxTQUFTO1lBQ3pCLGdCQUFnQixFQUFFLFdBQVc7WUFDN0Isa0JBQWtCLEVBQUUsUUFBUTtZQUM1QixRQUFRLEVBQUUsVUFBVTtZQUNwQixHQUFHLEVBQUUsS0FBSztZQUNWLElBQUksRUFBRSxLQUFLO1lBQ1gsU0FBUyxFQUFFLHVCQUF1QjtTQUNuQztRQUVELElBQUksRUFBRTtZQUNKLGVBQWUsRUFBRSxTQUFPLElBQUksTUFBRztTQUNoQztRQUVELElBQUksRUFBRTtZQUNKLGVBQWUsRUFBRSxTQUFPLElBQUksTUFBRztTQUNoQztRQUVELElBQUksRUFBRTtZQUNKLGVBQWUsRUFBRSxTQUFPLElBQUksTUFBRztTQUNoQztRQUVELElBQUksRUFBRTtZQUNKLGVBQWUsRUFBRSxTQUFPLElBQUksTUFBRztTQUNoQztRQUVELElBQUksRUFBRTtZQUNKLGVBQWUsRUFBRSxTQUFPLElBQUksTUFBRztTQUNoQztRQUVELFdBQVcsRUFBRTtZQUNYLFFBQVEsRUFBRSxPQUFPO1lBQ2pCLFVBQVUsRUFBRSxnQkFBZ0I7WUFDNUIsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsTUFBTTtZQUNkLE1BQU0sRUFBRSxRQUFRLENBQUMsU0FBUztZQUMxQixHQUFHLEVBQUUsQ0FBQztZQUNOLElBQUksRUFBRSxDQUFDO1NBQ1I7UUFFRCxJQUFJLEVBQUU7WUFDSixlQUFlLEVBQUUsU0FBTyxNQUFNLE1BQUc7WUFDakMsS0FBSyxFQUFFLEdBQUc7WUFDVixNQUFNLEVBQUUsRUFBRTtZQUNWLFFBQVEsRUFBRSxFQUFFO1lBQ1osY0FBYyxFQUFFLFNBQVM7WUFDekIsZ0JBQWdCLEVBQUUsV0FBVztZQUM3QixrQkFBa0IsRUFBRSxRQUFRO1lBQzVCLFFBQVEsRUFBRSxVQUFVO1lBQ3BCLE1BQU0sRUFBRSxLQUFLO1lBQ2IsSUFBSSxFQUFFLEtBQUs7WUFDWCxNQUFNLEVBQUUsUUFBUSxDQUFDLFNBQVM7WUFDMUIsVUFBVSxFQUFFLENBQUMsRUFBRTtZQUNmLFNBQVMsRUFBRSxDQUFDLEVBQUU7WUFDZCxTQUFTLEVBQUUsUUFBUTtZQUNuQixLQUFLLEVBQUUsTUFBTTtZQUNiLFVBQVUsRUFBRSxNQUFNO1lBQ2xCLGFBQWEsRUFBRSxXQUFXO1lBQzFCLFVBQVUsRUFBRSxRQUFRO1lBQ3BCLGFBQWEsRUFBRSxLQUFLO1lBQ3BCLFVBQVUsRUFBRSw0REFBNEQ7U0FDekU7UUFFRCxTQUFTLEVBQUU7WUFDVCxlQUFlLEVBQUUsU0FBTyxNQUFNLE1BQUc7WUFDakMsS0FBSyxFQUFFLEdBQUc7WUFDVixNQUFNLEVBQUUsRUFBRTtZQUNWLFFBQVEsRUFBRSxFQUFFO1lBQ1osY0FBYyxFQUFFLFNBQVM7WUFDekIsZ0JBQWdCLEVBQUUsV0FBVztZQUM3QixrQkFBa0IsRUFBRSxRQUFRO1lBQzVCLFFBQVEsRUFBRSxVQUFVO1lBQ3BCLE1BQU0sRUFBRSxNQUFNO1lBQ2QsSUFBSSxFQUFFLEtBQUs7WUFDWCxNQUFNLEVBQUUsUUFBUSxDQUFDLFNBQVM7WUFDMUIsVUFBVSxFQUFFLENBQUMsRUFBRTtZQUNmLFNBQVMsRUFBRSxDQUFDLEVBQUU7WUFDZCxTQUFTLEVBQUUsUUFBUTtZQUNuQixLQUFLLEVBQUUsTUFBTTtZQUNiLFVBQVUsRUFBRSxNQUFNO1lBQ2xCLGFBQWEsRUFBRSxXQUFXO1lBQzFCLFVBQVUsRUFBRSxRQUFRO1lBQ3BCLGFBQWEsRUFBRSxLQUFLO1lBQ3BCLFVBQVUsRUFBRSw0REFBNEQ7U0FDekU7UUFFRCxPQUFPLEVBQUU7WUFDUCxRQUFRLEVBQUUsVUFBVTtZQUNwQixlQUFlLEVBQUUsU0FBTyxPQUFPLE1BQUc7WUFDbEMsS0FBSyxFQUFFLEVBQUU7WUFDVCxNQUFNLEVBQUUsRUFBRTtZQUNWLGNBQWMsRUFBRSxTQUFTO1lBQ3pCLGdCQUFnQixFQUFFLFdBQVc7WUFDN0Isa0JBQWtCLEVBQUUsUUFBUTtZQUM1QixNQUFNLEVBQUUsRUFBRTtTQUNYO1FBQ0QsUUFBUSxFQUFFO1lBQ1IsR0FBRyxFQUFFLEtBQUs7WUFDVixLQUFLLEVBQUUsS0FBSztZQUNaLFNBQVMsRUFBRSxXQUFXO1NBQ3ZCO1FBQ0QsUUFBUSxFQUFFO1lBQ1IsR0FBRyxFQUFFLEtBQUs7WUFDVixJQUFJLEVBQUUsS0FBSztZQUNYLFNBQVMsRUFBRSwwQkFBMEI7U0FDdEM7UUFDRCxRQUFRLEVBQUU7WUFDUixNQUFNLEVBQUUsS0FBSztZQUNiLEtBQUssRUFBRSxLQUFLO1lBQ1osU0FBUyxFQUFFLHlCQUF5QjtTQUNyQztRQUNELFFBQVEsRUFBRTtZQUNSLEdBQUcsRUFBRSxLQUFLO1lBQ1YsSUFBSSxFQUFFLEtBQUs7WUFDWCxTQUFTLEVBQUUsWUFBWTtTQUN4QjtRQUNELFFBQVEsRUFBRTtZQUNSLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLEtBQUs7WUFDYixTQUFTLEVBQUUsMEJBQTBCO1NBQ3RDO0tBQ0Y7SUFFRCxJQUFJLEVBQUU7UUFDSixVQUFVLEVBQUUsTUFBTTtRQUNsQixNQUFNLEVBQUUsRUFBRTtRQUNWLFFBQVEsRUFBRSxVQUFVO1FBQ3BCLE1BQU0sRUFBRSxDQUFDO1FBRVQsR0FBRyxFQUFFO1lBQ0gsUUFBUSxFQUFFLFVBQVU7WUFDcEIsR0FBRyxFQUFFLENBQUMsRUFBRTtZQUNSLElBQUksRUFBRSxDQUFDO1lBQ1AsS0FBSyxFQUFFLE1BQU07WUFDYixPQUFPLEVBQUUsTUFBTTtZQUNmLGNBQWMsRUFBRSxRQUFRO1lBQ3hCLFVBQVUsRUFBRSxRQUFRO1lBQ3BCLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLEVBQUU7U0FDakI7UUFFRCxPQUFPLEVBQUU7WUFDUCxNQUFNLEVBQUUsRUFBRTtZQUNWLE9BQU8sRUFBRSxNQUFNO1lBQ2YsSUFBSSxFQUFFLENBQUM7WUFDUCxVQUFVLEVBQUUsQ0FBQztZQUNiLFdBQVcsRUFBRSxDQUFDO1lBQ2QsY0FBYyxFQUFFLFFBQVE7WUFDeEIsVUFBVSxFQUFFLFFBQVE7WUFDcEIsTUFBTSxFQUFFLG1CQUFtQjtZQUMzQixZQUFZLEVBQUUsYUFBYTtZQUMzQixVQUFVLEVBQUUsU0FBUztZQUNyQixRQUFRLEVBQUUsVUFBVTtZQUNwQixNQUFNLEVBQUUsQ0FBQztZQUNULFNBQVMsRUFBRSwyQ0FBMkM7U0FDdkQ7UUFFRCxTQUFTLEVBQUU7WUFDVCxTQUFTLEVBQUUsTUFBTTtZQUNqQixVQUFVLEVBQUUsU0FBUztZQUNyQixZQUFZLEVBQUUsTUFBTTtTQUNyQjtRQUVELFFBQVEsRUFBRTtZQUNSLE1BQU0sRUFBRSxFQUFFO1lBQ1YsS0FBSyxFQUFFLE1BQU07U0FDZDtRQUVELE9BQU8sRUFBRTtZQUNQLE1BQU0sRUFBRSxDQUFDO1lBQ1QsVUFBVSxFQUFFLFNBQVM7WUFDckIsSUFBSSxFQUFFLENBQUM7WUFDUCxNQUFNLEVBQUUsQ0FBQztZQUNULFFBQVEsRUFBRSxVQUFVO1lBQ3BCLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLENBQUM7U0FDVjtRQUVELElBQUksRUFBRTtZQUNKLEtBQUssRUFBRSxNQUFNO1lBQ2IsU0FBUyxFQUFFLFFBQVE7WUFDbkIsU0FBUyxFQUFFLFFBQVE7U0FDcEI7UUFDRCxTQUFTLEVBQUU7WUFDVCxVQUFVLEVBQUUsUUFBUTtZQUNwQixPQUFPLEVBQUUsRUFBRTtTQUNaO1FBQ0QsUUFBUSxFQUFFO1lBQ1IsT0FBTyxFQUFFLGNBQWM7WUFDdkIsS0FBSyxFQUFFLEVBQUU7WUFDVCxNQUFNLEVBQUUsRUFBRTtZQUNWLE1BQU0sRUFBRSxFQUFFO1lBQ1YsU0FBUyxFQUFFLDBFQUEwRTtZQUNyRixZQUFZLEVBQUUsQ0FBQztZQUNmLFVBQVUsRUFBRSxTQUFTO1NBQ3RCO1FBQ0QsUUFBUSxFQUFFO1lBQ1IsV0FBVyxFQUFFLEVBQUU7U0FDaEI7S0FDRjtDQUNNLENBQUMifQ==
// CONCATENATED MODULE: ./container/game/beauty-hunter/step-2/view.tsx


var item = function () { return (react["createElement"]("div", { style: { background: '#FFF', width: '100%', padding: 10, borderBottom: '1px solid #eee', display: 'flex', justifyContent: 'center', alignItems: 'center' } },
    react["createElement"]("img", { style: { width: 60 }, src: "https://upload.lixibox.com/system/pictures/files/000/026/092/medium/1523329786.jpg" }),
    react["createElement"]("span", { style: { fontSize: 14 } }, "Lixibox Bamboo Charcoal Oil Control Paper"))); };
var renderView = function (_a) {
    var props = _a.props, state = _a.state, reShake = _a.reShake;
    var countDown = state.countDown, isShaking = state.isShaking;
    return (react["createElement"]("div", { style: style.container },
        react["createElement"]("link", { rel: 'stylesheet', type: 'text/css', href: 'http://lxbtest.tk/assets/css/game.css' }),
        react["createElement"]("div", { style: style.info },
            react["createElement"]("div", { className: 'bgrgame', style: style.info.bgLight }),
            react["createElement"]("div", { className: 'gamegrd', style: style.info.bgGradient }),
            react["createElement"]("div", { style: style.info.content },
                react["createElement"]("div", { style: style.info.contentInfo },
                    react["createElement"]("div", { style: style.info.contentInfoPanel },
                        react["createElement"]("div", { style: [style.info.pig, style.info.pig5, { visibility: countDown < 14 && countDown > 10 ? 'visible' : 'hidden' }] }),
                        react["createElement"]("div", { style: [style.info.slogan, { visibility: countDown < 14 && countDown > 10 ? 'visible' : 'hidden' }] }),
                        react["createElement"]("div", { className: 'pig1', style: [style.info.pig, style.info.pig1, { visibility: countDown <= 10 && countDown > 0 && isShaking ? 'visible' : 'hidden' }] }),
                        react["createElement"]("div", { className: 'pig2', style: [style.info.pig, style.info.pig2, { visibility: countDown <= 10 && countDown > 0 && isShaking ? 'visible' : 'hidden' }] }),
                        react["createElement"]("div", { className: 'pig3', style: [style.info.pig, style.info.pig3, { visibility: countDown <= 10 && countDown > 0 && isShaking ? 'visible' : 'hidden' }] }),
                        react["createElement"]("div", { className: 'play', style: [style.info.slogan, { visibility: countDown <= 10 && countDown > 0 && isShaking ? 'visible' : 'hidden' }] }),
                        react["createElement"]("div", { style: [style.info.pig, style.info.pig5, { visibility: countDown <= 10 && countDown > 0 && !isShaking ? 'visible' : 'hidden' }] }),
                        react["createElement"]("div", { className: 'play', style: [style.info.slogan, { visibility: countDown <= 10 && countDown > 0 ? 'visible' : 'hidden' }] }),
                        react["createElement"]("div", { className: 'pig', style: [style.info.pig, style.info.pig4, { visibility: countDown === 14 || countDown <= 0 ? 'visible' : 'hidden' }] }),
                        react["createElement"]("div", { className: '', style: [style.info.emptySlogan, { visibility: countDown === 14 ? 'visible' : 'hidden' }] }),
                        react["createElement"]("div", { onClick: function () { return reShake(); }, className: 'play', style: [style.info.playAgain, { visibility: countDown === 14 ? 'visible' : 'hidden' }] }, "L\u1EAFc l\u1EA1i nh\u00E9")))),
            react["createElement"]("div", { style: style.info.bgFlora }, countDown <= 10 && countDown > 0
                &&
                    (react["createElement"]("div", { style: style.info.countdown }, countDown))),
            react["createElement"]("div", { style: { width: 0, height: 0, opacity: 0, visibility: 'hidden', overflow: 'hidden' } }),
            react["createElement"]("div", { className: countDown <= 10 && countDown > 0 && isShaking ? 'diamond-shake1' : '', style: [style.info.diamond, style.info.diamond1] }),
            react["createElement"]("div", { className: countDown <= 10 && countDown > 0 && isShaking ? 'diamond-shake2' : '', style: [style.info.diamond, style.info.diamond2] }),
            react["createElement"]("div", { className: countDown <= 10 && countDown > 0 && isShaking ? 'diamond-shake3' : '', style: [style.info.diamond, style.info.diamond3] }),
            react["createElement"]("div", { className: countDown <= 10 && countDown > 0 && isShaking ? 'diamond-shake4' : '', style: [style.info.diamond, style.info.diamond4] }),
            react["createElement"]("div", { className: countDown <= 10 && countDown > 0 && isShaking ? 'diamond-shake5' : '', style: [style.info.diamond, style.info.diamond5] }),
            (countDown < 14 && countDown > 10)
                && react["createElement"]("div", { style: style.info.playOverlay }, countDown - 10),
            countDown < 14 && countDown > 10
                && react["createElement"]("div", { className: 'play', style: style.info.play }, countDown - 10))));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRy9CLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUc1QixJQUFNLElBQUksR0FBRyxjQUFNLE9BQUEsQ0FDakIsNkJBQUssS0FBSyxFQUFFLEVBQUssVUFBVSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsWUFBWSxFQUFFLGdCQUFnQixFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsY0FBYyxFQUFFLFFBQVEsRUFBQyxVQUFVLEVBQUUsUUFBUSxFQUFDO0lBQzlKLDZCQUFLLEtBQUssRUFBRSxFQUFDLEtBQUssRUFBRSxFQUFFLEVBQUMsRUFBRSxHQUFHLEVBQUMsb0ZBQW9GLEdBQUc7SUFDcEgsOEJBQU0sS0FBSyxFQUFFLEVBQUMsUUFBUSxFQUFFLEVBQUUsRUFBQyxnREFBa0QsQ0FDekUsQ0FDUCxFQUxrQixDQUtsQixDQUFBO0FBRUQsSUFBTSxVQUFVLEdBQUcsVUFBQyxFQUluQjtRQUhDLGdCQUFLLEVBQ0wsZ0JBQUssRUFDTCxvQkFBTztJQUVDLElBQUEsMkJBQVMsRUFBRSwyQkFBUyxDQUFXO0lBQ3ZDLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUztRQUN6Qiw4QkFBTSxHQUFHLEVBQUMsWUFBWSxFQUFDLElBQUksRUFBQyxVQUFVLEVBQUMsSUFBSSxFQUFDLDZDQUE2QyxHQUFHO1FBQzVGLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSTtZQUNwQiw2QkFBSyxTQUFTLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBUTtZQUM1RCw2QkFBSyxTQUFTLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBUTtZQUMvRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPO2dCQUM1Qiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXO29CQUNoQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxnQkFBZ0I7d0JBQ3JDLDZCQUFLLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEVBQUMsVUFBVSxFQUFFLFNBQVMsR0FBRyxFQUFFLElBQUksU0FBUyxHQUFHLEVBQUUsQ0FBRSxDQUFDLENBQUMsU0FBUyxDQUFBLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxHQUFRO3dCQUM3SCw2QkFBSyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFFLFVBQVUsRUFBRSxTQUFTLEdBQUcsRUFBRSxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQSxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsR0FBUTt3QkFFaEgsNkJBQUssU0FBUyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxFQUFDLFVBQVUsRUFBRSxTQUFTLElBQUksRUFBRSxJQUFJLFNBQVMsR0FBRyxDQUFDLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUEsQ0FBQyxDQUFDLFFBQVEsRUFBQyxDQUFDLEdBQVE7d0JBQzNKLDZCQUFLLFNBQVMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsRUFBQyxVQUFVLEVBQUUsU0FBUyxJQUFJLEVBQUUsSUFBSSxTQUFTLEdBQUcsQ0FBQyxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFBLENBQUMsQ0FBQyxRQUFRLEVBQUMsQ0FBQyxHQUFRO3dCQUMzSiw2QkFBSyxTQUFTLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEVBQUMsVUFBVSxFQUFFLFNBQVMsSUFBSSxFQUFFLElBQUksU0FBUyxHQUFHLENBQUMsSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQSxDQUFDLENBQUMsUUFBUSxFQUFDLENBQUMsR0FBUTt3QkFDM0osNkJBQUssU0FBUyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFDLFVBQVUsRUFBRSxTQUFTLElBQUksRUFBRSxJQUFJLFNBQVMsR0FBRyxDQUFDLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUEsQ0FBQyxDQUFDLFFBQVEsRUFBQyxDQUFDLEdBQVE7d0JBRTdJLDZCQUFLLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEVBQUMsVUFBVSxFQUFFLFNBQVMsSUFBSSxFQUFFLElBQUksU0FBUyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFBLENBQUMsQ0FBQyxRQUFRLEVBQUMsQ0FBQyxHQUFRO3dCQUN6SSw2QkFBSyxTQUFTLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUMsVUFBVSxFQUFFLFNBQVMsSUFBSSxFQUFFLElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFBLENBQUMsQ0FBQyxRQUFRLEVBQUMsQ0FBQyxHQUFRO3dCQUVoSSw2QkFBSyxTQUFTLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEVBQUMsVUFBVSxFQUFFLFNBQVMsS0FBSyxFQUFFLElBQUksU0FBUyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFBLENBQUMsQ0FBQyxRQUFRLEVBQUMsQ0FBQyxHQUFRO3dCQUUvSSw2QkFBSyxTQUFTLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUMsVUFBVSxFQUFFLFNBQVMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQSxDQUFDLENBQUMsUUFBUSxFQUFDLENBQUMsR0FBUTt3QkFDakgsNkJBQUssT0FBTyxFQUFFLGNBQU0sT0FBQSxPQUFPLEVBQUUsRUFBVCxDQUFTLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFDLFVBQVUsRUFBRSxTQUFTLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUEsQ0FBQyxDQUFDLFFBQVEsRUFBQyxDQUFDLGlDQUFtQixDQUNwSixDQUNGLENBQ0Y7WUFDTiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLElBRTFCLFNBQVMsSUFBSSxFQUFFLElBQUksU0FBUyxHQUFHLENBQUM7O29CQUUvQixDQUNDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFDN0IsU0FBUyxDQUNOLENBQ04sQ0FFQTtZQUNOLDZCQUFLLEtBQUssRUFBRSxFQUFDLEtBQUssRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBQyxHQWVqRjtZQUVOLDZCQUFLLFNBQVMsRUFBRSxTQUFTLElBQUksRUFBRSxJQUFJLFNBQVMsR0FBRyxDQUFDLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQVE7WUFDL0ksNkJBQUssU0FBUyxFQUFFLFNBQVMsSUFBSSxFQUFFLElBQUksU0FBUyxHQUFHLENBQUMsSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBUTtZQUMvSSw2QkFBSyxTQUFTLEVBQUUsU0FBUyxJQUFJLEVBQUUsSUFBSSxTQUFTLEdBQUcsQ0FBQyxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFRO1lBQy9JLDZCQUFLLFNBQVMsRUFBRSxTQUFTLElBQUksRUFBRSxJQUFJLFNBQVMsR0FBRyxDQUFDLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQVE7WUFDL0ksNkJBQUssU0FBUyxFQUFFLFNBQVMsSUFBSSxFQUFFLElBQUksU0FBUyxHQUFHLENBQUMsSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBUTtZQUc3SSxDQUFDLFNBQVMsR0FBRyxFQUFFLElBQUksU0FBUyxHQUFHLEVBQUUsQ0FBQzttQkFDL0IsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFHLFNBQVMsR0FBRyxFQUFFLENBQU87WUFHN0QsU0FBUyxHQUFHLEVBQUUsSUFBSSxTQUFTLEdBQUcsRUFBRTttQkFDN0IsNkJBQUssU0FBUyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUcsU0FBUyxHQUFHLEVBQUUsQ0FBTyxDQUV2RSxDQUNGLENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./container/game/beauty-hunter/step-2/container.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var connect = __webpack_require__(129).connect;






var container_HalioLandingPageContainer = /** @class */ (function (_super) {
    __extends(HalioLandingPageContainer, _super);
    function HalioLandingPageContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.timer = null;
        _this.state = INITIAL_STATE;
        window.isShaking = false;
        setTimeout(function () {
            if (typeof window.DeviceMotionEvent != 'undefined') {
                // Shake sensitivity (a lower number is more)
                var sensitivity = 20;
                // Position variables
                var x1 = 0, y1 = 0, z1 = 0, x2 = 0, y2 = 0, z2 = 0;
                // Listen to motion events and update the position
                window.addEventListener('devicemotion', function (e) {
                    x1 = e.accelerationIncludingGravity.x;
                    y1 = e.accelerationIncludingGravity.y;
                    z1 = e.accelerationIncludingGravity.z;
                }, false);
                // Periodically check the position and fire
                // if the change is greater than the sensitivity
                setInterval(function () {
                    var change = Math.abs(x1 - x2 + y1 - y2 + z1 - z2);
                    if (change > sensitivity) {
                        window.isShaking = true;
                        try {
                            window.navigator.vibrate(200);
                        }
                        catch (e) { }
                    }
                    else {
                        window.isShaking = false;
                    }
                    // Update new position
                    x2 = x1;
                    y2 = y1;
                    z2 = z1;
                }, 150);
            }
        }, 3000);
        return _this;
        // this.aud1.loop = true;
        // this.aud2.loop = true;
        // this.aud3.loop = true;
    }
    HalioLandingPageContainer.prototype.componentWillMount = function () {
        var _a = this.props, history = _a.history, playGameAction = _a.playGameAction, gameStore = _a.gameStore;
        Object(responsive["b" /* isDesktopVersion */])() && history.push("" + routing["kb" /* ROUTING_SHOP_INDEX */]);
        this.initGame();
        this.props.updateMetaInfoAction({
            info: {
                url: "https://www.lixibox.com",
                type: "article",
                title: 'Beauty Hunter - Lixibox Game',
                description: 'Lixibox shop box mỹ phẩm cao cấp, trị mụn, dưỡng da thiết kế bởi các chuyên gia mailovesbeauty, love at first shine, changmakeup, skincare junkie ngo nhi',
                keyword: 'mỹ phẩm, dưỡng da, trị mụn, skincare, makeup, halio, lustre',
                image: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/meta/cover.jpeg'
            },
            structuredData: {
                breadcrumbList: []
            }
        });
    };
    HalioLandingPageContainer.prototype.initGame = function () {
        var _this = this;
        var _a = this.props, history = _a.history, playGameAction = _a.playGameAction, gameStore = _a.gameStore;
        this.timer = setInterval(function () {
            var _a = _this.state, countShaking = _a.countShaking, countDown = _a.countDown;
            if (countDown === 0) {
                window.removeEventListener('devicemotion', function () { });
                if (countShaking <= 2) {
                    _this.setState({
                        countDown: 14,
                        isShaking: false,
                        countShaking: 0
                    });
                    clearInterval(_this.timer);
                    return;
                }
                else {
                    !!gameStore && !!gameStore.loadGame && gameStore.loadGame.id
                        && playGameAction({ id: gameStore.loadGame.id });
                }
            }
            if (countDown === -3) {
                clearInterval(_this.timer);
                history.push('/games/beauty-hunter/result');
            }
            else {
                var isShaking = window.isShaking;
                if (countDown > 10) {
                }
                if (countDown === 10) {
                }
                if (countDown === -1) {
                }
                _this.setState({
                    countDown: countDown - 1,
                    isShaking: isShaking,
                    countShaking: countShaking + isShaking
                });
            }
            try {
                var aud = document.getElementById("audio");
                !!aud && aud.play();
            }
            catch (e) {
            }
        }, 1000);
    };
    HalioLandingPageContainer.prototype.componentWillUnmount = function () {
        // this.aud1.pause()
        // this.aud2.pause()
        // this.aud3.pause()
    };
    HalioLandingPageContainer.prototype.componentDidMount = function () {
        var appContainer = document.getElementsByTagName('app-container')[0];
        appContainer.style.position = 'fixed';
    };
    HalioLandingPageContainer.prototype.componentDidUpdate = function () {
        try {
            // const aud: any = document.getElementById("audio");
            // !!aud && aud.play();
        }
        catch (e) {
        }
    };
    HalioLandingPageContainer.prototype.reShake = function () {
        this.initGame();
    };
    HalioLandingPageContainer.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state,
            reShake: this.reShake.bind(this)
        };
        return view(args);
    };
    HalioLandingPageContainer.defaultProps = DEFAULT_PROPS;
    HalioLandingPageContainer = __decorate([
        radium
    ], HalioLandingPageContainer);
    return HalioLandingPageContainer;
}(react["Component"]));
;
/* harmony default export */ var container = __webpack_exports__["default"] = (connect(mapStateToProps, mapDispatchToProps)(container_HalioLandingPageContainer));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFDakMsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUNoRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUMvRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUcxRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsZUFBZSxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBQzlELE9BQU8sRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQzVELE9BQU8sVUFBVSxNQUFNLFFBQVEsQ0FBQztBQVVoQztJQUF3Qyw2Q0FBK0I7SUFJckUsbUNBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQTJDYjtRQTlDTyxXQUFLLEdBQVEsSUFBSSxDQUFDO1FBSXhCLEtBQUksQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDO1FBRTNCLE1BQU0sQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBRXpCLFVBQVUsQ0FBQztZQUNULEVBQUUsQ0FBQyxDQUFDLE9BQU8sTUFBTSxDQUFDLGlCQUFpQixJQUFJLFdBQVcsQ0FBQyxDQUFDLENBQUM7Z0JBQ25ELDZDQUE2QztnQkFDN0MsSUFBSSxXQUFXLEdBQUcsRUFBRSxDQUFDO2dCQUVyQixxQkFBcUI7Z0JBQ3JCLElBQUksRUFBRSxHQUFHLENBQUMsRUFBRSxFQUFFLEdBQUcsQ0FBQyxFQUFFLEVBQUUsR0FBRyxDQUFDLEVBQUUsRUFBRSxHQUFHLENBQUMsRUFBRSxFQUFFLEdBQUcsQ0FBQyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBRW5ELGtEQUFrRDtnQkFDbEQsTUFBTSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsRUFBRSxVQUFVLENBQUM7b0JBQy9DLEVBQUUsR0FBRyxDQUFDLENBQUMsNEJBQTRCLENBQUMsQ0FBQyxDQUFDO29CQUN0QyxFQUFFLEdBQUcsQ0FBQyxDQUFDLDRCQUE0QixDQUFDLENBQUMsQ0FBQztvQkFDdEMsRUFBRSxHQUFHLENBQUMsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDLENBQUM7Z0JBQzFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFFViwyQ0FBMkM7Z0JBQzNDLGdEQUFnRDtnQkFDaEQsV0FBVyxDQUFDO29CQUNSLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFDLEVBQUUsR0FBQyxFQUFFLEdBQUMsRUFBRSxHQUFDLEVBQUUsR0FBQyxFQUFFLENBQUMsQ0FBQztvQkFFekMsRUFBRSxDQUFDLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUM7d0JBQ3pCLE1BQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO3dCQUN4QixJQUFJLENBQUM7NEJBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQUMsQ0FBQzt3QkFBQyxLQUFLLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUEsQ0FBQztvQkFDcEQsQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQzt3QkFDTixNQUFNLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztvQkFDM0IsQ0FBQztvQkFFRCxzQkFBc0I7b0JBQ3RCLEVBQUUsR0FBRyxFQUFFLENBQUM7b0JBQ1IsRUFBRSxHQUFHLEVBQUUsQ0FBQztvQkFDUixFQUFFLEdBQUcsRUFBRSxDQUFDO2dCQUNaLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUNWLENBQUM7UUFDSCxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7O1FBRVQseUJBQXlCO1FBQ3pCLHlCQUF5QjtRQUN6Qix5QkFBeUI7SUFDM0IsQ0FBQztJQUVELHNEQUFrQixHQUFsQjtRQUNRLElBQUEsZUFBbUQsRUFBakQsb0JBQU8sRUFBRSxrQ0FBYyxFQUFFLHdCQUFTLENBQWdCO1FBRTFELGdCQUFnQixFQUFFLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFHLGtCQUFvQixDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBRWhCLElBQUksQ0FBQyxLQUFLLENBQUMsb0JBQW9CLENBQUM7WUFDOUIsSUFBSSxFQUFFO2dCQUNKLEdBQUcsRUFBRSx5QkFBeUI7Z0JBQzlCLElBQUksRUFBRSxTQUFTO2dCQUNmLEtBQUssRUFBRSw4QkFBOEI7Z0JBQ3JDLFdBQVcsRUFBRSwySkFBMko7Z0JBQ3hLLE9BQU8sRUFBRSw2REFBNkQ7Z0JBQ3RFLEtBQUssRUFBRSxpQkFBaUIsR0FBRyxnQ0FBZ0M7YUFDNUQ7WUFDRCxjQUFjLEVBQUU7Z0JBQ2QsY0FBYyxFQUFFLEVBQUU7YUFDbkI7U0FDRixDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsNENBQVEsR0FBUjtRQUFBLGlCQW9EQztRQW5ETyxJQUFBLGVBQW1ELEVBQWpELG9CQUFPLEVBQUUsa0NBQWMsRUFBRSx3QkFBUyxDQUFnQjtRQUUxRCxJQUFJLENBQUMsS0FBSyxHQUFHLFdBQVcsQ0FBQztZQUNqQixJQUFBLGdCQUF3QyxFQUF0Qyw4QkFBWSxFQUFFLHdCQUFTLENBQWdCO1lBRS9DLEVBQUUsQ0FBQyxDQUFDLFNBQVMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNwQixNQUFNLENBQUMsbUJBQW1CLENBQUMsY0FBYyxFQUFFLGNBQU8sQ0FBQyxDQUFDLENBQUM7Z0JBRXJELEVBQUUsQ0FBQyxDQUFDLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUN0QixLQUFJLENBQUMsUUFBUSxDQUFDO3dCQUNaLFNBQVMsRUFBRSxFQUFFO3dCQUNiLFNBQVMsRUFBRSxLQUFLO3dCQUNoQixZQUFZLEVBQUUsQ0FBQztxQkFDTixDQUFDLENBQUM7b0JBQ2IsYUFBYSxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDMUIsTUFBTSxDQUFDO2dCQUVULENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ04sQ0FBQyxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsSUFBSSxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7MkJBQ3pELGNBQWMsQ0FBQyxFQUFDLEVBQUUsRUFBRSxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUUsRUFBQyxDQUFDLENBQUM7Z0JBQ2pELENBQUM7WUFDSCxDQUFDO1lBRUQsRUFBRSxDQUFDLENBQUMsU0FBUyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDckIsYUFBYSxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDMUIsT0FBTyxDQUFDLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDO1lBQzlDLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixJQUFNLFNBQVMsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDO2dCQUNuQyxFQUFFLENBQUMsQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDckIsQ0FBQztnQkFDRCxFQUFFLENBQUMsQ0FBQyxTQUFTLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDdkIsQ0FBQztnQkFDRCxFQUFFLENBQUMsQ0FBQyxTQUFTLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN2QixDQUFDO2dCQUdELEtBQUksQ0FBQyxRQUFRLENBQUM7b0JBQ1osU0FBUyxFQUFFLFNBQVMsR0FBRyxDQUFDO29CQUN4QixTQUFTLFdBQUE7b0JBQ1QsWUFBWSxFQUFFLFlBQVksR0FBRyxTQUFTO2lCQUM3QixDQUFDLENBQUM7WUFDZixDQUFDO1lBRUQsSUFBSSxDQUFDO2dCQUNILElBQU0sR0FBRyxHQUFRLFFBQVEsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ2xELENBQUMsQ0FBQyxHQUFHLElBQUksR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ3RCLENBQUM7WUFBQyxLQUFLLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRVosQ0FBQztRQUNILENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUVYLENBQUM7SUFFRCx3REFBb0IsR0FBcEI7UUFDRSxvQkFBb0I7UUFDcEIsb0JBQW9CO1FBQ3BCLG9CQUFvQjtJQUN0QixDQUFDO0lBRUQscURBQWlCLEdBQWpCO1FBQ0UsSUFBTSxZQUFZLEdBQU8sUUFBUSxDQUFDLG9CQUFvQixDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzNFLFlBQVksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQztJQUN4QyxDQUFDO0lBRUQsc0RBQWtCLEdBQWxCO1FBQ0UsSUFBSSxDQUFDO1lBQ0gscURBQXFEO1lBQ3JELHVCQUF1QjtRQUN6QixDQUFDO1FBQUMsS0FBSyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUVaLENBQUM7SUFDSCxDQUFDO0lBRUQsMkNBQU8sR0FBUDtRQUNFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQixDQUFDO0lBRUQsMENBQU0sR0FBTjtRQUNFLElBQU0sSUFBSSxHQUFHO1lBQ1gsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ2pDLENBQUM7UUFFRixNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUE1Sk0sc0NBQVksR0FBVyxhQUFhLENBQUM7SUFEeEMseUJBQXlCO1FBRDlCLE1BQU07T0FDRCx5QkFBeUIsQ0E4SjlCO0lBQUQsZ0NBQUM7Q0FBQSxBQTlKRCxDQUF3QyxLQUFLLENBQUMsU0FBUyxHQThKdEQ7QUFBQSxDQUFDO0FBRUYsZUFBZSxPQUFPLENBQ3BCLGVBQWUsRUFDZixrQkFBa0IsQ0FDbkIsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDIn0=

/***/ })

}]);