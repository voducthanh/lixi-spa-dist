(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[92],{

/***/ 851:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// EXTERNAL MODULE: ./utils/uri.ts
var uri = __webpack_require__(347);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./container/error/style.tsx

/* harmony default export */ var style = ({
    wrap: {
        display: variable["display"].flex,
        background: variable["colorWhite"],
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        maxWidth: 550,
        paddingTop: 50,
        paddingLeft: 0,
        paddingRight: 0,
        paddingBottom: 30,
        zIndex: variable["zIndexMax"],
        position: 'relative',
        margin: '50px auto'
    },
    overlay: {
        position: 'fixed',
        width: '100%',
        height: '100%',
        background: variable["colorBlack08"],
        zIndex: variable["zIndexMax"],
        top: 0,
        left: 0,
    },
    image: {
        display: variable["display"].block,
        width: 300,
        height: 'auto',
        minWidth: 100,
        minHeight: 100,
        marginBottom: 10,
        pointerEvents: 'none'
    },
    emptyContent: {
        marginBottom: 40,
        title: {
            fontSize: 24,
            lineHeight: '32px',
            marginBottom: 10,
            fontFamily: variable["fontTrirong"],
            fontWeight: 600,
            textAlign: 'center',
            color: variable["color75"],
        },
        description: {
            fontSize: 16,
            color: variable["color97"],
            textAlign: 'center',
            width: '100%',
            margin: '0 auto',
        }
    },
    linkNav: {
        width: '100%',
        display: variable["display"].flex,
        flexWrap: 'wrap',
        justifyContent: 'center'
    },
    iconShop: {
        width: 40,
        height: 40,
        color: variable["colorA2"]
    },
    iconMagazine: {
        width: 40,
        height: 40,
        color: variable["colorWhite"]
    },
    iconInner: {
        height: 18
    },
    linkShop: {
        display: variable["display"].flex,
        border: "1px solid " + variable["colorA2"],
        margin: '5px 10px',
        borderRadius: 22,
        paddingRight: 25,
        paddingLeft: 10,
        height: 44,
        cursor: 'pointer',
        transition: variable["transitionNormal"],
        ':hover': {
            boxShadow: variable["shadow3"]
        }
    },
    textShop: {
        display: variable["display"].block,
        color: variable["colorA2"],
        lineHeight: '42px',
        fontFamily: variable["fontAvenirMedium"],
        whiteSpace: 'nowrap',
        fontSize: 16,
        textTransform: 'uppercase'
    },
    linkMagazine: {
        cursor: 'pointer',
        display: variable["display"].flex,
        background: variable["color4D"],
        margin: '5px 10px',
        borderRadius: 22,
        paddingRight: 25,
        paddingLeft: 10,
        height: 44,
        transition: variable["transitionNormal"],
        ':hover': {
            boxShadow: variable["shadow3"]
        }
    },
    textMagazine: {
        display: variable["display"].block,
        color: variable["colorWhite"],
        lineHeight: '42px',
        fontFamily: variable["fontAvenirMedium"],
        whiteSpace: 'nowrap',
        fontSize: 16,
        textTransform: 'uppercase'
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSxzQkFBc0IsQ0FBQztBQUVqRCxlQUFlO0lBQ2IsSUFBSSxFQUFFO1FBQ0osT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUM5QixVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVU7UUFDL0IsYUFBYSxFQUFFLFFBQVE7UUFDdkIsY0FBYyxFQUFFLFFBQVE7UUFDeEIsVUFBVSxFQUFFLFFBQVE7UUFDcEIsS0FBSyxFQUFFLE1BQU07UUFDYixRQUFRLEVBQUUsR0FBRztRQUNiLFVBQVUsRUFBRSxFQUFFO1FBQ2QsV0FBVyxFQUFFLENBQUM7UUFDZCxZQUFZLEVBQUUsQ0FBQztRQUNmLGFBQWEsRUFBRSxFQUFFO1FBQ2pCLE1BQU0sRUFBRSxRQUFRLENBQUMsU0FBUztRQUMxQixRQUFRLEVBQUUsVUFBVTtRQUNwQixNQUFNLEVBQUUsV0FBVztLQUNwQjtJQUVELE9BQU8sRUFBRTtRQUNQLFFBQVEsRUFBRSxPQUFPO1FBQ2pCLEtBQUssRUFBRSxNQUFNO1FBQ2IsTUFBTSxFQUFFLE1BQU07UUFDZCxVQUFVLEVBQUUsUUFBUSxDQUFDLFlBQVk7UUFDakMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxTQUFTO1FBQzFCLEdBQUcsRUFBRSxDQUFDO1FBQ04sSUFBSSxFQUFFLENBQUM7S0FDUjtJQUVELEtBQUssRUFBRTtRQUNMLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7UUFDL0IsS0FBSyxFQUFFLEdBQUc7UUFDVixNQUFNLEVBQUUsTUFBTTtRQUNkLFFBQVEsRUFBRSxHQUFHO1FBQ2IsU0FBUyxFQUFFLEdBQUc7UUFDZCxZQUFZLEVBQUUsRUFBRTtRQUNoQixhQUFhLEVBQUUsTUFBTTtLQUN0QjtJQUVELFlBQVksRUFBRTtRQUNaLFlBQVksRUFBRSxFQUFFO1FBRWhCLEtBQUssRUFBRTtZQUNMLFFBQVEsRUFBRSxFQUFFO1lBQ1osVUFBVSxFQUFFLE1BQU07WUFDbEIsWUFBWSxFQUFFLEVBQUU7WUFDaEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxXQUFXO1lBQ2hDLFVBQVUsRUFBRSxHQUFHO1lBQ2YsU0FBUyxFQUFFLFFBQVE7WUFDbkIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO1NBQ3hCO1FBRUQsV0FBVyxFQUFFO1lBQ1gsUUFBUSxFQUFFLEVBQUU7WUFDWixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87WUFDdkIsU0FBUyxFQUFFLFFBQVE7WUFDbkIsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsUUFBUTtTQUNqQjtLQUNGO0lBRUQsT0FBTyxFQUFFO1FBQ1AsS0FBSyxFQUFFLE1BQU07UUFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1FBQzlCLFFBQVEsRUFBRSxNQUFNO1FBQ2hCLGNBQWMsRUFBRSxRQUFRO0tBQ3pCO0lBRUQsUUFBUSxFQUFFO1FBQ1IsS0FBSyxFQUFFLEVBQUU7UUFDVCxNQUFNLEVBQUUsRUFBRTtRQUNWLEtBQUssRUFBRSxRQUFRLENBQUMsT0FBTztLQUN4QjtJQUVELFlBQVksRUFBRTtRQUNaLEtBQUssRUFBRSxFQUFFO1FBQ1QsTUFBTSxFQUFFLEVBQUU7UUFDVixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7S0FDM0I7SUFFRCxTQUFTLEVBQUU7UUFDVCxNQUFNLEVBQUUsRUFBRTtLQUNYO0lBRUQsUUFBUSxFQUFFO1FBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUM5QixNQUFNLEVBQUUsZUFBYSxRQUFRLENBQUMsT0FBUztRQUN2QyxNQUFNLEVBQUUsVUFBVTtRQUNsQixZQUFZLEVBQUUsRUFBRTtRQUNoQixZQUFZLEVBQUUsRUFBRTtRQUNoQixXQUFXLEVBQUUsRUFBRTtRQUNmLE1BQU0sRUFBRSxFQUFFO1FBQ1YsTUFBTSxFQUFFLFNBQVM7UUFDakIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7UUFFckMsUUFBUSxFQUFFO1lBQ1IsU0FBUyxFQUFFLFFBQVEsQ0FBQyxPQUFPO1NBQzVCO0tBQ0Y7SUFFRCxRQUFRLEVBQUU7UUFDUixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1FBQy9CLEtBQUssRUFBRSxRQUFRLENBQUMsT0FBTztRQUN2QixVQUFVLEVBQUUsTUFBTTtRQUNsQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtRQUNyQyxVQUFVLEVBQUUsUUFBUTtRQUNwQixRQUFRLEVBQUUsRUFBRTtRQUNaLGFBQWEsRUFBRSxXQUFXO0tBQzNCO0lBRUQsWUFBWSxFQUFFO1FBQ1osTUFBTSxFQUFFLFNBQVM7UUFDakIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUM5QixVQUFVLEVBQUUsUUFBUSxDQUFDLE9BQU87UUFDNUIsTUFBTSxFQUFFLFVBQVU7UUFDbEIsWUFBWSxFQUFFLEVBQUU7UUFDaEIsWUFBWSxFQUFFLEVBQUU7UUFDaEIsV0FBVyxFQUFFLEVBQUU7UUFDZixNQUFNLEVBQUUsRUFBRTtRQUNWLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO1FBRXJDLFFBQVEsRUFBRTtZQUNSLFNBQVMsRUFBRSxRQUFRLENBQUMsT0FBTztTQUM1QjtLQUNGO0lBRUQsWUFBWSxFQUFFO1FBQ1osT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztRQUMvQixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7UUFDMUIsVUFBVSxFQUFFLE1BQU07UUFDbEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7UUFDckMsVUFBVSxFQUFFLFFBQVE7UUFDcEIsUUFBUSxFQUFFLEVBQUU7UUFDWixhQUFhLEVBQUUsV0FBVztLQUMzQjtDQUNNLENBQUMifQ==
// CONCATENATED MODULE: ./container/error/view.tsx



var background = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/404/404.png';

var renderView = function (_a) {
    var handleBackToHome = _a.handleBackToHome, handleReload = _a.handleReload;
    return (react["createElement"]("div", null,
        react["createElement"]("div", { style: style.overlay }),
        react["createElement"]("div", { style: style.wrap },
            react["createElement"]("img", { src: background, style: style.image }),
            react["createElement"]("div", { style: style.emptyContent },
                react["createElement"]("div", { style: style.emptyContent.title }, "C\u00D3 G\u00CC \u0110\u00D3 SAI SAI"),
                react["createElement"]("div", { style: style.emptyContent.description }, "C\u00F3 v\u1EBB nh\u01B0 b\u1EA1n \u0111\u00E3 v\u00E0o \u0111\u1ECBa ch\u1EC9 kh\u00F4ng \u0111\u00FAng"),
                react["createElement"]("div", { style: style.emptyContent.description }, "ho\u1EB7c s\u1EA3n ph\u1EA9m kh\u00F4ng c\u00F2n b\u00E1n tr\u00EAn LIXIBOX")),
            react["createElement"]("div", { style: style.linkNav },
                react["createElement"]("div", { style: style.linkShop, onClick: function () { return handleBackToHome(); } },
                    react["createElement"](icon["a" /* default */], { name: 'cart-line', style: style.iconShop, innerStyle: style.iconInner }),
                    react["createElement"]("span", { style: style.textShop }, "V\u1EC1 Trang ch\u1EE7")),
                react["createElement"]("div", { style: style.linkMagazine, onClick: function () { return handleReload(); } },
                    react["createElement"](icon["a" /* default */], { name: 'refresh', style: style.iconMagazine, innerStyle: style.iconInner }),
                    react["createElement"]("span", { style: style.textMagazine }, "T\u1EA3i l\u1EA1i trang"))))));
};
/* harmony default export */ var view = __webpack_exports__["default"] = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRy9CLE9BQU8sSUFBSSxNQUFNLDBCQUEwQixDQUFDO0FBQzVDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3BELElBQU0sVUFBVSxHQUFHLGlCQUFpQixHQUFHLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixJQUFNLFVBQVUsR0FBRyxVQUFDLEVBQWdDO1FBQS9CLHNDQUFnQixFQUFFLDhCQUFZO0lBQ2pELE1BQU0sQ0FBQyxDQUNMO1FBQ0UsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxPQUFPLEdBQVE7UUFDakMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJO1lBQ3BCLDZCQUFLLEdBQUcsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLLEdBQUk7WUFDNUMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxZQUFZO2dCQUM1Qiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFlBQVksQ0FBQyxLQUFLLDJDQUF3QjtnQkFDNUQsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxZQUFZLENBQUMsV0FBVywrR0FBK0M7Z0JBQ3pGLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWSxDQUFDLFdBQVcsa0ZBQWdELENBQ3RGO1lBQ04sNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxPQUFPO2dCQUN2Qiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsY0FBTSxPQUFBLGdCQUFnQixFQUFFLEVBQWxCLENBQWtCO29CQUMzRCxvQkFBQyxJQUFJLElBQUMsSUFBSSxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxVQUFVLEVBQUUsS0FBSyxDQUFDLFNBQVMsR0FBSTtvQkFDL0UsOEJBQU0sS0FBSyxFQUFFLEtBQUssQ0FBQyxRQUFRLDZCQUFxQixDQUM1QztnQkFDTiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFlBQVksRUFBRSxPQUFPLEVBQUUsY0FBTSxPQUFBLFlBQVksRUFBRSxFQUFkLENBQWM7b0JBQzNELG9CQUFDLElBQUksSUFBQyxJQUFJLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWSxFQUFFLFVBQVUsRUFBRSxLQUFLLENBQUMsU0FBUyxHQUFJO29CQUNqRiw4QkFBTSxLQUFLLEVBQUUsS0FBSyxDQUFDLFlBQVksOEJBQXNCLENBQ2pELENBQ0YsQ0FDRixDQUNGLENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=

/***/ })

}]);