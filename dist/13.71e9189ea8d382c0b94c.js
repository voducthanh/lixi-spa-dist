(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[13],{

/***/ 756:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 96).then(__webpack_require__.bind(null, 779)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 793:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./utils/auth.ts + 1 modules
var auth = __webpack_require__(23);

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// CONCATENATED MODULE: ./api/love.ts


;
var fetchLoveList = function (_a) {
    var _b = _a.sort, sort = _b === void 0 ? 'desc' : _b, _c = _a.page, page = _c === void 0 ? 1 : _c, _d = _a.perPage, perPage = _d === void 0 ? 5 : _d;
    var query = "?sort[created_at]=" + sort + "&page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/loves" + query,
        description: 'Get love list',
        errorMesssage: "Can't get love list. Please try again",
    });
};
;
var getLoveDetail = function (_a) {
    var id = _a.id;
    return Object(restful_method["b" /* get */])({
        path: "/loves/" + id,
        description: 'Get love detail',
        errorMesssage: "Can't get love detail. Please try again",
    });
};
var addLove = function (_a) {
    var sharedUrl = _a.sharedUrl;
    return Object(restful_method["d" /* post */])({
        path: "/loves",
        data: {
            csrf_token: Object(auth["c" /* getCsrfToken */])(),
            shared_url: sharedUrl
        },
        description: 'User add love',
        errorMesssage: "Can't add love. Please try again",
    });
};
var getLoveBoxById = function (_a) {
    var id = _a.id;
    return Object(restful_method["b" /* get */])({
        path: "/loves/box/" + id,
        description: 'Get love box by id',
        errorMesssage: "Can't get love box by id. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG92ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxvdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3QyxPQUFPLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBTXBELENBQUM7QUFFRixNQUFNLENBQUMsSUFBTSxhQUFhLEdBQ3hCLFVBQUMsRUFBd0Q7UUFBdEQsWUFBYSxFQUFiLGtDQUFhLEVBQUUsWUFBUSxFQUFSLDZCQUFRLEVBQUUsZUFBVyxFQUFYLGdDQUFXO0lBQ3JDLElBQU0sS0FBSyxHQUFHLHVCQUFxQixJQUFJLGNBQVMsSUFBSSxrQkFBYSxPQUFTLENBQUM7SUFFM0UsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSxXQUFTLEtBQU87UUFDdEIsV0FBVyxFQUFFLGVBQWU7UUFDNUIsYUFBYSxFQUFFLHVDQUF1QztLQUN2RCxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFJSCxDQUFDO0FBRUYsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUN4QixVQUFDLEVBQXdCO1FBQXRCLFVBQUU7SUFBeUIsT0FBQSxHQUFHLENBQUM7UUFDaEMsSUFBSSxFQUFFLFlBQVUsRUFBSTtRQUNwQixXQUFXLEVBQUUsaUJBQWlCO1FBQzlCLGFBQWEsRUFBRSx5Q0FBeUM7S0FDekQsQ0FBQztBQUo0QixDQUk1QixDQUFDO0FBRUwsTUFBTSxDQUFDLElBQU0sT0FBTyxHQUNsQixVQUFDLEVBQWE7UUFBWCx3QkFBUztJQUVWLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDVixJQUFJLEVBQUUsUUFBUTtRQUNkLElBQUksRUFBRTtZQUNKLFVBQVUsRUFBRSxZQUFZLEVBQUU7WUFDMUIsVUFBVSxFQUFFLFNBQVM7U0FDdEI7UUFDRCxXQUFXLEVBQUUsZUFBZTtRQUM1QixhQUFhLEVBQUUsa0NBQWtDO0tBQ2xELENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQztBQUVKLE1BQU0sQ0FBQyxJQUFNLGNBQWMsR0FBRyxVQUFDLEVBQU07UUFBSixVQUFFO0lBQU8sT0FBQSxHQUFHLENBQUM7UUFDNUMsSUFBSSxFQUFFLGdCQUFjLEVBQUk7UUFDeEIsV0FBVyxFQUFFLG9CQUFvQjtRQUNqQyxhQUFhLEVBQUUsNENBQTRDO0tBQzVELENBQUM7QUFKd0MsQ0FJeEMsQ0FBQyJ9
// EXTERNAL MODULE: ./constants/api/love.ts
var love = __webpack_require__(49);

// CONCATENATED MODULE: ./action/love.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fetchLoveListAction; });
/* unused harmony export getLoveDetailAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return addLoveAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getLoveBoxByIdAction; });


/**
 * Fetch love list by filter params
 *
 * @param {'asc' | 'desc'} sort
 * @param {number} page ex: 1, 2, 3, 4
 * @param {number} perPage ex: 10, 15, 20
 */
var fetchLoveListAction = function (_a) {
    var sort = _a.sort, page = _a.page, perPage = _a.perPage;
    return function (dispatch, getState) {
        return dispatch({
            type: love["b" /* FETCH_LOVE_LIST */],
            payload: { promise: fetchLoveList({ sort: sort, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { page: page, perPage: perPage }
        });
    };
};
/**
 * Get love detail by id
 *
 * @param {number} id
 */
var getLoveDetailAction = function (_a) {
    var id = _a.id;
    return function (dispatch, getState) {
        return dispatch({
            type: love["d" /* GET_LOVE_DETAIL */],
            payload: { promise: getLoveDetail({ id: id }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
/**
* Add love by share_url
*
* @param {string} share_url
*/
var addLoveAction = function (_a) {
    var sharedUrl = _a.sharedUrl;
    return function (dispatch, getState) {
        return dispatch({
            type: love["a" /* ADD_LOVE */],
            payload: { promise: addLove({ sharedUrl: sharedUrl }).then(function (res) { return res; }) },
        });
    };
};
/**
 * Get love box by id
 *
 * @param {number} id
 */
var getLoveBoxByIdAction = function (_a) {
    var id = _a.id;
    return function (dispatch, getState) {
        return dispatch({
            type: love["c" /* GET_LOVE_BOX */],
            payload: { promise: getLoveBoxById({ id: id }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG92ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxvdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLE9BQU8sRUFDUCxhQUFhLEVBQ2IsYUFBYSxFQUNiLGNBQWMsR0FHZixNQUFNLGFBQWEsQ0FBQztBQUVyQixPQUFPLEVBQ0wsUUFBUSxFQUNSLFlBQVksRUFDWixlQUFlLEVBQ2YsZUFBZSxHQUNoQixNQUFNLHVCQUF1QixDQUFDO0FBRS9COzs7Ozs7R0FNRztBQUNILE1BQU0sQ0FBQyxJQUFNLG1CQUFtQixHQUM5QixVQUFDLEVBQXVDO1FBQXJDLGNBQUksRUFBRSxjQUFJLEVBQUUsb0JBQU87SUFDcEIsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLGVBQWU7WUFDckIsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLGFBQWEsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDN0UsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDeEIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7OztHQUlHO0FBQ0gsTUFBTSxDQUFDLElBQU0sbUJBQW1CLEdBQzlCLFVBQUMsRUFBd0I7UUFBdEIsVUFBRTtJQUNILE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxlQUFlO1lBQ3JCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxhQUFhLENBQUMsRUFBRSxFQUFFLElBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQzVELElBQUksRUFBRSxFQUFFLEVBQUUsSUFBQSxFQUFFO1NBQ2IsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUN4QixVQUFDLEVBQWE7UUFBWCx3QkFBUztJQUNWLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxRQUFRO1lBQ2QsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLE9BQU8sQ0FBQyxFQUFFLFNBQVMsV0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7U0FDOUQsQ0FBQztJQUhGLENBR0U7QUFKSixDQUlJLENBQUM7QUFFVDs7OztHQUlHO0FBQ0gsTUFBTSxDQUFDLElBQU0sb0JBQW9CLEdBQUcsVUFBQyxFQUFNO1FBQUosVUFBRTtJQUN2QyxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsWUFBWTtZQUNsQixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsY0FBYyxDQUFDLEVBQUUsRUFBRSxJQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUM3RCxJQUFJLEVBQUUsRUFBRSxFQUFFLElBQUEsRUFBRTtTQUNiLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDIn0=

/***/ }),

/***/ 803:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./constants/application/global.ts
var global = __webpack_require__(24);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./utils/index.ts
var utils = __webpack_require__(788);

// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/item/feedback-testimonial/ItemFeedback.style.tsx


var generateSwitchStyle = function (mobile, desktop) {
    var switchStyle = {
        MOBILE: { paddingRight: 10, width: mobile, minWidth: mobile },
        DESKTOP: { width: desktop }
    };
    return switchStyle[window.DEVICE_VERSION];
};
/* harmony default export */ var ItemFeedback_style = ({
    container: {
        height: '100%',
        itemSlider: {
            width: "100%",
            height: "100%",
            display: variable["display"].inlineBlock,
            borderRadius: 5,
            overflow: 'hidden',
            boxShadow: variable["shadowBlur"],
            position: variable["position"].relative
        },
        itemSliderPanel: {
            width: '100%',
            paddingTop: '75%',
            position: variable["position"].relative,
        },
        innerItemSliderPanel: function (imgUrl) { return ({
            width: '100%',
            height: '100%',
            top: 0,
            left: 0,
            position: variable["position"].absolute,
            backgroundImage: "url(" + imgUrl + ")",
            backgroundColor: variable["colorF7"],
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            transition: variable["transitionOpacity"]
        }); },
        info: {
            width: '100%',
            padding: 12,
            position: variable["position"].relative,
            background: variable["colorWhite"],
            display: variable["display"].flex,
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
        }
    },
    info: {
        container: [
            layout["a" /* flexContainer */].left, {
                width: '100%',
                marginBottom: 15
            }
        ],
        avatar: {
            width: 40,
            minWidth: 40,
            height: 40,
            borderRadius: 25,
            marginRight: 10,
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundColor: variable["colorE5"],
            transition: variable["transitionOpacity"],
            small: {
                width: 30,
                minWidth: 30,
                height: 30,
                borderRadius: '50%',
            }
        },
        username: {
            paddingRight: 15,
            marginBottom: 5,
            textAlign: 'left',
        },
        detail: {
            flex: 10,
            display: variable["display"].flex,
            flexDirection: 'column',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            justifyContent: 'center',
            username: {
                fontFamily: variable["fontAvenirDemiBold"],
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
                overflow: 'hidden',
                fontSize: 14,
                lineHeight: '22px',
                marginRight: 5,
                color: variable["colorBlack"]
            },
            ratingGroup: {
                display: variable["display"].flex,
                alignItems: 'center',
                lineHeight: '23px',
                height: 18,
                color: variable["color97"],
                cursor: 'pointer'
            }
        },
        description: {
            fontSize: 14,
            overflow: 'hidden',
            lineHeight: '22px',
            textAlign: 'justify',
            height: 66,
            maxHeight: 66,
            width: '100%',
            color: variable["colorBlack08"],
            whiteSpace: 'pre-wrap'
        }
    },
    icon: {
        position: variable["position"].absolute,
        top: '50%',
        left: '50%',
        color: variable["colorWhite"],
        transform: 'translate(-50%, -50%)',
        zIndex: variable["zIndex2"],
        inner: {
            width: 60,
            height: 60,
        }
    },
    videoIcon: {
        width: 70,
        height: 70,
        left: '55%',
        transform: 'translate(-50%, -50%)',
        borderTop: "35px solid " + variable["colorTransparent"],
        borderLeft: "51px solid " + variable["colorWhite"],
        borderBottom: "35px solid " + variable["colorTransparent"],
        position: variable["position"].absolute,
        boxSizing: "border-box",
        opacity: 0.8,
        top: '50%'
    },
    column: {
        1: { width: '100%' },
        2: { width: '50%' },
        3: generateSwitchStyle('75%', '33.33%'),
        4: generateSwitchStyle('47%', '25%'),
        5: generateSwitchStyle('47%', '20%'),
        6: generateSwitchStyle('50%', '16.66%'),
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSXRlbUZlZWRiYWNrLnN0eWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiSXRlbUZlZWRiYWNrLnN0eWxlLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxPQUFPLEtBQUssTUFBTSxNQUFNLHVCQUF1QixDQUFDO0FBQ2hELE9BQU8sS0FBSyxRQUFRLE1BQU0seUJBQXlCLENBQUM7QUFFcEQsSUFBTSxtQkFBbUIsR0FBRyxVQUFDLE1BQU0sRUFBRSxPQUFPO0lBQzFDLElBQU0sV0FBVyxHQUFHO1FBQ2xCLE1BQU0sRUFBRSxFQUFFLFlBQVksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFO1FBQzdELE9BQU8sRUFBRSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUU7S0FDNUIsQ0FBQztJQUVGLE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0FBQzVDLENBQUMsQ0FBQztBQUVGLGVBQWU7SUFDYixTQUFTLEVBQUU7UUFDVCxNQUFNLEVBQUUsTUFBTTtRQUVkLFVBQVUsRUFBRTtZQUNWLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLE1BQU07WUFDZCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxXQUFXO1lBQ3JDLFlBQVksRUFBRSxDQUFDO1lBQ2YsUUFBUSxFQUFFLFFBQVE7WUFDbEIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1lBQzlCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7U0FDckM7UUFFRCxlQUFlLEVBQUM7WUFDZCxLQUFLLEVBQUUsTUFBTTtZQUNiLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7U0FDckM7UUFFRCxvQkFBb0IsRUFBRSxVQUFDLE1BQU0sSUFBSyxPQUFBLENBQUM7WUFDakMsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsTUFBTTtZQUNkLEdBQUcsRUFBRSxDQUFDO1lBQ04sSUFBSSxFQUFFLENBQUM7WUFDUCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1lBQ3BDLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztZQUNqQyxlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87WUFDakMsa0JBQWtCLEVBQUUsUUFBUTtZQUM1QixjQUFjLEVBQUUsT0FBTztZQUN2QixVQUFVLEVBQUUsUUFBUSxDQUFDLGlCQUFpQjtTQUN2QyxDQUFDLEVBWGdDLENBV2hDO1FBRUYsSUFBSSxFQUFFO1lBQ0osS0FBSyxFQUFFLE1BQU07WUFDYixPQUFPLEVBQUUsRUFBRTtZQUNYLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDcEMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxVQUFVO1lBQy9CLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsYUFBYSxFQUFFLFFBQVE7WUFDdkIsY0FBYyxFQUFFLFlBQVk7WUFDNUIsVUFBVSxFQUFFLFlBQVk7U0FDekI7S0FDRjtJQUVELElBQUksRUFBRTtRQUNKLFNBQVMsRUFBRTtZQUNULE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFO2dCQUN6QixLQUFLLEVBQUUsTUFBTTtnQkFDYixZQUFZLEVBQUUsRUFBRTthQUNqQjtTQUFDO1FBRUosTUFBTSxFQUFFO1lBQ04sS0FBSyxFQUFFLEVBQUU7WUFDVCxRQUFRLEVBQUUsRUFBRTtZQUNaLE1BQU0sRUFBRSxFQUFFO1lBQ1YsWUFBWSxFQUFFLEVBQUU7WUFDaEIsV0FBVyxFQUFFLEVBQUU7WUFDZixrQkFBa0IsRUFBRSxRQUFRO1lBQzVCLGNBQWMsRUFBRSxPQUFPO1lBQ3ZCLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztZQUNqQyxVQUFVLEVBQUUsUUFBUSxDQUFDLGlCQUFpQjtZQUV0QyxLQUFLLEVBQUU7Z0JBQ0wsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsWUFBWSxFQUFFLEtBQUs7YUFDcEI7U0FDRjtRQUVELFFBQVEsRUFBRTtZQUNSLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFlBQVksRUFBRSxDQUFDO1lBQ2YsU0FBUyxFQUFFLE1BQU07U0FDbEI7UUFFRCxNQUFNLEVBQUU7WUFDTixJQUFJLEVBQUUsRUFBRTtZQUNSLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsYUFBYSxFQUFFLFFBQVE7WUFDdkIsUUFBUSxFQUFFLFFBQVE7WUFDbEIsWUFBWSxFQUFFLFVBQVU7WUFDeEIsY0FBYyxFQUFFLFFBQVE7WUFFeEIsUUFBUSxFQUFFO2dCQUNSLFVBQVUsRUFBRSxRQUFRLENBQUMsa0JBQWtCO2dCQUN2QyxZQUFZLEVBQUUsVUFBVTtnQkFDeEIsVUFBVSxFQUFFLFFBQVE7Z0JBQ3BCLFFBQVEsRUFBRSxRQUFRO2dCQUNsQixRQUFRLEVBQUUsRUFBRTtnQkFDWixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsV0FBVyxFQUFFLENBQUM7Z0JBQ2QsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2FBQzNCO1lBRUQsV0FBVyxFQUFFO2dCQUNYLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLFVBQVUsRUFBRSxRQUFRO2dCQUNwQixVQUFVLEVBQUUsTUFBTTtnQkFDaEIsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO2dCQUN2QixNQUFNLEVBQUUsU0FBUzthQUNwQjtTQUNGO1FBRUQsV0FBVyxFQUFFO1lBQ1gsUUFBUSxFQUFFLEVBQUU7WUFDWixRQUFRLEVBQUUsUUFBUTtZQUNsQixVQUFVLEVBQUUsTUFBTTtZQUNsQixTQUFTLEVBQUUsU0FBUztZQUNwQixNQUFNLEVBQUUsRUFBRTtZQUNWLFNBQVMsRUFBRSxFQUFFO1lBQ2IsS0FBSyxFQUFFLE1BQU07WUFDYixLQUFLLEVBQUUsUUFBUSxDQUFDLFlBQVk7WUFDNUIsVUFBVSxFQUFFLFVBQVU7U0FDdkI7S0FDRjtJQUVELElBQUksRUFBRTtRQUNKLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7UUFDcEMsR0FBRyxFQUFFLEtBQUs7UUFDVixJQUFJLEVBQUUsS0FBSztRQUNYLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtRQUMxQixTQUFTLEVBQUUsdUJBQXVCO1FBQ2xDLE1BQU0sRUFBRSxRQUFRLENBQUMsT0FBTztRQUV4QixLQUFLLEVBQUU7WUFDTCxLQUFLLEVBQUUsRUFBRTtZQUNULE1BQU0sRUFBRSxFQUFFO1NBQ1g7S0FDRjtJQUVELFNBQVMsRUFBRTtRQUNULEtBQUssRUFBRSxFQUFFO1FBQ1QsTUFBTSxFQUFFLEVBQUU7UUFDVixJQUFJLEVBQUUsS0FBSztRQUNYLFNBQVMsRUFBRSx1QkFBdUI7UUFDbEMsU0FBUyxFQUFFLGdCQUFjLFFBQVEsQ0FBQyxnQkFBa0I7UUFDcEQsVUFBVSxFQUFFLGdCQUFjLFFBQVEsQ0FBQyxVQUFZO1FBQy9DLFlBQVksRUFBRSxnQkFBYyxRQUFRLENBQUMsZ0JBQWtCO1FBQ3ZELFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7UUFDcEMsU0FBUyxFQUFFLFlBQVk7UUFDdkIsT0FBTyxFQUFFLEdBQUc7UUFDWixHQUFHLEVBQUUsS0FBSztLQUNYO0lBRUQsTUFBTSxFQUFFO1FBQ04sQ0FBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRTtRQUNwQixDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFO1FBQ25CLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDO1FBQ3ZDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDO1FBQ3BDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDO1FBQ3BDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDO0tBQ3hDO0NBQ0ssQ0FBQyJ9
// CONCATENATED MODULE: ./components/item/feedback-testimonial/ItemFeedback.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







;
var ItemFeedback_ItemFeedback = /** @class */ (function (_super) {
    __extends(ItemFeedback, _super);
    function ItemFeedback(props) {
        return _super.call(this, props) || this;
    }
    ItemFeedback.prototype.shouldComponentUpdate = function (nextProps, nextState) {
        var isNullCurrentData = null === this.props.data || 'undefined' === typeof this.props.data;
        var isNullNextData = null !== nextProps.data && 'undefined' !== typeof nextProps.data;
        if (isNullCurrentData && isNullNextData) {
            return true;
        }
        return false;
    };
    ItemFeedback.prototype.render = function () {
        var data = this.props.data;
        var pictureUrl = data && data.picture && data.picture.medium_url || data.picture_url || '';
        var avatar = data && data.user && data.user.avatar && data.user.avatar.medium_url
            || data.user_avatar && data.user_avatar.medium_url
            || '';
        return (react["createElement"](react_router_dom["NavLink"], { style: ItemFeedback_style.container.itemSlider, to: routing["q" /* ROUTING_COMMUNITY_PATH */] + "/" + (data && data.id || 0) },
            react["createElement"]("div", null,
                react["createElement"]("div", { style: ItemFeedback_style.container.itemSliderPanel },
                    react["createElement"]("div", { style: [
                            ItemFeedback_style.container.innerItemSliderPanel(pictureUrl),
                        ] }, data && data.video && !!data.video.url && react["createElement"]("div", { style: ItemFeedback_style.videoIcon }))),
                react["createElement"]("div", { style: ItemFeedback_style.container.info },
                    react["createElement"]("div", { style: ItemFeedback_style.info.container },
                        react["createElement"]("div", { style: [{ backgroundImage: "url(" + avatar + ")" }, ItemFeedback_style.info.avatar] }),
                        react["createElement"]("div", { style: ItemFeedback_style.info.detail },
                            react["createElement"]("div", { style: ItemFeedback_style.info.detail.username }, data && data.user && data.user.name || ''),
                            data && data.created_at
                                && (react["createElement"]("div", { style: ItemFeedback_style.info.detail.ratingGroup }, Object(utils["b" /* convertUnixTime */])(data.created_at, global["b" /* DATETIME_TYPE_FORMAT */].SHORT_DATE))))),
                    react["createElement"]("div", { style: ItemFeedback_style.info.description }, data && data.message || '')))));
    };
    ;
    ItemFeedback = __decorate([
        radium
    ], ItemFeedback);
    return ItemFeedback;
}(react["Component"]));
;
/* harmony default export */ var feedback_testimonial_ItemFeedback = (ItemFeedback_ItemFeedback);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSXRlbUZlZWRiYWNrLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiSXRlbUZlZWRiYWNrLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFDakMsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUVqRCxPQUFPLEtBQUssTUFBTSxzQkFBc0IsQ0FBQztBQU14QyxDQUFDO0FBR0Y7SUFBMkIsZ0NBQXdDO0lBQ2pFLHNCQUFZLEtBQUs7ZUFDZixrQkFBTSxLQUFLLENBQUM7SUFDZCxDQUFDO0lBRUQsNENBQXFCLEdBQXJCLFVBQXNCLFNBQVMsRUFBRSxTQUFTO1FBQ3hDLElBQU0saUJBQWlCLEdBQUcsSUFBSSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLFdBQVcsS0FBSyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO1FBQzdGLElBQU0sY0FBYyxHQUFHLElBQUksS0FBSyxTQUFTLENBQUMsSUFBSSxJQUFJLFdBQVcsS0FBSyxPQUFPLFNBQVMsQ0FBQyxJQUFJLENBQUM7UUFFeEYsRUFBRSxDQUFDLENBQUMsaUJBQWlCLElBQUksY0FBYyxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFBQyxDQUFDO1FBRXpELE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQsNkJBQU0sR0FBTjtRQUNVLElBQUEsc0JBQUksQ0FBZ0I7UUFFNUIsSUFBTSxVQUFVLEdBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxFQUFFLENBQUM7UUFDN0YsSUFBTSxNQUFNLEdBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVTtlQUM5RSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVTtlQUMvQyxFQUFFLENBQUM7UUFFUixNQUFNLENBQUMsQ0FDTCxvQkFBQyxPQUFPLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLEVBQUUsRUFBSyxzQkFBc0IsVUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUU7WUFDakc7Z0JBQ0UsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsZUFBZTtvQkFDekMsNkJBQUssS0FBSyxFQUFFOzRCQUNWLEtBQUssQ0FBQyxTQUFTLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDO3lCQUNqRCxJQUNFLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFNBQVMsR0FBUSxDQUMxRSxDQUNGO2dCQUNOLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUk7b0JBQzlCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVM7d0JBQzlCLDZCQUFLLEtBQUssRUFBRSxDQUFDLEVBQUUsZUFBZSxFQUFFLFNBQU8sTUFBTSxNQUFHLEVBQUMsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFRO3dCQUM3RSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNOzRCQUMzQiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxJQUNuQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxFQUFFLENBQ3RDOzRCQUVKLElBQUksSUFBSSxJQUFJLENBQUMsVUFBVTttQ0FDcEIsQ0FDRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxJQUN0QyxlQUFlLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxvQkFBb0IsQ0FBQyxVQUFVLENBQUMsQ0FDOUQsQ0FDUCxDQUVDLENBQ0Y7b0JBQ04sNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFHLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBTyxDQUNsRSxDQUNGLENBQ0UsQ0FDWCxDQUFDO0lBQ0osQ0FBQztJQUFBLENBQUM7SUF0REUsWUFBWTtRQURqQixNQUFNO09BQ0QsWUFBWSxDQXVEakI7SUFBRCxtQkFBQztDQUFBLEFBdkRELENBQTJCLEtBQUssQ0FBQyxTQUFTLEdBdUR6QztBQUFBLENBQUM7QUFFRixlQUFlLFlBQVksQ0FBQyJ9
// CONCATENATED MODULE: ./components/item/feedback-testimonial/index.tsx

/* harmony default export */ var feedback_testimonial = __webpack_exports__["a"] = (feedback_testimonial_ItemFeedback);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxZQUFZLE1BQU0sZ0JBQWdCLENBQUM7QUFDMUMsZUFBZSxZQUFZLENBQUMifQ==

/***/ }),

/***/ 829:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// CONCATENATED MODULE: ./components/testimonial/initialize.tsx
var DEFAULT_PROPS = {
    list: [],
    isHome: false,
    isSlide: false,
    showHeader: true
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLE1BQU0sRUFBRSxLQUFLO0lBQ2IsT0FBTyxFQUFFLEtBQUs7SUFDZCxVQUFVLEVBQUUsSUFBSTtDQUNQLENBQUMifQ==
// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./container/layout/fade-in/index.tsx
var fade_in = __webpack_require__(756);

// EXTERNAL MODULE: ./container/layout/main-block/index.tsx
var main_block = __webpack_require__(746);

// EXTERNAL MODULE: ./components/general/pagination/index.tsx + 4 modules
var pagination = __webpack_require__(751);

// EXTERNAL MODULE: ./components/item/feedback-testimonial/index.tsx + 2 modules
var feedback_testimonial = __webpack_require__(803);

// EXTERNAL MODULE: ./components/ui/loading-placeholder/index.tsx + 4 modules
var loading_placeholder = __webpack_require__(750);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./style/media-queries.ts
var media_queries = __webpack_require__(76);

// CONCATENATED MODULE: ./components/testimonial/style.tsx




/* harmony default export */ var testimonial_style = ({
    display: 'block',
    position: 'relative',
    zIndex: variable["zIndex5"],
    paddingTop: 20,
    groupProductVideomagazine: [
        layout["b" /* splitContainer */],
        layout["a" /* flexContainer */].justify, {
            marginBottom: 20,
        }
    ],
    feedContainer: {
        paddingTop: '30%',
        position: variable["position"].relative,
        panel: {
            position: variable["position"].absolute,
            top: 0,
            left: 0,
            height: '100%',
            width: '100%',
            overflowX: 'hidden',
            overflowY: 'auto',
            feed: {
                paddingTop: 1,
                paddingRight: 1,
                paddingBottom: 1,
                paddingLeft: 1,
            }
        }
    },
    mainBanner: {
        marginBottom: 20
    },
    list: function (isSlide) {
        if (isSlide === void 0) { isSlide = false; }
        var general = {
            display: variable["display"].flex,
            flexWrap: isSlide ? 'nowrap' : 'wrap',
            paddingTop: 20,
            paddingRight: 0,
            paddingBottom: 10,
            paddingLeft: 0,
            position: variable["position"].relative,
            overflowX: isSlide ? 'auto' : ''
        };
        var mobile =  true ? {
            paddingTop: 0,
            paddingRight: isSlide ? 20 : 0,
            paddingBottom: 0,
            paddingLeft: 0,
            marginRight: isSlide ? 5 : -10,
            marginLeft: -10
        } : undefined;
        return Object.assign({}, general, mobile);
    },
    item: function (isSlide) {
        if (isSlide === void 0) { isSlide = false; }
        return Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    marginBottom: 10,
                    paddingLeft: 5,
                    paddingRight: 5,
                    width: isSlide ? '47%' : '50%',
                    minWidth: isSlide ? '47%' : '',
                }],
            DESKTOP: [{
                    marginBottom: 20,
                    paddingLeft: 10,
                    paddingRight: 10,
                    width: '20%',
                }],
            GENERAL: [{ paddingTop: 0, paddingBottom: 0 }]
        });
    },
    videoWrap: {
        height: '100%'
    },
    customStyleLoading: {
        height: 300
    },
    placeholder: {
        width: '100%',
        title: {
            background: variable["colorF0"],
            display: variable["display"].block,
            width: 320,
            height: 56,
            margin: '0 auto 10px'
        },
        titleMobile: {
            margin: 0,
            textAlign: 'left'
        },
        productList: {
            flexWrap: 'wrap',
            display: variable["display"].flex,
            marginLeft: -10,
            marginRight: -10
        },
        productMobileItem: {
            minWidth: '50%',
            width: '50%',
        },
        productItem: {
            flex: 1,
            paddingLeft: 10,
            paddingRight: 10,
            marginBottom: 20,
            minWidth: '20%',
            width: '20%',
            height: 303,
            image: {
                width: '100%',
                height: '100%'
            },
            text: {
                width: '94%',
                height: 25,
                marginBottom: 10,
            },
            lastText: {
                width: '65%',
                height: 25,
            }
        },
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3RELE9BQU8sS0FBSyxNQUFNLE1BQU0sb0JBQW9CLENBQUM7QUFDN0MsT0FBTyxLQUFLLFFBQVEsTUFBTSxzQkFBc0IsQ0FBQztBQUNqRCxPQUFPLGFBQWEsTUFBTSwyQkFBMkIsQ0FBQztBQUV0RCxlQUFlO0lBQ2IsT0FBTyxFQUFFLE9BQU87SUFDaEIsUUFBUSxFQUFFLFVBQVU7SUFDcEIsTUFBTSxFQUFFLFFBQVEsQ0FBQyxPQUFPO0lBQ3hCLFVBQVUsRUFBRSxFQUFFO0lBRWQseUJBQXlCLEVBQUU7UUFDekIsTUFBTSxDQUFDLGNBQWM7UUFDckIsTUFBTSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUU7WUFDNUIsWUFBWSxFQUFFLEVBQUU7U0FDakI7S0FDRjtJQUVELGFBQWEsRUFBRTtRQUNiLFVBQVUsRUFBRSxLQUFLO1FBQ2pCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7UUFFcEMsS0FBSyxFQUFFO1lBQ0wsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxHQUFHLEVBQUUsQ0FBQztZQUNOLElBQUksRUFBRSxDQUFDO1lBQ1AsTUFBTSxFQUFFLE1BQU07WUFDZCxLQUFLLEVBQUUsTUFBTTtZQUNiLFNBQVMsRUFBRSxRQUFRO1lBQ25CLFNBQVMsRUFBRSxNQUFNO1lBRWpCLElBQUksRUFBRTtnQkFDSixVQUFVLEVBQUUsQ0FBQztnQkFDYixZQUFZLEVBQUUsQ0FBQztnQkFDZixhQUFhLEVBQUUsQ0FBQztnQkFDaEIsV0FBVyxFQUFFLENBQUM7YUFDZjtTQUNGO0tBQ0Y7SUFFRCxVQUFVLEVBQUU7UUFDVixZQUFZLEVBQUUsRUFBRTtLQUNqQjtJQUVELElBQUksRUFBRSxVQUFDLE9BQWU7UUFBZix3QkFBQSxFQUFBLGVBQWU7UUFDcEIsSUFBTSxPQUFPLEdBQUc7WUFDZCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsTUFBTTtZQUNyQyxVQUFVLEVBQUUsRUFBRTtZQUNkLFlBQVksRUFBRSxDQUFDO1lBQ2YsYUFBYSxFQUFFLEVBQUU7WUFDakIsV0FBVyxFQUFFLENBQUM7WUFDZCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1lBQ3BDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRTtTQUNqQyxDQUFBO1FBRUQsSUFBTSxNQUFNLEdBQUcsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pDLFVBQVUsRUFBRSxDQUFDO1lBQ2IsWUFBWSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlCLGFBQWEsRUFBRSxDQUFDO1lBQ2hCLFdBQVcsRUFBRSxDQUFDO1lBQ2QsV0FBVyxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDOUIsVUFBVSxFQUFFLENBQUMsRUFBRTtTQUNoQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUE7UUFFTixNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFRCxJQUFJLEVBQUUsVUFBQyxPQUFlO1FBQWYsd0JBQUEsRUFBQSxlQUFlO1FBQUssT0FBQSxZQUFZLENBQUM7WUFDdEMsTUFBTSxFQUFFLENBQUM7b0JBQ1AsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLFdBQVcsRUFBRSxDQUFDO29CQUNkLFlBQVksRUFBRSxDQUFDO29CQUNmLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSztvQkFDOUIsUUFBUSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFO2lCQUMvQixDQUFDO1lBRUYsT0FBTyxFQUFFLENBQUM7b0JBQ1IsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLFdBQVcsRUFBRSxFQUFFO29CQUNmLFlBQVksRUFBRSxFQUFFO29CQUNoQixLQUFLLEVBQUUsS0FBSztpQkFDYixDQUFDO1lBRUYsT0FBTyxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsQ0FBQyxFQUFFLGFBQWEsRUFBRSxDQUFDLEVBQUUsQ0FBQztTQUMvQyxDQUFDO0lBakJ5QixDQWlCekI7SUFFRixTQUFTLEVBQUU7UUFDVCxNQUFNLEVBQUUsTUFBTTtLQUNmO0lBRUQsa0JBQWtCLEVBQUU7UUFDbEIsTUFBTSxFQUFFLEdBQUc7S0FDWjtJQUVELFdBQVcsRUFBRTtRQUNYLEtBQUssRUFBRSxNQUFNO1FBRWIsS0FBSyxFQUFFO1lBQ0wsVUFBVSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1lBQzVCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDL0IsS0FBSyxFQUFFLEdBQUc7WUFDVixNQUFNLEVBQUUsRUFBRTtZQUNWLE1BQU0sRUFBRSxhQUFhO1NBQ3RCO1FBRUQsV0FBVyxFQUFFO1lBQ1gsTUFBTSxFQUFFLENBQUM7WUFDVCxTQUFTLEVBQUUsTUFBTTtTQUNsQjtRQUVELFdBQVcsRUFBRTtZQUNYLFFBQVEsRUFBRSxNQUFNO1lBQ2hCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsVUFBVSxFQUFFLENBQUMsRUFBRTtZQUNmLFdBQVcsRUFBRSxDQUFDLEVBQUU7U0FDakI7UUFFRCxpQkFBaUIsRUFBRTtZQUNqQixRQUFRLEVBQUUsS0FBSztZQUNmLEtBQUssRUFBRSxLQUFLO1NBQ2I7UUFFRCxXQUFXLEVBQUU7WUFDWCxJQUFJLEVBQUUsQ0FBQztZQUNQLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLEVBQUU7WUFDaEIsWUFBWSxFQUFFLEVBQUU7WUFDaEIsUUFBUSxFQUFFLEtBQUs7WUFDZixLQUFLLEVBQUUsS0FBSztZQUNaLE1BQU0sRUFBRSxHQUFHO1lBRVgsS0FBSyxFQUFFO2dCQUNMLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxNQUFNO2FBQ2Y7WUFFRCxJQUFJLEVBQUU7Z0JBQ0osS0FBSyxFQUFFLEtBQUs7Z0JBQ1osTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsWUFBWSxFQUFFLEVBQUU7YUFDakI7WUFFRCxRQUFRLEVBQUU7Z0JBQ1IsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osTUFBTSxFQUFFLEVBQUU7YUFDWDtTQUNGO0tBQ0Y7Q0FDSyxDQUFDIn0=
// CONCATENATED MODULE: ./components/testimonial/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};








var renderItemPlaceholder = function (item) { return (react["createElement"]("div", { style: [
        testimonial_style.placeholder.productItem,
        'MOBILE' === window.DEVICE_VERSION && testimonial_style.placeholder.productMobileItem
    ], key: item },
    react["createElement"](loading_placeholder["a" /* default */], { style: testimonial_style.placeholder.productItem.image }))); };
var renderLoadingPlaceholder = function (isHome) {
    if (isHome === void 0) { isHome = false; }
    var list = isHome || 'MOBILE' === window.DEVICE_VERSION ? [1, 2, 3, 4, 5] : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
    return (react["createElement"]("div", { style: testimonial_style.placeholder },
        react["createElement"](loading_placeholder["a" /* default */], { style: [testimonial_style.placeholder.title, 'MOBILE' === window.DEVICE_VERSION && testimonial_style.placeholder.titleMobile] }),
        react["createElement"]("div", { style: testimonial_style.placeholder.productList }, Array.isArray(list) && list.map(renderItemPlaceholder))));
};
var renderItems = function (_a) {
    var list = _a.list, openModal = _a.openModal, style = _a.style;
    return Array.isArray(list)
        && list.map(function (feedback) {
            return react["createElement"]("div", { key: feedback.id, style: style },
                react["createElement"](feedback_testimonial["a" /* default */], { data: feedback, openModal: openModal }));
        });
};
var renderView = function (props) {
    var per = props.per, list = props.list, total = props.total, isHome = props.isHome, current = props.current, isSlide = props.isSlide, urlList = props.urlList, openModal = props.openModal, showHeader = props.showHeader;
    var paginationProps = {
        per: per,
        total: total,
        current: current,
        urlList: urlList
    };
    var mainBlockProps = {
        showHeader: showHeader,
        title: showHeader ? 'Nhận xét về Lixibox' : '',
        viewMoreText: 'Xem thêm',
        viewMoreLink: routing["q" /* ROUTING_COMMUNITY_PATH */],
        showViewMore: isHome,
        content: 0 === list.length
            ? null
            : isHome
                ? (react["createElement"]("div", { style: testimonial_style.list(isSlide) }, renderItems({ list: list.slice(0, 5), style: testimonial_style.item(isSlide), openModal: openModal })))
                : (react["createElement"]("div", null,
                    react["createElement"](fade_in["a" /* default */], { itemStyle: testimonial_style.item(isSlide), style: testimonial_style.list(isSlide) }, renderItems({ list: list, style: testimonial_style.videoWrap, openModal: openModal })),
                    react["createElement"](pagination["a" /* default */], __assign({}, paginationProps)))),
        style: {}
    };
    return (react["createElement"]("summary-testimonial-list", { style: testimonial_style }, Array.isArray(list) && 0 === list.length ? renderLoadingPlaceholder(isHome) : react["createElement"](main_block["a" /* default */], __assign({}, mainBlockProps))));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFFN0UsT0FBTyxNQUFNLE1BQU0sZ0NBQWdDLENBQUM7QUFDcEQsT0FBTyxTQUFTLE1BQU0sbUNBQW1DLENBQUM7QUFDMUQsT0FBTyxVQUFVLE1BQU0sdUJBQXVCLENBQUM7QUFDL0MsT0FBTyxlQUFlLE1BQU0sOEJBQThCLENBQUM7QUFDM0QsT0FBTyxrQkFBa0IsTUFBTSwyQkFBMkIsQ0FBQztBQUczRCxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQUcsVUFBQyxJQUFJLElBQUssT0FBQSxDQUM3Qyw2QkFDRSxLQUFLLEVBQUU7UUFDTCxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVc7UUFDN0IsUUFBUSxLQUFLLE1BQU0sQ0FBQyxjQUFjLElBQUksS0FBSyxDQUFDLFdBQVcsQ0FBQyxpQkFBaUI7S0FDMUUsRUFDRCxHQUFHLEVBQUUsSUFBSTtJQUNULG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEdBQUksQ0FDOUQsQ0FDUCxFQVQ4QyxDQVM5QyxDQUFDO0FBR0YsSUFBTSx3QkFBd0IsR0FBRyxVQUFDLE1BQWM7SUFBZCx1QkFBQSxFQUFBLGNBQWM7SUFDOUMsSUFBTSxJQUFJLEdBQUcsTUFBTSxJQUFJLFFBQVEsS0FBSyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ2xJLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVztRQUMzQixvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxRQUFRLEtBQUssTUFBTSxDQUFDLGNBQWMsSUFBSSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxHQUFJO1FBQzdILDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsSUFDdEMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQ25ELENBQ0YsQ0FDUCxDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsSUFBTSxXQUFXLEdBQUcsVUFBQyxFQUEwQjtRQUF4QixjQUFJLEVBQUUsd0JBQVMsRUFBRSxnQkFBSztJQUFPLE9BQUEsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7V0FDbEUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFDLFFBQVE7WUFDbkIsT0FBQSw2QkFDRSxHQUFHLEVBQUUsUUFBUSxDQUFDLEVBQUUsRUFDaEIsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osb0JBQUMsZUFBZSxJQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLFNBQVMsR0FBSSxDQUNyRDtRQUpOLENBSU0sQ0FDUDtBQVBpRCxDQU9qRCxDQUFDO0FBRUosSUFBTSxVQUFVLEdBQUcsVUFBQyxLQUFhO0lBRTdCLElBQUEsZUFBRyxFQUNILGlCQUFJLEVBQ0osbUJBQUssRUFDTCxxQkFBTSxFQUNOLHVCQUFPLEVBQ1AsdUJBQU8sRUFDUCx1QkFBTyxFQUNQLDJCQUFTLEVBQ1QsNkJBQVUsQ0FDRjtJQUVWLElBQU0sZUFBZSxHQUFHO1FBQ3RCLEdBQUcsS0FBQTtRQUNILEtBQUssT0FBQTtRQUNMLE9BQU8sU0FBQTtRQUNQLE9BQU8sU0FBQTtLQUNSLENBQUM7SUFFRixJQUFNLGNBQWMsR0FBRztRQUNyQixVQUFVLFlBQUE7UUFDVixLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsRUFBRTtRQUM5QyxZQUFZLEVBQUUsVUFBVTtRQUN4QixZQUFZLEVBQUUsc0JBQXNCO1FBQ3BDLFlBQVksRUFBRSxNQUFNO1FBQ3BCLE9BQU8sRUFDTCxDQUFDLEtBQUssSUFBSSxDQUFDLE1BQU07WUFDZixDQUFDLENBQUMsSUFBSTtZQUNOLENBQUMsQ0FBQyxNQUFNO2dCQUNOLENBQUMsQ0FBQyxDQUNBLDZCQUNFLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUN6QixXQUFXLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsU0FBUyxXQUFBLEVBQUUsQ0FBQyxDQUMxRSxDQUNSO2dCQUNELENBQUMsQ0FBQyxDQUNBO29CQUNFLG9CQUFDLE1BQU0sSUFDTCxTQUFTLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFDOUIsS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQ3pCLFdBQVcsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxFQUFFLFNBQVMsV0FBQSxFQUFFLENBQUMsQ0FDbEQ7b0JBQ1Qsb0JBQUMsVUFBVSxlQUFLLGVBQWUsRUFBSSxDQUMvQixDQUNQO1FBQ1AsS0FBSyxFQUFFLEVBQUU7S0FDVixDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsa0RBQTBCLEtBQUssRUFBRSxLQUFLLElBQ25DLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLHdCQUF3QixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxvQkFBQyxTQUFTLGVBQUssY0FBYyxFQUFJLENBQ3ZGLENBQzVCLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./components/testimonial/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_TestimonialComponent = /** @class */ (function (_super) {
    __extends(TestimonialComponent, _super);
    function TestimonialComponent(props) {
        return _super.call(this, props) || this;
    }
    TestimonialComponent.prototype.shouldComponentUpdate = function (nextProps) {
        if (this.props.list.length !== nextProps.list.length) {
            return true;
        }
        return false;
    };
    TestimonialComponent.prototype.render = function () {
        return view(this.props);
    };
    TestimonialComponent.defaultProps = DEFAULT_PROPS;
    TestimonialComponent = __decorate([
        radium
    ], TestimonialComponent);
    return TestimonialComponent;
}(react["Component"]));
;
/* harmony default export */ var component = (component_TestimonialComponent);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFHakMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUM3QyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFHaEM7SUFBbUMsd0NBQTRCO0lBRzdELDhCQUFZLEtBQWE7ZUFDdkIsa0JBQU0sS0FBSyxDQUFDO0lBQ2QsQ0FBQztJQUVELG9EQUFxQixHQUFyQixVQUFzQixTQUFpQjtRQUNyQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUFDLENBQUM7UUFFdEUsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFRCxxQ0FBTSxHQUFOO1FBQ0UsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQWRNLGlDQUFZLEdBQVcsYUFBYSxDQUFDO0lBRHhDLG9CQUFvQjtRQUR6QixNQUFNO09BQ0Qsb0JBQW9CLENBZ0J6QjtJQUFELDJCQUFDO0NBQUEsQUFoQkQsQ0FBbUMsS0FBSyxDQUFDLFNBQVMsR0FnQmpEO0FBQUEsQ0FBQztBQUVGLGVBQWUsb0JBQW9CLENBQUMifQ==
// CONCATENATED MODULE: ./components/testimonial/index.tsx

/* harmony default export */ var testimonial = __webpack_exports__["a"] = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxvQkFBb0IsTUFBTSxhQUFhLENBQUM7QUFDL0MsZUFBZSxvQkFBb0IsQ0FBQyJ9

/***/ })

}]);