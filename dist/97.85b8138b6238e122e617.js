(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[97],{

/***/ 771:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// CONCATENATED MODULE: ./container/layout/split/initialize.tsx
var DEFAULT_PROPS = {
    type: 'left',
    size: 'normal',
    isWithBorderSplit: true,
    style: {}
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsTUFBTTtJQUNaLElBQUksRUFBRSxRQUFRO0lBQ2QsaUJBQWlCLEVBQUUsSUFBSTtJQUN2QixLQUFLLEVBQUUsRUFBRTtDQUNBLENBQUMifQ==
// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./container/layout/split/style.tsx

/* harmony default export */ var split_style = ({
    display: 'block',
    position: 'relative',
    zIndex: variable["zIndex5"],
    layout: {
        left: { borderRight: "1px solid " + variable["colorF0"] },
        leftWithOutBorderSplit: { borderRight: "0px solid " + variable["colorF0"] },
        right: { borderLeft: "1px solid " + variable["colorF0"] },
        rightWithOutBorderSplit: { borderLeft: "0px solid " + variable["colorF0"] },
    },
    customStyleLoading: {
        height: 400
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxlQUFlO0lBQ2IsT0FBTyxFQUFFLE9BQU87SUFDaEIsUUFBUSxFQUFFLFVBQVU7SUFDcEIsTUFBTSxFQUFFLFFBQVEsQ0FBQyxPQUFPO0lBRXhCLE1BQU0sRUFBRTtRQUNOLElBQUksRUFBRSxFQUFFLFdBQVcsRUFBRSxlQUFhLFFBQVEsQ0FBQyxPQUFTLEVBQUU7UUFDdEQsc0JBQXNCLEVBQUUsRUFBRSxXQUFXLEVBQUUsZUFBYSxRQUFRLENBQUMsT0FBUyxFQUFFO1FBRXhFLEtBQUssRUFBRSxFQUFFLFVBQVUsRUFBRSxlQUFhLFFBQVEsQ0FBQyxPQUFTLEVBQUU7UUFDdEQsdUJBQXVCLEVBQUUsRUFBRSxVQUFVLEVBQUUsZUFBYSxRQUFRLENBQUMsT0FBUyxFQUFFO0tBQ3pFO0lBRUQsa0JBQWtCLEVBQUU7UUFDbEIsTUFBTSxFQUFFLEdBQUc7S0FDWjtDQUNGLENBQUMifQ==
// CONCATENATED MODULE: ./container/layout/split/view.tsx



var renderView = function (props) {
    var subContainer = props.subContainer, mainContainer = props.mainContainer, type = props.type, _a = props.size, size = _a === void 0 ? 'normal' : _a, style = props.style, _b = props.isWithBorderSplit, isWithBorderSplit = _b === void 0 ? true : _b;
    var subLeftSize = {
        normal: layout["b" /* splitContainer */].left,
        larger: layout["b" /* splitContainer */].leftLarger
    };
    var subRightSize = {
        normal: layout["b" /* splitContainer */].right,
        larger: layout["b" /* splitContainer */].rightLarger
    };
    var mainSize = {
        normal: layout["b" /* splitContainer */].main,
        larger: layout["b" /* splitContainer */].mainSmaller,
    };
    var containerStyle = [layout["b" /* splitContainer */], layout["a" /* flexContainer */].justify, style];
    var leftStyle = [subLeftSize[size], split_style.layout.left, isWithBorderSplit && split_style.layout.leftWithOutBorderSplit];
    var rightStyle = [subLeftSize[size], split_style.layout.right, isWithBorderSplit && split_style.layout.rightWithOutBorderSplit];
    return (react["createElement"]("div", { style: containerStyle },
        'left' === type && react["createElement"]("div", { style: leftStyle }, subContainer),
        react["createElement"]("div", { style: mainSize[size] }, mainContainer),
        'right' === type && react["createElement"]("div", { style: rightStyle }, subContainer)));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRS9CLE9BQU8sS0FBSyxNQUFNLE1BQU0sdUJBQXVCLENBQUM7QUFFaEQsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRTVCLElBQU0sVUFBVSxHQUFHLFVBQUMsS0FBYTtJQUN2QixJQUFBLGlDQUFZLEVBQUUsbUNBQWEsRUFBRSxpQkFBSSxFQUFFLGVBQWUsRUFBZixvQ0FBZSxFQUFFLG1CQUFLLEVBQUUsNEJBQXdCLEVBQXhCLDZDQUF3QixDQUFXO0lBQ3RHLElBQU0sV0FBVyxHQUFHO1FBQ2xCLE1BQU0sRUFBRSxNQUFNLENBQUMsY0FBYyxDQUFDLElBQUk7UUFDbEMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxjQUFjLENBQUMsVUFBVTtLQUN6QyxDQUFDO0lBRUYsSUFBTSxZQUFZLEdBQUc7UUFDbkIsTUFBTSxFQUFFLE1BQU0sQ0FBQyxjQUFjLENBQUMsS0FBSztRQUNuQyxNQUFNLEVBQUUsTUFBTSxDQUFDLGNBQWMsQ0FBQyxXQUFXO0tBQzFDLENBQUM7SUFFRixJQUFNLFFBQVEsR0FBRztRQUNmLE1BQU0sRUFBRSxNQUFNLENBQUMsY0FBYyxDQUFDLElBQUk7UUFDbEMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxjQUFjLENBQUMsV0FBVztLQUMxQyxDQUFDO0lBRUYsSUFBTSxjQUFjLEdBQUcsQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLE1BQU0sQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3BGLElBQU0sU0FBUyxHQUFHLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLGlCQUFpQixJQUFJLEtBQUssQ0FBQyxNQUFNLENBQUMsc0JBQXNCLENBQUMsQ0FBQztJQUNuSCxJQUFNLFVBQVUsR0FBRyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxpQkFBaUIsSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDLENBQUM7SUFHdEgsTUFBTSxDQUFDLENBQ0wsNkJBQUssS0FBSyxFQUFFLGNBQWM7UUFDdkIsTUFBTSxLQUFLLElBQUksSUFBSSw2QkFBSyxLQUFLLEVBQUUsU0FBUyxJQUFHLFlBQVksQ0FBTztRQUMvRCw2QkFBSyxLQUFLLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFHLGFBQWEsQ0FBTztRQUNoRCxPQUFPLEtBQUssSUFBSSxJQUFJLDZCQUFLLEtBQUssRUFBRSxVQUFVLElBQUcsWUFBWSxDQUFPLENBQzdELENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./container/layout/split/container.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var container_SplitLayout = /** @class */ (function (_super) {
    __extends(SplitLayout, _super);
    function SplitLayout(props) {
        return _super.call(this, props) || this;
    }
    SplitLayout.prototype.render = function () {
        return view(this.props);
    };
    ;
    SplitLayout.defaultProps = DEFAULT_PROPS;
    SplitLayout = __decorate([
        radium
    ], SplitLayout);
    return SplitLayout;
}(react["PureComponent"]));
/* harmony default export */ var container = __webpack_exports__["default"] = (container_SplitLayout);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQzdDLE9BQU8sVUFBVSxNQUFNLFFBQVEsQ0FBQztBQUloQztJQUEwQiwrQkFBNkI7SUFHckQscUJBQVksS0FBYTtlQUN2QixrQkFBTSxLQUFLLENBQUM7SUFDZCxDQUFDO0lBRUQsNEJBQU0sR0FBTjtRQUNFLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFBQSxDQUFDO0lBUkssd0JBQVksR0FBVyxhQUFhLENBQUM7SUFEeEMsV0FBVztRQURoQixNQUFNO09BQ0QsV0FBVyxDQVVoQjtJQUFELGtCQUFDO0NBQUEsQUFWRCxDQUEwQixhQUFhLEdBVXRDO0FBRUQsZUFBZSxXQUFXLENBQUMifQ==

/***/ })

}]);