(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[91],{

/***/ 885:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./action/alert.ts
var action_alert = __webpack_require__(16);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./constants/application/alert.ts
var application_alert = __webpack_require__(15);

// CONCATENATED MODULE: ./container/alert/initialize.tsx
var DEFAULT_PROPS = {
    alertStore: { list: [] },
    closeAlert: function () { }
};
var INITIAL_STATE = {
    waitingClose: [],
    waitingShow: []
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixVQUFVLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFO0lBQ3hCLFVBQVUsRUFBRSxjQUFRLENBQUM7Q0FDdEIsQ0FBQztBQUVGLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixZQUFZLEVBQUUsRUFBRTtJQUNoQixXQUFXLEVBQUUsRUFBRTtDQUNOLENBQUMifQ==
// EXTERNAL MODULE: ./action/cart.ts + 1 modules
var cart = __webpack_require__(83);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./container/alert/style.tsx



/* harmony default export */ var style = ({
    container: function (isShowCart) { return Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                maxWidth: "calc(100% - 20px)",
                top: 10,
                right: 10 + (isShowCart ? 414 : 0),
            }],
        DESKTOP: [{
                maxWidth: "calc(100% - 40px)",
                top: 20,
                right: 20 + (isShowCart ? 414 : 0),
            }],
        GENERAL: [{
                transition: variable["transitionNormal"],
                display: variable["display"].block,
                position: variable["position"].fixed,
                width: 400,
                height: 'auto',
                zIndex: variable["zIndexMax"],
            }]
    }); },
    show: {
        visibility: variable["visible"].visible,
    },
    item: function (type, isRemove, isShow) {
        if (type === void 0) { type = 'DEFAULT'; }
        if (isRemove === void 0) { isRemove = false; }
        if (isShow === void 0) { isShow = false; }
        var borderColor = {
            DEFAULT: variable["colorBlack"],
            WARNING: variable["colorYellow"],
            SUCCESS: variable["colorGreen"],
            ERROR: variable["colorRed"],
        };
        return [
            layout["a" /* flexContainer */].justify,
            layout["a" /* flexContainer */].verticalCenter, {
                transition: variable["transitionNormal"],
                background: variable["colorWhite"],
                marginBottom: 20,
                boxShadow: variable["shadow3"],
                paddingTop: 10,
                paddingBottom: 10,
                borderLeft: "5px solid " + borderColor[type],
            },
        ];
    },
    icon: function (type) {
        if (type === void 0) { type = 'DEFAULT'; }
        var iconColor = {
            DEFAULT: variable["colorBlack"],
            WARNING: variable["colorYellow"],
            SUCCESS: variable["colorGreen"],
            ERROR: variable["colorRed"],
        };
        return {
            flex: 1,
            width: 50,
            minWidth: 50,
            height: 50,
            color: iconColor[type],
            marginRight: 10,
            marginLeft: 10,
        };
    },
    iconText: function (type) {
        var iconColor = {
            DEFAULT: variable["colorBlack"],
            WARNING: variable["colorYellow"],
            SUCCESS: variable["colorGreen"],
            ERROR: variable["colorRed"],
        };
        return {
            width: 50,
            minWidth: 50,
            maxWidth: 50,
            height: 50,
            borderRadius: '50%',
            marginRight: 10,
            marginLeft: 10,
            background: iconColor[type],
            color: variable["colorWhite"],
            lineHeight: '50px',
            textAlign: 'center',
            fontSize: 22,
            fontFamily: variable["fontAvenirDemiBold"]
        };
    },
    iconInner: {
        width: 26,
        height: 26
    },
    info: {
        flex: 10,
        title: {
            fontFamily: variable["fontAvenirDemiBold"],
            lineHeight: '18px',
            fontSize: 16,
            color: variable["colorBlack"],
            marginBottom: 5,
        },
        content: {
            fontSize: 14,
            lineHeight: '18px',
            color: variable["color75"],
        }
    },
    iconClose: {
        cursor: 'pointer',
        flex: 1,
        width: 50,
        minWidth: 50,
        height: 50,
        color: variable["colorBlack"],
        inner: {
            width: 14,
            height: 14
        }
    },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3RELE9BQU8sS0FBSyxNQUFNLE1BQU0sb0JBQW9CLENBQUM7QUFDN0MsT0FBTyxLQUFLLFFBQVEsTUFBTSxzQkFBc0IsQ0FBQztBQUVqRCxlQUFlO0lBQ2IsU0FBUyxFQUFFLFVBQUMsVUFBbUIsSUFBSyxPQUFBLFlBQVksQ0FBQztRQUMvQyxNQUFNLEVBQUUsQ0FBQztnQkFDUCxRQUFRLEVBQUUsbUJBQW1CO2dCQUM3QixHQUFHLEVBQUUsRUFBRTtnQkFDUCxLQUFLLEVBQUUsRUFBRSxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNuQyxDQUFDO1FBRUYsT0FBTyxFQUFFLENBQUM7Z0JBQ1IsUUFBUSxFQUFFLG1CQUFtQjtnQkFDN0IsR0FBRyxFQUFFLEVBQUU7Z0JBQ1AsS0FBSyxFQUFFLEVBQUUsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDbkMsQ0FBQztRQUVGLE9BQU8sRUFBRSxDQUFDO2dCQUNSLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO2dCQUNyQyxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO2dCQUMvQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLO2dCQUNqQyxLQUFLLEVBQUUsR0FBRztnQkFDVixNQUFNLEVBQUUsTUFBTTtnQkFDZCxNQUFNLEVBQUUsUUFBUSxDQUFDLFNBQVM7YUFDM0IsQ0FBQztLQUNILENBQUMsRUFyQmtDLENBcUJsQztJQUdGLElBQUksRUFBRTtRQUNKLFVBQVUsRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU87S0FDckM7SUFFRCxJQUFJLEVBQUUsVUFBQyxJQUFnQixFQUFFLFFBQWdCLEVBQUUsTUFBYztRQUFsRCxxQkFBQSxFQUFBLGdCQUFnQjtRQUFFLHlCQUFBLEVBQUEsZ0JBQWdCO1FBQUUsdUJBQUEsRUFBQSxjQUFjO1FBQ3ZELElBQU0sV0FBVyxHQUFHO1lBQ2xCLE9BQU8sRUFBRSxRQUFRLENBQUMsVUFBVTtZQUM1QixPQUFPLEVBQUUsUUFBUSxDQUFDLFdBQVc7WUFDN0IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1lBQzVCLEtBQUssRUFBRSxRQUFRLENBQUMsUUFBUTtTQUN6QixDQUFDO1FBRUYsTUFBTSxDQUFDO1lBQ0wsTUFBTSxDQUFDLGFBQWEsQ0FBQyxPQUFPO1lBQzVCLE1BQU0sQ0FBQyxhQUFhLENBQUMsY0FBYyxFQUFFO2dCQUNuQyxVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtnQkFDckMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMvQixZQUFZLEVBQUUsRUFBRTtnQkFDaEIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxPQUFPO2dCQUMzQixVQUFVLEVBQUUsRUFBRTtnQkFDZCxhQUFhLEVBQUUsRUFBRTtnQkFDakIsVUFBVSxFQUFFLGVBQWEsV0FBVyxDQUFDLElBQUksQ0FBRzthQUM3QztTQUNGLENBQUM7SUFDSixDQUFDO0lBRUQsSUFBSSxFQUFFLFVBQUMsSUFBZ0I7UUFBaEIscUJBQUEsRUFBQSxnQkFBZ0I7UUFDckIsSUFBTSxTQUFTLEdBQUc7WUFDaEIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1lBQzVCLE9BQU8sRUFBRSxRQUFRLENBQUMsV0FBVztZQUM3QixPQUFPLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDNUIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxRQUFRO1NBQ3pCLENBQUM7UUFFRixNQUFNLENBQUM7WUFDTCxJQUFJLEVBQUUsQ0FBQztZQUNQLEtBQUssRUFBRSxFQUFFO1lBQ1QsUUFBUSxFQUFFLEVBQUU7WUFDWixNQUFNLEVBQUUsRUFBRTtZQUNWLEtBQUssRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDO1lBQ3RCLFdBQVcsRUFBRSxFQUFFO1lBQ2YsVUFBVSxFQUFFLEVBQUU7U0FDZixDQUFDO0lBQ0osQ0FBQztJQUVELFFBQVEsRUFBRSxVQUFDLElBQUk7UUFDYixJQUFNLFNBQVMsR0FBRztZQUNoQixPQUFPLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDNUIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxXQUFXO1lBQzdCLE9BQU8sRUFBRSxRQUFRLENBQUMsVUFBVTtZQUM1QixLQUFLLEVBQUUsUUFBUSxDQUFDLFFBQVE7U0FDekIsQ0FBQztRQUVGLE1BQU0sQ0FBQztZQUNMLEtBQUssRUFBRSxFQUFFO1lBQ1QsUUFBUSxFQUFFLEVBQUU7WUFDWixRQUFRLEVBQUUsRUFBRTtZQUNaLE1BQU0sRUFBRSxFQUFFO1lBQ1YsWUFBWSxFQUFFLEtBQUs7WUFDbkIsV0FBVyxFQUFFLEVBQUU7WUFDZixVQUFVLEVBQUUsRUFBRTtZQUNkLFVBQVUsRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDO1lBQzNCLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtZQUMxQixVQUFVLEVBQUUsTUFBTTtZQUNsQixTQUFTLEVBQUUsUUFBUTtZQUNuQixRQUFRLEVBQUUsRUFBRTtZQUNaLFVBQVUsRUFBRSxRQUFRLENBQUMsa0JBQWtCO1NBQ3hDLENBQUM7SUFDSixDQUFDO0lBRUQsU0FBUyxFQUFFO1FBQ1QsS0FBSyxFQUFFLEVBQUU7UUFDVCxNQUFNLEVBQUUsRUFBRTtLQUNYO0lBRUQsSUFBSSxFQUFFO1FBQ0osSUFBSSxFQUFFLEVBQUU7UUFFUixLQUFLLEVBQUU7WUFDTCxVQUFVLEVBQUUsUUFBUSxDQUFDLGtCQUFrQjtZQUN2QyxVQUFVLEVBQUUsTUFBTTtZQUNsQixRQUFRLEVBQUUsRUFBRTtZQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtZQUMxQixZQUFZLEVBQUUsQ0FBQztTQUNoQjtRQUVELE9BQU8sRUFBRTtZQUNQLFFBQVEsRUFBRSxFQUFFO1lBQ1osVUFBVSxFQUFFLE1BQU07WUFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO1NBQ3hCO0tBQ0Y7SUFFRCxTQUFTLEVBQUU7UUFDVCxNQUFNLEVBQUUsU0FBUztRQUNqQixJQUFJLEVBQUUsQ0FBQztRQUNQLEtBQUssRUFBRSxFQUFFO1FBQ1QsUUFBUSxFQUFFLEVBQUU7UUFDWixNQUFNLEVBQUUsRUFBRTtRQUNWLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtRQUUxQixLQUFLLEVBQUU7WUFDTCxLQUFLLEVBQUUsRUFBRTtZQUNULE1BQU0sRUFBRSxFQUFFO1NBQ1g7S0FDRjtDQUNGLENBQUMifQ==
// CONCATENATED MODULE: ./container/alert/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};




;
var renderContent = function (_a) {
    var item = _a.item, alertIconProps = _a.alertIconProps;
    return true === item.isShowIconText
        ? react["createElement"]("div", { style: style.iconText(item.type) }, item.iconText)
        : ('' !== item.icon && react["createElement"](icon["a" /* default */], __assign({}, alertIconProps)));
};
var renderView = function (_a) {
    var props = _a.props, state = _a.state, handleCloseAlert = _a.handleCloseAlert;
    var alertStore = props.alertStore;
    var waitingClose = state.waitingClose, waitingShow = state.waitingShow;
    var generateContainerProps = function (item) {
        var isRemove = waitingClose.indexOf(item.id) >= 0;
        var isShow = waitingShow.indexOf(item.id) >= 0;
        return {
            key: "alert-item-" + item.id,
            style: style.item(item.type, isRemove, isShow)
        };
    };
    var generateAlertIconProps = function (item) { return ({
        style: style.icon(item.type),
        innerStyle: style.iconInner,
        name: item.icon,
    }); };
    /** Icon to force reomve alert item */
    var generateCloseIconProps = function (item) { return ({
        onClick: function () { return handleCloseAlert(item.id); },
        innerStyle: style.iconClose.inner,
        style: style.iconClose,
        name: 'close',
    }); };
    return (react["createElement"]("alert-container", { style: style.container(Object(cart["v" /* isShowCartSummaryLayout */])()) }, alertStore
        && Array.isArray(alertStore.list)
        && alertStore.list.map(function (item, index) {
            var containerProps = generateContainerProps(item);
            var alertIconProps = generateAlertIconProps(item);
            var closeIconProps = generateCloseIconProps(item);
            return (index === alertStore.list.length - 1
                && (react["createElement"]("alert-item", __assign({}, containerProps),
                    renderContent({ item: item, alertIconProps: alertIconProps }),
                    react["createElement"]("div", { style: style.info },
                        react["createElement"]("div", { style: style.info.content }, item.content)),
                    react["createElement"](icon["a" /* default */], __assign({}, closeIconProps)))));
        })));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFFNUQsT0FBTyxJQUFJLE1BQU0sMEJBQTBCLENBQUM7QUFFNUMsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBTTNCLENBQUM7QUFFRixJQUFNLGFBQWEsR0FDakIsVUFBQyxFQUF3QjtRQUF0QixjQUFJLEVBQUUsa0NBQWM7SUFBTyxPQUFBLElBQUksS0FBSyxJQUFJLENBQUMsY0FBYztRQUN4RCxDQUFDLENBQUMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFHLElBQUksQ0FBQyxRQUFRLENBQU87UUFDOUQsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyxJQUFJLElBQUksb0JBQUMsSUFBSSxlQUFLLGNBQWMsRUFBSSxDQUFDO0FBRnhCLENBRXdCLENBQUM7QUFFekQsSUFBTSxVQUFVLEdBQUcsVUFBQyxFQUFvRDtRQUFsRCxnQkFBSyxFQUFFLGdCQUFLLEVBQUUsc0NBQWdCO0lBQzFDLElBQUEsNkJBQVUsQ0FBVztJQUNyQixJQUFBLGlDQUFZLEVBQUUsK0JBQVcsQ0FBVztJQUU1QyxJQUFNLHNCQUFzQixHQUFHLFVBQUMsSUFBUztRQUN2QyxJQUFNLFFBQVEsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEQsSUFBTSxNQUFNLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRWpELE1BQU0sQ0FBQztZQUNMLEdBQUcsRUFBRSxnQkFBYyxJQUFJLENBQUMsRUFBSTtZQUM1QixLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxNQUFNLENBQUM7U0FDL0MsQ0FBQztJQUNKLENBQUMsQ0FBQztJQUVGLElBQU0sc0JBQXNCLEdBQUcsVUFBQyxJQUFTLElBQUssT0FBQSxDQUFDO1FBQzdDLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDNUIsVUFBVSxFQUFFLEtBQUssQ0FBQyxTQUFTO1FBQzNCLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtLQUNoQixDQUFDLEVBSjRDLENBSTVDLENBQUM7SUFFSCxzQ0FBc0M7SUFDdEMsSUFBTSxzQkFBc0IsR0FBRyxVQUFDLElBQVMsSUFBSyxPQUFBLENBQUM7UUFDN0MsT0FBTyxFQUFFLGNBQU0sT0FBQSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQXpCLENBQXlCO1FBQ3hDLFVBQVUsRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUs7UUFDakMsS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTO1FBQ3RCLElBQUksRUFBRSxPQUFPO0tBQ2QsQ0FBQyxFQUw0QyxDQUs1QyxDQUFDO0lBRUgsTUFBTSxDQUFDLENBQ0wseUNBQWlCLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLHVCQUF1QixFQUFFLENBQUMsSUFFOUQsVUFBVTtXQUNQLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztXQUM5QixVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLO1lBQ2pDLElBQU0sY0FBYyxHQUFHLHNCQUFzQixDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BELElBQU0sY0FBYyxHQUFHLHNCQUFzQixDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BELElBQU0sY0FBYyxHQUFHLHNCQUFzQixDQUFDLElBQUksQ0FBQyxDQUFDO1lBRXBELE1BQU0sQ0FBQyxDQUNMLEtBQUssS0FBSyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDO21CQUNqQyxDQUNELCtDQUFnQixjQUFjO29CQUMzQixhQUFhLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxjQUFjLGdCQUFBLEVBQUUsQ0FBQztvQkFDeEMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJO3dCQUFFLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sSUFBRyxJQUFJLENBQUMsT0FBTyxDQUFPLENBQU07b0JBQ2xGLG9CQUFDLElBQUksZUFBSyxjQUFjLEVBQUksQ0FDakIsQ0FDZCxDQUNGLENBQUM7UUFDSixDQUFDLENBQUMsQ0FFWSxDQUNuQixDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// CONCATENATED MODULE: ./container/alert/container.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var container_Alert = /** @class */ (function (_super) {
    __extends(Alert, _super);
    function Alert(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    Alert.prototype.componentDidMount = function () {
        this.forceCloseOldMessage();
    };
    Alert.prototype.forceCloseOldMessage = function () {
        var _a = this.props, list = _a.alertStore.list, closeAlert = _a.closeAlert;
        var currentTime = (new Date()).getTime();
        Array.isArray(list)
            && list.map(function (item) {
                var offsetTime = currentTime - item.id;
                offsetTime > 10000 && closeAlert(item.id);
            });
    };
    /**
     * Set automation closing for alert item
     *
     * @param {Array<alertItem>} alertList from nextProps
     *
     * 1. Push new item into waiting show -> display with animation.
     * TIME_OUT_OPEN_ALERT 100ms
     *
     * 2. Push new item into waiting close -> hide with aninmation
     * TIME_OUT_CLOSE_ALERT 6.000ms
     *
     * 3. Clean up list waitingShow/waitingClose by current item
     * 4. Update store to final remove alert item
     */
    Alert.prototype.setAutoClose = function (alertList) {
        var closeAlert = this.props.closeAlert;
        var lastItemIndex = alertList.length - 1;
        setTimeout(function () {
            /** 4. Update store to final remove alert item */
            closeAlert(alertList[lastItemIndex].id);
            var removeFist = true;
            Array.isArray(alertList)
                && alertList.map(function (item) {
                    if (item.id === alertList[lastItemIndex].id) {
                        removeFist = false;
                    }
                    removeFist && closeAlert(item.id);
                });
        }, application_alert["r" /* TIME_OUT_CLOSE_ALERT */] + 620);
    };
    /**
     * Force close alert item by id
     *
     * @param {number} alertId
     *
     * 1. Push new item into waiting close -> hide with aninmation
     * 2. Clean up list waitingShow/waitingClose by current item
     * 3. Update store to final remove alert item
     */
    Alert.prototype.handleCloseAlert = function (alertId) {
        var closeAlert = this.props.closeAlert;
        var waitingClose = this.state.waitingClose;
        /** 1. Push new item into waiting close -> hide with aninmation */
        this.setState({ waitingClose: waitingClose.concat([alertId]) });
        closeAlert(alertId);
    };
    Alert.prototype.componentWillReceiveProps = function (nextProps) {
        /**
         * When receive new item alert item
         * - Set up time to automation closing
         */
        if (nextProps.alertStore.list.length > (this.props.alertStore && this.props.alertStore.list.length || 0)) {
            this.setAutoClose(nextProps.alertStore.list);
        }
    };
    Alert.prototype.shouldComponentUpdate = function (nextProps, nextState) {
        var currentAlertLen = this.props.alertStore && this.props.alertStore.list.length || 0;
        var nextAlertLen = nextProps.alertStore && nextProps.alertStore.list.length || 0;
        if (currentAlertLen !== nextAlertLen) {
            return true;
        }
        return false;
    };
    Alert.prototype.render = function () {
        var renderViewProps = {
            props: this.props,
            state: this.state,
            handleCloseAlert: this.handleCloseAlert.bind(this)
        };
        return view(renderViewProps);
    };
    Alert.defaultProps = DEFAULT_PROPS;
    Alert = __decorate([
        radium
    ], Alert);
    return Alert;
}(react["Component"]));
;
/* harmony default export */ var container = (container_Alert);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDekUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFFNUQsT0FBTyxVQUFVLE1BQU0sUUFBUSxDQUFDO0FBR2hDO0lBQW9CLHlCQUErQjtJQUdqRCxlQUFZLEtBQWE7UUFBekIsWUFDRSxrQkFBTSxLQUFLLENBQUMsU0FFYjtRQURDLEtBQUksQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDOztJQUM3QixDQUFDO0lBRUQsaUNBQWlCLEdBQWpCO1FBQ0UsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7SUFDOUIsQ0FBQztJQUVELG9DQUFvQixHQUFwQjtRQUNRLElBQUEsZUFBaUQsRUFBakMseUJBQUksRUFBSSwwQkFBVSxDQUFnQjtRQUN4RCxJQUFNLFdBQVcsR0FBRyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUUzQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztlQUNkLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJO2dCQUNkLElBQU0sVUFBVSxHQUFHLFdBQVcsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO2dCQUN6QyxVQUFVLEdBQUcsS0FBSyxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDNUMsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7Ozs7Ozs7Ozs7Ozs7T0FhRztJQUNILDRCQUFZLEdBQVosVUFBYSxTQUFTO1FBQ1osSUFBQSxrQ0FBVSxDQUEwQjtRQUM1QyxJQUFNLGFBQWEsR0FBRyxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUUzQyxVQUFVLENBQUM7WUFDVCxpREFBaUQ7WUFDakQsVUFBVSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUN4QyxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdEIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUM7bUJBQ25CLFNBQVMsQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJO29CQUNuQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLFNBQVMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO3dCQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7b0JBQUMsQ0FBQztvQkFDcEUsVUFBVSxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3BDLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxFQUFFLG9CQUFvQixHQUFHLEdBQUcsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFRDs7Ozs7Ozs7T0FRRztJQUNILGdDQUFnQixHQUFoQixVQUFpQixPQUFPO1FBQ2QsSUFBQSxrQ0FBVSxDQUFnQjtRQUMxQixJQUFBLHNDQUFZLENBQTBCO1FBRTlDLGtFQUFrRTtRQUNsRSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsWUFBWSxFQUFNLFlBQVksU0FBRSxPQUFPLEVBQUMsRUFBWSxDQUFDLENBQUM7UUFDdEUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3RCLENBQUM7SUFFRCx5Q0FBeUIsR0FBekIsVUFBMEIsU0FBUztRQUNqQzs7O1dBR0c7UUFDSCxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN6RyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0MsQ0FBQztJQUNILENBQUM7SUFFRCxxQ0FBcUIsR0FBckIsVUFBc0IsU0FBaUIsRUFBRSxTQUFpQjtRQUN4RCxJQUFNLGVBQWUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQztRQUN4RixJQUFNLFlBQVksR0FBRyxTQUFTLENBQUMsVUFBVSxJQUFJLFNBQVMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUM7UUFDbkYsRUFBRSxDQUFDLENBQUMsZUFBZSxLQUFLLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQUMsQ0FBQztRQUV0RCxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVELHNCQUFNLEdBQU47UUFDRSxJQUFNLGVBQWUsR0FBRztZQUN0QixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLGdCQUFnQixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ25ELENBQUM7UUFFRixNQUFNLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFoR00sa0JBQVksR0FBRyxhQUFhLENBQUM7SUFEaEMsS0FBSztRQURWLE1BQU07T0FDRCxLQUFLLENBa0dWO0lBQUQsWUFBQztDQUFBLEFBbEdELENBQW9CLEtBQUssQ0FBQyxTQUFTLEdBa0dsQztBQUFBLENBQUM7QUFFRixlQUFlLEtBQUssQ0FBQyJ9
// CONCATENATED MODULE: ./container/alert/store.tsx
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapStateToProps", function() { return mapStateToProps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapDispatchToProps", function() { return mapDispatchToProps; });
var connect = __webpack_require__(129).connect;


var mapStateToProps = function (state) { return ({
    alertStore: state.alert,
    cartStore: state.cart,
}); };
var mapDispatchToProps = function (dispatch) { return ({
    closeAlert: function (alertId) { return dispatch(Object(action_alert["b" /* closeAlertAction */])(alertId)); }
}); };
/* harmony default export */ var store = __webpack_exports__["default"] = (connect(mapStateToProps, mapDispatchToProps)(container));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUN0RCxPQUFPLEtBQUssTUFBTSxhQUFhLENBQUM7QUFFaEMsTUFBTSxDQUFDLElBQU0sZUFBZSxHQUFHLFVBQUMsS0FBSyxJQUFLLE9BQUEsQ0FBQztJQUN6QyxVQUFVLEVBQUUsS0FBSyxDQUFDLEtBQUs7SUFDdkIsU0FBUyxFQUFFLEtBQUssQ0FBQyxJQUFJO0NBQ3RCLENBQUMsRUFId0MsQ0FHeEMsQ0FBQztBQUVILE1BQU0sQ0FBQyxJQUFNLGtCQUFrQixHQUFHLFVBQUMsUUFBUSxJQUFLLE9BQUEsQ0FBQztJQUMvQyxVQUFVLEVBQUUsVUFBQyxPQUFlLElBQUssT0FBQSxRQUFRLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBbkMsQ0FBbUM7Q0FDckUsQ0FBQyxFQUY4QyxDQUU5QyxDQUFDO0FBRUgsZUFBZSxPQUFPLENBQ3BCLGVBQWUsRUFDZixrQkFBa0IsQ0FDbkIsQ0FBQyxLQUFLLENBQUMsQ0FBQyJ9

/***/ })

}]);