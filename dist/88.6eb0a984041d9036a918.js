(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[88],{

/***/ 855:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// EXTERNAL MODULE: ./style/media-queries.ts
var media_queries = __webpack_require__(76);

// CONCATENATED MODULE: ./container/layout/overlay-reload/style.tsx




/* harmony default export */ var style = ({
    display: 'block',
    width: 0,
    height: 0,
    panel: [(style_a = {
                position: 'fixed',
                zIndex: variable["zIndexMax"],
                backgroundColor: variable["colorF7"],
                top: '50%',
                left: 0,
                alignItems: 'center',
                display: variable["display"].none,
                cursor: 'pointer',
                borderRadius: 3,
                boxShadow: variable["shadowBlurSort"]
            },
            style_a[media_queries["a" /* default */][false === Object(responsive["b" /* isDesktopVersion */])() ? 'tablet960' : 'tablet960Reverse']] = {
                display: variable["display"].flex
            },
            style_a),],
    hidden: {
        display: variable["display"].none,
        transition: variable["transitionNormal"]
    },
    content: [
        layout["a" /* flexContainer */].center,
        layout["a" /* flexContainer */].verticalFlex,
        layout["a" /* flexContainer */].verticalCenter,
        {
            textAlign: 'center',
            opacity: 0.8,
            flexDirection: 'row',
            padding: '10px 20px',
        }
    ],
    icon: {
        width: 15,
        height: 15,
        color: variable["color2E"],
    },
    close: {
        position: variable["position"].absolute,
        top: -10,
        right: -10,
        height: 20,
        width: 20,
        display: variable["display"].flex,
        alignItems: 'center',
        justifyContent: 'center',
        background: variable["colorBlack08"],
        borderRadius: 50,
    },
    deleteIcon: {
        width: 10,
        height: 10,
        color: variable["colorWhite"],
    },
    text: {
        fontSize: 16,
        color: variable["color2E"],
        lineHeight: '30px',
        paddingLeft: 20
    }
});
var style_a;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFN0QsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEtBQUssTUFBTSxNQUFNLHVCQUF1QixDQUFDO0FBQ2hELE9BQU8sYUFBYSxNQUFNLDhCQUE4QixDQUFDO0FBRXpELGVBQWU7SUFDYixPQUFPLEVBQUUsT0FBTztJQUNoQixLQUFLLEVBQUUsQ0FBQztJQUNSLE1BQU0sRUFBRSxDQUFDO0lBRVQsS0FBSyxFQUFFO2dCQUNMLFFBQVEsRUFBRSxPQUFPO2dCQUNqQixNQUFNLEVBQUUsUUFBUSxDQUFDLFNBQVM7Z0JBQzFCLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztnQkFDakMsR0FBRyxFQUFFLEtBQUs7Z0JBQ1YsSUFBSSxFQUFFLENBQUM7Z0JBQ1AsVUFBVSxFQUFFLFFBQVE7Z0JBQ3BCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLE1BQU0sRUFBRSxTQUFTO2dCQUNqQixZQUFZLEVBQUUsQ0FBQztnQkFDZixTQUFTLEVBQUUsUUFBUSxDQUFDLGNBQWM7O1lBRWxDLEdBQUMsYUFBYSxDQUFDLEtBQUssS0FBSyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLElBQUc7Z0JBQ2hGLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7YUFDL0I7aUJBRUY7SUFFRCxNQUFNLEVBQUU7UUFDTixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1FBQzlCLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO0tBQ3RDO0lBRUQsT0FBTyxFQUFFO1FBQ1AsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNO1FBQzNCLE1BQU0sQ0FBQyxhQUFhLENBQUMsWUFBWTtRQUNqQyxNQUFNLENBQUMsYUFBYSxDQUFDLGNBQWM7UUFDbkM7WUFDRSxTQUFTLEVBQUUsUUFBUTtZQUNuQixPQUFPLEVBQUUsR0FBRztZQUNaLGFBQWEsRUFBRSxLQUFLO1lBQ3BCLE9BQU8sRUFBRSxXQUFXO1NBQ3JCO0tBQ0Y7SUFFRCxJQUFJLEVBQUU7UUFDSixLQUFLLEVBQUUsRUFBRTtRQUNULE1BQU0sRUFBRSxFQUFFO1FBQ1YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO0tBQ3hCO0lBRUQsS0FBSyxFQUFFO1FBQ0wsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtRQUNwQyxHQUFHLEVBQUUsQ0FBQyxFQUFFO1FBQ1IsS0FBSyxFQUFFLENBQUMsRUFBRTtRQUNWLE1BQU0sRUFBRSxFQUFFO1FBQ1YsS0FBSyxFQUFFLEVBQUU7UUFDVCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1FBQzlCLFVBQVUsRUFBRSxRQUFRO1FBQ3BCLGNBQWMsRUFBRSxRQUFRO1FBQ3hCLFVBQVUsRUFBRSxRQUFRLENBQUMsWUFBWTtRQUNqQyxZQUFZLEVBQUUsRUFBRTtLQUNqQjtJQUVELFVBQVUsRUFBRTtRQUNWLEtBQUssRUFBRSxFQUFFO1FBQ1QsTUFBTSxFQUFFLEVBQUU7UUFDVixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7S0FDM0I7SUFFRCxJQUFJLEVBQUU7UUFDSixRQUFRLEVBQUUsRUFBRTtRQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsT0FBTztRQUN2QixVQUFVLEVBQUUsTUFBTTtRQUNsQixXQUFXLEVBQUUsRUFBRTtLQUNoQjtDQUNNLENBQUMifQ==
// CONCATENATED MODULE: ./container/layout/overlay-reload/view.tsx


var renderView = function (_a) {
    var state = _a.state, handleClose = _a.handleClose;
    var isShow = state.isShow;
    var contentProps = {
        key: 'overlay-reload-content',
        style: style.content,
        onClick: function () { return location.reload(); }
    };
    var iconProps = {
        name: 'refresh',
        style: style.icon,
    };
    var deleteIconProps = {
        name: 'close',
        style: style.deleteIcon,
    };
    return (react["createElement"]("orverlay-reload", { style: [style, !isShow && style.hidden] }));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRy9CLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUc1QixJQUFNLFVBQVUsR0FBRyxVQUFDLEVBQXNCO1FBQXBCLGdCQUFLLEVBQUUsNEJBQVc7SUFFOUIsSUFBQSxxQkFBTSxDQUFxQjtJQUVuQyxJQUFNLFlBQVksR0FBRztRQUNuQixHQUFHLEVBQUUsd0JBQXdCO1FBQzdCLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTztRQUNwQixPQUFPLEVBQUUsY0FBTSxPQUFBLFFBQVEsQ0FBQyxNQUFNLEVBQUUsRUFBakIsQ0FBaUI7S0FDakMsQ0FBQztJQUVGLElBQU0sU0FBUyxHQUFHO1FBQ2hCLElBQUksRUFBRSxTQUFTO1FBQ2YsS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJO0tBQ2xCLENBQUM7SUFFRixJQUFNLGVBQWUsR0FBRztRQUN0QixJQUFJLEVBQUUsT0FBTztRQUNiLEtBQUssRUFBRSxLQUFLLENBQUMsVUFBVTtLQUN4QixDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wseUNBQWlCLEtBQUssRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDLE1BQU0sSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLEdBQ3RDLENBQ25CLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./container/layout/overlay-reload/initialize.tsx
var DEFAULT_PROPS = {};
var INITIAL_STATE = { isShow: true };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRyxFQUFZLENBQUM7QUFFMUMsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBWSxDQUFDIn0=
// CONCATENATED MODULE: ./container/layout/overlay-reload/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_OverlayReload = /** @class */ (function (_super) {
    __extends(OverlayReload, _super);
    function OverlayReload(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    OverlayReload.prototype.handleClose = function () {
        this.setState({ isShow: false });
    };
    OverlayReload.prototype.componentDidMount = function () {
        window.onresize = function (event) {
            switch (window.DEVICE_VERSION) {
                case 'MOBILE':
                    if (window.innerWidth >= 960) {
                        window.location.reload();
                    }
                    break;
                case 'DESKTOP':
                    if (window.innerWidth < 960) {
                        window.location.reload();
                    }
                    break;
            }
        };
    };
    OverlayReload.prototype.render = function () {
        var args = {
            state: this.state,
            handleClose: this.handleClose.bind(this)
        };
        return view(args);
    };
    OverlayReload = __decorate([
        radium
    ], OverlayReload);
    return OverlayReload;
}(react["PureComponent"]));
;
/* harmony default export */ var component = __webpack_exports__["default"] = (component_OverlayReload);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFDaEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQTtBQUc1QztJQUE0QixpQ0FBdUI7SUFDakQsdUJBQVksS0FBSztRQUFqQixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUM7O0lBQzdCLENBQUM7SUFFRCxtQ0FBVyxHQUFYO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFBO0lBQ2xDLENBQUM7SUFFRCx5Q0FBaUIsR0FBakI7UUFDRSxNQUFNLENBQUMsUUFBUSxHQUFHLFVBQVUsS0FBSztZQUUvQixNQUFNLENBQUMsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDOUIsS0FBSyxRQUFRO29CQUFFLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxVQUFVLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQzt3QkFBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFBO29CQUFDLENBQUM7b0JBQUMsS0FBSyxDQUFDO2dCQUNoRixLQUFLLFNBQVM7b0JBQUUsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO3dCQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUM7b0JBQUMsQ0FBQztvQkFBQyxLQUFLLENBQUM7WUFDbkYsQ0FBQztRQUNILENBQUMsQ0FBQTtJQUNILENBQUM7SUFFRCw4QkFBTSxHQUFOO1FBQ0UsSUFBTSxJQUFJLEdBQUc7WUFDWCxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztTQUN6QyxDQUFBO1FBQ0QsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBMUJHLGFBQWE7UUFEbEIsTUFBTTtPQUNELGFBQWEsQ0EyQmxCO0lBQUQsb0JBQUM7Q0FBQSxBQTNCRCxDQUE0QixhQUFhLEdBMkJ4QztBQUFBLENBQUM7QUFFRixlQUFlLGFBQWEsQ0FBQyJ9

/***/ })

}]);