(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[23,5,13,21],{

/***/ 745:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 100).then(__webpack_require__.bind(null, 755)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 751:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/general/pagination/style.tsx

/* harmony default export */ var style = ({
    width: '100%',
    container: {
        paddingTop: 10,
        paddingRight: 20,
        paddingBottom: 10,
        paddingLeft: 20,
    },
    icon: {
        cursor: 'pointer',
        height: 36,
        width: 36,
        minWidth: 36,
        color: variable["colorBlack"],
        inner: {
            width: 14
        }
    },
    item: {
        height: 36,
        minWidth: 36,
        paddingTop: 0,
        paddingRight: 10,
        paddingBottom: 0,
        paddingLeft: 10,
        lineHeight: '36px',
        textAlign: 'center',
        backgroundColor: variable["colorWhite"],
        fontFamily: variable["fontAvenirMedium"],
        fontSize: 13,
        color: variable["color4D"],
        cursor: 'pointer',
        marginBottom: 10,
        verticalAlign: 'top',
        icon: {
            paddingRight: 0,
            paddingLeft: 0,
        },
        active: {
            backgroundColor: variable["colorWhite"],
            color: variable["colorBlack"],
            borderRadius: 3,
            pointerEvents: 'none',
            border: "1px solid " + variable["color75"],
            zIndex: variable["zIndex5"],
            fontFamily: variable["fontAvenirDemiBold"],
        },
        disabled: {
            pointerEvents: 'none',
            backgroundColor: variable["colorWhite"],
        },
        ':hover': {
            border: "1px solid " + variable["colorA2"],
        }
    },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxlQUFlO0lBQ2IsS0FBSyxFQUFFLE1BQU07SUFFYixTQUFTLEVBQUU7UUFDVCxVQUFVLEVBQUUsRUFBRTtRQUNkLFlBQVksRUFBRSxFQUFFO1FBQ2hCLGFBQWEsRUFBRSxFQUFFO1FBQ2pCLFdBQVcsRUFBRSxFQUFFO0tBQ2hCO0lBRUQsSUFBSSxFQUFFO1FBQ0osTUFBTSxFQUFFLFNBQVM7UUFDakIsTUFBTSxFQUFFLEVBQUU7UUFDVixLQUFLLEVBQUUsRUFBRTtRQUNULFFBQVEsRUFBRSxFQUFFO1FBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1FBRTFCLEtBQUssRUFBRTtZQUNMLEtBQUssRUFBRSxFQUFFO1NBQ1Y7S0FDRjtJQUVELElBQUksRUFBRTtRQUNKLE1BQU0sRUFBRSxFQUFFO1FBQ1YsUUFBUSxFQUFFLEVBQUU7UUFDWixVQUFVLEVBQUUsQ0FBQztRQUNiLFlBQVksRUFBRSxFQUFFO1FBQ2hCLGFBQWEsRUFBRSxDQUFDO1FBQ2hCLFdBQVcsRUFBRSxFQUFFO1FBQ2YsVUFBVSxFQUFFLE1BQU07UUFDbEIsU0FBUyxFQUFFLFFBQVE7UUFDbkIsZUFBZSxFQUFFLFFBQVEsQ0FBQyxVQUFVO1FBQ3BDLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO1FBQ3JDLFFBQVEsRUFBRSxFQUFFO1FBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO1FBQ3ZCLE1BQU0sRUFBRSxTQUFTO1FBQ2pCLFlBQVksRUFBRSxFQUFFO1FBQ2hCLGFBQWEsRUFBRSxLQUFLO1FBRXBCLElBQUksRUFBRTtZQUNKLFlBQVksRUFBRSxDQUFDO1lBQ2YsV0FBVyxFQUFFLENBQUM7U0FDZjtRQUdELE1BQU0sRUFBRTtZQUNOLGVBQWUsRUFBRSxRQUFRLENBQUMsVUFBVTtZQUNwQyxLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDMUIsWUFBWSxFQUFFLENBQUM7WUFDZixhQUFhLEVBQUUsTUFBTTtZQUNyQixNQUFNLEVBQUUsZUFBYSxRQUFRLENBQUMsT0FBUztZQUN2QyxNQUFNLEVBQUUsUUFBUSxDQUFDLE9BQU87WUFDeEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxrQkFBa0I7U0FDeEM7UUFFRCxRQUFRLEVBQUU7WUFDUixhQUFhLEVBQUUsTUFBTTtZQUNyQixlQUFlLEVBQUUsUUFBUSxDQUFDLFVBQVU7U0FDckM7UUFFRCxRQUFRLEVBQUU7WUFDUixNQUFNLEVBQUUsZUFBYSxRQUFRLENBQUMsT0FBUztTQUN4QztLQUNGO0NBQ0YsQ0FBQyJ9
// CONCATENATED MODULE: ./components/general/pagination/view.tsx






function renderComponent() {
    var _this = this;
    var _a = this.props, current = _a.current, per = _a.per, total = _a.total, urlList = _a.urlList, handleClick = _a.handleClick;
    var list = this.state.list;
    var numberList = [];
    var handleOnClick = function (val) { return 'function' === typeof handleClick && handleClick(val); };
    var isUrlListEmpty = null === urlList
        || Object(validate["l" /* isUndefined */])(urlList)
        || urlList.length === 0
        || total !== urlList.length;
    return isUrlListEmpty ? null : (react["createElement"]("div", { style: style },
        react["createElement"]("div", { style: [layout["a" /* flexContainer */].center, layout["a" /* flexContainer */].wrap, style.container] },
            current !== 1 &&
                react["createElement"](react_router_dom["NavLink"], { title: 'Trang ' + (current - 1), to: urlList[current - 2].link, onClick: function () { handleOnClick(current - 1), _this.handleScrollTop(); }, style: Object.assign({}, style.item, style.item.icon) },
                    react["createElement"](icon["a" /* default */], { name: 'arrow-left', style: style.icon, innerStyle: style.icon.inner })),
            total > 1
                && Array.isArray(list)
                && list.map(function (item) {
                    return react["createElement"](react_router_dom["NavLink"], { title: 'Trang ' + item.title, to: item.link, key: "pagi-item-" + item.number, onClick: function () { handleOnClick(item.title), _this.handleScrollTop(); }, style: Object.assign({}, style.item, (item.number === current) && style.item.active, item.disabled && style.item.disabled) }, item.title);
                }),
            current !== total &&
                react["createElement"](react_router_dom["NavLink"], { title: 'Trang ' + (current + 1), to: urlList[current].link, style: Object.assign({}, style.item, style.item.icon), onClick: function () { handleOnClick(current + 1), _this.handleScrollTop(); } },
                    react["createElement"](icon["a" /* default */], { name: 'arrow-right', style: style.icon, innerStyle: style.icon.inner })))));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUUzQyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDdEQsT0FBTyxLQUFLLE1BQU0sTUFBTSx1QkFBdUIsQ0FBQztBQUNoRCxPQUFPLElBQUksTUFBTSxlQUFlLENBQUM7QUFDakMsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRzVCLE1BQU07SUFBTixpQkErREM7SUE5RE8sSUFBQSxlQUEwRCxFQUF4RCxvQkFBTyxFQUFFLFlBQUcsRUFBRSxnQkFBSyxFQUFFLG9CQUFPLEVBQUUsNEJBQVcsQ0FBZ0I7SUFDekQsSUFBQSxzQkFBSSxDQUFnQjtJQUM1QixJQUFNLFVBQVUsR0FBa0IsRUFBRSxDQUFDO0lBRXJDLElBQU0sYUFBYSxHQUFHLFVBQUMsR0FBRyxJQUFLLE9BQUEsVUFBVSxLQUFLLE9BQU8sV0FBVyxJQUFJLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckQsQ0FBcUQsQ0FBQztJQUNyRixJQUFNLGNBQWMsR0FBRyxJQUFJLEtBQUssT0FBTztXQUNsQyxXQUFXLENBQUMsT0FBTyxDQUFDO1dBQ3BCLE9BQU8sQ0FBQyxNQUFNLEtBQUssQ0FBQztXQUNwQixLQUFLLEtBQUssT0FBTyxDQUFDLE1BQU0sQ0FBQztJQUU5QixNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQzdCLDZCQUFLLEtBQUssRUFBRSxLQUFLO1FBQ2YsNkJBQUssS0FBSyxFQUFFLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQztZQUVqRixPQUFPLEtBQUssQ0FBQztnQkFDYixvQkFBQyxPQUFPLElBQ04sS0FBSyxFQUFFLFFBQVEsR0FBRyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsRUFDL0IsRUFBRSxFQUFFLE9BQU8sQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUM3QixPQUFPLEVBQUUsY0FBUSxhQUFhLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxFQUFFLEtBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQSxDQUFDLENBQUMsRUFDckUsS0FBSyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7b0JBQ3JELG9CQUFDLElBQUksSUFDSCxJQUFJLEVBQUUsWUFBWSxFQUNsQixLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksRUFDakIsVUFBVSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFJLENBQzFCO1lBSVYsS0FBSyxHQUFHLENBQUM7bUJBQ04sS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7bUJBQ25CLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJO29CQUNkLE9BQUEsb0JBQUMsT0FBTyxJQUNOLEtBQUssRUFBRSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssRUFDNUIsRUFBRSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQ2IsR0FBRyxFQUFFLGVBQWEsSUFBSSxDQUFDLE1BQVEsRUFDL0IsT0FBTyxFQUFFLGNBQVEsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxLQUFJLENBQUMsZUFBZSxFQUFFLENBQUEsQ0FBQyxDQUFDLEVBQ3BFLEtBQUssRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFDckIsS0FBSyxDQUFDLElBQUksRUFDVixDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssT0FBTyxDQUFDLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQzlDLElBQUksQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQ3JDLElBQ0EsSUFBSSxDQUFDLEtBQUssQ0FDSDtnQkFYVixDQVdVLENBQ1g7WUFJRCxPQUFPLEtBQUssS0FBSztnQkFDakIsb0JBQUMsT0FBTyxJQUNOLEtBQUssRUFBRSxRQUFRLEdBQUcsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLEVBQy9CLEVBQUUsRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxFQUN6QixLQUFLLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsS0FBSyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUNyRCxPQUFPLEVBQUUsY0FBUSxhQUFhLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxFQUFFLEtBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQSxDQUFDLENBQUM7b0JBQ3JFLG9CQUFDLElBQUksSUFDSCxJQUFJLEVBQUUsYUFBYSxFQUNuQixLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksRUFDakIsVUFBVSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFJLENBQzFCLENBRVIsQ0FDRCxDQUNSLENBQUE7QUFDSCxDQUFDO0FBQUEsQ0FBQyJ9
// CONCATENATED MODULE: ./components/general/pagination/initialize.tsx
var DEFAULT_PROPS = {
    current: 0,
    per: 0,
    total: 0,
    urlList: [],
    canScrollToTop: true,
    elementId: '',
    scrollToElementNum: 0
};
var INITIAL_STATE = { list: [] };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixPQUFPLEVBQUUsQ0FBQztJQUNWLEdBQUcsRUFBRSxDQUFDO0lBQ04sS0FBSyxFQUFFLENBQUM7SUFDUixPQUFPLEVBQUUsRUFBRTtJQUNYLGNBQWMsRUFBRSxJQUFJO0lBQ3BCLFNBQVMsRUFBRSxFQUFFO0lBQ2Isa0JBQWtCLEVBQUUsQ0FBQztDQUNGLENBQUM7QUFFdEIsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBc0IsQ0FBQyJ9
// CONCATENATED MODULE: ./components/general/pagination/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var component_Pagination = /** @class */ (function (_super) {
    __extends(Pagination, _super);
    function Pagination(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    Pagination.prototype.componentDidMount = function () {
        this.initData();
    };
    Pagination.prototype.componentWillReceiveProps = function (nextProps) {
        this.initData(nextProps);
    };
    Pagination.prototype.handleScrollTop = function () {
        var _a = this.props, canScrollToTop = _a.canScrollToTop, elementId = _a.elementId, scrollToElementNum = _a.scrollToElementNum;
        canScrollToTop && window.scrollTo(0, 0);
        if (elementId && elementId.length > 0) {
            var element = document.getElementById(elementId);
            element && element.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
        }
        scrollToElementNum > 0 && window.scrollTo(0, scrollToElementNum);
    };
    Pagination.prototype.initData = function (props) {
        if (props === void 0) { props = this.props; }
        var urlList = props.urlList, current = props.current, per = props.per, total = props.total;
        if (null === urlList
            || true === Object(validate["l" /* isUndefined */])(urlList)
            || 0 === urlList.length
            || urlList.length !== total) {
            return;
        }
        var list = [], startInnerList, endInnerList;
        /** Generate head-list */
        if (current >= 5) {
            list.push({
                number: urlList[0].number,
                title: urlList[0].title,
                link: urlList[0].link,
                active: false,
                disabled: false
            });
            list.push({
                value: -1,
                number: -1,
                title: '...',
                link: '#',
                active: false,
                disabled: true
            });
        }
        /**
         * Generate inner list
         *
         * startInnerList : min 0
         * endInnerList : max pagingInfo.total
         *
         */
        startInnerList = current - 2;
        startInnerList = startInnerList <= 0
            /** Min value : 1 */
            ? 1
            : (startInnerList === 2
                /** If start value === 2 -> force to 1 */
                ? 1
                : startInnerList);
        endInnerList = startInnerList + 4;
        endInnerList = endInnerList > total
            /** Max value total */
            ? total
            : (
            /** If end value === total - 1 -> force to total */
            endInnerList === total - 1
                ? total
                : endInnerList);
        for (var i = startInnerList; i <= endInnerList; i++) {
            list.push({
                number: urlList[i - 1].number,
                title: urlList[i - 1].title,
                link: urlList[i - 1].link,
                active: i === current,
                disabled: false,
                endList: i === total,
            });
        }
        /** Generate tail-list */
        if (total > 5 && current <= total - 4) {
            list.push({
                value: -2,
                number: -2,
                title: '...',
                link: '#',
                active: false,
                disabled: true
            });
            list.push({
                number: urlList[total - 1].number,
                title: urlList[total - 1].title,
                link: urlList[total - 1].link,
                active: false,
                disabled: false,
                endList: true
            });
        }
        this.setState({ list: list });
    };
    /**
     * Go To Page
     *
     * @param newPage object : page clicked
     */
    Pagination.prototype.goToPage = function (newPage) {
        // const { onSelectPage } = this.props as IPaginationProps;
        // onSelectPage(newPage);
    };
    /**
     * Navigate page:
     *
     * @param direction string next/prev
     *
     * @return call to goToPage()
     */
    Pagination.prototype.navigatePage = function (direction) {
        // const { productByCategory } = this.props;
        // const pagingInfo = productByCategory.paging;
        if (direction === void 0) { direction = 'next'; }
        // /** Generate new page value */
        // const newPage = pagingInfo.current + ('next' === direction ? 1 : -1);
        // /** Check range value [1, total] */
        // if (newPage < 0 || newPage > pagingInfo.total) {
        //   return;
        // }
        // this.goToPage(newPage);
    };
    Pagination.prototype.render = function () { return renderComponent.bind(this)(); };
    Pagination.defaultProps = DEFAULT_PROPS;
    Pagination = __decorate([
        radium
    ], Pagination);
    return Pagination;
}(react["Component"]));
;
/* harmony default export */ var component = (component_Pagination);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFHakMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUN6QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDdEQsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFJNUQ7SUFBeUIsOEJBQW1EO0lBRTFFLG9CQUFZLEtBQUs7UUFBakIsWUFDRSxrQkFBTSxLQUFLLENBQUMsU0FFYjtRQURDLEtBQUksQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDOztJQUM3QixDQUFDO0lBRUQsc0NBQWlCLEdBQWpCO1FBQ0UsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xCLENBQUM7SUFFRCw4Q0FBeUIsR0FBekIsVUFBMEIsU0FBUztRQUNqQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFRCxvQ0FBZSxHQUFmO1FBQ1EsSUFBQSxlQUFrRixFQUFoRixrQ0FBYyxFQUFFLHdCQUFTLEVBQUUsMENBQWtCLENBQW9DO1FBQ3pGLGNBQWMsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUV4QyxFQUFFLENBQUMsQ0FBQyxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3RDLElBQU0sT0FBTyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDbkQsT0FBTyxJQUFJLE9BQU8sQ0FBQyxjQUFjLENBQUMsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUM7UUFDL0YsQ0FBQztRQUVELGtCQUFrQixHQUFHLENBQUMsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO0lBQ25FLENBQUM7SUFFRCw2QkFBUSxHQUFSLFVBQVMsS0FBa0I7UUFBbEIsc0JBQUEsRUFBQSxRQUFRLElBQUksQ0FBQyxLQUFLO1FBQ2pCLElBQUEsdUJBQU8sRUFBRSx1QkFBTyxFQUFFLGVBQUcsRUFBRSxtQkFBSyxDQUFXO1FBRS9DLEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxPQUFPO2VBQ2YsSUFBSSxLQUFLLFdBQVcsQ0FBQyxPQUFPLENBQUM7ZUFDN0IsQ0FBQyxLQUFLLE9BQU8sQ0FBQyxNQUFNO2VBQ3BCLE9BQU8sQ0FBQyxNQUFNLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQztZQUM5QixNQUFNLENBQUM7UUFDVCxDQUFDO1FBRUQsSUFBSSxJQUFJLEdBQWUsRUFBRSxFQUFFLGNBQWMsRUFBRSxZQUFZLENBQUM7UUFFeEQseUJBQXlCO1FBQ3pCLEVBQUUsQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2pCLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ1IsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNO2dCQUN6QixLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUs7Z0JBQ3ZCLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtnQkFDckIsTUFBTSxFQUFFLEtBQUs7Z0JBQ2IsUUFBUSxFQUFFLEtBQUs7YUFDaEIsQ0FBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDUixLQUFLLEVBQUUsQ0FBQyxDQUFDO2dCQUNULE1BQU0sRUFBRSxDQUFDLENBQUM7Z0JBQ1YsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osSUFBSSxFQUFFLEdBQUc7Z0JBQ1QsTUFBTSxFQUFFLEtBQUs7Z0JBQ2IsUUFBUSxFQUFFLElBQUk7YUFDZixDQUFDLENBQUM7UUFDTCxDQUFDO1FBRUQ7Ozs7OztXQU1HO1FBQ0gsY0FBYyxHQUFHLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFDN0IsY0FBYyxHQUFHLGNBQWMsSUFBSSxDQUFDO1lBQ2xDLG9CQUFvQjtZQUNwQixDQUFDLENBQUMsQ0FBQztZQUNILENBQUMsQ0FBQyxDQUNBLGNBQWMsS0FBSyxDQUFDO2dCQUNsQix5Q0FBeUM7Z0JBQ3pDLENBQUMsQ0FBQyxDQUFDO2dCQUNILENBQUMsQ0FBQyxjQUFjLENBQ25CLENBQUM7UUFFSixZQUFZLEdBQUcsY0FBYyxHQUFHLENBQUMsQ0FBQztRQUNsQyxZQUFZLEdBQUcsWUFBWSxHQUFHLEtBQUs7WUFDakMsc0JBQXNCO1lBQ3RCLENBQUMsQ0FBQyxLQUFLO1lBQ1AsQ0FBQyxDQUFDO1lBQ0EsbURBQW1EO1lBQ25ELFlBQVksS0FBSyxLQUFLLEdBQUcsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLEtBQUs7Z0JBQ1AsQ0FBQyxDQUFDLFlBQVksQ0FDakIsQ0FBQztRQUVKLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLGNBQWMsRUFBRSxDQUFDLElBQUksWUFBWSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDcEQsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDUixNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNO2dCQUM3QixLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLO2dCQUMzQixJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJO2dCQUN6QixNQUFNLEVBQUUsQ0FBQyxLQUFLLE9BQU87Z0JBQ3JCLFFBQVEsRUFBRSxLQUFLO2dCQUNmLE9BQU8sRUFBRSxDQUFDLEtBQUssS0FBSzthQUNyQixDQUFDLENBQUM7UUFDTCxDQUFDO1FBRUQseUJBQXlCO1FBQ3pCLEVBQUUsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLElBQUksT0FBTyxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ1IsS0FBSyxFQUFFLENBQUMsQ0FBQztnQkFDVCxNQUFNLEVBQUUsQ0FBQyxDQUFDO2dCQUNWLEtBQUssRUFBRSxLQUFLO2dCQUNaLElBQUksRUFBRSxHQUFHO2dCQUNULE1BQU0sRUFBRSxLQUFLO2dCQUNiLFFBQVEsRUFBRSxJQUFJO2FBQ2YsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDUixNQUFNLEVBQUUsT0FBTyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNO2dCQUNqQyxLQUFLLEVBQUUsT0FBTyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLO2dCQUMvQixJQUFJLEVBQUUsT0FBTyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJO2dCQUM3QixNQUFNLEVBQUUsS0FBSztnQkFDYixRQUFRLEVBQUUsS0FBSztnQkFDZixPQUFPLEVBQUUsSUFBSTthQUNkLENBQUMsQ0FBQztRQUNMLENBQUM7UUFFRCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCw2QkFBUSxHQUFSLFVBQVMsT0FBTztRQUNkLDJEQUEyRDtRQUMzRCx5QkFBeUI7SUFDM0IsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNILGlDQUFZLEdBQVosVUFBYSxTQUFrQjtRQUM3Qiw0Q0FBNEM7UUFDNUMsK0NBQStDO1FBRnBDLDBCQUFBLEVBQUEsa0JBQWtCO1FBSTdCLGlDQUFpQztRQUNqQyx3RUFBd0U7UUFFeEUsc0NBQXNDO1FBQ3RDLG1EQUFtRDtRQUNuRCxZQUFZO1FBQ1osSUFBSTtRQUVKLDBCQUEwQjtJQUM1QixDQUFDO0lBRUQsMkJBQU0sR0FBTixjQUFXLE1BQU0sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBekoxQyx1QkFBWSxHQUFxQixhQUFhLENBQUM7SUFEbEQsVUFBVTtRQURmLE1BQU07T0FDRCxVQUFVLENBMkpmO0lBQUQsaUJBQUM7Q0FBQSxBQTNKRCxDQUF5QixLQUFLLENBQUMsU0FBUyxHQTJKdkM7QUFBQSxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// CONCATENATED MODULE: ./components/general/pagination/index.tsx

/* harmony default export */ var pagination = __webpack_exports__["a"] = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxVQUFVLE1BQU0sYUFBYSxDQUFDO0FBQ3JDLGVBQWUsVUFBVSxDQUFDIn0=

/***/ }),

/***/ 754:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return updateMetaInfoAction; });
/* harmony import */ var _constants_api_meta__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(352);

var updateMetaInfoAction = function (data) { return ({
    type: _constants_api_meta__WEBPACK_IMPORTED_MODULE_0__[/* UPDATE_META_INFO */ "a"],
    payload: data
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWV0YS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1ldGEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLGdCQUFnQixFQUNqQixNQUFNLHVCQUF1QixDQUFDO0FBRS9CLE1BQU0sQ0FBQyxJQUFNLG9CQUFvQixHQUFHLFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FBQztJQUM3QyxJQUFJLEVBQUUsZ0JBQWdCO0lBQ3RCLE9BQU8sRUFBRSxJQUFJO0NBQ2QsQ0FBQyxFQUg0QyxDQUc1QyxDQUFDIn0=

/***/ }),

/***/ 756:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 96).then(__webpack_require__.bind(null, 779)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 760:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return BANNER_LIMIT_DEFAULT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BANNER_ID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return SEARCH_PARAM_DEFAULT; });
/** Limit value when fetch list banner */
var BANNER_LIMIT_DEFAULT = 10;
/**
 * Banner id
 * List banner name
 */
var BANNER_ID = {
    HOME_PAGE: 'homepage_version_2',
    HEADER_TOP: 'header_top',
    WEB_MOBILE_HOME_CATEGORY: 'web_mobile_home_category',
    FOOTER: 'footer_feature',
    HOME_FEATURE: 'home_feature',
    WEEKLY_SPECIALS_LARGE: 'weekly-specials-large',
    WEEKLY_SPECIALS_SMALL: 'weekly-specials-small',
    WEEKLY_SPECIALS_LARGE_02: 'weekly-specials-large-02',
    WEEKLY_SPECIALS_SMALL_02: 'weekly-specials-small-02',
    EVERYDAY_DEALS: 'everyday-deals',
    SUMMER_SALE: 'summer-sale',
    POPULAR_SEARCH: 'popular-search'
};
/** Default value for search params */
var SEARCH_PARAM_DEFAULT = {
    PAGE: 1,
    PER_PAGE: 36
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVmYXVsdC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRlZmF1bHQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEseUNBQXlDO0FBQ3pDLE1BQU0sQ0FBQyxJQUFNLG9CQUFvQixHQUFHLEVBQUUsQ0FBQztBQUV2Qzs7O0dBR0c7QUFDSCxNQUFNLENBQUMsSUFBTSxTQUFTLEdBQUc7SUFDdkIsU0FBUyxFQUFFLG9CQUFvQjtJQUMvQixVQUFVLEVBQUUsWUFBWTtJQUN4Qix3QkFBd0IsRUFBRSwwQkFBMEI7SUFDcEQsTUFBTSxFQUFFLGdCQUFnQjtJQUN4QixZQUFZLEVBQUUsY0FBYztJQUM1QixxQkFBcUIsRUFBRSx1QkFBdUI7SUFDOUMscUJBQXFCLEVBQUUsdUJBQXVCO0lBQzlDLHdCQUF3QixFQUFFLDBCQUEwQjtJQUNwRCx3QkFBd0IsRUFBRSwwQkFBMEI7SUFDcEQsY0FBYyxFQUFFLGdCQUFnQjtJQUNoQyxXQUFXLEVBQUUsYUFBYTtJQUMxQixjQUFjLEVBQUUsZ0JBQWdCO0NBQ2pDLENBQUM7QUFFRixzQ0FBc0M7QUFDdEMsTUFBTSxDQUFDLElBQU0sb0JBQW9CLEdBQUc7SUFDbEMsSUFBSSxFQUFFLENBQUM7SUFDUCxRQUFRLEVBQUUsRUFBRTtDQUNiLENBQUMifQ==

/***/ }),

/***/ 761:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// CONCATENATED MODULE: ./api/magazine.ts

;
var fetchMagazineList = function (_a) {
    var _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c, _d = _a.type, type = _d === void 0 ? 'default' : _d;
    var query = "?page=" + page + "&per_page=" + perPage + "&filter[post_type]=" + type;
    return Object(restful_method["b" /* get */])({
        path: "/magazines" + query,
        description: 'Get magazine list',
        errorMesssage: "Can't get magazine list. Please try again",
    });
};
var fetchMagazineDashboard = function () { return Object(restful_method["b" /* get */])({
    path: "/magazines/dashboard",
    description: 'Get magazine dashboard list',
    errorMesssage: "Can't get magazine dashboard list. Please try again",
}); };
/**
 * Fetch magazine category by slug (id)
 */
var fetchMagazineCategory = function (_a) {
    var slug = _a.slug, page = _a.page, perPage = _a.perPage;
    var query = slug + "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/category/" + query,
        description: 'Get magazine category by slug (id)',
        errorMesssage: "Can't get magazine category by slug (id). Please try again",
    });
};
/**
* Fetch magazine by slug (id)
*/
var fetchMagazineBySlug = function (_a) {
    var slug = _a.slug;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/" + slug,
        description: 'Get magazine by slug (id)',
        errorMesssage: "Can't get magazine by slug (id). Please try again",
    });
};
/**
* Fetch magazine related blogs by slug (id)
*/
var fetchMagazineRelatedBlog = function (_a) {
    var slug = _a.slug;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/" + slug + "/related_blogs",
        description: 'Get magazine related blogs by slug (id)',
        errorMesssage: "Can't get magazine related blogs by slug (id). Please try again",
    });
};
/**
* Fetch magazine related boxes by slug (id)
*/
var fetchMagazineRelatedBox = function (_a) {
    var slug = _a.slug, limit = _a.limit;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/" + slug + "/related_boxes?limit=" + limit,
        description: 'Get magazine related boxes by slug (id)',
        errorMesssage: "Can't get magazine related boxes by slug (id). Please try again",
    });
};
/**
* Fetch magazine by tag name
*/
var fetchMagazineByTagName = function (_a) {
    var slug = _a.slug, page = _a.page, perPage = _a.perPage;
    var query = slug + "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/tag/" + query,
        description: 'Get magazine by tag name',
        errorMesssage: "Can't get magazine by tag name. Please try again",
    });
};
/**
* Fetch search magazine by keyword
*/
var fetchMagazineByKeyword = function (_a) {
    var keyword = _a.keyword, page = _a.page, perPage = _a.perPage;
    var query = keyword + "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/search/" + query,
        description: 'Get search magazine by keyword',
        errorMesssage: "Can't get search magazine by keyword. Please try again",
    });
};
/**
* Fetch search suggestion magazine by keyword
*/
var fetchMagazineSuggestionByKeyword = function (_a) {
    var keyword = _a.keyword, limit = _a.limit;
    var query = keyword + "?limit=" + limit;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/search_suggestion/" + query,
        description: 'Get search suggestion magazine by keyword',
        errorMesssage: "Can't get search suggestion magazine by keyword. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFnYXppbmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtYWdhemluZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFNOUMsQ0FBQztBQUVGLE1BQU0sQ0FBQyxJQUFNLGlCQUFpQixHQUM1QixVQUFDLEVBQXFFO1FBQW5FLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWSxFQUFFLFlBQWdCLEVBQWhCLHFDQUFnQjtJQUN6QyxJQUFNLEtBQUssR0FBRyxXQUFTLElBQUksa0JBQWEsT0FBTywyQkFBc0IsSUFBTSxDQUFDO0lBRTVFLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsZUFBYSxLQUFPO1FBQzFCLFdBQVcsRUFBRSxtQkFBbUI7UUFDaEMsYUFBYSxFQUFFLDJDQUEyQztLQUMzRCxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSixNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FBRyxjQUFNLE9BQUEsR0FBRyxDQUFDO0lBQzlDLElBQUksRUFBRSxzQkFBc0I7SUFDNUIsV0FBVyxFQUFFLDZCQUE2QjtJQUMxQyxhQUFhLEVBQUUscURBQXFEO0NBQ3JFLENBQUMsRUFKMEMsQ0FJMUMsQ0FBQztBQUVIOztHQUVHO0FBQ0gsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQ2hDLFVBQUMsRUFBdUI7UUFBckIsY0FBSSxFQUFFLGNBQUksRUFBRSxvQkFBTztJQUNwQixJQUFNLEtBQUssR0FBTSxJQUFJLGNBQVMsSUFBSSxrQkFBYSxPQUFTLENBQUM7SUFDekQsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSx5QkFBdUIsS0FBTztRQUNwQyxXQUFXLEVBQUUsb0NBQW9DO1FBQ2pELGFBQWEsRUFBRSw0REFBNEQ7S0FDNUUsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUo7O0VBRUU7QUFDRixNQUFNLENBQUMsSUFBTSxtQkFBbUIsR0FBRyxVQUFDLEVBQVE7UUFBTixjQUFJO0lBQU8sT0FBQSxHQUFHLENBQUM7UUFDbkQsSUFBSSxFQUFFLGdCQUFjLElBQU07UUFDMUIsV0FBVyxFQUFFLDJCQUEyQjtRQUN4QyxhQUFhLEVBQUUsbURBQW1EO0tBQ25FLENBQUM7QUFKK0MsQ0FJL0MsQ0FBQztBQUVIOztFQUVFO0FBQ0YsTUFBTSxDQUFDLElBQU0sd0JBQXdCLEdBQUcsVUFBQyxFQUFRO1FBQU4sY0FBSTtJQUFPLE9BQUEsR0FBRyxDQUFDO1FBQ3hELElBQUksRUFBRSxnQkFBYyxJQUFJLG1CQUFnQjtRQUN4QyxXQUFXLEVBQUUseUNBQXlDO1FBQ3RELGFBQWEsRUFBRSxpRUFBaUU7S0FDakYsQ0FBQztBQUpvRCxDQUlwRCxDQUFDO0FBRUg7O0VBRUU7QUFDRixNQUFNLENBQUMsSUFBTSx1QkFBdUIsR0FBRyxVQUFDLEVBQWU7UUFBYixjQUFJLEVBQUUsZ0JBQUs7SUFBTyxPQUFBLEdBQUcsQ0FBQztRQUM5RCxJQUFJLEVBQUUsZ0JBQWMsSUFBSSw2QkFBd0IsS0FBTztRQUN2RCxXQUFXLEVBQUUseUNBQXlDO1FBQ3RELGFBQWEsRUFBRSxpRUFBaUU7S0FDakYsQ0FBQztBQUowRCxDQUkxRCxDQUFDO0FBRUg7O0VBRUU7QUFDRixNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FDakMsVUFBQyxFQUF1QjtRQUFyQixjQUFJLEVBQUUsY0FBSSxFQUFFLG9CQUFPO0lBQ3BCLElBQU0sS0FBSyxHQUFNLElBQUksY0FBUyxJQUFJLGtCQUFhLE9BQVMsQ0FBQztJQUN6RCxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLG9CQUFrQixLQUFPO1FBQy9CLFdBQVcsRUFBRSwwQkFBMEI7UUFDdkMsYUFBYSxFQUFFLGtEQUFrRDtLQUNsRSxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSjs7RUFFRTtBQUNGLE1BQU0sQ0FBQyxJQUFNLHNCQUFzQixHQUNqQyxVQUFDLEVBQTBCO1FBQXhCLG9CQUFPLEVBQUUsY0FBSSxFQUFFLG9CQUFPO0lBQ3ZCLElBQU0sS0FBSyxHQUFNLE9BQU8sY0FBUyxJQUFJLGtCQUFhLE9BQVMsQ0FBQztJQUM1RCxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLHVCQUFxQixLQUFPO1FBQ2xDLFdBQVcsRUFBRSxnQ0FBZ0M7UUFDN0MsYUFBYSxFQUFFLHdEQUF3RDtLQUN4RSxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSjs7RUFFRTtBQUNGLE1BQU0sQ0FBQyxJQUFNLGdDQUFnQyxHQUMzQyxVQUFDLEVBQWtCO1FBQWhCLG9CQUFPLEVBQUUsZ0JBQUs7SUFDZixJQUFNLEtBQUssR0FBTSxPQUFPLGVBQVUsS0FBTyxDQUFDO0lBQzFDLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsa0NBQWdDLEtBQU87UUFDN0MsV0FBVyxFQUFFLDJDQUEyQztRQUN4RCxhQUFhLEVBQUUsbUVBQW1FO0tBQ25GLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQyJ9
// EXTERNAL MODULE: ./constants/api/magazine.ts
var magazine = __webpack_require__(22);

// CONCATENATED MODULE: ./action/magazine.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return fetchMagazineListAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return fetchMagazineDashboardAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return fetchMagazineCategoryAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fetchMagazineBySlugAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return fetchMagazineRelatedBlogAction; });
/* unused harmony export fetchMagazineRelatedBoxAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fetchMagazineByTagNameAction; });
/* unused harmony export showHideMagazineSearchAction */
/* unused harmony export fetchMagazineByKeywordAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return fetchMagazineSuggestionByKeywordAction; });


/**
 * Fetch love list by filter paramsx
 *
 * @param {number} page ex: 1, 2, 3, 4
 * @param {number} perPage ex: 10, 15, 20
 * @param {'default' | 'video-post' | 'quote-post'} type
 */
var fetchMagazineListAction = function (_a) {
    var page = _a.page, perPage = _a.perPage, type = _a.type;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["f" /* FETCH_MAGAZINE_LIST */],
            payload: { promise: fetchMagazineList({ page: page, perPage: perPage, type: type }).then(function (res) { return res; }) },
            meta: { page: page, perPage: perPage, type: type }
        });
    };
};
var fetchMagazineDashboardAction = function () {
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["e" /* FETCH_MAGAZINE_DASHBOARD */],
            payload: { promise: fetchMagazineDashboard().then(function (res) { return res; }) },
            meta: {}
        });
    };
};
/**
* Fetch magazine category by slug (id)
*
* @param {string} slug ex: makeup
* @param {number} page ex: 1, 2, 3, 4
* @param {number} perPage ex: 10, 15, 20
*/
var fetchMagazineCategoryAction = function (_a) {
    var slug = _a.slug, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["d" /* FETCH_MAGAZINE_CATEGORY */],
            payload: { promise: fetchMagazineCategory({ slug: slug, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { slug: slug, page: page, perPage: perPage }
        });
    };
};
/**
* Fetch magazine by slug (id)
*
* @param {string} slug ex: makeup
*/
var fetchMagazineBySlugAction = function (_a) {
    var slug = _a.slug;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["c" /* FETCH_MAGAZINE_BY_SLUG */],
            payload: { promise: fetchMagazineBySlug({ slug: slug }).then(function (res) { return res; }) },
            meta: { slug: slug }
        });
    };
};
/**
* Fetch magazine related blogs by slug (id)
*
* @param {string} slug ex: makeup
*/
var fetchMagazineRelatedBlogAction = function (_a) {
    var slug = _a.slug;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["g" /* FETCH_MAGAZINE_RELATED_BLOGS */],
            payload: { promise: fetchMagazineRelatedBlog({ slug: slug }).then(function (res) { return res; }) },
            meta: { slug: slug }
        });
    };
};
/**
* Fetch magazine related boxes by slug (id)
*
* @param {string} slug ex: makeup
* @param {number} limit ex: 1, 2, 3, 4
*/
var fetchMagazineRelatedBoxAction = function (_a) {
    var slug = _a.slug, _b = _a.limit, limit = _b === void 0 ? 6 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["h" /* FETCH_MAGAZINE_RELATED_BOXES */],
            payload: { promise: fetchMagazineRelatedBox({ slug: slug, limit: limit }).then(function (res) { return res; }) },
            meta: { slug: slug }
        });
    };
};
/**
* Fetch magazine by tag name
*
* @param {string} tagName ex: halio
* @param {number} page ex: 1, 2, 3, 4
* @param {number} perPage ex: 10, 15, 20
*/
var fetchMagazineByTagNameAction = function (_a) {
    var slug = _a.slug, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["j" /* FETCH_MAGAZINE_TAG */],
            payload: { promise: fetchMagazineByTagName({ slug: slug, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { slug: slug, page: page, perPage: perPage }
        });
    };
};
/**
* Show / Hide search suggest with state param
*
* @param {boolean} state
*/
var showHideMagazineSearchAction = function (state) {
    if (state === void 0) { state = false; }
    return ({
        type: magazine["a" /* DISPLAY_MAGAZINE_SEARCH_SUGGESTION */],
        payload: state
    });
};
/**
* Fetch magazine by keyword
*
* @param {string} keyword ex: halio
* @param {number} page ex: 1, 2, 3, 4
* @param {number} perPage ex: 10, 15, 20
*/
var fetchMagazineByKeywordAction = function (_a) {
    var keyword = _a.keyword, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["b" /* FETCH_MAGAZINE_BY_KEWORD */],
            payload: { promise: fetchMagazineByKeyword({ keyword: keyword, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { keyword: keyword }
        });
    };
};
/**
* Fetch magazine suggestion by keyword
*
* @param {string} keyword ex: halio
* @param {number} limit ex: 1, 2, 3, 4
*/
var fetchMagazineSuggestionByKeywordAction = function (_a) {
    var keyword = _a.keyword, _b = _a.limit, limit = _b === void 0 ? 10 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["i" /* FETCH_MAGAZINE_SUGGESTION_BY_KEWORD */],
            payload: { promise: fetchMagazineSuggestionByKeyword({ keyword: keyword, limit: limit }).then(function (res) { return res; }) },
            meta: { keyword: keyword }
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFnYXppbmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtYWdhemluZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBRUwsaUJBQWlCLEVBQ2pCLHNCQUFzQixFQUN0QixxQkFBcUIsRUFDckIsbUJBQW1CLEVBQ25CLHdCQUF3QixFQUN4Qix1QkFBdUIsRUFDdkIsc0JBQXNCLEVBQ3RCLHNCQUFzQixFQUN0QixnQ0FBZ0MsRUFDakMsTUFBTSxpQkFBaUIsQ0FBQztBQUV6QixPQUFPLEVBQ0wsbUJBQW1CLEVBQ25CLHdCQUF3QixFQUN4Qix1QkFBdUIsRUFDdkIsc0JBQXNCLEVBQ3RCLDRCQUE0QixFQUM1Qiw0QkFBNEIsRUFDNUIsa0JBQWtCLEVBQ2xCLGtDQUFrQyxFQUNsQyx3QkFBd0IsRUFDeEIsbUNBQW1DLEVBQ3BDLE1BQU0sMkJBQTJCLENBQUM7QUFFbkM7Ozs7OztHQU1HO0FBQ0gsTUFBTSxDQUFDLElBQU0sdUJBQXVCLEdBQ2xDLFVBQUMsRUFBZ0Q7UUFBOUMsY0FBSSxFQUFFLG9CQUFPLEVBQUUsY0FBSTtJQUNwQixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsbUJBQW1CO1lBQ3pCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDakYsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUU7U0FDOUIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVCxNQUFNLENBQUMsSUFBTSw0QkFBNEIsR0FDdkM7SUFDRSxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsd0JBQXdCO1lBQzlCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUMvRCxJQUFJLEVBQUUsRUFBRTtTQUNULENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7OztFQU1FO0FBQ0YsTUFBTSxDQUFDLElBQU0sMkJBQTJCLEdBQ3RDLFVBQUMsRUFBZ0M7UUFBOUIsY0FBSSxFQUFFLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUM3QixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsdUJBQXVCO1lBQzdCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxxQkFBcUIsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDckYsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDOUIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0seUJBQXlCLEdBQ3BDLFVBQUMsRUFBUTtRQUFOLGNBQUk7SUFDTCxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsc0JBQXNCO1lBQzVCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDcEUsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUU7U0FDZixDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUdUOzs7O0VBSUU7QUFDRixNQUFNLENBQUMsSUFBTSw4QkFBOEIsR0FDekMsVUFBQyxFQUFRO1FBQU4sY0FBSTtJQUNMLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSw0QkFBNEI7WUFDbEMsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLHdCQUF3QixDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN6RSxJQUFJLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRTtTQUNmLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7O0VBS0U7QUFDRixNQUFNLENBQUMsSUFBTSw2QkFBNkIsR0FDeEMsVUFBQyxFQUFtQjtRQUFqQixjQUFJLEVBQUUsYUFBUyxFQUFULDhCQUFTO0lBQ2hCLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSw0QkFBNEI7WUFDbEMsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLHVCQUF1QixDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUMvRSxJQUFJLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRTtTQUNmLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBR1Q7Ozs7OztFQU1FO0FBQ0YsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsRUFBZ0M7UUFBOUIsY0FBSSxFQUFFLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUM3QixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsa0JBQWtCO1lBQ3hCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxzQkFBc0IsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDdEYsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDOUIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsS0FBYTtJQUFiLHNCQUFBLEVBQUEsYUFBYTtJQUFLLE9BQUEsQ0FBQztRQUNsQixJQUFJLEVBQUUsa0NBQWtDO1FBQ3hDLE9BQU8sRUFBRSxLQUFLO0tBQ2YsQ0FBQztBQUhpQixDQUdqQixDQUFDO0FBRUw7Ozs7OztFQU1FO0FBQ0YsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsRUFBbUM7UUFBakMsb0JBQU8sRUFBRSxZQUFRLEVBQVIsNkJBQVEsRUFBRSxlQUFZLEVBQVosaUNBQVk7SUFDaEMsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLHdCQUF3QjtZQUM5QixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsc0JBQXNCLENBQUMsRUFBRSxPQUFPLFNBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQ3pGLElBQUksRUFBRSxFQUFFLE9BQU8sU0FBQSxFQUFFO1NBQ2xCLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7O0VBS0U7QUFDRixNQUFNLENBQUMsSUFBTSxzQ0FBc0MsR0FDakQsVUFBQyxFQUF1QjtRQUFyQixvQkFBTyxFQUFFLGFBQVUsRUFBViwrQkFBVTtJQUNwQixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsbUNBQW1DO1lBQ3pDLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxnQ0FBZ0MsQ0FBQyxFQUFFLE9BQU8sU0FBQSxFQUFFLEtBQUssT0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDM0YsSUFBSSxFQUFFLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDbEIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUMifQ==

/***/ }),

/***/ 762:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// EXTERNAL MODULE: ./utils/auth.ts + 1 modules
var auth = __webpack_require__(23);

// CONCATENATED MODULE: ./api/shop.ts


var fetchDataHotDeal = function (_a) {
    var _b = _a.limit, limit = _b === void 0 ? 20 : _b;
    return Object(restful_method["b" /* get */])({
        path: "/shop/hot_deal?limit=" + limit,
        description: 'Get data hot deal in home page',
        errorMesssage: "Can't get data. Please try again",
    });
};
var fetchDataHomePage = function () { return Object(restful_method["b" /* get */])({
    path: '/shop',
    description: 'Get group data in home page',
    errorMesssage: "Can't get latest boxs data. Please try again",
}); };
var fetchProductByCategory = function (idCategory, query) {
    if (query === void 0) { query = ''; }
    return Object(restful_method["b" /* get */])({
        path: "/browse_nodes/" + idCategory + ".json" + query,
        description: 'Fetch list product in category | DANH SÁCH SẢN PHẨM TRONG CATEGORY',
        errorMesssage: "Can't get list product by category. Please try again",
    });
};
var getProductDetail = function (idProduct) { return Object(restful_method["b" /* get */])({
    path: "/boxes/" + idProduct + ".json",
    description: 'Get Product Detail | LẤY CHI TIẾT SẢN PHẨM',
    errorMesssage: "Can't get product detail. Please try again",
}); };
;
var generateParams = function (key, value) { return value ? "&" + key + "=" + value : ''; };
var fetchRedeemBoxes = function (_a) {
    var _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 72 : _c, filter = _a.filter, sort = _a.sort;
    var query = "?page=" + page + "&per_page=" + perPage
        + generateParams('filter[brand]', filter && filter.brand)
        + generateParams('filter[category]', filter && filter.category)
        + generateParams('filter[coins_price]', filter && filter.coinsPrice)
        + generateParams('sort[coins_price]', filter && sort && sort.coinsPrice);
    return Object(restful_method["b" /* get */])({
        path: "/boxes/redeems" + query,
        description: 'Fetch list redeem boxes',
        errorMesssage: "Can't fetch data. Please try again",
    });
};
var addWaitList = function (_a) {
    var boxId = _a.boxId;
    var data = {
        csrf_token: Object(auth["c" /* getCsrfToken */])(),
    };
    return Object(restful_method["d" /* post */])({
        path: "/boxes/" + boxId + "/waitlist",
        data: data,
        description: 'Add item intowait list | BOXES - WAIT LIST',
        errorMesssage: "Can't Get listwait list. Please try again",
    });
};
var fetchFeedbackBoxes = function (_a) {
    var productId = _a.productId, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c;
    var query = "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/boxes/" + productId + "/feedbacks" + query,
        description: 'Fetch list feedbacks boxes',
        errorMesssage: "Can't fetch data. Please try again",
    });
};
var fetchSavingSetsBoxes = function (_a) {
    var productId = _a.productId, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c;
    var query = "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/boxes/" + productId + "/saving_sets" + query,
        description: 'Fetch list saving sets boxes',
        errorMesssage: "Can't fetch data. Please try again",
    });
};
var fetchMagazinesBoxes = function (_a) {
    var productId = _a.productId, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c;
    var query = "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/boxes/" + productId + "/magazines" + query,
        description: 'Fetch magazine boxes',
        errorMesssage: "Can't fetch data. Please try again",
    });
};
var fetchRelatedBoxes = function (_a) {
    var productId = _a.productId, _b = _a.limit, limit = _b === void 0 ? 10 : _b;
    var query = "?limit=" + limit;
    return Object(restful_method["b" /* get */])({
        path: "/boxes/" + productId + "/related_boxes" + query,
        description: 'Fetch related boxes',
        errorMesssage: "Can't fetch data. Please try again",
    });
};
var fetchMakeups = function (_a) {
    var boxId = _a.boxId, _b = _a.limit, limit = _b === void 0 ? 10 : _b;
    return Object(restful_method["b" /* get */])({
        path: "/makeups/" + boxId + "?limit=" + limit,
        description: 'Fetch makeups',
        errorMesssage: "Can't fetch data. Please try again",
    });
};
var fetchStoreBoxes = function (_a) {
    var productId = _a.productId;
    return Object(restful_method["b" /* get */])({
        path: "/boxes/" + productId + "/store_boxes",
        description: 'Fetch store boxes',
        errorMesssage: "Can't fetch store boxes. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hvcC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNob3AudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTdDLE1BQU0sQ0FBQyxJQUFNLGdCQUFnQixHQUFHLFVBQUMsRUFBYztRQUFaLGFBQVUsRUFBViwrQkFBVTtJQUFPLE9BQUEsR0FBRyxDQUFDO1FBQ3RELElBQUksRUFBRSwwQkFBd0IsS0FBTztRQUNyQyxXQUFXLEVBQUUsZ0NBQWdDO1FBQzdDLGFBQWEsRUFBRSxrQ0FBa0M7S0FDbEQsQ0FBQztBQUprRCxDQUlsRCxDQUFDO0FBRUgsTUFBTSxDQUFDLElBQU0saUJBQWlCLEdBQUcsY0FBTSxPQUFBLEdBQUcsQ0FBQztJQUN6QyxJQUFJLEVBQUUsT0FBTztJQUNiLFdBQVcsRUFBRSw2QkFBNkI7SUFDMUMsYUFBYSxFQUFFLDhDQUE4QztDQUM5RCxDQUFDLEVBSnFDLENBSXJDLENBQUM7QUFFSCxNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FDakMsVUFBQyxVQUFrQixFQUFFLEtBQVU7SUFBVixzQkFBQSxFQUFBLFVBQVU7SUFBSyxPQUFBLEdBQUcsQ0FBQztRQUN0QyxJQUFJLEVBQUUsbUJBQWlCLFVBQVUsYUFBUSxLQUFPO1FBQ2hELFdBQVcsRUFBRSxvRUFBb0U7UUFDakYsYUFBYSxFQUFFLHNEQUFzRDtLQUN0RSxDQUFDO0FBSmtDLENBSWxDLENBQUM7QUFFTCxNQUFNLENBQUMsSUFBTSxnQkFBZ0IsR0FDM0IsVUFBQyxTQUFpQixJQUFLLE9BQUEsR0FBRyxDQUFDO0lBQ3pCLElBQUksRUFBRSxZQUFVLFNBQVMsVUFBTztJQUNoQyxXQUFXLEVBQUUsNENBQTRDO0lBQ3pELGFBQWEsRUFBRSw0Q0FBNEM7Q0FDNUQsQ0FBQyxFQUpxQixDQUlyQixDQUFDO0FBYUosQ0FBQztBQUVGLElBQU0sY0FBYyxHQUFHLFVBQUMsR0FBRyxFQUFFLEtBQUssSUFBSyxPQUFBLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBSSxHQUFHLFNBQUksS0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQS9CLENBQStCLENBQUM7QUFFdkUsTUFBTSxDQUFDLElBQU0sZ0JBQWdCLEdBQzNCLFVBQUMsRUFBZ0U7UUFBOUQsWUFBUSxFQUFSLDZCQUFRLEVBQUUsZUFBWSxFQUFaLGlDQUFZLEVBQUUsa0JBQU0sRUFBRSxjQUFJO0lBRXJDLElBQU0sS0FBSyxHQUNULFdBQVMsSUFBSSxrQkFBYSxPQUFTO1VBQ2pDLGNBQWMsQ0FBQyxlQUFlLEVBQUUsTUFBTSxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUM7VUFDdkQsY0FBYyxDQUFDLGtCQUFrQixFQUFFLE1BQU0sSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDO1VBQzdELGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxNQUFNLElBQUksTUFBTSxDQUFDLFVBQVUsQ0FBQztVQUNsRSxjQUFjLENBQUMsbUJBQW1CLEVBQUUsTUFBTSxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQ3ZFO0lBRUgsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSxtQkFBaUIsS0FBTztRQUM5QixXQUFXLEVBQUUseUJBQXlCO1FBQ3RDLGFBQWEsRUFBRSxvQ0FBb0M7S0FDcEQsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUosTUFBTSxDQUFDLElBQU0sV0FBVyxHQUN0QixVQUFDLEVBQVM7UUFBUCxnQkFBSztJQUNOLElBQU0sSUFBSSxHQUFHO1FBQ1gsVUFBVSxFQUFFLFlBQVksRUFBRTtLQUMzQixDQUFDO0lBRUYsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNWLElBQUksRUFBRSxZQUFVLEtBQUssY0FBVztRQUNoQyxJQUFJLE1BQUE7UUFDSixXQUFXLEVBQUUsNENBQTRDO1FBQ3pELGFBQWEsRUFBRSwyQ0FBMkM7S0FDM0QsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUosTUFBTSxDQUFDLElBQU0sa0JBQWtCLEdBQzdCLFVBQUMsRUFBcUM7UUFBbkMsd0JBQVMsRUFBRSxZQUFRLEVBQVIsNkJBQVEsRUFBRSxlQUFZLEVBQVosaUNBQVk7SUFDbEMsSUFBTSxLQUFLLEdBQUcsV0FBUyxJQUFJLGtCQUFhLE9BQVMsQ0FBQztJQUVsRCxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLFlBQVUsU0FBUyxrQkFBYSxLQUFPO1FBQzdDLFdBQVcsRUFBRSw0QkFBNEI7UUFDekMsYUFBYSxFQUFFLG9DQUFvQztLQUNwRCxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSixNQUFNLENBQUMsSUFBTSxvQkFBb0IsR0FDL0IsVUFBQyxFQUFxQztRQUFuQyx3QkFBUyxFQUFFLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUNsQyxJQUFNLEtBQUssR0FBRyxXQUFTLElBQUksa0JBQWEsT0FBUyxDQUFDO0lBRWxELE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsWUFBVSxTQUFTLG9CQUFlLEtBQU87UUFDL0MsV0FBVyxFQUFFLDhCQUE4QjtRQUMzQyxhQUFhLEVBQUUsb0NBQW9DO0tBQ3BELENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQztBQUVKLE1BQU0sQ0FBQyxJQUFNLG1CQUFtQixHQUM5QixVQUFDLEVBQXFDO1FBQW5DLHdCQUFTLEVBQUUsWUFBUSxFQUFSLDZCQUFRLEVBQUUsZUFBWSxFQUFaLGlDQUFZO0lBQ2xDLElBQU0sS0FBSyxHQUFHLFdBQVMsSUFBSSxrQkFBYSxPQUFTLENBQUM7SUFFbEQsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSxZQUFVLFNBQVMsa0JBQWEsS0FBTztRQUM3QyxXQUFXLEVBQUUsc0JBQXNCO1FBQ25DLGFBQWEsRUFBRSxvQ0FBb0M7S0FDcEQsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUosTUFBTSxDQUFDLElBQU0saUJBQWlCLEdBQzVCLFVBQUMsRUFBeUI7UUFBdkIsd0JBQVMsRUFBRSxhQUFVLEVBQVYsK0JBQVU7SUFDdEIsSUFBTSxLQUFLLEdBQUcsWUFBVSxLQUFPLENBQUM7SUFFaEMsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSxZQUFVLFNBQVMsc0JBQWlCLEtBQU87UUFDakQsV0FBVyxFQUFFLHFCQUFxQjtRQUNsQyxhQUFhLEVBQUUsb0NBQW9DO0tBQ3BELENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQztBQUVKLE1BQU0sQ0FBQyxJQUFNLFlBQVksR0FBRyxVQUFDLEVBQXFCO1FBQW5CLGdCQUFLLEVBQUUsYUFBVSxFQUFWLCtCQUFVO0lBQU8sT0FBQSxHQUFHLENBQUM7UUFDekQsSUFBSSxFQUFFLGNBQVksS0FBSyxlQUFVLEtBQU87UUFDeEMsV0FBVyxFQUFFLGVBQWU7UUFDNUIsYUFBYSxFQUFFLG9DQUFvQztLQUNwRCxDQUFDO0FBSnFELENBSXJELENBQUM7QUFFSCxNQUFNLENBQUMsSUFBTSxlQUFlLEdBQUcsVUFBQyxFQUFhO1FBQVgsd0JBQVM7SUFBTyxPQUFBLEdBQUcsQ0FBQztRQUNwRCxJQUFJLEVBQUUsWUFBVSxTQUFTLGlCQUFjO1FBQ3ZDLFdBQVcsRUFBRSxtQkFBbUI7UUFDaEMsYUFBYSxFQUFFLDJDQUEyQztLQUMzRCxDQUFDO0FBSmdELENBSWhELENBQUMifQ==
// EXTERNAL MODULE: ./constants/api/shop.ts
var shop = __webpack_require__(13);

// CONCATENATED MODULE: ./action/shop.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fetchDataHomePageAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return fetchProductByCategoryAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "m", function() { return updateProductNameMobileAction; });
/* unused harmony export updateCategoryFilterStateAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "l", function() { return getProductDetailAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return fetchRedeemBoxesAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return addToWaitListAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return fetchFeedbackBoxesAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "j", function() { return fetchSavingSetsBoxesAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return fetchMagazinesBoxesAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return fetchRelatedBoxesAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return fetchDataHotDealAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return fetchMakeupsAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "k", function() { return fetchStoreBoxesAction; });


/** Get collection data in home page */
var fetchDataHomePageAction = function () { return function (dispatch, getState) {
    return dispatch({
        type: shop["b" /* FECTH_DATA_HOME_PAGE */],
        payload: { promise: fetchDataHomePage().then(function (res) { return res; }) },
    });
}; };
/** Get Product list by cagegory old version */
/*
export const fetchProductByCategoryAction =
  (categoryFilter: any) => {
    const [idCategory, params] = categoryFilterApiFormat(categoryFilter);

    return (dispatch, getState) => dispatch({
      type: FETCH_PRODUCT_BY_CATEGORY,
      payload: { promise: fetchProductByCategory(idCategory, params).then(res => res) },
      meta: { metaFilter: { idCategory, params } }
    });
  };
  */
/** Get Product list by cagegory new version */
var fetchProductByCategoryAction = function (_a) {
    var idCategory = _a.idCategory, searchQuery = _a.searchQuery;
    return function (dispatch, getState) { return dispatch({
        type: shop["g" /* FETCH_PRODUCT_BY_CATEGORY */],
        payload: { promise: fetchProductByCategory(idCategory, searchQuery).then(function (res) { return res; }) },
        meta: { metaFilter: { idCategory: idCategory, searchQuery: searchQuery } }
    }); };
};
/**  Update Product name cho Product detail trên Mobile */
var updateProductNameMobileAction = function (productName) { return ({
    type: shop["n" /* UPDATE_PRODUCT_NAME_MOBILE */],
    payload: productName
}); };
/**  Update filter for category */
var updateCategoryFilterStateAction = function (categoryFilter) { return ({
    type: shop["m" /* UPDATE_CATEGORY_FILTER_STATE */],
    payload: categoryFilter
}); };
/** Get product detail */
var getProductDetailAction = function (productId) {
    return function (dispatch, getState) {
        return dispatch({
            type: shop["l" /* GET_PRODUCT_DETAIL */],
            payload: { promise: getProductDetail(productId).then(function (res) { return res; }) },
            meta: { productId: productId }
        });
    };
};
/** Fetch redeem boxes list */
var fetchRedeemBoxesAction = function (_a) {
    var page = _a.page, _b = _a.perPage, perPage = _b === void 0 ? 72 : _b, filter = _a.filter, sort = _a.sort;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["h" /* FETCH_REDEEM_BOXES */],
            payload: { promise: fetchRedeemBoxes({ page: page, perPage: perPage, filter: filter, sort: sort }).then(function (res) { return res; }) },
            meta: { page: page, perPage: perPage }
        });
    };
};
/** Add product into wait list */
var addToWaitListAction = function (_a) {
    var boxId = _a.boxId, _b = _a.slug, slug = _b === void 0 ? '' : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["a" /* ADD_WAIT_LIST */],
            payload: { promise: addWaitList({ boxId: boxId }).then(function (res) { return res; }) },
            dispatch: dispatch,
            meta: { slug: slug }
        });
    };
};
/**
 * Fetch feebbacks boxes list by id or slug of product
 *
 * @param{string} id or slug of product
 * @param{number} page
 * @param{string} perPage
 */
var fetchFeedbackBoxesAction = function (_a) {
    var productId = _a.productId, page = _a.page, _b = _a.perPage, perPage = _b === void 0 ? 10 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["d" /* FETCH_FEEDBACK_BOXES */],
            payload: { promise: fetchFeedbackBoxes({ productId: productId, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { productId: productId, page: page, perPage: perPage }
        });
    };
};
/**
 * Fetch saving sets boxes list by id or slug of product
 *
 * @param{string} id or slug of product
 * @param{number} page
 * @param{string} perPage
 */
var fetchSavingSetsBoxesAction = function (_a) {
    var productId = _a.productId, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["j" /* FETCH_SAVING_SETS_BOXES */],
            payload: { promise: fetchSavingSetsBoxes({ productId: productId, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { productId: productId, page: page, perPage: perPage }
        });
    };
};
/**
 * Fetch magazine boxes list by id or slug of product
 *
 * @param{string} id or slug of product
 * @param{number} page
 * @param{number} perPage
 */
var fetchMagazinesBoxesAction = function (_a) {
    var productId = _a.productId, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["e" /* FETCH_MAGAZINES_BOXES */],
            payload: { promise: fetchMagazinesBoxes({ productId: productId, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { productId: productId, page: page, perPage: perPage }
        });
    };
};
/**
* Fetch related boxes list by id or slug of product
*
* @param{string} id or slug of product
* @param{limit} number
*/
var fetchRelatedBoxesAction = function (_a) {
    var productId = _a.productId, _b = _a.limit, limit = _b === void 0 ? 10 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["i" /* FETCH_RELATED_BOXES */],
            payload: { promise: fetchRelatedBoxes({ productId: productId, limit: limit }).then(function (res) { return res; }) },
            meta: { productId: productId }
        });
    };
};
var fetchDataHotDealAction = function (_a) {
    var limit = _a.limit;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["c" /* FECTH_DATA_HOT_DEAL */],
            payload: { promise: fetchDataHotDeal({ limit: limit }).then(function (res) { return res; }) },
        });
    };
};
/**
* Fetch makeups by id or slug of product
*
* @param{string} id or slug of product
* @param{limit} number
*/
var fetchMakeupsAction = function (_a) {
    var boxId = _a.boxId, _b = _a.limit, limit = _b === void 0 ? 10 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["f" /* FETCH_MAKEUPS */],
            payload: { promise: fetchMakeups({ boxId: boxId, limit: limit }).then(function (res) { return res; }) },
            meta: { boxId: boxId }
        });
    };
};
/**
* Fetch store boxes of box detail by product id
*
* @param{string} id or slug of product
*/
var fetchStoreBoxesAction = function (_a) {
    var productId = _a.productId;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["k" /* FETCH_STORE_BOXES */],
            payload: { promise: fetchStoreBoxes({ productId: productId }).then(function (res) { return res; }) },
            meta: { productId: productId }
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hvcC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNob3AudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUEsT0FBTyxFQUNMLGdCQUFnQixFQUNoQixzQkFBc0IsRUFDdEIsaUJBQWlCLEVBQ2pCLGdCQUFnQixFQUVoQixnQkFBZ0IsRUFDaEIsV0FBVyxFQUNYLGtCQUFrQixFQUNsQixvQkFBb0IsRUFDcEIsbUJBQW1CLEVBQ25CLGlCQUFpQixFQUNqQixZQUFZLEVBQ1osZUFBZSxFQUNoQixNQUFNLGFBQWEsQ0FBQztBQUVyQixPQUFPLEVBQ0wsbUJBQW1CLEVBQ25CLG9CQUFvQixFQUNwQix5QkFBeUIsRUFDekIsNEJBQTRCLEVBQzVCLDBCQUEwQixFQUMxQixrQkFBa0IsRUFDbEIsa0JBQWtCLEVBQ2xCLGFBQWEsRUFDYixvQkFBb0IsRUFDcEIsdUJBQXVCLEVBQ3ZCLHFCQUFxQixFQUNyQixtQkFBbUIsRUFDbkIsYUFBYSxFQUNiLGlCQUFpQixFQUNsQixNQUFNLHVCQUF1QixDQUFDO0FBRS9CLHVDQUF1QztBQUN2QyxNQUFNLENBQUMsSUFBTSx1QkFBdUIsR0FDbEMsY0FBTSxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7SUFDdkIsT0FBQSxRQUFRLENBQUM7UUFDUCxJQUFJLEVBQUUsb0JBQW9CO1FBQzFCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtLQUMzRCxDQUFDO0FBSEYsQ0FHRSxFQUpFLENBSUYsQ0FBQztBQUVQLCtDQUErQztBQUMvQzs7Ozs7Ozs7Ozs7SUFXSTtBQUVKLCtDQUErQztBQUMvQyxNQUFNLENBQUMsSUFBTSw0QkFBNEIsR0FBRyxVQUFDLEVBQTJCO1FBQXpCLDBCQUFVLEVBQUUsNEJBQVc7SUFFcEUsTUFBTSxDQUFDLFVBQUMsUUFBUSxFQUFFLFFBQVEsSUFBSyxPQUFBLFFBQVEsQ0FBQztRQUN0QyxJQUFJLEVBQUUseUJBQXlCO1FBQy9CLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxzQkFBc0IsQ0FBQyxVQUFVLEVBQUUsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1FBQ3RGLElBQUksRUFBRSxFQUFFLFVBQVUsRUFBRSxFQUFFLFVBQVUsWUFBQSxFQUFFLFdBQVcsYUFBQSxFQUFFLEVBQUU7S0FDbEQsQ0FBQyxFQUo2QixDQUk3QixDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUYsMERBQTBEO0FBQzFELE1BQU0sQ0FBQyxJQUFNLDZCQUE2QixHQUN4QyxVQUFDLFdBQWdCLElBQUssT0FBQSxDQUFDO0lBQ3JCLElBQUksRUFBRSwwQkFBMEI7SUFDaEMsT0FBTyxFQUFFLFdBQVc7Q0FDckIsQ0FBQyxFQUhvQixDQUdwQixDQUFDO0FBRUwsa0NBQWtDO0FBQ2xDLE1BQU0sQ0FBQyxJQUFNLCtCQUErQixHQUMxQyxVQUFDLGNBQW1CLElBQUssT0FBQSxDQUFDO0lBQ3hCLElBQUksRUFBRSw0QkFBNEI7SUFDbEMsT0FBTyxFQUFFLGNBQWM7Q0FDeEIsQ0FBQyxFQUh1QixDQUd2QixDQUFDO0FBRUwseUJBQXlCO0FBQ3pCLE1BQU0sQ0FBQyxJQUFNLHNCQUFzQixHQUNqQyxVQUFDLFNBQWlCO0lBQ2hCLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxrQkFBa0I7WUFDeEIsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUNsRSxJQUFJLEVBQUUsRUFBRSxTQUFTLFdBQUEsRUFBRTtTQUNwQixDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUVULDhCQUE4QjtBQUM5QixNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FDakMsVUFBQyxFQUE0RDtRQUExRCxjQUFJLEVBQUUsZUFBWSxFQUFaLGlDQUFZLEVBQUUsa0JBQU0sRUFBRSxjQUFJO0lBQ2pDLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxrQkFBa0I7WUFDeEIsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLGdCQUFnQixDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsTUFBTSxRQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN4RixJQUFJLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRTtTQUN4QixDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUVULGlDQUFpQztBQUNqQyxNQUFNLENBQUMsSUFBTSxtQkFBbUIsR0FDOUIsVUFBQyxFQUFvQjtRQUFsQixnQkFBSyxFQUFFLFlBQVMsRUFBVCw4QkFBUztJQUNqQixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsYUFBYTtZQUNuQixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsV0FBVyxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUM3RCxRQUFRLFVBQUE7WUFDUixJQUFJLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRTtTQUNmLENBQUM7SUFMRixDQUtFO0FBTkosQ0FNSSxDQUFDO0FBRVQ7Ozs7OztHQU1HO0FBQ0gsTUFBTSxDQUFDLElBQU0sd0JBQXdCLEdBQ25DLFVBQUMsRUFBaUM7UUFBL0Isd0JBQVMsRUFBRSxjQUFJLEVBQUUsZUFBWSxFQUFaLGlDQUFZO0lBQzlCLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxvQkFBb0I7WUFDMUIsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLGtCQUFrQixDQUFDLEVBQUUsU0FBUyxXQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN2RixJQUFJLEVBQUUsRUFBRSxTQUFTLFdBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRTtTQUNuQyxDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUVUOzs7Ozs7R0FNRztBQUNILE1BQU0sQ0FBQyxJQUFNLDBCQUEwQixHQUNyQyxVQUFDLEVBQXFDO1FBQW5DLHdCQUFTLEVBQUUsWUFBUSxFQUFSLDZCQUFRLEVBQUUsZUFBWSxFQUFaLGlDQUFZO0lBQ2xDLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSx1QkFBdUI7WUFDN0IsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLG9CQUFvQixDQUFDLEVBQUUsU0FBUyxXQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN6RixJQUFJLEVBQUUsRUFBRSxTQUFTLFdBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRTtTQUNuQyxDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUVUOzs7Ozs7R0FNRztBQUNILE1BQU0sQ0FBQyxJQUFNLHlCQUF5QixHQUNwQyxVQUFDLEVBQXFDO1FBQW5DLHdCQUFTLEVBQUUsWUFBUSxFQUFSLDZCQUFRLEVBQUUsZUFBWSxFQUFaLGlDQUFZO0lBQ2xDLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxxQkFBcUI7WUFDM0IsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLG1CQUFtQixDQUFDLEVBQUUsU0FBUyxXQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN4RixJQUFJLEVBQUUsRUFBRSxTQUFTLFdBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRTtTQUNuQyxDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUVUOzs7OztFQUtFO0FBQ0YsTUFBTSxDQUFDLElBQU0sdUJBQXVCLEdBQ2xDLFVBQUMsRUFBeUI7UUFBdkIsd0JBQVMsRUFBRSxhQUFVLEVBQVYsK0JBQVU7SUFDdEIsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLG1CQUFtQjtZQUN6QixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsaUJBQWlCLENBQUMsRUFBRSxTQUFTLFdBQUEsRUFBRSxLQUFLLE9BQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQzlFLElBQUksRUFBRSxFQUFFLFNBQVMsV0FBQSxFQUFFO1NBQ3BCLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQsTUFBTSxDQUFDLElBQU0sc0JBQXNCLEdBQUcsVUFBQyxFQUFTO1FBQVAsZ0JBQUs7SUFDNUMsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLG1CQUFtQjtZQUN6QixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1NBQ25FLENBQUM7SUFIRixDQUdFO0FBSkosQ0FJSSxDQUFDO0FBRVA7Ozs7O0VBS0U7QUFDRixNQUFNLENBQUMsSUFBTSxrQkFBa0IsR0FDN0IsVUFBQyxFQUFxQjtRQUFuQixnQkFBSyxFQUFFLGFBQVUsRUFBViwrQkFBVTtJQUNsQixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsYUFBYTtZQUNuQixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsWUFBWSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUNyRSxJQUFJLEVBQUUsRUFBRSxLQUFLLE9BQUEsRUFBRTtTQUNoQixDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUVUOzs7O0VBSUU7QUFDRixNQUFNLENBQUMsSUFBTSxxQkFBcUIsR0FBRyxVQUFDLEVBQWE7UUFBWCx3QkFBUztJQUMvQyxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsaUJBQWlCO1lBQ3ZCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxlQUFlLENBQUMsRUFBRSxTQUFTLFdBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQ3JFLElBQUksRUFBRSxFQUFFLFNBQVMsV0FBQSxFQUFFO1NBQ3BCLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDIn0=

/***/ }),

/***/ 764:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// EXTERNAL MODULE: ./utils/auth.ts + 1 modules
var auth = __webpack_require__(23);

// CONCATENATED MODULE: ./api/activity-feed.ts


;
var fecthActivityFeedList = function (_a) {
    var _b = _a.userId, userId = _b === void 0 ? 0 : _b, _c = _a.limit, limit = _c === void 0 ? 20 : _c, _d = _a.currentId, currentId = _d === void 0 ? 0 : _d, _e = _a.feedType, feedType = _e === void 0 ? '' : _e;
    var query = "?limit=" + limit
        + (!!currentId ? "&current_id=" + currentId : '')
        + (!!userId ? "&user_id=" + userId : '')
        + (!!feedType ? "&feed_type=" + feedType : '');
    return Object(restful_method["b" /* get */])({
        path: "/activity_feeds" + query,
        description: "Fetch list activity feed",
        errorMesssage: "Can't fetch data activity feeds. Please try again",
    });
};
;
var fecthActivityFeedCommentList = function (_a) {
    var id = _a.id, lastCommentId = _a.lastCommentId, page = _a.page, perPage = _a.perPage;
    var lastCommentParams = 'undefined' !== typeof lastCommentId ? "&last_comment_id=" + lastCommentId : '';
    var query = "?&page=" + page + "&per_page=" + perPage + lastCommentParams;
    return Object(restful_method["b" /* get */])({
        path: "/activity_feeds/" + id + "/comments" + query,
        description: "Fetch activity feed comment list",
        errorMesssage: "Can't fetch activity feed comment list. Please try again",
    });
};
;
var addActivityFeedComment = function (_a) {
    var id = _a.id, content = _a.content, lastCommentId = _a.lastCommentId;
    return Object(restful_method["d" /* post */])({
        path: "/activity_feeds/" + id + "/comments",
        data: {
            csrf_token: Object(auth["c" /* getCsrfToken */])(),
            content: content,
            last_comment_id: lastCommentId,
        },
        description: "Add activity feed comment",
        errorMesssage: "Can't add activity feed comment. Please try again",
    });
};
;
var addActivityFeedLike = function (_a) {
    var id = _a.id;
    return Object(restful_method["d" /* post */])({
        path: "/activity_feeds/" + id + "/like",
        data: {
            csrf_token: Object(auth["c" /* getCsrfToken */])()
        },
        description: "Add activity feed like",
        errorMesssage: "Can't add activity feed like. Please try again",
    });
};
;
var deleteActivityFeedLike = function (_a) {
    var id = _a.id;
    return Object(restful_method["a" /* del */])({
        path: "/activity_feeds/" + id + "/unlike",
        data: { csrf_token: Object(auth["c" /* getCsrfToken */])() },
        description: "Delete activity feed like",
        errorMesssage: "Can't delete activity feed like. Please try again",
    });
};
var fetchActivityFeedDetail = function (_a) {
    var feedId = _a.feedId;
    return Object(restful_method["b" /* get */])({
        path: "/activity_feeds/" + feedId,
        description: "Fetch activity feed detail",
        errorMesssage: "Can't fetch activity feed detail. Please try again",
    });
};
/**
 * Get Collection (Top feed)
 *
 * @param {number} page ex: 1
 * @param {number} perPage ex: 12
 */
var getCollection = function (_a) {
    var _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    var query = "?&page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/community" + query,
        description: "Get collection",
        errorMesssage: "Can't get collection. Please try again",
    });
};
/**
* Get Collection detail (Top feed)
*
* @param {number} id ex: 1
*/
var getCollectionDetail = function (_a) {
    var id = _a.id;
    return Object(restful_method["b" /* get */])({
        path: "/community/" + id,
        description: "Get collection detail",
        errorMesssage: "Can't get collection detail. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0aXZpdHktZmVlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFjdGl2aXR5LWZlZWQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDMUQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQVE1QyxDQUFDO0FBRUYsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQ2hDLFVBQUMsRUFBZ0Y7UUFBOUUsY0FBVSxFQUFWLCtCQUFVLEVBQUUsYUFBVSxFQUFWLCtCQUFVLEVBQUUsaUJBQWEsRUFBYixrQ0FBYSxFQUFFLGdCQUFhLEVBQWIsa0NBQWE7SUFDckQsSUFBTSxLQUFLLEdBQUcsWUFBVSxLQUFPO1VBQzNCLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsaUJBQWUsU0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7VUFDL0MsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxjQUFZLE1BQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1VBQ3RDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsZ0JBQWMsUUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUVqRCxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLG9CQUFrQixLQUFPO1FBQy9CLFdBQVcsRUFBRSwwQkFBMEI7UUFDdkMsYUFBYSxFQUFFLG1EQUFtRDtLQUNuRSxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFRSCxDQUFDO0FBRUYsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsRUFBbUU7UUFBakUsVUFBRSxFQUFFLGdDQUFhLEVBQUUsY0FBSSxFQUFFLG9CQUFPO0lBQ2pDLElBQU0saUJBQWlCLEdBQUcsV0FBVyxLQUFLLE9BQU8sYUFBYSxDQUFDLENBQUMsQ0FBQyxzQkFBb0IsYUFBZSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDMUcsSUFBTSxLQUFLLEdBQUcsWUFBVSxJQUFJLGtCQUFhLE9BQU8sR0FBRyxpQkFBbUIsQ0FBQztJQUV2RSxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLHFCQUFtQixFQUFFLGlCQUFZLEtBQU87UUFDOUMsV0FBVyxFQUFFLGtDQUFrQztRQUMvQyxhQUFhLEVBQUUsMERBQTBEO0tBQzFFLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQztBQU9ILENBQUM7QUFFRixNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FDakMsVUFBQyxFQUE0RDtRQUExRCxVQUFFLEVBQUUsb0JBQU8sRUFBRSxnQ0FBYTtJQUFxQyxPQUFBLElBQUksQ0FBQztRQUNyRSxJQUFJLEVBQUUscUJBQW1CLEVBQUUsY0FBVztRQUN0QyxJQUFJLEVBQUU7WUFDSixVQUFVLEVBQUUsWUFBWSxFQUFFO1lBQzFCLE9BQU8sRUFBRSxPQUFPO1lBQ2hCLGVBQWUsRUFBRSxhQUFhO1NBQy9CO1FBQ0QsV0FBVyxFQUFFLDJCQUEyQjtRQUN4QyxhQUFhLEVBQUUsbURBQW1EO0tBQ25FLENBQUM7QUFUZ0UsQ0FTaEUsQ0FBQztBQUtKLENBQUM7QUFFRixNQUFNLENBQUMsSUFBTSxtQkFBbUIsR0FBRyxVQUFDLEVBQWlDO1FBQS9CLFVBQUU7SUFBa0MsT0FBQSxJQUFJLENBQUM7UUFDN0UsSUFBSSxFQUFFLHFCQUFtQixFQUFFLFVBQU87UUFDbEMsSUFBSSxFQUFFO1lBQ0osVUFBVSxFQUFFLFlBQVksRUFBRTtTQUMzQjtRQUNELFdBQVcsRUFBRSx3QkFBd0I7UUFDckMsYUFBYSxFQUFFLGdEQUFnRDtLQUNoRSxDQUFDO0FBUHdFLENBT3hFLENBQUM7QUFLRixDQUFDO0FBRUYsTUFBTSxDQUFDLElBQU0sc0JBQXNCLEdBQUcsVUFBQyxFQUFvQztRQUFsQyxVQUFFO0lBQXFDLE9BQUEsR0FBRyxDQUFDO1FBQ2xGLElBQUksRUFBRSxxQkFBbUIsRUFBRSxZQUFTO1FBQ3BDLElBQUksRUFBRSxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsRUFBRTtRQUNwQyxXQUFXLEVBQUUsMkJBQTJCO1FBQ3hDLGFBQWEsRUFBRSxtREFBbUQ7S0FDbkUsQ0FBQztBQUw4RSxDQUs5RSxDQUFDO0FBRUgsTUFBTSxDQUFDLElBQU0sdUJBQXVCLEdBQUcsVUFBQyxFQUFVO1FBQVIsa0JBQU07SUFBTyxPQUFBLEdBQUcsQ0FBQztRQUN6RCxJQUFJLEVBQUUscUJBQW1CLE1BQVE7UUFDakMsV0FBVyxFQUFFLDRCQUE0QjtRQUN6QyxhQUFhLEVBQUUsb0RBQW9EO0tBQ3BFLENBQUM7QUFKcUQsQ0FJckQsQ0FBQztBQUVIOzs7OztHQUtHO0FBQ0gsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUN4QixVQUFDLEVBQTBCO1FBQXhCLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUN2QixJQUFNLEtBQUssR0FBRyxZQUFVLElBQUksa0JBQWEsT0FBUyxDQUFDO0lBRW5ELE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsZUFBYSxLQUFPO1FBQzFCLFdBQVcsRUFBRSxnQkFBZ0I7UUFDN0IsYUFBYSxFQUFFLHdDQUF3QztLQUN4RCxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSjs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0sbUJBQW1CLEdBQUcsVUFBQyxFQUFNO1FBQUosVUFBRTtJQUFPLE9BQUEsR0FBRyxDQUFDO1FBQ2pELElBQUksRUFBRSxnQkFBYyxFQUFJO1FBQ3hCLFdBQVcsRUFBRSx1QkFBdUI7UUFDcEMsYUFBYSxFQUFFLCtDQUErQztLQUMvRCxDQUFDO0FBSjZDLENBSTdDLENBQUMifQ==
// EXTERNAL MODULE: ./constants/api/activity-feed.ts
var activity_feed = __webpack_require__(27);

// CONCATENATED MODULE: ./action/activity-feed.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return fecthActivityFeedListAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return fecthActivityFeedCommentListAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return addActivityFeedCommentAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return addActivityFeedLikeAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return deleteActivityFeedLikeAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return fetchActivityFeedDetailAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return getCollectionAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return getCollectionDetailAction; });


/**
 * Fetch activity feed list by filter (limit & current_id)
 *
 * @param {number} limit default 20
 * @param {number} currentId
 */
var fecthActivityFeedListAction = function (_a) {
    var _b = _a.limit, limit = _b === void 0 ? 20 : _b, _c = _a.currentId, currentId = _c === void 0 ? 0 : _c, _d = _a.userId, userId = _d === void 0 ? 0 : _d, _e = _a.feedType, feedType = _e === void 0 ? '' : _e;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["f" /* FETCH_ACTIVITY_FEED_LIST */],
            payload: { promise: fecthActivityFeedList({ limit: limit, currentId: currentId, userId: userId, feedType: feedType }).then(function (res) { return res; }) },
            meta: { metaFilter: { limit: limit, currentId: currentId } }
        });
    };
};
/**
* Fetch activity feed comment list
*
* @param {number} id
* @param {number} lastCommentId
* @param {number} page
* @param {number} perPage
*/
var fecthActivityFeedCommentListAction = function (_a) {
    var id = _a.id, lastCommentId = _a.lastCommentId, page = _a.page, perPage = _a.perPage;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["d" /* FETCH_ACTIVITY_FEED_COMMENT_LIST */],
            payload: { promise: fecthActivityFeedCommentList({ id: id, lastCommentId: lastCommentId, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
/**
* Delete activity feed comment
*
* @param {number} id
* @param {string} content
* @param {number} lastCommentId
*/
var addActivityFeedCommentAction = function (_a) {
    var id = _a.id, content = _a.content, lastCommentId = _a.lastCommentId;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["a" /* ADD_ACTIVITY_FEED_COMMENT */],
            payload: { promise: addActivityFeedComment({ id: id, content: content, lastCommentId: lastCommentId }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
/**
* Add activity feed like
*
* @param {number} id
*/
var addActivityFeedLikeAction = function (_a) {
    var id = _a.id;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["b" /* ADD_ACTIVITY_FEED_LIKE */],
            payload: { promise: addActivityFeedLike({ id: id }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
/**
* Delete activity feed like
*
* @param {number} id
*/
var deleteActivityFeedLikeAction = function (_a) {
    var id = _a.id;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["c" /* DELETE_ACTIVITY_FEED_LIKE */],
            payload: { promise: deleteActivityFeedLike({ id: id }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
/**
* Fetch activity feed detail by id
*
* @param {number} feedId
*/
var fetchActivityFeedDetailAction = function (_a) {
    var feedId = _a.feedId;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["e" /* FETCH_ACTIVITY_FEED_DETAIL */],
            payload: { promise: fetchActivityFeedDetail({ feedId: feedId }).then(function (res) { return res; }) },
            meta: { feedId: feedId }
        });
    };
};
/**
 * Get Collection Action (Top feed)
 *
 * @param {number} page ex: 1
 * @param {number} perPage ex: 12
 */
var getCollectionAction = function (_a) {
    var _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["g" /* GET_COLLECTION */],
            payload: { promise: getCollection({ page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: {}
        });
    };
};
/**
* Get Collection Detail Action (Top feed)
*
* @param {number} id ex: 1
*/
var getCollectionDetailAction = function (_a) {
    var id = _a.id;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["h" /* GET_COLLECTION_DETAIL */],
            payload: { promise: getCollectionDetail({ id: id }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0aXZpdHktZmVlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFjdGl2aXR5LWZlZWQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUVMLHFCQUFxQixFQUVyQiw0QkFBNEIsRUFFNUIsc0JBQXNCLEVBRXRCLG1CQUFtQixFQUVuQixzQkFBc0IsRUFDdEIsdUJBQXVCLEVBQ3ZCLGFBQWEsRUFDYixtQkFBbUIsRUFDcEIsTUFBTSxzQkFBc0IsQ0FBQztBQUU5QixPQUFPLEVBQ0wsd0JBQXdCLEVBQ3hCLGdDQUFnQyxFQUNoQyx5QkFBeUIsRUFDekIsc0JBQXNCLEVBQ3RCLHlCQUF5QixFQUN6QiwwQkFBMEIsRUFDMUIsY0FBYyxFQUNkLHFCQUFxQixHQUN0QixNQUFNLGdDQUFnQyxDQUFDO0FBR3hDOzs7OztHQUtHO0FBQ0gsTUFBTSxDQUFDLElBQU0sMkJBQTJCLEdBQ3RDLFVBQUMsRUFBZ0Y7UUFBOUUsYUFBVSxFQUFWLCtCQUFVLEVBQUUsaUJBQWEsRUFBYixrQ0FBYSxFQUFFLGNBQVUsRUFBViwrQkFBVSxFQUFFLGdCQUFhLEVBQWIsa0NBQWE7SUFDckQsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLHdCQUF3QjtZQUM5QixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUscUJBQXFCLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxTQUFTLFdBQUEsRUFBRSxNQUFNLFFBQUEsRUFBRSxRQUFRLFVBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQ3BHLElBQUksRUFBRSxFQUFFLFVBQVUsRUFBRSxFQUFFLEtBQUssT0FBQSxFQUFFLFNBQVMsV0FBQSxFQUFFLEVBQUU7U0FDM0MsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7Ozs7OztFQU9FO0FBQ0YsTUFBTSxDQUFDLElBQU0sa0NBQWtDLEdBQzdDLFVBQUMsRUFBbUU7UUFBakUsVUFBRSxFQUFFLGdDQUFhLEVBQUUsY0FBSSxFQUFFLG9CQUFPO0lBQ2pDLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxnQ0FBZ0M7WUFDdEMsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLDRCQUE0QixDQUFDLEVBQUUsRUFBRSxJQUFBLEVBQUUsYUFBYSxlQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN6RyxJQUFJLEVBQUUsRUFBRSxFQUFFLElBQUEsRUFBRTtTQUNiLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7OztFQU1FO0FBQ0YsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsRUFBNEQ7UUFBMUQsVUFBRSxFQUFFLG9CQUFPLEVBQUUsZ0NBQWE7SUFDM0IsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLHlCQUF5QjtZQUMvQixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsc0JBQXNCLENBQUMsRUFBRSxFQUFFLElBQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxhQUFhLGVBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQzdGLElBQUksRUFBRSxFQUFFLEVBQUUsSUFBQSxFQUFFO1NBQ2IsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0seUJBQXlCLEdBQ3BDLFVBQUMsRUFBaUM7UUFBL0IsVUFBRTtJQUNILE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxzQkFBc0I7WUFDNUIsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLG1CQUFtQixDQUFDLEVBQUUsRUFBRSxJQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUNsRSxJQUFJLEVBQUUsRUFBRSxFQUFFLElBQUEsRUFBRTtTQUNiLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7RUFJRTtBQUNGLE1BQU0sQ0FBQyxJQUFNLDRCQUE0QixHQUN2QyxVQUFDLEVBQW9DO1FBQWxDLFVBQUU7SUFDSCxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUseUJBQXlCO1lBQy9CLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxzQkFBc0IsQ0FBQyxFQUFFLEVBQUUsSUFBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDckUsSUFBSSxFQUFFLEVBQUUsRUFBRSxJQUFBLEVBQUU7U0FDYixDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUdUOzs7O0VBSUU7QUFDRixNQUFNLENBQUMsSUFBTSw2QkFBNkIsR0FDeEMsVUFBQyxFQUFVO1FBQVIsa0JBQU07SUFDUCxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsMEJBQTBCO1lBQ2hDLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSx1QkFBdUIsQ0FBQyxFQUFFLE1BQU0sUUFBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDMUUsSUFBSSxFQUFFLEVBQUUsTUFBTSxRQUFBLEVBQUU7U0FDakIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFHVDs7Ozs7R0FLRztBQUVILE1BQU0sQ0FBQyxJQUFNLG1CQUFtQixHQUM5QixVQUFDLEVBQTBCO1FBQXhCLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUFPLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqRCxPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxjQUFjO1lBQ3BCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxhQUFhLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQ3ZFLElBQUksRUFBRSxFQUFFO1NBQ1QsQ0FBQztJQUpGLENBSUU7QUFMNEIsQ0FLNUIsQ0FBQztBQUVQOzs7O0VBSUU7QUFFRixNQUFNLENBQUMsSUFBTSx5QkFBeUIsR0FDcEMsVUFBQyxFQUFNO1FBQUosVUFBRTtJQUFPLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUM3QixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxxQkFBcUI7WUFDM0IsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLG1CQUFtQixDQUFDLEVBQUUsRUFBRSxJQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUNsRSxJQUFJLEVBQUUsRUFBRSxFQUFFLElBQUEsRUFBRTtTQUNiLENBQUM7SUFKRixDQUlFO0FBTFEsQ0FLUixDQUFDIn0=

/***/ }),

/***/ 765:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/image.ts
var utils_image = __webpack_require__(348);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./components/magazine/image-slider/initialize.tsx
var DEFAULT_PROPS = {
    data: [],
    type: 'full',
    title: '',
    column: 3,
    showViewMore: false,
    showHeader: true,
    isCustomTitle: false,
    style: {},
    titleStyle: {}
};
var INITIAL_STATE = function (data) { return ({
    magazineList: data || [],
    magazineSlide: [],
    magazineSlideSelected: {},
    countChangeSlide: 0,
    firstInit: false
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLElBQUksRUFBRSxNQUFNO0lBQ1osS0FBSyxFQUFFLEVBQUU7SUFDVCxNQUFNLEVBQUUsQ0FBQztJQUNULFlBQVksRUFBRSxLQUFLO0lBQ25CLFVBQVUsRUFBRSxJQUFJO0lBQ2hCLGFBQWEsRUFBRSxLQUFLO0lBQ3BCLEtBQUssRUFBRSxFQUFFO0lBQ1QsVUFBVSxFQUFFLEVBQUU7Q0FDTCxDQUFDO0FBRVosTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHLFVBQUMsSUFBUyxJQUFLLE9BQUEsQ0FBQztJQUMzQyxZQUFZLEVBQUUsSUFBSSxJQUFJLEVBQUU7SUFDeEIsYUFBYSxFQUFFLEVBQUU7SUFDakIscUJBQXFCLEVBQUUsRUFBRTtJQUN6QixnQkFBZ0IsRUFBRSxDQUFDO0lBQ25CLFNBQVMsRUFBRSxLQUFLO0NBQ04sQ0FBQSxFQU5nQyxDQU1oQyxDQUFDIn0=
// EXTERNAL MODULE: ./container/layout/main-block/index.tsx
var main_block = __webpack_require__(746);

// EXTERNAL MODULE: ./components/ui/loading-placeholder/index.tsx + 4 modules
var loading_placeholder = __webpack_require__(750);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// EXTERNAL MODULE: ./style/component.ts
var component = __webpack_require__(744);

// CONCATENATED MODULE: ./components/magazine/image-slider-item/initialize.tsx
var initialize_DEFAULT_PROPS = {
    item: [],
    type: '',
    column: 4
};
var initialize_INITIAL_STATE = {
    isLoadedImage: false
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLElBQUksRUFBRSxFQUFFO0lBQ1IsTUFBTSxFQUFFLENBQUM7Q0FDQSxDQUFDO0FBRVosTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHO0lBQzNCLGFBQWEsRUFBRSxLQUFLO0NBQ1gsQ0FBQyJ9
// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ../node_modules/react-on-screen/lib/index.js
var lib = __webpack_require__(758);
var lib_default = /*#__PURE__*/__webpack_require__.n(lib);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/magazine/image-slider-item/style.tsx

var generateSwitchStyle = function (mobile, desktop) {
    var switchStyle = {
        MOBILE: { paddingRight: 10, width: mobile, minWidth: mobile },
        DESKTOP: { width: desktop }
    };
    return switchStyle[window.DEVICE_VERSION];
};
/* harmony default export */ var image_slider_item_style = ({
    mainWrap: {
        paddingLeft: 10,
        paddingRight: 10
    },
    heading: {
        paddingLeft: 10,
        display: variable["display"].inlineBlock,
        maxWidth: "100%",
        whiteSpace: "nowrap",
        overflow: "hidden",
        textOverflow: "ellipsis",
        fontFamily: variable["fontAvenirMedium"],
        color: variable["colorBlack"],
        fontSize: 20,
        lineHeight: "40px",
        height: 40,
        letterSpacing: -0.5,
        textTransform: "uppercase",
    },
    container: {
        width: '100%',
        overflowX: 'auto',
        whiteSpace: 'nowrap',
        paddingTop: 0,
        marginBottom: 0,
        display: variable["display"].flex,
        itemSlider: {
            width: "100%",
            height: "100%",
            display: variable["display"].inlineBlock,
            borderRadius: 5,
            overflow: 'hidden',
            boxShadow: variable["shadowBlur"],
            position: variable["position"].relative
        },
        itemSliderPanel: function (imgUrl) { return ({
            width: '100%',
            paddingTop: '62.5%',
            position: variable["position"].relative,
            backgroundImage: "url(" + imgUrl + ")",
            backgroundColor: variable["colorF7"],
            backgroundPosition: 'top center',
            transition: variable["transitionOpacity"],
            backgroundSize: 'cover',
        }); },
        videoIcon: {
            width: 70,
            height: 70,
            position: variable["position"].absolute,
            top: '50%',
            left: '55%',
            transform: 'translate(-50%, -50%)',
            borderTop: '35px solid transparent',
            boxSizing: 'border-box',
            borderLeft: '51px solid white',
            borderBottom: '35px solid transparent',
            opacity: .8
        },
        info: {
            width: '100%',
            height: 110,
            padding: 12,
            position: variable["position"].relative,
            background: variable["colorWhite"],
            display: variable["display"].flex,
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
            image: function (imgUrl) { return ({
                width: '100%',
                height: '100%',
                top: 0,
                left: 0,
                zIndex: -1,
                position: variable["position"].absolute,
                backgroundColor: variable["colorF7"],
                backgroundImage: "url(" + imgUrl + ")",
                backgroundPosition: 'bottom center',
                backgroundSize: 'cover',
                filter: 'blur(4px)',
                transform: "scaleY(-1) scale(1.1)",
                'WebkitBackfaceVisibility': 'hidden',
                'WebkitPerspective': 1000,
                'WebkitTransform': ['translate3d(0,0,0)', 'translateZ(0)'],
                'backfaceVisibility': 'hidden',
                perspective: 1000,
            }); },
            title: {
                color: variable["colorBlack"],
                whiteSpace: 'pre-wrap',
                fontFamily: variable["fontAvenirMedium"],
                fontSize: 14,
                lineHeight: '22px',
                maxHeight: '44px',
                overflow: 'hidden',
                marginBottom: 5
            },
            description: {
                fontSize: 12,
                color: variable["colorBlack"],
                fontFamily: variable["fontAvenirMedium"],
                opacity: .7,
                marginRight: 10,
                whiteSpace: 'pre-wrap',
                lineHeight: '18px',
                maxHeight: 36,
                overflow: 'hidden',
            },
            tagList: {
                width: '100%',
                display: variable["display"].flex,
                flexWrap: 'wrap',
                height: '36px',
                maxHeight: '36px',
                overflow: 'hidden'
            },
            tagItem: {
                fontSize: 12,
                lineHeight: '18px',
                color: variable["colorBlack"],
                fontFamily: variable["fontAvenirMedium"],
                opacity: .7,
                marginRight: 10,
                whiteSpace: 'pre-wrap'
            },
        }
    },
    column: {
        1: { width: '100%' },
        2: { width: '50%' },
        3: generateSwitchStyle('75%', '33.33%'),
        4: generateSwitchStyle('47%', '25%'),
        5: generateSwitchStyle('47%', '20%'),
        6: generateSwitchStyle('50%', '16.66%'),
    },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxJQUFNLG1CQUFtQixHQUFHLFVBQUMsTUFBTSxFQUFFLE9BQU87SUFDMUMsSUFBTSxXQUFXLEdBQUc7UUFDbEIsTUFBTSxFQUFFLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUU7UUFDN0QsT0FBTyxFQUFFLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRTtLQUM1QixDQUFDO0lBRUYsTUFBTSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUM7QUFDNUMsQ0FBQyxDQUFDO0FBRUYsZUFBZTtJQUNiLFFBQVEsRUFBRTtRQUNSLFdBQVcsRUFBRSxFQUFFO1FBQ2YsWUFBWSxFQUFFLEVBQUU7S0FDakI7SUFFRCxPQUFPLEVBQUU7UUFDUCxXQUFXLEVBQUUsRUFBRTtRQUNmLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLFdBQVc7UUFDckMsUUFBUSxFQUFFLE1BQU07UUFDaEIsVUFBVSxFQUFFLFFBQVE7UUFDcEIsUUFBUSxFQUFFLFFBQVE7UUFDbEIsWUFBWSxFQUFFLFVBQVU7UUFDeEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7UUFDckMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1FBQzFCLFFBQVEsRUFBRSxFQUFFO1FBQ1osVUFBVSxFQUFFLE1BQU07UUFDbEIsTUFBTSxFQUFFLEVBQUU7UUFDVixhQUFhLEVBQUUsQ0FBQyxHQUFHO1FBQ25CLGFBQWEsRUFBRSxXQUFXO0tBQzNCO0lBRUQsU0FBUyxFQUFFO1FBQ1QsS0FBSyxFQUFFLE1BQU07UUFDYixTQUFTLEVBQUUsTUFBTTtRQUNqQixVQUFVLEVBQUUsUUFBUTtRQUNwQixVQUFVLEVBQUUsQ0FBQztRQUNiLFlBQVksRUFBRSxDQUFDO1FBQ2YsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUU5QixVQUFVLEVBQUU7WUFDVixLQUFLLEVBQUUsTUFBTTtZQUNiLE1BQU0sRUFBRSxNQUFNO1lBQ2QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztZQUNyQyxZQUFZLEVBQUUsQ0FBQztZQUNmLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTtZQUM5QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1NBQ3JDO1FBRUQsZUFBZSxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztZQUM1QixLQUFLLEVBQUUsTUFBTTtZQUNiLFVBQVUsRUFBRSxPQUFPO1lBQ25CLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDcEMsZUFBZSxFQUFFLFNBQU8sTUFBTSxNQUFHO1lBQ2pDLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztZQUNqQyxrQkFBa0IsRUFBRSxZQUFZO1lBQ2hDLFVBQVUsRUFBRSxRQUFRLENBQUMsaUJBQWlCO1lBQ3RDLGNBQWMsRUFBRSxPQUFPO1NBQ3hCLENBQUMsRUFUMkIsQ0FTM0I7UUFFRixTQUFTLEVBQUU7WUFDVCxLQUFLLEVBQUUsRUFBRTtZQUNULE1BQU0sRUFBRSxFQUFFO1lBQ1YsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxHQUFHLEVBQUUsS0FBSztZQUNWLElBQUksRUFBRSxLQUFLO1lBQ1gsU0FBUyxFQUFFLHVCQUF1QjtZQUNsQyxTQUFTLEVBQUUsd0JBQXdCO1lBQ25DLFNBQVMsRUFBRSxZQUFZO1lBQ3ZCLFVBQVUsRUFBRSxrQkFBa0I7WUFDOUIsWUFBWSxFQUFFLHdCQUF3QjtZQUN0QyxPQUFPLEVBQUUsRUFBRTtTQUNaO1FBRUQsSUFBSSxFQUFFO1lBQ0osS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsR0FBRztZQUNYLE9BQU8sRUFBRSxFQUFFO1lBQ1gsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDL0IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixhQUFhLEVBQUUsUUFBUTtZQUN2QixjQUFjLEVBQUUsWUFBWTtZQUM1QixVQUFVLEVBQUUsWUFBWTtZQUV4QixLQUFLLEVBQUUsVUFBQyxNQUFNLElBQUssT0FBQSxDQUFDO2dCQUNsQixLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsTUFBTTtnQkFDZCxHQUFHLEVBQUUsQ0FBQztnQkFDTixJQUFJLEVBQUUsQ0FBQztnQkFDUCxNQUFNLEVBQUUsQ0FBQyxDQUFDO2dCQUNWLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztnQkFDakMsZUFBZSxFQUFFLFNBQU8sTUFBTSxNQUFHO2dCQUNqQyxrQkFBa0IsRUFBRSxlQUFlO2dCQUNuQyxjQUFjLEVBQUUsT0FBTztnQkFDdkIsTUFBTSxFQUFFLFdBQVc7Z0JBQ25CLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLDBCQUEwQixFQUFFLFFBQVE7Z0JBQ3BDLG1CQUFtQixFQUFFLElBQUk7Z0JBQ3pCLGlCQUFpQixFQUFFLENBQUMsb0JBQW9CLEVBQUUsZUFBZSxDQUFDO2dCQUMxRCxvQkFBb0IsRUFBRSxRQUFRO2dCQUM5QixXQUFXLEVBQUUsSUFBSTthQUNsQixDQUFDLEVBbEJpQixDQWtCakI7WUFFRixLQUFLLEVBQUU7Z0JBQ0wsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMxQixVQUFVLEVBQUUsVUFBVTtnQkFDdEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxNQUFNO2dCQUNsQixTQUFTLEVBQUUsTUFBTTtnQkFDakIsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFlBQVksRUFBRSxDQUFDO2FBQ2hCO1lBRUQsV0FBVyxFQUFFO2dCQUNYLFFBQVEsRUFBRSxFQUFFO2dCQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLE9BQU8sRUFBRSxFQUFFO2dCQUNYLFdBQVcsRUFBRSxFQUFFO2dCQUNmLFVBQVUsRUFBRSxVQUFVO2dCQUN0QixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsU0FBUyxFQUFFLEVBQUU7Z0JBQ2IsUUFBUSxFQUFFLFFBQVE7YUFDbkI7WUFFRCxPQUFPLEVBQUU7Z0JBQ1AsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsUUFBUSxFQUFFLE1BQU07Z0JBQ2hCLE1BQU0sRUFBRSxNQUFNO2dCQUNkLFNBQVMsRUFBRSxNQUFNO2dCQUNqQixRQUFRLEVBQUUsUUFBUTthQUNuQjtZQUVELE9BQU8sRUFBRTtnQkFDUCxRQUFRLEVBQUUsRUFBRTtnQkFDWixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtnQkFDckMsT0FBTyxFQUFFLEVBQUU7Z0JBQ1gsV0FBVyxFQUFFLEVBQUU7Z0JBQ2YsVUFBVSxFQUFFLFVBQVU7YUFDdkI7U0FDRjtLQUNGO0lBRUQsTUFBTSxFQUFFO1FBQ04sQ0FBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRTtRQUNwQixDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFO1FBQ25CLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDO1FBQ3ZDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDO1FBQ3BDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDO1FBQ3BDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDO0tBQ3hDO0NBQ00sQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider-item/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};





function renderComponent(_a) {
    var state = _a.state, props = _a.props, handleLoadImage = _a.handleLoadImage;
    var _b = props, item = _b.item, type = _b.type, column = _b.column;
    var isLoadedImage = state.isLoadedImage;
    var linkProps = {
        to: routing["Ma" /* ROUTING_MAGAZINE_DETAIL_PATH */] + "/" + (item && item.slug || ''),
        style: image_slider_item_style.container.itemSlider
    };
    return (react["createElement"](lib_default.a, { style: Object.assign({}, image_slider_item_style.column[column || 4], image_slider_item_style.mainWrap), offset: 200 }, function (_a) {
        var isVisible = _a.isVisible;
        if (!!isVisible) {
            handleLoadImage();
        }
        return (react["createElement"](react_router_dom["NavLink"], __assign({}, linkProps),
            react["createElement"]("div", { style: [
                    image_slider_item_style.container.itemSliderPanel(isLoadedImage && item ? item.cover_image && item.cover_image.medium_url : ''),
                    { opacity: isLoadedImage ? 1 : 0 }
                ] }, 'video' === type && (react["createElement"]("div", { style: image_slider_item_style.container.videoIcon }))),
            react["createElement"]("div", { style: image_slider_item_style.container.info },
                react["createElement"]("div", { style: image_slider_item_style.container.info.title }, item && item.title || ''),
                react["createElement"]("div", { style: image_slider_item_style.container.info.description }, item && item.description || ''))));
    }));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQ3RGLE9BQU8sZUFBZSxNQUFNLGlCQUFpQixDQUFDO0FBRTlDLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixNQUFNLDBCQUEwQixFQUFpQztRQUEvQixnQkFBSyxFQUFFLGdCQUFLLEVBQUUsb0NBQWU7SUFDdkQsSUFBQSxVQUF3QyxFQUF0QyxjQUFJLEVBQUUsY0FBSSxFQUFFLGtCQUFNLENBQXFCO0lBQ3ZDLElBQUEsbUNBQWEsQ0FBVztJQUVoQyxJQUFNLFNBQVMsR0FBRztRQUNoQixFQUFFLEVBQUssNEJBQTRCLFVBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFFO1FBQ2hFLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLFVBQVU7S0FDbEMsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLG9CQUFDLGVBQWUsSUFBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxFQUFFLE1BQU0sRUFBRSxHQUFHLElBRTVGLFVBQUMsRUFBYTtZQUFYLHdCQUFTO1FBQ1YsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFBQyxlQUFlLEVBQUUsQ0FBQTtRQUFBLENBQUM7UUFFckMsTUFBTSxDQUFDLENBQ0wsb0JBQUMsT0FBTyxlQUFLLFNBQVM7WUFDcEIsNkJBQUssS0FBSyxFQUFFO29CQUNSLEtBQUssQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFFLGFBQWEsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDOUcsRUFBRSxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtpQkFDbkMsSUFFQyxPQUFPLEtBQUssSUFBSSxJQUFJLENBQ2xCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBUSxDQUM5QyxDQUVDO1lBQ04sNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSTtnQkFDOUIsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBRyxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQU87Z0JBQ3hFLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLElBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksRUFBRSxDQUFPLENBQ2hGLENBQ0UsQ0FDWCxDQUFBO0lBQ0gsQ0FBQyxDQUVhLENBQ25CLENBQUM7QUFDSixDQUFDO0FBQUEsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider-item/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_SlideItem = /** @class */ (function (_super) {
    __extends(SlideItem, _super);
    function SlideItem(props) {
        var _this = _super.call(this, props) || this;
        _this.state = initialize_INITIAL_STATE;
        return _this;
    }
    // shouldComponentUpdate(nextProps: IProps, nextState: IState) {
    //   // if (true === isEmptyObject(this.props.data) && false === isEmptyObject(nextProps.data)) { return true; }
    //   // if (this.props.listLikedId.length !== nextProps.listLikedId.length) { return true; }
    //   return true;
    // }
    SlideItem.prototype.handleLoadImage = function () {
        var isLoadedImage = this.state.isLoadedImage;
        if (!!isLoadedImage) {
            return;
        }
        this.setState({ isLoadedImage: true });
    };
    SlideItem.prototype.render = function () {
        var renderViewProps = {
            props: this.props,
            state: this.state,
            handleLoadImage: this.handleLoadImage.bind(this)
        };
        return renderComponent(renderViewProps);
    };
    SlideItem.defaultProps = initialize_DEFAULT_PROPS;
    SlideItem = __decorate([
        radium
    ], SlideItem);
    return SlideItem;
}(react["Component"]));
;
/* harmony default export */ var image_slider_item_component = (component_SlideItem);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDNUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUl6QztJQUF3Qiw2QkFBK0I7SUFHckQsbUJBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUM7O0lBQzdCLENBQUM7SUFFRCxnRUFBZ0U7SUFDaEUsZ0hBQWdIO0lBQ2hILDRGQUE0RjtJQUU1RixpQkFBaUI7SUFDakIsSUFBSTtJQUVKLG1DQUFlLEdBQWY7UUFDVSxJQUFBLHdDQUFhLENBQWdCO1FBQ3JDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sQ0FBQztRQUNULENBQUM7UUFFRCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUMsYUFBYSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELDBCQUFNLEdBQU47UUFDRSxJQUFNLGVBQWUsR0FBRztZQUN0QixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLGVBQWUsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDakQsQ0FBQztRQUVGLE1BQU0sQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQS9CTSxzQkFBWSxHQUFXLGFBQWEsQ0FBQztJQUR4QyxTQUFTO1FBRGQsTUFBTTtPQUNELFNBQVMsQ0FpQ2Q7SUFBRCxnQkFBQztDQUFBLEFBakNELENBQXdCLEtBQUssQ0FBQyxTQUFTLEdBaUN0QztBQUFBLENBQUM7QUFFRixlQUFlLFNBQVMsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider-item/index.tsx

/* harmony default export */ var image_slider_item = (image_slider_item_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxTQUFTLE1BQU0sYUFBYSxDQUFDO0FBQ3BDLGVBQWUsU0FBUyxDQUFDIn0=
// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// CONCATENATED MODULE: ./components/magazine/image-slider/style.tsx




var INLINE_STYLE = {
    '.magazine-slide-container .pagination': {
        opacity: 0,
        transform: 'translateY(20px)'
    },
    '.magazine-slide-container:hover .pagination': {
        opacity: 1,
        transform: 'translateY(0)'
    },
    '.magazine-slide-container .left-nav': {
        width: 40,
        height: 60,
        opacity: 0,
        transform: 'translate(-60px, -50%)',
        visibility: 'hidden',
    },
    '.magazine-slide-container:hover .left-nav': {
        opacity: 1,
        transform: 'translate(1px, -50%)',
        visibility: 'visible',
    },
    '.magazine-slide-container .right-nav': {
        width: 40,
        height: 60,
        opacity: 0,
        transform: 'translate(60px, -50%)',
        visibility: 'hidden',
    },
    '.magazine-slide-container:hover .right-nav': {
        opacity: 1,
        transform: 'translate(1px, -50%)',
        visibility: 'visible',
    }
};
/* harmony default export */ var image_slider_style = ({
    container: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{ marginBottom: 10 }],
        DESKTOP: [{ marginBottom: 30 }],
        GENERAL: [{ display: variable["display"].block, width: '100%' }]
    }),
    magazineSlide: {
        position: 'relative',
        overflow: 'hidden',
        container: Object.assign({}, layout["a" /* flexContainer */].justify, component["c" /* block */].content, {
            paddingTop: 10,
            transition: variable["transitionOpacity"],
        }),
        pagination: [
            layout["a" /* flexContainer */].center,
            component["f" /* slidePagination */],
            {
                transition: variable["transitionNormal"],
                bottom: 0
            },
        ],
        navigation: [
            layout["a" /* flexContainer */].center,
            layout["a" /* flexContainer */].verticalCenter,
            component["e" /* slideNavigation */]
        ],
    },
    customStyleLoading: {
        height: 300
    },
    mobileWrap: {
        width: '100%',
        overflowX: 'auto',
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        panel: {
            flexWrap: 'wrap'
        },
        item: {
            width: '100%',
            marginBottom: 5
        }
    },
    placeholder: {
        width: '100%',
        display: variable["display"].flex,
        marginBottom: 35,
        item: {
            flex: 1,
            paddingLeft: 10,
            paddingRight: 10,
        },
        image: {
            width: '100%',
            height: 282,
            marginBottom: 10,
        },
        text: {
            width: '94%',
            height: 25,
            marginBottom: 10,
        },
        lastText: {
            width: '65%',
            height: 25,
        }
    },
    desktop: {
        mainWrap: {
            display: variable["display"].block,
            marginLeft: -9,
            marginRight: -9
        },
        container: {
            width: '100%',
            overflowX: 'auto',
            whiteSpace: 'nowrap',
            paddingTop: 0,
            paddingBottom: 15,
            display: variable["display"].flex,
            overflow: 'hidden',
            itemSlider: {
                width: "100%",
                display: variable["display"].inlineBlock,
                borderRadius: 5,
                overflow: 'hidden',
                boxShadow: variable["shadowBlur"],
                position: variable["position"].relative,
            },
            itemSliderPanel: function (imgUrl) { return ({
                width: '100%',
                paddingTop: '62.5%',
                position: variable["position"].relative,
                backgroundImage: "url(" + imgUrl + ")",
                backgroundColor: variable["colorF7"],
                backgroundPosition: 'top center',
                backgroundSize: 'cover',
            }); },
            videoIcon: {
                width: 70,
                height: 70,
                position: variable["position"].absolute,
                top: '50%',
                left: '55%',
                transform: 'translate(-50%, -50%)',
                borderTop: '35px solid transparent',
                boxSizing: 'border-box',
                borderLeft: '51px solid white',
                borderBottom: '35px solid transparent',
                opacity: .8
            },
            info: {
                width: '100%',
                height: 110,
                padding: 12,
                position: variable["position"].relative,
                background: variable["colorWhite"],
                display: variable["display"].flex,
                flexDirection: 'column',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                image: function (imgUrl) { return ({
                    width: '100%',
                    height: '100%',
                    top: 0,
                    left: 0,
                    zIndex: -1,
                    position: variable["position"].absolute,
                    backgroundColor: variable["colorF7"],
                    backgroundImage: "url(" + imgUrl + ")",
                    backgroundPosition: 'bottom center',
                    backgroundSize: 'cover',
                    filter: 'blur(4px)',
                    transform: "scaleY(-1) scale(1.1)",
                    'WebkitBackfaceVisibility': 'hidden',
                    'WebkitPerspective': 1000,
                    'WebkitTransform': ['translate3d(0,0,0)', 'translateZ(0)'],
                    'backfaceVisibility': 'hidden',
                    perspective: 1000,
                }); },
                title: {
                    color: variable["colorBlack"],
                    whiteSpace: 'pre-wrap',
                    fontFamily: variable["fontAvenirMedium"],
                    fontSize: 14,
                    lineHeight: '22px',
                    maxHeight: '44px',
                    overflow: 'hidden',
                    marginBottom: 5
                },
                description: {
                    fontSize: 12,
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap',
                    lineHeight: '18px',
                    maxHeight: 36,
                    overflow: 'hidden',
                },
                tagList: {
                    width: '100%',
                    display: variable["display"].flex,
                    flexWrap: 'wrap',
                    height: '36px',
                    maxHeight: '36px',
                    overflow: 'hidden'
                },
                tagItem: {
                    fontSize: 12,
                    lineHeight: '18px',
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap'
                },
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFNBQVMsTUFBTSwwQkFBMEIsQ0FBQztBQUN0RCxPQUFPLEtBQUssTUFBTSxNQUFNLHVCQUF1QixDQUFDO0FBQ2hELE9BQU8sS0FBSyxRQUFRLE1BQU0seUJBQXlCLENBQUM7QUFDcEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRXpELE1BQU0sQ0FBQyxJQUFNLFlBQVksR0FBRztJQUMxQix1Q0FBdUMsRUFBRTtRQUN2QyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxrQkFBa0I7S0FDOUI7SUFFRCw2Q0FBNkMsRUFBRTtRQUM3QyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxlQUFlO0tBQzNCO0lBRUQscUNBQXFDLEVBQUU7UUFDckMsS0FBSyxFQUFFLEVBQUU7UUFDVCxNQUFNLEVBQUUsRUFBRTtRQUNWLE9BQU8sRUFBRSxDQUFDO1FBQ1YsU0FBUyxFQUFFLHdCQUF3QjtRQUNuQyxVQUFVLEVBQUUsUUFBUTtLQUNyQjtJQUVELDJDQUEyQyxFQUFFO1FBQzNDLE9BQU8sRUFBRSxDQUFDO1FBQ1YsU0FBUyxFQUFFLHNCQUFzQjtRQUNqQyxVQUFVLEVBQUUsU0FBUztLQUN0QjtJQUVELHNDQUFzQyxFQUFFO1FBQ3RDLEtBQUssRUFBRSxFQUFFO1FBQ1QsTUFBTSxFQUFFLEVBQUU7UUFDVixPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSx1QkFBdUI7UUFDbEMsVUFBVSxFQUFFLFFBQVE7S0FDckI7SUFFRCw0Q0FBNEMsRUFBRTtRQUM1QyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxzQkFBc0I7UUFDakMsVUFBVSxFQUFFLFNBQVM7S0FDdEI7Q0FDRixDQUFDO0FBRUYsZUFBZTtJQUNiLFNBQVMsRUFBRSxZQUFZLENBQUM7UUFDdEIsTUFBTSxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLENBQUM7UUFDOUIsT0FBTyxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLENBQUM7UUFDL0IsT0FBTyxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxDQUFDO0tBQzlELENBQUM7SUFFRixhQUFhLEVBQUU7UUFDYixRQUFRLEVBQUUsVUFBVTtRQUNwQixRQUFRLEVBQUUsUUFBUTtRQUVsQixTQUFTLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQ3pCLE1BQU0sQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUM1QixTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFDdkI7WUFDRSxVQUFVLEVBQUUsRUFBRTtZQUNkLFVBQVUsRUFBRSxRQUFRLENBQUMsaUJBQWlCO1NBQ3ZDLENBQ0Y7UUFFRCxVQUFVLEVBQUU7WUFDVixNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU07WUFDM0IsU0FBUyxDQUFDLGVBQWU7WUFDekI7Z0JBQ0UsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLE1BQU0sRUFBRSxDQUFDO2FBQ1Y7U0FDRjtRQUVELFVBQVUsRUFBRTtZQUNWLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTTtZQUMzQixNQUFNLENBQUMsYUFBYSxDQUFDLGNBQWM7WUFDbkMsU0FBUyxDQUFDLGVBQWU7U0FDMUI7S0FDRjtJQUVELGtCQUFrQixFQUFFO1FBQ2xCLE1BQU0sRUFBRSxHQUFHO0tBQ1o7SUFFRCxVQUFVLEVBQUU7UUFDVixLQUFLLEVBQUUsTUFBTTtRQUNiLFNBQVMsRUFBRSxNQUFNO1FBQ2pCLFVBQVUsRUFBRSxDQUFDO1FBQ2IsWUFBWSxFQUFFLENBQUM7UUFDZixhQUFhLEVBQUUsQ0FBQztRQUNoQixXQUFXLEVBQUUsQ0FBQztRQUVkLEtBQUssRUFBRTtZQUNMLFFBQVEsRUFBRSxNQUFNO1NBQ2pCO1FBRUQsSUFBSSxFQUFFO1lBQ0osS0FBSyxFQUFFLE1BQU07WUFDYixZQUFZLEVBQUUsQ0FBQztTQUNoQjtLQUNGO0lBRUQsV0FBVyxFQUFFO1FBQ1gsS0FBSyxFQUFFLE1BQU07UUFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1FBQzlCLFlBQVksRUFBRSxFQUFFO1FBRWhCLElBQUksRUFBRTtZQUNKLElBQUksRUFBRSxDQUFDO1lBQ1AsV0FBVyxFQUFFLEVBQUU7WUFDZixZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELEtBQUssRUFBRTtZQUNMLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLEdBQUc7WUFDWCxZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELElBQUksRUFBRTtZQUNKLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLEVBQUU7WUFDVixZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELFFBQVEsRUFBRTtZQUNSLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLEVBQUU7U0FDWDtLQUNGO0lBRUQsT0FBTyxFQUFFO1FBQ1AsUUFBUSxFQUFFO1lBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztZQUMvQixVQUFVLEVBQUUsQ0FBQyxDQUFDO1lBQ2QsV0FBVyxFQUFFLENBQUMsQ0FBQztTQUNoQjtRQUVELFNBQVMsRUFBRTtZQUNULEtBQUssRUFBRSxNQUFNO1lBQ2IsU0FBUyxFQUFFLE1BQU07WUFDakIsVUFBVSxFQUFFLFFBQVE7WUFDcEIsVUFBVSxFQUFFLENBQUM7WUFDYixhQUFhLEVBQUUsRUFBRTtZQUNqQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLFFBQVEsRUFBRSxRQUFRO1lBRWxCLFVBQVUsRUFBRTtnQkFDVixLQUFLLEVBQUUsTUFBTTtnQkFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxXQUFXO2dCQUNyQyxZQUFZLEVBQUUsQ0FBQztnQkFDZixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUM5QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2FBQ3JDO1lBRUQsZUFBZSxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztnQkFDNUIsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsVUFBVSxFQUFFLE9BQU87Z0JBQ25CLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztnQkFDakMsZUFBZSxFQUFFLFFBQVEsQ0FBQyxPQUFPO2dCQUNqQyxrQkFBa0IsRUFBRSxZQUFZO2dCQUNoQyxjQUFjLEVBQUUsT0FBTzthQUN4QixDQUFDLEVBUjJCLENBUTNCO1lBRUYsU0FBUyxFQUFFO2dCQUNULEtBQUssRUFBRSxFQUFFO2dCQUNULE1BQU0sRUFBRSxFQUFFO2dCQUNWLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLEdBQUcsRUFBRSxLQUFLO2dCQUNWLElBQUksRUFBRSxLQUFLO2dCQUNYLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLFNBQVMsRUFBRSx3QkFBd0I7Z0JBQ25DLFNBQVMsRUFBRSxZQUFZO2dCQUN2QixVQUFVLEVBQUUsa0JBQWtCO2dCQUM5QixZQUFZLEVBQUUsd0JBQXdCO2dCQUN0QyxPQUFPLEVBQUUsRUFBRTthQUNaO1lBRUQsSUFBSSxFQUFFO2dCQUNKLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxHQUFHO2dCQUNYLE9BQU8sRUFBRSxFQUFFO2dCQUNYLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLFVBQVUsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDL0IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsYUFBYSxFQUFFLFFBQVE7Z0JBQ3ZCLGNBQWMsRUFBRSxZQUFZO2dCQUM1QixVQUFVLEVBQUUsWUFBWTtnQkFFeEIsS0FBSyxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztvQkFDbEIsS0FBSyxFQUFFLE1BQU07b0JBQ2IsTUFBTSxFQUFFLE1BQU07b0JBQ2QsR0FBRyxFQUFFLENBQUM7b0JBQ04sSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLENBQUMsQ0FBQztvQkFDVixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUNwQyxlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87b0JBQ2pDLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztvQkFDakMsa0JBQWtCLEVBQUUsZUFBZTtvQkFDbkMsY0FBYyxFQUFFLE9BQU87b0JBQ3ZCLE1BQU0sRUFBRSxXQUFXO29CQUNuQixTQUFTLEVBQUUsdUJBQXVCO29CQUNsQywwQkFBMEIsRUFBRSxRQUFRO29CQUNwQyxtQkFBbUIsRUFBRSxJQUFJO29CQUN6QixpQkFBaUIsRUFBRSxDQUFDLG9CQUFvQixFQUFFLGVBQWUsQ0FBQztvQkFDMUQsb0JBQW9CLEVBQUUsUUFBUTtvQkFDOUIsV0FBVyxFQUFFLElBQUk7aUJBQ2xCLENBQUMsRUFsQmlCLENBa0JqQjtnQkFFRixLQUFLLEVBQUU7b0JBQ0wsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsVUFBVTtvQkFDdEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7b0JBQ3JDLFFBQVEsRUFBRSxFQUFFO29CQUNaLFVBQVUsRUFBRSxNQUFNO29CQUNsQixTQUFTLEVBQUUsTUFBTTtvQkFDakIsUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLFlBQVksRUFBRSxDQUFDO2lCQUNoQjtnQkFFRCxXQUFXLEVBQUU7b0JBQ1gsUUFBUSxFQUFFLEVBQUU7b0JBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsVUFBVSxFQUFFLFVBQVU7b0JBQ3RCLFVBQVUsRUFBRSxNQUFNO29CQUNsQixTQUFTLEVBQUUsRUFBRTtvQkFDYixRQUFRLEVBQUUsUUFBUTtpQkFDbkI7Z0JBRUQsT0FBTyxFQUFFO29CQUNQLEtBQUssRUFBRSxNQUFNO29CQUNiLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7b0JBQzlCLFFBQVEsRUFBRSxNQUFNO29CQUNoQixNQUFNLEVBQUUsTUFBTTtvQkFDZCxTQUFTLEVBQUUsTUFBTTtvQkFDakIsUUFBUSxFQUFFLFFBQVE7aUJBQ25CO2dCQUVELE9BQU8sRUFBRTtvQkFDUCxRQUFRLEVBQUUsRUFBRTtvQkFDWixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsVUFBVSxFQUFFLFVBQVU7aUJBQ3ZCO2FBQ0Y7U0FDRjtLQUNGO0NBQ0ssQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider/view-desktop.tsx
var view_desktop_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};






var renderSlider = function (_a) {
    var column = _a.column, type = _a.type, magazineSlideSelected = _a.magazineSlideSelected, style = _a.style;
    return (react["createElement"]("magazine-category", { style: image_slider_style.desktop.mainWrap },
        react["createElement"]("div", { style: [image_slider_style.desktop.container, style] }, magazineSlideSelected
            && Array.isArray(magazineSlideSelected.list)
            && magazineSlideSelected.list.map(function (item) {
                var slideProps = {
                    item: item,
                    type: type,
                    column: column
                };
                return react["createElement"](image_slider_item, view_desktop_assign({ key: "slider-item-" + item.id }, slideProps));
            }))));
};
var renderNavigation = function (_a) {
    var magazineSlide = _a.magazineSlide, navLeftSlide = _a.navLeftSlide, navRightSlide = _a.navRightSlide;
    var leftNavProps = {
        className: 'left-nav',
        onClick: navLeftSlide,
        style: [image_slider_style.magazineSlide.navigation, component["e" /* slideNavigation */]['left'], { top: "calc(50% - 65px)" }]
    };
    var rightNavProps = {
        className: 'right-nav',
        onClick: navRightSlide,
        style: [image_slider_style.magazineSlide.navigation, component["e" /* slideNavigation */]['right'], { top: "calc(50% - 65px)" }]
    };
    return magazineSlide.length <= 1
        ? null
        : (react["createElement"]("div", null,
            react["createElement"]("div", view_desktop_assign({}, leftNavProps),
                react["createElement"](icon["a" /* default */], { name: 'angle-left', style: component["e" /* slideNavigation */].icon })),
            react["createElement"]("div", view_desktop_assign({}, rightNavProps),
                react["createElement"](icon["a" /* default */], { name: 'angle-right', style: component["e" /* slideNavigation */].icon }))));
};
var renderPagination = function (_a) {
    var magazineSlide = _a.magazineSlide, selectSlide = _a.selectSlide, countChangeSlide = _a.countChangeSlide;
    var generateItemProps = function (item, index) { return ({
        key: "product-slider-" + item.id,
        onClick: function () { return selectSlide(index); },
        style: [
            component["f" /* slidePagination */].item,
            index === countChangeSlide && component["f" /* slidePagination */].itemActive
        ]
    }); };
    return magazineSlide.length <= 1
        ? null
        : (react["createElement"]("div", { style: image_slider_style.magazineSlide.pagination, className: 'pagination' }, Array.isArray(magazineSlide)
            && magazineSlide.map(function (item, $index) {
                var itemProps = generateItemProps(item, $index);
                return react["createElement"]("div", view_desktop_assign({}, itemProps));
            })));
};
var renderDesktop = function (_a) {
    var props = _a.props, state = _a.state, handleMouseHover = _a.handleMouseHover, selectSlide = _a.selectSlide, navLeftSlide = _a.navLeftSlide, navRightSlide = _a.navRightSlide;
    var _b = state, magazineSlide = _b.magazineSlide, magazineSlideSelected = _b.magazineSlideSelected, countChangeSlide = _b.countChangeSlide;
    var _c = props, column = _c.column, type = _c.type, style = _c.style;
    var containerProps = {
        style: image_slider_style.magazineSlide,
        onMouseEnter: handleMouseHover
    };
    return (react["createElement"]("div", view_desktop_assign({}, containerProps, { className: 'magazine-slide-container' }),
        renderSlider({ column: column, type: type, magazineSlideSelected: magazineSlideSelected, style: style }),
        renderPagination({ magazineSlide: magazineSlide, selectSlide: selectSlide, countChangeSlide: countChangeSlide }),
        renderNavigation({ magazineSlide: magazineSlide, navLeftSlide: navLeftSlide, navRightSlide: navRightSlide }),
        react["createElement"](radium["Style"], { rules: INLINE_STYLE })));
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1kZXNrdG9wLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1kZXNrdG9wLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFL0IsT0FBTyxJQUFJLE1BQU0sZUFBZSxDQUFDO0FBQ2pDLE9BQU8sS0FBSyxTQUFTLE1BQU0sMEJBQTBCLENBQUM7QUFFdEQsT0FBTyxlQUFlLE1BQU0sc0JBQXNCLENBQUM7QUFFbkQsT0FBTyxLQUFLLEVBQUUsRUFBRSxZQUFZLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFHOUMsSUFBTSxZQUFZLEdBQUcsVUFBQyxFQUE4QztRQUE1QyxrQkFBTSxFQUFFLGNBQUksRUFBRSxnREFBcUIsRUFBRSxnQkFBSztJQUFPLE9BQUEsQ0FDdkUsMkNBQW1CLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVE7UUFDOUMsNkJBQUssS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLElBRXhDLHFCQUFxQjtlQUNsQixLQUFLLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQztlQUN6QyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSTtnQkFDcEMsSUFBTSxVQUFVLEdBQUc7b0JBQ2pCLElBQUksTUFBQTtvQkFDSixJQUFJLE1BQUE7b0JBQ0osTUFBTSxRQUFBO2lCQUNQLENBQUE7Z0JBRUQsTUFBTSxDQUFDLG9CQUFDLGVBQWUsYUFBQyxHQUFHLEVBQUUsaUJBQWUsSUFBSSxDQUFDLEVBQUksSUFBTSxVQUFVLEVBQUksQ0FBQTtZQUMzRSxDQUFDLENBQUMsQ0FFQSxDQUNZLENBQ3JCO0FBbEJ3RSxDQWtCeEUsQ0FBQztBQUVGLElBQU0sZ0JBQWdCLEdBQUcsVUFBQyxFQUE4QztRQUE1QyxnQ0FBYSxFQUFFLDhCQUFZLEVBQUUsZ0NBQWE7SUFDcEUsSUFBTSxZQUFZLEdBQUc7UUFDbkIsU0FBUyxFQUFFLFVBQVU7UUFDckIsT0FBTyxFQUFFLFlBQVk7UUFDckIsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxrQkFBa0IsRUFBRSxDQUFDO0tBQ3hHLENBQUM7SUFFRixJQUFNLGFBQWEsR0FBRztRQUNwQixTQUFTLEVBQUUsV0FBVztRQUN0QixPQUFPLEVBQUUsYUFBYTtRQUN0QixLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxFQUFFLEVBQUUsR0FBRyxFQUFFLGtCQUFrQixFQUFFLENBQUM7S0FDekcsQ0FBQztJQUVGLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxJQUFJLENBQUM7UUFDOUIsQ0FBQyxDQUFDLElBQUk7UUFDTixDQUFDLENBQUMsQ0FDQTtZQUNFLHdDQUFTLFlBQVk7Z0JBQ25CLG9CQUFDLElBQUksSUFBQyxJQUFJLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsZUFBZSxDQUFDLElBQUksR0FBSSxDQUMvRDtZQUNOLHdDQUFTLGFBQWE7Z0JBQ3BCLG9CQUFDLElBQUksSUFBQyxJQUFJLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsZUFBZSxDQUFDLElBQUksR0FBSSxDQUNoRSxDQUNGLENBQ1AsQ0FBQztBQUNOLENBQUMsQ0FBQztBQUVGLElBQU0sZ0JBQWdCLEdBQUcsVUFBQyxFQUFnRDtRQUE5QyxnQ0FBYSxFQUFFLDRCQUFXLEVBQUUsc0NBQWdCO0lBQ3RFLElBQU0saUJBQWlCLEdBQUcsVUFBQyxJQUFJLEVBQUUsS0FBSyxJQUFLLE9BQUEsQ0FBQztRQUMxQyxHQUFHLEVBQUUsb0JBQWtCLElBQUksQ0FBQyxFQUFJO1FBQ2hDLE9BQU8sRUFBRSxjQUFNLE9BQUEsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFsQixDQUFrQjtRQUNqQyxLQUFLLEVBQUU7WUFDTCxTQUFTLENBQUMsZUFBZSxDQUFDLElBQUk7WUFDOUIsS0FBSyxLQUFLLGdCQUFnQixJQUFJLFNBQVMsQ0FBQyxlQUFlLENBQUMsVUFBVTtTQUNuRTtLQUNGLENBQUMsRUFQeUMsQ0FPekMsQ0FBQztJQUVILE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxJQUFJLENBQUM7UUFDOUIsQ0FBQyxDQUFDLElBQUk7UUFDTixDQUFDLENBQUMsQ0FDQSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsU0FBUyxFQUFFLFlBQVksSUFFL0QsS0FBSyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUM7ZUFDekIsYUFBYSxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxNQUFNO2dCQUNoQyxJQUFNLFNBQVMsR0FBRyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7Z0JBQ2xELE1BQU0sQ0FBQyx3Q0FBUyxTQUFTLEVBQVEsQ0FBQztZQUNwQyxDQUFDLENBQUMsQ0FFQSxDQUNQLENBQUM7QUFDTixDQUFDLENBQUM7QUFFRixNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUcsVUFBQyxFQUE0RTtRQUExRSxnQkFBSyxFQUFFLGdCQUFLLEVBQUUsc0NBQWdCLEVBQUUsNEJBQVcsRUFBRSw4QkFBWSxFQUFFLGdDQUFhO0lBQ2hHLElBQUEsVUFBNEUsRUFBMUUsZ0NBQWEsRUFBRSxnREFBcUIsRUFBRSxzQ0FBZ0IsQ0FBcUI7SUFDN0UsSUFBQSxVQUF5QyxFQUF2QyxrQkFBTSxFQUFFLGNBQUksRUFBRSxnQkFBSyxDQUFxQjtJQUVoRCxJQUFNLGNBQWMsR0FBRztRQUNyQixLQUFLLEVBQUUsS0FBSyxDQUFDLGFBQWE7UUFDMUIsWUFBWSxFQUFFLGdCQUFnQjtLQUMvQixDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsd0NBQVMsY0FBYyxJQUFFLFNBQVMsRUFBRSwwQkFBMEI7UUFDM0QsWUFBWSxDQUFDLEVBQUUsTUFBTSxRQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUscUJBQXFCLHVCQUFBLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQztRQUM1RCxnQkFBZ0IsQ0FBQyxFQUFFLGFBQWEsZUFBQSxFQUFFLFdBQVcsYUFBQSxFQUFFLGdCQUFnQixrQkFBQSxFQUFFLENBQUM7UUFDbEUsZ0JBQWdCLENBQUMsRUFBRSxhQUFhLGVBQUEsRUFBRSxZQUFZLGNBQUEsRUFBRSxhQUFhLGVBQUEsRUFBRSxDQUFDO1FBQ2pFLG9CQUFDLEtBQUssSUFBQyxLQUFLLEVBQUUsWUFBWSxHQUFJLENBQzFCLENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider/view-mobile.tsx





var renderMobile = function (_a) {
    var type = _a.type, magazineList = _a.magazineList, column = _a.column;
    return (react["createElement"]("div", { style: [component["c" /* block */].content, image_slider_style.mobileWrap] },
        react["createElement"]("div", { style: [layout["a" /* flexContainer */].noWrap, image_slider_style.mobileWrap.panel] }, Array.isArray(magazineList)
            && magazineList.map(function (magazine, index) { return react["createElement"]("div", { style: image_slider_style.mobileWrap.item },
                react["createElement"](image_slider_item, { key: "image-slide-item-" + index, item: magazine, type: type, column: column })); }))));
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1tb2JpbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LW1vYmlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxlQUFlLE1BQU0sc0JBQXNCLENBQUM7QUFFbkQsT0FBTyxLQUFLLFNBQVMsTUFBTSwwQkFBMEIsQ0FBQztBQUN0RCxPQUFPLEtBQUssTUFBTSxNQUFNLHVCQUF1QixDQUFDO0FBQ2hELE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixNQUFNLENBQUMsSUFBTSxZQUFZLEdBQUcsVUFBQyxFQUE4QjtRQUE1QixjQUFJLEVBQUUsOEJBQVksRUFBRSxrQkFBTTtJQUV2RCxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsVUFBVSxDQUFDO1FBQ3JELDZCQUFLLEtBQUssRUFBRSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLElBRTdELEtBQUssQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDO2VBQ3hCLFlBQVksQ0FBQyxHQUFHLENBQUMsVUFBQyxRQUFRLEVBQUUsS0FBSyxJQUFLLE9BQUEsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSTtnQkFBRSxvQkFBQyxlQUFlLElBQUMsR0FBRyxFQUFFLHNCQUFvQixLQUFPLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxNQUFNLEdBQUksQ0FBTSxFQUExSSxDQUEwSSxDQUFDLENBRWxMLENBQ0YsQ0FDUCxDQUFDO0FBQ0osQ0FBQyxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/image-slider/view.tsx
var view_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};







var renderItemPlaceholder = function (item) { return (react["createElement"]("div", { style: image_slider_style.placeholder.item, key: item },
    react["createElement"](loading_placeholder["a" /* default */], { style: image_slider_style.placeholder.image }))); };
var renderLoadingPlaceholder = function () { return (react["createElement"]("div", { style: image_slider_style.placeholder }, [1, 2, 3, 4].map(renderItemPlaceholder))); };
var renderCustomTitle = function (_a) {
    var title = _a.title, isCustomTitle = _a.isCustomTitle, _b = _a.titleStyle, titleStyle = _b === void 0 ? {} : _b;
    return false === isCustomTitle
        ? null
        : (react["createElement"]("div", null,
            react["createElement"]("div", { style: [component["c" /* block */].heading, component["c" /* block */].heading.multiLine, image_slider_style.desktopTitle, titleStyle] },
                react["createElement"]("div", { style: component["c" /* block */].heading.title.multiLine },
                    react["createElement"]("span", { style: [component["c" /* block */].heading.title.text, component["c" /* block */].heading.title.text.multiLine] }, title)))));
};
var renderView = function (_a) {
    var props = _a.props, state = _a.state, handleMouseHover = _a.handleMouseHover, selectSlide = _a.selectSlide, navLeftSlide = _a.navLeftSlide, navRightSlide = _a.navRightSlide;
    var magazineList = state.magazineList;
    var _b = props, type = _b.type, title = _b.title, showViewMore = _b.showViewMore, column = _b.column, showHeader = _b.showHeader, isCustomTitle = _b.isCustomTitle, titleStyle = _b.titleStyle;
    var renderMobileProps = { type: type, magazineList: magazineList, column: column };
    var renderDesktopProps = {
        props: props,
        state: state,
        navLeftSlide: navLeftSlide,
        navRightSlide: navRightSlide,
        selectSlide: selectSlide,
        handleMouseHover: handleMouseHover
    };
    var switchStyle = {
        MOBILE: function () { return renderMobile(renderMobileProps); },
        DESKTOP: function () { return renderDesktop(renderDesktopProps); }
    };
    var mainBlockProps = {
        title: isCustomTitle ? '' : title,
        showHeader: showHeader,
        showViewMore: showViewMore,
        content: 0 === magazineList.length
            ? renderLoadingPlaceholder()
            : switchStyle[window.DEVICE_VERSION](),
        style: {}
    };
    return (react["createElement"]("div", { style: image_slider_style.container },
        isCustomTitle && renderCustomTitle({ title: title, isCustomTitle: isCustomTitle, titleStyle: titleStyle }),
        react["createElement"](main_block["a" /* default */], view_assign({}, mainBlockProps))));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFHL0IsT0FBTyxTQUFTLE1BQU0sc0NBQXNDLENBQUM7QUFFN0QsT0FBTyxrQkFBa0IsTUFBTSw4QkFBOEIsQ0FBQztBQUU5RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDL0MsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUU3QyxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsT0FBTyxLQUFLLFNBQVMsTUFBTSwwQkFBMEIsQ0FBQztBQUV0RCxJQUFNLHFCQUFxQixHQUFHLFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FDdEMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJO0lBQzNDLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBSSxDQUNsRCxDQUNQLEVBSnVDLENBSXZDLENBQUM7QUFFRixJQUFNLHdCQUF3QixHQUFHLGNBQU0sT0FBQSxDQUNyQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsSUFDMUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FDcEMsQ0FDUCxFQUpzQyxDQUl0QyxDQUFDO0FBRUYsSUFBTSxpQkFBaUIsR0FBRyxVQUFDLEVBQXlDO1FBQXZDLGdCQUFLLEVBQUUsZ0NBQWEsRUFBRSxrQkFBZSxFQUFmLG9DQUFlO0lBQU8sT0FBQSxLQUFLLEtBQUssYUFBYTtRQUM5RixDQUFDLENBQUMsSUFBSTtRQUNOLENBQUMsQ0FBQyxDQUNBO1lBQ0UsNkJBQUssS0FBSyxFQUFFLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxZQUFZLEVBQUUsVUFBVSxDQUFDO2dCQUN0Ryw2QkFBSyxLQUFLLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFNBQVM7b0JBQ2pELDhCQUFNLEtBQUssRUFBRSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFDNUYsS0FBSyxDQUNELENBQ0gsQ0FDRixDQUNGLENBQ1A7QUFac0UsQ0FZdEUsQ0FBQztBQUVKLElBQU0sVUFBVSxHQUFHLFVBQUMsRUFBNEU7UUFBMUUsZ0JBQUssRUFBRSxnQkFBSyxFQUFFLHNDQUFnQixFQUFFLDRCQUFXLEVBQUUsOEJBQVksRUFBRSxnQ0FBYTtJQUNwRixJQUFBLGlDQUFZLENBQXFCO0lBQ25DLElBQUEsVUFBOEYsRUFBNUYsY0FBSSxFQUFFLGdCQUFLLEVBQUUsOEJBQVksRUFBRSxrQkFBTSxFQUFFLDBCQUFVLEVBQUUsZ0NBQWEsRUFBRSwwQkFBVSxDQUFxQjtJQUVyRyxJQUFNLGlCQUFpQixHQUFHLEVBQUUsSUFBSSxNQUFBLEVBQUUsWUFBWSxjQUFBLEVBQUUsTUFBTSxRQUFBLEVBQUUsQ0FBQztJQUN6RCxJQUFNLGtCQUFrQixHQUFHO1FBQ3pCLEtBQUssT0FBQTtRQUNMLEtBQUssT0FBQTtRQUNMLFlBQVksY0FBQTtRQUNaLGFBQWEsZUFBQTtRQUNiLFdBQVcsYUFBQTtRQUNYLGdCQUFnQixrQkFBQTtLQUNqQixDQUFDO0lBRUYsSUFBTSxXQUFXLEdBQUc7UUFDbEIsTUFBTSxFQUFFLGNBQU0sT0FBQSxZQUFZLENBQUMsaUJBQWlCLENBQUMsRUFBL0IsQ0FBK0I7UUFDN0MsT0FBTyxFQUFFLGNBQU0sT0FBQSxhQUFhLENBQUMsa0JBQWtCLENBQUMsRUFBakMsQ0FBaUM7S0FDakQsQ0FBQztJQUVGLElBQU0sY0FBYyxHQUFHO1FBQ3JCLEtBQUssRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSztRQUNqQyxVQUFVLFlBQUE7UUFDVixZQUFZLGNBQUE7UUFDWixPQUFPLEVBQUUsQ0FBQyxLQUFLLFlBQVksQ0FBQyxNQUFNO1lBQ2hDLENBQUMsQ0FBQyx3QkFBd0IsRUFBRTtZQUM1QixDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFBRTtRQUN4QyxLQUFLLEVBQUUsRUFBRTtLQUNWLENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFNBQVM7UUFDeEIsYUFBYSxJQUFJLGlCQUFpQixDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsYUFBYSxlQUFBLEVBQUUsVUFBVSxZQUFBLEVBQUUsQ0FBQztRQUN6RSxvQkFBQyxTQUFTLGVBQUssY0FBYyxFQUFJLENBQzdCLENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/image-slider/component.tsx
var component_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var component_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var component_ImageSlide = /** @class */ (function (_super) {
    component_extends(ImageSlide, _super);
    function ImageSlide(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE(props.data);
        return _this;
    }
    ImageSlide.prototype.componentDidMount = function () {
        this.initDataSlide();
    };
    /**
     * When component will receive new props -> init data for slide
     * @param nextProps prop from parent
     */
    ImageSlide.prototype.componentWillReceiveProps = function (nextProps) {
        this.initDataSlide(nextProps.data);
    };
    /**
     * Init data for slide
     * When : Component will mount OR Component will receive new props
     */
    ImageSlide.prototype.initDataSlide = function (_newMagazineList) {
        var _this = this;
        if (_newMagazineList === void 0) { _newMagazineList = this.state.magazineList; }
        if (true === Object(responsive["b" /* isDesktopVersion */])()) {
            if (false === this.state.firstInit) {
                this.setState({ firstInit: true });
            }
            /**
             * On DESKTOP
             * Init data for magazine slide & magazine slide selected
             */
            var _magazineSlide_1 = [];
            var groupMagazine_1 = {
                id: 0,
                list: []
            };
            /** Assign data into each slider */
            if (_newMagazineList.length > (this.props.column || 3)) {
                Array.isArray(_newMagazineList)
                    && _newMagazineList.map(function (magazine, $index) {
                        groupMagazine_1.id = _magazineSlide_1.length;
                        groupMagazine_1.list.push(magazine);
                        if (groupMagazine_1.list.length === _this.props.column) {
                            _magazineSlide_1.push(Object.assign({}, groupMagazine_1));
                            groupMagazine_1.list = [];
                        }
                    });
            }
            else {
                _magazineSlide_1 = [{ id: 0, list: _newMagazineList }];
            }
            this.setState({
                magazineList: _newMagazineList,
                magazineSlide: _magazineSlide_1,
                magazineSlideSelected: _magazineSlide_1[0] || {}
            });
        }
        else {
            /**
             * On Mobile
             * Only init data for list magazine, not apply slide animation
             */
            this.setState({ magazineList: _newMagazineList });
        }
    };
    /**
     * Navigate slide by button left or right
     * @param _direction `LEFT` or `RIGHT`
     * Will set new index value by @param _direction
     */
    ImageSlide.prototype.navSlide = function (_direction) {
        var _a = this.state, magazineSlide = _a.magazineSlide, countChangeSlide = _a.countChangeSlide;
        /**
         * If navigate to right: increase index value -> set +1 by countChangeSlide
         * If vavigate to left: decrease index value -> set -1 by countChangeSlide
         */
        var newIndexValue = 'left' === _direction ? -1 : 1;
        newIndexValue += countChangeSlide;
        /**
         * Validate new value in range [0, magazineSlide.length - 1]
         */
        newIndexValue = newIndexValue === magazineSlide.length
            ? 0 /** If over max value -> set 0 */
            : (newIndexValue === -1
                /** If under min value -> set magazineSlide.length - 1 */
                ? magazineSlide.length - 1
                : newIndexValue);
        /** Change to new index value */
        this.selectSlide(newIndexValue);
    };
    ImageSlide.prototype.navLeftSlide = function () {
        this.navSlide('left');
    };
    ImageSlide.prototype.navRightSlide = function () {
        this.navSlide('right');
    };
    /**
     * Change slide by set state and setTimeout for animation
     * @param _index new index value
     */
    ImageSlide.prototype.selectSlide = function (_index) {
        var _this = this;
        /** Change background */
        setTimeout(function () { return _this.setState(function (prevState, props) { return ({
            countChangeSlide: _index,
            magazineSlideSelected: prevState.magazineSlide[_index],
        }); }); }, 10);
    };
    ImageSlide.prototype.handleMouseHover = function () {
        var _a = this.props, _b = _a.data, data = _b === void 0 ? [] : _b, _c = _a.column, column = _c === void 0 ? 3 : _c;
        var preLoadImageList = Array.isArray(data)
            ? data
                .filter(function (item, $index) { return $index >= column; })
                .map(function (item) { return item.cover_image.original_url; })
            : [];
        Object(utils_image["b" /* preLoadImage */])(preLoadImageList);
    };
    // shouldComponentUpdate(nextProps: IProps, nextState: IState) {
    //   // if (this.props.data.length !== nextProps.data.length) { return true; }
    //   // if (this.state.countChangeSlide !== nextState.countChangeSlide) { return true; }
    //   // if (this.props.data.length > 0 && this.state.firstInit !== nextState.firstInit) { return true; }
    //   return true;
    // }
    ImageSlide.prototype.render = function () {
        var _this = this;
        var renderViewProps = {
            props: this.props,
            state: this.state,
            handleMouseHover: this.handleMouseHover.bind(this),
            selectSlide: function (index) { return _this.selectSlide(index); },
            navLeftSlide: this.navLeftSlide.bind(this),
            navRightSlide: this.navRightSlide.bind(this)
        };
        return view(renderViewProps);
    };
    ImageSlide.defaultProps = DEFAULT_PROPS;
    ImageSlide = component_decorate([
        radium
    ], ImageSlide);
    return ImageSlide;
}(react["Component"]));
;
/* harmony default export */ var image_slider_component = (component_ImageSlide);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRzdELE9BQU8sRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQzVELE9BQU8sVUFBVSxNQUFNLFFBQVEsQ0FBQztBQUdoQztJQUF5Qiw4QkFBK0I7SUFHdEQsb0JBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDOztJQUN6QyxDQUFDO0lBRUQsc0NBQWlCLEdBQWpCO1FBQ0UsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFFRDs7O09BR0c7SUFDSCw4Q0FBeUIsR0FBekIsVUFBMEIsU0FBUztRQUNqQyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsa0NBQWEsR0FBYixVQUFjLGdCQUFzRDtRQUFwRSxpQkEwQ0M7UUExQ2EsaUNBQUEsRUFBQSxtQkFBK0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZO1FBQ2xFLEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUVoQyxFQUFFLENBQUMsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFZLENBQUMsQ0FBQztZQUFDLENBQUM7WUFDckY7OztlQUdHO1lBQ0gsSUFBSSxnQkFBYyxHQUFlLEVBQUUsQ0FBQztZQUNwQyxJQUFJLGVBQWEsR0FBc0M7Z0JBQ3JELEVBQUUsRUFBRSxDQUFDO2dCQUNMLElBQUksRUFBRSxFQUFFO2FBQ1QsQ0FBQztZQUVGLG1DQUFtQztZQUNuQyxFQUFFLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZELEtBQUssQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUM7dUJBQzFCLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxVQUFDLFFBQVEsRUFBRSxNQUFNO3dCQUN2QyxlQUFhLENBQUMsRUFBRSxHQUFHLGdCQUFjLENBQUMsTUFBTSxDQUFDO3dCQUN6QyxlQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFFbEMsRUFBRSxDQUFDLENBQUMsZUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssS0FBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDOzRCQUNwRCxnQkFBYyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxlQUFhLENBQUMsQ0FBQyxDQUFDOzRCQUN0RCxlQUFhLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQzt3QkFDMUIsQ0FBQztvQkFDSCxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixnQkFBYyxHQUFHLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7WUFDdkQsQ0FBQztZQUVELElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ1osWUFBWSxFQUFFLGdCQUFnQjtnQkFDOUIsYUFBYSxFQUFFLGdCQUFjO2dCQUM3QixxQkFBcUIsRUFBRSxnQkFBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUU7YUFDckMsQ0FBQyxDQUFDO1FBQ2YsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ047OztlQUdHO1lBQ0gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLFlBQVksRUFBRSxnQkFBZ0IsRUFBWSxDQUFDLENBQUM7UUFDOUQsQ0FBQztJQUNILENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsNkJBQVEsR0FBUixVQUFTLFVBQVU7UUFDWCxJQUFBLGVBQWdELEVBQTlDLGdDQUFhLEVBQUUsc0NBQWdCLENBQWdCO1FBRXZEOzs7V0FHRztRQUNILElBQUksYUFBYSxHQUFHLE1BQU0sS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbkQsYUFBYSxJQUFJLGdCQUFnQixDQUFDO1FBRWxDOztXQUVHO1FBQ0gsYUFBYSxHQUFHLGFBQWEsS0FBSyxhQUFhLENBQUMsTUFBTTtZQUNwRCxDQUFDLENBQUMsQ0FBQyxDQUFDLGlDQUFpQztZQUNyQyxDQUFDLENBQUMsQ0FDQSxhQUFhLEtBQUssQ0FBQyxDQUFDO2dCQUNsQix5REFBeUQ7Z0JBQ3pELENBQUMsQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUM7Z0JBQzFCLENBQUMsQ0FBQyxhQUFhLENBQ2xCLENBQUM7UUFFSixnQ0FBZ0M7UUFDaEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRUQsaUNBQVksR0FBWjtRQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDeEIsQ0FBQztJQUVELGtDQUFhLEdBQWI7UUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxnQ0FBVyxHQUFYLFVBQVksTUFBTTtRQUFsQixpQkFPQztRQUxDLHdCQUF3QjtRQUN4QixVQUFVLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxRQUFRLENBQUMsVUFBQyxTQUFTLEVBQUUsS0FBSyxJQUFLLE9BQUEsQ0FBQztZQUNwRCxnQkFBZ0IsRUFBRSxNQUFNO1lBQ3hCLHFCQUFxQixFQUFFLFNBQVMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDO1NBQzVDLENBQUEsRUFIeUMsQ0FHekMsQ0FBQyxFQUhJLENBR0osRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNyQixDQUFDO0lBRUQscUNBQWdCLEdBQWhCO1FBQ1EsSUFBQSxlQUFzQyxFQUFwQyxZQUFTLEVBQVQsOEJBQVMsRUFBRSxjQUFVLEVBQVYsK0JBQVUsQ0FBZ0I7UUFFN0MsSUFBTSxnQkFBZ0IsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztZQUMxQyxDQUFDLENBQUMsSUFBSTtpQkFDSCxNQUFNLENBQUMsVUFBQyxJQUFJLEVBQUUsTUFBTSxJQUFLLE9BQUEsTUFBTSxJQUFJLE1BQU0sRUFBaEIsQ0FBZ0IsQ0FBQztpQkFDMUMsR0FBRyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQTdCLENBQTZCLENBQUM7WUFDN0MsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUVQLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFRCxnRUFBZ0U7SUFDaEUsOEVBQThFO0lBQzlFLHdGQUF3RjtJQUN4Rix3R0FBd0c7SUFFeEcsaUJBQWlCO0lBQ2pCLElBQUk7SUFFSiwyQkFBTSxHQUFOO1FBQUEsaUJBV0M7UUFWQyxJQUFNLGVBQWUsR0FBRztZQUN0QixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLGdCQUFnQixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2xELFdBQVcsRUFBRSxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQXZCLENBQXVCO1lBQzdDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUMsYUFBYSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztTQUM3QyxDQUFDO1FBRUYsTUFBTSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBdEpNLHVCQUFZLEdBQVcsYUFBYSxDQUFDO0lBRHhDLFVBQVU7UUFEZixNQUFNO09BQ0QsVUFBVSxDQXdKZjtJQUFELGlCQUFDO0NBQUEsQUF4SkQsQ0FBeUIsS0FBSyxDQUFDLFNBQVMsR0F3SnZDO0FBQUEsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/image-slider/index.tsx

/* harmony default export */ var image_slider = __webpack_exports__["a"] = (image_slider_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxVQUFVLE1BQU0sYUFBYSxDQUFDO0FBQ3JDLGVBQWUsVUFBVSxDQUFDIn0=

/***/ }),

/***/ 774:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FEEDABLE_TYPE; });
var FEEDABLE_TYPE = {
    FEEDBACK: 'Feedback',
    BLOG: 'Blog',
    LOVE: 'Love',
    THEME: 'Theme',
    BRAND: 'Brand',
    DISCOUNTCODE: 'DiscountCode',
    UNBOXING: 'Unboxing',
    BOX: 'Box',
    BROWSENODE: 'BrowseNode'
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmVlZGFibGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJmZWVkYWJsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUc7SUFDM0IsUUFBUSxFQUFFLFVBQVU7SUFDcEIsSUFBSSxFQUFFLE1BQU07SUFDWixJQUFJLEVBQUUsTUFBTTtJQUNaLEtBQUssRUFBRSxPQUFPO0lBQ2QsS0FBSyxFQUFFLE9BQU87SUFDZCxZQUFZLEVBQUUsY0FBYztJQUM1QixRQUFRLEVBQUUsVUFBVTtJQUNwQixHQUFHLEVBQUUsS0FBSztJQUNWLFVBQVUsRUFBRSxZQUFZO0NBQ3pCLENBQUMifQ==

/***/ }),

/***/ 777:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// EXTERNAL MODULE: ./utils/auth.ts + 1 modules
var auth = __webpack_require__(23);

// CONCATENATED MODULE: ./api/tracking.ts


;
var generateParams = function (key, value) {
    return value ? (_a = {}, _a[key] = value, _a) : {};
    var _a;
};
var trackingViewBox = function (_a) {
    var boxId = _a.boxId, expertTrackingItemCode = _a.expertTrackingItemCode, campaignCode = _a.campaignCode, referrerObjectType = _a.referrerObjectType, referrerObjectId = _a.referrerObjectId;
    var csrfToken = Object(auth["c" /* getCsrfToken */])();
    var data = Object.assign({}, {
        csrf_token: csrfToken,
        box_id: boxId
    }, generateParams('expert_tracking_item_code', expertTrackingItemCode), generateParams('campaign_code', campaignCode), generateParams('referrer_object_type', referrerObjectType), generateParams('referrer_object_id', referrerObjectId));
    return Object(restful_method["d" /* post */])({
        path: '/trackings/view_box',
        data: data,
        description: 'Tracking view box',
        errorMesssage: "Can't tracking data. Please try again",
    });
};
/**
* Fetch experts tracking groupst by code
*
* @param {string} code
*/
var fetchExpertsTrackingGroup = function (code) {
    return Object(restful_method["b" /* get */])({
        path: "/experts/tracking_groups/" + code,
        description: 'Fetch experts tracking groupst by code',
        errorMesssage: "Can't getch experts tracking groupst by code. Please try again",
    });
};
var trackingViewGroup = function (_a) {
    var groupObjectType = _a.groupObjectType, groupObjectId = _a.groupObjectId, referrerObjectType = _a.referrerObjectType, referrerObjectId = _a.referrerObjectId, campaignCode = _a.campaignCode;
    var csrfToken = Object(auth["c" /* getCsrfToken */])();
    var data = Object.assign({}, {
        csrf_token: csrfToken,
        group_object_type: groupObjectType,
        group_object_id: groupObjectId
    }, generateParams('referrer_object_type', referrerObjectType), generateParams('referrer_object_id', referrerObjectId), generateParams('campaign_code', campaignCode));
    return Object(restful_method["d" /* post */])({
        path: '/trackings/view_group',
        data: data,
        description: 'Tracking view group',
        errorMesssage: "Can't tracking group. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhY2tpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0cmFja2luZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3JELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFTNUMsQ0FBQztBQUVGLElBQU0sY0FBYyxHQUFHLFVBQUMsR0FBRyxFQUFFLEtBQUs7SUFBSyxPQUFBLEtBQUssQ0FBQyxDQUFDLFdBQUcsR0FBQyxHQUFHLElBQUcsS0FBSyxNQUFHLENBQUMsQ0FBQyxFQUFFOztBQUE3QixDQUE2QixDQUFDO0FBRXJFLE1BQU0sQ0FBQyxJQUFNLGVBQWUsR0FDMUIsVUFBQyxFQUE2RztRQUEzRyxnQkFBSyxFQUFFLGtEQUFzQixFQUFFLDhCQUFZLEVBQUUsMENBQWtCLEVBQUUsc0NBQWdCO0lBRWxGLElBQU0sU0FBUyxHQUFHLFlBQVksRUFBRSxDQUFDO0lBRWpDLElBQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUMzQjtRQUNFLFVBQVUsRUFBRSxTQUFTO1FBQ3JCLE1BQU0sRUFBRSxLQUFLO0tBQ2QsRUFDRCxjQUFjLENBQUMsMkJBQTJCLEVBQUUsc0JBQXNCLENBQUMsRUFDbkUsY0FBYyxDQUFDLGVBQWUsRUFBRSxZQUFZLENBQUMsRUFDN0MsY0FBYyxDQUFDLHNCQUFzQixFQUFFLGtCQUFrQixDQUFDLEVBQzFELGNBQWMsQ0FBQyxvQkFBb0IsRUFBRSxnQkFBZ0IsQ0FBQyxDQUN2RCxDQUFDO0lBRUYsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNWLElBQUksRUFBRSxxQkFBcUI7UUFDM0IsSUFBSSxNQUFBO1FBQ0osV0FBVyxFQUFFLG1CQUFtQjtRQUNoQyxhQUFhLEVBQUUsdUNBQXVDO0tBQ3ZELENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQztBQUVKOzs7O0VBSUU7QUFDRixNQUFNLENBQUMsSUFBTSx5QkFBeUIsR0FDcEMsVUFBQyxJQUFZO0lBQ1gsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSw4QkFBNEIsSUFBTTtRQUN4QyxXQUFXLEVBQUUsd0NBQXdDO1FBQ3JELGFBQWEsRUFBRSxnRUFBZ0U7S0FDaEYsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUosTUFBTSxDQUFDLElBQU0saUJBQWlCLEdBQzVCLFVBQUMsRUFJZTtRQUpiLG9DQUFlLEVBQ2hCLGdDQUFhLEVBQ2IsMENBQWtCLEVBQ2xCLHNDQUFnQixFQUNoQiw4QkFBWTtJQUVaLElBQU0sU0FBUyxHQUFHLFlBQVksRUFBRSxDQUFDO0lBRWpDLElBQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUMzQjtRQUNFLFVBQVUsRUFBRSxTQUFTO1FBQ3JCLGlCQUFpQixFQUFFLGVBQWU7UUFDbEMsZUFBZSxFQUFFLGFBQWE7S0FDL0IsRUFDRCxjQUFjLENBQUMsc0JBQXNCLEVBQUUsa0JBQWtCLENBQUMsRUFDMUQsY0FBYyxDQUFDLG9CQUFvQixFQUFFLGdCQUFnQixDQUFDLEVBQ3RELGNBQWMsQ0FBQyxlQUFlLEVBQUUsWUFBWSxDQUFDLENBQzlDLENBQUM7SUFFRixNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ1YsSUFBSSxFQUFFLHVCQUF1QjtRQUM3QixJQUFJLE1BQUE7UUFDSixXQUFXLEVBQUUscUJBQXFCO1FBQ2xDLGFBQWEsRUFBRSx3Q0FBd0M7S0FDeEQsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDIn0=
// EXTERNAL MODULE: ./constants/api/tracking.ts
var tracking = __webpack_require__(62);

// CONCATENATED MODULE: ./action/tracking.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return trackingViewBoxAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return trackingViewGroupAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fetchExpertsTrackingGroupAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return saveProductTrackingAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return saveUtmIdTrackingAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return changeRoutingAction; });


/** Tracking view box */
var trackingViewBoxAction = function (_a) {
    var boxId = _a.boxId, expertTrackingItemCode = _a.expertTrackingItemCode, campaignCode = _a.campaignCode, referrerObjectType = _a.referrerObjectType, referrerObjectId = _a.referrerObjectId;
    return function (dispatch, getState) {
        return dispatch({
            type: tracking["f" /* TRACKING_VIEW_BOX */],
            payload: { promise: trackingViewBox({ boxId: boxId, expertTrackingItemCode: expertTrackingItemCode, campaignCode: campaignCode, referrerObjectType: referrerObjectType, referrerObjectId: referrerObjectId }).then(function (res) { return res; }) },
        });
    };
};
/** Tracking view group */
var trackingViewGroupAction = function (_a) {
    var groupObjectType = _a.groupObjectType, groupObjectId = _a.groupObjectId, referrerObjectType = _a.referrerObjectType, referrerObjectId = _a.referrerObjectId, campaignCode = _a.campaignCode;
    return function (dispatch, getState) {
        return dispatch({
            type: tracking["g" /* TRACKING_VIEW_GROUP */],
            payload: {
                promise: trackingViewGroup({
                    groupObjectType: groupObjectType,
                    groupObjectId: groupObjectId,
                    referrerObjectType: referrerObjectType,
                    referrerObjectId: referrerObjectId,
                    campaignCode: campaignCode
                }).then(function (res) { return res; })
            },
            meta: { groupObjectId: groupObjectId, campaignCode: campaignCode }
        });
    };
};
/**
* Fetch experts tracking groupst by code
*
* @param {string} code
*/
var fetchExpertsTrackingGroupAction = function (code) {
    return function (dispatch, getState) {
        return dispatch({
            type: tracking["b" /* FETCH_EXPERTS_TRACKING_GROUP */],
            payload: { promise: fetchExpertsTrackingGroup(code).then(function (res) { return res; }) },
            meta: { code: code }
        });
    };
};
/**
* Save product tracking
*
* @param {object} product
*/
var saveProductTrackingAction = function (data) {
    return function (dispatch, getState) {
        return dispatch({
            type: tracking["c" /* SAVE_PRODUCT_TRACKING */],
            payload: data,
        });
    };
};
/**
* Save utm id tracking
*
* @param {string} utmId
*/
var saveUtmIdTrackingAction = function (_a) {
    var utmId = _a.utmId;
    return function (dispatch, getState) {
        return dispatch({
            type: tracking["d" /* SAVE_UTM_ID_TRACKING */],
            payload: utmId,
        });
    };
};
var changeRoutingAction = function (_a) {
    var routing = _a.routing;
    return function (dispatch, getState) {
        return dispatch({
            type: tracking["a" /* CHANGE_ROUTING */],
            payload: { routing: routing },
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhY2tpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0cmFja2luZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBRUwsZUFBZSxFQUNmLGlCQUFpQixFQUNqQix5QkFBeUIsRUFDMUIsTUFBTSxpQkFBaUIsQ0FBQztBQUV6QixPQUFPLEVBQ0wsaUJBQWlCLEVBQ2pCLG1CQUFtQixFQUNuQiw0QkFBNEIsRUFDNUIscUJBQXFCLEVBQ3JCLG9CQUFvQixFQUNwQixjQUFjLEVBQ2YsTUFBTSwyQkFBMkIsQ0FBQztBQUVuQyx3QkFBd0I7QUFDeEIsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQ2hDLFVBQUMsRUFBNkc7UUFBM0csZ0JBQUssRUFBRSxrREFBc0IsRUFBRSw4QkFBWSxFQUFFLDBDQUFrQixFQUFFLHNDQUFnQjtJQUNsRixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsaUJBQWlCO1lBQ3ZCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxlQUFlLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxzQkFBc0Isd0JBQUEsRUFBRSxZQUFZLGNBQUEsRUFBRSxrQkFBa0Isb0JBQUEsRUFBRSxnQkFBZ0Isa0JBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1NBQzlJLENBQUM7SUFIRixDQUdFO0FBSkosQ0FJSSxDQUFDO0FBRVQsMEJBQTBCO0FBQzFCLE1BQU0sQ0FBQyxJQUFNLHVCQUF1QixHQUNsQyxVQUFDLEVBTUE7UUFMQyxvQ0FBZSxFQUNmLGdDQUFhLEVBQ2IsMENBQWtCLEVBQ2xCLHNDQUFnQixFQUNoQiw4QkFBWTtJQUVaLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxtQkFBbUI7WUFDekIsT0FBTyxFQUFFO2dCQUNQLE9BQU8sRUFBRSxpQkFBaUIsQ0FBQztvQkFDekIsZUFBZSxpQkFBQTtvQkFDZixhQUFhLGVBQUE7b0JBQ2Isa0JBQWtCLG9CQUFBO29CQUNsQixnQkFBZ0Isa0JBQUE7b0JBQ2hCLFlBQVksY0FBQTtpQkFDYixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQzthQUNwQjtZQUNELElBQUksRUFBRSxFQUFFLGFBQWEsZUFBQSxFQUFFLFlBQVksY0FBQSxFQUFFO1NBQ3RDLENBQUM7SUFaRixDQVlFO0FBYkosQ0FhSSxDQUFDO0FBRVQ7Ozs7RUFJRTtBQUNGLE1BQU0sQ0FBQyxJQUFNLCtCQUErQixHQUMxQyxVQUFDLElBQVk7SUFDWCxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsNEJBQTRCO1lBQ2xDLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDdEUsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUU7U0FDZixDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUVUOzs7O0VBSUU7QUFDRixNQUFNLENBQUMsSUFBTSx5QkFBeUIsR0FDcEMsVUFBQyxJQUFJO0lBQ0gsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLHFCQUFxQjtZQUMzQixPQUFPLEVBQUUsSUFBSTtTQUNkLENBQUM7SUFIRixDQUdFO0FBSkosQ0FJSSxDQUFDO0FBRVQ7Ozs7RUFJRTtBQUNGLE1BQU0sQ0FBQyxJQUFNLHVCQUF1QixHQUNsQyxVQUFDLEVBQVM7UUFBUCxnQkFBSztJQUNOLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxvQkFBb0I7WUFDMUIsT0FBTyxFQUFFLEtBQUs7U0FDZixDQUFDO0lBSEYsQ0FHRTtBQUpKLENBSUksQ0FBQztBQUVULE1BQU0sQ0FBQyxJQUFNLG1CQUFtQixHQUFHLFVBQUMsRUFBVztRQUFULG9CQUFPO0lBQU8sT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ25FLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLGNBQWM7WUFDcEIsT0FBTyxFQUFFLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDckIsQ0FBQztJQUhGLENBR0U7QUFKOEMsQ0FJOUMsQ0FBQyJ9

/***/ }),

/***/ 781:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./constants/application/default.ts
var application_default = __webpack_require__(760);

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// CONCATENATED MODULE: ./api/banner.ts


;
var fetchBanner = function (_a) {
    var idBanner = _a.idBanner, _b = _a.limit, limit = _b === void 0 ? application_default["b" /* BANNER_LIMIT_DEFAULT */] : _b;
    var query = "?limit_numer=" + limit;
    return Object(restful_method["b" /* get */])({
        path: "/banners/" + idBanner + query,
        description: 'Get banner list by id | GET BANNER',
        errorMesssage: "Can't fetch list banner. Please try again",
    });
};
var fetchTheme = function () {
    return Object(restful_method["b" /* get */])({
        path: "/themes",
        description: 'Fetch list all themes | GET THEME',
        errorMesssage: "Can't fetch list all themes. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFubmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYmFubmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxHQUFHLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUs5QyxDQUFDO0FBRUYsTUFBTSxDQUFDLElBQU0sV0FBVyxHQUN0QixVQUFDLEVBQTZEO1FBQTNELHNCQUFRLEVBQUUsYUFBNEIsRUFBNUIsaURBQTRCO0lBQ3ZDLElBQU0sS0FBSyxHQUFHLGtCQUFnQixLQUFPLENBQUM7SUFFdEMsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSxjQUFZLFFBQVEsR0FBRyxLQUFPO1FBQ3BDLFdBQVcsRUFBRSxvQ0FBb0M7UUFDakQsYUFBYSxFQUFFLDJDQUEyQztLQUMzRCxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSixNQUFNLENBQUMsSUFBTSxVQUFVLEdBQUc7SUFDeEIsT0FBQSxHQUFHLENBQUM7UUFDRixJQUFJLEVBQUUsU0FBUztRQUNmLFdBQVcsRUFBRSxtQ0FBbUM7UUFDaEQsYUFBYSxFQUFFLCtDQUErQztLQUMvRCxDQUFDO0FBSkYsQ0FJRSxDQUFDIn0=
// EXTERNAL MODULE: ./constants/api/banner.ts
var banner = __webpack_require__(172);

// CONCATENATED MODULE: ./action/banner.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fetchBannerAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fetchThemeAction; });


/**
 *  Fetch Banner list Action with bannerID and limit value
 *
 * @param {string} idBanner
 * @param {number} limit defalut with `BANNER_LIMIT_DEFAULT`
 */
var fetchBannerAction = function (_a) {
    var idBanner = _a.idBanner, limit = _a.limit;
    return function (dispatch, getState) {
        return dispatch({
            type: banner["a" /* FETCH_BANNER */],
            payload: { promise: fetchBanner({ idBanner: idBanner, limit: limit }).then(function (res) { return res; }) },
            meta: { metaFilter: { idBanner: idBanner, limit: limit } }
        });
    };
};
/** Fecth list all themes */
var fetchThemeAction = function () {
    return function (dispatch, getState) {
        return dispatch({
            type: banner["b" /* FETCH_THEME */],
            payload: { promise: fetchTheme().then(function (res) { return res; }) },
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFubmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYmFubmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBcUIsV0FBVyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzRSxPQUFPLEVBQUUsWUFBWSxFQUFFLFdBQVcsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBRXBFOzs7OztHQUtHO0FBQ0gsTUFBTSxDQUFDLElBQU0saUJBQWlCLEdBQzVCLFVBQUMsRUFBc0M7UUFBcEMsc0JBQVEsRUFBRSxnQkFBSztJQUNoQixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsWUFBWTtZQUNsQixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsV0FBVyxDQUFDLEVBQUUsUUFBUSxVQUFBLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN2RSxJQUFJLEVBQUUsRUFBRSxVQUFVLEVBQUUsRUFBRSxRQUFRLFVBQUEsRUFBRSxLQUFLLE9BQUEsRUFBRSxFQUFFO1NBQzFDLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQsNEJBQTRCO0FBQzVCLE1BQU0sQ0FBQyxJQUFNLGdCQUFnQixHQUFHO0lBQzlCLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxXQUFXO1lBQ2pCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7U0FDcEQsQ0FBQztJQUhGLENBR0U7QUFKSixDQUlJLENBQUMifQ==

/***/ }),

/***/ 782:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// CONCATENATED MODULE: ./api/menu.ts

/** Get Main menu - Browsers node */
var fetchListMenu = function () { return Object(restful_method["b" /* get */])({
    path: '/browse_nodes',
    description: 'Get list menu | BROWSE NODE - MENU CHÍNH',
    errorMesssage: "Can't get list Menu. Please try again",
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVudS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1lbnUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBRS9DLG9DQUFvQztBQUNwQyxNQUFNLENBQUMsSUFBTSxhQUFhLEdBQ3hCLGNBQU0sT0FBQSxHQUFHLENBQUM7SUFDUixJQUFJLEVBQUUsZUFBZTtJQUNyQixXQUFXLEVBQUUsMENBQTBDO0lBQ3ZELGFBQWEsRUFBRSx1Q0FBdUM7Q0FDdkQsQ0FBQyxFQUpJLENBSUosQ0FBQyJ9
// EXTERNAL MODULE: ./constants/api/menu.ts
var menu = __webpack_require__(114);

// CONCATENATED MODULE: ./action/menu.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return showHideMobileMenuAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fetchListMenuAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return updateMenuSelectedAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return showHideInfoMenuAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return showHideSpecialDealMenuAction; });
/* unused harmony export showHideMobileMagazineMenuAction */


/**
 * Show / Hide CartSumary with state params
 *
 * @param {boolean} state
 */
var showHideMobileMenuAction = function (state) {
    if (state === void 0) { state = false; }
    return ({
        type: menu["c" /* DISPLAY_MOBILE_MENU */],
        payload: state
    });
};
/** Get Main menu - Browsers node */
var fetchListMenuAction = function () { return function (dispatch, getState) {
    return dispatch({
        type: menu["e" /* FETCH_LIST_MENU */],
        payload: { promise: fetchListMenu().then(function (res) { return res; }) },
    });
}; };
/** BACKGROUND ACTION: UPDATE SELECT MENU TREE */
var updateMenuSelectedAction = function (idCategory) { return ({
    type: menu["f" /* UPDATE_MENU_SELECTED */],
    payload: { idCategory: idCategory }
}); };
/**
* Show / Hide Info menu with state params
*
* @param {boolean} state
*/
var showHideInfoMenuAction = function (state) {
    if (state === void 0) { state = false; }
    return ({
        type: menu["a" /* DISPLAY_INFO_MENU */],
        payload: state
    });
};
/**
* Show / Hide specail deal menu with state params
*
* @param {boolean} state
*/
var showHideSpecialDealMenuAction = function (state) {
    if (state === void 0) { state = false; }
    return ({
        type: menu["d" /* DISPLAY_SPECIAL_DEAL_MENU */],
        payload: state
    });
};
/**
* Show / Hide menu of magazine page with state params
*
* @param {boolean} state
*/
var showHideMobileMagazineMenuAction = function (state) {
    if (state === void 0) { state = false; }
    return ({
        type: menu["b" /* DISPLAY_MOBILE_MAGAZINE_MENU */],
        payload: state
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVudS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1lbnUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUU1QyxPQUFPLEVBQ0wsbUJBQW1CLEVBQ25CLGVBQWUsRUFDZixvQkFBb0IsRUFDcEIsaUJBQWlCLEVBQ2pCLHlCQUF5QixFQUN6Qiw0QkFBNEIsRUFDN0IsTUFBTSx1QkFBdUIsQ0FBQztBQUUvQjs7OztHQUlHO0FBQ0gsTUFBTSxDQUFDLElBQU0sd0JBQXdCLEdBQ25DLFVBQUMsS0FBYTtJQUFiLHNCQUFBLEVBQUEsYUFBYTtJQUFLLE9BQUEsQ0FBQztRQUNsQixJQUFJLEVBQUUsbUJBQW1CO1FBQ3pCLE9BQU8sRUFBRSxLQUFLO0tBQ2YsQ0FBQztBQUhpQixDQUdqQixDQUFDO0FBRUwsb0NBQW9DO0FBQ3BDLE1BQU0sQ0FBQyxJQUFNLG1CQUFtQixHQUM5QixjQUFNLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtJQUN2QixPQUFBLFFBQVEsQ0FBQztRQUNQLElBQUksRUFBRSxlQUFlO1FBQ3JCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxhQUFhLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7S0FDdkQsQ0FBQztBQUhGLENBR0UsRUFKRSxDQUlGLENBQUM7QUFFUCxpREFBaUQ7QUFDakQsTUFBTSxDQUFDLElBQU0sd0JBQXdCLEdBQUcsVUFBQSxVQUFVLElBQUksT0FBQSxDQUFDO0lBQ3JELElBQUksRUFBRSxvQkFBb0I7SUFDMUIsT0FBTyxFQUFFLEVBQUUsVUFBVSxZQUFBLEVBQUU7Q0FDeEIsQ0FBQyxFQUhvRCxDQUdwRCxDQUFDO0FBRUg7Ozs7RUFJRTtBQUNGLE1BQU0sQ0FBQyxJQUFNLHNCQUFzQixHQUNqQyxVQUFDLEtBQWE7SUFBYixzQkFBQSxFQUFBLGFBQWE7SUFBSyxPQUFBLENBQUM7UUFDbEIsSUFBSSxFQUFFLGlCQUFpQjtRQUN2QixPQUFPLEVBQUUsS0FBSztLQUNmLENBQUM7QUFIaUIsQ0FHakIsQ0FBQztBQUVMOzs7O0VBSUU7QUFDRixNQUFNLENBQUMsSUFBTSw2QkFBNkIsR0FDeEMsVUFBQyxLQUFhO0lBQWIsc0JBQUEsRUFBQSxhQUFhO0lBQUssT0FBQSxDQUFDO1FBQ2xCLElBQUksRUFBRSx5QkFBeUI7UUFDL0IsT0FBTyxFQUFFLEtBQUs7S0FDZixDQUFDO0FBSGlCLENBR2pCLENBQUM7QUFFTDs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0sZ0NBQWdDLEdBQzNDLFVBQUMsS0FBYTtJQUFiLHNCQUFBLEVBQUEsYUFBYTtJQUFLLE9BQUEsQ0FBQztRQUNsQixJQUFJLEVBQUUsNEJBQTRCO1FBQ2xDLE9BQU8sRUFBRSxLQUFLO0tBQ2YsQ0FBQztBQUhpQixDQUdqQixDQUFDIn0=

/***/ }),

/***/ 793:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./utils/auth.ts + 1 modules
var auth = __webpack_require__(23);

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// CONCATENATED MODULE: ./api/love.ts


;
var fetchLoveList = function (_a) {
    var _b = _a.sort, sort = _b === void 0 ? 'desc' : _b, _c = _a.page, page = _c === void 0 ? 1 : _c, _d = _a.perPage, perPage = _d === void 0 ? 5 : _d;
    var query = "?sort[created_at]=" + sort + "&page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/loves" + query,
        description: 'Get love list',
        errorMesssage: "Can't get love list. Please try again",
    });
};
;
var getLoveDetail = function (_a) {
    var id = _a.id;
    return Object(restful_method["b" /* get */])({
        path: "/loves/" + id,
        description: 'Get love detail',
        errorMesssage: "Can't get love detail. Please try again",
    });
};
var addLove = function (_a) {
    var sharedUrl = _a.sharedUrl;
    return Object(restful_method["d" /* post */])({
        path: "/loves",
        data: {
            csrf_token: Object(auth["c" /* getCsrfToken */])(),
            shared_url: sharedUrl
        },
        description: 'User add love',
        errorMesssage: "Can't add love. Please try again",
    });
};
var getLoveBoxById = function (_a) {
    var id = _a.id;
    return Object(restful_method["b" /* get */])({
        path: "/loves/box/" + id,
        description: 'Get love box by id',
        errorMesssage: "Can't get love box by id. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG92ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxvdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3QyxPQUFPLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBTXBELENBQUM7QUFFRixNQUFNLENBQUMsSUFBTSxhQUFhLEdBQ3hCLFVBQUMsRUFBd0Q7UUFBdEQsWUFBYSxFQUFiLGtDQUFhLEVBQUUsWUFBUSxFQUFSLDZCQUFRLEVBQUUsZUFBVyxFQUFYLGdDQUFXO0lBQ3JDLElBQU0sS0FBSyxHQUFHLHVCQUFxQixJQUFJLGNBQVMsSUFBSSxrQkFBYSxPQUFTLENBQUM7SUFFM0UsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSxXQUFTLEtBQU87UUFDdEIsV0FBVyxFQUFFLGVBQWU7UUFDNUIsYUFBYSxFQUFFLHVDQUF1QztLQUN2RCxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFJSCxDQUFDO0FBRUYsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUN4QixVQUFDLEVBQXdCO1FBQXRCLFVBQUU7SUFBeUIsT0FBQSxHQUFHLENBQUM7UUFDaEMsSUFBSSxFQUFFLFlBQVUsRUFBSTtRQUNwQixXQUFXLEVBQUUsaUJBQWlCO1FBQzlCLGFBQWEsRUFBRSx5Q0FBeUM7S0FDekQsQ0FBQztBQUo0QixDQUk1QixDQUFDO0FBRUwsTUFBTSxDQUFDLElBQU0sT0FBTyxHQUNsQixVQUFDLEVBQWE7UUFBWCx3QkFBUztJQUVWLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDVixJQUFJLEVBQUUsUUFBUTtRQUNkLElBQUksRUFBRTtZQUNKLFVBQVUsRUFBRSxZQUFZLEVBQUU7WUFDMUIsVUFBVSxFQUFFLFNBQVM7U0FDdEI7UUFDRCxXQUFXLEVBQUUsZUFBZTtRQUM1QixhQUFhLEVBQUUsa0NBQWtDO0tBQ2xELENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQztBQUVKLE1BQU0sQ0FBQyxJQUFNLGNBQWMsR0FBRyxVQUFDLEVBQU07UUFBSixVQUFFO0lBQU8sT0FBQSxHQUFHLENBQUM7UUFDNUMsSUFBSSxFQUFFLGdCQUFjLEVBQUk7UUFDeEIsV0FBVyxFQUFFLG9CQUFvQjtRQUNqQyxhQUFhLEVBQUUsNENBQTRDO0tBQzVELENBQUM7QUFKd0MsQ0FJeEMsQ0FBQyJ9
// EXTERNAL MODULE: ./constants/api/love.ts
var love = __webpack_require__(49);

// CONCATENATED MODULE: ./action/love.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fetchLoveListAction; });
/* unused harmony export getLoveDetailAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return addLoveAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getLoveBoxByIdAction; });


/**
 * Fetch love list by filter params
 *
 * @param {'asc' | 'desc'} sort
 * @param {number} page ex: 1, 2, 3, 4
 * @param {number} perPage ex: 10, 15, 20
 */
var fetchLoveListAction = function (_a) {
    var sort = _a.sort, page = _a.page, perPage = _a.perPage;
    return function (dispatch, getState) {
        return dispatch({
            type: love["b" /* FETCH_LOVE_LIST */],
            payload: { promise: fetchLoveList({ sort: sort, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { page: page, perPage: perPage }
        });
    };
};
/**
 * Get love detail by id
 *
 * @param {number} id
 */
var getLoveDetailAction = function (_a) {
    var id = _a.id;
    return function (dispatch, getState) {
        return dispatch({
            type: love["d" /* GET_LOVE_DETAIL */],
            payload: { promise: getLoveDetail({ id: id }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
/**
* Add love by share_url
*
* @param {string} share_url
*/
var addLoveAction = function (_a) {
    var sharedUrl = _a.sharedUrl;
    return function (dispatch, getState) {
        return dispatch({
            type: love["a" /* ADD_LOVE */],
            payload: { promise: addLove({ sharedUrl: sharedUrl }).then(function (res) { return res; }) },
        });
    };
};
/**
 * Get love box by id
 *
 * @param {number} id
 */
var getLoveBoxByIdAction = function (_a) {
    var id = _a.id;
    return function (dispatch, getState) {
        return dispatch({
            type: love["c" /* GET_LOVE_BOX */],
            payload: { promise: getLoveBoxById({ id: id }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG92ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxvdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLE9BQU8sRUFDUCxhQUFhLEVBQ2IsYUFBYSxFQUNiLGNBQWMsR0FHZixNQUFNLGFBQWEsQ0FBQztBQUVyQixPQUFPLEVBQ0wsUUFBUSxFQUNSLFlBQVksRUFDWixlQUFlLEVBQ2YsZUFBZSxHQUNoQixNQUFNLHVCQUF1QixDQUFDO0FBRS9COzs7Ozs7R0FNRztBQUNILE1BQU0sQ0FBQyxJQUFNLG1CQUFtQixHQUM5QixVQUFDLEVBQXVDO1FBQXJDLGNBQUksRUFBRSxjQUFJLEVBQUUsb0JBQU87SUFDcEIsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLGVBQWU7WUFDckIsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLGFBQWEsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDN0UsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDeEIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7OztHQUlHO0FBQ0gsTUFBTSxDQUFDLElBQU0sbUJBQW1CLEdBQzlCLFVBQUMsRUFBd0I7UUFBdEIsVUFBRTtJQUNILE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxlQUFlO1lBQ3JCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxhQUFhLENBQUMsRUFBRSxFQUFFLElBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQzVELElBQUksRUFBRSxFQUFFLEVBQUUsSUFBQSxFQUFFO1NBQ2IsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUN4QixVQUFDLEVBQWE7UUFBWCx3QkFBUztJQUNWLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxRQUFRO1lBQ2QsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLE9BQU8sQ0FBQyxFQUFFLFNBQVMsV0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7U0FDOUQsQ0FBQztJQUhGLENBR0U7QUFKSixDQUlJLENBQUM7QUFFVDs7OztHQUlHO0FBQ0gsTUFBTSxDQUFDLElBQU0sb0JBQW9CLEdBQUcsVUFBQyxFQUFNO1FBQUosVUFBRTtJQUN2QyxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsWUFBWTtZQUNsQixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsY0FBYyxDQUFDLEVBQUUsRUFBRSxJQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUM3RCxJQUFJLEVBQUUsRUFBRSxFQUFFLElBQUEsRUFBRTtTQUNiLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDIn0=

/***/ }),

/***/ 803:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./constants/application/global.ts
var global = __webpack_require__(24);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./utils/index.ts
var utils = __webpack_require__(788);

// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/item/feedback-testimonial/ItemFeedback.style.tsx


var generateSwitchStyle = function (mobile, desktop) {
    var switchStyle = {
        MOBILE: { paddingRight: 10, width: mobile, minWidth: mobile },
        DESKTOP: { width: desktop }
    };
    return switchStyle[window.DEVICE_VERSION];
};
/* harmony default export */ var ItemFeedback_style = ({
    container: {
        height: '100%',
        itemSlider: {
            width: "100%",
            height: "100%",
            display: variable["display"].inlineBlock,
            borderRadius: 5,
            overflow: 'hidden',
            boxShadow: variable["shadowBlur"],
            position: variable["position"].relative
        },
        itemSliderPanel: {
            width: '100%',
            paddingTop: '75%',
            position: variable["position"].relative,
        },
        innerItemSliderPanel: function (imgUrl) { return ({
            width: '100%',
            height: '100%',
            top: 0,
            left: 0,
            position: variable["position"].absolute,
            backgroundImage: "url(" + imgUrl + ")",
            backgroundColor: variable["colorF7"],
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            transition: variable["transitionOpacity"]
        }); },
        info: {
            width: '100%',
            padding: 12,
            position: variable["position"].relative,
            background: variable["colorWhite"],
            display: variable["display"].flex,
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
        }
    },
    info: {
        container: [
            layout["a" /* flexContainer */].left, {
                width: '100%',
                marginBottom: 15
            }
        ],
        avatar: {
            width: 40,
            minWidth: 40,
            height: 40,
            borderRadius: 25,
            marginRight: 10,
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundColor: variable["colorE5"],
            transition: variable["transitionOpacity"],
            small: {
                width: 30,
                minWidth: 30,
                height: 30,
                borderRadius: '50%',
            }
        },
        username: {
            paddingRight: 15,
            marginBottom: 5,
            textAlign: 'left',
        },
        detail: {
            flex: 10,
            display: variable["display"].flex,
            flexDirection: 'column',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            justifyContent: 'center',
            username: {
                fontFamily: variable["fontAvenirDemiBold"],
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
                overflow: 'hidden',
                fontSize: 14,
                lineHeight: '22px',
                marginRight: 5,
                color: variable["colorBlack"]
            },
            ratingGroup: {
                display: variable["display"].flex,
                alignItems: 'center',
                lineHeight: '23px',
                height: 18,
                color: variable["color97"],
                cursor: 'pointer'
            }
        },
        description: {
            fontSize: 14,
            overflow: 'hidden',
            lineHeight: '22px',
            textAlign: 'justify',
            height: 66,
            maxHeight: 66,
            width: '100%',
            color: variable["colorBlack08"],
            whiteSpace: 'pre-wrap'
        }
    },
    icon: {
        position: variable["position"].absolute,
        top: '50%',
        left: '50%',
        color: variable["colorWhite"],
        transform: 'translate(-50%, -50%)',
        zIndex: variable["zIndex2"],
        inner: {
            width: 60,
            height: 60,
        }
    },
    videoIcon: {
        width: 70,
        height: 70,
        left: '55%',
        transform: 'translate(-50%, -50%)',
        borderTop: "35px solid " + variable["colorTransparent"],
        borderLeft: "51px solid " + variable["colorWhite"],
        borderBottom: "35px solid " + variable["colorTransparent"],
        position: variable["position"].absolute,
        boxSizing: "border-box",
        opacity: 0.8,
        top: '50%'
    },
    column: {
        1: { width: '100%' },
        2: { width: '50%' },
        3: generateSwitchStyle('75%', '33.33%'),
        4: generateSwitchStyle('47%', '25%'),
        5: generateSwitchStyle('47%', '20%'),
        6: generateSwitchStyle('50%', '16.66%'),
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSXRlbUZlZWRiYWNrLnN0eWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiSXRlbUZlZWRiYWNrLnN0eWxlLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxPQUFPLEtBQUssTUFBTSxNQUFNLHVCQUF1QixDQUFDO0FBQ2hELE9BQU8sS0FBSyxRQUFRLE1BQU0seUJBQXlCLENBQUM7QUFFcEQsSUFBTSxtQkFBbUIsR0FBRyxVQUFDLE1BQU0sRUFBRSxPQUFPO0lBQzFDLElBQU0sV0FBVyxHQUFHO1FBQ2xCLE1BQU0sRUFBRSxFQUFFLFlBQVksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFO1FBQzdELE9BQU8sRUFBRSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUU7S0FDNUIsQ0FBQztJQUVGLE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0FBQzVDLENBQUMsQ0FBQztBQUVGLGVBQWU7SUFDYixTQUFTLEVBQUU7UUFDVCxNQUFNLEVBQUUsTUFBTTtRQUVkLFVBQVUsRUFBRTtZQUNWLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLE1BQU07WUFDZCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxXQUFXO1lBQ3JDLFlBQVksRUFBRSxDQUFDO1lBQ2YsUUFBUSxFQUFFLFFBQVE7WUFDbEIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1lBQzlCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7U0FDckM7UUFFRCxlQUFlLEVBQUM7WUFDZCxLQUFLLEVBQUUsTUFBTTtZQUNiLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7U0FDckM7UUFFRCxvQkFBb0IsRUFBRSxVQUFDLE1BQU0sSUFBSyxPQUFBLENBQUM7WUFDakMsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsTUFBTTtZQUNkLEdBQUcsRUFBRSxDQUFDO1lBQ04sSUFBSSxFQUFFLENBQUM7WUFDUCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1lBQ3BDLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztZQUNqQyxlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87WUFDakMsa0JBQWtCLEVBQUUsUUFBUTtZQUM1QixjQUFjLEVBQUUsT0FBTztZQUN2QixVQUFVLEVBQUUsUUFBUSxDQUFDLGlCQUFpQjtTQUN2QyxDQUFDLEVBWGdDLENBV2hDO1FBRUYsSUFBSSxFQUFFO1lBQ0osS0FBSyxFQUFFLE1BQU07WUFDYixPQUFPLEVBQUUsRUFBRTtZQUNYLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDcEMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxVQUFVO1lBQy9CLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsYUFBYSxFQUFFLFFBQVE7WUFDdkIsY0FBYyxFQUFFLFlBQVk7WUFDNUIsVUFBVSxFQUFFLFlBQVk7U0FDekI7S0FDRjtJQUVELElBQUksRUFBRTtRQUNKLFNBQVMsRUFBRTtZQUNULE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFO2dCQUN6QixLQUFLLEVBQUUsTUFBTTtnQkFDYixZQUFZLEVBQUUsRUFBRTthQUNqQjtTQUFDO1FBRUosTUFBTSxFQUFFO1lBQ04sS0FBSyxFQUFFLEVBQUU7WUFDVCxRQUFRLEVBQUUsRUFBRTtZQUNaLE1BQU0sRUFBRSxFQUFFO1lBQ1YsWUFBWSxFQUFFLEVBQUU7WUFDaEIsV0FBVyxFQUFFLEVBQUU7WUFDZixrQkFBa0IsRUFBRSxRQUFRO1lBQzVCLGNBQWMsRUFBRSxPQUFPO1lBQ3ZCLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztZQUNqQyxVQUFVLEVBQUUsUUFBUSxDQUFDLGlCQUFpQjtZQUV0QyxLQUFLLEVBQUU7Z0JBQ0wsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsWUFBWSxFQUFFLEtBQUs7YUFDcEI7U0FDRjtRQUVELFFBQVEsRUFBRTtZQUNSLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFlBQVksRUFBRSxDQUFDO1lBQ2YsU0FBUyxFQUFFLE1BQU07U0FDbEI7UUFFRCxNQUFNLEVBQUU7WUFDTixJQUFJLEVBQUUsRUFBRTtZQUNSLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsYUFBYSxFQUFFLFFBQVE7WUFDdkIsUUFBUSxFQUFFLFFBQVE7WUFDbEIsWUFBWSxFQUFFLFVBQVU7WUFDeEIsY0FBYyxFQUFFLFFBQVE7WUFFeEIsUUFBUSxFQUFFO2dCQUNSLFVBQVUsRUFBRSxRQUFRLENBQUMsa0JBQWtCO2dCQUN2QyxZQUFZLEVBQUUsVUFBVTtnQkFDeEIsVUFBVSxFQUFFLFFBQVE7Z0JBQ3BCLFFBQVEsRUFBRSxRQUFRO2dCQUNsQixRQUFRLEVBQUUsRUFBRTtnQkFDWixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsV0FBVyxFQUFFLENBQUM7Z0JBQ2QsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2FBQzNCO1lBRUQsV0FBVyxFQUFFO2dCQUNYLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLFVBQVUsRUFBRSxRQUFRO2dCQUNwQixVQUFVLEVBQUUsTUFBTTtnQkFDaEIsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO2dCQUN2QixNQUFNLEVBQUUsU0FBUzthQUNwQjtTQUNGO1FBRUQsV0FBVyxFQUFFO1lBQ1gsUUFBUSxFQUFFLEVBQUU7WUFDWixRQUFRLEVBQUUsUUFBUTtZQUNsQixVQUFVLEVBQUUsTUFBTTtZQUNsQixTQUFTLEVBQUUsU0FBUztZQUNwQixNQUFNLEVBQUUsRUFBRTtZQUNWLFNBQVMsRUFBRSxFQUFFO1lBQ2IsS0FBSyxFQUFFLE1BQU07WUFDYixLQUFLLEVBQUUsUUFBUSxDQUFDLFlBQVk7WUFDNUIsVUFBVSxFQUFFLFVBQVU7U0FDdkI7S0FDRjtJQUVELElBQUksRUFBRTtRQUNKLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7UUFDcEMsR0FBRyxFQUFFLEtBQUs7UUFDVixJQUFJLEVBQUUsS0FBSztRQUNYLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtRQUMxQixTQUFTLEVBQUUsdUJBQXVCO1FBQ2xDLE1BQU0sRUFBRSxRQUFRLENBQUMsT0FBTztRQUV4QixLQUFLLEVBQUU7WUFDTCxLQUFLLEVBQUUsRUFBRTtZQUNULE1BQU0sRUFBRSxFQUFFO1NBQ1g7S0FDRjtJQUVELFNBQVMsRUFBRTtRQUNULEtBQUssRUFBRSxFQUFFO1FBQ1QsTUFBTSxFQUFFLEVBQUU7UUFDVixJQUFJLEVBQUUsS0FBSztRQUNYLFNBQVMsRUFBRSx1QkFBdUI7UUFDbEMsU0FBUyxFQUFFLGdCQUFjLFFBQVEsQ0FBQyxnQkFBa0I7UUFDcEQsVUFBVSxFQUFFLGdCQUFjLFFBQVEsQ0FBQyxVQUFZO1FBQy9DLFlBQVksRUFBRSxnQkFBYyxRQUFRLENBQUMsZ0JBQWtCO1FBQ3ZELFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7UUFDcEMsU0FBUyxFQUFFLFlBQVk7UUFDdkIsT0FBTyxFQUFFLEdBQUc7UUFDWixHQUFHLEVBQUUsS0FBSztLQUNYO0lBRUQsTUFBTSxFQUFFO1FBQ04sQ0FBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRTtRQUNwQixDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFO1FBQ25CLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDO1FBQ3ZDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDO1FBQ3BDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDO1FBQ3BDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDO0tBQ3hDO0NBQ0ssQ0FBQyJ9
// CONCATENATED MODULE: ./components/item/feedback-testimonial/ItemFeedback.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







;
var ItemFeedback_ItemFeedback = /** @class */ (function (_super) {
    __extends(ItemFeedback, _super);
    function ItemFeedback(props) {
        return _super.call(this, props) || this;
    }
    ItemFeedback.prototype.shouldComponentUpdate = function (nextProps, nextState) {
        var isNullCurrentData = null === this.props.data || 'undefined' === typeof this.props.data;
        var isNullNextData = null !== nextProps.data && 'undefined' !== typeof nextProps.data;
        if (isNullCurrentData && isNullNextData) {
            return true;
        }
        return false;
    };
    ItemFeedback.prototype.render = function () {
        var data = this.props.data;
        var pictureUrl = data && data.picture && data.picture.medium_url || data.picture_url || '';
        var avatar = data && data.user && data.user.avatar && data.user.avatar.medium_url
            || data.user_avatar && data.user_avatar.medium_url
            || '';
        return (react["createElement"](react_router_dom["NavLink"], { style: ItemFeedback_style.container.itemSlider, to: routing["q" /* ROUTING_COMMUNITY_PATH */] + "/" + (data && data.id || 0) },
            react["createElement"]("div", null,
                react["createElement"]("div", { style: ItemFeedback_style.container.itemSliderPanel },
                    react["createElement"]("div", { style: [
                            ItemFeedback_style.container.innerItemSliderPanel(pictureUrl),
                        ] }, data && data.video && !!data.video.url && react["createElement"]("div", { style: ItemFeedback_style.videoIcon }))),
                react["createElement"]("div", { style: ItemFeedback_style.container.info },
                    react["createElement"]("div", { style: ItemFeedback_style.info.container },
                        react["createElement"]("div", { style: [{ backgroundImage: "url(" + avatar + ")" }, ItemFeedback_style.info.avatar] }),
                        react["createElement"]("div", { style: ItemFeedback_style.info.detail },
                            react["createElement"]("div", { style: ItemFeedback_style.info.detail.username }, data && data.user && data.user.name || ''),
                            data && data.created_at
                                && (react["createElement"]("div", { style: ItemFeedback_style.info.detail.ratingGroup }, Object(utils["b" /* convertUnixTime */])(data.created_at, global["b" /* DATETIME_TYPE_FORMAT */].SHORT_DATE))))),
                    react["createElement"]("div", { style: ItemFeedback_style.info.description }, data && data.message || '')))));
    };
    ;
    ItemFeedback = __decorate([
        radium
    ], ItemFeedback);
    return ItemFeedback;
}(react["Component"]));
;
/* harmony default export */ var feedback_testimonial_ItemFeedback = (ItemFeedback_ItemFeedback);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSXRlbUZlZWRiYWNrLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiSXRlbUZlZWRiYWNrLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFDakMsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUVqRCxPQUFPLEtBQUssTUFBTSxzQkFBc0IsQ0FBQztBQU14QyxDQUFDO0FBR0Y7SUFBMkIsZ0NBQXdDO0lBQ2pFLHNCQUFZLEtBQUs7ZUFDZixrQkFBTSxLQUFLLENBQUM7SUFDZCxDQUFDO0lBRUQsNENBQXFCLEdBQXJCLFVBQXNCLFNBQVMsRUFBRSxTQUFTO1FBQ3hDLElBQU0saUJBQWlCLEdBQUcsSUFBSSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLFdBQVcsS0FBSyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO1FBQzdGLElBQU0sY0FBYyxHQUFHLElBQUksS0FBSyxTQUFTLENBQUMsSUFBSSxJQUFJLFdBQVcsS0FBSyxPQUFPLFNBQVMsQ0FBQyxJQUFJLENBQUM7UUFFeEYsRUFBRSxDQUFDLENBQUMsaUJBQWlCLElBQUksY0FBYyxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFBQyxDQUFDO1FBRXpELE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQsNkJBQU0sR0FBTjtRQUNVLElBQUEsc0JBQUksQ0FBZ0I7UUFFNUIsSUFBTSxVQUFVLEdBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxFQUFFLENBQUM7UUFDN0YsSUFBTSxNQUFNLEdBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVTtlQUM5RSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVTtlQUMvQyxFQUFFLENBQUM7UUFFUixNQUFNLENBQUMsQ0FDTCxvQkFBQyxPQUFPLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLEVBQUUsRUFBSyxzQkFBc0IsVUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUU7WUFDakc7Z0JBQ0UsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsZUFBZTtvQkFDekMsNkJBQUssS0FBSyxFQUFFOzRCQUNWLEtBQUssQ0FBQyxTQUFTLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDO3lCQUNqRCxJQUNFLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFNBQVMsR0FBUSxDQUMxRSxDQUNGO2dCQUNOLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUk7b0JBQzlCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVM7d0JBQzlCLDZCQUFLLEtBQUssRUFBRSxDQUFDLEVBQUUsZUFBZSxFQUFFLFNBQU8sTUFBTSxNQUFHLEVBQUMsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFRO3dCQUM3RSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNOzRCQUMzQiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxJQUNuQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxFQUFFLENBQ3RDOzRCQUVKLElBQUksSUFBSSxJQUFJLENBQUMsVUFBVTttQ0FDcEIsQ0FDRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxJQUN0QyxlQUFlLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxvQkFBb0IsQ0FBQyxVQUFVLENBQUMsQ0FDOUQsQ0FDUCxDQUVDLENBQ0Y7b0JBQ04sNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFHLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBTyxDQUNsRSxDQUNGLENBQ0UsQ0FDWCxDQUFDO0lBQ0osQ0FBQztJQUFBLENBQUM7SUF0REUsWUFBWTtRQURqQixNQUFNO09BQ0QsWUFBWSxDQXVEakI7SUFBRCxtQkFBQztDQUFBLEFBdkRELENBQTJCLEtBQUssQ0FBQyxTQUFTLEdBdUR6QztBQUFBLENBQUM7QUFFRixlQUFlLFlBQVksQ0FBQyJ9
// CONCATENATED MODULE: ./components/item/feedback-testimonial/index.tsx

/* harmony default export */ var feedback_testimonial = __webpack_exports__["a"] = (feedback_testimonial_ItemFeedback);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxZQUFZLE1BQU0sZ0JBQWdCLENBQUM7QUFDMUMsZUFBZSxZQUFZLENBQUMifQ==

/***/ }),

/***/ 806:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GROUP_OBJECT_TYPE; });
var GROUP_OBJECT_TYPE = {
    HOME_PAGE: 'HomePage',
    BROWSE_NODE: 'BrowseNode',
    BRAND: 'Brand',
    THEME: 'Theme',
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JvdXAtb2JqZWN0LXR5cGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJncm91cC1vYmplY3QtdHlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxNQUFNLENBQUMsSUFBTSxpQkFBaUIsR0FBRztJQUMvQixTQUFTLEVBQUUsVUFBVTtJQUNyQixXQUFXLEVBQUUsWUFBWTtJQUN6QixLQUFLLEVBQUUsT0FBTztJQUNkLEtBQUssRUFBRSxPQUFPO0NBQ2YsQ0FBQyJ9

/***/ }),

/***/ 812:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// CONCATENATED MODULE: ./components/banner/feature/initialize.tsx
var DEFAULT_PROPS = { list: [], style: {} };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBWSxDQUFDIn0=
// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// EXTERNAL MODULE: ./utils/encode.ts
var encode = __webpack_require__(7);

// EXTERNAL MODULE: ./components/ui/loading-placeholder/index.tsx + 4 modules
var loading_placeholder = __webpack_require__(750);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/banner/feature/style.tsx



/* harmony default export */ var feature_style = ({
    container: function (len) { return Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                paddingTop: 10,
                paddingBottom: 10,
                display: variable["display"].block,
                overflowX: 1 === len ? 'hidden' : 'scroll',
                overflowY: 'hidden'
            }],
        DESKTOP: [
            layout["a" /* flexContainer */].justify,
            {
                marginBottom: 50,
                display: variable["display"].flex,
            }
        ],
        GENERAL: [{ width: '100%' }]
    }); },
    panel: function (len) { return Object(responsive["a" /* combineStyle */])({
        MOBILE: [
            layout["a" /* flexContainer */].justify,
            {
                width: 1 === len ? 'calc(200% - 21px)' : '180%',
                maxWidth: 940,
                paddingLeft: 10,
                display: variable["display"].block,
            }
        ],
        DESKTOP: [{
                width: '100%',
                paddingLeft: 0,
                display: variable["display"].flex,
                justifyContent: 'space-between'
            }],
        GENERAL: [{ whiteSpace: 'nowrap' }]
    }); },
    link: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                display: variable["display"].inlineBlock,
                verticalAlign: 'top',
                maxWidth: 'calc(50% - 5px)',
                width: 'calc(50% - 5px)',
                marginRight: 10,
                paddingRight: 10,
                boxShadow: variable["shadowBlur"]
            }],
        DESKTOP: [{
                display: variable["display"].block,
                maxWidth: 'calc(50% - 10px)',
                width: 'calc(50% - 10px)',
                marginRight: 0,
                paddingRight: 0,
            }],
        GENERAL: [{
                whiteSpace: 'nowrap',
                flex: 1,
                cursor: 'pointer',
                borderRadius: 5,
                paddingTop: '21%',
                transition: variable["transitionNormal"],
                overflow: 'hidden',
                backgroundSize: 'cover',
                backgroundPosition: 'center',
            }]
    }),
    placeholder: {
        width: '100%',
        productList: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    marginLeft: 5,
                    marginRight: 5
                }],
            DESKTOP: [{
                    marginLeft: -10,
                    marginRight: -10,
                    marginBottom: 40
                }],
            GENERAL: [{
                    flexWrap: 'wrap',
                    display: variable["display"].flex
                }]
        }),
        productMobileItem: {
            minWidth: '50%',
            width: '50%',
        },
        productItem: {
            container: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        paddingLeft: 5,
                        paddingRight: 5,
                        marginBottom: 10,
                        height: 119
                    }],
                DESKTOP: [{
                        paddingLeft: 10,
                        paddingRight: 10,
                        marginBottom: 10,
                        height: 237
                    }],
                GENERAL: [{
                        flex: 1,
                        minWidth: '20%',
                        width: '20%',
                    }]
            }),
            image: {
                width: '100%',
                height: '100%'
            },
            text: {
                width: '94%',
                height: 25,
                marginBottom: 10,
            },
            lastText: {
                width: '65%',
                height: 25,
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ3pELE9BQU8sS0FBSyxNQUFNLE1BQU0sdUJBQXVCLENBQUM7QUFDaEQsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxlQUFlO0lBQ2IsU0FBUyxFQUFFLFVBQUMsR0FBRyxJQUFLLE9BQUEsWUFBWSxDQUFDO1FBQy9CLE1BQU0sRUFBRSxDQUFDO2dCQUNQLFVBQVUsRUFBRSxFQUFFO2dCQUNkLGFBQWEsRUFBRSxFQUFFO2dCQUNqQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO2dCQUMvQixTQUFTLEVBQUUsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxRQUFRO2dCQUMxQyxTQUFTLEVBQUUsUUFBUTthQUNwQixDQUFDO1FBRUYsT0FBTyxFQUFFO1lBQ1AsTUFBTSxDQUFDLGFBQWEsQ0FBQyxPQUFPO1lBQzVCO2dCQUNFLFlBQVksRUFBRSxFQUFFO2dCQUNoQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2FBQy9CO1NBQ0Y7UUFFRCxPQUFPLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQztLQUM3QixDQUFDLEVBbEJrQixDQWtCbEI7SUFFRixLQUFLLEVBQUUsVUFBQyxHQUFHLElBQUssT0FBQSxZQUFZLENBQUM7UUFDM0IsTUFBTSxFQUFFO1lBQ04sTUFBTSxDQUFDLGFBQWEsQ0FBQyxPQUFPO1lBQzVCO2dCQUNFLEtBQUssRUFBRSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsTUFBTTtnQkFDL0MsUUFBUSxFQUFFLEdBQUc7Z0JBQ2IsV0FBVyxFQUFFLEVBQUU7Z0JBQ2YsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSzthQUNoQztTQUNGO1FBRUQsT0FBTyxFQUFFLENBQUM7Z0JBQ1IsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsV0FBVyxFQUFFLENBQUM7Z0JBQ2QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsY0FBYyxFQUFFLGVBQWU7YUFDaEMsQ0FBQztRQUVGLE9BQU8sRUFBRSxDQUFDLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxDQUFDO0tBQ3BDLENBQUMsRUFuQmMsQ0FtQmQ7SUFFRixJQUFJLEVBQUUsWUFBWSxDQUFDO1FBQ2pCLE1BQU0sRUFBRSxDQUFDO2dCQUNQLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLFdBQVc7Z0JBQ3JDLGFBQWEsRUFBRSxLQUFLO2dCQUNwQixRQUFRLEVBQUUsaUJBQWlCO2dCQUMzQixLQUFLLEVBQUUsaUJBQWlCO2dCQUN4QixXQUFXLEVBQUUsRUFBRTtnQkFDZixZQUFZLEVBQUUsRUFBRTtnQkFDaEIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2FBQy9CLENBQUM7UUFFRixPQUFPLEVBQUUsQ0FBQztnQkFDUixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO2dCQUMvQixRQUFRLEVBQUUsa0JBQWtCO2dCQUM1QixLQUFLLEVBQUUsa0JBQWtCO2dCQUN6QixXQUFXLEVBQUUsQ0FBQztnQkFDZCxZQUFZLEVBQUUsQ0FBQzthQUNoQixDQUFDO1FBRUYsT0FBTyxFQUFFLENBQUM7Z0JBQ1IsVUFBVSxFQUFFLFFBQVE7Z0JBQ3BCLElBQUksRUFBRSxDQUFDO2dCQUNQLE1BQU0sRUFBRSxTQUFTO2dCQUNqQixZQUFZLEVBQUUsQ0FBQztnQkFDZixVQUFVLEVBQUUsS0FBSztnQkFDakIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLFFBQVEsRUFBRSxRQUFRO2dCQUNsQixjQUFjLEVBQUUsT0FBTztnQkFDdkIsa0JBQWtCLEVBQUUsUUFBUTthQUM3QixDQUFDO0tBQ0gsQ0FBQztJQUVGLFdBQVcsRUFBRTtRQUNYLEtBQUssRUFBRSxNQUFNO1FBRWIsV0FBVyxFQUFFLFlBQVksQ0FBQztZQUN4QixNQUFNLEVBQUUsQ0FBQztvQkFDUCxVQUFVLEVBQUUsQ0FBQztvQkFDYixXQUFXLEVBQUUsQ0FBQztpQkFDZixDQUFDO1lBRUYsT0FBTyxFQUFFLENBQUM7b0JBQ1IsVUFBVSxFQUFFLENBQUMsRUFBRTtvQkFDZixXQUFXLEVBQUUsQ0FBQyxFQUFFO29CQUNoQixZQUFZLEVBQUUsRUFBRTtpQkFDakIsQ0FBQztZQUVGLE9BQU8sRUFBRSxDQUFDO29CQUNSLFFBQVEsRUFBRSxNQUFNO29CQUNoQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2lCQUMvQixDQUFDO1NBQ0gsQ0FBQztRQUVGLGlCQUFpQixFQUFFO1lBQ2pCLFFBQVEsRUFBRSxLQUFLO1lBQ2YsS0FBSyxFQUFFLEtBQUs7U0FDYjtRQUVELFdBQVcsRUFBRTtZQUNYLFNBQVMsRUFBRSxZQUFZLENBQUM7Z0JBQ3RCLE1BQU0sRUFBRSxDQUFDO3dCQUNQLFdBQVcsRUFBRSxDQUFDO3dCQUNkLFlBQVksRUFBRSxDQUFDO3dCQUNmLFlBQVksRUFBRSxFQUFFO3dCQUNoQixNQUFNLEVBQUUsR0FBRztxQkFDWixDQUFDO2dCQUVGLE9BQU8sRUFBRSxDQUFDO3dCQUNSLFdBQVcsRUFBRSxFQUFFO3dCQUNmLFlBQVksRUFBRSxFQUFFO3dCQUNoQixZQUFZLEVBQUUsRUFBRTt3QkFDaEIsTUFBTSxFQUFFLEdBQUc7cUJBQ1osQ0FBQztnQkFFRixPQUFPLEVBQUUsQ0FBQzt3QkFDUixJQUFJLEVBQUUsQ0FBQzt3QkFDUCxRQUFRLEVBQUUsS0FBSzt3QkFDZixLQUFLLEVBQUUsS0FBSztxQkFDYixDQUFDO2FBQ0gsQ0FBQztZQUVGLEtBQUssRUFBRTtnQkFDTCxLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsTUFBTTthQUNmO1lBRUQsSUFBSSxFQUFFO2dCQUNKLEtBQUssRUFBRSxLQUFLO2dCQUNaLE1BQU0sRUFBRSxFQUFFO2dCQUNWLFlBQVksRUFBRSxFQUFFO2FBQ2pCO1lBRUQsUUFBUSxFQUFFO2dCQUNSLEtBQUssRUFBRSxLQUFLO2dCQUNaLE1BQU0sRUFBRSxFQUFFO2FBQ1g7U0FDRjtLQUNGO0NBQ0YsQ0FBQyJ9
// CONCATENATED MODULE: ./components/banner/feature/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};






var renderNavLink = function (style, link) { return react["createElement"](react_router_dom["NavLink"], { to: link, style: style }); };
var renderItemPlaceholder = function (item) { return (react["createElement"]("div", { style: [
        feature_style.placeholder.productItem.container,
        'MOBILE' === window.DEVICE_VERSION && feature_style.placeholder.productMobileItem
    ], key: item },
    react["createElement"](loading_placeholder["a" /* default */], { style: feature_style.placeholder.productItem.image }))); };
var renderLoadingPlaceholder = function () {
    var list = 'MOBILE' === window.DEVICE_VERSION ? [1] : [1, 2];
    return (react["createElement"]("div", { style: feature_style.placeholder },
        react["createElement"]("div", { style: feature_style.placeholder.productList }, list.map(renderItemPlaceholder))));
};
function renderItem(item, $index) {
    var itemProps = generateLinkProps(item);
    if (!!item.links && 2 === item.links.length) {
        return !!window.isInsightsBot && $index >= 2 ? null : (react["createElement"]("div", { key: "item-banner-" + $index, style: Object.assign({}, feature_style.link, { backgroundImage: "url(" + Object(encode["h" /* decodeEntities */])(item && item.cover_image && item.cover_image.medium_url || '') + ")" }, { position: 'relative' }) },
            react["createElement"]("div", { style: { display: 'flex', position: 'absolute', top: 0, left: 0, width: '100%', height: '100%' } },
                renderNavLink({ display: 'block', height: '100%', flex: 6 }, Object(validate["f" /* getNavLink */])(item.link)),
                renderNavLink({ display: 'block', height: '100%', flex: 5 }, Object(validate["f" /* getNavLink */])(item.links[0])),
                renderNavLink({ display: 'block', height: '100%', flex: 4 }, Object(validate["f" /* getNavLink */])(item.links[1])))));
    }
    return react["createElement"](react_router_dom["NavLink"], __assign({}, itemProps));
}
;
var generateLinkProps = function (item) { return ({
    style: Object.assign({}, feature_style.link, { backgroundImage: "url(" + Object(encode["h" /* decodeEntities */])(item && item.cover_image && item.cover_image.medium_url || '') + ")" }),
    to: Object(validate["f" /* getNavLink */])(item && item.link || ''),
    key: "banner-main-home-" + (item && item.id || 0),
}); };
var renderView = function (_a) {
    var list = _a.list, style = _a.style;
    return ((!list || 0 === list.length)
        ? renderLoadingPlaceholder()
        : (react["createElement"]("div", { style: Object.assign({}, feature_style.container(list.length), style) },
            react["createElement"]("div", { style: feature_style.panel(list.length) }, Array.isArray(list) && list.map(renderItem)))));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDdkQsT0FBTyxrQkFBa0IsTUFBTSw0Q0FBNEMsQ0FBQztBQUU1RSxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsSUFBTSxhQUFhLEdBQUcsVUFBQyxLQUFLLEVBQUUsSUFBSSxJQUFLLE9BQUEsb0JBQUMsT0FBTyxJQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLEtBQUssR0FBSSxFQUFuQyxDQUFtQyxDQUFDO0FBRTNFLE1BQU0sQ0FBQyxJQUFNLHFCQUFxQixHQUFHLFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FDN0MsNkJBQ0UsS0FBSyxFQUFFO1FBQ0wsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsU0FBUztRQUN2QyxRQUFRLEtBQUssTUFBTSxDQUFDLGNBQWMsSUFBSSxLQUFLLENBQUMsV0FBVyxDQUFDLGlCQUFpQjtLQUMxRSxFQUNELEdBQUcsRUFBRSxJQUFJO0lBQ1Qsb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBSSxDQUM5RCxDQUNQLEVBVDhDLENBUzlDLENBQUM7QUFFRixJQUFNLHdCQUF3QixHQUFHO0lBQy9CLElBQU0sSUFBSSxHQUFHLFFBQVEsS0FBSyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUUvRCxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVc7UUFDM0IsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxJQUN0QyxJQUFJLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQzVCLENBQ0YsQ0FDUCxDQUFDO0FBQ0osQ0FBQyxDQUFBO0FBRUQsb0JBQW9CLElBQUksRUFBRSxNQUFNO0lBQzlCLElBQU0sU0FBUyxHQUFHLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO0lBRTFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDNUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsYUFBYSxJQUFJLE1BQU0sSUFBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FDbkQsNkJBQ0UsR0FBRyxFQUFFLGlCQUFlLE1BQVEsRUFDNUIsS0FBSyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUNyQixLQUFLLENBQUMsSUFBSSxFQUNWLEVBQUUsZUFBZSxFQUFFLFNBQU8sY0FBYyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxJQUFJLEVBQUUsQ0FBQyxNQUFHLEVBQUUsRUFDNUcsRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLENBQ3pCO1lBQ0QsNkJBQUssS0FBSyxFQUFFLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUU7Z0JBQ2xHLGFBQWEsQ0FBQyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLEVBQUUsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbkYsYUFBYSxDQUFDLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN2RixhQUFhLENBQUMsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FDcEYsQ0FDRixDQUNQLENBQUE7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLG9CQUFDLE9BQU8sZUFBSyxTQUFTLEVBQUksQ0FBQztBQUNwQyxDQUFDO0FBQUEsQ0FBQztBQUVGLElBQU0saUJBQWlCLEdBQUcsVUFBQyxJQUFJLElBQUssT0FBQSxDQUFDO0lBQ25DLEtBQUssRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFDckIsS0FBSyxDQUFDLElBQUksRUFDVixFQUFFLGVBQWUsRUFBRSxTQUFPLGNBQWMsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsSUFBSSxFQUFFLENBQUMsTUFBRyxFQUFFLENBQzdHO0lBQ0QsRUFBRSxFQUFFLFVBQVUsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxFQUFFLENBQUM7SUFDdkMsR0FBRyxFQUFFLHVCQUFvQixJQUFJLElBQUksSUFBSSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUU7Q0FDaEQsQ0FBQyxFQVBrQyxDQU9sQyxDQUFDO0FBRUgsSUFBTSxVQUFVLEdBQUcsVUFBQyxFQUFlO1FBQWIsY0FBSSxFQUFFLGdCQUFLO0lBQU8sT0FBQSxDQUN0QyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQzFCLENBQUMsQ0FBQyx3QkFBd0IsRUFBRTtRQUM1QixDQUFDLENBQUMsQ0FDQSw2QkFBSyxLQUFLLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsS0FBSyxDQUFDO1lBQ2hFLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFDakMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUN4QyxDQUNGLENBQ1AsQ0FDSjtBQVZ1QyxDQVV2QyxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// CONCATENATED MODULE: ./components/banner/feature/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_BannerFeature = /** @class */ (function (_super) {
    __extends(BannerFeature, _super);
    function BannerFeature() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    BannerFeature.prototype.shouldComponentUpdate = function (nextProps) {
        var listLen = this.props.list && this.props.list.length || 0;
        var nextListLen = nextProps.list && nextProps.list.length || 0;
        if (listLen !== nextListLen) {
            return true;
        }
        return false;
    };
    BannerFeature.prototype.render = function () {
        var _a = this.props, list = _a.list, style = _a.style;
        return view({ list: list, style: style });
    };
    BannerFeature.defaultProps = DEFAULT_PROPS;
    BannerFeature = __decorate([
        radium
    ], BannerFeature);
    return BannerFeature;
}(react["Component"]));
;
/* harmony default export */ var component = (component_BannerFeature);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUNsQyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUdqQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQzdDLE9BQU8sVUFBVSxNQUFNLFFBQVEsQ0FBQztBQUdoQztJQUE0QixpQ0FBeUI7SUFBckQ7O0lBZUEsQ0FBQztJQVpDLDZDQUFxQixHQUFyQixVQUFzQixTQUFpQjtRQUNyQyxJQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDO1FBQy9ELElBQU0sV0FBVyxHQUFHLFNBQVMsQ0FBQyxJQUFJLElBQUksU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDO1FBQ2pFLEVBQUUsQ0FBQyxDQUFDLE9BQU8sS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUFDLENBQUM7UUFFN0MsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFRCw4QkFBTSxHQUFOO1FBQ1EsSUFBQSxlQUFzQyxFQUFwQyxjQUFJLEVBQUUsZ0JBQUssQ0FBMEI7UUFDN0MsTUFBTSxDQUFDLFVBQVUsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLEtBQUssT0FBQSxFQUFFLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBYk0sMEJBQVksR0FBVyxhQUFhLENBQUM7SUFEeEMsYUFBYTtRQURsQixNQUFNO09BQ0QsYUFBYSxDQWVsQjtJQUFELG9CQUFDO0NBQUEsQUFmRCxDQUE0QixTQUFTLEdBZXBDO0FBQUEsQ0FBQztBQUVGLGVBQWUsYUFBYSxDQUFDIn0=
// CONCATENATED MODULE: ./components/banner/feature/index.tsx

/* harmony default export */ var feature = __webpack_exports__["a"] = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxhQUFhLE1BQU0sYUFBYSxDQUFDO0FBQ3hDLGVBQWUsYUFBYSxDQUFDIn0=

/***/ }),

/***/ 828:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// CONCATENATED MODULE: ./components/testimonial/initialize.tsx
var DEFAULT_PROPS = {
    list: [],
    isHome: false,
    isSlide: false,
    showHeader: true
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLE1BQU0sRUFBRSxLQUFLO0lBQ2IsT0FBTyxFQUFFLEtBQUs7SUFDZCxVQUFVLEVBQUUsSUFBSTtDQUNQLENBQUMifQ==
// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./container/layout/fade-in/index.tsx
var fade_in = __webpack_require__(756);

// EXTERNAL MODULE: ./container/layout/main-block/index.tsx
var main_block = __webpack_require__(746);

// EXTERNAL MODULE: ./components/general/pagination/index.tsx + 4 modules
var pagination = __webpack_require__(751);

// EXTERNAL MODULE: ./components/item/feedback-testimonial/index.tsx + 2 modules
var feedback_testimonial = __webpack_require__(803);

// EXTERNAL MODULE: ./components/ui/loading-placeholder/index.tsx + 4 modules
var loading_placeholder = __webpack_require__(750);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./style/media-queries.ts
var media_queries = __webpack_require__(76);

// CONCATENATED MODULE: ./components/testimonial/style.tsx




/* harmony default export */ var testimonial_style = ({
    display: 'block',
    position: 'relative',
    zIndex: variable["zIndex5"],
    paddingTop: 20,
    groupProductVideomagazine: [
        layout["b" /* splitContainer */],
        layout["a" /* flexContainer */].justify, {
            marginBottom: 20,
        }
    ],
    feedContainer: {
        paddingTop: '30%',
        position: variable["position"].relative,
        panel: {
            position: variable["position"].absolute,
            top: 0,
            left: 0,
            height: '100%',
            width: '100%',
            overflowX: 'hidden',
            overflowY: 'auto',
            feed: {
                paddingTop: 1,
                paddingRight: 1,
                paddingBottom: 1,
                paddingLeft: 1,
            }
        }
    },
    mainBanner: {
        marginBottom: 20
    },
    list: function (isSlide) {
        if (isSlide === void 0) { isSlide = false; }
        var general = {
            display: variable["display"].flex,
            flexWrap: isSlide ? 'nowrap' : 'wrap',
            paddingTop: 20,
            paddingRight: 0,
            paddingBottom: 10,
            paddingLeft: 0,
            position: variable["position"].relative,
            overflowX: isSlide ? 'auto' : ''
        };
        var mobile =  true ? {
            paddingTop: 0,
            paddingRight: isSlide ? 20 : 0,
            paddingBottom: 0,
            paddingLeft: 0,
            marginRight: isSlide ? 5 : -10,
            marginLeft: -10
        } : undefined;
        return Object.assign({}, general, mobile);
    },
    item: function (isSlide) {
        if (isSlide === void 0) { isSlide = false; }
        return Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    marginBottom: 10,
                    paddingLeft: 5,
                    paddingRight: 5,
                    width: isSlide ? '47%' : '50%',
                    minWidth: isSlide ? '47%' : '',
                }],
            DESKTOP: [{
                    marginBottom: 20,
                    paddingLeft: 10,
                    paddingRight: 10,
                    width: '20%',
                }],
            GENERAL: [{ paddingTop: 0, paddingBottom: 0 }]
        });
    },
    videoWrap: {
        height: '100%'
    },
    customStyleLoading: {
        height: 300
    },
    placeholder: {
        width: '100%',
        title: {
            background: variable["colorF0"],
            display: variable["display"].block,
            width: 320,
            height: 56,
            margin: '0 auto 10px'
        },
        titleMobile: {
            margin: 0,
            textAlign: 'left'
        },
        productList: {
            flexWrap: 'wrap',
            display: variable["display"].flex,
            marginLeft: -10,
            marginRight: -10
        },
        productMobileItem: {
            minWidth: '50%',
            width: '50%',
        },
        productItem: {
            flex: 1,
            paddingLeft: 10,
            paddingRight: 10,
            marginBottom: 20,
            minWidth: '20%',
            width: '20%',
            height: 303,
            image: {
                width: '100%',
                height: '100%'
            },
            text: {
                width: '94%',
                height: 25,
                marginBottom: 10,
            },
            lastText: {
                width: '65%',
                height: 25,
            }
        },
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3RELE9BQU8sS0FBSyxNQUFNLE1BQU0sb0JBQW9CLENBQUM7QUFDN0MsT0FBTyxLQUFLLFFBQVEsTUFBTSxzQkFBc0IsQ0FBQztBQUNqRCxPQUFPLGFBQWEsTUFBTSwyQkFBMkIsQ0FBQztBQUV0RCxlQUFlO0lBQ2IsT0FBTyxFQUFFLE9BQU87SUFDaEIsUUFBUSxFQUFFLFVBQVU7SUFDcEIsTUFBTSxFQUFFLFFBQVEsQ0FBQyxPQUFPO0lBQ3hCLFVBQVUsRUFBRSxFQUFFO0lBRWQseUJBQXlCLEVBQUU7UUFDekIsTUFBTSxDQUFDLGNBQWM7UUFDckIsTUFBTSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUU7WUFDNUIsWUFBWSxFQUFFLEVBQUU7U0FDakI7S0FDRjtJQUVELGFBQWEsRUFBRTtRQUNiLFVBQVUsRUFBRSxLQUFLO1FBQ2pCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7UUFFcEMsS0FBSyxFQUFFO1lBQ0wsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxHQUFHLEVBQUUsQ0FBQztZQUNOLElBQUksRUFBRSxDQUFDO1lBQ1AsTUFBTSxFQUFFLE1BQU07WUFDZCxLQUFLLEVBQUUsTUFBTTtZQUNiLFNBQVMsRUFBRSxRQUFRO1lBQ25CLFNBQVMsRUFBRSxNQUFNO1lBRWpCLElBQUksRUFBRTtnQkFDSixVQUFVLEVBQUUsQ0FBQztnQkFDYixZQUFZLEVBQUUsQ0FBQztnQkFDZixhQUFhLEVBQUUsQ0FBQztnQkFDaEIsV0FBVyxFQUFFLENBQUM7YUFDZjtTQUNGO0tBQ0Y7SUFFRCxVQUFVLEVBQUU7UUFDVixZQUFZLEVBQUUsRUFBRTtLQUNqQjtJQUVELElBQUksRUFBRSxVQUFDLE9BQWU7UUFBZix3QkFBQSxFQUFBLGVBQWU7UUFDcEIsSUFBTSxPQUFPLEdBQUc7WUFDZCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsTUFBTTtZQUNyQyxVQUFVLEVBQUUsRUFBRTtZQUNkLFlBQVksRUFBRSxDQUFDO1lBQ2YsYUFBYSxFQUFFLEVBQUU7WUFDakIsV0FBVyxFQUFFLENBQUM7WUFDZCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1lBQ3BDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRTtTQUNqQyxDQUFBO1FBRUQsSUFBTSxNQUFNLEdBQUcsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pDLFVBQVUsRUFBRSxDQUFDO1lBQ2IsWUFBWSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlCLGFBQWEsRUFBRSxDQUFDO1lBQ2hCLFdBQVcsRUFBRSxDQUFDO1lBQ2QsV0FBVyxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDOUIsVUFBVSxFQUFFLENBQUMsRUFBRTtTQUNoQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUE7UUFFTixNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFRCxJQUFJLEVBQUUsVUFBQyxPQUFlO1FBQWYsd0JBQUEsRUFBQSxlQUFlO1FBQUssT0FBQSxZQUFZLENBQUM7WUFDdEMsTUFBTSxFQUFFLENBQUM7b0JBQ1AsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLFdBQVcsRUFBRSxDQUFDO29CQUNkLFlBQVksRUFBRSxDQUFDO29CQUNmLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSztvQkFDOUIsUUFBUSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFO2lCQUMvQixDQUFDO1lBRUYsT0FBTyxFQUFFLENBQUM7b0JBQ1IsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLFdBQVcsRUFBRSxFQUFFO29CQUNmLFlBQVksRUFBRSxFQUFFO29CQUNoQixLQUFLLEVBQUUsS0FBSztpQkFDYixDQUFDO1lBRUYsT0FBTyxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsQ0FBQyxFQUFFLGFBQWEsRUFBRSxDQUFDLEVBQUUsQ0FBQztTQUMvQyxDQUFDO0lBakJ5QixDQWlCekI7SUFFRixTQUFTLEVBQUU7UUFDVCxNQUFNLEVBQUUsTUFBTTtLQUNmO0lBRUQsa0JBQWtCLEVBQUU7UUFDbEIsTUFBTSxFQUFFLEdBQUc7S0FDWjtJQUVELFdBQVcsRUFBRTtRQUNYLEtBQUssRUFBRSxNQUFNO1FBRWIsS0FBSyxFQUFFO1lBQ0wsVUFBVSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1lBQzVCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDL0IsS0FBSyxFQUFFLEdBQUc7WUFDVixNQUFNLEVBQUUsRUFBRTtZQUNWLE1BQU0sRUFBRSxhQUFhO1NBQ3RCO1FBRUQsV0FBVyxFQUFFO1lBQ1gsTUFBTSxFQUFFLENBQUM7WUFDVCxTQUFTLEVBQUUsTUFBTTtTQUNsQjtRQUVELFdBQVcsRUFBRTtZQUNYLFFBQVEsRUFBRSxNQUFNO1lBQ2hCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsVUFBVSxFQUFFLENBQUMsRUFBRTtZQUNmLFdBQVcsRUFBRSxDQUFDLEVBQUU7U0FDakI7UUFFRCxpQkFBaUIsRUFBRTtZQUNqQixRQUFRLEVBQUUsS0FBSztZQUNmLEtBQUssRUFBRSxLQUFLO1NBQ2I7UUFFRCxXQUFXLEVBQUU7WUFDWCxJQUFJLEVBQUUsQ0FBQztZQUNQLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLEVBQUU7WUFDaEIsWUFBWSxFQUFFLEVBQUU7WUFDaEIsUUFBUSxFQUFFLEtBQUs7WUFDZixLQUFLLEVBQUUsS0FBSztZQUNaLE1BQU0sRUFBRSxHQUFHO1lBRVgsS0FBSyxFQUFFO2dCQUNMLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxNQUFNO2FBQ2Y7WUFFRCxJQUFJLEVBQUU7Z0JBQ0osS0FBSyxFQUFFLEtBQUs7Z0JBQ1osTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsWUFBWSxFQUFFLEVBQUU7YUFDakI7WUFFRCxRQUFRLEVBQUU7Z0JBQ1IsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osTUFBTSxFQUFFLEVBQUU7YUFDWDtTQUNGO0tBQ0Y7Q0FDSyxDQUFDIn0=
// CONCATENATED MODULE: ./components/testimonial/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};








var renderItemPlaceholder = function (item) { return (react["createElement"]("div", { style: [
        testimonial_style.placeholder.productItem,
        'MOBILE' === window.DEVICE_VERSION && testimonial_style.placeholder.productMobileItem
    ], key: item },
    react["createElement"](loading_placeholder["a" /* default */], { style: testimonial_style.placeholder.productItem.image }))); };
var renderLoadingPlaceholder = function (isHome) {
    if (isHome === void 0) { isHome = false; }
    var list = isHome || 'MOBILE' === window.DEVICE_VERSION ? [1, 2, 3, 4, 5] : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
    return (react["createElement"]("div", { style: testimonial_style.placeholder },
        react["createElement"](loading_placeholder["a" /* default */], { style: [testimonial_style.placeholder.title, 'MOBILE' === window.DEVICE_VERSION && testimonial_style.placeholder.titleMobile] }),
        react["createElement"]("div", { style: testimonial_style.placeholder.productList }, Array.isArray(list) && list.map(renderItemPlaceholder))));
};
var renderItems = function (_a) {
    var list = _a.list, openModal = _a.openModal, style = _a.style;
    return Array.isArray(list)
        && list.map(function (feedback) {
            return react["createElement"]("div", { key: feedback.id, style: style },
                react["createElement"](feedback_testimonial["a" /* default */], { data: feedback, openModal: openModal }));
        });
};
var renderView = function (props) {
    var per = props.per, list = props.list, total = props.total, isHome = props.isHome, current = props.current, isSlide = props.isSlide, urlList = props.urlList, openModal = props.openModal, showHeader = props.showHeader;
    var paginationProps = {
        per: per,
        total: total,
        current: current,
        urlList: urlList
    };
    var mainBlockProps = {
        showHeader: showHeader,
        title: showHeader ? 'Nhận xét về Lixibox' : '',
        viewMoreText: 'Xem thêm',
        viewMoreLink: routing["q" /* ROUTING_COMMUNITY_PATH */],
        showViewMore: isHome,
        content: 0 === list.length
            ? null
            : isHome
                ? (react["createElement"]("div", { style: testimonial_style.list(isSlide) }, renderItems({ list: list.slice(0, 5), style: testimonial_style.item(isSlide), openModal: openModal })))
                : (react["createElement"]("div", null,
                    react["createElement"](fade_in["a" /* default */], { itemStyle: testimonial_style.item(isSlide), style: testimonial_style.list(isSlide) }, renderItems({ list: list, style: testimonial_style.videoWrap, openModal: openModal })),
                    react["createElement"](pagination["a" /* default */], __assign({}, paginationProps)))),
        style: {}
    };
    return (react["createElement"]("summary-testimonial-list", { style: testimonial_style }, Array.isArray(list) && 0 === list.length ? renderLoadingPlaceholder(isHome) : react["createElement"](main_block["a" /* default */], __assign({}, mainBlockProps))));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFFN0UsT0FBTyxNQUFNLE1BQU0sZ0NBQWdDLENBQUM7QUFDcEQsT0FBTyxTQUFTLE1BQU0sbUNBQW1DLENBQUM7QUFDMUQsT0FBTyxVQUFVLE1BQU0sdUJBQXVCLENBQUM7QUFDL0MsT0FBTyxlQUFlLE1BQU0sOEJBQThCLENBQUM7QUFDM0QsT0FBTyxrQkFBa0IsTUFBTSwyQkFBMkIsQ0FBQztBQUczRCxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQUcsVUFBQyxJQUFJLElBQUssT0FBQSxDQUM3Qyw2QkFDRSxLQUFLLEVBQUU7UUFDTCxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVc7UUFDN0IsUUFBUSxLQUFLLE1BQU0sQ0FBQyxjQUFjLElBQUksS0FBSyxDQUFDLFdBQVcsQ0FBQyxpQkFBaUI7S0FDMUUsRUFDRCxHQUFHLEVBQUUsSUFBSTtJQUNULG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEdBQUksQ0FDOUQsQ0FDUCxFQVQ4QyxDQVM5QyxDQUFDO0FBR0YsSUFBTSx3QkFBd0IsR0FBRyxVQUFDLE1BQWM7SUFBZCx1QkFBQSxFQUFBLGNBQWM7SUFDOUMsSUFBTSxJQUFJLEdBQUcsTUFBTSxJQUFJLFFBQVEsS0FBSyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ2xJLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVztRQUMzQixvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxRQUFRLEtBQUssTUFBTSxDQUFDLGNBQWMsSUFBSSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxHQUFJO1FBQzdILDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsSUFDdEMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQ25ELENBQ0YsQ0FDUCxDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsSUFBTSxXQUFXLEdBQUcsVUFBQyxFQUEwQjtRQUF4QixjQUFJLEVBQUUsd0JBQVMsRUFBRSxnQkFBSztJQUFPLE9BQUEsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7V0FDbEUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFDLFFBQVE7WUFDbkIsT0FBQSw2QkFDRSxHQUFHLEVBQUUsUUFBUSxDQUFDLEVBQUUsRUFDaEIsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osb0JBQUMsZUFBZSxJQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLFNBQVMsR0FBSSxDQUNyRDtRQUpOLENBSU0sQ0FDUDtBQVBpRCxDQU9qRCxDQUFDO0FBRUosSUFBTSxVQUFVLEdBQUcsVUFBQyxLQUFhO0lBRTdCLElBQUEsZUFBRyxFQUNILGlCQUFJLEVBQ0osbUJBQUssRUFDTCxxQkFBTSxFQUNOLHVCQUFPLEVBQ1AsdUJBQU8sRUFDUCx1QkFBTyxFQUNQLDJCQUFTLEVBQ1QsNkJBQVUsQ0FDRjtJQUVWLElBQU0sZUFBZSxHQUFHO1FBQ3RCLEdBQUcsS0FBQTtRQUNILEtBQUssT0FBQTtRQUNMLE9BQU8sU0FBQTtRQUNQLE9BQU8sU0FBQTtLQUNSLENBQUM7SUFFRixJQUFNLGNBQWMsR0FBRztRQUNyQixVQUFVLFlBQUE7UUFDVixLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsRUFBRTtRQUM5QyxZQUFZLEVBQUUsVUFBVTtRQUN4QixZQUFZLEVBQUUsc0JBQXNCO1FBQ3BDLFlBQVksRUFBRSxNQUFNO1FBQ3BCLE9BQU8sRUFDTCxDQUFDLEtBQUssSUFBSSxDQUFDLE1BQU07WUFDZixDQUFDLENBQUMsSUFBSTtZQUNOLENBQUMsQ0FBQyxNQUFNO2dCQUNOLENBQUMsQ0FBQyxDQUNBLDZCQUNFLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUN6QixXQUFXLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsU0FBUyxXQUFBLEVBQUUsQ0FBQyxDQUMxRSxDQUNSO2dCQUNELENBQUMsQ0FBQyxDQUNBO29CQUNFLG9CQUFDLE1BQU0sSUFDTCxTQUFTLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFDOUIsS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQ3pCLFdBQVcsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxFQUFFLFNBQVMsV0FBQSxFQUFFLENBQUMsQ0FDbEQ7b0JBQ1Qsb0JBQUMsVUFBVSxlQUFLLGVBQWUsRUFBSSxDQUMvQixDQUNQO1FBQ1AsS0FBSyxFQUFFLEVBQUU7S0FDVixDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsa0RBQTBCLEtBQUssRUFBRSxLQUFLLElBQ25DLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLHdCQUF3QixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxvQkFBQyxTQUFTLGVBQUssY0FBYyxFQUFJLENBQ3ZGLENBQzVCLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./components/testimonial/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_TestimonialComponent = /** @class */ (function (_super) {
    __extends(TestimonialComponent, _super);
    function TestimonialComponent(props) {
        return _super.call(this, props) || this;
    }
    TestimonialComponent.prototype.shouldComponentUpdate = function (nextProps) {
        if (this.props.list.length !== nextProps.list.length) {
            return true;
        }
        return false;
    };
    TestimonialComponent.prototype.render = function () {
        return view(this.props);
    };
    TestimonialComponent.defaultProps = DEFAULT_PROPS;
    TestimonialComponent = __decorate([
        radium
    ], TestimonialComponent);
    return TestimonialComponent;
}(react["Component"]));
;
/* harmony default export */ var component = (component_TestimonialComponent);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFHakMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUM3QyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFHaEM7SUFBbUMsd0NBQTRCO0lBRzdELDhCQUFZLEtBQWE7ZUFDdkIsa0JBQU0sS0FBSyxDQUFDO0lBQ2QsQ0FBQztJQUVELG9EQUFxQixHQUFyQixVQUFzQixTQUFpQjtRQUNyQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUFDLENBQUM7UUFFdEUsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFRCxxQ0FBTSxHQUFOO1FBQ0UsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQWRNLGlDQUFZLEdBQVcsYUFBYSxDQUFDO0lBRHhDLG9CQUFvQjtRQUR6QixNQUFNO09BQ0Qsb0JBQW9CLENBZ0J6QjtJQUFELDJCQUFDO0NBQUEsQUFoQkQsQ0FBbUMsS0FBSyxDQUFDLFNBQVMsR0FnQmpEO0FBQUEsQ0FBQztBQUVGLGVBQWUsb0JBQW9CLENBQUMifQ==
// CONCATENATED MODULE: ./components/testimonial/index.tsx

/* harmony default export */ var testimonial = __webpack_exports__["a"] = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxvQkFBb0IsTUFBTSxhQUFhLENBQUM7QUFDL0MsZUFBZSxvQkFBb0IsQ0FBQyJ9

/***/ }),

/***/ 919:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/format.ts
var format = __webpack_require__(116);

// EXTERNAL MODULE: ./utils/encode.ts
var encode = __webpack_require__(7);

// EXTERNAL MODULE: ./constants/application/feedable.ts
var feedable = __webpack_require__(774);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// EXTERNAL MODULE: ./constants/application/group-object-type.ts
var group_object_type = __webpack_require__(806);

// EXTERNAL MODULE: ./constants/application/magazine.ts
var magazine = __webpack_require__(170);

// EXTERNAL MODULE: ./constants/application/default.ts
var application_default = __webpack_require__(760);

// EXTERNAL MODULE: ./utils/uri.ts
var uri = __webpack_require__(347);

// EXTERNAL MODULE: ./components/product/slider/index.tsx + 6 modules
var slider = __webpack_require__(770);

// CONCATENATED MODULE: ./components/product/search-popular/initialize.tsx
var DEFAULT_PROPS = {
    list: []
};
var INITIAL_STATE = {};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtDQUNDLENBQUM7QUFFWixNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUcsRUFDbEIsQ0FBQyJ9
// EXTERNAL MODULE: ./style/component.ts
var component = __webpack_require__(744);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./components/ui/loading-placeholder/index.tsx + 4 modules
var loading_placeholder = __webpack_require__(750);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/product/search-popular/style.tsx


var generateSwitchStyle = function (mobile, desktop) {
    var switchStyle = {
        MOBILE: { paddingRight: 10, width: mobile, minWidth: mobile },
        DESKTOP: { width: desktop }
    };
    return switchStyle[window.DEVICE_VERSION];
};
/* harmony default export */ var search_popular_style = ({
    paddingTop: 20,
    marginBottom: 30,
    width: '100%',
    title: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                fontSize: 11
            }],
        DESKTOP: [{
                textAlign: 'center',
                marginBottom: 20
            }],
        GENERAL: [{
                maxWidth: '100%',
                width: '100%'
            }]
    }),
    month: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                fontSize: 12,
                padding: '0 7px',
                background: variable["colorRed"],
                borderRadius: 3,
                marginTop: -8,
                display: 'inline-block',
                height: 20,
                lineHeight: '20px',
                color: variable["colorWhite"],
                border: 'none',
                position: 'relative',
                top: -4,
            }],
        DESKTOP: [{
                fontSize: 14,
                padding: '0 10px',
                background: variable["colorRed"],
                borderRadius: 5,
                color: variable["colorWhite"],
                display: 'inline-block',
                height: 26,
                lineHeight: '26px',
                position: 'relative',
                top: -7
            }],
        GENERAL: [{}]
    }),
    container: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                justifyContent: 'space-between',
                paddingLeft: 10,
                paddingRight: 10,
                flexWrap: 'wrap',
            }],
        DESKTOP: [{
                marginLeft: -10,
                marginRight: -10
            }],
        GENERAL: [{
                display: variable["display"].flex,
                alignItems: 'center',
                width: '100%'
            }]
    }),
    column: {
        1: { width: '100%', },
        2: { width: 'calc(50% - 10px)', margin: 5 },
        3: generateSwitchStyle('200px', '33.33%'),
        4: generateSwitchStyle('200px', '25%'),
        5: generateSwitchStyle('200px', '20%'),
        6: generateSwitchStyle('50%', '16.66%'),
    },
    item: {
        container: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    width: 'calc(50% - 5px)'
                }],
            DESKTOP: [{
                    width: '20%',
                    marginLeft: 10,
                    marginRight: 10
                }],
            GENERAL: [{
                    boxShadow: variable["shadowBlur"],
                    display: variable["display"].flex,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginBottom: 10,
                    borderRadius: 3,
                    color: variable["colorBlack"],
                    paddingTop: 10,
                    paddingRight: 10,
                    paddingBottom: 10,
                    paddingLeft: 10,
                }]
        }),
        info: {
            name: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        fontSize: 13
                    }],
                DESKTOP: [{
                        fontSize: 18
                    }],
                GENERAL: [{
                        fontFamily: variable["fontAvenirDemiBold"],
                        marginBottom: 5
                    }]
            }),
            content: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        fontSize: 11
                    }],
                DESKTOP: [{
                        fontSize: 15
                    }],
                GENERAL: [{
                        color: variable["colorBlack06"]
                    }]
            }),
            count: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        fontSize: 11
                    }],
                DESKTOP: [{
                        fontSize: 15
                    }],
                GENERAL: [{
                        fontFamily: variable["fontAvenirDemiBold"],
                        color: variable["colorBlack08"]
                    }]
            }),
            image: function (url) { return ({
                backgroundImage: "url(" + url + ")",
                backgroundPosition: 'center',
                backgroundRepeat: 'no-repeat',
                backgroundSize: 'contain',
                width: '100%',
                paddingTop: '50%',
                transirtion: variable["transitionOpacity"],
                borderRadius: 3
            }); }
        }
    },
    placeholder: {
        width: '100%',
        title: {
            background: variable["colorF0"],
            display: variable["display"].block,
            width: 320,
            height: 50,
            margin: '0 auto 10px'
        },
        titleMobile: {
            marginBottom: 10,
            marginLeft: 10,
            marginRight: 10,
            textAlign: 'left',
            width: 'calc(100% - 20px)'
        },
        productList: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    marginLeft: 5,
                    marginRight: 5
                }],
            DESKTOP: [{
                    marginLeft: -10,
                    marginRight: -10
                }],
            GENERAL: [{
                    flexWrap: 'wrap',
                    display: variable["display"].flex
                }]
        }),
        productMobileItem: {
            minWidth: '50%',
            width: '50%',
        },
        productItem: {
            container: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        paddingLeft: 5,
                        paddingRight: 5,
                        marginBottom: 10
                    }],
                DESKTOP: [{
                        paddingLeft: 10,
                        paddingRight: 10,
                        marginBottom: 10
                    }],
                GENERAL: [{
                        flex: 1,
                        minWidth: '20%',
                        width: '20%',
                        height: 133
                    }]
            }),
            image: {
                width: '100%',
                height: '100%'
            },
            text: {
                width: '94%',
                height: 25,
                marginBottom: 10,
            },
            lastText: {
                width: '65%',
                height: 25,
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFekQsSUFBTSxtQkFBbUIsR0FBRyxVQUFDLE1BQU0sRUFBRSxPQUFPO0lBQzFDLElBQU0sV0FBVyxHQUFHO1FBQ2xCLE1BQU0sRUFBRSxFQUFFLFlBQVksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFO1FBQzdELE9BQU8sRUFBRSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUU7S0FDNUIsQ0FBQztJQUVGLE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0FBQzVDLENBQUMsQ0FBQztBQUVGLGVBQWU7SUFDYixVQUFVLEVBQUUsRUFBRTtJQUNkLFlBQVksRUFBRSxFQUFFO0lBQ2hCLEtBQUssRUFBRSxNQUFNO0lBRWIsS0FBSyxFQUFFLFlBQVksQ0FBQztRQUNsQixNQUFNLEVBQUUsQ0FBQztnQkFDUCxRQUFRLEVBQUUsRUFBRTthQUNiLENBQUM7UUFDRixPQUFPLEVBQUUsQ0FBQztnQkFDUixTQUFTLEVBQUUsUUFBUTtnQkFDbkIsWUFBWSxFQUFFLEVBQUU7YUFDakIsQ0FBQztRQUNGLE9BQU8sRUFBRSxDQUFDO2dCQUNSLFFBQVEsRUFBRSxNQUFNO2dCQUNoQixLQUFLLEVBQUUsTUFBTTthQUNkLENBQUM7S0FDSCxDQUFDO0lBRUYsS0FBSyxFQUFFLFlBQVksQ0FBQztRQUNsQixNQUFNLEVBQUUsQ0FBQztnQkFDUCxRQUFRLEVBQUUsRUFBRTtnQkFDWixPQUFPLEVBQUUsT0FBTztnQkFDaEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxRQUFRO2dCQUM3QixZQUFZLEVBQUUsQ0FBQztnQkFDZixTQUFTLEVBQUUsQ0FBQyxDQUFDO2dCQUNiLE9BQU8sRUFBRSxjQUFjO2dCQUN2QixNQUFNLEVBQUUsRUFBRTtnQkFDVixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMxQixNQUFNLEVBQUUsTUFBTTtnQkFDZCxRQUFRLEVBQUUsVUFBVTtnQkFDcEIsR0FBRyxFQUFFLENBQUMsQ0FBQzthQUNSLENBQUM7UUFFRixPQUFPLEVBQUUsQ0FBQztnQkFDUixRQUFRLEVBQUUsRUFBRTtnQkFDWixPQUFPLEVBQUUsUUFBUTtnQkFDakIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxRQUFRO2dCQUM3QixZQUFZLEVBQUUsQ0FBQztnQkFDZixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7Z0JBQzFCLE9BQU8sRUFBRSxjQUFjO2dCQUN2QixNQUFNLEVBQUUsRUFBRTtnQkFDVixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsUUFBUSxFQUFFLFVBQVU7Z0JBQ3BCLEdBQUcsRUFBRSxDQUFDLENBQUM7YUFDUixDQUFDO1FBRUYsT0FBTyxFQUFFLENBQUMsRUFDVCxDQUFDO0tBQ0gsQ0FBQztJQUVGLFNBQVMsRUFBRSxZQUFZLENBQUM7UUFDdEIsTUFBTSxFQUFFLENBQUM7Z0JBQ1AsY0FBYyxFQUFFLGVBQWU7Z0JBQy9CLFdBQVcsRUFBRSxFQUFFO2dCQUNmLFlBQVksRUFBRSxFQUFFO2dCQUNoQixRQUFRLEVBQUUsTUFBTTthQUNqQixDQUFDO1FBRUYsT0FBTyxFQUFFLENBQUM7Z0JBQ1IsVUFBVSxFQUFFLENBQUMsRUFBRTtnQkFDZixXQUFXLEVBQUUsQ0FBQyxFQUFFO2FBQ2pCLENBQUM7UUFFRixPQUFPLEVBQUUsQ0FBQztnQkFDUixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixVQUFVLEVBQUUsUUFBUTtnQkFDcEIsS0FBSyxFQUFFLE1BQU07YUFDZCxDQUFDO0tBQ0gsQ0FBQztJQUVGLE1BQU0sRUFBRTtRQUNOLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRSxNQUFNLEdBQUc7UUFDckIsQ0FBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUU7UUFDM0MsQ0FBQyxFQUFFLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxRQUFRLENBQUM7UUFDekMsQ0FBQyxFQUFFLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUM7UUFDdEMsQ0FBQyxFQUFFLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUM7UUFDdEMsQ0FBQyxFQUFFLG1CQUFtQixDQUFDLEtBQUssRUFBRSxRQUFRLENBQUM7S0FDeEM7SUFFRCxJQUFJLEVBQUU7UUFDSixTQUFTLEVBQUUsWUFBWSxDQUFDO1lBQ3RCLE1BQU0sRUFBRSxDQUFDO29CQUNQLEtBQUssRUFBRSxpQkFBaUI7aUJBQ3pCLENBQUM7WUFFRixPQUFPLEVBQUUsQ0FBQztvQkFDUixLQUFLLEVBQUUsS0FBSztvQkFDWixVQUFVLEVBQUUsRUFBRTtvQkFDZCxXQUFXLEVBQUUsRUFBRTtpQkFDaEIsQ0FBQztZQUVGLE9BQU8sRUFBRSxDQUFDO29CQUNSLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDOUIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtvQkFDOUIsVUFBVSxFQUFFLFFBQVE7b0JBQ3BCLGNBQWMsRUFBRSxlQUFlO29CQUMvQixZQUFZLEVBQUUsRUFBRTtvQkFDaEIsWUFBWSxFQUFFLENBQUM7b0JBQ2YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsRUFBRTtvQkFDZCxZQUFZLEVBQUUsRUFBRTtvQkFDaEIsYUFBYSxFQUFFLEVBQUU7b0JBQ2pCLFdBQVcsRUFBRSxFQUFFO2lCQUNoQixDQUFDO1NBQ0gsQ0FBQztRQUVGLElBQUksRUFBRTtZQUVKLElBQUksRUFBRSxZQUFZLENBQUM7Z0JBQ2pCLE1BQU0sRUFBRSxDQUFDO3dCQUNQLFFBQVEsRUFBRSxFQUFFO3FCQUNiLENBQUM7Z0JBQ0YsT0FBTyxFQUFFLENBQUM7d0JBQ1IsUUFBUSxFQUFFLEVBQUU7cUJBQ2IsQ0FBQztnQkFDRixPQUFPLEVBQUUsQ0FBQzt3QkFDUixVQUFVLEVBQUUsUUFBUSxDQUFDLGtCQUFrQjt3QkFDdkMsWUFBWSxFQUFFLENBQUM7cUJBQ2hCLENBQUM7YUFDSCxDQUFDO1lBRUYsT0FBTyxFQUFFLFlBQVksQ0FBQztnQkFDcEIsTUFBTSxFQUFFLENBQUM7d0JBQ1AsUUFBUSxFQUFFLEVBQUU7cUJBQ2IsQ0FBQztnQkFDRixPQUFPLEVBQUUsQ0FBQzt3QkFDUixRQUFRLEVBQUUsRUFBRTtxQkFDYixDQUFDO2dCQUNGLE9BQU8sRUFBRSxDQUFDO3dCQUNSLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTtxQkFDN0IsQ0FBQzthQUNILENBQUM7WUFFRixLQUFLLEVBQUUsWUFBWSxDQUFDO2dCQUNsQixNQUFNLEVBQUUsQ0FBQzt3QkFDUCxRQUFRLEVBQUUsRUFBRTtxQkFDYixDQUFDO2dCQUNGLE9BQU8sRUFBRSxDQUFDO3dCQUNSLFFBQVEsRUFBRSxFQUFFO3FCQUNiLENBQUM7Z0JBQ0YsT0FBTyxFQUFFLENBQUM7d0JBQ1IsVUFBVSxFQUFFLFFBQVEsQ0FBQyxrQkFBa0I7d0JBQ3ZDLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTtxQkFDN0IsQ0FBQzthQUNILENBQUM7WUFFRixLQUFLLEVBQUUsVUFBQyxHQUFHLElBQUssT0FBQSxDQUFDO2dCQUNmLGVBQWUsRUFBRSxTQUFPLEdBQUcsTUFBRztnQkFDOUIsa0JBQWtCLEVBQUUsUUFBUTtnQkFDNUIsZ0JBQWdCLEVBQUUsV0FBVztnQkFDN0IsY0FBYyxFQUFFLFNBQVM7Z0JBQ3pCLEtBQUssRUFBRSxNQUFNO2dCQUNiLFVBQVUsRUFBRSxLQUFLO2dCQUNqQixXQUFXLEVBQUUsUUFBUSxDQUFDLGlCQUFpQjtnQkFDdkMsWUFBWSxFQUFFLENBQUM7YUFDaEIsQ0FBQyxFQVRjLENBU2Q7U0FDSDtLQUNGO0lBRUQsV0FBVyxFQUFFO1FBQ1gsS0FBSyxFQUFFLE1BQU07UUFFYixLQUFLLEVBQUU7WUFDTCxVQUFVLEVBQUUsUUFBUSxDQUFDLE9BQU87WUFDNUIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztZQUMvQixLQUFLLEVBQUUsR0FBRztZQUNWLE1BQU0sRUFBRSxFQUFFO1lBQ1YsTUFBTSxFQUFFLGFBQWE7U0FDdEI7UUFFRCxXQUFXLEVBQUU7WUFDWCxZQUFZLEVBQUUsRUFBRTtZQUNoQixVQUFVLEVBQUUsRUFBRTtZQUNkLFdBQVcsRUFBRSxFQUFFO1lBQ2YsU0FBUyxFQUFFLE1BQU07WUFDakIsS0FBSyxFQUFFLG1CQUFtQjtTQUMzQjtRQUVELFdBQVcsRUFBRSxZQUFZLENBQUM7WUFDeEIsTUFBTSxFQUFFLENBQUM7b0JBQ1AsVUFBVSxFQUFFLENBQUM7b0JBQ2IsV0FBVyxFQUFFLENBQUM7aUJBQ2YsQ0FBQztZQUVGLE9BQU8sRUFBRSxDQUFDO29CQUNSLFVBQVUsRUFBRSxDQUFDLEVBQUU7b0JBQ2YsV0FBVyxFQUFFLENBQUMsRUFBRTtpQkFDakIsQ0FBQztZQUVGLE9BQU8sRUFBRSxDQUFDO29CQUNSLFFBQVEsRUFBRSxNQUFNO29CQUNoQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2lCQUMvQixDQUFDO1NBQ0gsQ0FBQztRQUVGLGlCQUFpQixFQUFFO1lBQ2pCLFFBQVEsRUFBRSxLQUFLO1lBQ2YsS0FBSyxFQUFFLEtBQUs7U0FDYjtRQUVELFdBQVcsRUFBRTtZQUNYLFNBQVMsRUFBRSxZQUFZLENBQUM7Z0JBQ3RCLE1BQU0sRUFBRSxDQUFDO3dCQUNQLFdBQVcsRUFBRSxDQUFDO3dCQUNkLFlBQVksRUFBRSxDQUFDO3dCQUNmLFlBQVksRUFBRSxFQUFFO3FCQUNqQixDQUFDO2dCQUVGLE9BQU8sRUFBRSxDQUFDO3dCQUNSLFdBQVcsRUFBRSxFQUFFO3dCQUNmLFlBQVksRUFBRSxFQUFFO3dCQUNoQixZQUFZLEVBQUUsRUFBRTtxQkFDakIsQ0FBQztnQkFFRixPQUFPLEVBQUUsQ0FBQzt3QkFDUixJQUFJLEVBQUUsQ0FBQzt3QkFDUCxRQUFRLEVBQUUsS0FBSzt3QkFDZixLQUFLLEVBQUUsS0FBSzt3QkFDWixNQUFNLEVBQUUsR0FBRztxQkFDWixDQUFDO2FBQ0gsQ0FBQztZQUVGLEtBQUssRUFBRTtnQkFDTCxLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsTUFBTTthQUNmO1lBRUQsSUFBSSxFQUFFO2dCQUNKLEtBQUssRUFBRSxLQUFLO2dCQUNaLE1BQU0sRUFBRSxFQUFFO2dCQUNWLFlBQVksRUFBRSxFQUFFO2FBQ2pCO1lBRUQsUUFBUSxFQUFFO2dCQUNSLEtBQUssRUFBRSxLQUFLO2dCQUNaLE1BQU0sRUFBRSxFQUFFO2FBQ1g7U0FDRjtLQUNGO0NBQ0ssQ0FBQyJ9
// CONCATENATED MODULE: ./components/product/search-popular/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};






var renderItemPlaceholder = function (item) { return (react["createElement"]("div", { style: [
        search_popular_style.placeholder.productItem.container,
        'MOBILE' === window.DEVICE_VERSION && search_popular_style.placeholder.productMobileItem
    ], key: item },
    react["createElement"](loading_placeholder["a" /* default */], { style: search_popular_style.placeholder.productItem.image }))); };
var renderLoadingPlaceholder = function (isHome) {
    if (isHome === void 0) { isHome = false; }
    var list = isHome || 'MOBILE' === window.DEVICE_VERSION ? [1, 2, 3, 4, 5, 6] : [1, 2, 3, 4, 5];
    return (react["createElement"]("div", { style: search_popular_style.placeholder },
        react["createElement"](loading_placeholder["a" /* default */], { style: [search_popular_style.placeholder.title, 'MOBILE' === window.DEVICE_VERSION && search_popular_style.placeholder.titleMobile] }),
        react["createElement"]("div", { style: search_popular_style.placeholder.productList }, Array.isArray(list) && list.map(renderItemPlaceholder))));
};
function view_renderItem(item) {
    var arr = item && item.name && item.name.split('-') || [];
    var name = '';
    var count = '';
    if (arr.length > 1) {
        name = arr[0].trim();
        count = arr[1].trim();
    }
    var navLinkProps = {
        key: "search-popular-item-" + item.id,
        to: routing["jb" /* ROUTING_SEARCH_PATH */] + "/" + item.link,
        style: search_popular_style.item.container
    };
    return (react["createElement"](react_router_dom["NavLink"], __assign({}, navLinkProps),
        react["createElement"]("div", { style: search_popular_style.item.info },
            react["createElement"]("div", { style: search_popular_style.item.info.name }, name),
            react["createElement"]("div", { style: search_popular_style.item.info.content },
                react["createElement"]("span", { style: search_popular_style.item.info.count }, count),
                " l\u01B0\u1EE3t t\u00ECm ki\u1EBFm cho s\u1EA3n ph\u1EA9m n\u00E0y")),
        react["createElement"]("div", { style: search_popular_style.item.info.image(item.cover_image.thumb_url) })));
}
;
var renderView = function (_a) {
    var props = _a.props, state = _a.state;
    var list = props.list;
    return list && !!list.length
        ? (react["createElement"]("div", { style: search_popular_style },
            react["createElement"]("div", { style: [component["c" /* block */].heading.title, search_popular_style.title] },
                react["createElement"]("span", { style: component["c" /* block */].heading.title.text },
                    "T\u00CCM KI\u1EBEM PH\u1ED4 BI\u1EBEN ",
                    react["createElement"]("span", { style: search_popular_style.month }, "30 ng\u00E0y qua"))),
            react["createElement"]("div", { style: search_popular_style.container }, list.map(view_renderItem))))
        : renderLoadingPlaceholder();
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQzNDLE9BQU8sS0FBSyxTQUFTLE1BQU0sMEJBQTBCLENBQUM7QUFDdEQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDN0UsT0FBTyxrQkFBa0IsTUFBTSw0Q0FBNEMsQ0FBQztBQUU1RSxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQUcsVUFBQyxJQUFJLElBQUssT0FBQSxDQUM3Qyw2QkFDRSxLQUFLLEVBQUU7UUFDTCxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxTQUFTO1FBQ3ZDLFFBQVEsS0FBSyxNQUFNLENBQUMsY0FBYyxJQUFJLEtBQUssQ0FBQyxXQUFXLENBQUMsaUJBQWlCO0tBQzFFLEVBQ0QsR0FBRyxFQUFFLElBQUk7SUFDVCxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsS0FBSyxHQUFJLENBQzlELENBQ1AsRUFUOEMsQ0FTOUMsQ0FBQztBQUdGLElBQU0sd0JBQXdCLEdBQUcsVUFBQyxNQUFjO0lBQWQsdUJBQUEsRUFBQSxjQUFjO0lBQzlDLElBQU0sSUFBSSxHQUFHLE1BQU0sSUFBSSxRQUFRLEtBQUssTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNqRyxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVc7UUFDM0Isb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsUUFBUSxLQUFLLE1BQU0sQ0FBQyxjQUFjLElBQUksS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsR0FBSTtRQUM3SCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLElBQ3RDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUNuRCxDQUNGLENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLG9CQUFvQixJQUFJO0lBRXRCLElBQU0sR0FBRyxHQUFHLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUM1RCxJQUFJLElBQUksR0FBRyxFQUFFLENBQUM7SUFDZCxJQUFJLEtBQUssR0FBRyxFQUFFLENBQUM7SUFFZixFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbkIsSUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNyQixLQUFLLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFRCxJQUFNLFlBQVksR0FBRztRQUNuQixHQUFHLEVBQUUseUJBQXVCLElBQUksQ0FBQyxFQUFJO1FBQ3JDLEVBQUUsRUFBSyxtQkFBbUIsU0FBSSxJQUFJLENBQUMsSUFBTTtRQUN6QyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTO0tBQzVCLENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCxvQkFBQyxPQUFPLGVBQUssWUFBWTtRQUN2Qiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJO1lBQ3pCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUcsSUFBSSxDQUFPO1lBQzlDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPO2dCQUFFLDhCQUFNLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUcsS0FBSyxDQUFRO3FGQUFxQyxDQUN4SDtRQUNOLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsR0FBUSxDQUM3RCxDQUNYLENBQUM7QUFDSixDQUFDO0FBQUEsQ0FBQztBQUVGLE1BQU0sQ0FBQyxJQUFNLFVBQVUsR0FBRyxVQUFDLEVBQWdCO1FBQWQsZ0JBQUssRUFBRSxnQkFBSztJQUMvQixJQUFBLGlCQUFJLENBQVc7SUFFdkIsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU07UUFDMUIsQ0FBQyxDQUFDLENBQ0EsNkJBQUssS0FBSyxFQUFFLEtBQUs7WUFDZiw2QkFBSyxLQUFLLEVBQUUsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQztnQkFDdEQsOEJBQU0sS0FBSyxFQUFFLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJOztvQkFBb0IsOEJBQU0sS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLLHVCQUFvQixDQUFPLENBQ2xIO1lBQ04sNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLElBQ3hCLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQ2pCLENBQ0YsQ0FDUDtRQUNELENBQUMsQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO0FBQ2pDLENBQUMsQ0FBQTtBQUVELGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./components/product/search-popular/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_SearchPopularComponent = /** @class */ (function (_super) {
    __extends(SearchPopularComponent, _super);
    function SearchPopularComponent(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    SearchPopularComponent.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state
        };
        return view(args);
    };
    SearchPopularComponent.defaultProps = DEFAULT_PROPS;
    SearchPopularComponent = __decorate([
        radium
    ], SearchPopularComponent);
    return SearchPopularComponent;
}(react["Component"]));
;
/* harmony default export */ var search_popular_component = (component_SearchPopularComponent);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFNakMsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDNUQsT0FBTyxVQUFVLE1BQU0sUUFBUSxDQUFDO0FBR2hDO0lBQXFDLDBDQUErQjtJQUdsRSxnQ0FBWSxLQUFhO1FBQXpCLFlBQ0Usa0JBQU0sS0FBSyxDQUFDLFNBRWI7UUFEQyxLQUFJLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQzs7SUFDN0IsQ0FBQztJQUVELHVDQUFNLEdBQU47UUFDRSxJQUFNLElBQUksR0FBRztZQUNYLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7U0FDbEIsQ0FBQztRQUVGLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDMUIsQ0FBQztJQWRNLG1DQUFZLEdBQVcsYUFBYSxDQUFDO0lBRHhDLHNCQUFzQjtRQUQzQixNQUFNO09BQ0Qsc0JBQXNCLENBZ0IzQjtJQUFELDZCQUFDO0NBQUEsQUFoQkQsQ0FBcUMsS0FBSyxDQUFDLFNBQVMsR0FnQm5EO0FBQUEsQ0FBQztBQUVGLGVBQWUsc0JBQXNCLENBQUMifQ==
// CONCATENATED MODULE: ./components/product/search-popular/index.tsx

/* harmony default export */ var search_popular = (search_popular_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxzQkFBc0IsTUFBTSxhQUFhLENBQUM7QUFDakQsZUFBZSxzQkFBc0IsQ0FBQyJ9
// CONCATENATED MODULE: ./components/banner/carousel/initialize.tsx
var initialize_DEFAULT_PROPS = { list: [], style: {} };
var initialize_INITIAL_STATE = { selectedIndex: 0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBWSxDQUFDO0FBRS9ELE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRyxFQUFFLGFBQWEsRUFBRSxDQUFDLEVBQUUsQ0FBQyJ9
// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// CONCATENATED MODULE: ./components/banner/carousel/style.tsx



/* harmony default export */ var carousel_style = ({
    container: function (len) { return Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                paddingTop: 10,
                paddingBottom: 20,
                display: variable["display"].block,
                overflowX: 'hidden',
                overflowY: 'hidden'
            }],
        DESKTOP: [
            layout["a" /* flexContainer */].justify,
            {
                marginBottom: 50,
                display: variable["display"].flex,
            }
        ],
        GENERAL: [{ width: '100%' }]
    }); },
    panel: function (len) { return Object(responsive["a" /* combineStyle */])({
        MOBILE: [
            layout["a" /* flexContainer */].justify,
            {
                width: 1 === len ? 'calc(100% - 20px)' : 'calc(100% - 20px)',
                maxWidth: 940,
                paddingLeft: 20,
                display: variable["display"].block,
            }
        ],
        DESKTOP: [{
                width: '100%',
                paddingLeft: 0,
                display: variable["display"].flex,
                justifyContent: 'space-between'
            }],
        GENERAL: [{ whiteSpace: 'nowrap', transition: variable["transitionTransform"] }]
    }); },
    link: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                display: variable["display"].inlineBlock,
                verticalAlign: 'top',
                maxWidth: '100%',
                width: '100%',
                marginRight: 10,
                paddingRight: 0,
            }],
        DESKTOP: [{
                display: variable["display"].block,
                maxWidth: 'calc(50% - 10px)',
                width: 'calc(50% - 10px)',
                marginRight: 0,
                paddingRight: 0,
            }],
        GENERAL: [{
                whiteSpace: 'nowrap',
                flex: 1,
                cursor: 'pointer',
                borderRadius: 7,
                paddingTop: '41%',
                transition: variable["transitionNormal"],
                overflow: 'hidden',
                backgroundSize: 'cover',
                backgroundPosition: 'center',
            }]
    }),
    lastLink: {
        marginRight: 20
    },
    placeholder: {
        width: 'calc(100% - 40px)',
        margin: 20,
        paddingTop: '37%',
        background: variable["colorF7"],
        borderRadius: 7
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ3pELE9BQU8sS0FBSyxNQUFNLE1BQU0sdUJBQXVCLENBQUM7QUFDaEQsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxlQUFlO0lBQ2IsU0FBUyxFQUFFLFVBQUMsR0FBRyxJQUFLLE9BQUEsWUFBWSxDQUFDO1FBQy9CLE1BQU0sRUFBRSxDQUFDO2dCQUNQLFVBQVUsRUFBRSxFQUFFO2dCQUNkLGFBQWEsRUFBRSxFQUFFO2dCQUNqQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO2dCQUMvQixTQUFTLEVBQUUsUUFBUTtnQkFDbkIsU0FBUyxFQUFFLFFBQVE7YUFDcEIsQ0FBQztRQUVGLE9BQU8sRUFBRTtZQUNQLE1BQU0sQ0FBQyxhQUFhLENBQUMsT0FBTztZQUM1QjtnQkFDRSxZQUFZLEVBQUUsRUFBRTtnQkFDaEIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTthQUMvQjtTQUNGO1FBRUQsT0FBTyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLENBQUM7S0FDN0IsQ0FBQyxFQWxCa0IsQ0FrQmxCO0lBRUYsS0FBSyxFQUFFLFVBQUMsR0FBRyxJQUFLLE9BQUEsWUFBWSxDQUFDO1FBQzNCLE1BQU0sRUFBRTtZQUNOLE1BQU0sQ0FBQyxhQUFhLENBQUMsT0FBTztZQUM1QjtnQkFDRSxLQUFLLEVBQUUsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLG1CQUFtQjtnQkFDNUQsUUFBUSxFQUFFLEdBQUc7Z0JBQ2IsV0FBVyxFQUFFLEVBQUU7Z0JBQ2YsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSzthQUNoQztTQUNGO1FBRUQsT0FBTyxFQUFFLENBQUM7Z0JBQ1IsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsV0FBVyxFQUFFLENBQUM7Z0JBQ2QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsY0FBYyxFQUFFLGVBQWU7YUFDaEMsQ0FBQztRQUVGLE9BQU8sRUFBRSxDQUFDLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxVQUFVLEVBQUUsUUFBUSxDQUFDLG1CQUFtQixFQUFFLENBQUM7S0FDOUUsQ0FBQyxFQW5CYyxDQW1CZDtJQUVGLElBQUksRUFBRSxZQUFZLENBQUM7UUFDakIsTUFBTSxFQUFFLENBQUM7Z0JBQ1AsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztnQkFDckMsYUFBYSxFQUFFLEtBQUs7Z0JBQ3BCLFFBQVEsRUFBRSxNQUFNO2dCQUNoQixLQUFLLEVBQUUsTUFBTTtnQkFDYixXQUFXLEVBQUUsRUFBRTtnQkFDZixZQUFZLEVBQUUsQ0FBQzthQUNoQixDQUFDO1FBRUYsT0FBTyxFQUFFLENBQUM7Z0JBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztnQkFDL0IsUUFBUSxFQUFFLGtCQUFrQjtnQkFDNUIsS0FBSyxFQUFFLGtCQUFrQjtnQkFDekIsV0FBVyxFQUFFLENBQUM7Z0JBQ2QsWUFBWSxFQUFFLENBQUM7YUFDaEIsQ0FBQztRQUVGLE9BQU8sRUFBRSxDQUFDO2dCQUNSLFVBQVUsRUFBRSxRQUFRO2dCQUNwQixJQUFJLEVBQUUsQ0FBQztnQkFDUCxNQUFNLEVBQUUsU0FBUztnQkFDakIsWUFBWSxFQUFFLENBQUM7Z0JBQ2YsVUFBVSxFQUFFLEtBQUs7Z0JBQ2pCLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO2dCQUNyQyxRQUFRLEVBQUUsUUFBUTtnQkFDbEIsY0FBYyxFQUFFLE9BQU87Z0JBQ3ZCLGtCQUFrQixFQUFFLFFBQVE7YUFDN0IsQ0FBQztLQUNILENBQUM7SUFFRixRQUFRLEVBQUU7UUFDUixXQUFXLEVBQUUsRUFBRTtLQUNoQjtJQUVELFdBQVcsRUFBRTtRQUNYLEtBQUssRUFBRSxtQkFBbUI7UUFDMUIsTUFBTSxFQUFFLEVBQUU7UUFDVixVQUFVLEVBQUUsS0FBSztRQUNqQixVQUFVLEVBQUUsUUFBUSxDQUFDLE9BQU87UUFDNUIsWUFBWSxFQUFFLENBQUM7S0FDaEI7Q0FDRixDQUFDIn0=
// CONCATENATED MODULE: ./components/banner/carousel/view.tsx
var view_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};





var renderNavLink = function (style, link) { return react["createElement"](react_router_dom["NavLink"], { to: link, style: style }); };
var view_renderLoadingPlaceholder = function () {
    var list = 'MOBILE' === window.DEVICE_VERSION ? [1] : [1, 2];
    return (react["createElement"]("div", { style: carousel_style.placeholder }));
};
function carousel_view_renderItem(item, $index) {
    var itemProps = generateLinkProps(item, $index, this.total, this.selectedIndex);
    if (!!item.links && 2 === item.links.length) {
        return (react["createElement"]("div", { key: "item-banner-" + $index, style: Object.assign({}, carousel_style.link, $index === this.total - 1 && carousel_style.lastLink, { backgroundImage: $index <= this.selectedIndex + 1 ? "url(" + Object(encode["h" /* decodeEntities */])(item && item.cover_image && item.cover_image.medium_url || '') + ")" : '' }, { position: 'relative' }) },
            react["createElement"]("div", { style: { display: 'flex', position: 'absolute', top: 0, left: 0, width: '100%', height: '100%' } },
                renderNavLink({ display: 'block', height: '100%', flex: 6 }, Object(validate["f" /* getNavLink */])(item.link)),
                renderNavLink({ display: 'block', height: '100%', flex: 5 }, Object(validate["f" /* getNavLink */])(item.links[0])),
                renderNavLink({ display: 'block', height: '100%', flex: 4 }, Object(validate["f" /* getNavLink */])(item.links[1])))));
    }
    return react["createElement"](react_router_dom["NavLink"], view_assign({}, itemProps));
}
;
var generateLinkProps = function (item, $index, total, selectedIndex) { return ({
    style: Object.assign({}, carousel_style.link, $index === total - 1 && carousel_style.lastLink, { backgroundImage: $index <= selectedIndex + 1 ? "url(" + Object(encode["h" /* decodeEntities */])(item && item.cover_image && item.cover_image.medium_url || '') + ")" : '' }),
    to: Object(validate["f" /* getNavLink */])(item && item.link || ''),
    key: "banner-main-home-" + (item && item.id || 0),
}); };
var view_renderView = function (_a) {
    var selectedIndex = _a.selectedIndex, list = _a.list, style = _a.style, handleTouchStart = _a.handleTouchStart, handleTouchMove = _a.handleTouchMove;
    return ((!list || 0 === list.length)
        ? view_renderLoadingPlaceholder()
        : (react["createElement"]("div", { style: Object.assign({}, carousel_style.container(list.length), style) },
            react["createElement"]("div", { style: Object.assign({}, carousel_style.panel(list.length), {
                    transform: "translateX(calc(" + selectedIndex * (-100) + "% + " + selectedIndex * 10 + "px + " + (0 === selectedIndex ? -10 : 0) + "px + " + (list.length - 1 === selectedIndex ? 10 : 0) + "px ))"
                }), onTouchStart: handleTouchStart, onTouchMove: handleTouchMove }, Array.isArray(list) && list.map(carousel_view_renderItem, { total: list.length, selectedIndex: selectedIndex })))));
};
/* harmony default export */ var carousel_view = (view_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFHdkQsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRTVCLElBQU0sYUFBYSxHQUFHLFVBQUMsS0FBSyxFQUFFLElBQUksSUFBSyxPQUFBLG9CQUFDLE9BQU8sSUFBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxLQUFLLEdBQUksRUFBbkMsQ0FBbUMsQ0FBQztBQUUzRSxJQUFNLHdCQUF3QixHQUFHO0lBQy9CLElBQU0sSUFBSSxHQUFHLFFBQVEsS0FBSyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUUvRCxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsR0FDdkIsQ0FDUCxDQUFDO0FBQ0osQ0FBQyxDQUFBO0FBRUQsb0JBQW9CLElBQUksRUFBRSxNQUFNO0lBQzlCLElBQU0sU0FBUyxHQUFHLGlCQUFpQixDQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7SUFFbEYsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUM1QyxNQUFNLENBQUMsQ0FDTCw2QkFDRSxHQUFHLEVBQUUsaUJBQWUsTUFBUSxFQUM1QixLQUFLLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQ3JCLEtBQUssQ0FBQyxJQUFJLEVBQ1YsTUFBTSxLQUFLLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxJQUFJLEtBQUssQ0FBQyxRQUFRLEVBQzNDLEVBQUUsZUFBZSxFQUFFLE1BQU0sSUFBSSxJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBTyxjQUFjLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLElBQUksRUFBRSxDQUFDLE1BQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQ3BKLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRSxDQUN6QjtZQUNELDZCQUFLLEtBQUssRUFBRSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFO2dCQUNsRyxhQUFhLENBQUMsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ25GLGFBQWEsQ0FBQyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLEVBQUUsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdkYsYUFBYSxDQUFDLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQ3BGLENBQ0YsQ0FDUCxDQUFBO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQyxvQkFBQyxPQUFPLGVBQUssU0FBUyxFQUFJLENBQUM7QUFDcEMsQ0FBQztBQUFBLENBQUM7QUFFRixJQUFNLGlCQUFpQixHQUFHLFVBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsYUFBYSxJQUFLLE9BQUEsQ0FBQztJQUNqRSxLQUFLLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQ3JCLEtBQUssQ0FBQyxJQUFJLEVBQ1YsTUFBTSxLQUFLLEtBQUssR0FBRyxDQUFDLElBQUksS0FBSyxDQUFDLFFBQVEsRUFDdEMsRUFBRSxlQUFlLEVBQUUsTUFBTSxJQUFJLGFBQWEsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQU8sY0FBYyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxJQUFJLEVBQUUsQ0FBQyxNQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUNoSjtJQUNELEVBQUUsRUFBRSxVQUFVLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDO0lBQ3ZDLEdBQUcsRUFBRSx1QkFBb0IsSUFBSSxJQUFJLElBQUksQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFFO0NBQ2hELENBQUMsRUFSZ0UsQ0FRaEUsQ0FBQztBQUVILElBQU0sVUFBVSxHQUFHLFVBQUMsRUFBaUU7UUFBL0QsZ0NBQWEsRUFBRSxjQUFJLEVBQUUsZ0JBQUssRUFBRSxzQ0FBZ0IsRUFBRSxvQ0FBZTtJQUFPLE9BQUEsQ0FDeEYsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUMxQixDQUFDLENBQUMsd0JBQXdCLEVBQUU7UUFDNUIsQ0FBQyxDQUFDLENBQ0EsNkJBQUssS0FBSyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEtBQUssQ0FBQztZQUNoRSw2QkFDRSxLQUFLLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQ3JCLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUN4QjtvQkFDRSxTQUFTLEVBQUUscUJBQW1CLGFBQWEsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLFlBQU8sYUFBYSxHQUFHLEVBQUUsY0FBUSxDQUFDLEtBQUssYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFRLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxLQUFLLGFBQWEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQU87aUJBQzVLLENBQ0YsRUFDRCxZQUFZLEVBQUUsZ0JBQWdCLEVBQzlCLFdBQVcsRUFBRSxlQUFlLElBQzNCLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxhQUFhLGVBQUEsRUFBRSxDQUFDLENBQy9FLENBQ0YsQ0FDUCxDQUNKO0FBbEJ5RixDQWtCekYsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./components/banner/carousel/component.tsx
var component_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var component_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_BannerFeature = /** @class */ (function (_super) {
    component_extends(BannerFeature, _super);
    function BannerFeature(props) {
        var _this = _super.call(this, props) || this;
        _this.xDown = null;
        _this.yDown = null;
        _this.state = initialize_INITIAL_STATE;
        return _this;
    }
    BannerFeature.prototype.handleTouchStart = function (e) {
        this.xDown = e.touches[0].clientX;
        this.yDown = e.touches[0].clientY;
    };
    BannerFeature.prototype.handleTouchMove = function (e) {
        if (!this.xDown || !this.yDown) {
            return;
        }
        var xUp = e.touches[0].clientX;
        var yUp = e.touches[0].clientY;
        var xDiff = this.xDown - xUp;
        var yDiff = this.yDown - yUp;
        if (Math.abs(xDiff) > Math.abs(yDiff) && xDiff > 0) {
            this.handleswipeLeft();
        }
        if (Math.abs(xDiff) > Math.abs(yDiff) && xDiff < 0) {
            this.handleswipeRight();
        }
        this.xDown = null;
        this.yDown = null;
        if (Math.abs(xDiff) > 5) {
            e.preventDefault();
            e.returnValue = false;
            return false;
        }
    };
    BannerFeature.prototype.handleswipeLeft = function () {
        var selectedIndex = this.state.selectedIndex;
        var list = this.props.list;
        if (selectedIndex >= list.length - 1) {
            return;
        }
        this.setState({ selectedIndex: selectedIndex + 1 });
        console.log(selectedIndex + 1);
    };
    BannerFeature.prototype.handleswipeRight = function () {
        var selectedIndex = this.state.selectedIndex;
        var list = this.props.list;
        if (selectedIndex === 0) {
            return;
        }
        this.setState({ selectedIndex: selectedIndex - 1 });
        console.log(selectedIndex - 1);
    };
    BannerFeature.prototype.shouldComponentUpdate = function (nextProps, nextState) {
        var listLen = this.props.list && this.props.list.length || 0;
        var nextListLen = nextProps.list && nextProps.list.length || 0;
        if (listLen !== nextListLen) {
            return true;
        }
        if (this.state.selectedIndex !== nextState.selectedIndex) {
            return true;
        }
        return false;
    };
    BannerFeature.prototype.render = function () {
        var selectedIndex = this.state.selectedIndex;
        var _a = this.props, list = _a.list, style = _a.style;
        return carousel_view({
            selectedIndex: selectedIndex,
            list: list,
            style: style,
            handleTouchStart: this.handleTouchStart.bind(this),
            handleTouchMove: this.handleTouchMove.bind(this)
        });
    };
    BannerFeature.defaultProps = initialize_DEFAULT_PROPS;
    BannerFeature = component_decorate([
        radium
    ], BannerFeature);
    return BannerFeature;
}(react["Component"]));
;
/* harmony default export */ var carousel_component = (component_BannerFeature);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUNsQyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUdqQyxPQUFPLEVBQUUsYUFBYSxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUM1RCxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFHaEM7SUFBNEIsaUNBQXlCO0lBS25ELHVCQUFZLEtBQWE7UUFBekIsWUFDRSxrQkFBTSxLQUFLLENBQUMsU0FFYjtRQU5PLFdBQUssR0FBUSxJQUFJLENBQUM7UUFDbEIsV0FBSyxHQUFRLElBQUksQ0FBQztRQUl4QixLQUFJLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQzs7SUFDN0IsQ0FBQztJQUVELHdDQUFnQixHQUFoQixVQUFpQixDQUFDO1FBQ2hCLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7UUFDbEMsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztJQUNwQyxDQUFDO0lBRUQsdUNBQWUsR0FBZixVQUFnQixDQUFDO1FBQ2YsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDL0IsTUFBTSxDQUFDO1FBQ1QsQ0FBQztRQUVELElBQU0sR0FBRyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO1FBQ2pDLElBQU0sR0FBRyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO1FBRWpDLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO1FBQy9CLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO1FBRS9CLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuRCxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDekIsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuRCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUMxQixDQUFDO1FBRUQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFFbEIsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUNuQixDQUFDLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztZQUN0QixNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2YsQ0FBQztJQUNILENBQUM7SUFFRCx1Q0FBZSxHQUFmO1FBQ1UsSUFBQSx3Q0FBYSxDQUEwQjtRQUN2QyxJQUFBLHNCQUFJLENBQWdCO1FBRTVCLEVBQUUsQ0FBQyxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUM7UUFBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxhQUFhLEVBQUcsYUFBYSxHQUFHLENBQUMsRUFBQyxDQUFDLENBQUE7UUFDbkQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDLENBQUE7SUFDaEMsQ0FBQztJQUVELHdDQUFnQixHQUFoQjtRQUNVLElBQUEsd0NBQWEsQ0FBMEI7UUFDdkMsSUFBQSxzQkFBSSxDQUFnQjtRQUU1QixFQUFFLENBQUMsQ0FBQyxhQUFhLEtBQUssQ0FBRSxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQztRQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLGFBQWEsRUFBRyxhQUFhLEdBQUcsQ0FBQyxFQUFDLENBQUMsQ0FBQTtRQUNuRCxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUMsQ0FBQTtJQUNoQyxDQUFDO0lBRUQsNkNBQXFCLEdBQXJCLFVBQXNCLFNBQWlCLEVBQUUsU0FBaUI7UUFDeEQsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQztRQUMvRCxJQUFNLFdBQVcsR0FBRyxTQUFTLENBQUMsSUFBSSxJQUFJLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQztRQUNqRSxFQUFFLENBQUMsQ0FBQyxPQUFPLEtBQUssV0FBVyxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFBQyxDQUFDO1FBQzdDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxLQUFLLFNBQVMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUFDLENBQUM7UUFFMUUsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFRCw4QkFBTSxHQUFOO1FBQ1UsSUFBQSx3Q0FBYSxDQUEyQjtRQUMxQyxJQUFBLGVBQXNDLEVBQXBDLGNBQUksRUFBRSxnQkFBSyxDQUEwQjtRQUM3QyxNQUFNLENBQUMsVUFBVSxDQUFDO1lBQ2hCLGFBQWEsZUFBQTtZQUNiLElBQUksTUFBQTtZQUNKLEtBQUssT0FBQTtZQUNMLGdCQUFnQixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2xELGVBQWUsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FBRSxDQUFDLENBQUM7SUFDeEQsQ0FBQztJQS9FTSwwQkFBWSxHQUFXLGFBQWEsQ0FBQztJQUR4QyxhQUFhO1FBRGxCLE1BQU07T0FDRCxhQUFhLENBaUZsQjtJQUFELG9CQUFDO0NBQUEsQUFqRkQsQ0FBNEIsU0FBUyxHQWlGcEM7QUFBQSxDQUFDO0FBRUYsZUFBZSxhQUFhLENBQUMifQ==
// CONCATENATED MODULE: ./components/banner/carousel/index.tsx

/* harmony default export */ var carousel = (carousel_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxhQUFhLE1BQU0sYUFBYSxDQUFDO0FBQ3hDLGVBQWUsYUFBYSxDQUFDIn0=
// CONCATENATED MODULE: ./components/container/mobile-category/style.tsx

var INLINE_STYLE = {
    '.icon-category:hover .icon-category-logo': {
        filter: 'invert(100%)'
    }
};
/* harmony default export */ var mobile_category_style = ({
    container: {
        categoryList: {
            display: variable["display"].flex,
            flexWrap: 'wrap',
            paddingTop: 10,
            paddingBottom: 0,
            paddingLeft: 0,
            paddingRight: 0,
            title: {
                textAlign: 'center',
                fontSize: 22,
                textTransform: 'uppercase',
                fontFamily: 'avenir-next-light',
                marginTop: 10,
                marginBottom: 10,
                fontWeight: 800,
            },
            category: {
                display: variable["display"].flex,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                width: '33.33%',
                marginBottom: 25,
                img: {
                    width: 70,
                    minWidth: 70,
                    maxWidth: 70,
                    height: 70,
                    minHeight: 70,
                    maxHeight: 70,
                    paddingTop: 12,
                    paddingRight: 12,
                    paddingBottom: 12,
                    paddingLeft: 12,
                    marginBottom: 10,
                    borderRadius: '40%',
                    background: variable["colorF7"],
                    border: "1px solid " + variable["colorF0"],
                    transition: variable["transitionNormal"],
                    opacity: .9
                },
                name: {
                    fontSize: 12,
                    color: variable["color2E"],
                    fontFamily: variable["fontAvenirMedium"],
                    textTransform: 'uppercase'
                }
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxNQUFNLENBQUMsSUFBTSxZQUFZLEdBQUc7SUFDMUIsMENBQTBDLEVBQUU7UUFDMUMsTUFBTSxFQUFFLGNBQWM7S0FDdkI7Q0FDRixDQUFBO0FBRUQsZUFBZTtJQUNiLFNBQVMsRUFBRTtRQUVULFlBQVksRUFBRTtZQUNaLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsUUFBUSxFQUFFLE1BQU07WUFDaEIsVUFBVSxFQUFFLEVBQUU7WUFDZCxhQUFhLEVBQUUsQ0FBQztZQUNoQixXQUFXLEVBQUUsQ0FBQztZQUNkLFlBQVksRUFBRSxDQUFDO1lBRWYsS0FBSyxFQUFFO2dCQUNMLFNBQVMsRUFBRSxRQUFRO2dCQUNuQixRQUFRLEVBQUUsRUFBRTtnQkFDWixhQUFhLEVBQUUsV0FBVztnQkFDMUIsVUFBVSxFQUFFLG1CQUFtQjtnQkFDL0IsU0FBUyxFQUFFLEVBQUU7Z0JBQ2IsWUFBWSxFQUFFLEVBQUU7Z0JBQ2hCLFVBQVUsRUFBRSxHQUFHO2FBQ2hCO1lBRUQsUUFBUSxFQUFFO2dCQUNSLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLGFBQWEsRUFBRSxRQUFRO2dCQUN2QixjQUFjLEVBQUUsUUFBUTtnQkFDeEIsVUFBVSxFQUFFLFFBQVE7Z0JBQ3BCLEtBQUssRUFBRSxRQUFRO2dCQUNmLFlBQVksRUFBRSxFQUFFO2dCQUVoQixHQUFHLEVBQUU7b0JBQ0gsS0FBSyxFQUFFLEVBQUU7b0JBQ1QsUUFBUSxFQUFFLEVBQUU7b0JBQ1osUUFBUSxFQUFFLEVBQUU7b0JBQ1osTUFBTSxFQUFFLEVBQUU7b0JBQ1YsU0FBUyxFQUFFLEVBQUU7b0JBQ2IsU0FBUyxFQUFFLEVBQUU7b0JBQ2IsVUFBVSxFQUFFLEVBQUU7b0JBQ2QsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLGFBQWEsRUFBRSxFQUFFO29CQUNqQixXQUFXLEVBQUUsRUFBRTtvQkFDZixZQUFZLEVBQUUsRUFBRTtvQkFDaEIsWUFBWSxFQUFFLEtBQUs7b0JBQ25CLFVBQVUsRUFBRSxRQUFRLENBQUMsT0FBTztvQkFDNUIsTUFBTSxFQUFFLGVBQWEsUUFBUSxDQUFDLE9BQVM7b0JBQ3ZDLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO29CQUNyQyxPQUFPLEVBQUUsRUFBRTtpQkFDWjtnQkFFRCxJQUFJLEVBQUU7b0JBQ0osUUFBUSxFQUFFLEVBQUU7b0JBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO29CQUN2QixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsYUFBYSxFQUFFLFdBQVc7aUJBQzNCO2FBQ0Y7U0FDRjtLQUNGO0NBQ0ssQ0FBQyJ9
// CONCATENATED MODULE: ./components/container/mobile-category/initialize.tsx

var mobile_category_initialize_DEFAULT_PROPS = {};
var mobile_category_initialize_INITIAL_STATE = {};
var categoryList = [
    {
        id: 1,
        name: 'Tóc',
        slug: routing["Xa" /* ROUTING_PRODUCT_CATEGORY_PATH */] + "/hair",
        imgUrl: '../../../assets/images/category/a-13.png'
    },
    {
        id: 2,
        name: 'Trang điểm',
        slug: routing["Xa" /* ROUTING_PRODUCT_CATEGORY_PATH */] + "/makeup",
        imgUrl: '../../../assets/images/category/a-14.png'
    },
    {
        id: 3,
        name: 'Dưỡng da',
        slug: routing["Xa" /* ROUTING_PRODUCT_CATEGORY_PATH */] + "/skin-care",
        imgUrl: '../../../assets/images/category/a-15.png'
    },
    {
        id: 4,
        name: 'Cơ thể',
        slug: routing["Xa" /* ROUTING_PRODUCT_CATEGORY_PATH */] + "/bath-body",
        imgUrl: '../../../assets/images/category/a-16.png'
    },
    {
        id: 5,
        name: 'Cọ & Phụ kiện',
        slug: routing["Xa" /* ROUTING_PRODUCT_CATEGORY_PATH */] + "/tools-accessories",
        imgUrl: '../../../assets/images/category/a-17.png'
    },
    {
        id: 6,
        name: 'Nước hoa',
        slug: routing["Xa" /* ROUTING_PRODUCT_CATEGORY_PATH */] + "/perfume",
        imgUrl: '../../../assets/images/category/a-18.png'
    },
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBRXZGLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRyxFQUFFLENBQUM7QUFFaEMsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHLEVBQUUsQ0FBQztBQUVoQyxNQUFNLENBQUMsSUFBTSxZQUFZLEdBQUc7SUFDMUI7UUFDRSxFQUFFLEVBQUUsQ0FBQztRQUNMLElBQUksRUFBRSxLQUFLO1FBQ1gsSUFBSSxFQUFLLDZCQUE2QixVQUFPO1FBQzdDLE1BQU0sRUFBRSwwQ0FBMEM7S0FDbkQ7SUFDRDtRQUNFLEVBQUUsRUFBRSxDQUFDO1FBQ0wsSUFBSSxFQUFFLFlBQVk7UUFDbEIsSUFBSSxFQUFLLDZCQUE2QixZQUFTO1FBQy9DLE1BQU0sRUFBRSwwQ0FBMEM7S0FDbkQ7SUFDRDtRQUNFLEVBQUUsRUFBRSxDQUFDO1FBQ0wsSUFBSSxFQUFFLFVBQVU7UUFDaEIsSUFBSSxFQUFLLDZCQUE2QixlQUFZO1FBQ2xELE1BQU0sRUFBRSwwQ0FBMEM7S0FDbkQ7SUFDRDtRQUNFLEVBQUUsRUFBRSxDQUFDO1FBQ0wsSUFBSSxFQUFFLFFBQVE7UUFDZCxJQUFJLEVBQUssNkJBQTZCLGVBQVk7UUFDbEQsTUFBTSxFQUFFLDBDQUEwQztLQUNuRDtJQUNEO1FBQ0UsRUFBRSxFQUFFLENBQUM7UUFDTCxJQUFJLEVBQUUsZUFBZTtRQUNyQixJQUFJLEVBQUssNkJBQTZCLHVCQUFvQjtRQUMxRCxNQUFNLEVBQUUsMENBQTBDO0tBQ25EO0lBQ0Q7UUFDRSxFQUFFLEVBQUUsQ0FBQztRQUNMLElBQUksRUFBRSxVQUFVO1FBQ2hCLElBQUksRUFBSyw2QkFBNkIsYUFBVTtRQUNoRCxNQUFNLEVBQUUsMENBQTBDO0tBQ25EO0NBQ0YsQ0FBQSJ9
// CONCATENATED MODULE: ./components/container/mobile-category/view.tsx
var mobile_category_view_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};






function renderComponent() {
    var categoryStyle = mobile_category_style.container.categoryList;
    var renderItem = function (item) {
        var navLinkProps = {
            to: item.slug,
            key: "category-slide-" + item.id,
            style: categoryStyle.category,
            class: 'icon-category'
        };
        return (react["createElement"](react_router_dom["NavLink"], mobile_category_view_assign({}, navLinkProps),
            react["createElement"]("img", { style: categoryStyle.category.img, src: item.imgUrl, className: 'icon-category-logo' }),
            react["createElement"]("div", { style: categoryStyle.category.name }, Object(encode["h" /* decodeEntities */])(item.name))));
    };
    return (react["createElement"]("mobile-category", { style: mobile_category_style.container },
        react["createElement"]("div", { style: categoryStyle.title }, "Danh m\u1EE5c S\u1EA3n ph\u1EA9m"),
        react["createElement"]("div", { style: categoryStyle }, Array.isArray(categoryList)
            && categoryList.map(renderItem)),
        react["createElement"](radium["Style"], { rules: INLINE_STYLE })));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUMvQixPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFFM0MsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBRXZELE9BQU8sS0FBSyxFQUFFLEVBQUUsWUFBWSxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBQzlDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFFNUMsTUFBTTtJQUVKLElBQU0sYUFBYSxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDO0lBQ25ELElBQU0sVUFBVSxHQUFHLFVBQUMsSUFBSTtRQUN0QixJQUFNLFlBQVksR0FBRztZQUNuQixFQUFFLEVBQUUsSUFBSSxDQUFDLElBQUk7WUFDYixHQUFHLEVBQUUsb0JBQWtCLElBQUksQ0FBQyxFQUFJO1lBQ2hDLEtBQUssRUFBRSxhQUFhLENBQUMsUUFBUTtZQUM3QixLQUFLLEVBQUUsZUFBZTtTQUN2QixDQUFBO1FBRUQsTUFBTSxDQUFDLENBQ0wsb0JBQUMsT0FBTyxlQUFLLFlBQVk7WUFDdkIsNkJBQUssS0FBSyxFQUFFLGFBQWEsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLFNBQVMsRUFBRSxvQkFBb0IsR0FBRztZQUM1Riw2QkFBSyxLQUFLLEVBQUUsYUFBYSxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUcsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBTyxDQUNsRSxDQUNYLENBQUE7SUFDSCxDQUFDLENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCx5Q0FBaUIsS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTO1FBQ3JDLDZCQUFLLEtBQUssRUFBRSxhQUFhLENBQUMsS0FBSyx1Q0FBeUI7UUFDeEQsNkJBQUssS0FBSyxFQUFFLGFBQWEsSUFFckIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUM7ZUFDeEIsWUFBWSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FFN0I7UUFDTixvQkFBQyxLQUFLLElBQUMsS0FBSyxFQUFFLFlBQVksR0FBSSxDQUNkLENBQ25CLENBQUM7QUFDSixDQUFDO0FBQUEsQ0FBQyJ9
// CONCATENATED MODULE: ./components/container/mobile-category/component.tsx
var mobile_category_component_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var mobile_category_component_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_MobileCategory = /** @class */ (function (_super) {
    mobile_category_component_extends(MobileCategory, _super);
    function MobileCategory(props) {
        var _this = _super.call(this, props) || this;
        _this.state = mobile_category_initialize_INITIAL_STATE;
        return _this;
    }
    MobileCategory.prototype.render = function () {
        return renderComponent();
    };
    MobileCategory.defaultProps = mobile_category_initialize_DEFAULT_PROPS;
    MobileCategory = mobile_category_component_decorate([
        radium
    ], MobileCategory);
    return MobileCategory;
}(react["Component"]));
;
/* harmony default export */ var mobile_category_component = (component_MobileCategory);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUNsQyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sUUFBUSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBSTVEO0lBQTZCLGtDQUF5QjtJQUdwRCx3QkFBWSxLQUFLO1FBQWpCLFlBQ0Usa0JBQU0sS0FBSyxDQUFDLFNBRWI7UUFEQyxLQUFJLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQzs7SUFDN0IsQ0FBQztJQUVELCtCQUFNLEdBQU47UUFDRSxNQUFNLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDM0IsQ0FBQztJQVRNLDJCQUFZLEdBQUcsYUFBdUIsQ0FBQztJQUQxQyxjQUFjO1FBRG5CLE1BQU07T0FDRCxjQUFjLENBV25CO0lBQUQscUJBQUM7Q0FBQSxBQVhELENBQTZCLFNBQVMsR0FXckM7QUFBQSxDQUFDO0FBRUYsZUFBZSxjQUFjLENBQUMifQ==
// CONCATENATED MODULE: ./components/container/mobile-category/index.tsx

/* harmony default export */ var mobile_category = (mobile_category_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxjQUFjLE1BQU0sYUFBYSxDQUFDO0FBQ3pDLGVBQWUsY0FBYyxDQUFDIn0=
// EXTERNAL MODULE: ../node_modules/react-on-screen/lib/index.js
var lib = __webpack_require__(758);
var lib_default = /*#__PURE__*/__webpack_require__.n(lib);

// EXTERNAL MODULE: ./components/ui/loading/index.tsx + 4 modules
var loading = __webpack_require__(747);

// CONCATENATED MODULE: ./container/app-shop/index/initialize.tsx
var index_initialize_DEFAULT_PROPS = {};
var index_initialize_INITIAL_STATE = {
    isFetchedFeatureBanner: false,
    isFetchedHotBoxes: false,
    isFetchedMagazineList: false,
    isFetchedNewProducts: false,
    isFetchedPopularSearch: false,
    isFetchedWatchedList: false,
    isFetchedActivityFeed: false,
    isFetchedFooterBanner: false,
    isPriorotyBlock: true,
};
var bestSellingParams = {
    idCategory: 'best-selling-beauty-box',
    params: [
        { key: 'page', value: 1 },
        { key: 'sort', value: 'newest' }
    ]
};
var newProductsParams = {
    idCategory: 'new-products',
    params: [
        { key: 'page', value: 1 },
        { key: 'sort', value: 'newest' }
    ]
};
var MOBILE_CATEGORY_MENU = [
    {
        id: 1,
        name: 'Beauty Box',
        slug: "beauty-box"
    },
    {
        id: 2,
        name: 'Best Sellers',
        slug: "bestsellers"
    },
    {
        id: 3,
        name: 'Makeup',
        slug: "makeup"
    },
    {
        id: 4,
        name: 'Skin Care',
        slug: "skin-care"
    },
    {
        id: 5,
        name: 'Bath & Body',
        slug: "bath-body"
    },
    {
        id: 6,
        name: 'Hair',
        slug: "hair"
    },
    {
        id: 7,
        name: 'New Products',
        slug: "new-products"
    },
    {
        id: 8,
        name: 'Tools & Accessories',
        slug: "tools-accessories"
    },
    {
        id: 9,
        name: 'Perfume',
        slug: "perfume"
    }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRyxFQUFZLENBQUM7QUFDMUMsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHO0lBQzNCLHNCQUFzQixFQUFFLEtBQUs7SUFDN0IsaUJBQWlCLEVBQUUsS0FBSztJQUN4QixxQkFBcUIsRUFBRSxLQUFLO0lBQzVCLG9CQUFvQixFQUFFLEtBQUs7SUFDM0Isc0JBQXNCLEVBQUUsS0FBSztJQUM3QixvQkFBb0IsRUFBRSxLQUFLO0lBQzNCLHFCQUFxQixFQUFFLEtBQUs7SUFDNUIscUJBQXFCLEVBQUUsS0FBSztJQUM1QixlQUFlLEVBQUUsSUFBSTtDQUNaLENBQUM7QUFFWixNQUFNLENBQUMsSUFBTSxpQkFBaUIsR0FBRztJQUMvQixVQUFVLEVBQUUseUJBQXlCO0lBQ3JDLE1BQU0sRUFBRTtRQUNOLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFO1FBQ3pCLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFO0tBQ2pDO0NBQ0YsQ0FBQztBQUVGLE1BQU0sQ0FBQyxJQUFNLGlCQUFpQixHQUFHO0lBQy9CLFVBQVUsRUFBRSxjQUFjO0lBQzFCLE1BQU0sRUFBRTtRQUNOLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFO1FBQ3pCLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFO0tBQ2pDO0NBQ0YsQ0FBQztBQUVGLE1BQU0sQ0FBQyxJQUFNLG9CQUFvQixHQUFHO0lBQ2xDO1FBQ0UsRUFBRSxFQUFFLENBQUM7UUFDTCxJQUFJLEVBQUUsWUFBWTtRQUNsQixJQUFJLEVBQUUsWUFBWTtLQUNuQjtJQUNEO1FBQ0UsRUFBRSxFQUFFLENBQUM7UUFDTCxJQUFJLEVBQUUsY0FBYztRQUNwQixJQUFJLEVBQUUsYUFBYTtLQUNwQjtJQUNEO1FBQ0UsRUFBRSxFQUFFLENBQUM7UUFDTCxJQUFJLEVBQUUsUUFBUTtRQUNkLElBQUksRUFBRSxRQUFRO0tBQ2Y7SUFDRDtRQUNFLEVBQUUsRUFBRSxDQUFDO1FBQ0wsSUFBSSxFQUFFLFdBQVc7UUFDakIsSUFBSSxFQUFFLFdBQVc7S0FDbEI7SUFDRDtRQUNFLEVBQUUsRUFBRSxDQUFDO1FBQ0wsSUFBSSxFQUFFLGFBQWE7UUFDbkIsSUFBSSxFQUFFLFdBQVc7S0FDbEI7SUFDRDtRQUNFLEVBQUUsRUFBRSxDQUFDO1FBQ0wsSUFBSSxFQUFFLE1BQU07UUFDWixJQUFJLEVBQUUsTUFBTTtLQUNiO0lBQ0Q7UUFDRSxFQUFFLEVBQUUsQ0FBQztRQUNMLElBQUksRUFBRSxjQUFjO1FBQ3BCLElBQUksRUFBRSxjQUFjO0tBQ3JCO0lBQ0Q7UUFDRSxFQUFFLEVBQUUsQ0FBQztRQUNMLElBQUksRUFBRSxxQkFBcUI7UUFDM0IsSUFBSSxFQUFFLG1CQUFtQjtLQUMxQjtJQUNEO1FBQ0UsRUFBRSxFQUFFLENBQUM7UUFDTCxJQUFJLEVBQUUsU0FBUztRQUNmLElBQUksRUFBRSxTQUFTO0tBQ2hCO0NBQ0YsQ0FBQSJ9
// CONCATENATED MODULE: ./container/app-shop/index/style.tsx


/* harmony default export */ var index_style = ({
    display: 'block',
    position: 'relative',
    zIndex: variable["zIndex5"],
    wrapBannerMain: {
        marginBottom: 40
    },
    groupProductVideomagazine: [
        layout["b" /* splitContainer */],
        layout["a" /* flexContainer */].justify, {
            marginBottom: '20px',
        }
    ],
    feedContainer: {
        paddingTop: '30%',
        position: variable["position"].relative,
        panel: {
            position: variable["position"].absolute,
            top: 0,
            left: 0,
            height: '100%',
            width: '100%',
            overflowX: 'hidden',
            overflowY: 'auto',
            feed: {
                paddingTop: 1,
                paddingRight: 1,
                paddingBottom: 1,
                paddingLeft: 1,
            }
        }
    },
    mainBanner: {
        marginBottom: 20
    },
    list: {
        paddingTop: 8,
        paddingRight: 8,
        paddingBottom: 8,
        paddingLeft: 8,
        item: {
            marginBottom: 8,
            boxShadow: variable["shadowBlur"],
            image: {
                display: variable["display"].block,
                width: '100%',
                maxWidth: '100%',
                height: 'auto'
            },
            title: {
                width: '100%',
                height: 30,
                lineHeight: '30px',
                textAlign: 'center',
                fontSize: 14,
                color: variable["color4D"],
                overflow: variable["visible"].hidden,
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap'
            }
        }
    },
    placeholder: {
        banner: {
            width: '100%',
            paddingTop: '21%',
            marginBottom: 20,
            marginTop: 20
        },
        productList: {
            title: {
                width: 200,
                marginLeft: 10,
                height: 50,
                marginBottom: 20,
            },
            list: {
                display: variable["display"].flex
            },
            item: {
                flex: 1,
                paddingLeft: 10,
                paddingRight: 10,
                marginBottom: 20,
                minWidth: '50%',
                width: '50%',
                image: {
                    width: '100%',
                    height: 'auto',
                    paddingTop: '82%',
                    marginBottom: 10,
                },
                text: {
                    width: '94%',
                    height: 25,
                    marginBottom: 10,
                },
                lastText: {
                    width: '65%',
                    height: 25,
                }
            }
        },
        categorySlideWrap: {
            display: variable["display"].flex,
            padding: '0 10px',
            marginBottom: 20,
            justifyContent: 'space-between',
            categorySlide: {
                height: 50,
                width: 'calc(50% - 5px)'
            }
        }
    },
    viewMoreMobile: {
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
        btn: {
            display: variable["display"].block,
            color: variable["colorBlack"],
            border: "1px solid",
            paddingTop: 10,
            paddingRight: 20,
            paddingBottom: 10,
            paddingLeft: 20,
            borderRadius: 5,
            fontSize: 16
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLE1BQU0sTUFBTSx1QkFBdUIsQ0FBQztBQUNoRCxPQUFPLEtBQUssUUFBUSxNQUFNLHlCQUF5QixDQUFDO0FBRXBELGVBQWU7SUFDYixPQUFPLEVBQUUsT0FBTztJQUNoQixRQUFRLEVBQUUsVUFBVTtJQUNwQixNQUFNLEVBQUUsUUFBUSxDQUFDLE9BQU87SUFFeEIsY0FBYyxFQUFFO1FBQ2QsWUFBWSxFQUFFLEVBQUU7S0FDakI7SUFFRCx5QkFBeUIsRUFBRTtRQUN6QixNQUFNLENBQUMsY0FBYztRQUNyQixNQUFNLENBQUMsYUFBYSxDQUFDLE9BQU8sRUFBRTtZQUM1QixZQUFZLEVBQUUsTUFBTTtTQUNyQjtLQUNGO0lBRUQsYUFBYSxFQUFFO1FBQ2IsVUFBVSxFQUFFLEtBQUs7UUFDakIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtRQUVwQyxLQUFLLEVBQUU7WUFDTCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1lBQ3BDLEdBQUcsRUFBRSxDQUFDO1lBQ04sSUFBSSxFQUFFLENBQUM7WUFDUCxNQUFNLEVBQUUsTUFBTTtZQUNkLEtBQUssRUFBRSxNQUFNO1lBQ2IsU0FBUyxFQUFFLFFBQVE7WUFDbkIsU0FBUyxFQUFFLE1BQU07WUFFakIsSUFBSSxFQUFFO2dCQUNKLFVBQVUsRUFBRSxDQUFDO2dCQUNiLFlBQVksRUFBRSxDQUFDO2dCQUNmLGFBQWEsRUFBRSxDQUFDO2dCQUNoQixXQUFXLEVBQUUsQ0FBQzthQUNmO1NBQ0Y7S0FDRjtJQUVELFVBQVUsRUFBRTtRQUNWLFlBQVksRUFBRSxFQUFFO0tBQ2pCO0lBRUQsSUFBSSxFQUFFO1FBQ0osVUFBVSxFQUFFLENBQUM7UUFDYixZQUFZLEVBQUUsQ0FBQztRQUNmLGFBQWEsRUFBRSxDQUFDO1FBQ2hCLFdBQVcsRUFBRSxDQUFDO1FBRWQsSUFBSSxFQUFFO1lBQ0osWUFBWSxFQUFFLENBQUM7WUFDZixTQUFTLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFFOUIsS0FBSyxFQUFFO2dCQUNMLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7Z0JBQy9CLEtBQUssRUFBRSxNQUFNO2dCQUNiLFFBQVEsRUFBRSxNQUFNO2dCQUNoQixNQUFNLEVBQUUsTUFBTTthQUNmO1lBRUQsS0FBSyxFQUFFO2dCQUNMLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxFQUFFO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2dCQUNsQixTQUFTLEVBQUUsUUFBUTtnQkFDbkIsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO2dCQUN2QixRQUFRLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNO2dCQUNqQyxZQUFZLEVBQUUsVUFBVTtnQkFDeEIsVUFBVSxFQUFFLFFBQVE7YUFDckI7U0FDRjtLQUNGO0lBRUQsV0FBVyxFQUFFO1FBQ1gsTUFBTSxFQUFFO1lBQ04sS0FBSyxFQUFFLE1BQU07WUFDYixVQUFVLEVBQUUsS0FBSztZQUNqQixZQUFZLEVBQUUsRUFBRTtZQUNoQixTQUFTLEVBQUUsRUFBRTtTQUNkO1FBRUQsV0FBVyxFQUFFO1lBQ1gsS0FBSyxFQUFFO2dCQUNMLEtBQUssRUFBRSxHQUFHO2dCQUNWLFVBQVUsRUFBRSxFQUFFO2dCQUNkLE1BQU0sRUFBRSxFQUFFO2dCQUNWLFlBQVksRUFBRSxFQUFFO2FBQ2pCO1lBRUQsSUFBSSxFQUFFO2dCQUNKLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7YUFDL0I7WUFFRCxJQUFJLEVBQUU7Z0JBQ0osSUFBSSxFQUFFLENBQUM7Z0JBQ1AsV0FBVyxFQUFFLEVBQUU7Z0JBQ2YsWUFBWSxFQUFFLEVBQUU7Z0JBQ2hCLFlBQVksRUFBRSxFQUFFO2dCQUNoQixRQUFRLEVBQUUsS0FBSztnQkFDZixLQUFLLEVBQUUsS0FBSztnQkFFWixLQUFLLEVBQUU7b0JBQ0wsS0FBSyxFQUFFLE1BQU07b0JBQ2IsTUFBTSxFQUFFLE1BQU07b0JBQ2QsVUFBVSxFQUFFLEtBQUs7b0JBQ2pCLFlBQVksRUFBRSxFQUFFO2lCQUNqQjtnQkFFRCxJQUFJLEVBQUU7b0JBQ0osS0FBSyxFQUFFLEtBQUs7b0JBQ1osTUFBTSxFQUFFLEVBQUU7b0JBQ1YsWUFBWSxFQUFFLEVBQUU7aUJBQ2pCO2dCQUVELFFBQVEsRUFBRTtvQkFDUixLQUFLLEVBQUUsS0FBSztvQkFDWixNQUFNLEVBQUUsRUFBRTtpQkFDWDthQUNGO1NBQ0Y7UUFFRCxpQkFBaUIsRUFBRTtZQUNqQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLE9BQU8sRUFBRSxRQUFRO1lBQ2pCLFlBQVksRUFBRSxFQUFFO1lBQ2hCLGNBQWMsRUFBRSxlQUFlO1lBRS9CLGFBQWEsRUFBRTtnQkFDYixNQUFNLEVBQUUsRUFBRTtnQkFDVixLQUFLLEVBQUUsaUJBQWlCO2FBQ3pCO1NBQ0Y7S0FDRjtJQUVELGNBQWMsRUFBRTtRQUNkLFNBQVMsRUFBRSxRQUFRO1FBQ25CLFVBQVUsRUFBRSxFQUFFO1FBQ2QsV0FBVyxFQUFFLEVBQUU7UUFDZixZQUFZLEVBQUUsRUFBRTtRQUVoQixHQUFHLEVBQUU7WUFDSCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1lBQy9CLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtZQUMxQixNQUFNLEVBQUUsV0FBVztZQUNuQixVQUFVLEVBQUUsRUFBRTtZQUNkLFlBQVksRUFBRSxFQUFFO1lBQ2hCLGFBQWEsRUFBRSxFQUFFO1lBQ2pCLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLENBQUM7WUFDZixRQUFRLEVBQUUsRUFBRTtTQUNiO0tBQ0Y7Q0FDTSxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/index/view-mobile.tsx
var view_mobile_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
















var view_mobile_renderLoadingPlaceholder = function () { return (react["createElement"]("div", { style: index_style.placeholder },
    react["createElement"](loading_placeholder["a" /* default */], { style: index_style.placeholder.banner }),
    react["createElement"]("div", { style: index_style.placeholder.categorySlideWrap },
        react["createElement"](loading_placeholder["a" /* default */], { style: index_style.placeholder.categorySlideWrap.categorySlide }),
        react["createElement"](loading_placeholder["a" /* default */], { style: index_style.placeholder.categorySlideWrap.categorySlide })),
    react["createElement"]("div", { style: index_style.placeholder.productList },
        react["createElement"](loading_placeholder["a" /* default */], { style: index_style.placeholder.productList.title }),
        react["createElement"]("div", { style: index_style.placeholder.productList.list },
            react["createElement"]("div", { style: index_style.placeholder.productList.item },
                react["createElement"](loading_placeholder["a" /* default */], { style: index_style.placeholder.productList.item.image }),
                react["createElement"](loading_placeholder["a" /* default */], { style: index_style.placeholder.productList.item.text }),
                react["createElement"](loading_placeholder["a" /* default */], { style: index_style.placeholder.productList.item.text }),
                react["createElement"](loading_placeholder["a" /* default */], { style: index_style.placeholder.productList.item.lastText })),
            react["createElement"]("div", { style: index_style.placeholder.productList.item },
                react["createElement"](loading_placeholder["a" /* default */], { style: index_style.placeholder.productList.item.image }),
                react["createElement"](loading_placeholder["a" /* default */], { style: index_style.placeholder.productList.item.text }),
                react["createElement"](loading_placeholder["a" /* default */], { style: index_style.placeholder.productList.item.text }),
                react["createElement"](loading_placeholder["a" /* default */], { style: index_style.placeholder.productList.item.lastText })))))); };
var renderMobile = function (_a) {
    var props = _a.props, state = _a.state, generateCategoryHash = _a.generateCategoryHash, handleFeatureBanner = _a.handleFeatureBanner, handleFetchHotBoxes = _a.handleFetchHotBoxes, handleFetchMagazineList = _a.handleFetchMagazineList, handleFetchNewProducts = _a.handleFetchNewProducts, handleFetchPopularSearch = _a.handleFetchPopularSearch, handleFetchWatchedList = _a.handleFetchWatchedList, handleFetchActivityFeed = _a.handleFetchActivityFeed, handleFetchFooterBanner = _a.handleFetchFooterBanner;
    var bannerStore = props.bannerStore, categorySlideList = props.menuStore.categorySlideList, _b = props.shopStore, hotDeal = _b.hotDeal, productByCategory = _b.productByCategory, magazineList = props.magazineStore.magazineList, userWatchedList = props.userStore.userWatchedList;
    var isFetchedFeatureBanner = state.isFetchedFeatureBanner, isFetchedHotBoxes = state.isFetchedHotBoxes, isFetchedMagazineList = state.isFetchedMagazineList, isFetchedNewProducts = state.isFetchedNewProducts, isFetchedPopularSearch = state.isFetchedPopularSearch, isFetchedWatchedList = state.isFetchedWatchedList, isFetchedActivityFeed = state.isFetchedActivityFeed, isFetchedFooterBanner = state.isFetchedFooterBanner, isPriorotyBlock = state.isPriorotyBlock;
    // if (isEmptyObject(shopStore.dataHomePage)) {
    //   return renderLoadingPlaceholder();
    // }
    var mainBannerHash = Object(encode["j" /* objectToHash */])({ idBanner: application_default["a" /* BANNER_ID */].HOME_PAGE, limit: application_default["b" /* BANNER_LIMIT_DEFAULT */] });
    var listMainBanner = bannerStore.bannerList[mainBannerHash] || [];
    var featureBannerHash = Object(encode["j" /* objectToHash */])({ idBanner: application_default["a" /* BANNER_ID */].HOME_FEATURE, limit: application_default["b" /* BANNER_LIMIT_DEFAULT */] });
    var listFeatureBanner = bannerStore.bannerList[featureBannerHash] || [];
    var popularSearchHash = Object(encode["j" /* objectToHash */])({ idBanner: application_default["a" /* BANNER_ID */].POPULAR_SEARCH, limit: application_default["b" /* BANNER_LIMIT_DEFAULT */] });
    var popularSearchList = bannerStore.bannerList[popularSearchHash] || [];
    // const categoryBannerHash = objectToHash({ idBanner: BANNER_ID.WEB_MOBILE_HOME_CATEGORY, limit: BANNER_LIMIT_DEFAULT });
    // const listCategoryBanner = bannerStore.bannerList[categoryBannerHash] || [];
    var lastestBoxesProps = {
        title: 'SIÊU GIẢM GIÁ',
        column: 4,
        data: Array.isArray(hotDeal) ? hotDeal.slice(0, 10) : [],
        showViewMore: false,
        style: { paddingTop: 20 }
    };
    var bestSellingHash = Object(encode["j" /* objectToHash */])({ idCategory: bestSellingParams.idCategory, searchQuery: '' });
    var bestSellingList = !Object(validate["j" /* isEmptyObject */])(productByCategory)
        && !Object(validate["l" /* isUndefined */])(productByCategory[bestSellingHash])
        && !Object(validate["j" /* isEmptyObject */])(productByCategory[bestSellingHash])
        && Array.isArray(productByCategory[bestSellingHash].boxes)
        && productByCategory[bestSellingHash].boxes || [];
    var hottestBoxesProps = {
        title: 'BOX BÁN CHẠY',
        column: 4,
        data: bestSellingList.slice(0, 10),
        showViewMore: false,
        style: { paddingTop: 20 }
    };
    var newProductsHash = Object(encode["j" /* objectToHash */])({ idCategory: newProductsParams.idCategory, searchQuery: '' });
    var newProductsList = !Object(validate["j" /* isEmptyObject */])(productByCategory)
        && !Object(validate["l" /* isUndefined */])(productByCategory[newProductsHash])
        && !Object(validate["j" /* isEmptyObject */])(productByCategory[newProductsHash])
        && Array.isArray(productByCategory[newProductsHash].boxes)
        && productByCategory[newProductsHash].boxes || [];
    var latestIndividualBoxesUrl = routing["Xa" /* ROUTING_PRODUCT_CATEGORY_PATH */] + "/" + newProductsParams.idCategory;
    var latestIndividualBoxesProps = {
        title: 'MUA LẺ MỚI NHẤT',
        column: 5,
        data: newProductsList.slice(0, 10),
        viewMoreLink: latestIndividualBoxesUrl,
        style: { paddingTop: 20 }
    };
    var categorySlideProps = {
        list: MOBILE_CATEGORY_MENU || [],
        style: { paddingTop: 0, marginBottom: 0 },
        path: routing["Xa" /* ROUTING_PRODUCT_CATEGORY_PATH */]
    };
    var defaultMagazineListHash = Object(encode["j" /* objectToHash */])({ page: 1, perPage: 12, type: magazine["a" /* MAGAZINE_LIST_TYPE */].DEFAULT });
    return (react["createElement"]("shop-index-container", { style: index_style },
        react["createElement"](carousel, { list: listMainBanner }),
        react["createElement"](mobile_category, null),
        !!hotDeal && !!hotDeal.length
            && react["createElement"](slider["a" /* default */], view_mobile_assign({}, lastestBoxesProps)),
        !isPriorotyBlock
            ? (react["createElement"]("div", null,
                react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                    var isVisible = _a.isVisible;
                    !!isVisible
                        && !isFetchedFeatureBanner
                        && !!handleFeatureBanner
                        && handleFeatureBanner();
                    return react["createElement"](carousel, { list: listFeatureBanner });
                }),
                react["createElement"]("h2", { className: 'hidden-element' }, "Box b\u00E1n ch\u1EA1y | M\u1EF9 Ph\u1EA9m, D\u01B0\u1EE1ng Da, Tr\u1ECB M\u1EE5n, Skincare, Makeup"),
                react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                    var isVisible = _a.isVisible;
                    !!isVisible
                        && !isFetchedHotBoxes
                        && !!handleFetchHotBoxes
                        && handleFetchHotBoxes();
                    return react["createElement"](slider["a" /* default */], view_mobile_assign({}, hottestBoxesProps));
                }),
                react["createElement"]("h2", { className: 'hidden-element' }, "T\u1EEB kh\u00F3a t\u00ECm ki\u1EBFm ph\u1ED5 bi\u1EBFn nh\u1EA5t | M\u1EF9 Ph\u1EA9m, D\u01B0\u1EE1ng Da, Tr\u1ECB M\u1EE5n, Skincare, Makeup"),
                react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                    var isVisible = _a.isVisible;
                    !!isVisible
                        && !isFetchedPopularSearch
                        && !!handleFetchPopularSearch
                        && handleFetchPopularSearch();
                    return react["createElement"](search_popular, { list: popularSearchList });
                }),
                react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                    var isVisible = _a.isVisible;
                    !!isVisible
                        && !isFetchedWatchedList
                        && !!handleFetchWatchedList
                        && handleFetchWatchedList();
                    var watchedListHash = Object(encode["j" /* objectToHash */])({ page: 1, perPage: 10 });
                    var watchedList = userWatchedList[watchedListHash] && userWatchedList[watchedListHash].recently_viewed_boxes || [];
                    var watchedListProps = {
                        title: 'DANH SÁCH ĐÃ XEM',
                        column: 5,
                        data: watchedList,
                        showViewMore: false,
                    };
                    return 0 !== watchedList.length && react["createElement"](slider["a" /* default */], view_mobile_assign({}, watchedListProps));
                }),
                react["createElement"]("h2", { className: 'hidden-element' }, "S\u1EA3n ph\u1EA9m m\u1EDBi nh\u1EA5t | M\u1EF9 Ph\u1EA9m, D\u01B0\u1EE1ng Da, Tr\u1ECB M\u1EE5n, Skincare, Makeup"),
                react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                    var isVisible = _a.isVisible;
                    !!isVisible
                        && !isFetchedNewProducts
                        && !!handleFetchNewProducts
                        && handleFetchNewProducts();
                    return react["createElement"](slider["a" /* default */], view_mobile_assign({}, latestIndividualBoxesProps));
                }))) : react["createElement"](loading["a" /* default */], { style: { height: 400 } }),
        react["createElement"]("div", { style: index_style.viewMoreMobile },
            react["createElement"](react_router_dom["NavLink"], { to: latestIndividualBoxesUrl, style: index_style.viewMoreMobile.btn }, "Xem th\u00EAm s\u1EA3n ph\u1EA9m m\u1EDBi nh\u1EA5t"))));
};
/* harmony default export */ var view_mobile = (renderMobile);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1tb2JpbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LW1vYmlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUUvQixPQUFPLGFBQWEsTUFBTSxvQ0FBb0MsQ0FBQztBQUMvRCxPQUFPLGFBQWEsTUFBTSw0Q0FBNEMsQ0FBQztBQUN2RSxPQUFPLGNBQWMsTUFBTSxxQ0FBcUMsQ0FBQztBQUNqRSxPQUFPLGtCQUFrQixNQUFNLDRDQUE0QyxDQUFDO0FBQzVFLE9BQU8sY0FBYyxNQUFNLCtDQUErQyxDQUFDO0FBRTNFLE9BQU8sZUFBZSxNQUFNLGlCQUFpQixDQUFDO0FBQzlDLE9BQU8sT0FBTyxNQUFNLGdDQUFnQyxDQUFDO0FBRXJELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxTQUFTLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUN6RixPQUFPLEVBQUUsNkJBQTZCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUN2RixPQUFPLEVBQUUsYUFBYSxFQUFFLFdBQVcsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ3JFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUVyRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsaUJBQWlCLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDMUYsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQzNDLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixJQUFNLHdCQUF3QixHQUFHLGNBQU0sT0FBQSxDQUNyQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVc7SUFDM0Isb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFJO0lBQ3ZELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLGlCQUFpQjtRQUM3QyxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEdBQUk7UUFDaEYsb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsYUFBYSxHQUFJLENBQzVFO0lBQ04sNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVztRQUN2QyxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsS0FBSyxHQUFJO1FBQ2xFLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJO1lBQzVDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJO2dCQUM1QyxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBSTtnQkFDdkUsb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUk7Z0JBQ3RFLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFJO2dCQUN0RSxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBSSxDQUN0RTtZQUNOLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJO2dCQUM1QyxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBSTtnQkFDdkUsb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUk7Z0JBQ3RFLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFJO2dCQUN0RSxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBSSxDQUN0RSxDQUNGLENBQ0YsQ0FDRixDQUNQLEVBekJzQyxDQXlCdEMsQ0FBQTtBQUVELElBQU0sWUFBWSxHQUFHLFVBQUMsRUFZckI7UUFYQyxnQkFBSyxFQUNMLGdCQUFLLEVBQ0wsOENBQW9CLEVBQ3BCLDRDQUFtQixFQUNuQiw0Q0FBbUIsRUFDbkIsb0RBQXVCLEVBQ3ZCLGtEQUFzQixFQUN0QixzREFBd0IsRUFDeEIsa0RBQXNCLEVBQ3RCLG9EQUF1QixFQUN2QixvREFBdUI7SUFHckIsSUFBQSwrQkFBVyxFQUNFLHFEQUFpQixFQUM5QixvQkFBeUMsRUFBNUIsb0JBQU8sRUFBRSx3Q0FBaUIsRUFDdEIsK0NBQVksRUFDaEIsaURBQWUsQ0FDcEI7SUFHUixJQUFBLHFEQUFzQixFQUN0QiwyQ0FBaUIsRUFDakIsbURBQXFCLEVBQ3JCLGlEQUFvQixFQUNwQixxREFBc0IsRUFDdEIsaURBQW9CLEVBQ3BCLG1EQUFxQixFQUNyQixtREFBcUIsRUFDckIsdUNBQWUsQ0FDUDtJQUVWLCtDQUErQztJQUMvQyx1Q0FBdUM7SUFDdkMsSUFBSTtJQUVKLElBQU0sY0FBYyxHQUFHLFlBQVksQ0FBQyxFQUFFLFFBQVEsRUFBRSxTQUFTLENBQUMsU0FBUyxFQUFFLEtBQUssRUFBRSxvQkFBb0IsRUFBRSxDQUFDLENBQUM7SUFDcEcsSUFBTSxjQUFjLEdBQUcsV0FBVyxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLENBQUM7SUFFcEUsSUFBTSxpQkFBaUIsR0FBRyxZQUFZLENBQUMsRUFBRSxRQUFRLEVBQUUsU0FBUyxDQUFDLFlBQVksRUFBRSxLQUFLLEVBQUUsb0JBQW9CLEVBQUUsQ0FBQyxDQUFDO0lBQzFHLElBQU0saUJBQWlCLEdBQUcsV0FBVyxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUUxRSxJQUFNLGlCQUFpQixHQUFHLFlBQVksQ0FBQyxFQUFFLFFBQVEsRUFBRSxTQUFTLENBQUMsY0FBYyxFQUFFLEtBQUssRUFBRSxvQkFBb0IsRUFBRSxDQUFDLENBQUM7SUFDNUcsSUFBTSxpQkFBaUIsR0FBRyxXQUFXLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxDQUFDO0lBRTFFLDBIQUEwSDtJQUMxSCwrRUFBK0U7SUFFL0UsSUFBTSxpQkFBaUIsR0FBRztRQUN4QixLQUFLLEVBQUUsZUFBZTtRQUN0QixNQUFNLEVBQUUsQ0FBQztRQUNULElBQUksRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtRQUN4RCxZQUFZLEVBQUUsS0FBSztRQUNuQixLQUFLLEVBQUUsRUFBRSxVQUFVLEVBQUUsRUFBRSxFQUFFO0tBQzFCLENBQUM7SUFFRixJQUFNLGVBQWUsR0FBRyxZQUFZLENBQUMsRUFBRSxVQUFVLEVBQUUsaUJBQWlCLENBQUMsVUFBVSxFQUFFLFdBQVcsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ3BHLElBQU0sZUFBZSxHQUFHLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDO1dBQ3BELENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1dBQ2hELENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1dBQ2xELEtBQUssQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLENBQUMsS0FBSyxDQUFDO1dBQ3ZELGlCQUFpQixDQUFDLGVBQWUsQ0FBQyxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUM7SUFFcEQsSUFBTSxpQkFBaUIsR0FBRztRQUN4QixLQUFLLEVBQUUsY0FBYztRQUNyQixNQUFNLEVBQUUsQ0FBQztRQUNULElBQUksRUFBRSxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUM7UUFDbEMsWUFBWSxFQUFFLEtBQUs7UUFDbkIsS0FBSyxFQUFFLEVBQUUsVUFBVSxFQUFFLEVBQUUsRUFBRTtLQUMxQixDQUFDO0lBRUYsSUFBTSxlQUFlLEdBQUcsWUFBWSxDQUFDLEVBQUUsVUFBVSxFQUFFLGlCQUFpQixDQUFDLFVBQVUsRUFBRSxXQUFXLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNwRyxJQUFNLGVBQWUsR0FBRyxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQztXQUNwRCxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlLENBQUMsQ0FBQztXQUNoRCxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlLENBQUMsQ0FBQztXQUNsRCxLQUFLLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLGVBQWUsQ0FBQyxDQUFDLEtBQUssQ0FBQztXQUN2RCxpQkFBaUIsQ0FBQyxlQUFlLENBQUMsQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDO0lBRXBELElBQU0sd0JBQXdCLEdBQU0sNkJBQTZCLFNBQUksaUJBQWlCLENBQUMsVUFBWSxDQUFDO0lBQ3BHLElBQU0sMEJBQTBCLEdBQUc7UUFDakMsS0FBSyxFQUFFLGlCQUFpQjtRQUN4QixNQUFNLEVBQUUsQ0FBQztRQUNULElBQUksRUFBRSxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUM7UUFDbEMsWUFBWSxFQUFFLHdCQUF3QjtRQUN0QyxLQUFLLEVBQUUsRUFBRSxVQUFVLEVBQUUsRUFBRSxFQUFFO0tBQzFCLENBQUM7SUFFRixJQUFNLGtCQUFrQixHQUFHO1FBQ3pCLElBQUksRUFBRSxvQkFBb0IsSUFBSSxFQUFFO1FBQ2hDLEtBQUssRUFBRSxFQUFFLFVBQVUsRUFBRSxDQUFDLEVBQUUsWUFBWSxFQUFFLENBQUMsRUFBRTtRQUN6QyxJQUFJLEVBQUUsNkJBQTZCO0tBQ3BDLENBQUE7SUFFRCxJQUFNLHVCQUF1QixHQUFHLFlBQVksQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsa0JBQWtCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUV6RyxNQUFNLENBQUMsQ0FDTCw4Q0FBc0IsS0FBSyxFQUFFLEtBQUs7UUFDaEMsb0JBQUMsY0FBYyxJQUFDLElBQUksRUFBRSxjQUFjLEdBQUk7UUFDeEMsb0JBQUMsY0FBYyxPQUFHO1FBRWhCLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFNO2VBQzFCLG9CQUFDLGFBQWEsZUFBSyxpQkFBaUIsRUFBSTtRQUkzQyxDQUFDLGVBQWU7WUFDZCxDQUFDLENBQUMsQ0FDQTtnQkFDRSxvQkFBQyxlQUFlLElBQUMsTUFBTSxFQUFFLEdBQUcsSUFFeEIsVUFBQyxFQUFhO3dCQUFYLHdCQUFTO29CQUNWLENBQUMsQ0FBQyxTQUFTOzJCQUNOLENBQUMsc0JBQXNCOzJCQUN2QixDQUFDLENBQUMsbUJBQW1COzJCQUNyQixtQkFBbUIsRUFBRSxDQUFDO29CQUUzQixNQUFNLENBQUMsb0JBQUMsY0FBYyxJQUFDLElBQUksRUFBRSxpQkFBaUIsR0FBSSxDQUFDO2dCQUNyRCxDQUFDLENBRWE7Z0JBRWxCLDRCQUFJLFNBQVMsRUFBRSxnQkFBZ0IsMEdBQWtFO2dCQUNqRyxvQkFBQyxlQUFlLElBQUMsTUFBTSxFQUFFLEdBQUcsSUFFeEIsVUFBQyxFQUFhO3dCQUFYLHdCQUFTO29CQUNWLENBQUMsQ0FBQyxTQUFTOzJCQUNOLENBQUMsaUJBQWlCOzJCQUNsQixDQUFDLENBQUMsbUJBQW1COzJCQUNyQixtQkFBbUIsRUFBRSxDQUFDO29CQUUzQixNQUFNLENBQUMsb0JBQUMsYUFBYSxlQUFLLGlCQUFpQixFQUFJLENBQUM7Z0JBQ2xELENBQUMsQ0FFYTtnQkF1QmxCLDRCQUFJLFNBQVMsRUFBRSxnQkFBZ0IscUpBQW9GO2dCQUNuSCxvQkFBQyxlQUFlLElBQUMsTUFBTSxFQUFFLEdBQUcsSUFFeEIsVUFBQyxFQUFhO3dCQUFYLHdCQUFTO29CQUNWLENBQUMsQ0FBQyxTQUFTOzJCQUNOLENBQUMsc0JBQXNCOzJCQUN2QixDQUFDLENBQUMsd0JBQXdCOzJCQUMxQix3QkFBd0IsRUFBRSxDQUFDO29CQUVoQyxNQUFNLENBQUMsb0JBQUMsYUFBYSxJQUFDLElBQUksRUFBRSxpQkFBaUIsR0FBSSxDQUFDO2dCQUNwRCxDQUFDLENBRWE7Z0JBRWxCLG9CQUFDLGVBQWUsSUFBQyxNQUFNLEVBQUUsR0FBRyxJQUV4QixVQUFDLEVBQWE7d0JBQVgsd0JBQVM7b0JBQ1YsQ0FBQyxDQUFDLFNBQVM7MkJBQ04sQ0FBQyxvQkFBb0I7MkJBQ3JCLENBQUMsQ0FBQyxzQkFBc0I7MkJBQ3hCLHNCQUFzQixFQUFFLENBQUM7b0JBRTlCLElBQU0sZUFBZSxHQUFHLFlBQVksQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7b0JBQy9ELElBQU0sV0FBVyxHQUFHLGVBQWUsQ0FBQyxlQUFlLENBQUMsSUFBSSxlQUFlLENBQUMsZUFBZSxDQUFDLENBQUMscUJBQXFCLElBQUksRUFBRSxDQUFDO29CQUNySCxJQUFNLGdCQUFnQixHQUFHO3dCQUN2QixLQUFLLEVBQUUsa0JBQWtCO3dCQUN6QixNQUFNLEVBQUUsQ0FBQzt3QkFDVCxJQUFJLEVBQUUsV0FBVzt3QkFDakIsWUFBWSxFQUFFLEtBQUs7cUJBQ3BCLENBQUM7b0JBRUYsTUFBTSxDQUFDLENBQUMsS0FBSyxXQUFXLENBQUMsTUFBTSxJQUFJLG9CQUFDLGFBQWEsZUFBSyxnQkFBZ0IsRUFBSSxDQUFDO2dCQUM3RSxDQUFDLENBRWE7Z0JBRWxCLDRCQUFJLFNBQVMsRUFBRSxnQkFBZ0IseUhBQXVFO2dCQUN0RyxvQkFBQyxlQUFlLElBQUMsTUFBTSxFQUFFLEdBQUcsSUFFeEIsVUFBQyxFQUFhO3dCQUFYLHdCQUFTO29CQUNWLENBQUMsQ0FBQyxTQUFTOzJCQUNOLENBQUMsb0JBQW9COzJCQUNyQixDQUFDLENBQUMsc0JBQXNCOzJCQUN4QixzQkFBc0IsRUFBRSxDQUFDO29CQUU5QixNQUFNLENBQUMsb0JBQUMsYUFBYSxlQUFLLDBCQUEwQixFQUFJLENBQUM7Z0JBQzNELENBQUMsQ0FFYSxDQUNkLENBQ1AsQ0FBQyxDQUFDLENBQUMsb0JBQUMsT0FBTyxJQUFDLEtBQUssRUFBRSxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsR0FBSTtRQUczQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGNBQWM7WUFDOUIsb0JBQUMsT0FBTyxJQUFDLEVBQUUsRUFBRSx3QkFBd0IsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLGNBQWMsQ0FBQyxHQUFHLDBEQUFzQyxDQUN4RyxDQUNlLENBQ3hCLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLFlBQVksQ0FBQyJ9
// EXTERNAL MODULE: ./container/layout/wrap/index.tsx
var wrap = __webpack_require__(745);

// EXTERNAL MODULE: ./components/magazine/image-slider/index.tsx + 11 modules
var image_slider = __webpack_require__(765);

// EXTERNAL MODULE: ./components/testimonial/index.tsx + 4 modules
var testimonial = __webpack_require__(828);

// EXTERNAL MODULE: ./utils/image.ts
var utils_image = __webpack_require__(348);

// CONCATENATED MODULE: ./components/banner/home-main/initialize.tsx
var home_main_initialize_DEFAULT_PROPS = { list: [] };
var home_main_initialize_INITIAL_STATE = {
    selected: {
        id: 0,
        name: '',
        order: null,
        link: '',
        cover_image: {
            large_url: ''
        }
    },
    countChangeSlide: -1,
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQVksQ0FBQztBQUNwRCxNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUc7SUFDM0IsUUFBUSxFQUFFO1FBQ1IsRUFBRSxFQUFFLENBQUM7UUFDTCxJQUFJLEVBQUUsRUFBRTtRQUNSLEtBQUssRUFBRSxJQUFJO1FBQ1gsSUFBSSxFQUFFLEVBQUU7UUFDUixXQUFXLEVBQUU7WUFDWCxTQUFTLEVBQUUsRUFBRTtTQUNkO0tBQ0Y7SUFDRCxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7Q0FDWCxDQUFDIn0=
// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// CONCATENATED MODULE: ./components/banner/home-main/style.tsx




var style_INLINE_STYLE = {
    '.banner-main-container .pagination': {
        opacity: 0,
        transform: 'translate3d(0, 20px, 0)',
    },
    '.banner-main-container:hover .pagination': {
        opacity: 1,
        transform: 'translate3d(0, 0, 0)',
    },
    '.banner-main-container .left-nav': {
        width: 60,
        height: 90,
        opacity: 0,
        transform: 'translate3d(-60px, -50%, 0)',
        visibility: 'hidden',
    },
    '.banner-main-container:hover .left-nav': {
        opacity: 1,
        transform: 'translate3d(0, -50%, 0)',
        visibility: 'visible',
    },
    '.banner-main-container .right-nav': {
        width: 60,
        height: 90,
        opacity: 0,
        transform: 'translate3d(60px, -50%, 0)',
        visibility: 'hidden',
    },
    '.banner-main-container:hover .right-nav': {
        opacity: 1,
        transform: 'translate3d(0, -50%, 0)',
        visibility: 'visible',
    }
};
/* harmony default export */ var home_main_style = ({
    container: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{ paddingTop: '41.66%', }],
        DESKTOP: [{}],
        GENERAL: [{
                display: 'block',
                width: '100%',
                paddingTop: '41.66%',
                position: 'relative',
                overflow: 'hidden',
                background: variable["colorFA"],
            }]
    }),
    navigationStyle: [
        layout["a" /* flexContainer */].center,
        layout["a" /* flexContainer */].verticalCenter,
        component["e" /* slideNavigation */],
    ],
    default: {
        background: variable["colorFA"]
    },
    item: {
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: '0',
        left: '0',
        transition: variable["transitionOpacity"],
        cursor: 'pointer',
        opacity: 1
    },
    paginationStyle: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{ opacity: 1, transform: "transform: 'translate3d(0, 0, 0)'," }],
        DESKTOP: [{}],
        GENERAL: [
            layout["a" /* flexContainer */].center,
            component["f" /* slidePagination */],
            {
                transition: variable["transitionNormal"],
            }
        ]
    })
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRXpELE9BQU8sS0FBSyxNQUFNLE1BQU0sdUJBQXVCLENBQUM7QUFDaEQsT0FBTyxLQUFLLFNBQVMsTUFBTSwwQkFBMEIsQ0FBQztBQUN0RCxPQUFPLEtBQUssUUFBUSxNQUFNLHlCQUF5QixDQUFDO0FBRXBELE1BQU0sQ0FBQyxJQUFNLFlBQVksR0FBRztJQUMxQixvQ0FBb0MsRUFBRTtRQUNwQyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSx5QkFBeUI7S0FDckM7SUFFRCwwQ0FBMEMsRUFBRTtRQUMxQyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxzQkFBc0I7S0FDbEM7SUFFRCxrQ0FBa0MsRUFBRTtRQUNsQyxLQUFLLEVBQUUsRUFBRTtRQUNULE1BQU0sRUFBRSxFQUFFO1FBQ1YsT0FBTyxFQUFFLENBQUM7UUFDVixTQUFTLEVBQUUsNkJBQTZCO1FBQ3hDLFVBQVUsRUFBRSxRQUFRO0tBQ3JCO0lBRUQsd0NBQXdDLEVBQUU7UUFDeEMsT0FBTyxFQUFFLENBQUM7UUFDVixTQUFTLEVBQUUseUJBQXlCO1FBQ3BDLFVBQVUsRUFBRSxTQUFTO0tBQ3RCO0lBRUQsbUNBQW1DLEVBQUU7UUFDbkMsS0FBSyxFQUFFLEVBQUU7UUFDVCxNQUFNLEVBQUUsRUFBRTtRQUNWLE9BQU8sRUFBRSxDQUFDO1FBQ1YsU0FBUyxFQUFFLDRCQUE0QjtRQUN2QyxVQUFVLEVBQUUsUUFBUTtLQUNyQjtJQUVELHlDQUF5QyxFQUFFO1FBQ3pDLE9BQU8sRUFBRSxDQUFDO1FBQ1YsU0FBUyxFQUFFLHlCQUF5QjtRQUNwQyxVQUFVLEVBQUUsU0FBUztLQUN0QjtDQUNGLENBQUM7QUFFRixlQUFlO0lBQ2IsU0FBUyxFQUFFLFlBQVksQ0FBQztRQUN0QixNQUFNLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxRQUFRLEdBQUcsQ0FBQztRQUVuQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUM7UUFFYixPQUFPLEVBQUUsQ0FBQztnQkFDUixPQUFPLEVBQUUsT0FBTztnQkFDaEIsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsVUFBVSxFQUFFLFFBQVE7Z0JBQ3BCLFFBQVEsRUFBRSxVQUFVO2dCQUNwQixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxPQUFPO2FBQzdCLENBQUM7S0FDSCxDQUFDO0lBRUYsZUFBZSxFQUFFO1FBQ2YsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNO1FBQzNCLE1BQU0sQ0FBQyxhQUFhLENBQUMsY0FBYztRQUNuQyxTQUFTLENBQUMsZUFBZTtLQUMxQjtJQUVELE9BQU8sRUFBRTtRQUNQLFVBQVUsRUFBRSxRQUFRLENBQUMsT0FBTztLQUM3QjtJQUVELElBQUksRUFBRTtRQUNKLGtCQUFrQixFQUFFLFFBQVE7UUFDNUIsY0FBYyxFQUFFLE9BQU87UUFDdkIsS0FBSyxFQUFFLE1BQU07UUFDYixNQUFNLEVBQUUsTUFBTTtRQUNkLFFBQVEsRUFBRSxVQUFVO1FBQ3BCLEdBQUcsRUFBRSxHQUFHO1FBQ1IsSUFBSSxFQUFFLEdBQUc7UUFDVCxVQUFVLEVBQUUsUUFBUSxDQUFDLGlCQUFpQjtRQUN0QyxNQUFNLEVBQUUsU0FBUztRQUNqQixPQUFPLEVBQUUsQ0FBQztLQUNYO0lBRUQsZUFBZSxFQUFFLFlBQVksQ0FBQztRQUM1QixNQUFNLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUUsU0FBUyxFQUFFLG9DQUFvQyxFQUFFLENBQUM7UUFDekUsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDO1FBRWIsT0FBTyxFQUFFO1lBQ1AsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNO1lBQzNCLFNBQVMsQ0FBQyxlQUFlO1lBQ3pCO2dCQUNFLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO2FBQ3RDO1NBQ0Y7S0FDRixDQUFDO0NBQ0ssQ0FBQyJ9
// CONCATENATED MODULE: ./components/banner/home-main/view.tsx
var home_main_view_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};








;
var view_renderNavLink = function (link) {
    var navLinkProps = {
        style: {
            display: 'block',
            height: '100%',
            flex: 1
        },
        to: link
    };
    return react["createElement"](react_router_dom["NavLink"], home_main_view_assign({}, navLinkProps));
};
var renderImage = function (_a) {
    var selected = _a.selected;
    var props = {
        to: Object(validate["f" /* getNavLink */])(selected.link),
        style: Object.assign({}, home_main_style.item, selected.cover_image && { backgroundImage: "url(" + (selected.cover_image.original_url || '') + ")" }, { backgroundColor: selected.dominant_color || variable["colorF7"] })
    };
    if (!!selected.links && selected.links.length >= 1) {
        return (react["createElement"]("div", { style: Object.assign({}, home_main_style.item, selected.cover_image && { backgroundImage: "url(" + (selected.cover_image.original_url || '') + ")" }, { backgroundColor: selected.dominant_color || variable["colorF7"], display: 'flex' }) },
            view_renderNavLink(Object(validate["f" /* getNavLink */])(selected.link)),
            selected.links[0] && view_renderNavLink(Object(validate["f" /* getNavLink */])(selected.links[0])),
            selected.links[1] && view_renderNavLink(Object(validate["f" /* getNavLink */])(selected.links[1]))));
    }
    return react["createElement"](react_router_dom["NavLink"], home_main_view_assign({}, props));
};
var renderPagination = function (_a) {
    var list = _a.list, selectSlide = _a.selectSlide, countChangeSlide = _a.countChangeSlide;
    if (list.length <= 1) {
        return null;
    }
    var generateItemProps = function (item, $index) { return ({
        key: "banner-main-home-" + item.id,
        onClick: function () { return selectSlide(list, $index); },
        style: [
            component["f" /* slidePagination */].item,
            $index === countChangeSlide && component["f" /* slidePagination */].itemActive
        ]
    }); };
    return (react["createElement"]("div", { style: home_main_style.paginationStyle, className: 'pagination' }, Array.isArray(list)
        && list.map(function (item, $index) {
            var itemProps = generateItemProps(item, $index);
            return react["createElement"]("div", home_main_view_assign({}, itemProps));
        })));
};
var renderNavigation = function (_a) {
    var list = _a.list, navSlide = _a.navSlide;
    if (list.length <= 1) {
        return null;
    }
    var generateItemProps = function (type) { return ({
        className: type + "-nav",
        onClick: function () { return navSlide(type); },
        style: home_main_style.navigationStyle.concat([component["e" /* slideNavigation */][type]])
    }); };
    var leftItemProps = generateItemProps('left');
    var rightItemProps = generateItemProps('right');
    var generateIconProps = function (type) { return ({
        name: "angle-" + type,
        style: component["e" /* slideNavigation */].icon,
    }); };
    var leftIconProps = generateIconProps('left');
    var rightIconProps = generateIconProps('right');
    return (react["createElement"]("div", null,
        react["createElement"]("div", home_main_view_assign({}, leftItemProps),
            react["createElement"](icon["a" /* default */], home_main_view_assign({}, leftIconProps))),
        react["createElement"]("div", home_main_view_assign({}, rightItemProps),
            react["createElement"](icon["a" /* default */], home_main_view_assign({}, rightIconProps)))));
};
var home_main_view_renderView = function (_a) {
    var props = _a.props, state = _a.state, handleTouchStart = _a.handleTouchStart, handleTouchMove = _a.handleTouchMove, selectSlide = _a.selectSlide, navSlide = _a.navSlide;
    var _b = props.list, list = _b === void 0 ? [] : _b, style = props.style;
    var selected = state.selected, countChangeSlide = state.countChangeSlide;
    var containerProps = {
        style: [home_main_style.container, style],
        onTouchStart: function (e) { return handleTouchStart(e); },
        onTouchMove: function (e) { return handleTouchMove(e); },
        class: 'banner-main-container'
    };
    return (0 === list.length || -1 === countChangeSlide
        ? react["createElement"]("banner-home-main", { style: [home_main_style.container, style] })
        : (react["createElement"]("banner-home-main", home_main_view_assign({}, containerProps),
            renderImage({ selected: selected }),
            renderPagination({ list: list, selectSlide: selectSlide, countChangeSlide: countChangeSlide }),
            renderNavigation({ list: list, navSlide: navSlide }),
            react["createElement"](radium["Style"], { rules: style_INLINE_STYLE }))));
};
/* harmony default export */ var home_main_view = (home_main_view_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUMvQixPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFFM0MsT0FBTyxJQUFJLE1BQU0sZUFBZSxDQUFDO0FBQ2pDLE9BQU8sS0FBSyxTQUFTLE1BQU0sMEJBQTBCLENBQUM7QUFDdEQsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFHckQsT0FBTyxLQUFLLEVBQUUsRUFBRSxZQUFZLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFTN0MsQ0FBQztBQUVGLElBQU0sYUFBYSxHQUFHLFVBQUMsSUFBSTtJQUN6QixJQUFNLFlBQVksR0FBRztRQUNuQixLQUFLLEVBQUU7WUFDTCxPQUFPLEVBQUUsT0FBTztZQUNoQixNQUFNLEVBQUUsTUFBTTtZQUNkLElBQUksRUFBRSxDQUFDO1NBQ1I7UUFDRCxFQUFFLEVBQUUsSUFBSTtLQUNULENBQUM7SUFFRixNQUFNLENBQUMsb0JBQUMsT0FBTyxlQUFLLFlBQVksRUFBSSxDQUFDO0FBQ3ZDLENBQUMsQ0FBQTtBQUVELElBQU0sV0FBVyxHQUFHLFVBQUMsRUFBWTtRQUFWLHNCQUFRO0lBQzdCLElBQU0sS0FBSyxHQUFHO1FBQ1osRUFBRSxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO1FBQzdCLEtBQUssRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFDckIsS0FBSyxDQUFDLElBQUksRUFDVixRQUFRLENBQUMsV0FBVyxJQUFJLEVBQUUsZUFBZSxFQUFFLFVBQU8sUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLElBQUksRUFBRSxPQUFHLEVBQUUsRUFDOUYsRUFBRSxlQUFlLEVBQUUsUUFBUSxDQUFDLGNBQWMsSUFBSSxRQUFRLENBQUMsT0FBTyxFQUFFLENBQ2pFO0tBQ0YsQ0FBQztJQUVGLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbkQsTUFBTSxDQUFDLENBQ0wsNkJBQUssS0FBSyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUMxQixLQUFLLENBQUMsSUFBSSxFQUNWLFFBQVEsQ0FBQyxXQUFXLElBQUksRUFBRSxlQUFlLEVBQUUsVUFBTyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksSUFBSSxFQUFFLE9BQUcsRUFBRSxFQUM5RixFQUFFLGVBQWUsRUFBRSxRQUFRLENBQUMsY0FBYyxJQUFJLFFBQVEsQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxDQUNsRjtZQUNFLGFBQWEsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3hDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksYUFBYSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDakUsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxhQUFhLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUM5RCxDQUNQLENBQUE7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLG9CQUFDLE9BQU8sZUFBSyxLQUFLLEVBQUksQ0FBQztBQUNoQyxDQUFDLENBQUE7QUFFRCxJQUFNLGdCQUFnQixHQUFHLFVBQUMsRUFBdUM7UUFBckMsY0FBSSxFQUFFLDRCQUFXLEVBQUUsc0NBQWdCO0lBQzdELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUE7SUFBQyxDQUFDO0lBRXJDLElBQU0saUJBQWlCLEdBQUcsVUFBQyxJQUFJLEVBQUUsTUFBTSxJQUFLLE9BQUEsQ0FBQztRQUMzQyxHQUFHLEVBQUUsc0JBQW9CLElBQUksQ0FBQyxFQUFJO1FBQ2xDLE9BQU8sRUFBRSxjQUFNLE9BQUEsV0FBVyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsRUFBekIsQ0FBeUI7UUFDeEMsS0FBSyxFQUFFO1lBQ0wsU0FBUyxDQUFDLGVBQWUsQ0FBQyxJQUFJO1lBQzlCLE1BQU0sS0FBSyxnQkFBZ0IsSUFBSSxTQUFTLENBQUMsZUFBZSxDQUFDLFVBQVU7U0FDcEU7S0FDRixDQUFDLEVBUDBDLENBTzFDLENBQUM7SUFFSCxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGVBQWUsRUFBRSxTQUFTLEVBQUUsWUFBWSxJQUV0RCxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztXQUNoQixJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBSSxFQUFFLE1BQU07WUFDdkIsSUFBTSxTQUFTLEdBQUcsaUJBQWlCLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ2xELE1BQU0sQ0FBQyx3Q0FBUyxTQUFTLEVBQVEsQ0FBQztRQUNwQyxDQUFDLENBQUMsQ0FFQSxDQUNQLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixJQUFNLGdCQUFnQixHQUFHLFVBQUMsRUFBa0I7UUFBaEIsY0FBSSxFQUFFLHNCQUFRO0lBQ3hDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUE7SUFBQyxDQUFDO0lBRXJDLElBQU0saUJBQWlCLEdBQUcsVUFBQyxJQUFzQixJQUFLLE9BQUEsQ0FBQztRQUNyRCxTQUFTLEVBQUssSUFBSSxTQUFNO1FBQ3hCLE9BQU8sRUFBRSxjQUFNLE9BQUEsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFkLENBQWM7UUFDN0IsS0FBSyxFQUFNLEtBQUssQ0FBQyxlQUFlLFNBQUUsU0FBUyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBQztLQUNuRSxDQUFDLEVBSm9ELENBSXBELENBQUM7SUFFSCxJQUFNLGFBQWEsR0FBRyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNoRCxJQUFNLGNBQWMsR0FBRyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUVsRCxJQUFNLGlCQUFpQixHQUFHLFVBQUMsSUFBc0IsSUFBSyxPQUFBLENBQUM7UUFDckQsSUFBSSxFQUFFLFdBQVMsSUFBTTtRQUNyQixLQUFLLEVBQUUsU0FBUyxDQUFDLGVBQWUsQ0FBQyxJQUFJO0tBQ3RDLENBQUMsRUFIb0QsQ0FHcEQsQ0FBQztJQUVILElBQU0sYUFBYSxHQUFHLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELElBQU0sY0FBYyxHQUFHLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBRWxELE1BQU0sQ0FBQyxDQUNMO1FBQ0Usd0NBQVMsYUFBYTtZQUFFLG9CQUFDLElBQUksZUFBSyxhQUFhLEVBQUksQ0FBTTtRQUN6RCx3Q0FBUyxjQUFjO1lBQUUsb0JBQUMsSUFBSSxlQUFLLGNBQWMsRUFBSSxDQUFNLENBQ3ZELENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLElBQU0sVUFBVSxHQUFHLFVBQUMsRUFPRDtRQU5qQixnQkFBSyxFQUNMLGdCQUFLLEVBQ0wsc0NBQWdCLEVBQ2hCLG9DQUFlLEVBQ2YsNEJBQVcsRUFDWCxzQkFBUTtJQUlBLElBQUEsZUFBUyxFQUFULDhCQUFTLEVBQUUsbUJBQUssQ0FBVztJQUMzQixJQUFBLHlCQUFRLEVBQUUseUNBQWdCLENBQVc7SUFFN0MsSUFBTSxjQUFjLEdBQUc7UUFDckIsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUM7UUFDL0IsWUFBWSxFQUFFLFVBQUMsQ0FBQyxJQUFLLE9BQUEsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLEVBQW5CLENBQW1CO1FBQ3hDLFdBQVcsRUFBRSxVQUFDLENBQUMsSUFBSyxPQUFBLGVBQWUsQ0FBQyxDQUFDLENBQUMsRUFBbEIsQ0FBa0I7UUFDdEMsS0FBSyxFQUFFLHVCQUF1QjtLQUMvQixDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsQ0FBQyxLQUFLLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLEtBQUssZ0JBQWdCO1FBQzFDLENBQUMsQ0FBQywwQ0FBa0IsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsR0FBcUI7UUFDeEUsQ0FBQyxDQUFDLENBQ0EscURBQXNCLGNBQWM7WUFDakMsV0FBVyxDQUFDLEVBQUUsUUFBUSxVQUFBLEVBQUUsQ0FBQztZQUN6QixnQkFBZ0IsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLFdBQVcsYUFBQSxFQUFFLGdCQUFnQixrQkFBQSxFQUFFLENBQUM7WUFDekQsZ0JBQWdCLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxRQUFRLFVBQUEsRUFBRSxDQUFDO1lBQ3JDLG9CQUFDLEtBQUssSUFBQyxLQUFLLEVBQUUsWUFBWSxHQUFJLENBQ2IsQ0FDcEIsQ0FDSixDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// CONCATENATED MODULE: ./components/banner/home-main/component.tsx
var home_main_component_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var home_main_component_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var component_BannerHomeMain = /** @class */ (function (_super) {
    home_main_component_extends(BannerHomeMain, _super);
    function BannerHomeMain(props) {
        var _this = _super.call(this, props) || this;
        _this.xDown = null;
        _this.yDown = null;
        _this.timerChangeSlide = null;
        _this.state = home_main_initialize_INITIAL_STATE;
        return _this;
    }
    /** Stat animation with current props */
    BannerHomeMain.prototype.componentDidMount = function () {
        this.startAnimation();
    };
    /** Start animation with new props */
    BannerHomeMain.prototype.componentWillReceiveProps = function (nextProps) {
        this.startAnimation(nextProps);
    };
    /**
     * Start animation for banner
     *
     * @param {IBannerState} _props receive current props or nextProps
     *
     * 1. Check list banner. break out not exist banner list
     * 2. Init banner with start index at [0]
     * 3. Setup interval for slide
     */
    BannerHomeMain.prototype.startAnimation = function (_props) {
        var _this = this;
        if (_props === void 0) { _props = this.props; }
        var _a = _props.list, list = _a === void 0 ? [] : _a;
        /** 1. Check list banner. break out not exist banner list */
        if (0 === list.length) {
            return;
        }
        this.setState({
            countChangeSlide: 0,
            selected: list[0],
        });
        /** 2. Init banner with start index at [0] */
        this.selectSlide(list, 0, true);
        setTimeout(function () {
            var preLoadImageList = Array.isArray(list)
                ? list
                    .filter(function (item, $index) { return $index > 0; })
                    .map(function (item) { return item.cover_image.large_url; })
                : [];
            Object(utils_image["b" /* preLoadImage */])(preLoadImageList);
        }, 3000);
        clearInterval(this.timerChangeSlide);
        this.timerChangeSlide = setInterval(function () {
            setTimeout(function () {
                _this.navSlide('right', true);
            }, 10);
        }, 6000);
    };
    /**
     * Calculate for slide
     *
     * @param {'left' | 'right'} _direction direction left / right for button navigation
     * @param {Boolean} _keepAutoSlide option to break automation for slide
     *
     * 1. checking direction to increase / recude new index slide
     * 2. limit [min, max] for index value
     * 3. set new index for slde
     */
    BannerHomeMain.prototype.navSlide = function (_direction, _keepAutoSlide) {
        if (_keepAutoSlide === void 0) { _keepAutoSlide = false; }
        var _a = this.props.list, list = _a === void 0 ? [] : _a;
        /** 1. checking direction to increase / recude new index slide */
        var newIndexValue = 'left' === _direction ? -1 : 1;
        newIndexValue += this.state.countChangeSlide;
        /** 2. limit [min, max] for index value */
        newIndexValue = newIndexValue === list.length
            ? 0
            : (newIndexValue === -1
                ? list.length - 1
                : newIndexValue);
        /** 3. set new index for slde */
        this.selectSlide(list, newIndexValue, _keepAutoSlide);
    };
    /**
     * Navigate to new index for sldie
     *
     * @param {number} _index new index value
     * @param {boolean} _keepAutoSlide option to break automation for slide
     */
    BannerHomeMain.prototype.selectSlide = function (list, _index, _keepAutoSlide) {
        var _this = this;
        if (_keepAutoSlide === void 0) { _keepAutoSlide = false; }
        false === _keepAutoSlide
            && clearInterval(this.timerChangeSlide);
        /** While animation -> change image and reverse animation */
        setTimeout(function () {
            _this.setState({
                countChangeSlide: _index,
                selected: list[_index],
            });
        }, 10);
    };
    BannerHomeMain.prototype.handleTouchStart = function (e) {
        this.xDown = e.touches[0].clientX;
        this.yDown = e.touches[0].clientY;
    };
    BannerHomeMain.prototype.handleTouchMove = function (e) {
        if (!this.xDown || !this.yDown) {
            return;
        }
        var xUp = e.touches[0].clientX;
        var yUp = e.touches[0].clientY;
        var xDiff = this.xDown - xUp;
        var yDiff = this.yDown - yUp;
        if (Math.abs(xDiff) > Math.abs(yDiff)) {
            xDiff > 0
                ? this.navSlide('right')
                : this.navSlide('left');
        }
        this.xDown = null;
        this.yDown = null;
    };
    BannerHomeMain.prototype.shouldComponentUpdate = function (nextProps, nextState) {
        var listLen = this.props.list && this.props.list.length || 0;
        var nextListLen = nextProps.list && nextProps.list.length || 0;
        if (listLen !== nextListLen) {
            return true;
        }
        if (this.state.countChangeSlide !== nextState.countChangeSlide) {
            return true;
        }
        return false;
    };
    /** Destroy */
    BannerHomeMain.prototype.componentWillUnmount = function () {
        clearInterval(this.timerChangeSlide);
    };
    BannerHomeMain.prototype.render = function () {
        var renderViewProps = {
            props: this.props,
            state: this.state,
            handleTouchStart: this.handleTouchStart.bind(this),
            handleTouchMove: this.handleTouchMove.bind(this),
            selectSlide: this.selectSlide.bind(this),
            navSlide: this.navSlide.bind(this)
        };
        return home_main_view(renderViewProps);
    };
    BannerHomeMain.defaultProps = home_main_initialize_DEFAULT_PROPS;
    BannerHomeMain = home_main_component_decorate([
        radium
    ], BannerHomeMain);
    return BannerHomeMain;
}(react["Component"]));
;
/* harmony default export */ var home_main_component = (component_BannerHomeMain);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUNsQyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFFcEQsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDNUQsT0FBTyxVQUFVLE1BQU0sUUFBUSxDQUFDO0FBR2hDO0lBQTZCLGtDQUF5QjtJQU1wRCx3QkFBWSxLQUFhO1FBQXpCLFlBQ0Usa0JBQU0sS0FBSyxDQUFDLFNBRWI7UUFQTyxXQUFLLEdBQVEsSUFBSSxDQUFDO1FBQ2xCLFdBQUssR0FBUSxJQUFJLENBQUM7UUFDbEIsc0JBQWdCLEdBQVEsSUFBSSxDQUFDO1FBSW5DLEtBQUksQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDOztJQUM3QixDQUFDO0lBRUQsd0NBQXdDO0lBQ3hDLDBDQUFpQixHQUFqQjtRQUNFLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRUQscUNBQXFDO0lBQ3JDLGtEQUF5QixHQUF6QixVQUEwQixTQUFTO1FBQ2pDLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVEOzs7Ozs7OztPQVFHO0lBQ0gsdUNBQWMsR0FBZCxVQUFlLE1BQW1CO1FBQWxDLGlCQWlDQztRQWpDYyx1QkFBQSxFQUFBLFNBQVMsSUFBSSxDQUFDLEtBQUs7UUFDeEIsSUFBQSxnQkFBUyxFQUFULDhCQUFTLENBQVk7UUFFN0IsNERBQTREO1FBQzVELEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUN0QixNQUFNLENBQUM7UUFDVCxDQUFDO1FBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUNaLGdCQUFnQixFQUFFLENBQUM7WUFDbkIsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7U0FDUixDQUFDLENBQUM7UUFFYiw2Q0FBNkM7UUFDN0MsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRWhDLFVBQVUsQ0FBQztZQUNULElBQU0sZ0JBQWdCLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7Z0JBQzFDLENBQUMsQ0FBQyxJQUFJO3FCQUNILE1BQU0sQ0FBQyxVQUFDLElBQUksRUFBRSxNQUFNLElBQUssT0FBQSxNQUFNLEdBQUcsQ0FBQyxFQUFWLENBQVUsQ0FBQztxQkFDcEMsR0FBRyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQTFCLENBQTBCLENBQUM7Z0JBQzFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFFUCxZQUFZLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUNqQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxhQUFhLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFdBQVcsQ0FBQztZQUNsQyxVQUFVLENBQUM7Z0JBQ1QsS0FBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDL0IsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBRVQsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVEOzs7Ozs7Ozs7T0FTRztJQUNILGlDQUFRLEdBQVIsVUFBUyxVQUFVLEVBQUUsY0FBc0I7UUFBdEIsK0JBQUEsRUFBQSxzQkFBc0I7UUFDakMsSUFBQSxvQkFBUyxFQUFULDhCQUFTLENBQWdCO1FBRWpDLGlFQUFpRTtRQUNqRSxJQUFJLGFBQWEsR0FBRyxNQUFNLEtBQUssVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25ELGFBQWEsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDO1FBRTdDLDBDQUEwQztRQUMxQyxhQUFhLEdBQUcsYUFBYSxLQUFLLElBQUksQ0FBQyxNQUFNO1lBQzNDLENBQUMsQ0FBQyxDQUFDO1lBQ0gsQ0FBQyxDQUFDLENBQ0EsYUFBYSxLQUFLLENBQUMsQ0FBQztnQkFDbEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQztnQkFDakIsQ0FBQyxDQUFDLGFBQWEsQ0FDbEIsQ0FBQztRQUVKLGdDQUFnQztRQUNoQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxhQUFhLEVBQUUsY0FBYyxDQUFDLENBQUM7SUFDeEQsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0gsb0NBQVcsR0FBWCxVQUFZLElBQUksRUFBRSxNQUFNLEVBQUUsY0FBc0I7UUFBaEQsaUJBV0M7UUFYeUIsK0JBQUEsRUFBQSxzQkFBc0I7UUFDOUMsS0FBSyxLQUFLLGNBQWM7ZUFDbkIsYUFBYSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBRTFDLDREQUE0RDtRQUM1RCxVQUFVLENBQUM7WUFDVCxLQUFJLENBQUMsUUFBUSxDQUFDO2dCQUNaLGdCQUFnQixFQUFFLE1BQU07Z0JBQ3hCLFFBQVEsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDO2FBQ2IsQ0FBQyxDQUFDO1FBQ2YsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUVELHlDQUFnQixHQUFoQixVQUFpQixDQUFDO1FBQ2hCLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7UUFDbEMsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztJQUNwQyxDQUFDO0lBRUQsd0NBQWUsR0FBZixVQUFnQixDQUFDO1FBQ2YsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDL0IsTUFBTSxDQUFDO1FBQ1QsQ0FBQztRQUVELElBQU0sR0FBRyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO1FBQ2pDLElBQU0sR0FBRyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO1FBRWpDLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO1FBQy9CLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO1FBRS9CLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdEMsS0FBSyxHQUFHLENBQUM7Z0JBQ1AsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDO2dCQUN4QixDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM1QixDQUFDO1FBRUQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7SUFDcEIsQ0FBQztJQUVELDhDQUFxQixHQUFyQixVQUFzQixTQUFpQixFQUFFLFNBQWlCO1FBQ3hELElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUM7UUFDL0QsSUFBTSxXQUFXLEdBQUcsU0FBUyxDQUFDLElBQUksSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUM7UUFDakUsRUFBRSxDQUFDLENBQUMsT0FBTyxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQUMsQ0FBQztRQUM3QyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixLQUFLLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQUMsQ0FBQztRQUVoRixNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVELGNBQWM7SUFDZCw2Q0FBb0IsR0FBcEI7UUFDRSxhQUFhLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELCtCQUFNLEdBQU47UUFDRSxJQUFNLGVBQWUsR0FBRztZQUN0QixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLGdCQUFnQixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2xELGVBQWUsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDaEQsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUN4QyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ25DLENBQUM7UUFFRixNQUFNLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFwS00sMkJBQVksR0FBVyxhQUFhLENBQUM7SUFEeEMsY0FBYztRQURuQixNQUFNO09BQ0QsY0FBYyxDQXNLbkI7SUFBRCxxQkFBQztDQUFBLEFBdEtELENBQTZCLFNBQVMsR0FzS3JDO0FBQUEsQ0FBQztBQUVGLGVBQWUsY0FBYyxDQUFDIn0=
// CONCATENATED MODULE: ./components/banner/home-main/index.tsx

/* harmony default export */ var home_main = (home_main_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxjQUFjLE1BQU0sYUFBYSxDQUFDO0FBQ3pDLGVBQWUsY0FBYyxDQUFDIn0=
// EXTERNAL MODULE: ./components/banner/feature/index.tsx + 4 modules
var feature = __webpack_require__(812);

// EXTERNAL MODULE: ./container/layout/main-block/index.tsx
var main_block = __webpack_require__(746);

// CONCATENATED MODULE: ./components/banner/footer-shop/style.tsx



/* harmony default export */ var footer_shop_style = ({
    display: variable["display"].block,
    paddingTop: 30,
    paddingBottom: 30,
    container: [
        layout["a" /* flexContainer */].wrap,
        {
            marginLeft: -35,
            marginRight: -35,
        }
    ],
    banner: {
        container: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{ width: '50%' }],
            DESKTOP: [{ width: '25%', }],
            GENERAL: [{
                    height: 'auto',
                    paddingRight: 15,
                    paddingBottom: 0,
                    paddingLeft: 15,
                    cursor: 'pointer',
                    position: 'relative'
                }]
        }),
        innerContainer: {
            width: '100%',
            paddingTop: '161%',
            position: variable["position"].relative
        },
        image: {
            width: '100%',
            height: '100%',
            top: 0,
            left: 0,
            backgroundColor: variable["colorF7"],
            display: 'block',
            position: 'absolute',
            transition: variable["transitionOpacity"],
        }
    },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ3pELE9BQU8sS0FBSyxNQUFNLE1BQU0sdUJBQXVCLENBQUM7QUFDaEQsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxlQUFlO0lBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztJQUMvQixVQUFVLEVBQUUsRUFBRTtJQUNkLGFBQWEsRUFBRSxFQUFFO0lBRWpCLFNBQVMsRUFBRTtRQUNULE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSTtRQUN6QjtZQUNFLFVBQVUsRUFBRSxDQUFDLEVBQUU7WUFDZixXQUFXLEVBQUUsQ0FBQyxFQUFFO1NBQ2pCO0tBQ0Y7SUFFRCxNQUFNLEVBQUU7UUFDTixTQUFTLEVBQUUsWUFBWSxDQUFDO1lBQ3RCLE1BQU0sRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDO1lBQzFCLE9BQU8sRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssR0FBRyxDQUFDO1lBRTVCLE9BQU8sRUFBRSxDQUFDO29CQUNSLE1BQU0sRUFBRSxNQUFNO29CQUNkLFlBQVksRUFBRSxFQUFFO29CQUNoQixhQUFhLEVBQUUsQ0FBQztvQkFDaEIsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsTUFBTSxFQUFFLFNBQVM7b0JBQ2pCLFFBQVEsRUFBRSxVQUFVO2lCQUNyQixDQUFDO1NBQ0gsQ0FBQztRQUVGLGNBQWMsRUFBRTtZQUNkLEtBQUssRUFBRSxNQUFNO1lBQ2IsVUFBVSxFQUFFLE1BQU07WUFDbEIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtTQUNyQztRQUVELEtBQUssRUFBRTtZQUNMLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLE1BQU07WUFDZCxHQUFHLEVBQUUsQ0FBQztZQUNOLElBQUksRUFBRSxDQUFDO1lBQ1AsZUFBZSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1lBQ2pDLE9BQU8sRUFBRSxPQUFPO1lBQ2hCLFFBQVEsRUFBRSxVQUFVO1lBQ3BCLFVBQVUsRUFBRSxRQUFRLENBQUMsaUJBQWlCO1NBQ3ZDO0tBQ0Y7Q0FDSyxDQUFDIn0=
// CONCATENATED MODULE: ./components/banner/footer-shop/view.tsx
var footer_shop_view_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};






var view_generateItemProps = function (item, $index) { return ({
    key: "banner-footer-" + $index,
    to: Object(validate["f" /* getNavLink */])(item.link),
    style: footer_shop_style.banner.container
}); };
var generateImageProps = function (item, $index) { return ({
    style: footer_shop_style.banner.image,
    src: item.cover_image.original_url
}); };
var footer_shop_view_renderView = function (_a) {
    var list = _a.list;
    var mainBlockProps = {
        showHeader: true,
        title: 'THƯƠNG HIỆU NỔI BẬT',
        showViewMore: false,
        content: (react["createElement"]("div", { style: footer_shop_style.container }, Array.isArray(list)
            && list.map(function (item, $index) { return Object.assign({}, item, { generatedId: $index }); }).map(function (item) {
                var itemProps = view_generateItemProps(item, item.generatedId);
                var imageProps = generateImageProps(item, item.generatedId);
                return (react["createElement"](react_router_dom["NavLink"], footer_shop_view_assign({}, itemProps),
                    react["createElement"]("div", { style: footer_shop_style.banner.innerContainer },
                        react["createElement"]("img", footer_shop_view_assign({}, imageProps)))));
            }))),
        style: {}
    };
    return (react["createElement"]("div", { style: footer_shop_style },
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"](main_block["a" /* default */], footer_shop_view_assign({}, mainBlockProps)))));
};
/* harmony default export */ var footer_shop_view = (footer_shop_view_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sVUFBVSxNQUFNLGdDQUFnQyxDQUFDO0FBQ3hELE9BQU8sU0FBUyxNQUFNLHNDQUFzQyxDQUFDO0FBQzdELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUNyRCxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsSUFBTSxpQkFBaUIsR0FBRyxVQUFDLElBQUksRUFBRSxNQUFNLElBQUssT0FBQSxDQUFDO0lBQzNDLEdBQUcsRUFBRSxtQkFBaUIsTUFBUTtJQUM5QixFQUFFLEVBQUUsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDekIsS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUztDQUM5QixDQUFDLEVBSjBDLENBSTFDLENBQUM7QUFFSCxJQUFNLGtCQUFrQixHQUFHLFVBQUMsSUFBSSxFQUFFLE1BQU0sSUFBSyxPQUFBLENBQUM7SUFDNUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSztJQUN6QixHQUFHLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZO0NBQ25DLENBQUMsRUFIMkMsQ0FHM0MsQ0FBQztBQUVILElBQU0sVUFBVSxHQUFHLFVBQUMsRUFBUTtRQUFOLGNBQUk7SUFDeEIsSUFBTSxjQUFjLEdBQUc7UUFDckIsVUFBVSxFQUFFLElBQUk7UUFDaEIsS0FBSyxFQUFFLHFCQUFxQjtRQUM1QixZQUFZLEVBQUUsS0FBSztRQUNuQixPQUFPLEVBQUUsQ0FDUCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFNBQVMsSUFFdkIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7ZUFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxNQUFNLElBQUssT0FBQSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxXQUFXLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBaEQsQ0FBZ0QsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUk7Z0JBQ3ZGLElBQU0sU0FBUyxHQUFHLGlCQUFpQixDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQzVELElBQU0sVUFBVSxHQUFHLGtCQUFrQixDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQzlELE1BQU0sQ0FBQyxDQUNMLG9CQUFDLE9BQU8sZUFBSyxTQUFTO29CQUNwQiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxjQUFjO3dCQUNyQyx3Q0FBUyxVQUFVLEVBQUksQ0FDbkIsQ0FDRSxDQUNYLENBQUM7WUFDSixDQUFDLENBQUMsQ0FFQSxDQUNQO1FBQ0QsS0FBSyxFQUFFLEVBQUU7S0FDVixDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsNkJBQUssS0FBSyxFQUFFLEtBQUs7UUFDZixvQkFBQyxVQUFVO1lBQ1Qsb0JBQUMsU0FBUyxlQUFLLGNBQWMsRUFBSSxDQUN0QixDQUNULENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./components/banner/footer-shop/component.tsx
var footer_shop_component_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var footer_shop_component_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var component_BannerFooterShop = /** @class */ (function (_super) {
    footer_shop_component_extends(BannerFooterShop, _super);
    function BannerFooterShop() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    BannerFooterShop.prototype.shouldComponentUpdate = function (nextProps) {
        if (this.props.list.length !== nextProps.list.length) {
            return true;
        }
        return false;
    };
    BannerFooterShop.prototype.render = function () {
        var list = this.props.list;
        return footer_shop_view({ list: list });
    };
    BannerFooterShop = footer_shop_component_decorate([
        radium
    ], BannerFooterShop);
    return BannerFooterShop;
}(react["Component"]));
;
/* harmony default export */ var footer_shop_component = (component_BannerFooterShop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUNsQyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUdqQyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFHaEM7SUFBK0Isb0NBQXlCO0lBQXhEOztJQVdBLENBQUM7SUFWQyxnREFBcUIsR0FBckIsVUFBc0IsU0FBaUI7UUFDckMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxLQUFLLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFBQyxDQUFDO1FBRXRFLE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQsaUNBQU0sR0FBTjtRQUNVLElBQUEsc0JBQUksQ0FBZ0I7UUFDNUIsTUFBTSxDQUFDLFVBQVUsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBVkcsZ0JBQWdCO1FBRHJCLE1BQU07T0FDRCxnQkFBZ0IsQ0FXckI7SUFBRCx1QkFBQztDQUFBLEFBWEQsQ0FBK0IsU0FBUyxHQVd2QztBQUFBLENBQUM7QUFFRixlQUFlLGdCQUFnQixDQUFDIn0=
// CONCATENATED MODULE: ./components/banner/footer-shop/index.tsx

/* harmony default export */ var footer_shop = (footer_shop_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxnQkFBZ0IsTUFBTSxhQUFhLENBQUM7QUFDM0MsZUFBZSxnQkFBZ0IsQ0FBQyJ9
// EXTERNAL MODULE: ./utils/index.ts
var utils = __webpack_require__(788);

// EXTERNAL MODULE: ../node_modules/util/util.js
var util = __webpack_require__(354);

// CONCATENATED MODULE: ./container/app-shop/index/view-desktop.tsx
var view_desktop_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};



















var renderDesktop = function (_a) {
    var props = _a.props, state = _a.state, generateCategoryHash = _a.generateCategoryHash, handleFeatureBanner = _a.handleFeatureBanner, handleFetchHotBoxes = _a.handleFetchHotBoxes, handleFetchMagazineList = _a.handleFetchMagazineList, handleFetchNewProducts = _a.handleFetchNewProducts, handleFetchPopularSearch = _a.handleFetchPopularSearch, handleFetchWatchedList = _a.handleFetchWatchedList, handleFetchActivityFeed = _a.handleFetchActivityFeed, handleFetchFooterBanner = _a.handleFetchFooterBanner;
    var openModal = props.openModal, bannerStore = props.bannerStore, list = props.activityFeedStore.list, userWatchedList = props.userStore.userWatchedList, magazineList = props.magazineStore.magazineList, _b = props.shopStore, hotDeal = _b.hotDeal, productByCategory = _b.productByCategory;
    var isFetchedFeatureBanner = state.isFetchedFeatureBanner, isFetchedHotBoxes = state.isFetchedHotBoxes, isFetchedMagazineList = state.isFetchedMagazineList, isFetchedNewProducts = state.isFetchedNewProducts, isFetchedPopularSearch = state.isFetchedPopularSearch, isFetchedWatchedList = state.isFetchedWatchedList, isFetchedActivityFeed = state.isFetchedActivityFeed, isFetchedFooterBanner = state.isFetchedFooterBanner, isPriorotyBlock = state.isPriorotyBlock;
    var mainBannerHash = Object(encode["j" /* objectToHash */])({ idBanner: application_default["a" /* BANNER_ID */].HOME_PAGE, limit: application_default["b" /* BANNER_LIMIT_DEFAULT */] });
    var mainBannerProps = {
        list: bannerStore.bannerList[mainBannerHash] || [],
        style: index_style.mainBanner
    };
    var lastestBoxesProps = {
        title: 'SIÊU GIẢM GIÁ',
        column: 5,
        data: Array.isArray(hotDeal) ? hotDeal : [],
        showViewMore: false
    };
    var bestSellingHash = Object(encode["j" /* objectToHash */])({ idCategory: bestSellingParams.idCategory, searchQuery: '' });
    var bestSellingList = !Object(utils["e" /* isEmptyObject */])(productByCategory)
        && !Object(util["isUndefined"])(productByCategory[bestSellingHash])
        && !Object(utils["e" /* isEmptyObject */])(productByCategory[bestSellingHash])
        && Array.isArray(productByCategory[bestSellingHash].boxes)
        && productByCategory[bestSellingHash].boxes || [];
    var hottestBoxesProps = {
        title: 'BOX BÁN CHẠY',
        column: 5,
        // data: dataHomePage.hottest_boxes && dataHomePage.hottest_boxes.items || [],
        // viewMoreLink: `${ROUTING_PRODUCT_CATEGORY_PATH}/${dataHomePage.hottest_boxes && dataHomePage.hottest_boxes.slug || ''}`
        data: bestSellingList,
        viewMoreLink: routing["Xa" /* ROUTING_PRODUCT_CATEGORY_PATH */] + "/" + bestSellingParams.idCategory
    };
    var newProductsHash = Object(encode["j" /* objectToHash */])({ idCategory: newProductsParams.idCategory, searchQuery: '' });
    var newProductsList = !Object(utils["e" /* isEmptyObject */])(productByCategory)
        && !Object(util["isUndefined"])(productByCategory[newProductsHash])
        && !Object(utils["e" /* isEmptyObject */])(productByCategory[newProductsHash])
        && Array.isArray(productByCategory[newProductsHash].boxes)
        && productByCategory[newProductsHash].boxes || [];
    var latestIndividualBoxesProps = {
        title: 'MUA LẺ MỚI NHẤT',
        column: 5,
        // data: dataHomePage.latest_individual_boxes && dataHomePage.latest_individual_boxes.items || [],
        // viewMoreLink: `${ROUTING_PRODUCT_CATEGORY_PATH}/${dataHomePage.latest_individual_boxes && dataHomePage.latest_individual_boxes.slug || ''}`
        data: newProductsList,
        viewMoreLink: routing["Xa" /* ROUTING_PRODUCT_CATEGORY_PATH */] + "/" + newProductsParams.idCategory
    };
    var watchedListHash = Object(encode["j" /* objectToHash */])({ page: 1, perPage: 10 });
    var watchedList = userWatchedList[watchedListHash] && userWatchedList[watchedListHash].recently_viewed_boxes || [];
    var watchedListProps = {
        title: 'DANH SÁCH ĐÃ XEM',
        column: 5,
        data: watchedList,
        showViewMore: false,
    };
    var defaultMagazineListHash = Object(encode["j" /* objectToHash */])({ page: 1, perPage: 12, type: magazine["a" /* MAGAZINE_LIST_TYPE */].DEFAULT });
    var featureBannerHash = Object(encode["j" /* objectToHash */])({ idBanner: application_default["a" /* BANNER_ID */].HOME_FEATURE, limit: application_default["b" /* BANNER_LIMIT_DEFAULT */] });
    var listFeatureBanner = bannerStore.bannerList[featureBannerHash] || [];
    var testimonialProps = { list: list, openModal: openModal, isHome: true };
    var popularSearchHash = Object(encode["j" /* objectToHash */])({ idBanner: application_default["a" /* BANNER_ID */].POPULAR_SEARCH, limit: application_default["b" /* BANNER_LIMIT_DEFAULT */] });
    var tmpPopularSearchList = bannerStore.bannerList[popularSearchHash] || [];
    var popularSearchList = tmpPopularSearchList && tmpPopularSearchList.length > 5 ? tmpPopularSearchList.slice(0, 5) : tmpPopularSearchList;
    var footerBannerHash = Object(encode["j" /* objectToHash */])({ idBanner: application_default["a" /* BANNER_ID */].FOOTER, limit: application_default["b" /* BANNER_LIMIT_DEFAULT */] });
    var footerBannerProps = { list: bannerStore.bannerList[footerBannerHash] || [] };
    return (react["createElement"]("shop-index-container", { style: index_style },
        react["createElement"]("h1", { className: 'hidden-element' }, "Lixibox | M\u1EF9 Ph\u1EA9m, D\u01B0\u1EE1ng Da, Tr\u1ECB M\u1EE5n, Skincare, Makeup"),
        react["createElement"](wrap["a" /* default */], { type: 'larger', style: index_style.wrapBannerMain },
            react["createElement"](home_main, view_desktop_assign({}, mainBannerProps))),
        react["createElement"](wrap["a" /* default */], null,
            !!hotDeal && !!hotDeal.length
                && (react["createElement"]("div", null,
                    react["createElement"]("h2", { className: 'hidden-element' }, "Box m\u1EDBi nh\u1EA5t | M\u1EF9 Ph\u1EA9m, D\u01B0\u1EE1ng Da, Tr\u1ECB M\u1EE5n, Skincare, Makeup"),
                    react["createElement"](slider["a" /* default */], view_desktop_assign({}, lastestBoxesProps)))),
            !isPriorotyBlock
                ? (react["createElement"]("div", null,
                    react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                        var isVisible = _a.isVisible;
                        !!isVisible
                            && !isFetchedFeatureBanner
                            && !!handleFeatureBanner
                            && handleFeatureBanner();
                        return react["createElement"](feature["a" /* default */], { list: listFeatureBanner });
                    }),
                    react["createElement"]("h2", { className: 'hidden-element' }, "Box b\u00E1n ch\u1EA1y | M\u1EF9 Ph\u1EA9m, D\u01B0\u1EE1ng Da, Tr\u1ECB M\u1EE5n, Skincare, Makeup"),
                    react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                        var isVisible = _a.isVisible;
                        !!isVisible
                            && !isFetchedHotBoxes
                            && !!handleFetchHotBoxes
                            && handleFetchHotBoxes();
                        return react["createElement"](slider["a" /* default */], view_desktop_assign({}, hottestBoxesProps));
                    }),
                    react["createElement"]("h2", { className: 'hidden-element' }, "Blog l\u00E0m \u0111\u1EB9p | M\u1EF9 Ph\u1EA9m, D\u01B0\u1EE1ng Da, Tr\u1ECB M\u1EE5n, Skincare, Makeup"),
                    react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                        var isVisible = _a.isVisible;
                        !!isVisible
                            && !isFetchedMagazineList
                            && !!handleFetchMagazineList
                            && handleFetchMagazineList();
                        var magazineImageProps = {
                            title: 'TẠP CHÍ LÀM ĐẸP',
                            column: 4,
                            showViewMore: false,
                            data: magazineList[defaultMagazineListHash] || []
                        };
                        return react["createElement"](image_slider["a" /* default */], view_desktop_assign({}, magazineImageProps));
                    }),
                    react["createElement"]("h2", { className: 'hidden-element' }, "S\u1EA3n ph\u1EA9m m\u1EDBi nh\u1EA5t | M\u1EF9 Ph\u1EA9m, D\u01B0\u1EE1ng Da, Tr\u1ECB M\u1EE5n, Skincare, Makeup"),
                    react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                        var isVisible = _a.isVisible;
                        !!isVisible
                            && !isFetchedNewProducts
                            && !!handleFetchNewProducts
                            && handleFetchNewProducts();
                        return react["createElement"](slider["a" /* default */], view_desktop_assign({}, latestIndividualBoxesProps));
                    }),
                    react["createElement"]("h2", { className: 'hidden-element' }, "T\u1EEB kh\u00F3a t\u00ECm ki\u1EBFm ph\u1ED5 bi\u1EBFn nh\u1EA5t | M\u1EF9 Ph\u1EA9m, D\u01B0\u1EE1ng Da, Tr\u1ECB M\u1EE5n, Skincare, Makeup"),
                    react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                        var isVisible = _a.isVisible;
                        !!isVisible
                            && !isFetchedPopularSearch
                            && !!handleFetchPopularSearch
                            && handleFetchPopularSearch();
                        return react["createElement"](search_popular, { list: popularSearchList });
                    }),
                    react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                        var isVisible = _a.isVisible;
                        !!isVisible
                            && !isFetchedWatchedList
                            && !!handleFetchWatchedList
                            && handleFetchWatchedList();
                        return 0 !== watchedList.length && react["createElement"](slider["a" /* default */], view_desktop_assign({}, watchedListProps));
                    }),
                    react["createElement"]("h2", { className: 'hidden-element' }, "\u0110\u00E1nh gi\u00E1 s\u1EA3n ph\u1EA9m | M\u1EF9 Ph\u1EA9m, D\u01B0\u1EE1ng Da, Tr\u1ECB M\u1EE5n, Skincare, Makeup"),
                    react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                        var isVisible = _a.isVisible;
                        !!isVisible
                            && !isFetchedActivityFeed
                            && !!handleFetchActivityFeed
                            && handleFetchActivityFeed();
                        return react["createElement"](testimonial["a" /* default */], view_desktop_assign({}, testimonialProps));
                    }),
                    react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                        var isVisible = _a.isVisible;
                        !!isVisible
                            && !isFetchedFooterBanner
                            && !!handleFetchFooterBanner
                            && handleFetchFooterBanner();
                        return react["createElement"](footer_shop, view_desktop_assign({}, footerBannerProps));
                    }))) : react["createElement"](loading["a" /* default */], { style: { height: 400 } }))));
};
/* harmony default export */ var view_desktop = (renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1kZXNrdG9wLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1kZXNrdG9wLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRS9CLE9BQU8sVUFBVSxNQUFNLG1CQUFtQixDQUFDO0FBQzNDLE9BQU8sYUFBYSxNQUFNLG9DQUFvQyxDQUFDO0FBQy9ELE9BQU8sbUJBQW1CLE1BQU0sMkNBQTJDLENBQUM7QUFDNUUsT0FBTyxXQUFXLE1BQU0saUNBQWlDLENBQUM7QUFDMUQsT0FBTyxjQUFjLE1BQU0sc0NBQXNDLENBQUM7QUFDbEUsT0FBTyxhQUFhLE1BQU0sb0NBQW9DLENBQUM7QUFDL0QsT0FBTyxnQkFBZ0IsTUFBTSx3Q0FBd0MsQ0FBQztBQUN0RSxPQUFPLGFBQWEsTUFBTSw0Q0FBNEMsQ0FBQztBQUN2RSxPQUFPLGVBQWUsTUFBTSxpQkFBaUIsQ0FBQztBQUU5QyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsU0FBUyxFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDekYsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDdkYsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3JELE9BQU8sT0FBTyxNQUFNLGdDQUFnQyxDQUFDO0FBRXJELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUVwRSxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFDNUIsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFFbkMsSUFBTSxhQUFhLEdBQUcsVUFBQyxFQVl0QjtRQVhDLGdCQUFLLEVBQ0wsZ0JBQUssRUFDTCw4Q0FBb0IsRUFDcEIsNENBQW1CLEVBQ25CLDRDQUFtQixFQUNuQixvREFBdUIsRUFDdkIsa0RBQXNCLEVBQ3RCLHNEQUF3QixFQUN4QixrREFBc0IsRUFDdEIsb0RBQXVCLEVBQ3ZCLG9EQUF1QjtJQUdyQixJQUFBLDJCQUFTLEVBQ1QsK0JBQVcsRUFDVSxtQ0FBSSxFQUNaLGlEQUFlLEVBQ1gsK0NBQVksRUFDN0Isb0JBQXlDLEVBQTVCLG9CQUFPLEVBQUUsd0NBQWlCLENBQy9CO0lBR1IsSUFBQSxxREFBc0IsRUFDdEIsMkNBQWlCLEVBQ2pCLG1EQUFxQixFQUNyQixpREFBb0IsRUFDcEIscURBQXNCLEVBQ3RCLGlEQUFvQixFQUNwQixtREFBcUIsRUFDckIsbURBQXFCLEVBQ3JCLHVDQUFlLENBQ1A7SUFFVixJQUFNLGNBQWMsR0FBRyxZQUFZLENBQUMsRUFBRSxRQUFRLEVBQUUsU0FBUyxDQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUUsb0JBQW9CLEVBQUUsQ0FBQyxDQUFDO0lBQ3BHLElBQU0sZUFBZSxHQUFHO1FBQ3RCLElBQUksRUFBRSxXQUFXLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUU7UUFDbEQsS0FBSyxFQUFFLEtBQUssQ0FBQyxVQUFVO0tBQ3hCLENBQUM7SUFFRixJQUFNLGlCQUFpQixHQUFHO1FBQ3hCLEtBQUssRUFBRSxlQUFlO1FBQ3RCLE1BQU0sRUFBRSxDQUFDO1FBQ1QsSUFBSSxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTtRQUMzQyxZQUFZLEVBQUUsS0FBSztLQUNwQixDQUFDO0lBRUYsSUFBTSxlQUFlLEdBQUcsWUFBWSxDQUFDLEVBQUUsVUFBVSxFQUFFLGlCQUFpQixDQUFDLFVBQVUsRUFBRSxXQUFXLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNwRyxJQUFNLGVBQWUsR0FBRyxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQztXQUNwRCxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlLENBQUMsQ0FBQztXQUNoRCxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlLENBQUMsQ0FBQztXQUNsRCxLQUFLLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLGVBQWUsQ0FBQyxDQUFDLEtBQUssQ0FBQztXQUN2RCxpQkFBaUIsQ0FBQyxlQUFlLENBQUMsQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDO0lBRXBELElBQU0saUJBQWlCLEdBQUc7UUFDeEIsS0FBSyxFQUFFLGNBQWM7UUFDckIsTUFBTSxFQUFFLENBQUM7UUFDVCw4RUFBOEU7UUFDOUUsMEhBQTBIO1FBQzFILElBQUksRUFBRSxlQUFlO1FBQ3JCLFlBQVksRUFBSyw2QkFBNkIsU0FBSSxpQkFBaUIsQ0FBQyxVQUFZO0tBQ2pGLENBQUM7SUFFRixJQUFNLGVBQWUsR0FBRyxZQUFZLENBQUMsRUFBRSxVQUFVLEVBQUUsaUJBQWlCLENBQUMsVUFBVSxFQUFFLFdBQVcsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ3BHLElBQU0sZUFBZSxHQUFHLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDO1dBQ3BELENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1dBQ2hELENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1dBQ2xELEtBQUssQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLENBQUMsS0FBSyxDQUFDO1dBQ3ZELGlCQUFpQixDQUFDLGVBQWUsQ0FBQyxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUM7SUFFcEQsSUFBTSwwQkFBMEIsR0FBRztRQUNqQyxLQUFLLEVBQUUsaUJBQWlCO1FBQ3hCLE1BQU0sRUFBRSxDQUFDO1FBQ1Qsa0dBQWtHO1FBQ2xHLDhJQUE4STtRQUM5SSxJQUFJLEVBQUUsZUFBZTtRQUNyQixZQUFZLEVBQUssNkJBQTZCLFNBQUksaUJBQWlCLENBQUMsVUFBWTtLQUNqRixDQUFDO0lBRUYsSUFBTSxlQUFlLEdBQUcsWUFBWSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUMvRCxJQUFNLFdBQVcsR0FBRyxlQUFlLENBQUMsZUFBZSxDQUFDLElBQUksZUFBZSxDQUFDLGVBQWUsQ0FBQyxDQUFDLHFCQUFxQixJQUFJLEVBQUUsQ0FBQztJQUNySCxJQUFNLGdCQUFnQixHQUFHO1FBQ3ZCLEtBQUssRUFBRSxrQkFBa0I7UUFDekIsTUFBTSxFQUFFLENBQUM7UUFDVCxJQUFJLEVBQUUsV0FBVztRQUNqQixZQUFZLEVBQUUsS0FBSztLQUNwQixDQUFDO0lBRUYsSUFBTSx1QkFBdUIsR0FBRyxZQUFZLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLGtCQUFrQixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFFekcsSUFBTSxpQkFBaUIsR0FBRyxZQUFZLENBQUMsRUFBRSxRQUFRLEVBQUUsU0FBUyxDQUFDLFlBQVksRUFBRSxLQUFLLEVBQUUsb0JBQW9CLEVBQUUsQ0FBQyxDQUFDO0lBQzFHLElBQU0saUJBQWlCLEdBQUcsV0FBVyxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUUxRSxJQUFNLGdCQUFnQixHQUFHLEVBQUUsSUFBSSxNQUFBLEVBQUUsU0FBUyxXQUFBLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDO0lBRTNELElBQU0saUJBQWlCLEdBQUcsWUFBWSxDQUFDLEVBQUUsUUFBUSxFQUFFLFNBQVMsQ0FBQyxjQUFjLEVBQUUsS0FBSyxFQUFFLG9CQUFvQixFQUFFLENBQUMsQ0FBQztJQUM1RyxJQUFNLG9CQUFvQixHQUFHLFdBQVcsQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDN0UsSUFBTSxpQkFBaUIsR0FBRyxvQkFBb0IsSUFBSSxvQkFBb0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQztJQUU1SSxJQUFNLGdCQUFnQixHQUFHLFlBQVksQ0FBQyxFQUFFLFFBQVEsRUFBRSxTQUFTLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxvQkFBb0IsRUFBRSxDQUFDLENBQUM7SUFDbkcsSUFBTSxpQkFBaUIsR0FBRyxFQUFFLElBQUksRUFBRSxXQUFXLENBQUMsVUFBVSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxFQUFFLENBQUM7SUFFbkYsTUFBTSxDQUFDLENBQ0wsOENBQXNCLEtBQUssRUFBRSxLQUFLO1FBQ2hDLDRCQUFJLFNBQVMsRUFBRSxnQkFBZ0IsMkZBQTZEO1FBQzVGLG9CQUFDLFVBQVUsSUFBQyxJQUFJLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsY0FBYztZQUNyRCxvQkFBQyxjQUFjLGVBQUssZUFBZSxFQUFJLENBQzVCO1FBRWIsb0JBQUMsVUFBVTtZQWFQLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFNO21CQUMxQixDQUNEO29CQUNFLDRCQUFJLFNBQVMsRUFBRSxnQkFBZ0IsMEdBQWtFO29CQUNqRyxvQkFBQyxhQUFhLGVBQUssaUJBQWlCLEVBQUksQ0FDcEMsQ0FDUDtZQUlELENBQUMsZUFBZTtnQkFDZCxDQUFDLENBQUMsQ0FDQTtvQkFDRSxvQkFBQyxlQUFlLElBQUMsTUFBTSxFQUFFLEdBQUcsSUFFeEIsVUFBQyxFQUFhOzRCQUFYLHdCQUFTO3dCQUNWLENBQUMsQ0FBQyxTQUFTOytCQUNOLENBQUMsc0JBQXNCOytCQUN2QixDQUFDLENBQUMsbUJBQW1COytCQUNyQixtQkFBbUIsRUFBRSxDQUFDO3dCQUUzQixNQUFNLENBQUMsb0JBQUMsYUFBYSxJQUFDLElBQUksRUFBRSxpQkFBaUIsR0FBSSxDQUFDO29CQUNwRCxDQUFDLENBRWE7b0JBRWxCLDRCQUFJLFNBQVMsRUFBRSxnQkFBZ0IsMEdBQWtFO29CQUNqRyxvQkFBQyxlQUFlLElBQUMsTUFBTSxFQUFFLEdBQUcsSUFFeEIsVUFBQyxFQUFhOzRCQUFYLHdCQUFTO3dCQUNWLENBQUMsQ0FBQyxTQUFTOytCQUNOLENBQUMsaUJBQWlCOytCQUNsQixDQUFDLENBQUMsbUJBQW1COytCQUNyQixtQkFBbUIsRUFBRSxDQUFDO3dCQUUzQixNQUFNLENBQUMsb0JBQUMsYUFBYSxlQUFLLGlCQUFpQixFQUFJLENBQUM7b0JBQ2xELENBQUMsQ0FFYTtvQkFFbEIsNEJBQUksU0FBUyxFQUFFLGdCQUFnQiwrR0FBa0U7b0JBQ2pHLG9CQUFDLGVBQWUsSUFBQyxNQUFNLEVBQUUsR0FBRyxJQUV4QixVQUFDLEVBQWE7NEJBQVgsd0JBQVM7d0JBQ1YsQ0FBQyxDQUFDLFNBQVM7K0JBQ04sQ0FBQyxxQkFBcUI7K0JBQ3RCLENBQUMsQ0FBQyx1QkFBdUI7K0JBQ3pCLHVCQUF1QixFQUFFLENBQUM7d0JBRS9CLElBQU0sa0JBQWtCLEdBQUc7NEJBQ3pCLEtBQUssRUFBRSxpQkFBaUI7NEJBQ3hCLE1BQU0sRUFBRSxDQUFDOzRCQUNULFlBQVksRUFBRSxLQUFLOzRCQUNuQixJQUFJLEVBQUUsWUFBWSxDQUFDLHVCQUF1QixDQUFDLElBQUksRUFBRTt5QkFDbEQsQ0FBQzt3QkFFRixNQUFNLENBQUMsb0JBQUMsbUJBQW1CLGVBQUssa0JBQWtCLEVBQUksQ0FBQztvQkFDekQsQ0FBQyxDQUVhO29CQUVsQiw0QkFBSSxTQUFTLEVBQUUsZ0JBQWdCLHlIQUF1RTtvQkFDdEcsb0JBQUMsZUFBZSxJQUFDLE1BQU0sRUFBRSxHQUFHLElBRXhCLFVBQUMsRUFBYTs0QkFBWCx3QkFBUzt3QkFDVixDQUFDLENBQUMsU0FBUzsrQkFDTixDQUFDLG9CQUFvQjsrQkFDckIsQ0FBQyxDQUFDLHNCQUFzQjsrQkFDeEIsc0JBQXNCLEVBQUUsQ0FBQzt3QkFFOUIsTUFBTSxDQUFDLG9CQUFDLGFBQWEsZUFBSywwQkFBMEIsRUFBSSxDQUFDO29CQUMzRCxDQUFDLENBRWE7b0JBRWxCLDRCQUFJLFNBQVMsRUFBRSxnQkFBZ0IscUpBQW9GO29CQUNuSCxvQkFBQyxlQUFlLElBQUMsTUFBTSxFQUFFLEdBQUcsSUFFeEIsVUFBQyxFQUFhOzRCQUFYLHdCQUFTO3dCQUNWLENBQUMsQ0FBQyxTQUFTOytCQUNOLENBQUMsc0JBQXNCOytCQUN2QixDQUFDLENBQUMsd0JBQXdCOytCQUMxQix3QkFBd0IsRUFBRSxDQUFDO3dCQUVoQyxNQUFNLENBQUMsb0JBQUMsYUFBYSxJQUFDLElBQUksRUFBRSxpQkFBaUIsR0FBSSxDQUFDO29CQUNwRCxDQUFDLENBRWE7b0JBRWxCLG9CQUFDLGVBQWUsSUFBQyxNQUFNLEVBQUUsR0FBRyxJQUV4QixVQUFDLEVBQWE7NEJBQVgsd0JBQVM7d0JBQ1YsQ0FBQyxDQUFDLFNBQVM7K0JBQ04sQ0FBQyxvQkFBb0I7K0JBQ3JCLENBQUMsQ0FBQyxzQkFBc0I7K0JBQ3hCLHNCQUFzQixFQUFFLENBQUM7d0JBRTlCLE1BQU0sQ0FBQyxDQUFDLEtBQUssV0FBVyxDQUFDLE1BQU0sSUFBSSxvQkFBQyxhQUFhLGVBQUssZ0JBQWdCLEVBQUksQ0FBQztvQkFDN0UsQ0FBQyxDQUVhO29CQUVsQiw0QkFBSSxTQUFTLEVBQUUsZ0JBQWdCLDhIQUF1RTtvQkFDdEcsb0JBQUMsZUFBZSxJQUFDLE1BQU0sRUFBRSxHQUFHLElBRXhCLFVBQUMsRUFBYTs0QkFBWCx3QkFBUzt3QkFDVixDQUFDLENBQUMsU0FBUzsrQkFDTixDQUFDLHFCQUFxQjsrQkFDdEIsQ0FBQyxDQUFDLHVCQUF1QjsrQkFDekIsdUJBQXVCLEVBQUUsQ0FBQzt3QkFFL0IsTUFBTSxDQUFDLG9CQUFDLFdBQVcsZUFBSyxnQkFBZ0IsRUFBSSxDQUFBO29CQUM5QyxDQUFDLENBRWE7b0JBRWxCLG9CQUFDLGVBQWUsSUFBQyxNQUFNLEVBQUUsR0FBRyxJQUV4QixVQUFDLEVBQWE7NEJBQVgsd0JBQVM7d0JBQ1YsQ0FBQyxDQUFDLFNBQVM7K0JBQ04sQ0FBQyxxQkFBcUI7K0JBQ3RCLENBQUMsQ0FBQyx1QkFBdUI7K0JBQ3pCLHVCQUF1QixFQUFFLENBQUM7d0JBRS9CLE1BQU0sQ0FBQyxvQkFBQyxnQkFBZ0IsZUFBSyxpQkFBaUIsRUFBSSxDQUFBO29CQUNwRCxDQUFDLENBRWEsQ0FDZCxDQUNQLENBQUMsQ0FBQyxDQUFDLG9CQUFDLE9BQU8sSUFBQyxLQUFLLEVBQUUsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLEdBQUksQ0FFaEMsQ0FDUSxDQUN4QixDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsZUFBZSxhQUFhLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/index/view.tsx


var index_view_renderView = function (_a) {
    var props = _a.props, state = _a.state, generateCategoryHash = _a.generateCategoryHash, handleFeatureBanner = _a.handleFeatureBanner, handleFetchHotBoxes = _a.handleFetchHotBoxes, handleFetchMagazineList = _a.handleFetchMagazineList, handleFetchNewProducts = _a.handleFetchNewProducts, handleFetchPopularSearch = _a.handleFetchPopularSearch, handleFetchWatchedList = _a.handleFetchWatchedList, handleFetchActivityFeed = _a.handleFetchActivityFeed, handleFetchFooterBanner = _a.handleFetchFooterBanner;
    var switchView = {
        MOBILE: function () { return view_mobile({
            props: props,
            state: state,
            generateCategoryHash: generateCategoryHash,
            handleFeatureBanner: handleFeatureBanner,
            handleFetchHotBoxes: handleFetchHotBoxes,
            handleFetchMagazineList: handleFetchMagazineList,
            handleFetchNewProducts: handleFetchNewProducts,
            handleFetchPopularSearch: handleFetchPopularSearch,
            handleFetchWatchedList: handleFetchWatchedList,
            handleFetchActivityFeed: handleFetchActivityFeed,
            handleFetchFooterBanner: handleFetchFooterBanner,
        }); },
        DESKTOP: function () { return view_desktop({
            props: props,
            state: state,
            generateCategoryHash: generateCategoryHash,
            handleFeatureBanner: handleFeatureBanner,
            handleFetchHotBoxes: handleFetchHotBoxes,
            handleFetchMagazineList: handleFetchMagazineList,
            handleFetchNewProducts: handleFetchNewProducts,
            handleFetchPopularSearch: handleFetchPopularSearch,
            handleFetchWatchedList: handleFetchWatchedList,
            handleFetchActivityFeed: handleFetchActivityFeed,
            handleFetchFooterBanner: handleFetchFooterBanner,
        }); }
    };
    return switchView[window.DEVICE_VERSION]();
};
/* harmony default export */ var index_view = (index_view_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sWUFBWSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLGFBQWEsTUFBTSxnQkFBZ0IsQ0FBQztBQUUzQyxJQUFNLFVBQVUsR0FBRyxVQUFDLEVBWW5CO1FBWEMsZ0JBQUssRUFDTCxnQkFBSyxFQUNMLDhDQUFvQixFQUNwQiw0Q0FBbUIsRUFDbkIsNENBQW1CLEVBQ25CLG9EQUF1QixFQUN2QixrREFBc0IsRUFDdEIsc0RBQXdCLEVBQ3hCLGtEQUFzQixFQUN0QixvREFBdUIsRUFDdkIsb0RBQXVCO0lBRXZCLElBQU0sVUFBVSxHQUFHO1FBQ2pCLE1BQU0sRUFBRSxjQUFNLE9BQUEsWUFBWSxDQUFDO1lBQ3pCLEtBQUssT0FBQTtZQUNMLEtBQUssT0FBQTtZQUNMLG9CQUFvQixzQkFBQTtZQUNwQixtQkFBbUIscUJBQUE7WUFDbkIsbUJBQW1CLHFCQUFBO1lBQ25CLHVCQUF1Qix5QkFBQTtZQUN2QixzQkFBc0Isd0JBQUE7WUFDdEIsd0JBQXdCLDBCQUFBO1lBQ3hCLHNCQUFzQix3QkFBQTtZQUN0Qix1QkFBdUIseUJBQUE7WUFDdkIsdUJBQXVCLHlCQUFBO1NBQ3hCLENBQUMsRUFaWSxDQVlaO1FBQ0YsT0FBTyxFQUFFLGNBQU0sT0FBQSxhQUFhLENBQUM7WUFDM0IsS0FBSyxPQUFBO1lBQ0wsS0FBSyxPQUFBO1lBQ0wsb0JBQW9CLHNCQUFBO1lBQ3BCLG1CQUFtQixxQkFBQTtZQUNuQixtQkFBbUIscUJBQUE7WUFDbkIsdUJBQXVCLHlCQUFBO1lBQ3ZCLHNCQUFzQix3QkFBQTtZQUN0Qix3QkFBd0IsMEJBQUE7WUFDeEIsc0JBQXNCLHdCQUFBO1lBQ3RCLHVCQUF1Qix5QkFBQTtZQUN2Qix1QkFBdUIseUJBQUE7U0FDeEIsQ0FBQyxFQVphLENBWWI7S0FDSCxDQUFDO0lBRUYsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQztBQUM3QyxDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// EXTERNAL MODULE: ./action/modal.ts
var modal = __webpack_require__(84);

// EXTERNAL MODULE: ./action/banner.ts + 1 modules
var banner = __webpack_require__(781);

// EXTERNAL MODULE: ./action/love.ts + 1 modules
var love = __webpack_require__(793);

// EXTERNAL MODULE: ./action/menu.ts + 1 modules
var menu = __webpack_require__(782);

// EXTERNAL MODULE: ./action/meta.ts
var meta = __webpack_require__(754);

// EXTERNAL MODULE: ./action/shop.ts + 1 modules
var shop = __webpack_require__(762);

// EXTERNAL MODULE: ./action/tracking.ts + 1 modules
var tracking = __webpack_require__(777);

// EXTERNAL MODULE: ./action/cart.ts + 1 modules
var cart = __webpack_require__(83);

// EXTERNAL MODULE: ./action/user.ts + 1 modules
var user = __webpack_require__(349);

// EXTERNAL MODULE: ./action/magazine.ts + 1 modules
var action_magazine = __webpack_require__(761);

// EXTERNAL MODULE: ./action/activity-feed.ts + 1 modules
var activity_feed = __webpack_require__(764);

// CONCATENATED MODULE: ./container/app-shop/index/store.tsx











var mapStateToProps = function (state) { return ({
    loveStore: state.love,
    menuStore: state.menu,
    shopStore: state.shop,
    userStore: state.user,
    bannerStore: state.banner,
    magazineStore: state.magazine,
    themeStore: state.banner.theme,
    activityFeedStore: state.activityFeed
}); };
var mapDispatchToProps = function (dispatch) { return ({
    openModal: function (data) { return dispatch(Object(modal["c" /* openModalAction */])(data)); },
    fetchListMenuAction: function () { return dispatch(Object(menu["a" /* fetchListMenuAction */])()); },
    fetchMainBanner: function (data) { return dispatch(Object(banner["a" /* fetchBannerAction */])(data)); },
    fetchDataHotDealAction: function (data) { return dispatch(Object(shop["c" /* fetchDataHotDealAction */])(data)); },
    fetchDataHomePage: function () { return dispatch(Object(shop["b" /* fetchDataHomePageAction */])()); },
    fetchUserProfileAction: function () { return dispatch(Object(user["f" /* fetchUserProfileAction */])()); },
    fetchMagazineDashboard: function () { return dispatch(Object(action_magazine["d" /* fetchMagazineDashboardAction */])()); },
    fetchLoveListAction: function (data) { return dispatch(Object(love["b" /* fetchLoveListAction */])(data)); },
    fetchMagazineListAction: function (data) { return dispatch(Object(action_magazine["e" /* fetchMagazineListAction */])(data)); },
    fetchUserWatchedListAction: function (data) { return dispatch(Object(user["j" /* fetchUserWatchedListAction */])(data)); },
    trackingViewGroupAction: function (data) { return dispatch(Object(tracking["f" /* trackingViewGroupAction */])(data)); },
    fetchProductByCategory: function (data) { return dispatch(Object(shop["g" /* fetchProductByCategoryAction */])(data)); },
    clearCartAction: function () { return dispatch(Object(cart["f" /* clearCartAction */])()); },
    clearDeliveryConfigAction: function () { return dispatch(Object(cart["g" /* clearDeliveryConfigAction */])()); },
    updateMetaInfoAction: function (data) { return dispatch(Object(meta["a" /* updateMetaInfoAction */])(data)); },
    fecthActivityFeedListAction: function (data) { return dispatch(Object(activity_feed["e" /* fecthActivityFeedListAction */])(data)); }
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3hELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzNELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQzNELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQzNELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQzVELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxzQkFBc0IsRUFBRSw0QkFBNEIsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3JILE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ25FLE9BQU8sRUFBRSxlQUFlLEVBQUUseUJBQXlCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNsRixPQUFPLEVBQUUsc0JBQXNCLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUMxRixPQUFPLEVBQUUsdUJBQXVCLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNqRyxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUU1RSxNQUFNLENBQUMsSUFBTSxlQUFlLEdBQUcsVUFBQyxLQUFLLElBQUssT0FBQSxDQUFDO0lBQ3pDLFNBQVMsRUFBRSxLQUFLLENBQUMsSUFBSTtJQUNyQixTQUFTLEVBQUUsS0FBSyxDQUFDLElBQUk7SUFDckIsU0FBUyxFQUFFLEtBQUssQ0FBQyxJQUFJO0lBQ3JCLFNBQVMsRUFBRSxLQUFLLENBQUMsSUFBSTtJQUNyQixXQUFXLEVBQUUsS0FBSyxDQUFDLE1BQU07SUFDekIsYUFBYSxFQUFFLEtBQUssQ0FBQyxRQUFRO0lBQzdCLFVBQVUsRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUs7SUFDOUIsaUJBQWlCLEVBQUUsS0FBSyxDQUFDLFlBQVk7Q0FDdEMsQ0FBQyxFQVR3QyxDQVN4QyxDQUFDO0FBRUgsTUFBTSxDQUFDLElBQU0sa0JBQWtCLEdBQUcsVUFBQyxRQUFRLElBQUssT0FBQSxDQUFDO0lBQy9DLFNBQVMsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBL0IsQ0FBK0I7SUFFekQsbUJBQW1CLEVBQUUsY0FBTSxPQUFBLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLEVBQS9CLENBQStCO0lBQzFELGVBQWUsRUFBRSxVQUFDLElBQUksSUFBSyxPQUFBLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFqQyxDQUFpQztJQUM1RCxzQkFBc0IsRUFBRSxVQUFDLElBQUksSUFBSyxPQUFBLFFBQVEsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUF0QyxDQUFzQztJQUN4RSxpQkFBaUIsRUFBRSxjQUFNLE9BQUEsUUFBUSxDQUFDLHVCQUF1QixFQUFFLENBQUMsRUFBbkMsQ0FBbUM7SUFDNUQsc0JBQXNCLEVBQUUsY0FBTSxPQUFBLFFBQVEsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLEVBQWxDLENBQWtDO0lBQ2hFLHNCQUFzQixFQUFFLGNBQU0sT0FBQSxRQUFRLENBQUMsNEJBQTRCLEVBQUUsQ0FBQyxFQUF4QyxDQUF3QztJQUN0RSxtQkFBbUIsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFuQyxDQUFtQztJQUN2RSx1QkFBdUIsRUFBRSxVQUFDLElBQUksSUFBSyxPQUFBLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUF2QyxDQUF1QztJQUMxRSwwQkFBMEIsRUFBRSxVQUFDLElBQUksSUFBSyxPQUFBLFFBQVEsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUExQyxDQUEwQztJQUNoRix1QkFBdUIsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUF2QyxDQUF1QztJQUMvRSxzQkFBc0IsRUFBRSxVQUFDLElBQUksSUFBSyxPQUFBLFFBQVEsQ0FBQyw0QkFBNEIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUE1QyxDQUE0QztJQUU5RSxlQUFlLEVBQUUsY0FBTSxPQUFBLFFBQVEsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxFQUEzQixDQUEyQjtJQUNsRCx5QkFBeUIsRUFBRSxjQUFNLE9BQUEsUUFBUSxDQUFDLHlCQUF5QixFQUFFLENBQUMsRUFBckMsQ0FBcUM7SUFFdEUsb0JBQW9CLEVBQUUsVUFBQyxJQUFJLElBQUssT0FBQSxRQUFRLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBcEMsQ0FBb0M7SUFFcEUsMkJBQTJCLEVBQUUsVUFBQyxJQUFJLElBQUssT0FBQSxRQUFRLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBM0MsQ0FBMkM7Q0FDbkYsQ0FBQyxFQXJCOEMsQ0FxQjlDLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/index/container.tsx
var container_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var container_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var connect = __webpack_require__(129).connect;











var container_ShopIndexContainer = /** @class */ (function (_super) {
    container_extends(ShopIndexContainer, _super);
    function ShopIndexContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.state = index_initialize_INITIAL_STATE;
        return _this;
    }
    ShopIndexContainer.prototype.componentDidMount = function () {
        var becomeToParams = Object(format["e" /* getUrlParameter */])(this.props.location.search, 'refresh_user');
        if ('true' === becomeToParams) {
            this.props.clearCartAction();
            this.props.clearDeliveryConfigAction();
            //clearStorageUserBecome();
            this.props.fetchUserProfileAction();
        }
        // Get data  
        this.initData();
        // Set meta for SEO
        this.props.updateMetaInfoAction({
            info: {
                url: "https://www.lixibox.com",
                type: "article",
                title: 'Lixibox | Mỹ Phẩm, Dưỡng Da, Trị Mụn, Skincare, Makeup',
                description: 'Lixibox shop box mỹ phẩm cao cấp, trị mụn, dưỡng da thiết kế bởi các chuyên gia mailovesbeauty, love at first shine, changmakeup, skincare junkie ngo nhi',
                keyword: 'mỹ phẩm, dưỡng da, trị mụn, skincare, makeup, halio, lustre',
                image: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/meta/cover.jpeg'
            },
            structuredData: {
                breadcrumbList: []
            }
        });
    };
    /**
     * Init data for product / box component
     * - if without data: dispatch action to get
     * - if exsit data from state: nothing action
     */
    ShopIndexContainer.prototype.initData = function () {
        var _a = this.props, categorySlideList = _a.menuStore.categorySlideList, fetchDataHomePage = _a.fetchDataHomePage, fetchMainBanner = _a.fetchMainBanner, fetchMagazineListAction = _a.fetchMagazineListAction, fetchUserWatchedListAction = _a.fetchUserWatchedListAction, trackingViewGroupAction = _a.trackingViewGroupAction, fetchListMenuAction = _a.fetchListMenuAction, fetchDataHotDealAction = _a.fetchDataHotDealAction, fetchProductByCategory = _a.fetchProductByCategory;
        /** Home page banner */
        var fetchHomeBannerParam = {
            idBanner: application_default["a" /* BANNER_ID */].HOME_PAGE,
            limit: application_default["b" /* BANNER_LIMIT_DEFAULT */]
        };
        fetchMainBanner(fetchHomeBannerParam);
        // Fetch browse nodes
        // 0 === categorySlideList.length && isDesktopVersion() && fetchListMenuAction();
        /** Tracking view group */
        setTimeout(function () { trackingViewGroupAction({ groupObjectType: group_object_type["a" /* GROUP_OBJECT_TYPE */].HOME_PAGE, groupObjectId: group_object_type["a" /* GROUP_OBJECT_TYPE */].HOME_PAGE }); }, 3000);
        /** Get hot deal data */
        fetchDataHotDealAction({ limit: 20 });
    };
    ShopIndexContainer.prototype.componentWillReceiveProps = function (nextProps) {
        var _a = this.props, bannerList = _a.bannerStore.bannerList, hotDeal = _a.shopStore.hotDeal;
        var mainBannerHash = Object(encode["j" /* objectToHash */])({ idBanner: application_default["a" /* BANNER_ID */].HOME_PAGE, limit: application_default["b" /* BANNER_LIMIT_DEFAULT */] });
        if ((!!bannerList && !!bannerList[mainBannerHash] && !!bannerList[mainBannerHash].length
            && !!nextProps.shopStore.hotDeal && !!nextProps.shopStore.hotDeal.length) || (!!nextProps.bannerStore.bannerList && !!nextProps.bannerStore.bannerList[mainBannerHash] && !!nextProps.bannerStore.bannerList[mainBannerHash].length
            && !!hotDeal && !!hotDeal.length)) {
            this.setState({ isPriorotyBlock: false });
        }
    };
    // shouldComponentUpdate(nextProps: IProps) {
    //   /** Home page banner */
    //   const homeBannerHash = objectToHash({ idBanner: BANNER_ID.HOME_PAGE, limit: BANNER_LIMIT_DEFAULT });
    //   if (this.props.bannerStore.bannerList[homeBannerHash]
    //     && nextProps.bannerStore.bannerList[homeBannerHash]
    //     && (this.props.bannerStore.bannerList[homeBannerHash].length
    //       !== nextProps.bannerStore.bannerList[homeBannerHash].length)) {
    //     return true;
    //   }
    //   /** Feature banner banner */
    //   const featureBannerHash = objectToHash({ idBanner: BANNER_ID.HOME_FEATURE, limit: BANNER_LIMIT_DEFAULT });
    //   if (this.props.bannerStore.bannerList[featureBannerHash]
    //     && nextProps.bannerStore.bannerList[featureBannerHash]
    //     && (this.props.bannerStore.bannerList[featureBannerHash].length
    //       !== nextProps.bannerStore.bannerList[featureBannerHash].length)) {
    //     return true;
    //   }
    //   /** Home page data */
    //   if (isEmptyObject(this.props.shopStore.dataHomePage)
    //     && !isEmptyObject(nextProps.shopStore.dataHomePage)) {
    //     return true;
    //   }
    //   /** Magazine List */
    //   const defaultMagazineListHash = objectToHash({ type: MAGAZINE_LIST_TYPE.DEFAULT });
    //   if (this.props.magazineStore.magazineList[defaultMagazineListHash]
    //     && nextProps.magazineStore.magazineList[defaultMagazineListHash]
    //     && (this.props.magazineStore.magazineList[defaultMagazineListHash].length
    //       !== nextProps.magazineStore.magazineList[defaultMagazineListHash].length)) {
    //     return true;
    //   }
    //   /** Love List - Feedback */
    //   const loveListHash = objectToHash({ page: 1, per_page: 5 });
    //   if (this.props.loveStore.loveList[loveListHash]
    //     && nextProps.loveStore.loveList[loveListHash]
    //     && (this.props.loveStore.loveList[loveListHash].length
    //       !== nextProps.loveStore.loveList[loveListHash].length)) {
    //     return true;
    //   }
    //   /** Watched List */
    //   const watchedListHash = objectToHash({ page: 1, perPage: 25 });
    //   if (this.props.userStore.userWatchedList[watchedListHash]
    //     && nextProps.userStore.userWatchedList[watchedListHash]
    //     && (this.props.userStore.userWatchedList[watchedListHash].length
    //       !== nextProps.userStore.userWatchedList[watchedListHash].length)) {
    //     return true;
    //   }
    //   return false;
    // }
    ShopIndexContainer.prototype.generateCategoryHash = function (categoryFilterStatus) {
        /** Combine category filter hash by id category & params query */
        var _a = Object(uri["c" /* categoryFilterApiFormat */])(categoryFilterStatus), idCategory = _a[0], params = _a[1];
        return Object(encode["j" /* objectToHash */])({ idCategory: idCategory, params: params });
    };
    ShopIndexContainer.prototype.handleFeatureBanner = function () {
        var fetchMainBanner = this.props.fetchMainBanner;
        var _a = this.state, isFetchedFeatureBanner = _a.isFetchedFeatureBanner, isPriorotyBlock = _a.isPriorotyBlock;
        if (!!isPriorotyBlock) {
            return;
        }
        if (!isFetchedFeatureBanner) {
            var fetchFeatureBannerParam = {
                idBanner: application_default["a" /* BANNER_ID */].HOME_FEATURE,
                limit: application_default["b" /* BANNER_LIMIT_DEFAULT */]
            };
            fetchMainBanner(fetchFeatureBannerParam);
            this.setState({ isFetchedFeatureBanner: true });
        }
    };
    ShopIndexContainer.prototype.handleFetchHotBoxes = function () {
        var fetchProductByCategory = this.props.fetchProductByCategory;
        var _a = this.state, isFetchedHotBoxes = _a.isFetchedHotBoxes, isPriorotyBlock = _a.isPriorotyBlock;
        if (!!isPriorotyBlock) {
            return;
        }
        if (!isFetchedHotBoxes) {
            fetchProductByCategory({ idCategory: bestSellingParams.idCategory, searchQuery: '' });
            this.setState({ isFetchedHotBoxes: true });
        }
    };
    ShopIndexContainer.prototype.handleFetchMagazineList = function () {
        var fetchMagazineListAction = this.props.fetchMagazineListAction;
        var _a = this.state, isFetchedMagazineList = _a.isFetchedMagazineList, isPriorotyBlock = _a.isPriorotyBlock;
        if (!!isPriorotyBlock) {
            return;
        }
        if (!isFetchedMagazineList) {
            var fetchDefaultMagazineParam = {
                page: 1,
                perPage: 12,
                type: magazine["a" /* MAGAZINE_LIST_TYPE */].DEFAULT
            };
            Object(responsive["b" /* isDesktopVersion */])() && fetchMagazineListAction(fetchDefaultMagazineParam);
            this.setState({ isFetchedMagazineList: true });
        }
    };
    ShopIndexContainer.prototype.handleFetchNewProducts = function () {
        var fetchProductByCategory = this.props.fetchProductByCategory;
        var _a = this.state, isFetchedNewProducts = _a.isFetchedNewProducts, isPriorotyBlock = _a.isPriorotyBlock;
        if (!!isPriorotyBlock) {
            return;
        }
        if (!isFetchedNewProducts) {
            fetchProductByCategory({ idCategory: newProductsParams.idCategory, searchQuery: '' });
            this.setState({ isFetchedNewProducts: true });
        }
    };
    ShopIndexContainer.prototype.handleFetchPopularSearch = function () {
        var fetchMainBanner = this.props.fetchMainBanner;
        var _a = this.state, isFetchedPopularSearch = _a.isFetchedPopularSearch, isPriorotyBlock = _a.isPriorotyBlock;
        if (!!isPriorotyBlock) {
            return;
        }
        if (!isFetchedPopularSearch) {
            var fetchPopularSearchBannerParam = {
                idBanner: application_default["a" /* BANNER_ID */].POPULAR_SEARCH,
                limit: application_default["b" /* BANNER_LIMIT_DEFAULT */]
            };
            fetchMainBanner(fetchPopularSearchBannerParam);
            this.setState({ isFetchedPopularSearch: true });
        }
    };
    ShopIndexContainer.prototype.handleFetchWatchedList = function () {
        var fetchUserWatchedListAction = this.props.fetchUserWatchedListAction;
        var _a = this.state, isFetchedWatchedList = _a.isFetchedWatchedList, isPriorotyBlock = _a.isPriorotyBlock;
        if (!!isPriorotyBlock) {
            return;
        }
        if (!isFetchedWatchedList) {
            var fetchWatchedListParam = { page: 1, perPage: 10 };
            Object(responsive["b" /* isDesktopVersion */])() && fetchUserWatchedListAction(fetchWatchedListParam);
            this.setState({ isFetchedWatchedList: true });
        }
    };
    ShopIndexContainer.prototype.handleFetchActivityFeed = function () {
        var fecthActivityFeedListAction = this.props.fecthActivityFeedListAction;
        var _a = this.state, isFetchedActivityFeed = _a.isFetchedActivityFeed, isPriorotyBlock = _a.isPriorotyBlock;
        if (!!isPriorotyBlock) {
            return;
        }
        if (!isFetchedActivityFeed) {
            !!Object(responsive["b" /* isDesktopVersion */])()
                && fecthActivityFeedListAction
                && fecthActivityFeedListAction({ limit: 5, feedType: feedable["a" /* FEEDABLE_TYPE */].LOVE });
            this.setState({ isFetchedActivityFeed: true });
        }
    };
    ShopIndexContainer.prototype.handleFetchFooterBanner = function () {
        var fetchMainBanner = this.props.fetchMainBanner;
        var _a = this.state, isFetchedFooterBanner = _a.isFetchedFooterBanner, isPriorotyBlock = _a.isPriorotyBlock;
        if (!!isPriorotyBlock) {
            return;
        }
        var fetchFooterBannerParam = {
            idBanner: application_default["a" /* BANNER_ID */].FOOTER,
            limit: application_default["b" /* BANNER_LIMIT_DEFAULT */]
        };
        if (!isFetchedFooterBanner) {
            !!fetchMainBanner
                && fetchMainBanner(fetchFooterBannerParam);
            this.setState({ isFetchedFooterBanner: true });
        }
    };
    ShopIndexContainer.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state,
            handleFeatureBanner: this.handleFeatureBanner.bind(this),
            handleFetchHotBoxes: this.handleFetchHotBoxes.bind(this),
            handleFetchMagazineList: this.handleFetchMagazineList.bind(this),
            handleFetchNewProducts: this.handleFetchNewProducts.bind(this),
            generateCategoryHash: this.generateCategoryHash.bind(this),
            handleFetchPopularSearch: this.handleFetchPopularSearch.bind(this),
            handleFetchWatchedList: this.handleFetchWatchedList.bind(this),
            handleFetchActivityFeed: this.handleFetchActivityFeed.bind(this),
            handleFetchFooterBanner: this.handleFetchFooterBanner.bind(this),
        };
        return index_view(args);
    };
    ShopIndexContainer.defaultProps = index_initialize_DEFAULT_PROPS;
    ShopIndexContainer = container_decorate([
        radium
    ], ShopIndexContainer);
    return ShopIndexContainer;
}(react["Component"]));
;
/* harmony default export */ var container = __webpack_exports__["default"] = (Object(react_router_dom["withRouter"])(connect(mapStateToProps, mapDispatchToProps)(container_ShopIndexContainer)));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQzlDLE9BQU8sS0FBSyxNQUFNLE1BQU0sUUFBUSxDQUFDO0FBQ2pDLElBQU0sT0FBTyxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQyxPQUFPLENBQUM7QUFFL0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBRXhELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDeEUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDN0QsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDckYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDN0UsT0FBTyxFQUFFLG9CQUFvQixFQUFFLFNBQVMsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQ3pGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSx1QkFBdUIsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRWhGLE9BQU8sVUFBVSxNQUFNLFFBQVEsQ0FBQztBQUVoQyxPQUFPLEVBQUUsZUFBZSxFQUFFLGtCQUFrQixFQUFFLE1BQU0sU0FBUyxDQUFDO0FBQzlELE9BQU8sRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLGlCQUFpQixFQUFFLGlCQUFpQixFQUFFLE1BQU0sY0FBYyxDQUFDO0FBSWxHO0lBQWlDLHNDQUErQjtJQUc5RCw0QkFBWSxLQUFhO1FBQXpCLFlBQ0Usa0JBQU0sS0FBSyxDQUFDLFNBRWI7UUFEQyxLQUFJLENBQUMsS0FBSyxHQUFHLGFBQXVCLENBQUM7O0lBQ3ZDLENBQUM7SUFFRCw4Q0FBaUIsR0FBakI7UUFDRSxJQUFNLGNBQWMsR0FBRyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLGNBQWMsQ0FBQyxDQUFDO1FBQ25GLEVBQUUsQ0FBQyxDQUFDLE1BQU0sS0FBSyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQzlCLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDN0IsSUFBSSxDQUFDLEtBQUssQ0FBQyx5QkFBeUIsRUFBRSxDQUFDO1lBQ3ZDLDJCQUEyQjtZQUMzQixJQUFJLENBQUMsS0FBSyxDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFDdEMsQ0FBQztRQUVELGFBQWE7UUFDYixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFaEIsbUJBQW1CO1FBQ25CLElBQUksQ0FBQyxLQUFLLENBQUMsb0JBQW9CLENBQUM7WUFDOUIsSUFBSSxFQUFFO2dCQUNKLEdBQUcsRUFBRSx5QkFBeUI7Z0JBQzlCLElBQUksRUFBRSxTQUFTO2dCQUNmLEtBQUssRUFBRSx3REFBd0Q7Z0JBQy9ELFdBQVcsRUFBRSwySkFBMko7Z0JBQ3hLLE9BQU8sRUFBRSw2REFBNkQ7Z0JBQ3RFLEtBQUssRUFBRSxpQkFBaUIsR0FBRyxnQ0FBZ0M7YUFDNUQ7WUFDRCxjQUFjLEVBQUU7Z0JBQ2QsY0FBYyxFQUFFLEVBQUU7YUFDbkI7U0FDRixDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILHFDQUFRLEdBQVI7UUFDUSxJQUFBLGVBVWtCLEVBVFQsa0RBQWlCLEVBQzlCLHdDQUFpQixFQUNqQixvQ0FBZSxFQUNmLG9EQUF1QixFQUN2QiwwREFBMEIsRUFDMUIsb0RBQXVCLEVBQ3ZCLDRDQUFtQixFQUNuQixrREFBc0IsRUFDdEIsa0RBQXNCLENBQ0M7UUFFekIsdUJBQXVCO1FBQ3ZCLElBQU0sb0JBQW9CLEdBQUc7WUFDM0IsUUFBUSxFQUFFLFNBQVMsQ0FBQyxTQUFTO1lBQzdCLEtBQUssRUFBRSxvQkFBb0I7U0FDNUIsQ0FBQztRQUVGLGVBQWUsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBRXRDLHFCQUFxQjtRQUNyQixpRkFBaUY7UUFFakYsMEJBQTBCO1FBQzFCLFVBQVUsQ0FBQyxjQUFRLHVCQUF1QixDQUFDLEVBQUUsZUFBZSxFQUFFLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxhQUFhLEVBQUUsaUJBQWlCLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVsSix3QkFBd0I7UUFDeEIsc0JBQXNCLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUN4QyxDQUFDO0lBRUQsc0RBQXlCLEdBQXpCLFVBQTBCLFNBQVM7UUFDM0IsSUFBQSxlQUdRLEVBRkcsc0NBQVUsRUFDWiw4QkFBTyxDQUNQO1FBRWYsSUFBTSxjQUFjLEdBQUcsWUFBWSxDQUFDLEVBQUUsUUFBUSxFQUFFLFNBQVMsQ0FBQyxTQUFTLEVBQUUsS0FBSyxFQUFFLG9CQUFvQixFQUFFLENBQUMsQ0FBQztRQUVwRyxFQUFFLENBQUMsQ0FDRCxDQUNFLENBQUMsQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDLE1BQU07ZUFDaEYsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsT0FBTyxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQ3pFLElBQUksQ0FDSCxDQUFDLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsQ0FBQyxNQUFNO2VBQ2xKLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBRXBDLENBQUMsQ0FBQyxDQUFDO1lBQ0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLGVBQWUsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFBO1FBQzNDLENBQUM7SUFDSCxDQUFDO0lBRUQsNkNBQTZDO0lBQzdDLDRCQUE0QjtJQUM1Qix5R0FBeUc7SUFDekcsMERBQTBEO0lBQzFELDBEQUEwRDtJQUMxRCxtRUFBbUU7SUFDbkUsd0VBQXdFO0lBQ3hFLG1CQUFtQjtJQUNuQixNQUFNO0lBRU4saUNBQWlDO0lBQ2pDLCtHQUErRztJQUMvRyw2REFBNkQ7SUFDN0QsNkRBQTZEO0lBQzdELHNFQUFzRTtJQUN0RSwyRUFBMkU7SUFDM0UsbUJBQW1CO0lBQ25CLE1BQU07SUFFTiwwQkFBMEI7SUFDMUIseURBQXlEO0lBQ3pELDZEQUE2RDtJQUM3RCxtQkFBbUI7SUFDbkIsTUFBTTtJQUVOLHlCQUF5QjtJQUN6Qix3RkFBd0Y7SUFDeEYsdUVBQXVFO0lBQ3ZFLHVFQUF1RTtJQUN2RSxnRkFBZ0Y7SUFDaEYscUZBQXFGO0lBQ3JGLG1CQUFtQjtJQUNuQixNQUFNO0lBRU4sZ0NBQWdDO0lBQ2hDLGlFQUFpRTtJQUNqRSxvREFBb0Q7SUFDcEQsb0RBQW9EO0lBQ3BELDZEQUE2RDtJQUM3RCxrRUFBa0U7SUFDbEUsbUJBQW1CO0lBQ25CLE1BQU07SUFHTix3QkFBd0I7SUFDeEIsb0VBQW9FO0lBQ3BFLDhEQUE4RDtJQUM5RCw4REFBOEQ7SUFDOUQsdUVBQXVFO0lBQ3ZFLDRFQUE0RTtJQUM1RSxtQkFBbUI7SUFDbkIsTUFBTTtJQUVOLGtCQUFrQjtJQUNsQixJQUFJO0lBRUosaURBQW9CLEdBQXBCLFVBQXFCLG9CQUFvQjtRQUN2QyxpRUFBaUU7UUFDM0QsSUFBQSxrREFBb0UsRUFBbkUsa0JBQVUsRUFBRSxjQUFNLENBQWtEO1FBQzNFLE1BQU0sQ0FBQyxZQUFZLENBQUMsRUFBRSxVQUFVLFlBQUEsRUFBRSxNQUFNLFFBQUEsRUFBRSxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVELGdEQUFtQixHQUFuQjtRQUNVLElBQUEsNENBQWUsQ0FBZ0I7UUFDakMsSUFBQSxlQUF3RCxFQUF0RCxrREFBc0IsRUFBRSxvQ0FBZSxDQUFnQjtRQUUvRCxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQztRQUFDLENBQUM7UUFFbEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7WUFDNUIsSUFBTSx1QkFBdUIsR0FBRztnQkFDOUIsUUFBUSxFQUFFLFNBQVMsQ0FBQyxZQUFZO2dCQUNoQyxLQUFLLEVBQUUsb0JBQW9CO2FBQzVCLENBQUM7WUFFRixlQUFlLENBQUMsdUJBQXVCLENBQUMsQ0FBQztZQUN6QyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUNsRCxDQUFDO0lBQ0gsQ0FBQztJQUVELGdEQUFtQixHQUFuQjtRQUNVLElBQUEsMERBQXNCLENBQWdCO1FBQ3hDLElBQUEsZUFBbUQsRUFBakQsd0NBQWlCLEVBQUUsb0NBQWUsQ0FBZ0I7UUFFMUQsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUM7UUFBQyxDQUFDO1FBRWxDLEVBQUUsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO1lBQ3ZCLHNCQUFzQixDQUFDLEVBQUUsVUFBVSxFQUFFLGlCQUFpQixDQUFDLFVBQVUsRUFBRSxXQUFXLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUN0RixJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsaUJBQWlCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUM3QyxDQUFDO0lBQ0gsQ0FBQztJQUVELG9EQUF1QixHQUF2QjtRQUNVLElBQUEsNERBQXVCLENBQWdCO1FBQ3pDLElBQUEsZUFBdUQsRUFBckQsZ0RBQXFCLEVBQUUsb0NBQWUsQ0FBZ0I7UUFFOUQsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUM7UUFBQyxDQUFDO1FBRWxDLEVBQUUsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDO1lBQzNCLElBQU0seUJBQXlCLEdBQUc7Z0JBQ2hDLElBQUksRUFBRSxDQUFDO2dCQUNQLE9BQU8sRUFBRSxFQUFFO2dCQUNYLElBQUksRUFBRSxrQkFBa0IsQ0FBQyxPQUFPO2FBQ2pDLENBQUM7WUFFRixnQkFBZ0IsRUFBRSxJQUFJLHVCQUF1QixDQUFDLHlCQUF5QixDQUFDLENBQUM7WUFDekUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLHFCQUFxQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7UUFDakQsQ0FBQztJQUNILENBQUM7SUFFRCxtREFBc0IsR0FBdEI7UUFDVSxJQUFBLDBEQUFzQixDQUFnQjtRQUN4QyxJQUFBLGVBQXNELEVBQXBELDhDQUFvQixFQUFFLG9DQUFlLENBQWdCO1FBRTdELEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDO1FBQUMsQ0FBQztRQUVsQyxFQUFFLENBQUMsQ0FBQyxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQztZQUMxQixzQkFBc0IsQ0FBQyxFQUFFLFVBQVUsRUFBRSxpQkFBaUIsQ0FBQyxVQUFVLEVBQUUsV0FBVyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDdEYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLG9CQUFvQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7UUFDaEQsQ0FBQztJQUNILENBQUM7SUFFRCxxREFBd0IsR0FBeEI7UUFDVSxJQUFBLDRDQUFlLENBQWdCO1FBQ2pDLElBQUEsZUFBd0QsRUFBdEQsa0RBQXNCLEVBQUUsb0NBQWUsQ0FBZ0I7UUFFL0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUM7UUFBQyxDQUFDO1FBRWxDLEVBQUUsQ0FBQyxDQUFDLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDO1lBQzVCLElBQU0sNkJBQTZCLEdBQUc7Z0JBQ3BDLFFBQVEsRUFBRSxTQUFTLENBQUMsY0FBYztnQkFDbEMsS0FBSyxFQUFFLG9CQUFvQjthQUM1QixDQUFDO1lBRUYsZUFBZSxDQUFDLDZCQUE2QixDQUFDLENBQUE7WUFDOUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLHNCQUFzQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7UUFDbEQsQ0FBQztJQUNILENBQUM7SUFFRCxtREFBc0IsR0FBdEI7UUFDVSxJQUFBLGtFQUEwQixDQUFnQjtRQUM1QyxJQUFBLGVBQXNELEVBQXBELDhDQUFvQixFQUFFLG9DQUFlLENBQWdCO1FBRTdELEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDO1FBQUMsQ0FBQztRQUVsQyxFQUFFLENBQUMsQ0FBQyxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQztZQUMxQixJQUFNLHFCQUFxQixHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUM7WUFDdkQsZ0JBQWdCLEVBQUUsSUFBSSwwQkFBMEIsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQ3hFLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxvQkFBb0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBQ2hELENBQUM7SUFDSCxDQUFDO0lBRUQsb0RBQXVCLEdBQXZCO1FBQ1UsSUFBQSxvRUFBMkIsQ0FBZ0I7UUFDN0MsSUFBQSxlQUF1RCxFQUFyRCxnREFBcUIsRUFBRSxvQ0FBZSxDQUFnQjtRQUU5RCxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQztRQUFDLENBQUM7UUFFbEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUM7WUFDM0IsQ0FBQyxDQUFDLGdCQUFnQixFQUFFO21CQUNmLDJCQUEyQjttQkFDM0IsMkJBQTJCLENBQUMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztZQUM3RSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUscUJBQXFCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUNqRCxDQUFDO0lBQ0gsQ0FBQztJQUVELG9EQUF1QixHQUF2QjtRQUNVLElBQUEsNENBQWUsQ0FBZ0I7UUFDakMsSUFBQSxlQUF1RCxFQUFyRCxnREFBcUIsRUFBRSxvQ0FBZSxDQUFnQjtRQUU5RCxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQztRQUFDLENBQUM7UUFHbEMsSUFBTSxzQkFBc0IsR0FBRztZQUM3QixRQUFRLEVBQUUsU0FBUyxDQUFDLE1BQU07WUFDMUIsS0FBSyxFQUFFLG9CQUFvQjtTQUM1QixDQUFDO1FBRUYsRUFBRSxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUM7WUFDM0IsQ0FBQyxDQUFDLGVBQWU7bUJBQ1osZUFBZSxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLHFCQUFxQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7UUFDakQsQ0FBQztJQUNILENBQUM7SUFFRCxtQ0FBTSxHQUFOO1FBQ0UsSUFBTSxJQUFJLEdBQUc7WUFDWCxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLG1CQUFtQixFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ3hELG1CQUFtQixFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ3hELHVCQUF1QixFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2hFLHNCQUFzQixFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzlELG9CQUFvQixFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzFELHdCQUF3QixFQUFFLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2xFLHNCQUFzQixFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzlELHVCQUF1QixFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2hFLHVCQUF1QixFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ2pFLENBQUM7UUFFRixNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFwU00sK0JBQVksR0FBVyxhQUFhLENBQUM7SUFEeEMsa0JBQWtCO1FBRHZCLE1BQU07T0FDRCxrQkFBa0IsQ0FzU3ZCO0lBQUQseUJBQUM7Q0FBQSxBQXRTRCxDQUFpQyxLQUFLLENBQUMsU0FBUyxHQXNTL0M7QUFBQSxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQ3ZCLE9BQU8sQ0FDTCxlQUFlLEVBQ2Ysa0JBQWtCLENBQ25CLENBQUMsa0JBQWtCLENBQUMsQ0FDdEIsQ0FBQyJ9

/***/ })

}]);