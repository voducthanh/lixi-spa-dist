(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[13],{

/***/ 838:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./action/cart.ts + 1 modules
var cart = __webpack_require__(83);

// EXTERNAL MODULE: ./action/like.ts + 1 modules
var like = __webpack_require__(776);

// EXTERNAL MODULE: ./action/alert.ts
var action_alert = __webpack_require__(16);

// CONCATENATED MODULE: ./components/wish-list/store.tsx



var mapStateToProps = function (state) { return ({
    cartStore: state.cart,
}); };
var mapDispatchToProps = function (dispatch) { return ({
    addItemToCartAction: function (_a) {
        var boxId = _a.boxId, quantity = _a.quantity, displayCartSumary = _a.displayCartSumary;
        return dispatch(Object(cart["b" /* addItemToCartAction */])({ boxId: boxId, quantity: quantity, displayCartSumary: displayCartSumary }));
    },
    unLikeProduct: function (productId) { return dispatch(Object(like["a" /* UnLikeProductAction */])(productId)); },
    openAlert: function (data) { return dispatch(Object(action_alert["c" /* openAlertAction */])(data)); },
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDeEQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDeEQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXJELE1BQU0sQ0FBQyxJQUFNLGVBQWUsR0FBRyxVQUFDLEtBQUssSUFBSyxPQUFBLENBQUM7SUFDekMsU0FBUyxFQUFFLEtBQUssQ0FBQyxJQUFJO0NBQ3RCLENBQUMsRUFGd0MsQ0FFeEMsQ0FBQztBQUVILE1BQU0sQ0FBQyxJQUFNLGtCQUFrQixHQUFHLFVBQUMsUUFBUSxJQUFLLE9BQUEsQ0FBQztJQUMvQyxtQkFBbUIsRUFBRSxVQUFDLEVBQXNDO1lBQXBDLGdCQUFLLEVBQUUsc0JBQVEsRUFBRSx3Q0FBaUI7UUFDeEQsT0FBQSxRQUFRLENBQUMsbUJBQW1CLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxRQUFRLFVBQUEsRUFBRSxpQkFBaUIsbUJBQUEsRUFBRSxDQUFDLENBQUM7SUFBckUsQ0FBcUU7SUFDdkUsYUFBYSxFQUFFLFVBQUMsU0FBUyxJQUFLLE9BQUEsUUFBUSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQXhDLENBQXdDO0lBQ3RFLFNBQVMsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBL0IsQ0FBK0I7Q0FDMUQsQ0FBQyxFQUw4QyxDQUs5QyxDQUFDIn0=
// CONCATENATED MODULE: ./components/wish-list/initialize.tsx
var DEFAULT_PROPS = {
    list: [],
    title: '',
    showHeader: true,
    listLikedId: [],
    urlList: []
};
var INITIAL_STATE = {
    isLoadingAddToCard: false,
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLEtBQUssRUFBRSxFQUFFO0lBQ1QsVUFBVSxFQUFFLElBQUk7SUFDaEIsV0FBVyxFQUFFLEVBQUU7SUFDZixPQUFPLEVBQUUsRUFBRTtDQUNNLENBQUM7QUFFcEIsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHO0lBQzNCLGtCQUFrQixFQUFFLEtBQUs7Q0FDUixDQUFDIn0=
// EXTERNAL MODULE: ./components/general/pagination/index.tsx + 4 modules
var pagination = __webpack_require__(752);

// EXTERNAL MODULE: ./container/layout/main-block/index.tsx
var main_block = __webpack_require__(747);

// CONCATENATED MODULE: ./components/product/wish-item/store.tsx
var store_mapStateToProps = function (state) { return ({
    cartStore: state.cart
}); };
var store_mapDispatchToProps = function (dispatch) { return ({}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBSUEsTUFBTSxDQUFDLElBQU0sZUFBZSxHQUFHLFVBQUMsS0FBSyxJQUFLLE9BQUEsQ0FBQztJQUN6QyxTQUFTLEVBQUUsS0FBSyxDQUFDLElBQUk7Q0FDdEIsQ0FBQyxFQUZ3QyxDQUV4QyxDQUFDO0FBRUgsTUFBTSxDQUFDLElBQU0sa0JBQWtCLEdBQUcsVUFBQyxRQUFRLElBQUssT0FBQSxDQUFDLEVBQUUsQ0FBQyxFQUFKLENBQUksQ0FBQyJ9
// CONCATENATED MODULE: ./components/product/wish-item/initialize.tsx
var initialize_DEFAULT_PROPS = {};
var initialize_INITIAL_STATE = {
    isLoadingAddToCard: false,
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRyxFQUFZLENBQUM7QUFFMUMsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHO0lBQzNCLGtCQUFrQixFQUFFLEtBQUs7Q0FDaEIsQ0FBQyJ9
// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./utils/currency.ts
var currency = __webpack_require__(754);

// EXTERNAL MODULE: ./constants/application/alert.ts
var application_alert = __webpack_require__(15);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./components/ui/submit-button/index.tsx + 4 modules
var submit_button = __webpack_require__(749);

// EXTERNAL MODULE: ./components/ui/rating-star/index.tsx + 4 modules
var rating_star = __webpack_require__(763);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(347);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./components/product/wish-item/style.tsx


/* harmony default export */ var wish_item_style = ({
    container: {},
    row: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{ padding: "10px 10px 0px 10px", marginBottom: 0 }],
        DESKTOP: [{
                paddingLeft: 0,
                paddingRight: 0,
                paddingTop: 0,
                paddingBottom: 0,
                marginBottom: 20
            }],
        GENERAL: [{
                display: variable["display"].flex,
                justifyContent: 'space-between',
                flexWrap: 'wrap'
            }]
    }),
    contentGroup: {
        container: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{ marginBottom: 10, width: '100%' }],
            DESKTOP: [{ marginBottom: 20, width: 'calc(50% - 10px)' }],
            GENERAL: [{
                    borderRadius: 3,
                    boxShadow: variable["shadowBlurSort"],
                    paddingLeft: 10,
                    paddingRight: 10,
                    paddingTop: 10,
                    paddingBottom: 10
                }]
        }),
        inner: {
            display: variable["display"].flex,
            justifyContent: 'space-between',
        },
        imgGroup: {
            display: variable["display"].flex,
            flexDirection: 'column',
            justifyContent: 'space-between',
            width: 'calc(40% - 10px)',
            img: {
                backgroundSize: 'contain',
                backgroundPosition: 'center center',
                backgroundRepeat: 'no-repeat',
                width: '100%',
                paddingTop: '100%'
            },
            btn: {
                marginBottom: 0,
                maxWidth: 200,
            }
        },
        rateGroup: {
            display: variable["display"].flex,
            flexDirection: 'column',
            justifyContent: 'space-between',
            width: 'calc(60%)',
            header: {
                title: {
                    fontSize: 16,
                    marginBottom: 5,
                    color: variable["colorBlack"],
                },
                price: {
                    fontSize: 15,
                    marginBottom: 5,
                },
                date: {
                    fontSize: 12,
                    color: variable["colorBlack05"],
                },
                rateGroup: {
                    container: Object(responsive["a" /* combineStyle */])({
                        MOBILE: [{ flexDirection: 'column' }],
                        DESKTOP: [{ alignItems: 'center', flexDirection: 'row' }],
                        GENERAL: [{ display: variable["display"].flex, marginBottom: 10 }]
                    }),
                    ratingStarIcon: {
                        display: variable["display"].flex,
                        alignItems: 'center',
                    },
                    slash: Object(responsive["a" /* combineStyle */])({
                        MOBILE: [{ display: variable["display"].none }],
                        DESKTOP: [{ display: variable["display"].block }],
                        GENERAL: [{ color: variable["colorBlack05"] }]
                    }),
                    inner: {
                        width: 15,
                    },
                    rate: {
                        padding: '0px 5px',
                        height: 30,
                        lineHeight: '35px',
                    },
                    love: Object(responsive["a" /* combineStyle */])({
                        MOBILE: [{ lineHeight: '32px' }],
                        DESKTOP: [{ lineHeight: '35px' }],
                        GENERAL: [{ height: 30 }]
                    })
                },
                review: {
                    maxHeight: 200,
                    overflow: 'hidden',
                    fontSize: 14,
                },
            },
            btn: {
                marginBottom: 0
            },
        },
    },
    content: {
        container: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{ margin: '0 10px 10px 10px' }],
            DESKTOP: [{ margin: '0 0 20px 0' }],
            GENERAL: [{
                    display: variable["display"].flex,
                    width: '100%',
                    borderRadius: '3px',
                    border: "1px solid " + variable["colorD2"],
                }]
        }),
        imageGroup: {
            display: variable["display"].block,
            padding: '10px',
            image: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        width: window.innerWidth <= variable["breakPoint320"] ? '60px' : '100px',
                        height: window.innerWidth <= variable["breakPoint320"] ? '60px' : '100px'
                    }],
                DESKTOP: [{ width: '200px', height: '175px' }],
                GENERAL: [{
                        backgroundSize: 'cover',
                        backgroundPosition: 'center center',
                        padding: '10px'
                    }]
            }),
        }
    },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFekQsZUFBZTtJQUNiLFNBQVMsRUFBRSxFQUFFO0lBRWIsR0FBRyxFQUFFLFlBQVksQ0FBQztRQUNoQixNQUFNLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxZQUFZLEVBQUUsQ0FBQyxFQUFFLENBQUM7UUFDNUQsT0FBTyxFQUFFLENBQUM7Z0JBQ1IsV0FBVyxFQUFFLENBQUM7Z0JBQ2QsWUFBWSxFQUFFLENBQUM7Z0JBQ2YsVUFBVSxFQUFFLENBQUM7Z0JBQ2IsYUFBYSxFQUFFLENBQUM7Z0JBQ2hCLFlBQVksRUFBRSxFQUFFO2FBQ2pCLENBQUM7UUFDRixPQUFPLEVBQUUsQ0FBQztnQkFDUixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixjQUFjLEVBQUUsZUFBZTtnQkFDL0IsUUFBUSxFQUFFLE1BQU07YUFDakIsQ0FBQztLQUNILENBQUM7SUFFRixZQUFZLEVBQUU7UUFDWixTQUFTLEVBQUUsWUFBWSxDQUFDO1lBQ3RCLE1BQU0sRUFBRSxDQUFDLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLENBQUM7WUFDN0MsT0FBTyxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxrQkFBa0IsRUFBRSxDQUFDO1lBQzFELE9BQU8sRUFBRSxDQUFDO29CQUNSLFlBQVksRUFBRSxDQUFDO29CQUNmLFNBQVMsRUFBRSxRQUFRLENBQUMsY0FBYztvQkFDbEMsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLFVBQVUsRUFBRSxFQUFFO29CQUNkLGFBQWEsRUFBRSxFQUFFO2lCQUNsQixDQUFDO1NBQ0gsQ0FBQztRQUVGLEtBQUssRUFBRTtZQUNMLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsY0FBYyxFQUFFLGVBQWU7U0FDaEM7UUFFRCxRQUFRLEVBQUU7WUFDUixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLGFBQWEsRUFBRSxRQUFRO1lBQ3ZCLGNBQWMsRUFBRSxlQUFlO1lBQy9CLEtBQUssRUFBRSxrQkFBa0I7WUFFekIsR0FBRyxFQUFFO2dCQUNILGNBQWMsRUFBRSxTQUFTO2dCQUN6QixrQkFBa0IsRUFBRSxlQUFlO2dCQUNuQyxnQkFBZ0IsRUFBRSxXQUFXO2dCQUM3QixLQUFLLEVBQUUsTUFBTTtnQkFDYixVQUFVLEVBQUUsTUFBTTthQUNuQjtZQUVELEdBQUcsRUFBRTtnQkFDSCxZQUFZLEVBQUUsQ0FBQztnQkFDZixRQUFRLEVBQUUsR0FBRzthQUNkO1NBQ0Y7UUFFRCxTQUFTLEVBQUU7WUFDVCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLGFBQWEsRUFBRSxRQUFRO1lBQ3ZCLGNBQWMsRUFBRSxlQUFlO1lBQy9CLEtBQUssRUFBRSxXQUFXO1lBRWxCLE1BQU0sRUFBRTtnQkFDTixLQUFLLEVBQUU7b0JBQ0wsUUFBUSxFQUFFLEVBQUU7b0JBQ1osWUFBWSxFQUFFLENBQUM7b0JBQ2YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2lCQUMzQjtnQkFFRCxLQUFLLEVBQUU7b0JBQ0wsUUFBUSxFQUFFLEVBQUU7b0JBQ1osWUFBWSxFQUFFLENBQUM7aUJBQ2hCO2dCQUVELElBQUksRUFBRTtvQkFDSixRQUFRLEVBQUUsRUFBRTtvQkFDWixLQUFLLEVBQUUsUUFBUSxDQUFDLFlBQVk7aUJBQzdCO2dCQUVELFNBQVMsRUFBRTtvQkFDVCxTQUFTLEVBQUUsWUFBWSxDQUFDO3dCQUN0QixNQUFNLEVBQUUsQ0FBQyxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsQ0FBQzt3QkFDckMsT0FBTyxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsQ0FBQzt3QkFDekQsT0FBTyxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxDQUFDO3FCQUNoRSxDQUFDO29CQUVGLGNBQWMsRUFBRTt3QkFDZCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO3dCQUM5QixVQUFVLEVBQUUsUUFBUTtxQkFDckI7b0JBRUQsS0FBSyxFQUFFLFlBQVksQ0FBQzt3QkFDbEIsTUFBTSxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDNUMsT0FBTyxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQzt3QkFDOUMsT0FBTyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsUUFBUSxDQUFDLFlBQVksRUFBRSxDQUFDO3FCQUM1QyxDQUFDO29CQUVGLEtBQUssRUFBRTt3QkFDTCxLQUFLLEVBQUUsRUFBRTtxQkFDVjtvQkFFRCxJQUFJLEVBQUU7d0JBQ0osT0FBTyxFQUFFLFNBQVM7d0JBQ2xCLE1BQU0sRUFBRSxFQUFFO3dCQUNWLFVBQVUsRUFBRSxNQUFNO3FCQUNuQjtvQkFFRCxJQUFJLEVBQUUsWUFBWSxDQUFDO3dCQUNqQixNQUFNLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsQ0FBQzt3QkFDaEMsT0FBTyxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLENBQUM7d0JBQ2pDLE9BQU8sRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxDQUFDO3FCQUMxQixDQUFDO2lCQUNIO2dCQUVELE1BQU0sRUFBRTtvQkFDTixTQUFTLEVBQUUsR0FBRztvQkFDZCxRQUFRLEVBQUUsUUFBUTtvQkFDbEIsUUFBUSxFQUFFLEVBQUU7aUJBQ2I7YUFDRjtZQUVELEdBQUcsRUFBRTtnQkFDSCxZQUFZLEVBQUUsQ0FBQzthQUNoQjtTQUNGO0tBQ0Y7SUFFRCxPQUFPLEVBQUU7UUFDUCxTQUFTLEVBQUUsWUFBWSxDQUFDO1lBQ3RCLE1BQU0sRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFFLGtCQUFrQixFQUFFLENBQUM7WUFDeEMsT0FBTyxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLENBQUM7WUFDbkMsT0FBTyxFQUFFLENBQUM7b0JBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtvQkFDOUIsS0FBSyxFQUFFLE1BQU07b0JBQ2IsWUFBWSxFQUFFLEtBQUs7b0JBQ25CLE1BQU0sRUFBRSxlQUFhLFFBQVEsQ0FBQyxPQUFTO2lCQUN4QyxDQUFDO1NBQ0gsQ0FBQztRQUVGLFVBQVUsRUFBRTtZQUNWLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDL0IsT0FBTyxFQUFFLE1BQU07WUFFZixLQUFLLEVBQUUsWUFBWSxDQUFDO2dCQUNsQixNQUFNLEVBQUUsQ0FBQzt3QkFDUCxLQUFLLEVBQUUsTUFBTSxDQUFDLFVBQVUsSUFBSSxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU87d0JBQ3JFLE1BQU0sRUFBRSxNQUFNLENBQUMsVUFBVSxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTztxQkFDdkUsQ0FBQztnQkFFRixPQUFPLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxDQUFDO2dCQUM5QyxPQUFPLEVBQUUsQ0FBQzt3QkFDUixjQUFjLEVBQUUsT0FBTzt3QkFDdkIsa0JBQWtCLEVBQUUsZUFBZTt3QkFDbkMsT0FBTyxFQUFFLE1BQU07cUJBQ2hCLENBQUM7YUFDSCxDQUFDO1NBQ0g7S0FDRjtDQUNNLENBQUMifQ==
// CONCATENATED MODULE: ./components/product/wish-item/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};










function renderComponent(_a) {
    var props = _a.props, state = _a.state, handleAddToCart = _a.handleAddToCart;
    var _b = props, openAlert = _b.openAlert, addItemToCartAction = _b.addItemToCartAction, unLikeProduct = _b.unLikeProduct, item = _b.item;
    var isLoadingAddToCard = state.isLoadingAddToCard;
    var contentGroupStyle = wish_item_style.contentGroup.rateGroup.header;
    var itemProps = {
        to: routing["Za" /* ROUTING_PRODUCT_DETAIL_PATH */] + "/" + item.slug,
        key: item.id
    };
    var imageProps = {
        style: [
            wish_item_style.contentGroup.imgGroup.img,
            { backgroundImage: "url(" + item.primary_picture.medium_url + ")" }
        ]
    };
    var ratingStarProps = {
        value: item.rating.avg_rate || 0,
    };
    return (react["createElement"]("div", { style: wish_item_style.contentGroup.container },
        react["createElement"]("div", { style: wish_item_style.contentGroup.inner },
            react["createElement"]("div", { style: wish_item_style.contentGroup.imgGroup },
                react["createElement"](react_router_dom["NavLink"], __assign({}, itemProps),
                    react["createElement"]("div", __assign({}, imageProps))),
                react["createElement"](submit_button["a" /* default */], { loading: isLoadingAddToCard, color: 'white', icon: 'cart-line', styleIcon: { color: variable["colorPink"], marginRight: '0px' }, onSubmit: function () { return handleAddToCart(item.id); }, style: Object.assign({}, wish_item_style.contentGroup.imgGroup.btn) })),
            react["createElement"]("div", { style: wish_item_style.contentGroup.rateGroup },
                react["createElement"]("div", { style: contentGroupStyle },
                    react["createElement"]("div", { style: contentGroupStyle.title }, item.name),
                    react["createElement"]("div", { style: contentGroupStyle.price }, Object(currency["a" /* currenyFormat */])(item.original_price)),
                    react["createElement"]("div", { style: contentGroupStyle.rateGroup.container },
                        react["createElement"]("div", { style: contentGroupStyle.rateGroup.ratingStarIcon },
                            react["createElement"](rating_star["a" /* default */], __assign({}, ratingStarProps)),
                            react["createElement"]("span", { style: contentGroupStyle.rateGroup.rate }, item.rating.count)),
                        react["createElement"]("span", { style: contentGroupStyle.rateGroup.slash }, "|"),
                        react["createElement"]("div", { style: contentGroupStyle.rateGroup.ratingStarIcon },
                            react["createElement"](icon["a" /* default */], { innerStyle: contentGroupStyle.rateGroup.inner, name: 'heart-full' }),
                            react["createElement"]("div", { style: contentGroupStyle.rateGroup.love }, item.like_count)))),
                react["createElement"](submit_button["a" /* default */], { title: 'BỎ YÊU THÍCH', icon: 'heart-line', color: 'borderGrey', style: Object.assign({}, wish_item_style.contentGroup.rateGroup.btn), onSubmit: function () {
                        unLikeProduct(item.id);
                        openAlert(application_alert["q" /* ALERT_UNLIKE_PRODUCT */]);
                    } })))));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRzNDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUV4RCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUU1RSxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUlyRixPQUFPLFlBQVksTUFBTSx3QkFBd0IsQ0FBQztBQUNsRCxPQUFPLFVBQVUsTUFBTSxzQkFBc0IsQ0FBQztBQUM5QyxPQUFPLElBQUksTUFBTSxlQUFlLENBQUM7QUFHakMsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsTUFBTSwwQkFBMEIsRUFBaUM7UUFBL0IsZ0JBQUssRUFBRSxnQkFBSyxFQUFFLG9DQUFlO0lBQ3ZELElBQUEsVUFLYSxFQUpqQix3QkFBUyxFQUNULDRDQUFtQixFQUNuQixnQ0FBYSxFQUNiLGNBQUksQ0FDYztJQUVaLElBQUEsNkNBQWtCLENBQXFCO0lBRS9DLElBQU0saUJBQWlCLEdBQUcsS0FBSyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO0lBRTlELElBQU0sU0FBUyxHQUFHO1FBQ2hCLEVBQUUsRUFBSywyQkFBMkIsU0FBSSxJQUFJLENBQUMsSUFBTTtRQUNqRCxHQUFHLEVBQUUsSUFBSSxDQUFDLEVBQUU7S0FDYixDQUFDO0lBRUYsSUFBTSxVQUFVLEdBQUc7UUFDakIsS0FBSyxFQUFFO1lBQ0wsS0FBSyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsR0FBRztZQUMvQixFQUFFLGVBQWUsRUFBRSxTQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxNQUFHLEVBQUU7U0FDL0Q7S0FDRixDQUFDO0lBRUYsSUFBTSxlQUFlLEdBQUc7UUFDdEIsS0FBSyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxJQUFJLENBQUM7S0FDakMsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWSxDQUFDLFNBQVM7UUFDdEMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxZQUFZLENBQUMsS0FBSztZQUNsQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFlBQVksQ0FBQyxRQUFRO2dCQUNyQyxvQkFBQyxPQUFPLGVBQUssU0FBUztvQkFDcEIsd0NBQVMsVUFBVSxFQUFRLENBQ25CO2dCQUNWLG9CQUFDLFlBQVksSUFDWCxPQUFPLEVBQUUsa0JBQWtCLEVBQzNCLEtBQUssRUFBRSxPQUFPLEVBQ2QsSUFBSSxFQUFFLFdBQVcsRUFDakIsU0FBUyxFQUFFLEVBQUUsS0FBSyxFQUFFLFFBQVEsQ0FBQyxTQUFTLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxFQUM1RCxRQUFRLEVBQUUsY0FBTSxPQUFBLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQXhCLENBQXdCLEVBQ3hDLEtBQUssRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxLQUFLLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FDekQsQ0FDRTtZQUNOLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWSxDQUFDLFNBQVM7Z0JBQ3RDLDZCQUFLLEtBQUssRUFBRSxpQkFBaUI7b0JBQzNCLDZCQUFLLEtBQUssRUFBRSxpQkFBaUIsQ0FBQyxLQUFLLElBQUcsSUFBSSxDQUFDLElBQUksQ0FBTztvQkFDdEQsNkJBQUssS0FBSyxFQUFFLGlCQUFpQixDQUFDLEtBQUssSUFBRyxhQUFhLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFPO29CQUMvRSw2QkFBSyxLQUFLLEVBQUUsaUJBQWlCLENBQUMsU0FBUyxDQUFDLFNBQVM7d0JBQy9DLDZCQUFLLEtBQUssRUFBRSxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsY0FBYzs0QkFDcEQsb0JBQUMsVUFBVSxlQUFLLGVBQWUsRUFBSTs0QkFDbkMsOEJBQU0sS0FBSyxFQUFFLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQVEsQ0FDckU7d0JBQ04sOEJBQU0sS0FBSyxFQUFFLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxLQUFLLFFBQVU7d0JBQ3hELDZCQUFLLEtBQUssRUFBRSxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsY0FBYzs0QkFDcEQsb0JBQUMsSUFBSSxJQUFDLFVBQVUsRUFBRSxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxZQUFZLEdBQUk7NEJBQzNFLDZCQUFLLEtBQUssRUFBRSxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFHLElBQUksQ0FBQyxVQUFVLENBQU8sQ0FDakUsQ0FDRixDQUNGO2dCQUNOLG9CQUFDLFlBQVksSUFDWCxLQUFLLEVBQUUsY0FBYyxFQUNyQixJQUFJLEVBQUUsWUFBWSxFQUNsQixLQUFLLEVBQUUsWUFBWSxFQUNuQixLQUFLLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQ3JCLEtBQUssQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FDakMsRUFDRCxRQUFRLEVBQUU7d0JBQ1IsYUFBYSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQzt3QkFDdkIsU0FBUyxDQUFDLG9CQUFvQixDQUFDLENBQUM7b0JBQ2xDLENBQUMsR0FDRCxDQUNFLENBQ0YsQ0FDRixDQUNQLENBQUM7QUFDSixDQUFDO0FBQUEsQ0FBQyJ9
// CONCATENATED MODULE: ./components/product/wish-item/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var connect = __webpack_require__(129).connect;




var component_WishItem = /** @class */ (function (_super) {
    __extends(WishItem, _super);
    function WishItem(props) {
        var _this = _super.call(this, props) || this;
        _this.state = initialize_INITIAL_STATE;
        return _this;
    }
    WishItem.prototype.handleAddToCart = function (boxId) {
        var addItemToCartAction = this.props.addItemToCartAction;
        this.setState({
            isLoadingAddToCard: true,
        }, addItemToCartAction({ boxId: boxId, quantity: 1, displayCartSumary: Object(cart["v" /* isShowCartSummaryLayout */])() }));
    };
    WishItem.prototype.componentWillReceiveProps = function (nextProps) {
        var isAddCartSuccess = this.props.cartStore.isAddCartSuccess;
        isAddCartSuccess !== nextProps.cartStore.isAddCartSuccess && this.setState({ isLoadingAddToCard: false });
    };
    WishItem.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state,
            handleAddToCart: this.handleAddToCart.bind(this)
        };
        return renderComponent(args);
    };
    ;
    WishItem.defaultProps = initialize_DEFAULT_PROPS;
    WishItem = __decorate([
        radium
    ], WishItem);
    return WishItem;
}(react["PureComponent"]));
/* harmony default export */ var component = (connect(store_mapStateToProps, store_mapDispatchToProps)(component_WishItem));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUNqQyxJQUFNLE9BQU8sR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUMsT0FBTyxDQUFDO0FBRS9DLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBRS9ELE9BQU8sRUFBRSxlQUFlLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFDOUQsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFFNUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUd6QztJQUF1Qiw0QkFBNkI7SUFHbEQsa0JBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUM7O0lBQzdCLENBQUM7SUFFRCxrQ0FBZSxHQUFmLFVBQWdCLEtBQUs7UUFDWCxJQUFBLG9EQUFtQixDQUFnQjtRQUMzQyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ1osa0JBQWtCLEVBQUUsSUFBSTtTQUNmLEVBQUUsbUJBQW1CLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxRQUFRLEVBQUUsQ0FBQyxFQUFFLGlCQUFpQixFQUFFLHVCQUF1QixFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDMUcsQ0FBQztJQUVELDRDQUF5QixHQUF6QixVQUEwQixTQUFpQjtRQUNwQixJQUFBLHdEQUFnQixDQUFrQjtRQUV2RCxnQkFBZ0IsS0FBSyxTQUFTLENBQUMsU0FBUyxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxrQkFBa0IsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFBO0lBQzNHLENBQUM7SUFFRCx5QkFBTSxHQUFOO1FBQ0UsSUFBTSxJQUFJLEdBQUc7WUFDWCxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLGVBQWUsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDakQsQ0FBQTtRQUNELE1BQU0sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUFBLENBQUM7SUEzQksscUJBQVksR0FBVyxhQUFhLENBQUM7SUFEeEMsUUFBUTtRQURiLE1BQU07T0FDRCxRQUFRLENBNkJiO0lBQUQsZUFBQztDQUFBLEFBN0JELENBQXVCLGFBQWEsR0E2Qm5DO0FBRUQsZUFBZSxPQUFPLENBQ3BCLGVBQWUsRUFDZixrQkFBa0IsQ0FDbkIsQ0FBQyxRQUFRLENBQUMsQ0FBQyJ9
// CONCATENATED MODULE: ./components/product/wish-item/index.tsx

/* harmony default export */ var wish_item = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxRQUFRLE1BQU0sYUFBYSxDQUFDO0FBQ25DLGVBQWUsUUFBUSxDQUFDIn0=
// EXTERNAL MODULE: ./components/ui/loading-placeholder/index.tsx + 4 modules
var loading_placeholder = __webpack_require__(751);

// CONCATENATED MODULE: ./components/wish-list/style.tsx


/* harmony default export */ var wish_list_style = ({
    container: {},
    row: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{ padding: "10px 10px 0px 10px", marginBottom: 0 }],
        DESKTOP: [{
                paddingLeft: 0,
                paddingRight: 0,
                paddingTop: 0,
                paddingBottom: 0,
                marginBottom: 20
            }],
        GENERAL: [{
                display: variable["display"].flex,
                justifyContent: 'space-between',
                flexWrap: 'wrap'
            }]
    }),
    contentGroup: {
        container: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{ marginBottom: 10, width: '100%' }],
            DESKTOP: [{ marginBottom: 20, width: 'calc(50% - 10px)' }],
            GENERAL: [{
                    borderRadius: 3,
                    boxShadow: variable["shadowBlurSort"],
                    paddingLeft: 10,
                    paddingRight: 10,
                    paddingTop: 10,
                    paddingBottom: 10
                }]
        }),
        inner: {
            display: variable["display"].flex,
            justifyContent: 'space-between',
        },
        imgGroup: {
            display: variable["display"].flex,
            flexDirection: 'column',
            justifyContent: 'space-between',
            width: 'calc(40% - 10px)',
            img: {
                backgroundSize: 'contain',
                backgroundPosition: 'center center',
                backgroundRepeat: 'no-repeat',
                width: '100%',
                paddingTop: '100%'
            },
            btn: {
                marginBottom: 0,
                maxWidth: 200,
            }
        },
        rateGroup: {
            display: variable["display"].flex,
            flexDirection: 'column',
            justifyContent: 'space-between',
            width: 'calc(60%)',
            header: {
                title: {
                    fontSize: 16,
                    marginBottom: 5,
                    color: variable["colorBlack"],
                },
                price: {
                    fontSize: 15,
                    marginBottom: 5,
                },
                date: {
                    fontSize: 12,
                    color: variable["colorBlack05"],
                },
                rateGroup: {
                    container: Object(responsive["a" /* combineStyle */])({
                        MOBILE: [{ flexDirection: 'column' }],
                        DESKTOP: [{ alignItems: 'center', flexDirection: 'row' }],
                        GENERAL: [{ display: variable["display"].flex, marginBottom: 10 }]
                    }),
                    ratingStarIcon: {
                        display: variable["display"].flex,
                        alignItems: 'center',
                    },
                    slash: Object(responsive["a" /* combineStyle */])({
                        MOBILE: [{ display: variable["display"].none }],
                        DESKTOP: [{ display: variable["display"].block }],
                        GENERAL: [{ color: variable["colorBlack05"] }]
                    }),
                    inner: {
                        width: 15,
                    },
                    rate: {
                        padding: '0px 5px',
                        height: 30,
                        lineHeight: '35px',
                    },
                    love: Object(responsive["a" /* combineStyle */])({
                        MOBILE: [{ lineHeight: '32px' }],
                        DESKTOP: [{ lineHeight: '35px' }],
                        GENERAL: [{ height: 30 }]
                    })
                },
                review: {
                    maxHeight: 200,
                    overflow: 'hidden',
                    fontSize: 14,
                },
            },
            btn: {
                marginBottom: 0
            },
        },
    },
    content: {
        container: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{ margin: '0 10px 10px 10px' }],
            DESKTOP: [{ margin: '0 0 20px 0' }],
            GENERAL: [{
                    display: variable["display"].flex,
                    width: '100%',
                    borderRadius: '3px',
                    border: "1px solid " + variable["colorD2"],
                }]
        }),
        imageGroup: {
            display: variable["display"].block,
            padding: '10px',
            image: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        width: window.innerWidth <= variable["breakPoint320"] ? '60px' : '100px',
                        height: window.innerWidth <= variable["breakPoint320"] ? '60px' : '100px'
                    }],
                DESKTOP: [{ width: '200px', height: '175px' }],
                GENERAL: [{
                        backgroundSize: 'cover',
                        backgroundPosition: 'center center',
                        padding: '10px'
                    }]
            }),
        }
    },
    placeholder: {
        width: '100%',
        title: {
            background: variable["colorF0"],
            display: variable["display"].block,
            width: 100,
            height: 40,
            margin: '0 auto 30px'
        },
        control: {
            width: '100%',
            display: variable["display"].flex,
            justifyContent: 'space-between',
            paddingLeft: 10,
            paddingRight: 10,
            marginBottom: 20,
        },
        controlItem: {
            width: 150,
            height: 30,
            background: variable["colorF7"]
        },
        productList: {
            display: variable["display"].flex,
            flexWrap: 'wrap',
        },
        productMobileItem: {
            minWidth: '50%',
            width: '50%',
        },
        productItem: {
            flex: 1,
            paddingLeft: 10,
            paddingRight: 10,
            marginBottom: 20,
            minWidth: '50%',
            width: '50%',
            image: {
                width: '100%',
                height: 'auto',
                paddingTop: '50%',
                marginBottom: 10,
            },
            text: {
                width: '94%',
                height: 25,
                marginBottom: 10,
            },
            lastText: {
                width: '65%',
                height: 25,
            }
        },
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSxzQkFBc0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFFdEQsZUFBZTtJQUNiLFNBQVMsRUFBRSxFQUFFO0lBRWIsR0FBRyxFQUFFLFlBQVksQ0FBQztRQUNoQixNQUFNLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxZQUFZLEVBQUUsQ0FBQyxFQUFFLENBQUM7UUFDNUQsT0FBTyxFQUFFLENBQUM7Z0JBQ1IsV0FBVyxFQUFFLENBQUM7Z0JBQ2QsWUFBWSxFQUFFLENBQUM7Z0JBQ2YsVUFBVSxFQUFFLENBQUM7Z0JBQ2IsYUFBYSxFQUFFLENBQUM7Z0JBQ2hCLFlBQVksRUFBRSxFQUFFO2FBQ2pCLENBQUM7UUFDRixPQUFPLEVBQUUsQ0FBQztnQkFDUixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixjQUFjLEVBQUUsZUFBZTtnQkFDL0IsUUFBUSxFQUFFLE1BQU07YUFDakIsQ0FBQztLQUNILENBQUM7SUFFRixZQUFZLEVBQUU7UUFDWixTQUFTLEVBQUUsWUFBWSxDQUFDO1lBQ3RCLE1BQU0sRUFBRSxDQUFDLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLENBQUM7WUFDN0MsT0FBTyxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxrQkFBa0IsRUFBRSxDQUFDO1lBQzFELE9BQU8sRUFBRSxDQUFDO29CQUNSLFlBQVksRUFBRSxDQUFDO29CQUNmLFNBQVMsRUFBRSxRQUFRLENBQUMsY0FBYztvQkFDbEMsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLFVBQVUsRUFBRSxFQUFFO29CQUNkLGFBQWEsRUFBRSxFQUFFO2lCQUNsQixDQUFDO1NBQ0gsQ0FBQztRQUVGLEtBQUssRUFBRTtZQUNMLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsY0FBYyxFQUFFLGVBQWU7U0FDaEM7UUFFRCxRQUFRLEVBQUU7WUFDUixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLGFBQWEsRUFBRSxRQUFRO1lBQ3ZCLGNBQWMsRUFBRSxlQUFlO1lBQy9CLEtBQUssRUFBRSxrQkFBa0I7WUFFekIsR0FBRyxFQUFFO2dCQUNILGNBQWMsRUFBRSxTQUFTO2dCQUN6QixrQkFBa0IsRUFBRSxlQUFlO2dCQUNuQyxnQkFBZ0IsRUFBRSxXQUFXO2dCQUM3QixLQUFLLEVBQUUsTUFBTTtnQkFDYixVQUFVLEVBQUUsTUFBTTthQUNuQjtZQUVELEdBQUcsRUFBRTtnQkFDSCxZQUFZLEVBQUUsQ0FBQztnQkFDZixRQUFRLEVBQUUsR0FBRzthQUNkO1NBQ0Y7UUFFRCxTQUFTLEVBQUU7WUFDVCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLGFBQWEsRUFBRSxRQUFRO1lBQ3ZCLGNBQWMsRUFBRSxlQUFlO1lBQy9CLEtBQUssRUFBRSxXQUFXO1lBRWxCLE1BQU0sRUFBRTtnQkFDTixLQUFLLEVBQUU7b0JBQ0wsUUFBUSxFQUFFLEVBQUU7b0JBQ1osWUFBWSxFQUFFLENBQUM7b0JBQ2YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2lCQUMzQjtnQkFFRCxLQUFLLEVBQUU7b0JBQ0wsUUFBUSxFQUFFLEVBQUU7b0JBQ1osWUFBWSxFQUFFLENBQUM7aUJBQ2hCO2dCQUVELElBQUksRUFBRTtvQkFDSixRQUFRLEVBQUUsRUFBRTtvQkFDWixLQUFLLEVBQUUsUUFBUSxDQUFDLFlBQVk7aUJBQzdCO2dCQUVELFNBQVMsRUFBRTtvQkFDVCxTQUFTLEVBQUUsWUFBWSxDQUFDO3dCQUN0QixNQUFNLEVBQUUsQ0FBQyxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsQ0FBQzt3QkFDckMsT0FBTyxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsQ0FBQzt3QkFDekQsT0FBTyxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxDQUFDO3FCQUNoRSxDQUFDO29CQUVGLGNBQWMsRUFBRTt3QkFDZCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO3dCQUM5QixVQUFVLEVBQUUsUUFBUTtxQkFDckI7b0JBRUQsS0FBSyxFQUFFLFlBQVksQ0FBQzt3QkFDbEIsTUFBTSxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDNUMsT0FBTyxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQzt3QkFDOUMsT0FBTyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsUUFBUSxDQUFDLFlBQVksRUFBRSxDQUFDO3FCQUM1QyxDQUFDO29CQUVGLEtBQUssRUFBRTt3QkFDTCxLQUFLLEVBQUUsRUFBRTtxQkFDVjtvQkFFRCxJQUFJLEVBQUU7d0JBQ0osT0FBTyxFQUFFLFNBQVM7d0JBQ2xCLE1BQU0sRUFBRSxFQUFFO3dCQUNWLFVBQVUsRUFBRSxNQUFNO3FCQUNuQjtvQkFFRCxJQUFJLEVBQUUsWUFBWSxDQUFDO3dCQUNqQixNQUFNLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsQ0FBQzt3QkFDaEMsT0FBTyxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLENBQUM7d0JBQ2pDLE9BQU8sRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxDQUFDO3FCQUMxQixDQUFDO2lCQUNIO2dCQUVELE1BQU0sRUFBRTtvQkFDTixTQUFTLEVBQUUsR0FBRztvQkFDZCxRQUFRLEVBQUUsUUFBUTtvQkFDbEIsUUFBUSxFQUFFLEVBQUU7aUJBQ2I7YUFDRjtZQUVELEdBQUcsRUFBRTtnQkFDSCxZQUFZLEVBQUUsQ0FBQzthQUNoQjtTQUNGO0tBQ0Y7SUFFRCxPQUFPLEVBQUU7UUFDUCxTQUFTLEVBQUUsWUFBWSxDQUFDO1lBQ3RCLE1BQU0sRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFFLGtCQUFrQixFQUFFLENBQUM7WUFDeEMsT0FBTyxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLENBQUM7WUFDbkMsT0FBTyxFQUFFLENBQUM7b0JBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtvQkFDOUIsS0FBSyxFQUFFLE1BQU07b0JBQ2IsWUFBWSxFQUFFLEtBQUs7b0JBQ25CLE1BQU0sRUFBRSxlQUFhLFFBQVEsQ0FBQyxPQUFTO2lCQUN4QyxDQUFDO1NBQ0gsQ0FBQztRQUVGLFVBQVUsRUFBRTtZQUNWLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDL0IsT0FBTyxFQUFFLE1BQU07WUFFZixLQUFLLEVBQUUsWUFBWSxDQUFDO2dCQUNsQixNQUFNLEVBQUUsQ0FBQzt3QkFDUCxLQUFLLEVBQUUsTUFBTSxDQUFDLFVBQVUsSUFBSSxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU87d0JBQ3JFLE1BQU0sRUFBRSxNQUFNLENBQUMsVUFBVSxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTztxQkFDdkUsQ0FBQztnQkFFRixPQUFPLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxDQUFDO2dCQUM5QyxPQUFPLEVBQUUsQ0FBQzt3QkFDUixjQUFjLEVBQUUsT0FBTzt3QkFDdkIsa0JBQWtCLEVBQUUsZUFBZTt3QkFDbkMsT0FBTyxFQUFFLE1BQU07cUJBQ2hCLENBQUM7YUFDSCxDQUFDO1NBQ0g7S0FDRjtJQUVELFdBQVcsRUFBRTtRQUNYLEtBQUssRUFBRSxNQUFNO1FBRWIsS0FBSyxFQUFFO1lBQ0wsVUFBVSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1lBQzVCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDL0IsS0FBSyxFQUFFLEdBQUc7WUFDVixNQUFNLEVBQUUsRUFBRTtZQUNWLE1BQU0sRUFBRSxhQUFhO1NBQ3RCO1FBRUQsT0FBTyxFQUFFO1lBQ1AsS0FBSyxFQUFFLE1BQU07WUFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLGNBQWMsRUFBRSxlQUFlO1lBQy9CLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLEVBQUU7WUFDaEIsWUFBWSxFQUFFLEVBQUU7U0FDakI7UUFFRCxXQUFXLEVBQUU7WUFDWCxLQUFLLEVBQUUsR0FBRztZQUNWLE1BQU0sRUFBRSxFQUFFO1lBQ1YsVUFBVSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1NBQzdCO1FBRUQsV0FBVyxFQUFFO1lBQ1gsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixRQUFRLEVBQUUsTUFBTTtTQUNqQjtRQUVELGlCQUFpQixFQUFFO1lBQ2pCLFFBQVEsRUFBRSxLQUFLO1lBQ2YsS0FBSyxFQUFFLEtBQUs7U0FDYjtRQUVELFdBQVcsRUFBRTtZQUNYLElBQUksRUFBRSxDQUFDO1lBQ1AsV0FBVyxFQUFFLEVBQUU7WUFDZixZQUFZLEVBQUUsRUFBRTtZQUNoQixZQUFZLEVBQUUsRUFBRTtZQUNoQixRQUFRLEVBQUUsS0FBSztZQUNmLEtBQUssRUFBRSxLQUFLO1lBRVosS0FBSyxFQUFFO2dCQUNMLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxNQUFNO2dCQUNkLFVBQVUsRUFBRSxLQUFLO2dCQUNqQixZQUFZLEVBQUUsRUFBRTthQUNqQjtZQUVELElBQUksRUFBRTtnQkFDSixLQUFLLEVBQUUsS0FBSztnQkFDWixNQUFNLEVBQUUsRUFBRTtnQkFDVixZQUFZLEVBQUUsRUFBRTthQUNqQjtZQUVELFFBQVEsRUFBRTtnQkFDUixLQUFLLEVBQUUsS0FBSztnQkFDWixNQUFNLEVBQUUsRUFBRTthQUNYO1NBQ0Y7S0FDRjtDQUNNLENBQUMifQ==
// CONCATENATED MODULE: ./components/wish-list/view.tsx
var view_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};







var renderItemPlaceholder = function (item) { return (react["createElement"]("div", { style: [
        wish_list_style.placeholder.productItem,
        'MOBILE' === window.DEVICE_VERSION && wish_list_style.placeholder.productMobileItem
    ], key: item },
    react["createElement"](loading_placeholder["a" /* default */], { style: wish_list_style.placeholder.productItem.image }))); };
var renderLoadingPlaceholder = function () {
    var list = 'MOBILE' === window.DEVICE_VERSION ? [1, 2, 3, 4] : [1, 2, 3, 4, 5, 6, 7, 8];
    return (react["createElement"]("div", { style: wish_list_style.placeholder },
        react["createElement"]("div", { style: wish_list_style.placeholder.productList }, Array.isArray(list) && list.map(renderItemPlaceholder))));
};
function view_renderComponent() {
    var _a = this.props, title = _a.title, list = _a.list, style = _a.style, showHeader = _a.showHeader, openAlert = _a.openAlert, addItemToCartAction = _a.addItemToCartAction, unLikeProduct = _a.unLikeProduct, current = _a.current, per = _a.per, total = _a.total, urlList = _a.urlList;
    var isLoadingAddToCard = this.state.isLoadingAddToCard;
    var contentGroupStyle = wish_list_style.contentGroup.rateGroup.header;
    var paginationProps = {
        current: current,
        per: per,
        total: total,
        urlList: urlList
    };
    var mainBlockProps = {
        showHeader: showHeader,
        title: title,
        showViewMore: false,
        content: react["createElement"]("div", null,
            react["createElement"]("div", { style: wish_list_style.row }, Array.isArray(list)
                && list.map(function (item, index) {
                    var wishItemProps = {
                        key: "wish-item-" + item.id,
                        item: item,
                        openAlert: openAlert,
                        addItemToCartAction: addItemToCartAction,
                        unLikeProduct: unLikeProduct,
                        displayCartSumary: Object(cart["v" /* isShowCartSummaryLayout */])()
                    };
                    return react["createElement"](wish_item, view_assign({}, wishItemProps));
                })),
            react["createElement"](pagination["a" /* default */], view_assign({}, paginationProps)))
    };
    return (react["createElement"]("summary-wish-list", { style: [wish_list_style.container, style] },
        react["createElement"](main_block["a" /* default */], view_assign({}, mainBlockProps))));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFTL0IsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFFNUQsT0FBTyxVQUFVLE1BQU0sdUJBQXVCLENBQUM7QUFDL0MsT0FBTyxTQUFTLE1BQU0sbUNBQW1DLENBQUM7QUFJMUQsT0FBTyxRQUFRLE1BQU0sc0JBQXNCLENBQUM7QUFDNUMsT0FBTyxrQkFBa0IsTUFBTSwyQkFBMkIsQ0FBQztBQUkzRCxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQUcsVUFBQyxJQUFJLElBQUssT0FBQSxDQUM3Qyw2QkFDRSxLQUFLLEVBQUU7UUFDTCxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVc7UUFDN0IsUUFBUSxLQUFLLE1BQU0sQ0FBQyxjQUFjLElBQUksS0FBSyxDQUFDLFdBQVcsQ0FBQyxpQkFBaUI7S0FDMUUsRUFDRCxHQUFHLEVBQUUsSUFBSTtJQUNULG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEdBQUksQ0FDOUQsQ0FDUCxFQVQ4QyxDQVM5QyxDQUFDO0FBRUYsSUFBTSx3QkFBd0IsR0FBRztJQUMvQixJQUFNLElBQUksR0FBRyxRQUFRLEtBQUssTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDMUYsTUFBTSxDQUFDLENBQ0wsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXO1FBQzNCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsSUFDdEMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQ25ELENBQ0YsQ0FDUCxDQUFBO0FBQ0gsQ0FBQyxDQUFDO0FBRUYsTUFBTTtJQUNFLElBQUEsZUFZMEIsRUFYOUIsZ0JBQUssRUFDTCxjQUFJLEVBQ0osZ0JBQUssRUFDTCwwQkFBVSxFQUNWLHdCQUFTLEVBQ1QsNENBQW1CLEVBQ25CLGdDQUFhLEVBQ2Isb0JBQU8sRUFDUCxZQUFHLEVBQ0gsZ0JBQUssRUFDTCxvQkFBTyxDQUN3QjtJQUcvQixJQUFBLGtEQUFrQixDQUNhO0lBRWpDLElBQU0saUJBQWlCLEdBQUcsS0FBSyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO0lBRTlELElBQU0sZUFBZSxHQUFHO1FBQ3RCLE9BQU8sU0FBQTtRQUNQLEdBQUcsS0FBQTtRQUNILEtBQUssT0FBQTtRQUNMLE9BQU8sU0FBQTtLQUNSLENBQUM7SUFFRixJQUFNLGNBQWMsR0FBRztRQUNyQixVQUFVLFlBQUE7UUFDVixLQUFLLE9BQUE7UUFDTCxZQUFZLEVBQUUsS0FBSztRQUNuQixPQUFPLEVBQ0w7WUFDRSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLEdBQUcsSUFFakIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7bUJBQ2hCLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFJLEVBQUUsS0FBSztvQkFDdEIsSUFBTSxhQUFhLEdBQUc7d0JBQ3BCLEdBQUcsRUFBRSxlQUFhLElBQUksQ0FBQyxFQUFJO3dCQUMzQixJQUFJLE1BQUE7d0JBQ0osU0FBUyxXQUFBO3dCQUNULG1CQUFtQixxQkFBQTt3QkFDbkIsYUFBYSxlQUFBO3dCQUNiLGlCQUFpQixFQUFFLHVCQUF1QixFQUFFO3FCQUM3QyxDQUFBO29CQUVELE1BQU0sQ0FBQyxvQkFBQyxRQUFRLGVBQUssYUFBYSxFQUFJLENBQUE7Z0JBQ3hDLENBQUMsQ0FBQyxDQUVBO1lBQ04sb0JBQUMsVUFBVSxlQUFLLGVBQWUsRUFBSSxDQUMvQjtLQUNULENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCwyQ0FBbUIsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUM7UUFDaEQsb0JBQUMsU0FBUyxlQUFLLGNBQWMsRUFBSSxDQUtmLENBQ3JCLENBQUM7QUFDSixDQUFDO0FBQUEsQ0FBQyJ9
// CONCATENATED MODULE: ./components/wish-list/component.tsx
var component_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var component_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var component_connect = __webpack_require__(129).connect;




var component_SummaryWishList = /** @class */ (function (_super) {
    component_extends(SummaryWishList, _super);
    function SummaryWishList(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    SummaryWishList.prototype.componentWillReceiveProps = function (nextProps) {
        if (Object(cart["v" /* isShowCartSummaryLayout */])()) {
            this.setState({
                isLoadingAddToCard: false
            });
        }
    };
    SummaryWishList.prototype.render = function () {
        return view_renderComponent.bind(this)();
    };
    ;
    SummaryWishList.defaultProps = DEFAULT_PROPS;
    SummaryWishList = component_decorate([
        radium
    ], SummaryWishList);
    return SummaryWishList;
}(react["PureComponent"]));
/* harmony default export */ var wish_list_component = (component_connect(mapStateToProps, mapDispatchToProps)(component_SummaryWishList));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUNqQyxJQUFNLE9BQU8sR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUMsT0FBTyxDQUFDO0FBRS9DLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBRTVELE9BQU8sRUFBRSxlQUFlLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFDOUQsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFFNUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUd6QztJQUE4QixtQ0FBNkM7SUFHekUseUJBQVksS0FBcUI7UUFBakMsWUFDRSxrQkFBTSxLQUFLLENBQUMsU0FFYjtRQURDLEtBQUksQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDOztJQUM3QixDQUFDO0lBRUQsbURBQXlCLEdBQXpCLFVBQTBCLFNBQXlCO1FBRWpELEVBQUUsQ0FBQyxDQUFDLHVCQUF1QixFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzlCLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ1osa0JBQWtCLEVBQUUsS0FBSzthQUNSLENBQUMsQ0FBQztRQUN2QixDQUFDO0lBQ0gsQ0FBQztJQUVELGdDQUFNLEdBQU47UUFDRSxNQUFNLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO0lBQ3RDLENBQUM7SUFBQSxDQUFDO0lBbEJLLDRCQUFZLEdBQW1CLGFBQWEsQ0FBQztJQURoRCxlQUFlO1FBRHBCLE1BQU07T0FDRCxlQUFlLENBb0JwQjtJQUFELHNCQUFDO0NBQUEsQUFwQkQsQ0FBOEIsYUFBYSxHQW9CMUM7QUFFRCxlQUFlLE9BQU8sQ0FDcEIsZUFBZSxFQUNmLGtCQUFrQixDQUNuQixDQUFDLGVBQWUsQ0FBQyxDQUFDIn0=
// CONCATENATED MODULE: ./components/wish-list/index.tsx

/* harmony default export */ var wish_list = __webpack_exports__["a"] = (wish_list_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxlQUFlLE1BQU0sYUFBYSxDQUFDO0FBQzFDLGVBQWUsZUFBZSxDQUFDIn0=

/***/ })

}]);