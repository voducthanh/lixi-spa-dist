(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[27],{

/***/ 747:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 98).then(__webpack_require__.bind(null, 762)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 751:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// CONCATENATED MODULE: ./components/ui/loading-placeholder/initialize.tsx
var DEFAULT_PROPS = {
    style: {}
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixLQUFLLEVBQUUsRUFBRTtDQUNPLENBQUMifQ==
// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/ui/loading-placeholder/style.tsx

/* harmony default export */ var loading_placeholder_style = ({
    width: '100%',
    height: 100,
    backgroundColor: variable["colorF7"],
    backgroundRepeat: 'none',
    opacity: .4,
    display: variable["display"].block
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxlQUFlO0lBQ2IsS0FBSyxFQUFFLE1BQU07SUFDYixNQUFNLEVBQUUsR0FBRztJQUNYLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztJQUNqQyxnQkFBZ0IsRUFBRSxNQUFNO0lBQ3hCLE9BQU8sRUFBRSxFQUFFO0lBQ1gsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztDQUNoQyxDQUFDIn0=
// CONCATENATED MODULE: ./components/ui/loading-placeholder/view.tsx


var renderView = function (_a) {
    var style = _a.style;
    var formatedStyle = Array.isArray(style)
        ? style.slice() : style;
    return (react["createElement"]("div", { className: 'ani-bg', style: [loading_placeholder_style, formatedStyle] }));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRS9CLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixJQUFNLFVBQVUsR0FBRyxVQUFDLEVBQVM7UUFBUCxnQkFBSztJQUN6QixJQUFNLGFBQWEsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztRQUN4QyxDQUFDLENBQUssS0FBSyxTQUNYLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFFVixNQUFNLENBQUMsQ0FDTCw2QkFBSyxTQUFTLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxDQUFDLEtBQUssRUFBRSxhQUFhLENBQUMsR0FBUSxDQUNoRSxDQUFBO0FBQ0gsQ0FBQyxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// CONCATENATED MODULE: ./components/ui/loading-placeholder/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_LoadingPlaceholder = /** @class */ (function (_super) {
    __extends(LoadingPlaceholder, _super);
    function LoadingPlaceholder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LoadingPlaceholder.prototype.render = function () {
        return view({ style: this.props.style });
    };
    LoadingPlaceholder.defaultProps = DEFAULT_PROPS;
    LoadingPlaceholder = __decorate([
        radium
    ], LoadingPlaceholder);
    return LoadingPlaceholder;
}(react["PureComponent"]));
;
/* harmony default export */ var component = (component_LoadingPlaceholder);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBRTdDLE9BQU8sVUFBVSxNQUFNLFFBQVEsQ0FBQztBQUdoQztJQUFpQyxzQ0FBaUM7SUFBbEU7O0lBT0EsQ0FBQztJQUhDLG1DQUFNLEdBQU47UUFDRSxNQUFNLENBQUMsVUFBVSxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBTE0sK0JBQVksR0FBa0IsYUFBYSxDQUFDO0lBRC9DLGtCQUFrQjtRQUR2QixNQUFNO09BQ0Qsa0JBQWtCLENBT3ZCO0lBQUQseUJBQUM7Q0FBQSxBQVBELENBQWlDLGFBQWEsR0FPN0M7QUFBQSxDQUFDO0FBRUYsZUFBZSxrQkFBa0IsQ0FBQyJ9
// CONCATENATED MODULE: ./components/ui/loading-placeholder/index.tsx

/* harmony default export */ var loading_placeholder = __webpack_exports__["a"] = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxrQkFBa0IsTUFBTSxhQUFhLENBQUM7QUFDN0MsZUFBZSxrQkFBa0IsQ0FBQyJ9

/***/ }),

/***/ 754:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return currenyFormat; });
/**
 * Format currency to VND
 * @param {number} currency value number of currency
 * @return {string} curency with format
 */
var currenyFormat = function (currency, type) {
    if (type === void 0) { type = 'currency'; }
    /**
     * Validate value
     * - NULL
     * - UNDEFINED
     * NOT A NUMBER
     */
    if (null === currency || 'undefined' === typeof currency || true === isNaN(currency)) {
        return '0';
    }
    /**
     * Validate value
     * < 0
     */
    if (currency < 0) {
        return '0';
    }
    var suffix = 'currency' === type
        ? ''
        : 'coin' === type ? ' coin' : '';
    return currency.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + suffix;
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VycmVuY3kuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJjdXJyZW5jeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7OztHQUlHO0FBQ0gsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHLFVBQUMsUUFBZ0IsRUFBRSxJQUFpQjtJQUFqQixxQkFBQSxFQUFBLGlCQUFpQjtJQUMvRDs7Ozs7T0FLRztJQUNILEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxRQUFRLElBQUksV0FBVyxLQUFLLE9BQU8sUUFBUSxJQUFJLElBQUksS0FBSyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3JGLE1BQU0sQ0FBQyxHQUFHLENBQUM7SUFDYixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsRUFBRSxDQUFDLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFBQyxNQUFNLENBQUMsR0FBRyxDQUFDO0lBQUMsQ0FBQztJQUVqQyxJQUFNLE1BQU0sR0FBRyxVQUFVLEtBQUssSUFBSTtRQUNoQyxDQUFDLENBQUMsRUFBRTtRQUNKLENBQUMsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUVuQyxNQUFNLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLE9BQU8sQ0FBQyx5QkFBeUIsRUFBRSxLQUFLLENBQUMsR0FBRyxNQUFNLENBQUM7QUFDaEYsQ0FBQyxDQUFDIn0=

/***/ }),

/***/ 757:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 95).then(__webpack_require__.bind(null, 779)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 788:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _auth__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(20);
/* harmony import */ var _currency__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(754);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "c", function() { return _currency__WEBPACK_IMPORTED_MODULE_1__["a"]; });

/* harmony import */ var _encode__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "d", function() { return _encode__WEBPACK_IMPORTED_MODULE_2__["h"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _encode__WEBPACK_IMPORTED_MODULE_2__["d"]; });

/* harmony import */ var _format__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(116);
/* harmony import */ var _menu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(120);
/* harmony import */ var _navigate__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(353);
/* harmony import */ var _responsive__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(169);
/* harmony import */ var _storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(171);
/* harmony import */ var _uri__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(348);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _uri__WEBPACK_IMPORTED_MODULE_8__["b"]; });

/* harmony import */ var _validate__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(9);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "e", function() { return _validate__WEBPACK_IMPORTED_MODULE_9__["j"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "f", function() { return _validate__WEBPACK_IMPORTED_MODULE_9__["l"]; });












//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sUUFBUSxDQUFDO0FBQ3RDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxZQUFZLENBQUM7QUFDM0MsT0FBTyxFQUNMLGNBQWMsRUFDZCxZQUFZLEVBQ1osWUFBWSxFQUNaLGVBQWUsRUFDZixvQkFBb0IsRUFDckIsTUFBTSxVQUFVLENBQUM7QUFDbEIsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLFVBQVUsQ0FBQztBQUN4QyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFDMUMsT0FBTyxFQUNMLGdCQUFnQixFQUNoQixnQkFBZ0IsRUFDaEIsc0JBQXNCLEVBQ3RCLHdCQUF3QixFQUN4QixjQUFjLEVBQ2QsYUFBYSxHQUNkLE1BQU0sWUFBWSxDQUFDO0FBQ3BCLE9BQU8sRUFDTCxnQkFBZ0IsRUFDaEIsZUFBZSxFQUNoQixNQUFNLGNBQWMsQ0FBQztBQUN0QixPQUFPLEVBQ0wsR0FBRyxFQUNILEdBQUcsRUFDSCxLQUFLLEVBQ0wsWUFBWSxFQUNaLFVBQVUsRUFDVixXQUFXLEVBQ1osTUFBTSxXQUFXLENBQUM7QUFDbkIsT0FBTyxFQUNMLHVCQUF1QixFQUN2Qix1QkFBdUIsRUFDdkIscUJBQXFCLEVBQ3JCLHlCQUF5QixFQUN6QixpQkFBaUIsR0FDbEIsTUFBTSxPQUFPLENBQUM7QUFDZixPQUFPLEVBQ0wsU0FBUyxFQUNULGFBQWEsRUFDYixXQUFXLEVBQ1gsZUFBZSxFQUNoQixNQUFNLFlBQVksQ0FBQztBQUVwQixPQUFPO0FBQ0wsV0FBVztBQUNYLFlBQVk7QUFFWixlQUFlO0FBQ2YsYUFBYTtBQUViLGFBQWE7QUFDYixjQUFjLEVBQ2QsWUFBWSxFQUNaLFlBQVksRUFDWixlQUFlLEVBQ2Ysb0JBQW9CO0FBRXBCLGFBQWE7QUFDYixZQUFZO0FBRVosV0FBVztBQUNYLGdCQUFnQjtBQUVoQixlQUFlO0FBQ2YsZ0JBQWdCLEVBQ2hCLGdCQUFnQixFQUNoQixzQkFBc0IsRUFDdEIsd0JBQXdCLEVBQ3hCLGNBQWMsRUFDZCxhQUFhO0FBRWIsaUJBQWlCO0FBQ2pCLGdCQUFnQixFQUNoQixlQUFlO0FBRWYsY0FBYztBQUNkLEdBQUcsRUFDSCxHQUFHLEVBQ0gsS0FBSyxFQUNMLFlBQVksRUFDWixVQUFVLEVBQ1YsV0FBVztBQUVYLFVBQVU7QUFDVix1QkFBdUIsRUFDdkIsdUJBQXVCLEVBQ3ZCLHFCQUFxQixFQUNyQix5QkFBeUIsRUFDekIsaUJBQWlCO0FBRWpCLGVBQWU7QUFDZixTQUFTLEVBQ1QsYUFBYSxFQUNiLFdBQVcsRUFDWCxlQUFlLEVBQ2hCLENBQUEifQ==

/***/ }),

/***/ 826:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// CONCATENATED MODULE: ./components/testimonial/initialize.tsx
var DEFAULT_PROPS = {
    list: [],
    isHome: false,
    isSlide: false,
    showHeader: true
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLE1BQU0sRUFBRSxLQUFLO0lBQ2IsT0FBTyxFQUFFLEtBQUs7SUFDZCxVQUFVLEVBQUUsSUFBSTtDQUNQLENBQUMifQ==
// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./container/layout/fade-in/index.tsx
var fade_in = __webpack_require__(757);

// EXTERNAL MODULE: ./container/layout/main-block/index.tsx
var main_block = __webpack_require__(747);

// EXTERNAL MODULE: ./components/general/pagination/index.tsx + 4 modules
var pagination = __webpack_require__(752);

// EXTERNAL MODULE: ./components/item/feedback-testimonial/index.tsx + 2 modules
var feedback_testimonial = __webpack_require__(804);

// EXTERNAL MODULE: ./components/ui/loading-placeholder/index.tsx + 4 modules
var loading_placeholder = __webpack_require__(751);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./style/media-queries.ts
var media_queries = __webpack_require__(76);

// CONCATENATED MODULE: ./components/testimonial/style.tsx




/* harmony default export */ var testimonial_style = ({
    display: 'block',
    position: 'relative',
    zIndex: variable["zIndex5"],
    paddingTop: 20,
    groupProductVideomagazine: [
        layout["b" /* splitContainer */],
        layout["a" /* flexContainer */].justify, {
            marginBottom: 20,
        }
    ],
    feedContainer: {
        paddingTop: '30%',
        position: variable["position"].relative,
        panel: {
            position: variable["position"].absolute,
            top: 0,
            left: 0,
            height: '100%',
            width: '100%',
            overflowX: 'hidden',
            overflowY: 'auto',
            feed: {
                paddingTop: 1,
                paddingRight: 1,
                paddingBottom: 1,
                paddingLeft: 1,
            }
        }
    },
    mainBanner: {
        marginBottom: 20
    },
    list: function (isSlide) {
        if (isSlide === void 0) { isSlide = false; }
        var general = {
            display: variable["display"].flex,
            flexWrap: isSlide ? 'nowrap' : 'wrap',
            paddingTop: 20,
            paddingRight: 0,
            paddingBottom: 10,
            paddingLeft: 0,
            position: variable["position"].relative,
            overflowX: isSlide ? 'auto' : ''
        };
        var mobile =  true ? {
            paddingTop: 0,
            paddingRight: isSlide ? 20 : 0,
            paddingBottom: 0,
            paddingLeft: 0,
            marginRight: isSlide ? 5 : -10,
            marginLeft: -10
        } : undefined;
        return Object.assign({}, general, mobile);
    },
    item: function (isSlide) {
        if (isSlide === void 0) { isSlide = false; }
        return Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    marginBottom: 10,
                    paddingLeft: 5,
                    paddingRight: 5,
                    width: isSlide ? '47%' : '50%',
                    minWidth: isSlide ? '47%' : '',
                }],
            DESKTOP: [{
                    marginBottom: 20,
                    paddingLeft: 10,
                    paddingRight: 10,
                    width: '20%',
                }],
            GENERAL: [{ paddingTop: 0, paddingBottom: 0 }]
        });
    },
    videoWrap: {
        height: '100%'
    },
    customStyleLoading: {
        height: 300
    },
    placeholder: {
        width: '100%',
        title: {
            background: variable["colorF0"],
            display: variable["display"].block,
            width: 320,
            height: 56,
            margin: '0 auto 10px'
        },
        titleMobile: {
            margin: 0,
            textAlign: 'left'
        },
        productList: {
            flexWrap: 'wrap',
            display: variable["display"].flex,
            marginLeft: -10,
            marginRight: -10
        },
        productMobileItem: {
            minWidth: '50%',
            width: '50%',
        },
        productItem: {
            flex: 1,
            paddingLeft: 10,
            paddingRight: 10,
            marginBottom: 20,
            minWidth: '20%',
            width: '20%',
            height: 303,
            image: {
                width: '100%',
                height: '100%'
            },
            text: {
                width: '94%',
                height: 25,
                marginBottom: 10,
            },
            lastText: {
                width: '65%',
                height: 25,
            }
        },
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3RELE9BQU8sS0FBSyxNQUFNLE1BQU0sb0JBQW9CLENBQUM7QUFDN0MsT0FBTyxLQUFLLFFBQVEsTUFBTSxzQkFBc0IsQ0FBQztBQUNqRCxPQUFPLGFBQWEsTUFBTSwyQkFBMkIsQ0FBQztBQUV0RCxlQUFlO0lBQ2IsT0FBTyxFQUFFLE9BQU87SUFDaEIsUUFBUSxFQUFFLFVBQVU7SUFDcEIsTUFBTSxFQUFFLFFBQVEsQ0FBQyxPQUFPO0lBQ3hCLFVBQVUsRUFBRSxFQUFFO0lBRWQseUJBQXlCLEVBQUU7UUFDekIsTUFBTSxDQUFDLGNBQWM7UUFDckIsTUFBTSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUU7WUFDNUIsWUFBWSxFQUFFLEVBQUU7U0FDakI7S0FDRjtJQUVELGFBQWEsRUFBRTtRQUNiLFVBQVUsRUFBRSxLQUFLO1FBQ2pCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7UUFFcEMsS0FBSyxFQUFFO1lBQ0wsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxHQUFHLEVBQUUsQ0FBQztZQUNOLElBQUksRUFBRSxDQUFDO1lBQ1AsTUFBTSxFQUFFLE1BQU07WUFDZCxLQUFLLEVBQUUsTUFBTTtZQUNiLFNBQVMsRUFBRSxRQUFRO1lBQ25CLFNBQVMsRUFBRSxNQUFNO1lBRWpCLElBQUksRUFBRTtnQkFDSixVQUFVLEVBQUUsQ0FBQztnQkFDYixZQUFZLEVBQUUsQ0FBQztnQkFDZixhQUFhLEVBQUUsQ0FBQztnQkFDaEIsV0FBVyxFQUFFLENBQUM7YUFDZjtTQUNGO0tBQ0Y7SUFFRCxVQUFVLEVBQUU7UUFDVixZQUFZLEVBQUUsRUFBRTtLQUNqQjtJQUVELElBQUksRUFBRSxVQUFDLE9BQWU7UUFBZix3QkFBQSxFQUFBLGVBQWU7UUFDcEIsSUFBTSxPQUFPLEdBQUc7WUFDZCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsTUFBTTtZQUNyQyxVQUFVLEVBQUUsRUFBRTtZQUNkLFlBQVksRUFBRSxDQUFDO1lBQ2YsYUFBYSxFQUFFLEVBQUU7WUFDakIsV0FBVyxFQUFFLENBQUM7WUFDZCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1lBQ3BDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRTtTQUNqQyxDQUFBO1FBRUQsSUFBTSxNQUFNLEdBQUcsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pDLFVBQVUsRUFBRSxDQUFDO1lBQ2IsWUFBWSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlCLGFBQWEsRUFBRSxDQUFDO1lBQ2hCLFdBQVcsRUFBRSxDQUFDO1lBQ2QsV0FBVyxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDOUIsVUFBVSxFQUFFLENBQUMsRUFBRTtTQUNoQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUE7UUFFTixNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFRCxJQUFJLEVBQUUsVUFBQyxPQUFlO1FBQWYsd0JBQUEsRUFBQSxlQUFlO1FBQUssT0FBQSxZQUFZLENBQUM7WUFDdEMsTUFBTSxFQUFFLENBQUM7b0JBQ1AsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLFdBQVcsRUFBRSxDQUFDO29CQUNkLFlBQVksRUFBRSxDQUFDO29CQUNmLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSztvQkFDOUIsUUFBUSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFO2lCQUMvQixDQUFDO1lBRUYsT0FBTyxFQUFFLENBQUM7b0JBQ1IsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLFdBQVcsRUFBRSxFQUFFO29CQUNmLFlBQVksRUFBRSxFQUFFO29CQUNoQixLQUFLLEVBQUUsS0FBSztpQkFDYixDQUFDO1lBRUYsT0FBTyxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsQ0FBQyxFQUFFLGFBQWEsRUFBRSxDQUFDLEVBQUUsQ0FBQztTQUMvQyxDQUFDO0lBakJ5QixDQWlCekI7SUFFRixTQUFTLEVBQUU7UUFDVCxNQUFNLEVBQUUsTUFBTTtLQUNmO0lBRUQsa0JBQWtCLEVBQUU7UUFDbEIsTUFBTSxFQUFFLEdBQUc7S0FDWjtJQUVELFdBQVcsRUFBRTtRQUNYLEtBQUssRUFBRSxNQUFNO1FBRWIsS0FBSyxFQUFFO1lBQ0wsVUFBVSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1lBQzVCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDL0IsS0FBSyxFQUFFLEdBQUc7WUFDVixNQUFNLEVBQUUsRUFBRTtZQUNWLE1BQU0sRUFBRSxhQUFhO1NBQ3RCO1FBRUQsV0FBVyxFQUFFO1lBQ1gsTUFBTSxFQUFFLENBQUM7WUFDVCxTQUFTLEVBQUUsTUFBTTtTQUNsQjtRQUVELFdBQVcsRUFBRTtZQUNYLFFBQVEsRUFBRSxNQUFNO1lBQ2hCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsVUFBVSxFQUFFLENBQUMsRUFBRTtZQUNmLFdBQVcsRUFBRSxDQUFDLEVBQUU7U0FDakI7UUFFRCxpQkFBaUIsRUFBRTtZQUNqQixRQUFRLEVBQUUsS0FBSztZQUNmLEtBQUssRUFBRSxLQUFLO1NBQ2I7UUFFRCxXQUFXLEVBQUU7WUFDWCxJQUFJLEVBQUUsQ0FBQztZQUNQLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLEVBQUU7WUFDaEIsWUFBWSxFQUFFLEVBQUU7WUFDaEIsUUFBUSxFQUFFLEtBQUs7WUFDZixLQUFLLEVBQUUsS0FBSztZQUNaLE1BQU0sRUFBRSxHQUFHO1lBRVgsS0FBSyxFQUFFO2dCQUNMLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxNQUFNO2FBQ2Y7WUFFRCxJQUFJLEVBQUU7Z0JBQ0osS0FBSyxFQUFFLEtBQUs7Z0JBQ1osTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsWUFBWSxFQUFFLEVBQUU7YUFDakI7WUFFRCxRQUFRLEVBQUU7Z0JBQ1IsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osTUFBTSxFQUFFLEVBQUU7YUFDWDtTQUNGO0tBQ0Y7Q0FDSyxDQUFDIn0=
// CONCATENATED MODULE: ./components/testimonial/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};








var renderItemPlaceholder = function (item) { return (react["createElement"]("div", { style: [
        testimonial_style.placeholder.productItem,
        'MOBILE' === window.DEVICE_VERSION && testimonial_style.placeholder.productMobileItem
    ], key: item },
    react["createElement"](loading_placeholder["a" /* default */], { style: testimonial_style.placeholder.productItem.image }))); };
var renderLoadingPlaceholder = function (isHome) {
    if (isHome === void 0) { isHome = false; }
    var list = isHome || 'MOBILE' === window.DEVICE_VERSION ? [1, 2, 3, 4, 5] : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
    return (react["createElement"]("div", { style: testimonial_style.placeholder },
        react["createElement"](loading_placeholder["a" /* default */], { style: [testimonial_style.placeholder.title, 'MOBILE' === window.DEVICE_VERSION && testimonial_style.placeholder.titleMobile] }),
        react["createElement"]("div", { style: testimonial_style.placeholder.productList }, Array.isArray(list) && list.map(renderItemPlaceholder))));
};
var renderItems = function (_a) {
    var list = _a.list, openModal = _a.openModal, style = _a.style;
    return Array.isArray(list)
        && list.map(function (feedback) {
            return react["createElement"]("div", { key: feedback.id, style: style },
                react["createElement"](feedback_testimonial["a" /* default */], { data: feedback, openModal: openModal }));
        });
};
var renderView = function (props) {
    var per = props.per, list = props.list, total = props.total, isHome = props.isHome, current = props.current, isSlide = props.isSlide, urlList = props.urlList, openModal = props.openModal, showHeader = props.showHeader;
    var paginationProps = {
        per: per,
        total: total,
        current: current,
        urlList: urlList
    };
    var mainBlockProps = {
        showHeader: showHeader,
        title: showHeader ? 'Nhận xét về Lixibox' : '',
        viewMoreText: 'Xem thêm',
        viewMoreLink: routing["q" /* ROUTING_COMMUNITY_PATH */],
        showViewMore: isHome,
        content: 0 === list.length
            ? null
            : isHome
                ? (react["createElement"]("div", { style: testimonial_style.list(isSlide) }, renderItems({ list: list.slice(0, 5), style: testimonial_style.item(isSlide), openModal: openModal })))
                : (react["createElement"]("div", null,
                    react["createElement"](fade_in["a" /* default */], { itemStyle: testimonial_style.item(isSlide), style: testimonial_style.list(isSlide) }, renderItems({ list: list, style: testimonial_style.videoWrap, openModal: openModal })),
                    react["createElement"](pagination["a" /* default */], __assign({}, paginationProps)))),
        style: {}
    };
    return (react["createElement"]("summary-testimonial-list", { style: testimonial_style }, Array.isArray(list) && 0 === list.length ? renderLoadingPlaceholder(isHome) : react["createElement"](main_block["a" /* default */], __assign({}, mainBlockProps))));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFFN0UsT0FBTyxNQUFNLE1BQU0sZ0NBQWdDLENBQUM7QUFDcEQsT0FBTyxTQUFTLE1BQU0sbUNBQW1DLENBQUM7QUFDMUQsT0FBTyxVQUFVLE1BQU0sdUJBQXVCLENBQUM7QUFDL0MsT0FBTyxlQUFlLE1BQU0sOEJBQThCLENBQUM7QUFDM0QsT0FBTyxrQkFBa0IsTUFBTSwyQkFBMkIsQ0FBQztBQUczRCxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQUcsVUFBQyxJQUFJLElBQUssT0FBQSxDQUM3Qyw2QkFDRSxLQUFLLEVBQUU7UUFDTCxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVc7UUFDN0IsUUFBUSxLQUFLLE1BQU0sQ0FBQyxjQUFjLElBQUksS0FBSyxDQUFDLFdBQVcsQ0FBQyxpQkFBaUI7S0FDMUUsRUFDRCxHQUFHLEVBQUUsSUFBSTtJQUNULG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEdBQUksQ0FDOUQsQ0FDUCxFQVQ4QyxDQVM5QyxDQUFDO0FBR0YsSUFBTSx3QkFBd0IsR0FBRyxVQUFDLE1BQWM7SUFBZCx1QkFBQSxFQUFBLGNBQWM7SUFDOUMsSUFBTSxJQUFJLEdBQUcsTUFBTSxJQUFJLFFBQVEsS0FBSyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ2xJLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVztRQUMzQixvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxRQUFRLEtBQUssTUFBTSxDQUFDLGNBQWMsSUFBSSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxHQUFJO1FBQzdILDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsSUFDdEMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQ25ELENBQ0YsQ0FDUCxDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsSUFBTSxXQUFXLEdBQUcsVUFBQyxFQUEwQjtRQUF4QixjQUFJLEVBQUUsd0JBQVMsRUFBRSxnQkFBSztJQUFPLE9BQUEsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7V0FDbEUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFDLFFBQVE7WUFDbkIsT0FBQSw2QkFDRSxHQUFHLEVBQUUsUUFBUSxDQUFDLEVBQUUsRUFDaEIsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osb0JBQUMsZUFBZSxJQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLFNBQVMsR0FBSSxDQUNyRDtRQUpOLENBSU0sQ0FDUDtBQVBpRCxDQU9qRCxDQUFDO0FBRUosSUFBTSxVQUFVLEdBQUcsVUFBQyxLQUFhO0lBRTdCLElBQUEsZUFBRyxFQUNILGlCQUFJLEVBQ0osbUJBQUssRUFDTCxxQkFBTSxFQUNOLHVCQUFPLEVBQ1AsdUJBQU8sRUFDUCx1QkFBTyxFQUNQLDJCQUFTLEVBQ1QsNkJBQVUsQ0FDRjtJQUVWLElBQU0sZUFBZSxHQUFHO1FBQ3RCLEdBQUcsS0FBQTtRQUNILEtBQUssT0FBQTtRQUNMLE9BQU8sU0FBQTtRQUNQLE9BQU8sU0FBQTtLQUNSLENBQUM7SUFFRixJQUFNLGNBQWMsR0FBRztRQUNyQixVQUFVLFlBQUE7UUFDVixLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsRUFBRTtRQUM5QyxZQUFZLEVBQUUsVUFBVTtRQUN4QixZQUFZLEVBQUUsc0JBQXNCO1FBQ3BDLFlBQVksRUFBRSxNQUFNO1FBQ3BCLE9BQU8sRUFDTCxDQUFDLEtBQUssSUFBSSxDQUFDLE1BQU07WUFDZixDQUFDLENBQUMsSUFBSTtZQUNOLENBQUMsQ0FBQyxNQUFNO2dCQUNOLENBQUMsQ0FBQyxDQUNBLDZCQUNFLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUN6QixXQUFXLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsU0FBUyxXQUFBLEVBQUUsQ0FBQyxDQUMxRSxDQUNSO2dCQUNELENBQUMsQ0FBQyxDQUNBO29CQUNFLG9CQUFDLE1BQU0sSUFDTCxTQUFTLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFDOUIsS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQ3pCLFdBQVcsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxFQUFFLFNBQVMsV0FBQSxFQUFFLENBQUMsQ0FDbEQ7b0JBQ1Qsb0JBQUMsVUFBVSxlQUFLLGVBQWUsRUFBSSxDQUMvQixDQUNQO1FBQ1AsS0FBSyxFQUFFLEVBQUU7S0FDVixDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsa0RBQTBCLEtBQUssRUFBRSxLQUFLLElBQ25DLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLHdCQUF3QixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxvQkFBQyxTQUFTLGVBQUssY0FBYyxFQUFJLENBQ3ZGLENBQzVCLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./components/testimonial/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_TestimonialComponent = /** @class */ (function (_super) {
    __extends(TestimonialComponent, _super);
    function TestimonialComponent(props) {
        return _super.call(this, props) || this;
    }
    TestimonialComponent.prototype.shouldComponentUpdate = function (nextProps) {
        if (this.props.list.length !== nextProps.list.length) {
            return true;
        }
        return false;
    };
    TestimonialComponent.prototype.render = function () {
        return view(this.props);
    };
    TestimonialComponent.defaultProps = DEFAULT_PROPS;
    TestimonialComponent = __decorate([
        radium
    ], TestimonialComponent);
    return TestimonialComponent;
}(react["Component"]));
;
/* harmony default export */ var component = (component_TestimonialComponent);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFHakMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUM3QyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFHaEM7SUFBbUMsd0NBQTRCO0lBRzdELDhCQUFZLEtBQWE7ZUFDdkIsa0JBQU0sS0FBSyxDQUFDO0lBQ2QsQ0FBQztJQUVELG9EQUFxQixHQUFyQixVQUFzQixTQUFpQjtRQUNyQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUFDLENBQUM7UUFFdEUsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFRCxxQ0FBTSxHQUFOO1FBQ0UsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQWRNLGlDQUFZLEdBQVcsYUFBYSxDQUFDO0lBRHhDLG9CQUFvQjtRQUR6QixNQUFNO09BQ0Qsb0JBQW9CLENBZ0J6QjtJQUFELDJCQUFDO0NBQUEsQUFoQkQsQ0FBbUMsS0FBSyxDQUFDLFNBQVMsR0FnQmpEO0FBQUEsQ0FBQztBQUVGLGVBQWUsb0JBQW9CLENBQUMifQ==
// CONCATENATED MODULE: ./components/testimonial/index.tsx

/* harmony default export */ var testimonial = __webpack_exports__["a"] = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxvQkFBb0IsTUFBTSxhQUFhLENBQUM7QUFDL0MsZUFBZSxvQkFBb0IsQ0FBQyJ9

/***/ }),

/***/ 882:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./action/love.ts + 1 modules
var love = __webpack_require__(793);

// EXTERNAL MODULE: ./action/modal.ts
var modal = __webpack_require__(84);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// EXTERNAL MODULE: ./utils/encode.ts
var encode = __webpack_require__(7);

// EXTERNAL MODULE: ./utils/format.ts
var format = __webpack_require__(116);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// CONCATENATED MODULE: ./container/app-shop/testimonial/initialize.tsx
var DEFAULT_PROPS = {
    perPage: 25
};
var INITIAL_STATE = {
    urlList: [],
    page: 1
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixPQUFPLEVBQUUsRUFBRTtDQUNGLENBQUM7QUFFWixNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUc7SUFDM0IsT0FBTyxFQUFFLEVBQUU7SUFDWCxJQUFJLEVBQUUsQ0FBQztDQUNFLENBQUMifQ==
// EXTERNAL MODULE: ./container/layout/wrap/index.tsx
var wrap = __webpack_require__(746);

// EXTERNAL MODULE: ./components/testimonial/index.tsx + 4 modules
var testimonial = __webpack_require__(826);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./container/app-shop/testimonial/style.tsx

/* harmony default export */ var style = ({
    display: 'block',
    position: 'relative',
    zIndex: variable["zIndex5"],
    paddingTop: 30
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxlQUFlO0lBQ2IsT0FBTyxFQUFFLE9BQU87SUFDaEIsUUFBUSxFQUFFLFVBQVU7SUFDcEIsTUFBTSxFQUFFLFFBQVEsQ0FBQyxPQUFPO0lBQ3hCLFVBQVUsRUFBRSxFQUFFO0NBQ1AsQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/testimonial/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};





var renderView = function (_a) {
    var props = _a.props, state = _a.state;
    var _b = props, loveList = _b.loveStore.loveList, openModal = _b.openModal, perPage = _b.perPage;
    var _c = state, urlList = _c.urlList, page = _c.page;
    var loveListHash = Object(encode["j" /* objectToHash */])({ page: page, perPage: perPage });
    var list = loveList[loveListHash] || [];
    var _urlList = list.loves && 0 !== list.loves.length ? urlList : [];
    var _d = 0 !== list.length && list.paging || { current_page: 0, per_page: 0, total_pages: 0 }, current_page = _d.current_page, per_page = _d.per_page, total_pages = _d.total_pages;
    var testimonialProps = {
        list: list && list.loves || [],
        openModal: openModal,
        current: current_page,
        per: per_page,
        total: total_pages,
        urlList: _urlList
    };
    return (react["createElement"]("testimonial-container", { style: style },
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"](testimonial["a" /* default */], __assign({}, testimonialProps)))));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxVQUFVLE1BQU0sbUJBQW1CLENBQUM7QUFDM0MsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3JELE9BQU8sV0FBVyxNQUFNLGlDQUFpQyxDQUFDO0FBRzFELE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixJQUFNLFVBQVUsR0FBRyxVQUFDLEVBQWdCO1FBQWQsZ0JBQUssRUFBRSxnQkFBSztJQUMxQixJQUFBLFVBSWEsRUFISixnQ0FBUSxFQUNyQix3QkFBUyxFQUNULG9CQUFPLENBQ1c7SUFFZCxJQUFBLFVBQW1DLEVBQWpDLG9CQUFPLEVBQUUsY0FBSSxDQUFxQjtJQUUxQyxJQUFNLFlBQVksR0FBRyxZQUFZLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUM7SUFDckQsSUFBTSxJQUFJLEdBQUcsUUFBUSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMxQyxJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDaEUsSUFBQSx5RkFBOEgsRUFBNUgsOEJBQVksRUFBRSxzQkFBUSxFQUFFLDRCQUFXLENBQTBGO0lBRXJJLElBQU0sZ0JBQWdCLEdBQUc7UUFDdkIsSUFBSSxFQUFFLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUU7UUFDOUIsU0FBUyxFQUFFLFNBQVM7UUFDcEIsT0FBTyxFQUFFLFlBQVk7UUFDckIsR0FBRyxFQUFFLFFBQVE7UUFDYixLQUFLLEVBQUUsV0FBVztRQUNsQixPQUFPLEVBQUUsUUFBUTtLQUNsQixDQUFDO0lBQ0YsTUFBTSxDQUFDLENBQ0wsK0NBQXVCLEtBQUssRUFBRSxLQUFLO1FBQ2pDLG9CQUFDLFVBQVU7WUFDVCxvQkFBQyxXQUFXLGVBQUssZ0JBQWdCLEVBQUksQ0FDMUIsQ0FDUyxDQUN6QixDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/testimonial/container.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var container_TestimonialContainer = /** @class */ (function (_super) {
    __extends(TestimonialContainer, _super);
    function TestimonialContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    TestimonialContainer.prototype.componentDidMount = function () {
        this.initData();
    };
    TestimonialContainer.prototype.initData = function () {
        var _a = this.props, loveStore = _a.loveStore, fetchLoveListAction = _a.fetchLoveListAction, perPage = _a.perPage;
        var page = this.getPage(location.pathname);
        /** Love List - Feedback */
        var params = { page: page, perPage: perPage };
        var keyHash = Object(encode["j" /* objectToHash */])(params);
        fetchLoveListAction(params);
        !Object(validate["l" /* isUndefined */])(loveStore.loveList[keyHash])
            && this.initPagination(this.props);
    };
    TestimonialContainer.prototype.initPagination = function (props) {
        if (props === void 0) { props = this.props; }
        var loveList = props.loveStore.loveList, perPage = props.perPage, location = props.location;
        var page = this.getPage(location.pathname);
        var params = { page: page, perPage: perPage };
        var keyHash = Object(encode["j" /* objectToHash */])(params);
        var total_pages = (loveList[keyHash]
            && loveList[keyHash].paging || 0).total_pages;
        var urlList = [];
        for (var i = 1; i <= total_pages; i++) {
            urlList.push({
                number: i,
                title: i,
                link: routing["nb" /* ROUTING_TESTIMONIAL */] + "?page=" + i
            });
        }
        this.setState({ urlList: urlList });
    };
    TestimonialContainer.prototype.getPage = function (url) {
        var page = Object(format["e" /* getUrlParameter */])(location.search, 'page') || 1;
        this.setState({ page: page });
        return page;
    };
    TestimonialContainer.prototype.componentWillReceiveProps = function (nextProps) {
        var _a = this.props, isFetchLoveListSuccess = _a.loveStore.isFetchLoveListSuccess, fetchLoveListAction = _a.fetchLoveListAction, perPage = _a.perPage;
        var page = Object(format["e" /* getUrlParameter */])(location.search, 'page') || 1;
        var params = { page: page, perPage: perPage };
        page !== this.state.page
            && this.setState({ page: page }, function () { return fetchLoveListAction(params); });
        !isFetchLoveListSuccess
            && nextProps.loveStore.isFetchLoveListSuccess
            && this.initPagination(nextProps);
    };
    TestimonialContainer.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state
        };
        return view(args);
    };
    TestimonialContainer.defaultProps = DEFAULT_PROPS;
    TestimonialContainer = __decorate([
        radium
    ], TestimonialContainer);
    return TestimonialContainer;
}(react["PureComponent"]));
;
/* harmony default export */ var container = (container_TestimonialContainer);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ3RELE9BQU8sRUFBZ0IsWUFBWSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDbkUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBRXhELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBSTdFLE9BQU8sRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQzVELE9BQU8sVUFBVSxNQUFNLFFBQVEsQ0FBQztBQUdoQztJQUFtQyx3Q0FBbUM7SUFHcEUsOEJBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUF1QixDQUFDOztJQUN2QyxDQUFDO0lBRUQsZ0RBQWlCLEdBQWpCO1FBQ0UsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xCLENBQUM7SUFFRCx1Q0FBUSxHQUFSO1FBQ1EsSUFBQSxlQUFrRSxFQUFoRSx3QkFBUyxFQUFFLDRDQUFtQixFQUFFLG9CQUFPLENBQTBCO1FBRXpFLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRTdDLDJCQUEyQjtRQUMzQixJQUFNLE1BQU0sR0FBRyxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUM7UUFDakMsSUFBTSxPQUFPLEdBQUcsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3JDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxDQUFBO1FBQzNCLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7ZUFDcEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELDZDQUFjLEdBQWQsVUFBZSxLQUFrQjtRQUFsQixzQkFBQSxFQUFBLFFBQVEsSUFBSSxDQUFDLEtBQUs7UUFDVixJQUFBLG1DQUFRLEVBQUksdUJBQU8sRUFBRSx5QkFBUSxDQUFXO1FBRTdELElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzdDLElBQU0sTUFBTSxHQUFHLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQztRQUNqQyxJQUFNLE9BQU8sR0FBRyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFN0IsSUFBQTt5REFBVyxDQUVnQjtRQUVuQyxJQUFNLE9BQU8sR0FBZSxFQUFFLENBQUM7UUFFL0IsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxXQUFXLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUV0QyxPQUFPLENBQUMsSUFBSSxDQUFDO2dCQUNYLE1BQU0sRUFBRSxDQUFDO2dCQUNULEtBQUssRUFBRSxDQUFDO2dCQUNSLElBQUksRUFBSyxtQkFBbUIsY0FBUyxDQUFHO2FBQ3pDLENBQUMsQ0FBQztRQUNMLENBQUM7UUFFRCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDO0lBQzdCLENBQUM7SUFFRCxzQ0FBTyxHQUFQLFVBQVEsR0FBRztRQUNULElBQU0sSUFBSSxHQUFHLGVBQWUsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDO1FBRXhCLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsd0RBQXlCLEdBQXpCLFVBQTBCLFNBQVM7UUFDM0IsSUFBQSxlQUFvRixFQUFyRSw0REFBc0IsRUFBSSw0Q0FBbUIsRUFBRSxvQkFBTyxDQUFnQjtRQUczRixJQUFNLElBQUksR0FBRyxlQUFlLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0QsSUFBTSxNQUFNLEdBQUcsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDO1FBRWpDLElBQUksS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUk7ZUFDbkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLEVBQUUsY0FBTSxPQUFBLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxFQUEzQixDQUEyQixDQUFDLENBQUM7UUFFaEUsQ0FBQyxzQkFBc0I7ZUFDbEIsU0FBUyxDQUFDLFNBQVMsQ0FBQyxzQkFBc0I7ZUFDMUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQscUNBQU0sR0FBTjtRQUNFLElBQU0sSUFBSSxHQUFHO1lBQ1gsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztTQUNsQixDQUFBO1FBQ0QsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBN0VNLGlDQUFZLEdBQVcsYUFBYSxDQUFDO0lBRHhDLG9CQUFvQjtRQUR6QixNQUFNO09BQ0Qsb0JBQW9CLENBK0V6QjtJQUFELDJCQUFDO0NBQUEsQUEvRUQsQ0FBbUMsS0FBSyxDQUFDLGFBQWEsR0ErRXJEO0FBQUEsQ0FBQztBQUVGLGVBQWUsb0JBQW9CLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/testimonial/store.tsx
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapStateToProps", function() { return mapStateToProps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapDispatchToProps", function() { return mapDispatchToProps; });
var connect = __webpack_require__(129).connect;



var mapStateToProps = function (state) { return ({
    loveStore: state.love
}); };
var mapDispatchToProps = function (dispatch) { return ({
    fetchLoveListAction: function (data) { return dispatch(Object(love["b" /* fetchLoveListAction */])(data)); },
    openModal: function (data) { return dispatch(Object(modal["c" /* openModalAction */])(data)); },
}); };
/* harmony default export */ var store = __webpack_exports__["default"] = (connect(mapStateToProps, mapDispatchToProps)(container));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUMzRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDeEQsT0FBTyxvQkFBb0IsTUFBTSxhQUFhLENBQUM7QUFFL0MsTUFBTSxDQUFDLElBQU0sZUFBZSxHQUFHLFVBQUMsS0FBSyxJQUFLLE9BQUEsQ0FBQztJQUN6QyxTQUFTLEVBQUUsS0FBSyxDQUFDLElBQUk7Q0FDdEIsQ0FBQyxFQUZ3QyxDQUV4QyxDQUFDO0FBRUgsTUFBTSxDQUFDLElBQU0sa0JBQWtCLEdBQUcsVUFBQyxRQUFRLElBQUssT0FBQSxDQUFDO0lBQy9DLG1CQUFtQixFQUFFLFVBQUMsSUFBUyxJQUFLLE9BQUEsUUFBUSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxDQUFDLEVBQW5DLENBQW1DO0lBQ3ZFLFNBQVMsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBL0IsQ0FBK0I7Q0FDMUQsQ0FBQyxFQUg4QyxDQUc5QyxDQUFDO0FBRUgsZUFBZSxPQUFPLENBQ3BCLGVBQWUsRUFDZixrQkFBa0IsQ0FDbkIsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDIn0=

/***/ })

}]);