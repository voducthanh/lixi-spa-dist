(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[71,21],{

/***/ 753:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 97).then(__webpack_require__.bind(null, 771)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 754:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return updateMetaInfoAction; });
/* harmony import */ var _constants_api_meta__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(352);

var updateMetaInfoAction = function (data) { return ({
    type: _constants_api_meta__WEBPACK_IMPORTED_MODULE_0__[/* UPDATE_META_INFO */ "a"],
    payload: data
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWV0YS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1ldGEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLGdCQUFnQixFQUNqQixNQUFNLHVCQUF1QixDQUFDO0FBRS9CLE1BQU0sQ0FBQyxJQUFNLG9CQUFvQixHQUFHLFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FBQztJQUM3QyxJQUFJLEVBQUUsZ0JBQWdCO0lBQ3RCLE9BQU8sRUFBRSxJQUFJO0NBQ2QsQ0FBQyxFQUg0QyxDQUc1QyxDQUFDIn0=

/***/ }),

/***/ 757:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return renderHtmlContent; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var renderHtmlContent = function (message_html, style) {
    if (style === void 0) { style = {}; }
    return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { style: style, dangerouslySetInnerHTML: { __html: message_html } }));
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHRtbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImh0bWwudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRS9CLE1BQU0sQ0FBQyxJQUFNLGlCQUFpQixHQUFHLFVBQUMsWUFBb0IsRUFBRSxLQUFVO0lBQVYsc0JBQUEsRUFBQSxVQUFVO0lBQUssT0FBQSxDQUNyRSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxFQUFFLHVCQUF1QixFQUFFLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxHQUFJLENBQ3pFO0FBRnNFLENBRXRFLENBQUMifQ==

/***/ }),

/***/ 760:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// EXTERNAL MODULE: ./utils/auth.ts + 1 modules
var auth = __webpack_require__(23);

// CONCATENATED MODULE: ./api/shop.ts


var fetchDataHotDeal = function (_a) {
    var _b = _a.limit, limit = _b === void 0 ? 20 : _b;
    return Object(restful_method["b" /* get */])({
        path: "/shop/hot_deal?limit=" + limit,
        description: 'Get data hot deal in home page',
        errorMesssage: "Can't get data. Please try again",
    });
};
var fetchDataHomePage = function () { return Object(restful_method["b" /* get */])({
    path: '/shop',
    description: 'Get group data in home page',
    errorMesssage: "Can't get latest boxs data. Please try again",
}); };
var fetchProductByCategory = function (idCategory, query) {
    if (query === void 0) { query = ''; }
    return Object(restful_method["b" /* get */])({
        path: "/browse_nodes/" + idCategory + ".json" + query,
        description: 'Fetch list product in category | DANH SÁCH SẢN PHẨM TRONG CATEGORY',
        errorMesssage: "Can't get list product by category. Please try again",
    });
};
var getProductDetail = function (idProduct) { return Object(restful_method["b" /* get */])({
    path: "/boxes/" + idProduct + ".json",
    description: 'Get Product Detail | LẤY CHI TIẾT SẢN PHẨM',
    errorMesssage: "Can't get product detail. Please try again",
}); };
;
var generateParams = function (key, value) { return value ? "&" + key + "=" + value : ''; };
var fetchRedeemBoxes = function (_a) {
    var _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 72 : _c, filter = _a.filter, sort = _a.sort;
    var query = "?page=" + page + "&per_page=" + perPage
        + generateParams('filter[brand]', filter && filter.brand)
        + generateParams('filter[category]', filter && filter.category)
        + generateParams('filter[coins_price]', filter && filter.coinsPrice)
        + generateParams('sort[coins_price]', filter && sort && sort.coinsPrice);
    return Object(restful_method["b" /* get */])({
        path: "/boxes/redeems" + query,
        description: 'Fetch list redeem boxes',
        errorMesssage: "Can't fetch data. Please try again",
    });
};
var addWaitList = function (_a) {
    var boxId = _a.boxId;
    var data = {
        csrf_token: Object(auth["c" /* getCsrfToken */])(),
    };
    return Object(restful_method["d" /* post */])({
        path: "/boxes/" + boxId + "/waitlist",
        data: data,
        description: 'Add item intowait list | BOXES - WAIT LIST',
        errorMesssage: "Can't Get listwait list. Please try again",
    });
};
var fetchFeedbackBoxes = function (_a) {
    var productId = _a.productId, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c;
    var query = "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/boxes/" + productId + "/feedbacks" + query,
        description: 'Fetch list feedbacks boxes',
        errorMesssage: "Can't fetch data. Please try again",
    });
};
var fetchSavingSetsBoxes = function (_a) {
    var productId = _a.productId, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c;
    var query = "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/boxes/" + productId + "/saving_sets" + query,
        description: 'Fetch list saving sets boxes',
        errorMesssage: "Can't fetch data. Please try again",
    });
};
var fetchMagazinesBoxes = function (_a) {
    var productId = _a.productId, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c;
    var query = "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/boxes/" + productId + "/magazines" + query,
        description: 'Fetch magazine boxes',
        errorMesssage: "Can't fetch data. Please try again",
    });
};
var fetchRelatedBoxes = function (_a) {
    var productId = _a.productId, _b = _a.limit, limit = _b === void 0 ? 10 : _b;
    var query = "?limit=" + limit;
    return Object(restful_method["b" /* get */])({
        path: "/boxes/" + productId + "/related_boxes" + query,
        description: 'Fetch related boxes',
        errorMesssage: "Can't fetch data. Please try again",
    });
};
var fetchMakeups = function (_a) {
    var boxId = _a.boxId, _b = _a.limit, limit = _b === void 0 ? 10 : _b;
    return Object(restful_method["b" /* get */])({
        path: "/makeups/" + boxId + "?limit=" + limit,
        description: 'Fetch makeups',
        errorMesssage: "Can't fetch data. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hvcC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNob3AudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTdDLE1BQU0sQ0FBQyxJQUFNLGdCQUFnQixHQUFHLFVBQUMsRUFBYztRQUFaLGFBQVUsRUFBViwrQkFBVTtJQUFPLE9BQUEsR0FBRyxDQUFDO1FBQ3RELElBQUksRUFBRSwwQkFBd0IsS0FBTztRQUNyQyxXQUFXLEVBQUUsZ0NBQWdDO1FBQzdDLGFBQWEsRUFBRSxrQ0FBa0M7S0FDbEQsQ0FBQztBQUprRCxDQUlsRCxDQUFDO0FBRUgsTUFBTSxDQUFDLElBQU0saUJBQWlCLEdBQUcsY0FBTSxPQUFBLEdBQUcsQ0FBQztJQUN6QyxJQUFJLEVBQUUsT0FBTztJQUNiLFdBQVcsRUFBRSw2QkFBNkI7SUFDMUMsYUFBYSxFQUFFLDhDQUE4QztDQUM5RCxDQUFDLEVBSnFDLENBSXJDLENBQUM7QUFFSCxNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FDakMsVUFBQyxVQUFrQixFQUFFLEtBQVU7SUFBVixzQkFBQSxFQUFBLFVBQVU7SUFBSyxPQUFBLEdBQUcsQ0FBQztRQUN0QyxJQUFJLEVBQUUsbUJBQWlCLFVBQVUsYUFBUSxLQUFPO1FBQ2hELFdBQVcsRUFBRSxvRUFBb0U7UUFDakYsYUFBYSxFQUFFLHNEQUFzRDtLQUN0RSxDQUFDO0FBSmtDLENBSWxDLENBQUM7QUFFTCxNQUFNLENBQUMsSUFBTSxnQkFBZ0IsR0FDM0IsVUFBQyxTQUFpQixJQUFLLE9BQUEsR0FBRyxDQUFDO0lBQ3pCLElBQUksRUFBRSxZQUFVLFNBQVMsVUFBTztJQUNoQyxXQUFXLEVBQUUsNENBQTRDO0lBQ3pELGFBQWEsRUFBRSw0Q0FBNEM7Q0FDNUQsQ0FBQyxFQUpxQixDQUlyQixDQUFDO0FBYUosQ0FBQztBQUVGLElBQU0sY0FBYyxHQUFHLFVBQUMsR0FBRyxFQUFFLEtBQUssSUFBSyxPQUFBLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBSSxHQUFHLFNBQUksS0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQS9CLENBQStCLENBQUM7QUFFdkUsTUFBTSxDQUFDLElBQU0sZ0JBQWdCLEdBQzNCLFVBQUMsRUFBZ0U7UUFBOUQsWUFBUSxFQUFSLDZCQUFRLEVBQUUsZUFBWSxFQUFaLGlDQUFZLEVBQUUsa0JBQU0sRUFBRSxjQUFJO0lBRXJDLElBQU0sS0FBSyxHQUNULFdBQVMsSUFBSSxrQkFBYSxPQUFTO1VBQ2pDLGNBQWMsQ0FBQyxlQUFlLEVBQUUsTUFBTSxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUM7VUFDdkQsY0FBYyxDQUFDLGtCQUFrQixFQUFFLE1BQU0sSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDO1VBQzdELGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxNQUFNLElBQUksTUFBTSxDQUFDLFVBQVUsQ0FBQztVQUNsRSxjQUFjLENBQUMsbUJBQW1CLEVBQUUsTUFBTSxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQ3ZFO0lBRUgsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSxtQkFBaUIsS0FBTztRQUM5QixXQUFXLEVBQUUseUJBQXlCO1FBQ3RDLGFBQWEsRUFBRSxvQ0FBb0M7S0FDcEQsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUosTUFBTSxDQUFDLElBQU0sV0FBVyxHQUN0QixVQUFDLEVBQVM7UUFBUCxnQkFBSztJQUNOLElBQU0sSUFBSSxHQUFHO1FBQ1gsVUFBVSxFQUFFLFlBQVksRUFBRTtLQUMzQixDQUFDO0lBRUYsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNWLElBQUksRUFBRSxZQUFVLEtBQUssY0FBVztRQUNoQyxJQUFJLE1BQUE7UUFDSixXQUFXLEVBQUUsNENBQTRDO1FBQ3pELGFBQWEsRUFBRSwyQ0FBMkM7S0FDM0QsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUosTUFBTSxDQUFDLElBQU0sa0JBQWtCLEdBQzdCLFVBQUMsRUFBcUM7UUFBbkMsd0JBQVMsRUFBRSxZQUFRLEVBQVIsNkJBQVEsRUFBRSxlQUFZLEVBQVosaUNBQVk7SUFDbEMsSUFBTSxLQUFLLEdBQUcsV0FBUyxJQUFJLGtCQUFhLE9BQVMsQ0FBQztJQUVsRCxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLFlBQVUsU0FBUyxrQkFBYSxLQUFPO1FBQzdDLFdBQVcsRUFBRSw0QkFBNEI7UUFDekMsYUFBYSxFQUFFLG9DQUFvQztLQUNwRCxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSixNQUFNLENBQUMsSUFBTSxvQkFBb0IsR0FDL0IsVUFBQyxFQUFxQztRQUFuQyx3QkFBUyxFQUFFLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUNsQyxJQUFNLEtBQUssR0FBRyxXQUFTLElBQUksa0JBQWEsT0FBUyxDQUFDO0lBRWxELE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsWUFBVSxTQUFTLG9CQUFlLEtBQU87UUFDL0MsV0FBVyxFQUFFLDhCQUE4QjtRQUMzQyxhQUFhLEVBQUUsb0NBQW9DO0tBQ3BELENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQztBQUVKLE1BQU0sQ0FBQyxJQUFNLG1CQUFtQixHQUM5QixVQUFDLEVBQXFDO1FBQW5DLHdCQUFTLEVBQUUsWUFBUSxFQUFSLDZCQUFRLEVBQUUsZUFBWSxFQUFaLGlDQUFZO0lBQ2xDLElBQU0sS0FBSyxHQUFHLFdBQVMsSUFBSSxrQkFBYSxPQUFTLENBQUM7SUFFbEQsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSxZQUFVLFNBQVMsa0JBQWEsS0FBTztRQUM3QyxXQUFXLEVBQUUsc0JBQXNCO1FBQ25DLGFBQWEsRUFBRSxvQ0FBb0M7S0FDcEQsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUosTUFBTSxDQUFDLElBQU0saUJBQWlCLEdBQzVCLFVBQUMsRUFBeUI7UUFBdkIsd0JBQVMsRUFBRSxhQUFVLEVBQVYsK0JBQVU7SUFDdEIsSUFBTSxLQUFLLEdBQUcsWUFBVSxLQUFPLENBQUM7SUFFaEMsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSxZQUFVLFNBQVMsc0JBQWlCLEtBQU87UUFDakQsV0FBVyxFQUFFLHFCQUFxQjtRQUNsQyxhQUFhLEVBQUUsb0NBQW9DO0tBQ3BELENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQztBQUVKLE1BQU0sQ0FBQyxJQUFNLFlBQVksR0FBRyxVQUFDLEVBQXFCO1FBQW5CLGdCQUFLLEVBQUUsYUFBVSxFQUFWLCtCQUFVO0lBQU8sT0FBQSxHQUFHLENBQUM7UUFDdkQsSUFBSSxFQUFFLGNBQVksS0FBSyxlQUFVLEtBQU87UUFDeEMsV0FBVyxFQUFFLGVBQWU7UUFDNUIsYUFBYSxFQUFFLG9DQUFvQztLQUNwRCxDQUFDO0FBSm1ELENBSW5ELENBQUMifQ==
// EXTERNAL MODULE: ./constants/api/shop.ts
var shop = __webpack_require__(17);

// CONCATENATED MODULE: ./action/shop.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fetchDataHomePageAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return fetchProductByCategoryAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "l", function() { return updateProductNameMobileAction; });
/* unused harmony export updateCategoryFilterStateAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "k", function() { return getProductDetailAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return fetchRedeemBoxesAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return addToWaitListAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return fetchFeedbackBoxesAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "j", function() { return fetchSavingSetsBoxesAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return fetchMagazinesBoxesAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return fetchRelatedBoxesAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return fetchDataHotDealAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return fetchMakeupsAction; });


/** Get collection data in home page */
var fetchDataHomePageAction = function () { return function (dispatch, getState) {
    return dispatch({
        type: shop["b" /* FECTH_DATA_HOME_PAGE */],
        payload: { promise: fetchDataHomePage().then(function (res) { return res; }) },
    });
}; };
/** Get Product list by cagegory old version */
/*
export const fetchProductByCategoryAction =
  (categoryFilter: any) => {
    const [idCategory, params] = categoryFilterApiFormat(categoryFilter);

    return (dispatch, getState) => dispatch({
      type: FETCH_PRODUCT_BY_CATEGORY,
      payload: { promise: fetchProductByCategory(idCategory, params).then(res => res) },
      meta: { metaFilter: { idCategory, params } }
    });
  };
  */
/** Get Product list by cagegory new version */
var fetchProductByCategoryAction = function (_a) {
    var idCategory = _a.idCategory, searchQuery = _a.searchQuery;
    return function (dispatch, getState) { return dispatch({
        type: shop["g" /* FETCH_PRODUCT_BY_CATEGORY */],
        payload: { promise: fetchProductByCategory(idCategory, searchQuery).then(function (res) { return res; }) },
        meta: { metaFilter: { idCategory: idCategory, searchQuery: searchQuery } }
    }); };
};
/**  Update Product name cho Product detail trên Mobile */
var updateProductNameMobileAction = function (productName) { return ({
    type: shop["m" /* UPDATE_PRODUCT_NAME_MOBILE */],
    payload: productName
}); };
/**  Update filter for category */
var updateCategoryFilterStateAction = function (categoryFilter) { return ({
    type: shop["l" /* UPDATE_CATEGORY_FILTER_STATE */],
    payload: categoryFilter
}); };
/** Get product detail */
var getProductDetailAction = function (productId) {
    return function (dispatch, getState) {
        return dispatch({
            type: shop["k" /* GET_PRODUCT_DETAIL */],
            payload: { promise: getProductDetail(productId).then(function (res) { return res; }) },
            meta: { productId: productId }
        });
    };
};
/** Fetch redeem boxes list */
var fetchRedeemBoxesAction = function (_a) {
    var page = _a.page, _b = _a.perPage, perPage = _b === void 0 ? 72 : _b, filter = _a.filter, sort = _a.sort;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["h" /* FETCH_REDEEM_BOXES */],
            payload: { promise: fetchRedeemBoxes({ page: page, perPage: perPage, filter: filter, sort: sort }).then(function (res) { return res; }) },
            meta: { page: page, perPage: perPage }
        });
    };
};
/** Add product into wait list */
var addToWaitListAction = function (_a) {
    var boxId = _a.boxId, _b = _a.slug, slug = _b === void 0 ? '' : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["a" /* ADD_WAIT_LIST */],
            payload: { promise: addWaitList({ boxId: boxId }).then(function (res) { return res; }) },
            dispatch: dispatch,
            meta: { slug: slug }
        });
    };
};
/**
 * Fetch feebbacks boxes list by id or slug of product
 *
 * @param{string} id or slug of product
 * @param{number} page
 * @param{string} perPage
 */
var fetchFeedbackBoxesAction = function (_a) {
    var productId = _a.productId, page = _a.page, _b = _a.perPage, perPage = _b === void 0 ? 10 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["d" /* FETCH_FEEDBACK_BOXES */],
            payload: { promise: fetchFeedbackBoxes({ productId: productId, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { productId: productId, page: page, perPage: perPage }
        });
    };
};
/**
 * Fetch saving sets boxes list by id or slug of product
 *
 * @param{string} id or slug of product
 * @param{number} page
 * @param{string} perPage
 */
var fetchSavingSetsBoxesAction = function (_a) {
    var productId = _a.productId, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["j" /* FETCH_SAVING_SETS_BOXES */],
            payload: { promise: fetchSavingSetsBoxes({ productId: productId, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { productId: productId, page: page, perPage: perPage }
        });
    };
};
/**
 * Fetch magazine boxes list by id or slug of product
 *
 * @param{string} id or slug of product
 * @param{number} page
 * @param{number} perPage
 */
var fetchMagazinesBoxesAction = function (_a) {
    var productId = _a.productId, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["e" /* FETCH_MAGAZINES_BOXES */],
            payload: { promise: fetchMagazinesBoxes({ productId: productId, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { productId: productId, page: page, perPage: perPage }
        });
    };
};
/**
* Fetch related boxes list by id or slug of product
*
* @param{string} id or slug of product
* @param{limit} number
*/
var fetchRelatedBoxesAction = function (_a) {
    var productId = _a.productId, _b = _a.limit, limit = _b === void 0 ? 10 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["i" /* FETCH_RELATED_BOXES */],
            payload: { promise: fetchRelatedBoxes({ productId: productId, limit: limit }).then(function (res) { return res; }) },
            meta: { productId: productId }
        });
    };
};
var fetchDataHotDealAction = function (_a) {
    var limit = _a.limit;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["c" /* FECTH_DATA_HOT_DEAL */],
            payload: { promise: fetchDataHotDeal({ limit: limit }).then(function (res) { return res; }) },
        });
    };
};
/**
* Fetch makeups by id or slug of product
*
* @param{string} id or slug of product
* @param{limit} number
*/
var fetchMakeupsAction = function (_a) {
    var boxId = _a.boxId, _b = _a.limit, limit = _b === void 0 ? 10 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["f" /* FETCH_MAKEUPS */],
            payload: { promise: fetchMakeups({ boxId: boxId, limit: limit }).then(function (res) { return res; }) },
            meta: { boxId: boxId }
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hvcC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNob3AudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUEsT0FBTyxFQUNMLGdCQUFnQixFQUNoQixzQkFBc0IsRUFDdEIsaUJBQWlCLEVBQ2pCLGdCQUFnQixFQUVoQixnQkFBZ0IsRUFDaEIsV0FBVyxFQUNYLGtCQUFrQixFQUNsQixvQkFBb0IsRUFDcEIsbUJBQW1CLEVBQ25CLGlCQUFpQixFQUNqQixZQUFZLEVBQ2IsTUFBTSxhQUFhLENBQUM7QUFFckIsT0FBTyxFQUNMLG1CQUFtQixFQUNuQixvQkFBb0IsRUFDcEIseUJBQXlCLEVBQ3pCLDRCQUE0QixFQUM1QiwwQkFBMEIsRUFDMUIsa0JBQWtCLEVBQ2xCLGtCQUFrQixFQUNsQixhQUFhLEVBQ2Isb0JBQW9CLEVBQ3BCLHVCQUF1QixFQUN2QixxQkFBcUIsRUFDckIsbUJBQW1CLEVBQ25CLGFBQWEsRUFDZCxNQUFNLHVCQUF1QixDQUFDO0FBRS9CLHVDQUF1QztBQUN2QyxNQUFNLENBQUMsSUFBTSx1QkFBdUIsR0FDbEMsY0FBTSxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7SUFDdkIsT0FBQSxRQUFRLENBQUM7UUFDUCxJQUFJLEVBQUUsb0JBQW9CO1FBQzFCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtLQUMzRCxDQUFDO0FBSEYsQ0FHRSxFQUpFLENBSUYsQ0FBQztBQUVQLCtDQUErQztBQUMvQzs7Ozs7Ozs7Ozs7SUFXSTtBQUVKLCtDQUErQztBQUMvQyxNQUFNLENBQUMsSUFBTSw0QkFBNEIsR0FBRyxVQUFDLEVBQTJCO1FBQXpCLDBCQUFVLEVBQUUsNEJBQVc7SUFFcEUsTUFBTSxDQUFDLFVBQUMsUUFBUSxFQUFFLFFBQVEsSUFBSyxPQUFBLFFBQVEsQ0FBQztRQUN0QyxJQUFJLEVBQUUseUJBQXlCO1FBQy9CLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxzQkFBc0IsQ0FBQyxVQUFVLEVBQUUsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1FBQ3RGLElBQUksRUFBRSxFQUFFLFVBQVUsRUFBRSxFQUFFLFVBQVUsWUFBQSxFQUFFLFdBQVcsYUFBQSxFQUFFLEVBQUU7S0FDbEQsQ0FBQyxFQUo2QixDQUk3QixDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUYsMERBQTBEO0FBQzFELE1BQU0sQ0FBQyxJQUFNLDZCQUE2QixHQUN4QyxVQUFDLFdBQWdCLElBQUssT0FBQSxDQUFDO0lBQ3JCLElBQUksRUFBRSwwQkFBMEI7SUFDaEMsT0FBTyxFQUFFLFdBQVc7Q0FDckIsQ0FBQyxFQUhvQixDQUdwQixDQUFDO0FBRUwsa0NBQWtDO0FBQ2xDLE1BQU0sQ0FBQyxJQUFNLCtCQUErQixHQUMxQyxVQUFDLGNBQW1CLElBQUssT0FBQSxDQUFDO0lBQ3hCLElBQUksRUFBRSw0QkFBNEI7SUFDbEMsT0FBTyxFQUFFLGNBQWM7Q0FDeEIsQ0FBQyxFQUh1QixDQUd2QixDQUFDO0FBRUwseUJBQXlCO0FBQ3pCLE1BQU0sQ0FBQyxJQUFNLHNCQUFzQixHQUNqQyxVQUFDLFNBQWlCO0lBQ2hCLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxrQkFBa0I7WUFDeEIsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUNsRSxJQUFJLEVBQUUsRUFBRSxTQUFTLFdBQUEsRUFBRTtTQUNwQixDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUVULDhCQUE4QjtBQUM5QixNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FDakMsVUFBQyxFQUE0RDtRQUExRCxjQUFJLEVBQUUsZUFBWSxFQUFaLGlDQUFZLEVBQUUsa0JBQU0sRUFBRSxjQUFJO0lBQ2pDLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxrQkFBa0I7WUFDeEIsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLGdCQUFnQixDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsTUFBTSxRQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN4RixJQUFJLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRTtTQUN4QixDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUVULGlDQUFpQztBQUNqQyxNQUFNLENBQUMsSUFBTSxtQkFBbUIsR0FDOUIsVUFBQyxFQUFvQjtRQUFsQixnQkFBSyxFQUFFLFlBQVMsRUFBVCw4QkFBUztJQUNqQixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsYUFBYTtZQUNuQixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsV0FBVyxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUM3RCxRQUFRLFVBQUE7WUFDUixJQUFJLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRTtTQUNmLENBQUM7SUFMRixDQUtFO0FBTkosQ0FNSSxDQUFDO0FBRVQ7Ozs7OztHQU1HO0FBQ0gsTUFBTSxDQUFDLElBQU0sd0JBQXdCLEdBQ25DLFVBQUMsRUFBaUM7UUFBL0Isd0JBQVMsRUFBRSxjQUFJLEVBQUUsZUFBWSxFQUFaLGlDQUFZO0lBQzlCLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxvQkFBb0I7WUFDMUIsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLGtCQUFrQixDQUFDLEVBQUUsU0FBUyxXQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN2RixJQUFJLEVBQUUsRUFBRSxTQUFTLFdBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRTtTQUNuQyxDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUVUOzs7Ozs7R0FNRztBQUNILE1BQU0sQ0FBQyxJQUFNLDBCQUEwQixHQUNyQyxVQUFDLEVBQXFDO1FBQW5DLHdCQUFTLEVBQUUsWUFBUSxFQUFSLDZCQUFRLEVBQUUsZUFBWSxFQUFaLGlDQUFZO0lBQ2xDLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSx1QkFBdUI7WUFDN0IsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLG9CQUFvQixDQUFDLEVBQUUsU0FBUyxXQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN6RixJQUFJLEVBQUUsRUFBRSxTQUFTLFdBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRTtTQUNuQyxDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUVUOzs7Ozs7R0FNRztBQUNILE1BQU0sQ0FBQyxJQUFNLHlCQUF5QixHQUNwQyxVQUFDLEVBQXFDO1FBQW5DLHdCQUFTLEVBQUUsWUFBUSxFQUFSLDZCQUFRLEVBQUUsZUFBWSxFQUFaLGlDQUFZO0lBQ2xDLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxxQkFBcUI7WUFDM0IsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLG1CQUFtQixDQUFDLEVBQUUsU0FBUyxXQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN4RixJQUFJLEVBQUUsRUFBRSxTQUFTLFdBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRTtTQUNuQyxDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUVUOzs7OztFQUtFO0FBQ0YsTUFBTSxDQUFDLElBQU0sdUJBQXVCLEdBQ2xDLFVBQUMsRUFBeUI7UUFBdkIsd0JBQVMsRUFBRSxhQUFVLEVBQVYsK0JBQVU7SUFDdEIsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLG1CQUFtQjtZQUN6QixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsaUJBQWlCLENBQUMsRUFBRSxTQUFTLFdBQUEsRUFBRSxLQUFLLE9BQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQzlFLElBQUksRUFBRSxFQUFFLFNBQVMsV0FBQSxFQUFFO1NBQ3BCLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQsTUFBTSxDQUFDLElBQU0sc0JBQXNCLEdBQUcsVUFBQyxFQUFTO1FBQVAsZ0JBQUs7SUFDNUMsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLG1CQUFtQjtZQUN6QixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1NBQ25FLENBQUM7SUFIRixDQUdFO0FBSkosQ0FJSSxDQUFDO0FBRVA7Ozs7O0VBS0U7QUFDRixNQUFNLENBQUMsSUFBTSxrQkFBa0IsR0FDN0IsVUFBQyxFQUFxQjtRQUFuQixnQkFBSyxFQUFFLGFBQVUsRUFBViwrQkFBVTtJQUNsQixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsYUFBYTtZQUNuQixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsWUFBWSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUNyRSxJQUFJLEVBQUUsRUFBRSxLQUFLLE9BQUEsRUFBRTtTQUNoQixDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQyJ9

/***/ }),

/***/ 761:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// EXTERNAL MODULE: ./utils/auth.ts + 1 modules
var auth = __webpack_require__(23);

// CONCATENATED MODULE: ./api/activity-feed.ts


;
var fecthActivityFeedList = function (_a) {
    var _b = _a.userId, userId = _b === void 0 ? 0 : _b, _c = _a.limit, limit = _c === void 0 ? 20 : _c, _d = _a.currentId, currentId = _d === void 0 ? 0 : _d, _e = _a.feedType, feedType = _e === void 0 ? '' : _e;
    var query = "?limit=" + limit
        + (!!currentId ? "&current_id=" + currentId : '')
        + (!!userId ? "&user_id=" + userId : '')
        + (!!feedType ? "&feed_type=" + feedType : '');
    return Object(restful_method["b" /* get */])({
        path: "/activity_feeds" + query,
        description: "Fetch list activity feed",
        errorMesssage: "Can't fetch data activity feeds. Please try again",
    });
};
;
var fecthActivityFeedCommentList = function (_a) {
    var id = _a.id, lastCommentId = _a.lastCommentId, page = _a.page, perPage = _a.perPage;
    var lastCommentParams = 'undefined' !== typeof lastCommentId ? "&last_comment_id=" + lastCommentId : '';
    var query = "?&page=" + page + "&per_page=" + perPage + lastCommentParams;
    return Object(restful_method["b" /* get */])({
        path: "/activity_feeds/" + id + "/comments" + query,
        description: "Fetch activity feed comment list",
        errorMesssage: "Can't fetch activity feed comment list. Please try again",
    });
};
;
var addActivityFeedComment = function (_a) {
    var id = _a.id, content = _a.content, lastCommentId = _a.lastCommentId;
    return Object(restful_method["d" /* post */])({
        path: "/activity_feeds/" + id + "/comments",
        data: {
            csrf_token: Object(auth["c" /* getCsrfToken */])(),
            content: content,
            last_comment_id: lastCommentId,
        },
        description: "Add activity feed comment",
        errorMesssage: "Can't add activity feed comment. Please try again",
    });
};
;
var addActivityFeedLike = function (_a) {
    var id = _a.id;
    return Object(restful_method["d" /* post */])({
        path: "/activity_feeds/" + id + "/like",
        data: {
            csrf_token: Object(auth["c" /* getCsrfToken */])()
        },
        description: "Add activity feed like",
        errorMesssage: "Can't add activity feed like. Please try again",
    });
};
;
var deleteActivityFeedLike = function (_a) {
    var id = _a.id;
    return Object(restful_method["a" /* del */])({
        path: "/activity_feeds/" + id + "/unlike",
        data: { csrf_token: Object(auth["c" /* getCsrfToken */])() },
        description: "Delete activity feed like",
        errorMesssage: "Can't delete activity feed like. Please try again",
    });
};
var fetchActivityFeedDetail = function (_a) {
    var feedId = _a.feedId;
    return Object(restful_method["b" /* get */])({
        path: "/activity_feeds/" + feedId,
        description: "Fetch activity feed detail",
        errorMesssage: "Can't fetch activity feed detail. Please try again",
    });
};
/**
 * Get Collection (Top feed)
 *
 * @param {number} page ex: 1
 * @param {number} perPage ex: 12
 */
var getCollection = function (_a) {
    var _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    var query = "?&page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/community" + query,
        description: "Get collection",
        errorMesssage: "Can't get collection. Please try again",
    });
};
/**
* Get Collection detail (Top feed)
*
* @param {number} id ex: 1
*/
var getCollectionDetail = function (_a) {
    var id = _a.id;
    return Object(restful_method["b" /* get */])({
        path: "/community/" + id,
        description: "Get collection detail",
        errorMesssage: "Can't get collection detail. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0aXZpdHktZmVlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFjdGl2aXR5LWZlZWQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDMUQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQVE1QyxDQUFDO0FBRUYsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQ2hDLFVBQUMsRUFBZ0Y7UUFBOUUsY0FBVSxFQUFWLCtCQUFVLEVBQUUsYUFBVSxFQUFWLCtCQUFVLEVBQUUsaUJBQWEsRUFBYixrQ0FBYSxFQUFFLGdCQUFhLEVBQWIsa0NBQWE7SUFDckQsSUFBTSxLQUFLLEdBQUcsWUFBVSxLQUFPO1VBQzNCLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsaUJBQWUsU0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7VUFDL0MsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxjQUFZLE1BQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1VBQ3RDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsZ0JBQWMsUUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUVqRCxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLG9CQUFrQixLQUFPO1FBQy9CLFdBQVcsRUFBRSwwQkFBMEI7UUFDdkMsYUFBYSxFQUFFLG1EQUFtRDtLQUNuRSxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFRSCxDQUFDO0FBRUYsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsRUFBbUU7UUFBakUsVUFBRSxFQUFFLGdDQUFhLEVBQUUsY0FBSSxFQUFFLG9CQUFPO0lBQ2pDLElBQU0saUJBQWlCLEdBQUcsV0FBVyxLQUFLLE9BQU8sYUFBYSxDQUFDLENBQUMsQ0FBQyxzQkFBb0IsYUFBZSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDMUcsSUFBTSxLQUFLLEdBQUcsWUFBVSxJQUFJLGtCQUFhLE9BQU8sR0FBRyxpQkFBbUIsQ0FBQztJQUV2RSxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLHFCQUFtQixFQUFFLGlCQUFZLEtBQU87UUFDOUMsV0FBVyxFQUFFLGtDQUFrQztRQUMvQyxhQUFhLEVBQUUsMERBQTBEO0tBQzFFLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQztBQU9ILENBQUM7QUFFRixNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FDakMsVUFBQyxFQUE0RDtRQUExRCxVQUFFLEVBQUUsb0JBQU8sRUFBRSxnQ0FBYTtJQUFxQyxPQUFBLElBQUksQ0FBQztRQUNyRSxJQUFJLEVBQUUscUJBQW1CLEVBQUUsY0FBVztRQUN0QyxJQUFJLEVBQUU7WUFDSixVQUFVLEVBQUUsWUFBWSxFQUFFO1lBQzFCLE9BQU8sRUFBRSxPQUFPO1lBQ2hCLGVBQWUsRUFBRSxhQUFhO1NBQy9CO1FBQ0QsV0FBVyxFQUFFLDJCQUEyQjtRQUN4QyxhQUFhLEVBQUUsbURBQW1EO0tBQ25FLENBQUM7QUFUZ0UsQ0FTaEUsQ0FBQztBQUtKLENBQUM7QUFFRixNQUFNLENBQUMsSUFBTSxtQkFBbUIsR0FBRyxVQUFDLEVBQWlDO1FBQS9CLFVBQUU7SUFBa0MsT0FBQSxJQUFJLENBQUM7UUFDN0UsSUFBSSxFQUFFLHFCQUFtQixFQUFFLFVBQU87UUFDbEMsSUFBSSxFQUFFO1lBQ0osVUFBVSxFQUFFLFlBQVksRUFBRTtTQUMzQjtRQUNELFdBQVcsRUFBRSx3QkFBd0I7UUFDckMsYUFBYSxFQUFFLGdEQUFnRDtLQUNoRSxDQUFDO0FBUHdFLENBT3hFLENBQUM7QUFLRixDQUFDO0FBRUYsTUFBTSxDQUFDLElBQU0sc0JBQXNCLEdBQUcsVUFBQyxFQUFvQztRQUFsQyxVQUFFO0lBQXFDLE9BQUEsR0FBRyxDQUFDO1FBQ2xGLElBQUksRUFBRSxxQkFBbUIsRUFBRSxZQUFTO1FBQ3BDLElBQUksRUFBRSxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsRUFBRTtRQUNwQyxXQUFXLEVBQUUsMkJBQTJCO1FBQ3hDLGFBQWEsRUFBRSxtREFBbUQ7S0FDbkUsQ0FBQztBQUw4RSxDQUs5RSxDQUFDO0FBRUgsTUFBTSxDQUFDLElBQU0sdUJBQXVCLEdBQUcsVUFBQyxFQUFVO1FBQVIsa0JBQU07SUFBTyxPQUFBLEdBQUcsQ0FBQztRQUN6RCxJQUFJLEVBQUUscUJBQW1CLE1BQVE7UUFDakMsV0FBVyxFQUFFLDRCQUE0QjtRQUN6QyxhQUFhLEVBQUUsb0RBQW9EO0tBQ3BFLENBQUM7QUFKcUQsQ0FJckQsQ0FBQztBQUVIOzs7OztHQUtHO0FBQ0gsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUN4QixVQUFDLEVBQTBCO1FBQXhCLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUN2QixJQUFNLEtBQUssR0FBRyxZQUFVLElBQUksa0JBQWEsT0FBUyxDQUFDO0lBRW5ELE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsZUFBYSxLQUFPO1FBQzFCLFdBQVcsRUFBRSxnQkFBZ0I7UUFDN0IsYUFBYSxFQUFFLHdDQUF3QztLQUN4RCxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSjs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0sbUJBQW1CLEdBQUcsVUFBQyxFQUFNO1FBQUosVUFBRTtJQUFPLE9BQUEsR0FBRyxDQUFDO1FBQ2pELElBQUksRUFBRSxnQkFBYyxFQUFJO1FBQ3hCLFdBQVcsRUFBRSx1QkFBdUI7UUFDcEMsYUFBYSxFQUFFLCtDQUErQztLQUMvRCxDQUFDO0FBSjZDLENBSTdDLENBQUMifQ==
// EXTERNAL MODULE: ./constants/api/activity-feed.ts
var activity_feed = __webpack_require__(27);

// CONCATENATED MODULE: ./action/activity-feed.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return fecthActivityFeedListAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return fecthActivityFeedCommentListAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return addActivityFeedCommentAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return addActivityFeedLikeAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return deleteActivityFeedLikeAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return fetchActivityFeedDetailAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return getCollectionAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return getCollectionDetailAction; });


/**
 * Fetch activity feed list by filter (limit & current_id)
 *
 * @param {number} limit default 20
 * @param {number} currentId
 */
var fecthActivityFeedListAction = function (_a) {
    var _b = _a.limit, limit = _b === void 0 ? 20 : _b, _c = _a.currentId, currentId = _c === void 0 ? 0 : _c, _d = _a.userId, userId = _d === void 0 ? 0 : _d, _e = _a.feedType, feedType = _e === void 0 ? '' : _e;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["f" /* FETCH_ACTIVITY_FEED_LIST */],
            payload: { promise: fecthActivityFeedList({ limit: limit, currentId: currentId, userId: userId, feedType: feedType }).then(function (res) { return res; }) },
            meta: { metaFilter: { limit: limit, currentId: currentId } }
        });
    };
};
/**
* Fetch activity feed comment list
*
* @param {number} id
* @param {number} lastCommentId
* @param {number} page
* @param {number} perPage
*/
var fecthActivityFeedCommentListAction = function (_a) {
    var id = _a.id, lastCommentId = _a.lastCommentId, page = _a.page, perPage = _a.perPage;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["d" /* FETCH_ACTIVITY_FEED_COMMENT_LIST */],
            payload: { promise: fecthActivityFeedCommentList({ id: id, lastCommentId: lastCommentId, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
/**
* Delete activity feed comment
*
* @param {number} id
* @param {string} content
* @param {number} lastCommentId
*/
var addActivityFeedCommentAction = function (_a) {
    var id = _a.id, content = _a.content, lastCommentId = _a.lastCommentId;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["a" /* ADD_ACTIVITY_FEED_COMMENT */],
            payload: { promise: addActivityFeedComment({ id: id, content: content, lastCommentId: lastCommentId }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
/**
* Add activity feed like
*
* @param {number} id
*/
var addActivityFeedLikeAction = function (_a) {
    var id = _a.id;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["b" /* ADD_ACTIVITY_FEED_LIKE */],
            payload: { promise: addActivityFeedLike({ id: id }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
/**
* Delete activity feed like
*
* @param {number} id
*/
var deleteActivityFeedLikeAction = function (_a) {
    var id = _a.id;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["c" /* DELETE_ACTIVITY_FEED_LIKE */],
            payload: { promise: deleteActivityFeedLike({ id: id }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
/**
* Fetch activity feed detail by id
*
* @param {number} feedId
*/
var fetchActivityFeedDetailAction = function (_a) {
    var feedId = _a.feedId;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["e" /* FETCH_ACTIVITY_FEED_DETAIL */],
            payload: { promise: fetchActivityFeedDetail({ feedId: feedId }).then(function (res) { return res; }) },
            meta: { feedId: feedId }
        });
    };
};
/**
 * Get Collection Action (Top feed)
 *
 * @param {number} page ex: 1
 * @param {number} perPage ex: 12
 */
var getCollectionAction = function (_a) {
    var _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["g" /* GET_COLLECTION */],
            payload: { promise: getCollection({ page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: {}
        });
    };
};
/**
* Get Collection Detail Action (Top feed)
*
* @param {number} id ex: 1
*/
var getCollectionDetailAction = function (_a) {
    var id = _a.id;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["h" /* GET_COLLECTION_DETAIL */],
            payload: { promise: getCollectionDetail({ id: id }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0aXZpdHktZmVlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFjdGl2aXR5LWZlZWQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUVMLHFCQUFxQixFQUVyQiw0QkFBNEIsRUFFNUIsc0JBQXNCLEVBRXRCLG1CQUFtQixFQUVuQixzQkFBc0IsRUFDdEIsdUJBQXVCLEVBQ3ZCLGFBQWEsRUFDYixtQkFBbUIsRUFDcEIsTUFBTSxzQkFBc0IsQ0FBQztBQUU5QixPQUFPLEVBQ0wsd0JBQXdCLEVBQ3hCLGdDQUFnQyxFQUNoQyx5QkFBeUIsRUFDekIsc0JBQXNCLEVBQ3RCLHlCQUF5QixFQUN6QiwwQkFBMEIsRUFDMUIsY0FBYyxFQUNkLHFCQUFxQixHQUN0QixNQUFNLGdDQUFnQyxDQUFDO0FBR3hDOzs7OztHQUtHO0FBQ0gsTUFBTSxDQUFDLElBQU0sMkJBQTJCLEdBQ3RDLFVBQUMsRUFBZ0Y7UUFBOUUsYUFBVSxFQUFWLCtCQUFVLEVBQUUsaUJBQWEsRUFBYixrQ0FBYSxFQUFFLGNBQVUsRUFBViwrQkFBVSxFQUFFLGdCQUFhLEVBQWIsa0NBQWE7SUFDckQsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLHdCQUF3QjtZQUM5QixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUscUJBQXFCLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxTQUFTLFdBQUEsRUFBRSxNQUFNLFFBQUEsRUFBRSxRQUFRLFVBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQ3BHLElBQUksRUFBRSxFQUFFLFVBQVUsRUFBRSxFQUFFLEtBQUssT0FBQSxFQUFFLFNBQVMsV0FBQSxFQUFFLEVBQUU7U0FDM0MsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7Ozs7OztFQU9FO0FBQ0YsTUFBTSxDQUFDLElBQU0sa0NBQWtDLEdBQzdDLFVBQUMsRUFBbUU7UUFBakUsVUFBRSxFQUFFLGdDQUFhLEVBQUUsY0FBSSxFQUFFLG9CQUFPO0lBQ2pDLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxnQ0FBZ0M7WUFDdEMsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLDRCQUE0QixDQUFDLEVBQUUsRUFBRSxJQUFBLEVBQUUsYUFBYSxlQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN6RyxJQUFJLEVBQUUsRUFBRSxFQUFFLElBQUEsRUFBRTtTQUNiLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7OztFQU1FO0FBQ0YsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsRUFBNEQ7UUFBMUQsVUFBRSxFQUFFLG9CQUFPLEVBQUUsZ0NBQWE7SUFDM0IsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLHlCQUF5QjtZQUMvQixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsc0JBQXNCLENBQUMsRUFBRSxFQUFFLElBQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxhQUFhLGVBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQzdGLElBQUksRUFBRSxFQUFFLEVBQUUsSUFBQSxFQUFFO1NBQ2IsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0seUJBQXlCLEdBQ3BDLFVBQUMsRUFBaUM7UUFBL0IsVUFBRTtJQUNILE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxzQkFBc0I7WUFDNUIsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLG1CQUFtQixDQUFDLEVBQUUsRUFBRSxJQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUNsRSxJQUFJLEVBQUUsRUFBRSxFQUFFLElBQUEsRUFBRTtTQUNiLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7RUFJRTtBQUNGLE1BQU0sQ0FBQyxJQUFNLDRCQUE0QixHQUN2QyxVQUFDLEVBQW9DO1FBQWxDLFVBQUU7SUFDSCxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUseUJBQXlCO1lBQy9CLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxzQkFBc0IsQ0FBQyxFQUFFLEVBQUUsSUFBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDckUsSUFBSSxFQUFFLEVBQUUsRUFBRSxJQUFBLEVBQUU7U0FDYixDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUdUOzs7O0VBSUU7QUFDRixNQUFNLENBQUMsSUFBTSw2QkFBNkIsR0FDeEMsVUFBQyxFQUFVO1FBQVIsa0JBQU07SUFDUCxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsMEJBQTBCO1lBQ2hDLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSx1QkFBdUIsQ0FBQyxFQUFFLE1BQU0sUUFBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDMUUsSUFBSSxFQUFFLEVBQUUsTUFBTSxRQUFBLEVBQUU7U0FDakIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFHVDs7Ozs7R0FLRztBQUVILE1BQU0sQ0FBQyxJQUFNLG1CQUFtQixHQUM5QixVQUFDLEVBQTBCO1FBQXhCLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUFPLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqRCxPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxjQUFjO1lBQ3BCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxhQUFhLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQ3ZFLElBQUksRUFBRSxFQUFFO1NBQ1QsQ0FBQztJQUpGLENBSUU7QUFMNEIsQ0FLNUIsQ0FBQztBQUVQOzs7O0VBSUU7QUFFRixNQUFNLENBQUMsSUFBTSx5QkFBeUIsR0FDcEMsVUFBQyxFQUFNO1FBQUosVUFBRTtJQUFPLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUM3QixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxxQkFBcUI7WUFDM0IsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLG1CQUFtQixDQUFDLEVBQUUsRUFBRSxJQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUNsRSxJQUFJLEVBQUUsRUFBRSxFQUFFLElBQUEsRUFBRTtTQUNiLENBQUM7SUFKRixDQUlFO0FBTFEsQ0FLUixDQUFDIn0=

/***/ }),

/***/ 762:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// CONCATENATED MODULE: ./api/magazine.ts

;
var fetchMagazineList = function (_a) {
    var _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c, _d = _a.type, type = _d === void 0 ? 'default' : _d;
    var query = "?page=" + page + "&per_page=" + perPage + "&filter[post_type]=" + type;
    return Object(restful_method["b" /* get */])({
        path: "/magazines" + query,
        description: 'Get magazine list',
        errorMesssage: "Can't get magazine list. Please try again",
    });
};
var fetchMagazineDashboard = function () { return Object(restful_method["b" /* get */])({
    path: "/magazines/dashboard",
    description: 'Get magazine dashboard list',
    errorMesssage: "Can't get magazine dashboard list. Please try again",
}); };
/**
 * Fetch magazine category by slug (id)
 */
var fetchMagazineCategory = function (_a) {
    var slug = _a.slug, page = _a.page, perPage = _a.perPage;
    var query = slug + "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/category/" + query,
        description: 'Get magazine category by slug (id)',
        errorMesssage: "Can't get magazine category by slug (id). Please try again",
    });
};
/**
* Fetch magazine by slug (id)
*/
var fetchMagazineBySlug = function (_a) {
    var slug = _a.slug;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/" + slug,
        description: 'Get magazine by slug (id)',
        errorMesssage: "Can't get magazine by slug (id). Please try again",
    });
};
/**
* Fetch magazine related blogs by slug (id)
*/
var fetchMagazineRelatedBlog = function (_a) {
    var slug = _a.slug;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/" + slug + "/related_blogs",
        description: 'Get magazine related blogs by slug (id)',
        errorMesssage: "Can't get magazine related blogs by slug (id). Please try again",
    });
};
/**
* Fetch magazine related boxes by slug (id)
*/
var fetchMagazineRelatedBox = function (_a) {
    var slug = _a.slug, limit = _a.limit;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/" + slug + "/related_boxes?limit=" + limit,
        description: 'Get magazine related boxes by slug (id)',
        errorMesssage: "Can't get magazine related boxes by slug (id). Please try again",
    });
};
/**
* Fetch magazine by tag name
*/
var fetchMagazineByTagName = function (_a) {
    var slug = _a.slug, page = _a.page, perPage = _a.perPage;
    var query = slug + "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/tag/" + query,
        description: 'Get magazine by tag name',
        errorMesssage: "Can't get magazine by tag name. Please try again",
    });
};
/**
* Fetch search magazine by keyword
*/
var fetchMagazineByKeyword = function (_a) {
    var keyword = _a.keyword, page = _a.page, perPage = _a.perPage;
    var query = keyword + "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/search/" + query,
        description: 'Get search magazine by keyword',
        errorMesssage: "Can't get search magazine by keyword. Please try again",
    });
};
/**
* Fetch search suggestion magazine by keyword
*/
var fetchMagazineSuggestionByKeyword = function (_a) {
    var keyword = _a.keyword, limit = _a.limit;
    var query = keyword + "?limit=" + limit;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/search_suggestion/" + query,
        description: 'Get search suggestion magazine by keyword',
        errorMesssage: "Can't get search suggestion magazine by keyword. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFnYXppbmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtYWdhemluZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFNOUMsQ0FBQztBQUVGLE1BQU0sQ0FBQyxJQUFNLGlCQUFpQixHQUM1QixVQUFDLEVBQXFFO1FBQW5FLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWSxFQUFFLFlBQWdCLEVBQWhCLHFDQUFnQjtJQUN6QyxJQUFNLEtBQUssR0FBRyxXQUFTLElBQUksa0JBQWEsT0FBTywyQkFBc0IsSUFBTSxDQUFDO0lBRTVFLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsZUFBYSxLQUFPO1FBQzFCLFdBQVcsRUFBRSxtQkFBbUI7UUFDaEMsYUFBYSxFQUFFLDJDQUEyQztLQUMzRCxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSixNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FBRyxjQUFNLE9BQUEsR0FBRyxDQUFDO0lBQzlDLElBQUksRUFBRSxzQkFBc0I7SUFDNUIsV0FBVyxFQUFFLDZCQUE2QjtJQUMxQyxhQUFhLEVBQUUscURBQXFEO0NBQ3JFLENBQUMsRUFKMEMsQ0FJMUMsQ0FBQztBQUVIOztHQUVHO0FBQ0gsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQ2hDLFVBQUMsRUFBdUI7UUFBckIsY0FBSSxFQUFFLGNBQUksRUFBRSxvQkFBTztJQUNwQixJQUFNLEtBQUssR0FBTSxJQUFJLGNBQVMsSUFBSSxrQkFBYSxPQUFTLENBQUM7SUFDekQsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSx5QkFBdUIsS0FBTztRQUNwQyxXQUFXLEVBQUUsb0NBQW9DO1FBQ2pELGFBQWEsRUFBRSw0REFBNEQ7S0FDNUUsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUo7O0VBRUU7QUFDRixNQUFNLENBQUMsSUFBTSxtQkFBbUIsR0FBRyxVQUFDLEVBQVE7UUFBTixjQUFJO0lBQU8sT0FBQSxHQUFHLENBQUM7UUFDbkQsSUFBSSxFQUFFLGdCQUFjLElBQU07UUFDMUIsV0FBVyxFQUFFLDJCQUEyQjtRQUN4QyxhQUFhLEVBQUUsbURBQW1EO0tBQ25FLENBQUM7QUFKK0MsQ0FJL0MsQ0FBQztBQUVIOztFQUVFO0FBQ0YsTUFBTSxDQUFDLElBQU0sd0JBQXdCLEdBQUcsVUFBQyxFQUFRO1FBQU4sY0FBSTtJQUFPLE9BQUEsR0FBRyxDQUFDO1FBQ3hELElBQUksRUFBRSxnQkFBYyxJQUFJLG1CQUFnQjtRQUN4QyxXQUFXLEVBQUUseUNBQXlDO1FBQ3RELGFBQWEsRUFBRSxpRUFBaUU7S0FDakYsQ0FBQztBQUpvRCxDQUlwRCxDQUFDO0FBRUg7O0VBRUU7QUFDRixNQUFNLENBQUMsSUFBTSx1QkFBdUIsR0FBRyxVQUFDLEVBQWU7UUFBYixjQUFJLEVBQUUsZ0JBQUs7SUFBTyxPQUFBLEdBQUcsQ0FBQztRQUM5RCxJQUFJLEVBQUUsZ0JBQWMsSUFBSSw2QkFBd0IsS0FBTztRQUN2RCxXQUFXLEVBQUUseUNBQXlDO1FBQ3RELGFBQWEsRUFBRSxpRUFBaUU7S0FDakYsQ0FBQztBQUowRCxDQUkxRCxDQUFDO0FBRUg7O0VBRUU7QUFDRixNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FDakMsVUFBQyxFQUF1QjtRQUFyQixjQUFJLEVBQUUsY0FBSSxFQUFFLG9CQUFPO0lBQ3BCLElBQU0sS0FBSyxHQUFNLElBQUksY0FBUyxJQUFJLGtCQUFhLE9BQVMsQ0FBQztJQUN6RCxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLG9CQUFrQixLQUFPO1FBQy9CLFdBQVcsRUFBRSwwQkFBMEI7UUFDdkMsYUFBYSxFQUFFLGtEQUFrRDtLQUNsRSxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSjs7RUFFRTtBQUNGLE1BQU0sQ0FBQyxJQUFNLHNCQUFzQixHQUNqQyxVQUFDLEVBQTBCO1FBQXhCLG9CQUFPLEVBQUUsY0FBSSxFQUFFLG9CQUFPO0lBQ3ZCLElBQU0sS0FBSyxHQUFNLE9BQU8sY0FBUyxJQUFJLGtCQUFhLE9BQVMsQ0FBQztJQUM1RCxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLHVCQUFxQixLQUFPO1FBQ2xDLFdBQVcsRUFBRSxnQ0FBZ0M7UUFDN0MsYUFBYSxFQUFFLHdEQUF3RDtLQUN4RSxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSjs7RUFFRTtBQUNGLE1BQU0sQ0FBQyxJQUFNLGdDQUFnQyxHQUMzQyxVQUFDLEVBQWtCO1FBQWhCLG9CQUFPLEVBQUUsZ0JBQUs7SUFDZixJQUFNLEtBQUssR0FBTSxPQUFPLGVBQVUsS0FBTyxDQUFDO0lBQzFDLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsa0NBQWdDLEtBQU87UUFDN0MsV0FBVyxFQUFFLDJDQUEyQztRQUN4RCxhQUFhLEVBQUUsbUVBQW1FO0tBQ25GLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQyJ9
// EXTERNAL MODULE: ./constants/api/magazine.ts
var magazine = __webpack_require__(22);

// CONCATENATED MODULE: ./action/magazine.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return fetchMagazineListAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return fetchMagazineDashboardAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return fetchMagazineCategoryAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fetchMagazineBySlugAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return fetchMagazineRelatedBlogAction; });
/* unused harmony export fetchMagazineRelatedBoxAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fetchMagazineByTagNameAction; });
/* unused harmony export showHideMagazineSearchAction */
/* unused harmony export fetchMagazineByKeywordAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return fetchMagazineSuggestionByKeywordAction; });


/**
 * Fetch love list by filter paramsx
 *
 * @param {number} page ex: 1, 2, 3, 4
 * @param {number} perPage ex: 10, 15, 20
 * @param {'default' | 'video-post' | 'quote-post'} type
 */
var fetchMagazineListAction = function (_a) {
    var page = _a.page, perPage = _a.perPage, type = _a.type;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["f" /* FETCH_MAGAZINE_LIST */],
            payload: { promise: fetchMagazineList({ page: page, perPage: perPage, type: type }).then(function (res) { return res; }) },
            meta: { page: page, perPage: perPage, type: type }
        });
    };
};
var fetchMagazineDashboardAction = function () {
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["e" /* FETCH_MAGAZINE_DASHBOARD */],
            payload: { promise: fetchMagazineDashboard().then(function (res) { return res; }) },
            meta: {}
        });
    };
};
/**
* Fetch magazine category by slug (id)
*
* @param {string} slug ex: makeup
* @param {number} page ex: 1, 2, 3, 4
* @param {number} perPage ex: 10, 15, 20
*/
var fetchMagazineCategoryAction = function (_a) {
    var slug = _a.slug, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["d" /* FETCH_MAGAZINE_CATEGORY */],
            payload: { promise: fetchMagazineCategory({ slug: slug, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { slug: slug, page: page, perPage: perPage }
        });
    };
};
/**
* Fetch magazine by slug (id)
*
* @param {string} slug ex: makeup
*/
var fetchMagazineBySlugAction = function (_a) {
    var slug = _a.slug;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["c" /* FETCH_MAGAZINE_BY_SLUG */],
            payload: { promise: fetchMagazineBySlug({ slug: slug }).then(function (res) { return res; }) },
            meta: { slug: slug }
        });
    };
};
/**
* Fetch magazine related blogs by slug (id)
*
* @param {string} slug ex: makeup
*/
var fetchMagazineRelatedBlogAction = function (_a) {
    var slug = _a.slug;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["g" /* FETCH_MAGAZINE_RELATED_BLOGS */],
            payload: { promise: fetchMagazineRelatedBlog({ slug: slug }).then(function (res) { return res; }) },
            meta: { slug: slug }
        });
    };
};
/**
* Fetch magazine related boxes by slug (id)
*
* @param {string} slug ex: makeup
* @param {number} limit ex: 1, 2, 3, 4
*/
var fetchMagazineRelatedBoxAction = function (_a) {
    var slug = _a.slug, _b = _a.limit, limit = _b === void 0 ? 6 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["h" /* FETCH_MAGAZINE_RELATED_BOXES */],
            payload: { promise: fetchMagazineRelatedBox({ slug: slug, limit: limit }).then(function (res) { return res; }) },
            meta: { slug: slug }
        });
    };
};
/**
* Fetch magazine by tag name
*
* @param {string} tagName ex: halio
* @param {number} page ex: 1, 2, 3, 4
* @param {number} perPage ex: 10, 15, 20
*/
var fetchMagazineByTagNameAction = function (_a) {
    var slug = _a.slug, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["j" /* FETCH_MAGAZINE_TAG */],
            payload: { promise: fetchMagazineByTagName({ slug: slug, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { slug: slug, page: page, perPage: perPage }
        });
    };
};
/**
* Show / Hide search suggest with state param
*
* @param {boolean} state
*/
var showHideMagazineSearchAction = function (state) {
    if (state === void 0) { state = false; }
    return ({
        type: magazine["a" /* DISPLAY_MAGAZINE_SEARCH_SUGGESTION */],
        payload: state
    });
};
/**
* Fetch magazine by keyword
*
* @param {string} keyword ex: halio
* @param {number} page ex: 1, 2, 3, 4
* @param {number} perPage ex: 10, 15, 20
*/
var fetchMagazineByKeywordAction = function (_a) {
    var keyword = _a.keyword, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["b" /* FETCH_MAGAZINE_BY_KEWORD */],
            payload: { promise: fetchMagazineByKeyword({ keyword: keyword, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { keyword: keyword }
        });
    };
};
/**
* Fetch magazine suggestion by keyword
*
* @param {string} keyword ex: halio
* @param {number} limit ex: 1, 2, 3, 4
*/
var fetchMagazineSuggestionByKeywordAction = function (_a) {
    var keyword = _a.keyword, _b = _a.limit, limit = _b === void 0 ? 10 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["i" /* FETCH_MAGAZINE_SUGGESTION_BY_KEWORD */],
            payload: { promise: fetchMagazineSuggestionByKeyword({ keyword: keyword, limit: limit }).then(function (res) { return res; }) },
            meta: { keyword: keyword }
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFnYXppbmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtYWdhemluZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBRUwsaUJBQWlCLEVBQ2pCLHNCQUFzQixFQUN0QixxQkFBcUIsRUFDckIsbUJBQW1CLEVBQ25CLHdCQUF3QixFQUN4Qix1QkFBdUIsRUFDdkIsc0JBQXNCLEVBQ3RCLHNCQUFzQixFQUN0QixnQ0FBZ0MsRUFDakMsTUFBTSxpQkFBaUIsQ0FBQztBQUV6QixPQUFPLEVBQ0wsbUJBQW1CLEVBQ25CLHdCQUF3QixFQUN4Qix1QkFBdUIsRUFDdkIsc0JBQXNCLEVBQ3RCLDRCQUE0QixFQUM1Qiw0QkFBNEIsRUFDNUIsa0JBQWtCLEVBQ2xCLGtDQUFrQyxFQUNsQyx3QkFBd0IsRUFDeEIsbUNBQW1DLEVBQ3BDLE1BQU0sMkJBQTJCLENBQUM7QUFFbkM7Ozs7OztHQU1HO0FBQ0gsTUFBTSxDQUFDLElBQU0sdUJBQXVCLEdBQ2xDLFVBQUMsRUFBZ0Q7UUFBOUMsY0FBSSxFQUFFLG9CQUFPLEVBQUUsY0FBSTtJQUNwQixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsbUJBQW1CO1lBQ3pCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDakYsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUU7U0FDOUIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVCxNQUFNLENBQUMsSUFBTSw0QkFBNEIsR0FDdkM7SUFDRSxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsd0JBQXdCO1lBQzlCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUMvRCxJQUFJLEVBQUUsRUFBRTtTQUNULENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7OztFQU1FO0FBQ0YsTUFBTSxDQUFDLElBQU0sMkJBQTJCLEdBQ3RDLFVBQUMsRUFBZ0M7UUFBOUIsY0FBSSxFQUFFLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUM3QixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsdUJBQXVCO1lBQzdCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxxQkFBcUIsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDckYsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDOUIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0seUJBQXlCLEdBQ3BDLFVBQUMsRUFBUTtRQUFOLGNBQUk7SUFDTCxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsc0JBQXNCO1lBQzVCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDcEUsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUU7U0FDZixDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUdUOzs7O0VBSUU7QUFDRixNQUFNLENBQUMsSUFBTSw4QkFBOEIsR0FDekMsVUFBQyxFQUFRO1FBQU4sY0FBSTtJQUNMLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSw0QkFBNEI7WUFDbEMsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLHdCQUF3QixDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN6RSxJQUFJLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRTtTQUNmLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7O0VBS0U7QUFDRixNQUFNLENBQUMsSUFBTSw2QkFBNkIsR0FDeEMsVUFBQyxFQUFtQjtRQUFqQixjQUFJLEVBQUUsYUFBUyxFQUFULDhCQUFTO0lBQ2hCLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSw0QkFBNEI7WUFDbEMsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLHVCQUF1QixDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUMvRSxJQUFJLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRTtTQUNmLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBR1Q7Ozs7OztFQU1FO0FBQ0YsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsRUFBZ0M7UUFBOUIsY0FBSSxFQUFFLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUM3QixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsa0JBQWtCO1lBQ3hCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxzQkFBc0IsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDdEYsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDOUIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsS0FBYTtJQUFiLHNCQUFBLEVBQUEsYUFBYTtJQUFLLE9BQUEsQ0FBQztRQUNsQixJQUFJLEVBQUUsa0NBQWtDO1FBQ3hDLE9BQU8sRUFBRSxLQUFLO0tBQ2YsQ0FBQztBQUhpQixDQUdqQixDQUFDO0FBRUw7Ozs7OztFQU1FO0FBQ0YsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsRUFBbUM7UUFBakMsb0JBQU8sRUFBRSxZQUFRLEVBQVIsNkJBQVEsRUFBRSxlQUFZLEVBQVosaUNBQVk7SUFDaEMsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLHdCQUF3QjtZQUM5QixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsc0JBQXNCLENBQUMsRUFBRSxPQUFPLFNBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQ3pGLElBQUksRUFBRSxFQUFFLE9BQU8sU0FBQSxFQUFFO1NBQ2xCLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7O0VBS0U7QUFDRixNQUFNLENBQUMsSUFBTSxzQ0FBc0MsR0FDakQsVUFBQyxFQUF1QjtRQUFyQixvQkFBTyxFQUFFLGFBQVUsRUFBViwrQkFBVTtJQUNwQixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsbUNBQW1DO1lBQ3pDLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxnQ0FBZ0MsQ0FBQyxFQUFFLE9BQU8sU0FBQSxFQUFFLEtBQUssT0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDM0YsSUFBSSxFQUFFLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDbEIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUMifQ==

/***/ }),

/***/ 764:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/image.ts
var utils_image = __webpack_require__(348);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./components/magazine/image-slider/initialize.tsx
var DEFAULT_PROPS = {
    data: [],
    type: 'full',
    title: '',
    column: 3,
    showViewMore: false,
    showHeader: true,
    isCustomTitle: false,
    style: {},
    titleStyle: {}
};
var INITIAL_STATE = function (data) { return ({
    magazineList: data || [],
    magazineSlide: [],
    magazineSlideSelected: {},
    countChangeSlide: 0,
    firstInit: false
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLElBQUksRUFBRSxNQUFNO0lBQ1osS0FBSyxFQUFFLEVBQUU7SUFDVCxNQUFNLEVBQUUsQ0FBQztJQUNULFlBQVksRUFBRSxLQUFLO0lBQ25CLFVBQVUsRUFBRSxJQUFJO0lBQ2hCLGFBQWEsRUFBRSxLQUFLO0lBQ3BCLEtBQUssRUFBRSxFQUFFO0lBQ1QsVUFBVSxFQUFFLEVBQUU7Q0FDTCxDQUFDO0FBRVosTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHLFVBQUMsSUFBUyxJQUFLLE9BQUEsQ0FBQztJQUMzQyxZQUFZLEVBQUUsSUFBSSxJQUFJLEVBQUU7SUFDeEIsYUFBYSxFQUFFLEVBQUU7SUFDakIscUJBQXFCLEVBQUUsRUFBRTtJQUN6QixnQkFBZ0IsRUFBRSxDQUFDO0lBQ25CLFNBQVMsRUFBRSxLQUFLO0NBQ04sQ0FBQSxFQU5nQyxDQU1oQyxDQUFDIn0=
// EXTERNAL MODULE: ./container/layout/main-block/index.tsx
var main_block = __webpack_require__(746);

// EXTERNAL MODULE: ./components/ui/loading-placeholder/index.tsx + 4 modules
var loading_placeholder = __webpack_require__(750);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// EXTERNAL MODULE: ./style/component.ts
var component = __webpack_require__(744);

// CONCATENATED MODULE: ./components/magazine/image-slider-item/initialize.tsx
var initialize_DEFAULT_PROPS = {
    item: [],
    type: '',
    column: 4
};
var initialize_INITIAL_STATE = {
    isLoadedImage: false
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLElBQUksRUFBRSxFQUFFO0lBQ1IsTUFBTSxFQUFFLENBQUM7Q0FDQSxDQUFDO0FBRVosTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHO0lBQzNCLGFBQWEsRUFBRSxLQUFLO0NBQ1gsQ0FBQyJ9
// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ../node_modules/react-on-screen/lib/index.js
var lib = __webpack_require__(767);
var lib_default = /*#__PURE__*/__webpack_require__.n(lib);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/magazine/image-slider-item/style.tsx

var generateSwitchStyle = function (mobile, desktop) {
    var switchStyle = {
        MOBILE: { paddingRight: 10, width: mobile, minWidth: mobile },
        DESKTOP: { width: desktop }
    };
    return switchStyle[window.DEVICE_VERSION];
};
/* harmony default export */ var image_slider_item_style = ({
    mainWrap: {
        paddingLeft: 10,
        paddingRight: 10
    },
    heading: {
        paddingLeft: 10,
        display: variable["display"].inlineBlock,
        maxWidth: "100%",
        whiteSpace: "nowrap",
        overflow: "hidden",
        textOverflow: "ellipsis",
        fontFamily: variable["fontAvenirMedium"],
        color: variable["colorBlack"],
        fontSize: 20,
        lineHeight: "40px",
        height: 40,
        letterSpacing: -0.5,
        textTransform: "uppercase",
    },
    container: {
        width: '100%',
        overflowX: 'auto',
        whiteSpace: 'nowrap',
        paddingTop: 0,
        marginBottom: 0,
        display: variable["display"].flex,
        itemSlider: {
            width: "100%",
            height: "100%",
            display: variable["display"].inlineBlock,
            borderRadius: 5,
            overflow: 'hidden',
            boxShadow: variable["shadowBlur"],
            position: variable["position"].relative
        },
        itemSliderPanel: function (imgUrl) { return ({
            width: '100%',
            paddingTop: '62.5%',
            position: variable["position"].relative,
            backgroundImage: "url(" + imgUrl + ")",
            backgroundColor: variable["colorF7"],
            backgroundPosition: 'top center',
            transition: variable["transitionOpacity"],
            backgroundSize: 'cover',
        }); },
        videoIcon: {
            width: 70,
            height: 70,
            position: variable["position"].absolute,
            top: '50%',
            left: '55%',
            transform: 'translate(-50%, -50%)',
            borderTop: '35px solid transparent',
            boxSizing: 'border-box',
            borderLeft: '51px solid white',
            borderBottom: '35px solid transparent',
            opacity: .8
        },
        info: {
            width: '100%',
            height: 110,
            padding: 12,
            position: variable["position"].relative,
            background: variable["colorWhite"],
            display: variable["display"].flex,
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
            image: function (imgUrl) { return ({
                width: '100%',
                height: '100%',
                top: 0,
                left: 0,
                zIndex: -1,
                position: variable["position"].absolute,
                backgroundColor: variable["colorF7"],
                backgroundImage: "url(" + imgUrl + ")",
                backgroundPosition: 'bottom center',
                backgroundSize: 'cover',
                filter: 'blur(4px)',
                transform: "scaleY(-1) scale(1.1)",
                'WebkitBackfaceVisibility': 'hidden',
                'WebkitPerspective': 1000,
                'WebkitTransform': ['translate3d(0,0,0)', 'translateZ(0)'],
                'backfaceVisibility': 'hidden',
                perspective: 1000,
            }); },
            title: {
                color: variable["colorBlack"],
                whiteSpace: 'pre-wrap',
                fontFamily: variable["fontAvenirMedium"],
                fontSize: 14,
                lineHeight: '22px',
                maxHeight: '44px',
                overflow: 'hidden',
                marginBottom: 5
            },
            description: {
                fontSize: 12,
                color: variable["colorBlack"],
                fontFamily: variable["fontAvenirMedium"],
                opacity: .7,
                marginRight: 10,
                whiteSpace: 'pre-wrap',
                lineHeight: '18px',
                maxHeight: 36,
                overflow: 'hidden',
            },
            tagList: {
                width: '100%',
                display: variable["display"].flex,
                flexWrap: 'wrap',
                height: '36px',
                maxHeight: '36px',
                overflow: 'hidden'
            },
            tagItem: {
                fontSize: 12,
                lineHeight: '18px',
                color: variable["colorBlack"],
                fontFamily: variable["fontAvenirMedium"],
                opacity: .7,
                marginRight: 10,
                whiteSpace: 'pre-wrap'
            },
        }
    },
    column: {
        1: { width: '100%' },
        2: { width: '50%' },
        3: generateSwitchStyle('75%', '33.33%'),
        4: generateSwitchStyle('47%', '25%'),
        5: generateSwitchStyle('47%', '20%'),
        6: generateSwitchStyle('50%', '16.66%'),
    },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxJQUFNLG1CQUFtQixHQUFHLFVBQUMsTUFBTSxFQUFFLE9BQU87SUFDMUMsSUFBTSxXQUFXLEdBQUc7UUFDbEIsTUFBTSxFQUFFLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUU7UUFDN0QsT0FBTyxFQUFFLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRTtLQUM1QixDQUFDO0lBRUYsTUFBTSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUM7QUFDNUMsQ0FBQyxDQUFDO0FBRUYsZUFBZTtJQUNiLFFBQVEsRUFBRTtRQUNSLFdBQVcsRUFBRSxFQUFFO1FBQ2YsWUFBWSxFQUFFLEVBQUU7S0FDakI7SUFFRCxPQUFPLEVBQUU7UUFDUCxXQUFXLEVBQUUsRUFBRTtRQUNmLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLFdBQVc7UUFDckMsUUFBUSxFQUFFLE1BQU07UUFDaEIsVUFBVSxFQUFFLFFBQVE7UUFDcEIsUUFBUSxFQUFFLFFBQVE7UUFDbEIsWUFBWSxFQUFFLFVBQVU7UUFDeEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7UUFDckMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1FBQzFCLFFBQVEsRUFBRSxFQUFFO1FBQ1osVUFBVSxFQUFFLE1BQU07UUFDbEIsTUFBTSxFQUFFLEVBQUU7UUFDVixhQUFhLEVBQUUsQ0FBQyxHQUFHO1FBQ25CLGFBQWEsRUFBRSxXQUFXO0tBQzNCO0lBRUQsU0FBUyxFQUFFO1FBQ1QsS0FBSyxFQUFFLE1BQU07UUFDYixTQUFTLEVBQUUsTUFBTTtRQUNqQixVQUFVLEVBQUUsUUFBUTtRQUNwQixVQUFVLEVBQUUsQ0FBQztRQUNiLFlBQVksRUFBRSxDQUFDO1FBQ2YsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUU5QixVQUFVLEVBQUU7WUFDVixLQUFLLEVBQUUsTUFBTTtZQUNiLE1BQU0sRUFBRSxNQUFNO1lBQ2QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztZQUNyQyxZQUFZLEVBQUUsQ0FBQztZQUNmLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTtZQUM5QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1NBQ3JDO1FBRUQsZUFBZSxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztZQUM1QixLQUFLLEVBQUUsTUFBTTtZQUNiLFVBQVUsRUFBRSxPQUFPO1lBQ25CLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDcEMsZUFBZSxFQUFFLFNBQU8sTUFBTSxNQUFHO1lBQ2pDLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztZQUNqQyxrQkFBa0IsRUFBRSxZQUFZO1lBQ2hDLFVBQVUsRUFBRSxRQUFRLENBQUMsaUJBQWlCO1lBQ3RDLGNBQWMsRUFBRSxPQUFPO1NBQ3hCLENBQUMsRUFUMkIsQ0FTM0I7UUFFRixTQUFTLEVBQUU7WUFDVCxLQUFLLEVBQUUsRUFBRTtZQUNULE1BQU0sRUFBRSxFQUFFO1lBQ1YsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxHQUFHLEVBQUUsS0FBSztZQUNWLElBQUksRUFBRSxLQUFLO1lBQ1gsU0FBUyxFQUFFLHVCQUF1QjtZQUNsQyxTQUFTLEVBQUUsd0JBQXdCO1lBQ25DLFNBQVMsRUFBRSxZQUFZO1lBQ3ZCLFVBQVUsRUFBRSxrQkFBa0I7WUFDOUIsWUFBWSxFQUFFLHdCQUF3QjtZQUN0QyxPQUFPLEVBQUUsRUFBRTtTQUNaO1FBRUQsSUFBSSxFQUFFO1lBQ0osS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsR0FBRztZQUNYLE9BQU8sRUFBRSxFQUFFO1lBQ1gsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDL0IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixhQUFhLEVBQUUsUUFBUTtZQUN2QixjQUFjLEVBQUUsWUFBWTtZQUM1QixVQUFVLEVBQUUsWUFBWTtZQUV4QixLQUFLLEVBQUUsVUFBQyxNQUFNLElBQUssT0FBQSxDQUFDO2dCQUNsQixLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsTUFBTTtnQkFDZCxHQUFHLEVBQUUsQ0FBQztnQkFDTixJQUFJLEVBQUUsQ0FBQztnQkFDUCxNQUFNLEVBQUUsQ0FBQyxDQUFDO2dCQUNWLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztnQkFDakMsZUFBZSxFQUFFLFNBQU8sTUFBTSxNQUFHO2dCQUNqQyxrQkFBa0IsRUFBRSxlQUFlO2dCQUNuQyxjQUFjLEVBQUUsT0FBTztnQkFDdkIsTUFBTSxFQUFFLFdBQVc7Z0JBQ25CLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLDBCQUEwQixFQUFFLFFBQVE7Z0JBQ3BDLG1CQUFtQixFQUFFLElBQUk7Z0JBQ3pCLGlCQUFpQixFQUFFLENBQUMsb0JBQW9CLEVBQUUsZUFBZSxDQUFDO2dCQUMxRCxvQkFBb0IsRUFBRSxRQUFRO2dCQUM5QixXQUFXLEVBQUUsSUFBSTthQUNsQixDQUFDLEVBbEJpQixDQWtCakI7WUFFRixLQUFLLEVBQUU7Z0JBQ0wsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMxQixVQUFVLEVBQUUsVUFBVTtnQkFDdEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxNQUFNO2dCQUNsQixTQUFTLEVBQUUsTUFBTTtnQkFDakIsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFlBQVksRUFBRSxDQUFDO2FBQ2hCO1lBRUQsV0FBVyxFQUFFO2dCQUNYLFFBQVEsRUFBRSxFQUFFO2dCQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLE9BQU8sRUFBRSxFQUFFO2dCQUNYLFdBQVcsRUFBRSxFQUFFO2dCQUNmLFVBQVUsRUFBRSxVQUFVO2dCQUN0QixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsU0FBUyxFQUFFLEVBQUU7Z0JBQ2IsUUFBUSxFQUFFLFFBQVE7YUFDbkI7WUFFRCxPQUFPLEVBQUU7Z0JBQ1AsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsUUFBUSxFQUFFLE1BQU07Z0JBQ2hCLE1BQU0sRUFBRSxNQUFNO2dCQUNkLFNBQVMsRUFBRSxNQUFNO2dCQUNqQixRQUFRLEVBQUUsUUFBUTthQUNuQjtZQUVELE9BQU8sRUFBRTtnQkFDUCxRQUFRLEVBQUUsRUFBRTtnQkFDWixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtnQkFDckMsT0FBTyxFQUFFLEVBQUU7Z0JBQ1gsV0FBVyxFQUFFLEVBQUU7Z0JBQ2YsVUFBVSxFQUFFLFVBQVU7YUFDdkI7U0FDRjtLQUNGO0lBRUQsTUFBTSxFQUFFO1FBQ04sQ0FBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRTtRQUNwQixDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFO1FBQ25CLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDO1FBQ3ZDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDO1FBQ3BDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDO1FBQ3BDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDO0tBQ3hDO0NBQ00sQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider-item/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};





function renderComponent(_a) {
    var state = _a.state, props = _a.props, handleLoadImage = _a.handleLoadImage;
    var _b = props, item = _b.item, type = _b.type, column = _b.column;
    var isLoadedImage = state.isLoadedImage;
    var linkProps = {
        to: routing["Ma" /* ROUTING_MAGAZINE_DETAIL_PATH */] + "/" + (item && item.slug || ''),
        style: image_slider_item_style.container.itemSlider
    };
    return (react["createElement"](lib_default.a, { style: Object.assign({}, image_slider_item_style.column[column || 4], image_slider_item_style.mainWrap), offset: 200 }, function (_a) {
        var isVisible = _a.isVisible;
        if (!!isVisible) {
            handleLoadImage();
        }
        return (react["createElement"](react_router_dom["NavLink"], __assign({}, linkProps),
            react["createElement"]("div", { style: [
                    image_slider_item_style.container.itemSliderPanel(isLoadedImage && item ? item.cover_image && item.cover_image.medium_url : ''),
                    { opacity: isLoadedImage ? 1 : 0 }
                ] }, 'video' === type && (react["createElement"]("div", { style: image_slider_item_style.container.videoIcon }))),
            react["createElement"]("div", { style: image_slider_item_style.container.info },
                react["createElement"]("div", { style: image_slider_item_style.container.info.title }, item && item.title || ''),
                react["createElement"]("div", { style: image_slider_item_style.container.info.description }, item && item.description || ''))));
    }));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQ3RGLE9BQU8sZUFBZSxNQUFNLGlCQUFpQixDQUFDO0FBRTlDLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixNQUFNLDBCQUEwQixFQUFpQztRQUEvQixnQkFBSyxFQUFFLGdCQUFLLEVBQUUsb0NBQWU7SUFDdkQsSUFBQSxVQUF3QyxFQUF0QyxjQUFJLEVBQUUsY0FBSSxFQUFFLGtCQUFNLENBQXFCO0lBQ3ZDLElBQUEsbUNBQWEsQ0FBVztJQUVoQyxJQUFNLFNBQVMsR0FBRztRQUNoQixFQUFFLEVBQUssNEJBQTRCLFVBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFFO1FBQ2hFLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLFVBQVU7S0FDbEMsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLG9CQUFDLGVBQWUsSUFBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxFQUFFLE1BQU0sRUFBRSxHQUFHLElBRTVGLFVBQUMsRUFBYTtZQUFYLHdCQUFTO1FBQ1YsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFBQyxlQUFlLEVBQUUsQ0FBQTtRQUFBLENBQUM7UUFFckMsTUFBTSxDQUFDLENBQ0wsb0JBQUMsT0FBTyxlQUFLLFNBQVM7WUFDcEIsNkJBQUssS0FBSyxFQUFFO29CQUNSLEtBQUssQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFFLGFBQWEsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDOUcsRUFBRSxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtpQkFDbkMsSUFFQyxPQUFPLEtBQUssSUFBSSxJQUFJLENBQ2xCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBUSxDQUM5QyxDQUVDO1lBQ04sNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSTtnQkFDOUIsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBRyxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQU87Z0JBQ3hFLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLElBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksRUFBRSxDQUFPLENBQ2hGLENBQ0UsQ0FDWCxDQUFBO0lBQ0gsQ0FBQyxDQUVhLENBQ25CLENBQUM7QUFDSixDQUFDO0FBQUEsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider-item/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_SlideItem = /** @class */ (function (_super) {
    __extends(SlideItem, _super);
    function SlideItem(props) {
        var _this = _super.call(this, props) || this;
        _this.state = initialize_INITIAL_STATE;
        return _this;
    }
    // shouldComponentUpdate(nextProps: IProps, nextState: IState) {
    //   // if (true === isEmptyObject(this.props.data) && false === isEmptyObject(nextProps.data)) { return true; }
    //   // if (this.props.listLikedId.length !== nextProps.listLikedId.length) { return true; }
    //   return true;
    // }
    SlideItem.prototype.handleLoadImage = function () {
        var isLoadedImage = this.state.isLoadedImage;
        if (!!isLoadedImage) {
            return;
        }
        this.setState({ isLoadedImage: true });
    };
    SlideItem.prototype.render = function () {
        var renderViewProps = {
            props: this.props,
            state: this.state,
            handleLoadImage: this.handleLoadImage.bind(this)
        };
        return renderComponent(renderViewProps);
    };
    SlideItem.defaultProps = initialize_DEFAULT_PROPS;
    SlideItem = __decorate([
        radium
    ], SlideItem);
    return SlideItem;
}(react["Component"]));
;
/* harmony default export */ var image_slider_item_component = (component_SlideItem);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDNUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUl6QztJQUF3Qiw2QkFBK0I7SUFHckQsbUJBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUM7O0lBQzdCLENBQUM7SUFFRCxnRUFBZ0U7SUFDaEUsZ0hBQWdIO0lBQ2hILDRGQUE0RjtJQUU1RixpQkFBaUI7SUFDakIsSUFBSTtJQUVKLG1DQUFlLEdBQWY7UUFDVSxJQUFBLHdDQUFhLENBQWdCO1FBQ3JDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sQ0FBQztRQUNULENBQUM7UUFFRCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUMsYUFBYSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELDBCQUFNLEdBQU47UUFDRSxJQUFNLGVBQWUsR0FBRztZQUN0QixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLGVBQWUsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDakQsQ0FBQztRQUVGLE1BQU0sQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQS9CTSxzQkFBWSxHQUFXLGFBQWEsQ0FBQztJQUR4QyxTQUFTO1FBRGQsTUFBTTtPQUNELFNBQVMsQ0FpQ2Q7SUFBRCxnQkFBQztDQUFBLEFBakNELENBQXdCLEtBQUssQ0FBQyxTQUFTLEdBaUN0QztBQUFBLENBQUM7QUFFRixlQUFlLFNBQVMsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider-item/index.tsx

/* harmony default export */ var image_slider_item = (image_slider_item_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxTQUFTLE1BQU0sYUFBYSxDQUFDO0FBQ3BDLGVBQWUsU0FBUyxDQUFDIn0=
// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// CONCATENATED MODULE: ./components/magazine/image-slider/style.tsx




var INLINE_STYLE = {
    '.magazine-slide-container .pagination': {
        opacity: 0,
        transform: 'translateY(20px)'
    },
    '.magazine-slide-container:hover .pagination': {
        opacity: 1,
        transform: 'translateY(0)'
    },
    '.magazine-slide-container .left-nav': {
        width: 40,
        height: 60,
        opacity: 0,
        transform: 'translate(-60px, -50%)',
        visibility: 'hidden',
    },
    '.magazine-slide-container:hover .left-nav': {
        opacity: 1,
        transform: 'translate(1px, -50%)',
        visibility: 'visible',
    },
    '.magazine-slide-container .right-nav': {
        width: 40,
        height: 60,
        opacity: 0,
        transform: 'translate(60px, -50%)',
        visibility: 'hidden',
    },
    '.magazine-slide-container:hover .right-nav': {
        opacity: 1,
        transform: 'translate(1px, -50%)',
        visibility: 'visible',
    }
};
/* harmony default export */ var image_slider_style = ({
    container: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{ marginBottom: 10 }],
        DESKTOP: [{ marginBottom: 30 }],
        GENERAL: [{ display: variable["display"].block, width: '100%' }]
    }),
    magazineSlide: {
        position: 'relative',
        overflow: 'hidden',
        container: Object.assign({}, layout["a" /* flexContainer */].justify, component["c" /* block */].content, {
            paddingTop: 10,
            transition: variable["transitionOpacity"],
        }),
        pagination: [
            layout["a" /* flexContainer */].center,
            component["f" /* slidePagination */],
            {
                transition: variable["transitionNormal"],
                bottom: 0
            },
        ],
        navigation: [
            layout["a" /* flexContainer */].center,
            layout["a" /* flexContainer */].verticalCenter,
            component["e" /* slideNavigation */]
        ],
    },
    customStyleLoading: {
        height: 300
    },
    mobileWrap: {
        width: '100%',
        overflowX: 'auto',
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        panel: {
            flexWrap: 'wrap'
        },
        item: {
            width: '100%',
            marginBottom: 5
        }
    },
    placeholder: {
        width: '100%',
        display: variable["display"].flex,
        marginBottom: 35,
        item: {
            flex: 1,
            paddingLeft: 10,
            paddingRight: 10,
        },
        image: {
            width: '100%',
            height: 282,
            marginBottom: 10,
        },
        text: {
            width: '94%',
            height: 25,
            marginBottom: 10,
        },
        lastText: {
            width: '65%',
            height: 25,
        }
    },
    desktop: {
        mainWrap: {
            display: variable["display"].block,
            marginLeft: -9,
            marginRight: -9
        },
        container: {
            width: '100%',
            overflowX: 'auto',
            whiteSpace: 'nowrap',
            paddingTop: 0,
            paddingBottom: 15,
            display: variable["display"].flex,
            overflow: 'hidden',
            itemSlider: {
                width: "100%",
                display: variable["display"].inlineBlock,
                borderRadius: 5,
                overflow: 'hidden',
                boxShadow: variable["shadowBlur"],
                position: variable["position"].relative,
            },
            itemSliderPanel: function (imgUrl) { return ({
                width: '100%',
                paddingTop: '62.5%',
                position: variable["position"].relative,
                backgroundImage: "url(" + imgUrl + ")",
                backgroundColor: variable["colorF7"],
                backgroundPosition: 'top center',
                backgroundSize: 'cover',
            }); },
            videoIcon: {
                width: 70,
                height: 70,
                position: variable["position"].absolute,
                top: '50%',
                left: '55%',
                transform: 'translate(-50%, -50%)',
                borderTop: '35px solid transparent',
                boxSizing: 'border-box',
                borderLeft: '51px solid white',
                borderBottom: '35px solid transparent',
                opacity: .8
            },
            info: {
                width: '100%',
                height: 110,
                padding: 12,
                position: variable["position"].relative,
                background: variable["colorWhite"],
                display: variable["display"].flex,
                flexDirection: 'column',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                image: function (imgUrl) { return ({
                    width: '100%',
                    height: '100%',
                    top: 0,
                    left: 0,
                    zIndex: -1,
                    position: variable["position"].absolute,
                    backgroundColor: variable["colorF7"],
                    backgroundImage: "url(" + imgUrl + ")",
                    backgroundPosition: 'bottom center',
                    backgroundSize: 'cover',
                    filter: 'blur(4px)',
                    transform: "scaleY(-1) scale(1.1)",
                    'WebkitBackfaceVisibility': 'hidden',
                    'WebkitPerspective': 1000,
                    'WebkitTransform': ['translate3d(0,0,0)', 'translateZ(0)'],
                    'backfaceVisibility': 'hidden',
                    perspective: 1000,
                }); },
                title: {
                    color: variable["colorBlack"],
                    whiteSpace: 'pre-wrap',
                    fontFamily: variable["fontAvenirMedium"],
                    fontSize: 14,
                    lineHeight: '22px',
                    maxHeight: '44px',
                    overflow: 'hidden',
                    marginBottom: 5
                },
                description: {
                    fontSize: 12,
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap',
                    lineHeight: '18px',
                    maxHeight: 36,
                    overflow: 'hidden',
                },
                tagList: {
                    width: '100%',
                    display: variable["display"].flex,
                    flexWrap: 'wrap',
                    height: '36px',
                    maxHeight: '36px',
                    overflow: 'hidden'
                },
                tagItem: {
                    fontSize: 12,
                    lineHeight: '18px',
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap'
                },
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFNBQVMsTUFBTSwwQkFBMEIsQ0FBQztBQUN0RCxPQUFPLEtBQUssTUFBTSxNQUFNLHVCQUF1QixDQUFDO0FBQ2hELE9BQU8sS0FBSyxRQUFRLE1BQU0seUJBQXlCLENBQUM7QUFDcEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRXpELE1BQU0sQ0FBQyxJQUFNLFlBQVksR0FBRztJQUMxQix1Q0FBdUMsRUFBRTtRQUN2QyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxrQkFBa0I7S0FDOUI7SUFFRCw2Q0FBNkMsRUFBRTtRQUM3QyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxlQUFlO0tBQzNCO0lBRUQscUNBQXFDLEVBQUU7UUFDckMsS0FBSyxFQUFFLEVBQUU7UUFDVCxNQUFNLEVBQUUsRUFBRTtRQUNWLE9BQU8sRUFBRSxDQUFDO1FBQ1YsU0FBUyxFQUFFLHdCQUF3QjtRQUNuQyxVQUFVLEVBQUUsUUFBUTtLQUNyQjtJQUVELDJDQUEyQyxFQUFFO1FBQzNDLE9BQU8sRUFBRSxDQUFDO1FBQ1YsU0FBUyxFQUFFLHNCQUFzQjtRQUNqQyxVQUFVLEVBQUUsU0FBUztLQUN0QjtJQUVELHNDQUFzQyxFQUFFO1FBQ3RDLEtBQUssRUFBRSxFQUFFO1FBQ1QsTUFBTSxFQUFFLEVBQUU7UUFDVixPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSx1QkFBdUI7UUFDbEMsVUFBVSxFQUFFLFFBQVE7S0FDckI7SUFFRCw0Q0FBNEMsRUFBRTtRQUM1QyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxzQkFBc0I7UUFDakMsVUFBVSxFQUFFLFNBQVM7S0FDdEI7Q0FDRixDQUFDO0FBRUYsZUFBZTtJQUNiLFNBQVMsRUFBRSxZQUFZLENBQUM7UUFDdEIsTUFBTSxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLENBQUM7UUFDOUIsT0FBTyxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLENBQUM7UUFDL0IsT0FBTyxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxDQUFDO0tBQzlELENBQUM7SUFFRixhQUFhLEVBQUU7UUFDYixRQUFRLEVBQUUsVUFBVTtRQUNwQixRQUFRLEVBQUUsUUFBUTtRQUVsQixTQUFTLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQ3pCLE1BQU0sQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUM1QixTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFDdkI7WUFDRSxVQUFVLEVBQUUsRUFBRTtZQUNkLFVBQVUsRUFBRSxRQUFRLENBQUMsaUJBQWlCO1NBQ3ZDLENBQ0Y7UUFFRCxVQUFVLEVBQUU7WUFDVixNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU07WUFDM0IsU0FBUyxDQUFDLGVBQWU7WUFDekI7Z0JBQ0UsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLE1BQU0sRUFBRSxDQUFDO2FBQ1Y7U0FDRjtRQUVELFVBQVUsRUFBRTtZQUNWLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTTtZQUMzQixNQUFNLENBQUMsYUFBYSxDQUFDLGNBQWM7WUFDbkMsU0FBUyxDQUFDLGVBQWU7U0FDMUI7S0FDRjtJQUVELGtCQUFrQixFQUFFO1FBQ2xCLE1BQU0sRUFBRSxHQUFHO0tBQ1o7SUFFRCxVQUFVLEVBQUU7UUFDVixLQUFLLEVBQUUsTUFBTTtRQUNiLFNBQVMsRUFBRSxNQUFNO1FBQ2pCLFVBQVUsRUFBRSxDQUFDO1FBQ2IsWUFBWSxFQUFFLENBQUM7UUFDZixhQUFhLEVBQUUsQ0FBQztRQUNoQixXQUFXLEVBQUUsQ0FBQztRQUVkLEtBQUssRUFBRTtZQUNMLFFBQVEsRUFBRSxNQUFNO1NBQ2pCO1FBRUQsSUFBSSxFQUFFO1lBQ0osS0FBSyxFQUFFLE1BQU07WUFDYixZQUFZLEVBQUUsQ0FBQztTQUNoQjtLQUNGO0lBRUQsV0FBVyxFQUFFO1FBQ1gsS0FBSyxFQUFFLE1BQU07UUFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1FBQzlCLFlBQVksRUFBRSxFQUFFO1FBRWhCLElBQUksRUFBRTtZQUNKLElBQUksRUFBRSxDQUFDO1lBQ1AsV0FBVyxFQUFFLEVBQUU7WUFDZixZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELEtBQUssRUFBRTtZQUNMLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLEdBQUc7WUFDWCxZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELElBQUksRUFBRTtZQUNKLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLEVBQUU7WUFDVixZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELFFBQVEsRUFBRTtZQUNSLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLEVBQUU7U0FDWDtLQUNGO0lBRUQsT0FBTyxFQUFFO1FBQ1AsUUFBUSxFQUFFO1lBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztZQUMvQixVQUFVLEVBQUUsQ0FBQyxDQUFDO1lBQ2QsV0FBVyxFQUFFLENBQUMsQ0FBQztTQUNoQjtRQUVELFNBQVMsRUFBRTtZQUNULEtBQUssRUFBRSxNQUFNO1lBQ2IsU0FBUyxFQUFFLE1BQU07WUFDakIsVUFBVSxFQUFFLFFBQVE7WUFDcEIsVUFBVSxFQUFFLENBQUM7WUFDYixhQUFhLEVBQUUsRUFBRTtZQUNqQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLFFBQVEsRUFBRSxRQUFRO1lBRWxCLFVBQVUsRUFBRTtnQkFDVixLQUFLLEVBQUUsTUFBTTtnQkFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxXQUFXO2dCQUNyQyxZQUFZLEVBQUUsQ0FBQztnQkFDZixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUM5QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2FBQ3JDO1lBRUQsZUFBZSxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztnQkFDNUIsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsVUFBVSxFQUFFLE9BQU87Z0JBQ25CLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztnQkFDakMsZUFBZSxFQUFFLFFBQVEsQ0FBQyxPQUFPO2dCQUNqQyxrQkFBa0IsRUFBRSxZQUFZO2dCQUNoQyxjQUFjLEVBQUUsT0FBTzthQUN4QixDQUFDLEVBUjJCLENBUTNCO1lBRUYsU0FBUyxFQUFFO2dCQUNULEtBQUssRUFBRSxFQUFFO2dCQUNULE1BQU0sRUFBRSxFQUFFO2dCQUNWLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLEdBQUcsRUFBRSxLQUFLO2dCQUNWLElBQUksRUFBRSxLQUFLO2dCQUNYLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLFNBQVMsRUFBRSx3QkFBd0I7Z0JBQ25DLFNBQVMsRUFBRSxZQUFZO2dCQUN2QixVQUFVLEVBQUUsa0JBQWtCO2dCQUM5QixZQUFZLEVBQUUsd0JBQXdCO2dCQUN0QyxPQUFPLEVBQUUsRUFBRTthQUNaO1lBRUQsSUFBSSxFQUFFO2dCQUNKLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxHQUFHO2dCQUNYLE9BQU8sRUFBRSxFQUFFO2dCQUNYLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLFVBQVUsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDL0IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsYUFBYSxFQUFFLFFBQVE7Z0JBQ3ZCLGNBQWMsRUFBRSxZQUFZO2dCQUM1QixVQUFVLEVBQUUsWUFBWTtnQkFFeEIsS0FBSyxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztvQkFDbEIsS0FBSyxFQUFFLE1BQU07b0JBQ2IsTUFBTSxFQUFFLE1BQU07b0JBQ2QsR0FBRyxFQUFFLENBQUM7b0JBQ04sSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLENBQUMsQ0FBQztvQkFDVixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUNwQyxlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87b0JBQ2pDLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztvQkFDakMsa0JBQWtCLEVBQUUsZUFBZTtvQkFDbkMsY0FBYyxFQUFFLE9BQU87b0JBQ3ZCLE1BQU0sRUFBRSxXQUFXO29CQUNuQixTQUFTLEVBQUUsdUJBQXVCO29CQUNsQywwQkFBMEIsRUFBRSxRQUFRO29CQUNwQyxtQkFBbUIsRUFBRSxJQUFJO29CQUN6QixpQkFBaUIsRUFBRSxDQUFDLG9CQUFvQixFQUFFLGVBQWUsQ0FBQztvQkFDMUQsb0JBQW9CLEVBQUUsUUFBUTtvQkFDOUIsV0FBVyxFQUFFLElBQUk7aUJBQ2xCLENBQUMsRUFsQmlCLENBa0JqQjtnQkFFRixLQUFLLEVBQUU7b0JBQ0wsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsVUFBVTtvQkFDdEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7b0JBQ3JDLFFBQVEsRUFBRSxFQUFFO29CQUNaLFVBQVUsRUFBRSxNQUFNO29CQUNsQixTQUFTLEVBQUUsTUFBTTtvQkFDakIsUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLFlBQVksRUFBRSxDQUFDO2lCQUNoQjtnQkFFRCxXQUFXLEVBQUU7b0JBQ1gsUUFBUSxFQUFFLEVBQUU7b0JBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsVUFBVSxFQUFFLFVBQVU7b0JBQ3RCLFVBQVUsRUFBRSxNQUFNO29CQUNsQixTQUFTLEVBQUUsRUFBRTtvQkFDYixRQUFRLEVBQUUsUUFBUTtpQkFDbkI7Z0JBRUQsT0FBTyxFQUFFO29CQUNQLEtBQUssRUFBRSxNQUFNO29CQUNiLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7b0JBQzlCLFFBQVEsRUFBRSxNQUFNO29CQUNoQixNQUFNLEVBQUUsTUFBTTtvQkFDZCxTQUFTLEVBQUUsTUFBTTtvQkFDakIsUUFBUSxFQUFFLFFBQVE7aUJBQ25CO2dCQUVELE9BQU8sRUFBRTtvQkFDUCxRQUFRLEVBQUUsRUFBRTtvQkFDWixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsVUFBVSxFQUFFLFVBQVU7aUJBQ3ZCO2FBQ0Y7U0FDRjtLQUNGO0NBQ0ssQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider/view-desktop.tsx
var view_desktop_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};






var renderSlider = function (_a) {
    var column = _a.column, type = _a.type, magazineSlideSelected = _a.magazineSlideSelected, style = _a.style;
    return (react["createElement"]("magazine-category", { style: image_slider_style.desktop.mainWrap },
        react["createElement"]("div", { style: [image_slider_style.desktop.container, style] }, magazineSlideSelected
            && Array.isArray(magazineSlideSelected.list)
            && magazineSlideSelected.list.map(function (item) {
                var slideProps = {
                    item: item,
                    type: type,
                    column: column
                };
                return react["createElement"](image_slider_item, view_desktop_assign({ key: "slider-item-" + item.id }, slideProps));
            }))));
};
var renderNavigation = function (_a) {
    var magazineSlide = _a.magazineSlide, navLeftSlide = _a.navLeftSlide, navRightSlide = _a.navRightSlide;
    var leftNavProps = {
        className: 'left-nav',
        onClick: navLeftSlide,
        style: [image_slider_style.magazineSlide.navigation, component["e" /* slideNavigation */]['left'], { top: "calc(50% - 65px)" }]
    };
    var rightNavProps = {
        className: 'right-nav',
        onClick: navRightSlide,
        style: [image_slider_style.magazineSlide.navigation, component["e" /* slideNavigation */]['right'], { top: "calc(50% - 65px)" }]
    };
    return magazineSlide.length <= 1
        ? null
        : (react["createElement"]("div", null,
            react["createElement"]("div", view_desktop_assign({}, leftNavProps),
                react["createElement"](icon["a" /* default */], { name: 'angle-left', style: component["e" /* slideNavigation */].icon })),
            react["createElement"]("div", view_desktop_assign({}, rightNavProps),
                react["createElement"](icon["a" /* default */], { name: 'angle-right', style: component["e" /* slideNavigation */].icon }))));
};
var renderPagination = function (_a) {
    var magazineSlide = _a.magazineSlide, selectSlide = _a.selectSlide, countChangeSlide = _a.countChangeSlide;
    var generateItemProps = function (item, index) { return ({
        key: "product-slider-" + item.id,
        onClick: function () { return selectSlide(index); },
        style: [
            component["f" /* slidePagination */].item,
            index === countChangeSlide && component["f" /* slidePagination */].itemActive
        ]
    }); };
    return magazineSlide.length <= 1
        ? null
        : (react["createElement"]("div", { style: image_slider_style.magazineSlide.pagination, className: 'pagination' }, Array.isArray(magazineSlide)
            && magazineSlide.map(function (item, $index) {
                var itemProps = generateItemProps(item, $index);
                return react["createElement"]("div", view_desktop_assign({}, itemProps));
            })));
};
var renderDesktop = function (_a) {
    var props = _a.props, state = _a.state, handleMouseHover = _a.handleMouseHover, selectSlide = _a.selectSlide, navLeftSlide = _a.navLeftSlide, navRightSlide = _a.navRightSlide;
    var _b = state, magazineSlide = _b.magazineSlide, magazineSlideSelected = _b.magazineSlideSelected, countChangeSlide = _b.countChangeSlide;
    var _c = props, column = _c.column, type = _c.type, style = _c.style;
    var containerProps = {
        style: image_slider_style.magazineSlide,
        onMouseEnter: handleMouseHover
    };
    return (react["createElement"]("div", view_desktop_assign({}, containerProps, { className: 'magazine-slide-container' }),
        renderSlider({ column: column, type: type, magazineSlideSelected: magazineSlideSelected, style: style }),
        renderPagination({ magazineSlide: magazineSlide, selectSlide: selectSlide, countChangeSlide: countChangeSlide }),
        renderNavigation({ magazineSlide: magazineSlide, navLeftSlide: navLeftSlide, navRightSlide: navRightSlide }),
        react["createElement"](radium["Style"], { rules: INLINE_STYLE })));
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1kZXNrdG9wLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1kZXNrdG9wLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFL0IsT0FBTyxJQUFJLE1BQU0sZUFBZSxDQUFDO0FBQ2pDLE9BQU8sS0FBSyxTQUFTLE1BQU0sMEJBQTBCLENBQUM7QUFFdEQsT0FBTyxlQUFlLE1BQU0sc0JBQXNCLENBQUM7QUFFbkQsT0FBTyxLQUFLLEVBQUUsRUFBRSxZQUFZLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFHOUMsSUFBTSxZQUFZLEdBQUcsVUFBQyxFQUE4QztRQUE1QyxrQkFBTSxFQUFFLGNBQUksRUFBRSxnREFBcUIsRUFBRSxnQkFBSztJQUFPLE9BQUEsQ0FDdkUsMkNBQW1CLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVE7UUFDOUMsNkJBQUssS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLElBRXhDLHFCQUFxQjtlQUNsQixLQUFLLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQztlQUN6QyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSTtnQkFDcEMsSUFBTSxVQUFVLEdBQUc7b0JBQ2pCLElBQUksTUFBQTtvQkFDSixJQUFJLE1BQUE7b0JBQ0osTUFBTSxRQUFBO2lCQUNQLENBQUE7Z0JBRUQsTUFBTSxDQUFDLG9CQUFDLGVBQWUsYUFBQyxHQUFHLEVBQUUsaUJBQWUsSUFBSSxDQUFDLEVBQUksSUFBTSxVQUFVLEVBQUksQ0FBQTtZQUMzRSxDQUFDLENBQUMsQ0FFQSxDQUNZLENBQ3JCO0FBbEJ3RSxDQWtCeEUsQ0FBQztBQUVGLElBQU0sZ0JBQWdCLEdBQUcsVUFBQyxFQUE4QztRQUE1QyxnQ0FBYSxFQUFFLDhCQUFZLEVBQUUsZ0NBQWE7SUFDcEUsSUFBTSxZQUFZLEdBQUc7UUFDbkIsU0FBUyxFQUFFLFVBQVU7UUFDckIsT0FBTyxFQUFFLFlBQVk7UUFDckIsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxrQkFBa0IsRUFBRSxDQUFDO0tBQ3hHLENBQUM7SUFFRixJQUFNLGFBQWEsR0FBRztRQUNwQixTQUFTLEVBQUUsV0FBVztRQUN0QixPQUFPLEVBQUUsYUFBYTtRQUN0QixLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxFQUFFLEVBQUUsR0FBRyxFQUFFLGtCQUFrQixFQUFFLENBQUM7S0FDekcsQ0FBQztJQUVGLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxJQUFJLENBQUM7UUFDOUIsQ0FBQyxDQUFDLElBQUk7UUFDTixDQUFDLENBQUMsQ0FDQTtZQUNFLHdDQUFTLFlBQVk7Z0JBQ25CLG9CQUFDLElBQUksSUFBQyxJQUFJLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsZUFBZSxDQUFDLElBQUksR0FBSSxDQUMvRDtZQUNOLHdDQUFTLGFBQWE7Z0JBQ3BCLG9CQUFDLElBQUksSUFBQyxJQUFJLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsZUFBZSxDQUFDLElBQUksR0FBSSxDQUNoRSxDQUNGLENBQ1AsQ0FBQztBQUNOLENBQUMsQ0FBQztBQUVGLElBQU0sZ0JBQWdCLEdBQUcsVUFBQyxFQUFnRDtRQUE5QyxnQ0FBYSxFQUFFLDRCQUFXLEVBQUUsc0NBQWdCO0lBQ3RFLElBQU0saUJBQWlCLEdBQUcsVUFBQyxJQUFJLEVBQUUsS0FBSyxJQUFLLE9BQUEsQ0FBQztRQUMxQyxHQUFHLEVBQUUsb0JBQWtCLElBQUksQ0FBQyxFQUFJO1FBQ2hDLE9BQU8sRUFBRSxjQUFNLE9BQUEsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFsQixDQUFrQjtRQUNqQyxLQUFLLEVBQUU7WUFDTCxTQUFTLENBQUMsZUFBZSxDQUFDLElBQUk7WUFDOUIsS0FBSyxLQUFLLGdCQUFnQixJQUFJLFNBQVMsQ0FBQyxlQUFlLENBQUMsVUFBVTtTQUNuRTtLQUNGLENBQUMsRUFQeUMsQ0FPekMsQ0FBQztJQUVILE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxJQUFJLENBQUM7UUFDOUIsQ0FBQyxDQUFDLElBQUk7UUFDTixDQUFDLENBQUMsQ0FDQSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsU0FBUyxFQUFFLFlBQVksSUFFL0QsS0FBSyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUM7ZUFDekIsYUFBYSxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxNQUFNO2dCQUNoQyxJQUFNLFNBQVMsR0FBRyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7Z0JBQ2xELE1BQU0sQ0FBQyx3Q0FBUyxTQUFTLEVBQVEsQ0FBQztZQUNwQyxDQUFDLENBQUMsQ0FFQSxDQUNQLENBQUM7QUFDTixDQUFDLENBQUM7QUFFRixNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUcsVUFBQyxFQUE0RTtRQUExRSxnQkFBSyxFQUFFLGdCQUFLLEVBQUUsc0NBQWdCLEVBQUUsNEJBQVcsRUFBRSw4QkFBWSxFQUFFLGdDQUFhO0lBQ2hHLElBQUEsVUFBNEUsRUFBMUUsZ0NBQWEsRUFBRSxnREFBcUIsRUFBRSxzQ0FBZ0IsQ0FBcUI7SUFDN0UsSUFBQSxVQUF5QyxFQUF2QyxrQkFBTSxFQUFFLGNBQUksRUFBRSxnQkFBSyxDQUFxQjtJQUVoRCxJQUFNLGNBQWMsR0FBRztRQUNyQixLQUFLLEVBQUUsS0FBSyxDQUFDLGFBQWE7UUFDMUIsWUFBWSxFQUFFLGdCQUFnQjtLQUMvQixDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsd0NBQVMsY0FBYyxJQUFFLFNBQVMsRUFBRSwwQkFBMEI7UUFDM0QsWUFBWSxDQUFDLEVBQUUsTUFBTSxRQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUscUJBQXFCLHVCQUFBLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQztRQUM1RCxnQkFBZ0IsQ0FBQyxFQUFFLGFBQWEsZUFBQSxFQUFFLFdBQVcsYUFBQSxFQUFFLGdCQUFnQixrQkFBQSxFQUFFLENBQUM7UUFDbEUsZ0JBQWdCLENBQUMsRUFBRSxhQUFhLGVBQUEsRUFBRSxZQUFZLGNBQUEsRUFBRSxhQUFhLGVBQUEsRUFBRSxDQUFDO1FBQ2pFLG9CQUFDLEtBQUssSUFBQyxLQUFLLEVBQUUsWUFBWSxHQUFJLENBQzFCLENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider/view-mobile.tsx





var renderMobile = function (_a) {
    var type = _a.type, magazineList = _a.magazineList, column = _a.column;
    return (react["createElement"]("div", { style: [component["c" /* block */].content, image_slider_style.mobileWrap] },
        react["createElement"]("div", { style: [layout["a" /* flexContainer */].noWrap, image_slider_style.mobileWrap.panel] }, Array.isArray(magazineList)
            && magazineList.map(function (magazine, index) { return react["createElement"]("div", { style: image_slider_style.mobileWrap.item },
                react["createElement"](image_slider_item, { key: "image-slide-item-" + index, item: magazine, type: type, column: column })); }))));
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1tb2JpbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LW1vYmlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxlQUFlLE1BQU0sc0JBQXNCLENBQUM7QUFFbkQsT0FBTyxLQUFLLFNBQVMsTUFBTSwwQkFBMEIsQ0FBQztBQUN0RCxPQUFPLEtBQUssTUFBTSxNQUFNLHVCQUF1QixDQUFDO0FBQ2hELE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixNQUFNLENBQUMsSUFBTSxZQUFZLEdBQUcsVUFBQyxFQUE4QjtRQUE1QixjQUFJLEVBQUUsOEJBQVksRUFBRSxrQkFBTTtJQUV2RCxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsVUFBVSxDQUFDO1FBQ3JELDZCQUFLLEtBQUssRUFBRSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLElBRTdELEtBQUssQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDO2VBQ3hCLFlBQVksQ0FBQyxHQUFHLENBQUMsVUFBQyxRQUFRLEVBQUUsS0FBSyxJQUFLLE9BQUEsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSTtnQkFBRSxvQkFBQyxlQUFlLElBQUMsR0FBRyxFQUFFLHNCQUFvQixLQUFPLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxNQUFNLEdBQUksQ0FBTSxFQUExSSxDQUEwSSxDQUFDLENBRWxMLENBQ0YsQ0FDUCxDQUFDO0FBQ0osQ0FBQyxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/image-slider/view.tsx
var view_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};







var renderItemPlaceholder = function (item) { return (react["createElement"]("div", { style: image_slider_style.placeholder.item, key: item },
    react["createElement"](loading_placeholder["a" /* default */], { style: image_slider_style.placeholder.image }))); };
var renderLoadingPlaceholder = function () { return (react["createElement"]("div", { style: image_slider_style.placeholder }, [1, 2, 3, 4].map(renderItemPlaceholder))); };
var renderCustomTitle = function (_a) {
    var title = _a.title, isCustomTitle = _a.isCustomTitle, _b = _a.titleStyle, titleStyle = _b === void 0 ? {} : _b;
    return false === isCustomTitle
        ? null
        : (react["createElement"]("div", null,
            react["createElement"]("div", { style: [component["c" /* block */].heading, component["c" /* block */].heading.multiLine, image_slider_style.desktopTitle, titleStyle] },
                react["createElement"]("div", { style: component["c" /* block */].heading.title.multiLine },
                    react["createElement"]("span", { style: [component["c" /* block */].heading.title.text, component["c" /* block */].heading.title.text.multiLine] }, title)))));
};
var renderView = function (_a) {
    var props = _a.props, state = _a.state, handleMouseHover = _a.handleMouseHover, selectSlide = _a.selectSlide, navLeftSlide = _a.navLeftSlide, navRightSlide = _a.navRightSlide;
    var magazineList = state.magazineList;
    var _b = props, type = _b.type, title = _b.title, showViewMore = _b.showViewMore, column = _b.column, showHeader = _b.showHeader, isCustomTitle = _b.isCustomTitle, titleStyle = _b.titleStyle;
    var renderMobileProps = { type: type, magazineList: magazineList, column: column };
    var renderDesktopProps = {
        props: props,
        state: state,
        navLeftSlide: navLeftSlide,
        navRightSlide: navRightSlide,
        selectSlide: selectSlide,
        handleMouseHover: handleMouseHover
    };
    var switchStyle = {
        MOBILE: function () { return renderMobile(renderMobileProps); },
        DESKTOP: function () { return renderDesktop(renderDesktopProps); }
    };
    var mainBlockProps = {
        title: isCustomTitle ? '' : title,
        showHeader: showHeader,
        showViewMore: showViewMore,
        content: 0 === magazineList.length
            ? renderLoadingPlaceholder()
            : switchStyle[window.DEVICE_VERSION](),
        style: {}
    };
    return (react["createElement"]("div", { style: image_slider_style.container },
        isCustomTitle && renderCustomTitle({ title: title, isCustomTitle: isCustomTitle, titleStyle: titleStyle }),
        react["createElement"](main_block["a" /* default */], view_assign({}, mainBlockProps))));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFHL0IsT0FBTyxTQUFTLE1BQU0sc0NBQXNDLENBQUM7QUFFN0QsT0FBTyxrQkFBa0IsTUFBTSw4QkFBOEIsQ0FBQztBQUU5RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDL0MsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUU3QyxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsT0FBTyxLQUFLLFNBQVMsTUFBTSwwQkFBMEIsQ0FBQztBQUV0RCxJQUFNLHFCQUFxQixHQUFHLFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FDdEMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJO0lBQzNDLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBSSxDQUNsRCxDQUNQLEVBSnVDLENBSXZDLENBQUM7QUFFRixJQUFNLHdCQUF3QixHQUFHLGNBQU0sT0FBQSxDQUNyQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsSUFDMUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FDcEMsQ0FDUCxFQUpzQyxDQUl0QyxDQUFDO0FBRUYsSUFBTSxpQkFBaUIsR0FBRyxVQUFDLEVBQXlDO1FBQXZDLGdCQUFLLEVBQUUsZ0NBQWEsRUFBRSxrQkFBZSxFQUFmLG9DQUFlO0lBQU8sT0FBQSxLQUFLLEtBQUssYUFBYTtRQUM5RixDQUFDLENBQUMsSUFBSTtRQUNOLENBQUMsQ0FBQyxDQUNBO1lBQ0UsNkJBQUssS0FBSyxFQUFFLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxZQUFZLEVBQUUsVUFBVSxDQUFDO2dCQUN0Ryw2QkFBSyxLQUFLLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFNBQVM7b0JBQ2pELDhCQUFNLEtBQUssRUFBRSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFDNUYsS0FBSyxDQUNELENBQ0gsQ0FDRixDQUNGLENBQ1A7QUFac0UsQ0FZdEUsQ0FBQztBQUVKLElBQU0sVUFBVSxHQUFHLFVBQUMsRUFBNEU7UUFBMUUsZ0JBQUssRUFBRSxnQkFBSyxFQUFFLHNDQUFnQixFQUFFLDRCQUFXLEVBQUUsOEJBQVksRUFBRSxnQ0FBYTtJQUNwRixJQUFBLGlDQUFZLENBQXFCO0lBQ25DLElBQUEsVUFBOEYsRUFBNUYsY0FBSSxFQUFFLGdCQUFLLEVBQUUsOEJBQVksRUFBRSxrQkFBTSxFQUFFLDBCQUFVLEVBQUUsZ0NBQWEsRUFBRSwwQkFBVSxDQUFxQjtJQUVyRyxJQUFNLGlCQUFpQixHQUFHLEVBQUUsSUFBSSxNQUFBLEVBQUUsWUFBWSxjQUFBLEVBQUUsTUFBTSxRQUFBLEVBQUUsQ0FBQztJQUN6RCxJQUFNLGtCQUFrQixHQUFHO1FBQ3pCLEtBQUssT0FBQTtRQUNMLEtBQUssT0FBQTtRQUNMLFlBQVksY0FBQTtRQUNaLGFBQWEsZUFBQTtRQUNiLFdBQVcsYUFBQTtRQUNYLGdCQUFnQixrQkFBQTtLQUNqQixDQUFDO0lBRUYsSUFBTSxXQUFXLEdBQUc7UUFDbEIsTUFBTSxFQUFFLGNBQU0sT0FBQSxZQUFZLENBQUMsaUJBQWlCLENBQUMsRUFBL0IsQ0FBK0I7UUFDN0MsT0FBTyxFQUFFLGNBQU0sT0FBQSxhQUFhLENBQUMsa0JBQWtCLENBQUMsRUFBakMsQ0FBaUM7S0FDakQsQ0FBQztJQUVGLElBQU0sY0FBYyxHQUFHO1FBQ3JCLEtBQUssRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSztRQUNqQyxVQUFVLFlBQUE7UUFDVixZQUFZLGNBQUE7UUFDWixPQUFPLEVBQUUsQ0FBQyxLQUFLLFlBQVksQ0FBQyxNQUFNO1lBQ2hDLENBQUMsQ0FBQyx3QkFBd0IsRUFBRTtZQUM1QixDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFBRTtRQUN4QyxLQUFLLEVBQUUsRUFBRTtLQUNWLENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFNBQVM7UUFDeEIsYUFBYSxJQUFJLGlCQUFpQixDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsYUFBYSxlQUFBLEVBQUUsVUFBVSxZQUFBLEVBQUUsQ0FBQztRQUN6RSxvQkFBQyxTQUFTLGVBQUssY0FBYyxFQUFJLENBQzdCLENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/image-slider/component.tsx
var component_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var component_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var component_ImageSlide = /** @class */ (function (_super) {
    component_extends(ImageSlide, _super);
    function ImageSlide(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE(props.data);
        return _this;
    }
    ImageSlide.prototype.componentDidMount = function () {
        this.initDataSlide();
    };
    /**
     * When component will receive new props -> init data for slide
     * @param nextProps prop from parent
     */
    ImageSlide.prototype.componentWillReceiveProps = function (nextProps) {
        this.initDataSlide(nextProps.data);
    };
    /**
     * Init data for slide
     * When : Component will mount OR Component will receive new props
     */
    ImageSlide.prototype.initDataSlide = function (_newMagazineList) {
        var _this = this;
        if (_newMagazineList === void 0) { _newMagazineList = this.state.magazineList; }
        if (true === Object(responsive["b" /* isDesktopVersion */])()) {
            if (false === this.state.firstInit) {
                this.setState({ firstInit: true });
            }
            /**
             * On DESKTOP
             * Init data for magazine slide & magazine slide selected
             */
            var _magazineSlide_1 = [];
            var groupMagazine_1 = {
                id: 0,
                list: []
            };
            /** Assign data into each slider */
            if (_newMagazineList.length > (this.props.column || 3)) {
                Array.isArray(_newMagazineList)
                    && _newMagazineList.map(function (magazine, $index) {
                        groupMagazine_1.id = _magazineSlide_1.length;
                        groupMagazine_1.list.push(magazine);
                        if (groupMagazine_1.list.length === _this.props.column) {
                            _magazineSlide_1.push(Object.assign({}, groupMagazine_1));
                            groupMagazine_1.list = [];
                        }
                    });
            }
            else {
                _magazineSlide_1 = [{ id: 0, list: _newMagazineList }];
            }
            this.setState({
                magazineList: _newMagazineList,
                magazineSlide: _magazineSlide_1,
                magazineSlideSelected: _magazineSlide_1[0] || {}
            });
        }
        else {
            /**
             * On Mobile
             * Only init data for list magazine, not apply slide animation
             */
            this.setState({ magazineList: _newMagazineList });
        }
    };
    /**
     * Navigate slide by button left or right
     * @param _direction `LEFT` or `RIGHT`
     * Will set new index value by @param _direction
     */
    ImageSlide.prototype.navSlide = function (_direction) {
        var _a = this.state, magazineSlide = _a.magazineSlide, countChangeSlide = _a.countChangeSlide;
        /**
         * If navigate to right: increase index value -> set +1 by countChangeSlide
         * If vavigate to left: decrease index value -> set -1 by countChangeSlide
         */
        var newIndexValue = 'left' === _direction ? -1 : 1;
        newIndexValue += countChangeSlide;
        /**
         * Validate new value in range [0, magazineSlide.length - 1]
         */
        newIndexValue = newIndexValue === magazineSlide.length
            ? 0 /** If over max value -> set 0 */
            : (newIndexValue === -1
                /** If under min value -> set magazineSlide.length - 1 */
                ? magazineSlide.length - 1
                : newIndexValue);
        /** Change to new index value */
        this.selectSlide(newIndexValue);
    };
    ImageSlide.prototype.navLeftSlide = function () {
        this.navSlide('left');
    };
    ImageSlide.prototype.navRightSlide = function () {
        this.navSlide('right');
    };
    /**
     * Change slide by set state and setTimeout for animation
     * @param _index new index value
     */
    ImageSlide.prototype.selectSlide = function (_index) {
        var _this = this;
        /** Change background */
        setTimeout(function () { return _this.setState(function (prevState, props) { return ({
            countChangeSlide: _index,
            magazineSlideSelected: prevState.magazineSlide[_index],
        }); }); }, 10);
    };
    ImageSlide.prototype.handleMouseHover = function () {
        var _a = this.props, _b = _a.data, data = _b === void 0 ? [] : _b, _c = _a.column, column = _c === void 0 ? 3 : _c;
        var preLoadImageList = Array.isArray(data)
            ? data
                .filter(function (item, $index) { return $index >= column; })
                .map(function (item) { return item.cover_image.original_url; })
            : [];
        Object(utils_image["b" /* preLoadImage */])(preLoadImageList);
    };
    // shouldComponentUpdate(nextProps: IProps, nextState: IState) {
    //   // if (this.props.data.length !== nextProps.data.length) { return true; }
    //   // if (this.state.countChangeSlide !== nextState.countChangeSlide) { return true; }
    //   // if (this.props.data.length > 0 && this.state.firstInit !== nextState.firstInit) { return true; }
    //   return true;
    // }
    ImageSlide.prototype.render = function () {
        var _this = this;
        var renderViewProps = {
            props: this.props,
            state: this.state,
            handleMouseHover: this.handleMouseHover.bind(this),
            selectSlide: function (index) { return _this.selectSlide(index); },
            navLeftSlide: this.navLeftSlide.bind(this),
            navRightSlide: this.navRightSlide.bind(this)
        };
        return view(renderViewProps);
    };
    ImageSlide.defaultProps = DEFAULT_PROPS;
    ImageSlide = component_decorate([
        radium
    ], ImageSlide);
    return ImageSlide;
}(react["Component"]));
;
/* harmony default export */ var image_slider_component = (component_ImageSlide);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRzdELE9BQU8sRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQzVELE9BQU8sVUFBVSxNQUFNLFFBQVEsQ0FBQztBQUdoQztJQUF5Qiw4QkFBK0I7SUFHdEQsb0JBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDOztJQUN6QyxDQUFDO0lBRUQsc0NBQWlCLEdBQWpCO1FBQ0UsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFFRDs7O09BR0c7SUFDSCw4Q0FBeUIsR0FBekIsVUFBMEIsU0FBUztRQUNqQyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsa0NBQWEsR0FBYixVQUFjLGdCQUFzRDtRQUFwRSxpQkEwQ0M7UUExQ2EsaUNBQUEsRUFBQSxtQkFBK0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZO1FBQ2xFLEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUVoQyxFQUFFLENBQUMsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFZLENBQUMsQ0FBQztZQUFDLENBQUM7WUFDckY7OztlQUdHO1lBQ0gsSUFBSSxnQkFBYyxHQUFlLEVBQUUsQ0FBQztZQUNwQyxJQUFJLGVBQWEsR0FBc0M7Z0JBQ3JELEVBQUUsRUFBRSxDQUFDO2dCQUNMLElBQUksRUFBRSxFQUFFO2FBQ1QsQ0FBQztZQUVGLG1DQUFtQztZQUNuQyxFQUFFLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZELEtBQUssQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUM7dUJBQzFCLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxVQUFDLFFBQVEsRUFBRSxNQUFNO3dCQUN2QyxlQUFhLENBQUMsRUFBRSxHQUFHLGdCQUFjLENBQUMsTUFBTSxDQUFDO3dCQUN6QyxlQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFFbEMsRUFBRSxDQUFDLENBQUMsZUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssS0FBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDOzRCQUNwRCxnQkFBYyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxlQUFhLENBQUMsQ0FBQyxDQUFDOzRCQUN0RCxlQUFhLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQzt3QkFDMUIsQ0FBQztvQkFDSCxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixnQkFBYyxHQUFHLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7WUFDdkQsQ0FBQztZQUVELElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ1osWUFBWSxFQUFFLGdCQUFnQjtnQkFDOUIsYUFBYSxFQUFFLGdCQUFjO2dCQUM3QixxQkFBcUIsRUFBRSxnQkFBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUU7YUFDckMsQ0FBQyxDQUFDO1FBQ2YsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ047OztlQUdHO1lBQ0gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLFlBQVksRUFBRSxnQkFBZ0IsRUFBWSxDQUFDLENBQUM7UUFDOUQsQ0FBQztJQUNILENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsNkJBQVEsR0FBUixVQUFTLFVBQVU7UUFDWCxJQUFBLGVBQWdELEVBQTlDLGdDQUFhLEVBQUUsc0NBQWdCLENBQWdCO1FBRXZEOzs7V0FHRztRQUNILElBQUksYUFBYSxHQUFHLE1BQU0sS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbkQsYUFBYSxJQUFJLGdCQUFnQixDQUFDO1FBRWxDOztXQUVHO1FBQ0gsYUFBYSxHQUFHLGFBQWEsS0FBSyxhQUFhLENBQUMsTUFBTTtZQUNwRCxDQUFDLENBQUMsQ0FBQyxDQUFDLGlDQUFpQztZQUNyQyxDQUFDLENBQUMsQ0FDQSxhQUFhLEtBQUssQ0FBQyxDQUFDO2dCQUNsQix5REFBeUQ7Z0JBQ3pELENBQUMsQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUM7Z0JBQzFCLENBQUMsQ0FBQyxhQUFhLENBQ2xCLENBQUM7UUFFSixnQ0FBZ0M7UUFDaEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRUQsaUNBQVksR0FBWjtRQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDeEIsQ0FBQztJQUVELGtDQUFhLEdBQWI7UUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxnQ0FBVyxHQUFYLFVBQVksTUFBTTtRQUFsQixpQkFPQztRQUxDLHdCQUF3QjtRQUN4QixVQUFVLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxRQUFRLENBQUMsVUFBQyxTQUFTLEVBQUUsS0FBSyxJQUFLLE9BQUEsQ0FBQztZQUNwRCxnQkFBZ0IsRUFBRSxNQUFNO1lBQ3hCLHFCQUFxQixFQUFFLFNBQVMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDO1NBQzVDLENBQUEsRUFIeUMsQ0FHekMsQ0FBQyxFQUhJLENBR0osRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNyQixDQUFDO0lBRUQscUNBQWdCLEdBQWhCO1FBQ1EsSUFBQSxlQUFzQyxFQUFwQyxZQUFTLEVBQVQsOEJBQVMsRUFBRSxjQUFVLEVBQVYsK0JBQVUsQ0FBZ0I7UUFFN0MsSUFBTSxnQkFBZ0IsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztZQUMxQyxDQUFDLENBQUMsSUFBSTtpQkFDSCxNQUFNLENBQUMsVUFBQyxJQUFJLEVBQUUsTUFBTSxJQUFLLE9BQUEsTUFBTSxJQUFJLE1BQU0sRUFBaEIsQ0FBZ0IsQ0FBQztpQkFDMUMsR0FBRyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQTdCLENBQTZCLENBQUM7WUFDN0MsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUVQLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFRCxnRUFBZ0U7SUFDaEUsOEVBQThFO0lBQzlFLHdGQUF3RjtJQUN4Rix3R0FBd0c7SUFFeEcsaUJBQWlCO0lBQ2pCLElBQUk7SUFFSiwyQkFBTSxHQUFOO1FBQUEsaUJBV0M7UUFWQyxJQUFNLGVBQWUsR0FBRztZQUN0QixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLGdCQUFnQixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2xELFdBQVcsRUFBRSxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQXZCLENBQXVCO1lBQzdDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUMsYUFBYSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztTQUM3QyxDQUFDO1FBRUYsTUFBTSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBdEpNLHVCQUFZLEdBQVcsYUFBYSxDQUFDO0lBRHhDLFVBQVU7UUFEZixNQUFNO09BQ0QsVUFBVSxDQXdKZjtJQUFELGlCQUFDO0NBQUEsQUF4SkQsQ0FBeUIsS0FBSyxDQUFDLFNBQVMsR0F3SnZDO0FBQUEsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/image-slider/index.tsx

/* harmony default export */ var image_slider = __webpack_exports__["a"] = (image_slider_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxVQUFVLE1BQU0sYUFBYSxDQUFDO0FBQ3JDLGVBQWUsVUFBVSxDQUFDIn0=

/***/ }),

/***/ 772:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./action/activity-feed.ts + 1 modules
var activity_feed = __webpack_require__(761);

// EXTERNAL MODULE: ./action/like.ts + 1 modules
var like = __webpack_require__(776);

// EXTERNAL MODULE: ./action/modal.ts
var modal = __webpack_require__(84);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/encode.ts
var encode = __webpack_require__(7);

// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// EXTERNAL MODULE: ./components/ui/loading/index.tsx + 4 modules
var loading = __webpack_require__(747);

// EXTERNAL MODULE: ./components/ui/rating-star/index.tsx + 4 modules
var rating_star = __webpack_require__(763);

// EXTERNAL MODULE: ./components/container/image-slider-community/index.tsx + 11 modules
var image_slider_community = __webpack_require__(778);

// EXTERNAL MODULE: ./utils/auth.ts + 1 modules
var auth = __webpack_require__(23);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// EXTERNAL MODULE: ./utils/html.tsx
var html = __webpack_require__(757);

// EXTERNAL MODULE: ./utils/format.ts
var format = __webpack_require__(116);

// EXTERNAL MODULE: ./constants/application/global.ts
var global = __webpack_require__(24);

// EXTERNAL MODULE: ./constants/application/feedable.ts
var application_feedable = __webpack_require__(774);

// EXTERNAL MODULE: ./constants/application/modal.ts
var application_modal = __webpack_require__(749);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// CONCATENATED MODULE: ./components/container/feed-item/style.tsx



var INLINE_STYLE = {
    tooltip: {
        '.icon-item .tooltip': {
            opacity: 0,
            visibility: 'hidden'
        },
        '.icon-item:hover .tooltip': {
            opacity: 1,
            visibility: 'visible'
        }
    }
};
/* harmony default export */ var feed_item_style = ({
    width: '100%',
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    headerWrap: {
        display: variable["display"].flex,
        marginBottom: 15,
        item: {
            container: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        paddingTop: 10,
                        paddingBottom: 10,
                        marginBottom: 10,
                        borderBottom: "1px solid " + variable["colorF0"],
                    }],
                DESKTOP: [{
                        paddingTop: 20,
                        paddingBottom: 10,
                        marginBottom: 10,
                        borderBottom: "1px solid " + variable["colorF0"],
                    }],
                GENERAL: [{
                        background: variable["colorWhite"]
                    }]
            }),
            lastChild: {
                marginBottom: 0
            },
            small: {
                boxShadow: 'none',
                border: "1px solid " + variable["colorB0"],
                marginBottom: 20,
                last: {
                    marginBottom: 0,
                }
            },
            info: {
                container: [
                    layout["a" /* flexContainer */].left, {
                        width: '100%',
                        paddingLeft: 20,
                        paddingRight: 20,
                    }
                ],
                avatar: {
                    width: 40,
                    minWidth: 40,
                    height: 40,
                    borderRadius: 25,
                    marginRight: 10,
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    backgroundColor: variable["colorE5"],
                    cursor: 'pointer',
                    display: variable["display"].block,
                    small: {
                        width: 30,
                        minWidth: 30,
                        height: 30,
                        borderRadius: '50%',
                    }
                },
                username: {
                    paddingRight: 15,
                    marginBottom: 5,
                    textAlign: 'left',
                },
                detail: {
                    flex: 10,
                    display: variable["display"].flex,
                    flexDirection: 'column',
                    textOverflow: 'ellipsis',
                    justifyContent: 'center',
                    username: {
                        fontFamily: variable["fontAvenirDemiBold"],
                        textOverflow: 'ellipsis',
                        whiteSpace: 'nowrap',
                        fontSize: 14,
                        lineHeight: '22px',
                        marginRight: 5,
                        cursor: 'pointer',
                        color: variable["colorBlack"],
                        position: variable["position"].relative
                    },
                    ratingGroup: {
                        display: variable["display"].flex,
                        alignItems: 'center',
                        rating: {
                            marginLeft: -3,
                            marginRight: 5
                        },
                        date: {
                            display: variable["display"].block,
                            lineHeight: '23px',
                            height: 18,
                            color: variable["color97"],
                            cursor: 'pointer'
                        }
                    }
                },
                description: {
                    container: {
                        fontSize: 14,
                        overflow: 'hidden',
                        lineHeight: '22px',
                        textAlign: 'justify',
                        paddingLeft: 20,
                        paddingRight: 20,
                        marginBottom: 10
                    },
                    viewMore: {
                        fontFamily: variable["fontAvenirDemiBold"],
                        fontSize: 15,
                        cursor: 'pointer'
                    },
                }
            },
            image: function (imgUrl) {
                if (imgUrl === void 0) { imgUrl = ''; }
                return {
                    backgroundImage: "url(" + imgUrl + ")",
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                    width: '100%',
                    height: '100%'
                };
            },
            imageContent: {
                maxWidth: '100%',
                minWidth: '100%',
                height: 'auto',
                // marginTop: 10,
                // marginRight: 10,
                // marginBottom: 10,
                // marginLeft: 10,
                display: variable["display"].block,
                objectFit: 'cover',
                cursor: 'pointer'
            },
            fullHeight: {
                height: '100%'
            },
            onePicture: {
                height: 'auto'
            },
            text: {
                color: variable["colorBlack06"]
            },
            inner: {
                width: 15
            },
            likeCommentCount: {
                marginBottom: 10,
                display: variable["display"].flex,
                borderBottom: "1px solid " + variable["colorBlack005"],
                width: '100%',
                alignItems: 'center'
            },
            likeCount: {
                display: variable["display"].flex,
                flex: 1,
                alignItems: 'center'
            },
            likeCountEmpty: {
                flex: 1
            },
            commentCount: {
                fontFamily: variable["fontAvenirRegular"],
                fontSize: 13,
                lineHeight: '30px',
                color: variable["colorBlack06"],
                flex: 1,
                textAlign: 'right',
                paddingLeft: 3,
                paddingRight: 3,
            },
            likeCommentIconGroup: {
                left: {
                    display: variable["display"].flex
                },
                right: {},
                container: {
                    paddingLeft: 13,
                    paddingRight: 13,
                    display: variable["display"].flex,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                },
                icon: {
                    display: variable["display"].flex,
                    alignItems: 'center',
                    justifyContent: 'center',
                    flex: 1,
                    cursor: 'pointer'
                },
                innerIconLike: {
                    width: 22
                },
                innerIconComment: {
                    width: 21,
                    position: variable["position"].relative,
                    top: 3
                },
                innerIconFly: {
                    width: 24,
                    top: 2,
                    position: variable["position"].relative
                },
                innerIconHeart: {
                    width: 16,
                    top: 3,
                    right: -4,
                    position: variable["position"].relative
                }
            },
            countingGroup: {
                paddingLeft: 20,
                paddingRight: 20,
                paddingTop: 10,
                paddingBottom: 10,
                userList: {
                    height: 18,
                    display: variable["display"].inlineFlex,
                    alignItems: 'center',
                    justifyContent: 'center',
                    verticalAlign: 'top',
                    marginRight: 10,
                },
                userItem: function (index) { return ({
                    width: 15,
                    height: 15,
                    backgroundColor: variable["randomColorList"](-1),
                    backgroundSize: 'cover',
                    borderRadius: '50%',
                    display: variable["display"].inlineBlock,
                    marginRight: -7,
                    border: "1px solid " + variable["colorWhite"],
                    cursor: 'pointer',
                }); },
                text: {
                    fontSize: 12,
                    lineHeight: '18px',
                    color: variable["color97"],
                    cursor: 'pointer',
                }
            },
            commentGroup: {
                paddingTop: 10,
                container: {
                    paddingLeft: 20,
                    paddingRight: 20,
                    display: variable["display"].flex,
                    paddingTop: 10,
                },
                contenGroup: {
                    display: variable["display"].block,
                    padding: '4px 10px',
                    background: variable["colorF0"],
                    borderRadius: 15,
                    date: {
                        fontFamily: variable["fontAvenirRegular"],
                        fontSize: 11,
                        color: variable["color75"],
                        marginBottom: 5
                    },
                    comment: {
                        fontFamily: variable["fontAvenirRegular"],
                        fontSize: 14,
                        lineHeight: '22px',
                        color: variable["color2E"],
                        textAlign: 'justify',
                        wordBreak: "break-word"
                    }
                }
            },
            inputCommentGroup: {
                display: variable["display"].flex,
                alignItems: 'center',
                paddingTop: 10,
                paddingBottom: 10,
                marginLeft: 20,
                marginRight: 20,
                avatar: {
                    width: 30,
                    minWidth: 30,
                    height: 30,
                    borderRadius: 25,
                    marginRight: 10,
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    backgroundColor: variable["colorE5"],
                    marginLeft: 0
                },
                input: {
                    height: 30,
                    width: '100%',
                    lineHeight: '30px',
                    textAlign: 'left',
                    color: variable["color4D"],
                    fontSize: 12,
                    fontFamily: variable["fontAvenirMedium"]
                },
                inputText: {
                    width: '100%',
                    border: 'none',
                    outline: 'none',
                    boxShadow: 'none',
                    paddingLeft: 10,
                    paddingRight: 10,
                    height: 28,
                    lineHeight: '28px',
                    fontSize: 12,
                    color: variable["colorBlack"],
                    background: 'transparent',
                    whiteSpace: 'nowrap',
                    maxWidth: '100%',
                    overflow: 'hidden',
                    margin: 0,
                    borderRadius: 0
                },
                sendComment: {
                    display: variable["display"].flex,
                    alignItems: 'center',
                    width: 50,
                    minWidth: 50,
                    fontSize: 12,
                    justifyContent: 'center',
                    cursor: 'pointer',
                    fontFamily: variable["fontAvenirMedium"],
                    lineHeight: '28px',
                    color: variable["colorPink"],
                    textTransform: 'uppercase'
                },
                commentInputGroup: {
                    width: '100%',
                    height: 30,
                    border: "1px solid " + variable["colorD2"],
                    borderRadius: 15,
                    display: variable["display"].flex
                },
            }
        },
        imgProduct: {
            container: {
                height: 40,
                display: variable["display"].flex,
                alignItems: 'center',
                justifyContent: 'center'
            },
            img: {
                height: '100%',
                width: 'auto'
            }
        },
        viewDetail: {
            container: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{}],
                DESKTOP: [{}],
                GENERAL: [{
                        height: 30,
                        border: "1px solid " + variable["colorBlack03"],
                        borderRadius: 5,
                        display: variable["display"].flex,
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingLeft: 10,
                        paddingRight: 10,
                        color: variable["colorBlack08"]
                    }]
            })
        }
    },
    videoContainer: {
        position: variable["position"].relative,
        width: '100%',
        paddingTop: '56.25%',
        cursor: 'pointer',
        thumbnail: function (backgroundImageUrl) { return ({
            backgroundImage: "url(" + backgroundImageUrl,
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            width: '100%',
            height: '100%',
            backgroundColor: variable["colorF7"],
            position: variable["position"].absolute,
            top: 0,
            marginBottom: 10
        }); },
        videoWrap: {
            width: '100%',
            height: '100%',
            marginBottom: 10,
            position: variable["position"].absolute,
            top: 0,
            video: {
                transition: variable["transitionNormal"],
                backgroundColor: variable["colorF7"],
                width: '100%',
                height: '100%'
            }
        }
    },
    icon: {
        position: variable["position"].absolute,
        top: '50%',
        left: '50%',
        color: variable["colorWhite"],
        transform: 'translate(-50%, -50%)',
        zIndex: variable["zIndex2"],
        inner: {
            width: 60,
            height: 60
        }
    },
    imgSlider: {
        display: variable["display"].block,
        whiteSpace: 'nowrap',
        overflow: 'auto hidden',
        img: {
            width: 'auto',
            maxHeight: 300,
            minHeight: 300,
            display: variable["display"].inline,
            marginTop: 10,
            marginBottom: 10,
            marginRight: 20,
            borderRadius: 3,
            boxShadow: variable["shadowBlurSort"]
        }
    },
    customStyleLoading: {
        height: 80
    },
    pictureList: {
        paddingTop: '100%',
        position: variable["position"].relative,
        marginBottom: 10,
        display: variable["display"].block,
        pictureWrap: {
            position: variable["position"].absolute,
            top: 0,
            left: 0,
            width: '100%',
            height: '100%'
        }
    },
    pictures: {
        onePicture: {
            minWith: '100%',
            maxWidth: '100%',
            height: '100%'
        },
        twoPicture: {
            container: function (isHorizontal) { return ({
                display: variable["display"].flex,
                flexDirection: isHorizontal ? 'column' : 'row',
                marginLeft: isHorizontal ? 0 : -1,
                marginRight: isHorizontal ? 0 : -1,
                marginTop: isHorizontal ? -1 : 0,
                marginBottom: isHorizontal ? -1 : 0,
                height: '100%'
            }); },
            horizontal: {
                width: '100%',
                height: '50%',
                paddingTop: 1,
                paddingBottom: 1
            },
            vertical: {
                width: '50%',
                paddingLeft: 1,
                paddingRight: 1
            }
        },
        threePicture: {
            container: function (isHorizontal) { return ({
                display: variable["display"].flex,
                flexDirection: isHorizontal ? 'column' : 'row',
                justifyContent: isHorizontal ? 'center' : 'space-between',
                width: '100%',
                height: '100%'
            }); },
            horizontal: {
                flex: 1,
                marginBottom: 1,
                height: '50%',
            },
            vertical: {
                flex: 1,
                marginRight: 1
            },
            pictureGroup: {
                container: function (isHorizontal) { return ({
                    display: variable["display"].flex,
                    flexDirection: isHorizontal ? 'row' : 'column',
                    flex: 1,
                    justifyContent: isHorizontal ? 'space-between' : 'center',
                    height: isHorizontal ? '' : '100%',
                    marginLeft: isHorizontal ? -1 : 1,
                    marginRight: isHorizontal ? -1 : 0,
                    marginTop: isHorizontal ? 1 : -1,
                    marginBottom: isHorizontal ? 0 : -1,
                }); },
                horizontal: {
                    flex: 1,
                    marginLeft: 1,
                    marginRight: 1
                },
                vertical: {
                    width: '100%',
                    height: '50%',
                    flex: 1,
                    paddingBottom: 1,
                    paddingTop: 1
                }
            },
        },
        fourPicture: {
            container: function (isHorizontal) { return ({
                display: variable["display"].flex,
                flexDirection: isHorizontal ? 'column' : 'row',
                justifyContent: isHorizontal ? 'center' : 'space-between',
                width: '100%',
                height: '100%'
            }); },
            horizontal: {
                flex: 2,
                marginBottom: 2
            },
            vertical: {
                flex: 2,
                border: "1px solid " + variable["colorWhite"]
            },
            pictureGroup: {
                container: function (isHorizontal) { return ({
                    display: variable["display"].flex,
                    flexDirection: isHorizontal ? 'row' : 'column',
                    flex: 1,
                    justifyContent: isHorizontal ? 'space-between' : 'center',
                    height: isHorizontal ? '' : '100%',
                    marginLeft: isHorizontal ? -1 : 0,
                    marginRight: isHorizontal ? -1 : 0,
                }); },
                horizontal: {
                    flex: 1,
                    marginLeft: 1,
                    marginRight: 1
                },
                vertical: {
                    width: '100%',
                    height: '33.33%',
                    flex: 1,
                    border: "1px solid " + variable["colorWhite"]
                }
            },
        },
        viewMore: {
            container: function (isHorizontal) { return ({
                position: variable["position"].relative,
                flex: isHorizontal ? 1 : '',
                width: '100%',
                marginLeft: isHorizontal ? 1 : 0,
                marginRight: isHorizontal ? 1 : 0,
                height: isHorizontal ? 'auto' : '33.33%',
                paddingTop: isHorizontal ? '' : 1,
                paddingBottom: isHorizontal ? '' : 1,
                cursor: 'pointer'
            }); },
            imgViewMore: function (isHorizontal) {
                return !isHorizontal
                    ? {
                        paddingTop: 0,
                        paddingRight: 0,
                        paddingBottom: 0,
                        paddingLeft: 0,
                        height: '100%'
                    }
                    : {
                        width: '100%',
                        height: '100%',
                        margin: 0,
                        padding: 0
                    };
            },
            num: {
                position: variable["position"].absolute,
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                fontSize: 36,
                fontFamily: variable["fontAvenirDemiBold"],
                color: variable["colorWhite"],
                zIndex: variable["zIndex9"],
            },
            bg: {
                position: variable["position"].absolute,
                top: 0,
                width: '100%',
                height: '100%',
                zIndex: variable["zIndex8"],
                backgroundColor: variable["colorBlack04"]
            }
        }
    },
    tooltip: {
        position: variable["position"].absolute,
        top: -3,
        right: -10,
        transform: 'translateX(100%)',
        group: {
            height: '100%',
            position: variable["position"].relative,
            text: {
                padding: '3px 8px',
                color: variable["colorWhite"],
                background: variable["colorBlack"],
                borderRadius: 3,
                boxShadow: variable["shadowBlurSort"],
                whiteSpace: 'nowrap',
                lineHeight: '20px',
                fontSize: 12
            },
            icon: {
                position: variable["position"].absolute,
                top: 7,
                left: -5,
                height: 5,
                width: 5,
                borderWidth: 6,
                borderStyle: 'solid',
                borderColor: variable["colorTransparent"] + " " + variable["colorBlack"] + " " + variable["colorTransparent"] + "  " + variable["colorTransparent"],
                transform: 'translateX(-50%)'
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEtBQUssTUFBTSxNQUFNLHVCQUF1QixDQUFDO0FBQ2hELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUV6RCxNQUFNLENBQUMsSUFBTSxZQUFZLEdBQUc7SUFDMUIsT0FBTyxFQUFFO1FBQ1AscUJBQXFCLEVBQUU7WUFDckIsT0FBTyxFQUFFLENBQUM7WUFDVixVQUFVLEVBQUUsUUFBUTtTQUNyQjtRQUVELDJCQUEyQixFQUFFO1lBQzNCLE9BQU8sRUFBRSxDQUFDO1lBQ1YsVUFBVSxFQUFFLFNBQVM7U0FDdEI7S0FDRjtDQUNGLENBQUM7QUFFRixlQUFlO0lBQ2IsS0FBSyxFQUFFLE1BQU07SUFDYixVQUFVLEVBQUUsRUFBRTtJQUNkLFlBQVksRUFBRSxFQUFFO0lBQ2hCLGFBQWEsRUFBRSxFQUFFO0lBQ2pCLFdBQVcsRUFBRSxFQUFFO0lBRWYsVUFBVSxFQUFFO1FBQ1YsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUM5QixZQUFZLEVBQUUsRUFBRTtRQUVoQixJQUFJLEVBQUU7WUFDSixTQUFTLEVBQUUsWUFBWSxDQUFDO2dCQUN0QixNQUFNLEVBQUUsQ0FBQzt3QkFDUCxVQUFVLEVBQUUsRUFBRTt3QkFDZCxhQUFhLEVBQUUsRUFBRTt3QkFDakIsWUFBWSxFQUFFLEVBQUU7d0JBQ2hCLFlBQVksRUFBRSxlQUFhLFFBQVEsQ0FBQyxPQUFTO3FCQUM5QyxDQUFDO2dCQUVGLE9BQU8sRUFBRSxDQUFDO3dCQUNSLFVBQVUsRUFBRSxFQUFFO3dCQUNkLGFBQWEsRUFBRSxFQUFFO3dCQUNqQixZQUFZLEVBQUUsRUFBRTt3QkFDaEIsWUFBWSxFQUFFLGVBQWEsUUFBUSxDQUFDLE9BQVM7cUJBQzlDLENBQUM7Z0JBRUYsT0FBTyxFQUFFLENBQUM7d0JBQ1IsVUFBVSxFQUFFLFFBQVEsQ0FBQyxVQUFVO3FCQUNoQyxDQUFDO2FBQ0gsQ0FBQztZQUVGLFNBQVMsRUFBRTtnQkFDVCxZQUFZLEVBQUUsQ0FBQzthQUNoQjtZQUVELEtBQUssRUFBRTtnQkFDTCxTQUFTLEVBQUUsTUFBTTtnQkFDakIsTUFBTSxFQUFFLGVBQWEsUUFBUSxDQUFDLE9BQVM7Z0JBQ3ZDLFlBQVksRUFBRSxFQUFFO2dCQUVoQixJQUFJLEVBQUU7b0JBQ0osWUFBWSxFQUFFLENBQUM7aUJBQ2hCO2FBQ0Y7WUFFRCxJQUFJLEVBQUU7Z0JBQ0osU0FBUyxFQUFFO29CQUNULE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFO3dCQUN6QixLQUFLLEVBQUUsTUFBTTt3QkFDYixXQUFXLEVBQUUsRUFBRTt3QkFDZixZQUFZLEVBQUUsRUFBRTtxQkFDakI7aUJBQUM7Z0JBRUosTUFBTSxFQUFFO29CQUNOLEtBQUssRUFBRSxFQUFFO29CQUNULFFBQVEsRUFBRSxFQUFFO29CQUNaLE1BQU0sRUFBRSxFQUFFO29CQUNWLFlBQVksRUFBRSxFQUFFO29CQUNoQixXQUFXLEVBQUUsRUFBRTtvQkFDZixrQkFBa0IsRUFBRSxRQUFRO29CQUM1QixjQUFjLEVBQUUsT0FBTztvQkFDdkIsZUFBZSxFQUFFLFFBQVEsQ0FBQyxPQUFPO29CQUNqQyxNQUFNLEVBQUUsU0FBUztvQkFDakIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztvQkFFL0IsS0FBSyxFQUFFO3dCQUNMLEtBQUssRUFBRSxFQUFFO3dCQUNULFFBQVEsRUFBRSxFQUFFO3dCQUNaLE1BQU0sRUFBRSxFQUFFO3dCQUNWLFlBQVksRUFBRSxLQUFLO3FCQUNwQjtpQkFDRjtnQkFFRCxRQUFRLEVBQUU7b0JBQ1IsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLFlBQVksRUFBRSxDQUFDO29CQUNmLFNBQVMsRUFBRSxNQUFNO2lCQUNsQjtnQkFFRCxNQUFNLEVBQUU7b0JBQ04sSUFBSSxFQUFFLEVBQUU7b0JBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtvQkFDOUIsYUFBYSxFQUFFLFFBQVE7b0JBQ3ZCLFlBQVksRUFBRSxVQUFVO29CQUN4QixjQUFjLEVBQUUsUUFBUTtvQkFFeEIsUUFBUSxFQUFFO3dCQUNSLFVBQVUsRUFBRSxRQUFRLENBQUMsa0JBQWtCO3dCQUN2QyxZQUFZLEVBQUUsVUFBVTt3QkFDeEIsVUFBVSxFQUFFLFFBQVE7d0JBQ3BCLFFBQVEsRUFBRSxFQUFFO3dCQUNaLFVBQVUsRUFBRSxNQUFNO3dCQUNsQixXQUFXLEVBQUUsQ0FBQzt3QkFDZCxNQUFNLEVBQUUsU0FBUzt3QkFDakIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO3dCQUMxQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO3FCQUNyQztvQkFFRCxXQUFXLEVBQUU7d0JBQ1gsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTt3QkFDOUIsVUFBVSxFQUFFLFFBQVE7d0JBRXBCLE1BQU0sRUFBRTs0QkFDTixVQUFVLEVBQUUsQ0FBQyxDQUFDOzRCQUNkLFdBQVcsRUFBRSxDQUFDO3lCQUNmO3dCQUVELElBQUksRUFBRTs0QkFDSixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLOzRCQUMvQixVQUFVLEVBQUUsTUFBTTs0QkFDbEIsTUFBTSxFQUFFLEVBQUU7NEJBQ1YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPOzRCQUN2QixNQUFNLEVBQUUsU0FBUzt5QkFDbEI7cUJBQ0Y7aUJBQ0Y7Z0JBRUQsV0FBVyxFQUFFO29CQUNYLFNBQVMsRUFBRTt3QkFDVCxRQUFRLEVBQUUsRUFBRTt3QkFDWixRQUFRLEVBQUUsUUFBUTt3QkFDbEIsVUFBVSxFQUFFLE1BQU07d0JBQ2xCLFNBQVMsRUFBRSxTQUFTO3dCQUNwQixXQUFXLEVBQUUsRUFBRTt3QkFDZixZQUFZLEVBQUUsRUFBRTt3QkFDaEIsWUFBWSxFQUFFLEVBQUU7cUJBQ2pCO29CQUVELFFBQVEsRUFBRTt3QkFDUixVQUFVLEVBQUUsUUFBUSxDQUFDLGtCQUFrQjt3QkFDdkMsUUFBUSxFQUFFLEVBQUU7d0JBQ1osTUFBTSxFQUFFLFNBQVM7cUJBQ2xCO2lCQUNGO2FBQ0Y7WUFFRCxLQUFLLEVBQUUsVUFBQyxNQUFXO2dCQUFYLHVCQUFBLEVBQUEsV0FBVztnQkFDakIsTUFBTSxDQUFDO29CQUNMLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztvQkFDakMsa0JBQWtCLEVBQUUsUUFBUTtvQkFDNUIsY0FBYyxFQUFFLE9BQU87b0JBQ3ZCLGdCQUFnQixFQUFFLFdBQVc7b0JBQzdCLEtBQUssRUFBRSxNQUFNO29CQUNiLE1BQU0sRUFBRSxNQUFNO2lCQUNmLENBQUE7WUFDSCxDQUFDO1lBRUQsWUFBWSxFQUFFO2dCQUNaLFFBQVEsRUFBRSxNQUFNO2dCQUNoQixRQUFRLEVBQUUsTUFBTTtnQkFDaEIsTUFBTSxFQUFFLE1BQU07Z0JBQ2QsaUJBQWlCO2dCQUNqQixtQkFBbUI7Z0JBQ25CLG9CQUFvQjtnQkFDcEIsa0JBQWtCO2dCQUNsQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO2dCQUMvQixTQUFTLEVBQUUsT0FBTztnQkFDbEIsTUFBTSxFQUFFLFNBQVM7YUFDbEI7WUFFRCxVQUFVLEVBQUU7Z0JBQ1YsTUFBTSxFQUFFLE1BQU07YUFDZjtZQUVELFVBQVUsRUFBRTtnQkFDVixNQUFNLEVBQUUsTUFBTTthQUNmO1lBRUQsSUFBSSxFQUFFO2dCQUNKLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTthQUM3QjtZQUVELEtBQUssRUFBRTtnQkFDTCxLQUFLLEVBQUUsRUFBRTthQUNWO1lBRUQsZ0JBQWdCLEVBQUU7Z0JBQ2hCLFlBQVksRUFBRSxFQUFFO2dCQUNoQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixZQUFZLEVBQUUsZUFBYSxRQUFRLENBQUMsYUFBZTtnQkFDbkQsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsVUFBVSxFQUFFLFFBQVE7YUFDckI7WUFFRCxTQUFTLEVBQUU7Z0JBQ1QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsSUFBSSxFQUFFLENBQUM7Z0JBQ1AsVUFBVSxFQUFFLFFBQVE7YUFDckI7WUFFRCxjQUFjLEVBQUU7Z0JBQ2QsSUFBSSxFQUFFLENBQUM7YUFDUjtZQUVELFlBQVksRUFBRTtnQkFDWixVQUFVLEVBQUUsUUFBUSxDQUFDLGlCQUFpQjtnQkFDdEMsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osVUFBVSxFQUFFLE1BQU07Z0JBQ2xCLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTtnQkFDNUIsSUFBSSxFQUFFLENBQUM7Z0JBQ1AsU0FBUyxFQUFFLE9BQU87Z0JBQ2xCLFdBQVcsRUFBRSxDQUFDO2dCQUNkLFlBQVksRUFBRSxDQUFDO2FBQ2hCO1lBRUQsb0JBQW9CLEVBQUU7Z0JBQ3BCLElBQUksRUFBRTtvQkFDSixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2lCQUMvQjtnQkFFRCxLQUFLLEVBQUUsRUFBRTtnQkFFVCxTQUFTLEVBQUU7b0JBQ1QsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7b0JBQzlCLFVBQVUsRUFBRSxRQUFRO29CQUNwQixjQUFjLEVBQUUsZUFBZTtpQkFDaEM7Z0JBRUQsSUFBSSxFQUFFO29CQUNKLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7b0JBQzlCLFVBQVUsRUFBRSxRQUFRO29CQUNwQixjQUFjLEVBQUUsUUFBUTtvQkFDeEIsSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLFNBQVM7aUJBQ2xCO2dCQUVELGFBQWEsRUFBRTtvQkFDYixLQUFLLEVBQUUsRUFBRTtpQkFDVjtnQkFFRCxnQkFBZ0IsRUFBRTtvQkFDaEIsS0FBSyxFQUFFLEVBQUU7b0JBQ1QsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtvQkFDcEMsR0FBRyxFQUFFLENBQUM7aUJBQ1A7Z0JBRUQsWUFBWSxFQUFFO29CQUNaLEtBQUssRUFBRSxFQUFFO29CQUNULEdBQUcsRUFBRSxDQUFDO29CQUNOLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7aUJBQ3JDO2dCQUVELGNBQWMsRUFBRTtvQkFDZCxLQUFLLEVBQUUsRUFBRTtvQkFDVCxHQUFHLEVBQUUsQ0FBQztvQkFDTixLQUFLLEVBQUUsQ0FBQyxDQUFDO29CQUNULFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7aUJBQ3JDO2FBQ0Y7WUFFRCxhQUFhLEVBQUU7Z0JBQ2IsV0FBVyxFQUFFLEVBQUU7Z0JBQ2YsWUFBWSxFQUFFLEVBQUU7Z0JBQ2hCLFVBQVUsRUFBRSxFQUFFO2dCQUNkLGFBQWEsRUFBRSxFQUFFO2dCQUVqQixRQUFRLEVBQUU7b0JBQ1IsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBVTtvQkFDcEMsVUFBVSxFQUFFLFFBQVE7b0JBQ3BCLGNBQWMsRUFBRSxRQUFRO29CQUN4QixhQUFhLEVBQUUsS0FBSztvQkFDcEIsV0FBVyxFQUFFLEVBQUU7aUJBQ2hCO2dCQUVELFFBQVEsRUFBRSxVQUFDLEtBQUssSUFBSyxPQUFBLENBQUM7b0JBQ3BCLEtBQUssRUFBRSxFQUFFO29CQUNULE1BQU0sRUFBRSxFQUFFO29CQUNWLGVBQWUsRUFBRSxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUM3QyxjQUFjLEVBQUUsT0FBTztvQkFDdkIsWUFBWSxFQUFFLEtBQUs7b0JBQ25CLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLFdBQVc7b0JBQ3JDLFdBQVcsRUFBRSxDQUFDLENBQUM7b0JBQ2YsTUFBTSxFQUFFLGVBQWEsUUFBUSxDQUFDLFVBQVk7b0JBQzFDLE1BQU0sRUFBRSxTQUFTO2lCQUNsQixDQUFDLEVBVm1CLENBVW5CO2dCQUVGLElBQUksRUFBRTtvQkFDSixRQUFRLEVBQUUsRUFBRTtvQkFDWixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO29CQUN2QixNQUFNLEVBQUUsU0FBUztpQkFDbEI7YUFDRjtZQUVELFlBQVksRUFBRTtnQkFDWixVQUFVLEVBQUUsRUFBRTtnQkFFZCxTQUFTLEVBQUU7b0JBQ1QsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7b0JBQzlCLFVBQVUsRUFBRSxFQUFFO2lCQUNmO2dCQUVELFdBQVcsRUFBRTtvQkFDWCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO29CQUMvQixPQUFPLEVBQUUsVUFBVTtvQkFDbkIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxPQUFPO29CQUM1QixZQUFZLEVBQUUsRUFBRTtvQkFFaEIsSUFBSSxFQUFFO3dCQUNKLFVBQVUsRUFBRSxRQUFRLENBQUMsaUJBQWlCO3dCQUN0QyxRQUFRLEVBQUUsRUFBRTt3QkFDWixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87d0JBQ3ZCLFlBQVksRUFBRSxDQUFDO3FCQUNoQjtvQkFFRCxPQUFPLEVBQUU7d0JBQ1AsVUFBVSxFQUFFLFFBQVEsQ0FBQyxpQkFBaUI7d0JBQ3RDLFFBQVEsRUFBRSxFQUFFO3dCQUNaLFVBQVUsRUFBRSxNQUFNO3dCQUNsQixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87d0JBQ3ZCLFNBQVMsRUFBRSxTQUFTO3dCQUNwQixTQUFTLEVBQUUsWUFBWTtxQkFDeEI7aUJBQ0Y7YUFDRjtZQUVELGlCQUFpQixFQUFFO2dCQUNqQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixVQUFVLEVBQUUsUUFBUTtnQkFDcEIsVUFBVSxFQUFFLEVBQUU7Z0JBQ2QsYUFBYSxFQUFFLEVBQUU7Z0JBQ2pCLFVBQVUsRUFBRSxFQUFFO2dCQUNkLFdBQVcsRUFBRSxFQUFFO2dCQUVmLE1BQU0sRUFBRTtvQkFDTixLQUFLLEVBQUUsRUFBRTtvQkFDVCxRQUFRLEVBQUUsRUFBRTtvQkFDWixNQUFNLEVBQUUsRUFBRTtvQkFDVixZQUFZLEVBQUUsRUFBRTtvQkFDaEIsV0FBVyxFQUFFLEVBQUU7b0JBQ2Ysa0JBQWtCLEVBQUUsUUFBUTtvQkFDNUIsY0FBYyxFQUFFLE9BQU87b0JBQ3ZCLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztvQkFDakMsVUFBVSxFQUFFLENBQUM7aUJBQ2Q7Z0JBRUQsS0FBSyxFQUFFO29CQUNMLE1BQU0sRUFBRSxFQUFFO29CQUNWLEtBQUssRUFBRSxNQUFNO29CQUNiLFVBQVUsRUFBRSxNQUFNO29CQUNsQixTQUFTLEVBQUUsTUFBTTtvQkFDakIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO29CQUN2QixRQUFRLEVBQUUsRUFBRTtvQkFDWixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtpQkFDdEM7Z0JBRUQsU0FBUyxFQUFFO29CQUNULEtBQUssRUFBRSxNQUFNO29CQUNiLE1BQU0sRUFBRSxNQUFNO29CQUNkLE9BQU8sRUFBRSxNQUFNO29CQUNmLFNBQVMsRUFBRSxNQUFNO29CQUNqQixXQUFXLEVBQUUsRUFBRTtvQkFDZixZQUFZLEVBQUUsRUFBRTtvQkFDaEIsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsVUFBVSxFQUFFLE1BQU07b0JBQ2xCLFFBQVEsRUFBRSxFQUFFO29CQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDMUIsVUFBVSxFQUFFLGFBQWE7b0JBQ3pCLFVBQVUsRUFBRSxRQUFRO29CQUNwQixRQUFRLEVBQUUsTUFBTTtvQkFDaEIsUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLE1BQU0sRUFBRSxDQUFDO29CQUNULFlBQVksRUFBRSxDQUFDO2lCQUNoQjtnQkFFRCxXQUFXLEVBQUU7b0JBQ1gsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtvQkFDOUIsVUFBVSxFQUFFLFFBQVE7b0JBQ3BCLEtBQUssRUFBRSxFQUFFO29CQUNULFFBQVEsRUFBRSxFQUFFO29CQUNaLFFBQVEsRUFBRSxFQUFFO29CQUNaLGNBQWMsRUFBRSxRQUFRO29CQUN4QixNQUFNLEVBQUUsU0FBUztvQkFDakIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7b0JBQ3JDLFVBQVUsRUFBRSxNQUFNO29CQUNsQixLQUFLLEVBQUUsUUFBUSxDQUFDLFNBQVM7b0JBQ3pCLGFBQWEsRUFBRSxXQUFXO2lCQUMzQjtnQkFFRCxpQkFBaUIsRUFBRTtvQkFDakIsS0FBSyxFQUFFLE1BQU07b0JBQ2IsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsTUFBTSxFQUFFLGVBQWEsUUFBUSxDQUFDLE9BQVM7b0JBQ3ZDLFlBQVksRUFBRSxFQUFFO29CQUNoQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2lCQUMvQjthQUNGO1NBQ0Y7UUFFRCxVQUFVLEVBQUU7WUFDVixTQUFTLEVBQUU7Z0JBQ1QsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsVUFBVSxFQUFFLFFBQVE7Z0JBQ3BCLGNBQWMsRUFBRSxRQUFRO2FBQ3pCO1lBRUQsR0FBRyxFQUFFO2dCQUNILE1BQU0sRUFBRSxNQUFNO2dCQUNkLEtBQUssRUFBRSxNQUFNO2FBQ2Q7U0FDRjtRQUVELFVBQVUsRUFBRTtZQUNWLFNBQVMsRUFBRSxZQUFZLENBQUM7Z0JBQ3RCLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQztnQkFDWixPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUM7Z0JBQ2IsT0FBTyxFQUFFLENBQUM7d0JBQ1IsTUFBTSxFQUFFLEVBQUU7d0JBQ1YsTUFBTSxFQUFFLGVBQWEsUUFBUSxDQUFDLFlBQWM7d0JBQzVDLFlBQVksRUFBRSxDQUFDO3dCQUNmLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7d0JBQzlCLFVBQVUsRUFBRSxRQUFRO3dCQUNwQixjQUFjLEVBQUUsUUFBUTt3QkFDeEIsV0FBVyxFQUFFLEVBQUU7d0JBQ2YsWUFBWSxFQUFFLEVBQUU7d0JBQ2hCLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTtxQkFDN0IsQ0FBQzthQUNILENBQUM7U0FDSDtLQUNGO0lBRUQsY0FBYyxFQUFFO1FBQ2QsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtRQUNwQyxLQUFLLEVBQUUsTUFBTTtRQUNiLFVBQVUsRUFBRSxRQUFRO1FBQ3BCLE1BQU0sRUFBRSxTQUFTO1FBRWpCLFNBQVMsRUFBRSxVQUFDLGtCQUEwQixJQUFLLE9BQUEsQ0FBQztZQUMxQyxlQUFlLEVBQUUsU0FBTyxrQkFBb0I7WUFDNUMsa0JBQWtCLEVBQUUsUUFBUTtZQUM1QixjQUFjLEVBQUUsT0FBTztZQUN2QixLQUFLLEVBQUUsTUFBTTtZQUNiLE1BQU0sRUFBRSxNQUFNO1lBQ2QsZUFBZSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1lBQ2pDLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDcEMsR0FBRyxFQUFFLENBQUM7WUFDTixZQUFZLEVBQUUsRUFBRTtTQUNqQixDQUFDLEVBVnlDLENBVXpDO1FBRUYsU0FBUyxFQUFFO1lBQ1QsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsTUFBTTtZQUNkLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDcEMsR0FBRyxFQUFFLENBQUM7WUFFTixLQUFLLEVBQUU7Z0JBQ0wsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztnQkFDakMsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsTUFBTSxFQUFFLE1BQU07YUFDZjtTQUNGO0tBQ0Y7SUFFRCxJQUFJLEVBQUU7UUFDSixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1FBQ3BDLEdBQUcsRUFBRSxLQUFLO1FBQ1YsSUFBSSxFQUFFLEtBQUs7UUFDWCxLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7UUFDMUIsU0FBUyxFQUFFLHVCQUF1QjtRQUNsQyxNQUFNLEVBQUUsUUFBUSxDQUFDLE9BQU87UUFFeEIsS0FBSyxFQUFFO1lBQ0wsS0FBSyxFQUFFLEVBQUU7WUFDVCxNQUFNLEVBQUUsRUFBRTtTQUNYO0tBQ0Y7SUFFRCxTQUFTLEVBQUU7UUFDVCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1FBQy9CLFVBQVUsRUFBRSxRQUFRO1FBQ3BCLFFBQVEsRUFBRSxhQUFhO1FBRXZCLEdBQUcsRUFBRTtZQUNILEtBQUssRUFBRSxNQUFNO1lBQ2IsU0FBUyxFQUFFLEdBQUc7WUFDZCxTQUFTLEVBQUUsR0FBRztZQUNkLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU07WUFDaEMsU0FBUyxFQUFFLEVBQUU7WUFDYixZQUFZLEVBQUUsRUFBRTtZQUNoQixXQUFXLEVBQUUsRUFBRTtZQUNmLFlBQVksRUFBRSxDQUFDO1lBQ2YsU0FBUyxFQUFFLFFBQVEsQ0FBQyxjQUFjO1NBQ25DO0tBQ0Y7SUFFRCxrQkFBa0IsRUFBRTtRQUNsQixNQUFNLEVBQUUsRUFBRTtLQUNYO0lBRUQsV0FBVyxFQUFFO1FBQ1gsVUFBVSxFQUFFLE1BQU07UUFDbEIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtRQUNwQyxZQUFZLEVBQUUsRUFBRTtRQUNoQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1FBRS9CLFdBQVcsRUFBRTtZQUNYLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDcEMsR0FBRyxFQUFFLENBQUM7WUFDTixJQUFJLEVBQUUsQ0FBQztZQUNQLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLE1BQU07U0FDZjtLQUNGO0lBRUQsUUFBUSxFQUFFO1FBQ1IsVUFBVSxFQUFFO1lBQ1YsT0FBTyxFQUFFLE1BQU07WUFDZixRQUFRLEVBQUUsTUFBTTtZQUNoQixNQUFNLEVBQUUsTUFBTTtTQUNmO1FBRUQsVUFBVSxFQUFFO1lBQ1YsU0FBUyxFQUFFLFVBQUMsWUFBWSxJQUFLLE9BQUEsQ0FBQztnQkFDNUIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsYUFBYSxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxLQUFLO2dCQUM5QyxVQUFVLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDakMsV0FBVyxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xDLFNBQVMsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNoQyxZQUFZLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbkMsTUFBTSxFQUFFLE1BQU07YUFDZixDQUFDLEVBUjJCLENBUTNCO1lBRUYsVUFBVSxFQUFFO2dCQUNWLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxLQUFLO2dCQUNiLFVBQVUsRUFBRSxDQUFDO2dCQUNiLGFBQWEsRUFBRSxDQUFDO2FBQ2pCO1lBRUQsUUFBUSxFQUFFO2dCQUNSLEtBQUssRUFBRSxLQUFLO2dCQUNaLFdBQVcsRUFBRSxDQUFDO2dCQUNkLFlBQVksRUFBRSxDQUFDO2FBQ2hCO1NBQ0Y7UUFFRCxZQUFZLEVBQUU7WUFDWixTQUFTLEVBQUUsVUFBQyxZQUFZLElBQUssT0FBQSxDQUFDO2dCQUM1QixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixhQUFhLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUs7Z0JBQzlDLGNBQWMsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsZUFBZTtnQkFDekQsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsTUFBTSxFQUFFLE1BQU07YUFDZixDQUFDLEVBTjJCLENBTTNCO1lBRUYsVUFBVSxFQUFFO2dCQUNWLElBQUksRUFBRSxDQUFDO2dCQUNQLFlBQVksRUFBRSxDQUFDO2dCQUNmLE1BQU0sRUFBRSxLQUFLO2FBQ2Q7WUFFRCxRQUFRLEVBQUU7Z0JBQ1IsSUFBSSxFQUFFLENBQUM7Z0JBQ1AsV0FBVyxFQUFFLENBQUM7YUFDZjtZQUVELFlBQVksRUFBRTtnQkFDWixTQUFTLEVBQUUsVUFBQyxZQUFZLElBQUssT0FBQSxDQUFDO29CQUM1QixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO29CQUM5QixhQUFhLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLFFBQVE7b0JBQzlDLElBQUksRUFBRSxDQUFDO29CQUNQLGNBQWMsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsUUFBUTtvQkFDekQsTUFBTSxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNO29CQUNsQyxVQUFVLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDakMsV0FBVyxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ2xDLFNBQVMsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNoQyxZQUFZLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDcEMsQ0FBQyxFQVYyQixDQVUzQjtnQkFFRixVQUFVLEVBQUU7b0JBQ1YsSUFBSSxFQUFFLENBQUM7b0JBQ1AsVUFBVSxFQUFFLENBQUM7b0JBQ2IsV0FBVyxFQUFFLENBQUM7aUJBQ2Y7Z0JBRUQsUUFBUSxFQUFFO29CQUNSLEtBQUssRUFBRSxNQUFNO29CQUNiLE1BQU0sRUFBRSxLQUFLO29CQUNiLElBQUksRUFBRSxDQUFDO29CQUNQLGFBQWEsRUFBRSxDQUFDO29CQUNoQixVQUFVLEVBQUUsQ0FBQztpQkFDZDthQUNGO1NBQ0Y7UUFFRCxXQUFXLEVBQUU7WUFDWCxTQUFTLEVBQUUsVUFBQyxZQUFZLElBQUssT0FBQSxDQUFDO2dCQUM1QixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixhQUFhLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUs7Z0JBQzlDLGNBQWMsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsZUFBZTtnQkFDekQsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsTUFBTSxFQUFFLE1BQU07YUFDZixDQUFDLEVBTjJCLENBTTNCO1lBRUYsVUFBVSxFQUFFO2dCQUNWLElBQUksRUFBRSxDQUFDO2dCQUNQLFlBQVksRUFBRSxDQUFDO2FBQ2hCO1lBRUQsUUFBUSxFQUFFO2dCQUNSLElBQUksRUFBRSxDQUFDO2dCQUNQLE1BQU0sRUFBRSxlQUFhLFFBQVEsQ0FBQyxVQUFZO2FBQzNDO1lBRUQsWUFBWSxFQUFFO2dCQUNaLFNBQVMsRUFBRSxVQUFDLFlBQVksSUFBSyxPQUFBLENBQUM7b0JBQzVCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7b0JBQzlCLGFBQWEsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsUUFBUTtvQkFDOUMsSUFBSSxFQUFFLENBQUM7b0JBQ1AsY0FBYyxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxRQUFRO29CQUN6RCxNQUFNLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU07b0JBQ2xDLFVBQVUsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNqQyxXQUFXLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDbkMsQ0FBQyxFQVIyQixDQVEzQjtnQkFFRixVQUFVLEVBQUU7b0JBQ1YsSUFBSSxFQUFFLENBQUM7b0JBQ1AsVUFBVSxFQUFFLENBQUM7b0JBQ2IsV0FBVyxFQUFFLENBQUM7aUJBQ2Y7Z0JBRUQsUUFBUSxFQUFFO29CQUNSLEtBQUssRUFBRSxNQUFNO29CQUNiLE1BQU0sRUFBRSxRQUFRO29CQUNoQixJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsZUFBYSxRQUFRLENBQUMsVUFBWTtpQkFDM0M7YUFDRjtTQUNGO1FBRUQsUUFBUSxFQUFFO1lBQ1IsU0FBUyxFQUFFLFVBQUMsWUFBWSxJQUFLLE9BQUEsQ0FBQztnQkFDNUIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtnQkFDcEMsSUFBSSxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUMzQixLQUFLLEVBQUUsTUFBTTtnQkFDYixVQUFVLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hDLFdBQVcsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDakMsTUFBTSxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxRQUFRO2dCQUN4QyxVQUFVLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pDLGFBQWEsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDcEMsTUFBTSxFQUFFLFNBQVM7YUFDbEIsQ0FBQyxFQVYyQixDQVUzQjtZQUVGLFdBQVcsRUFBRSxVQUFDLFlBQVk7Z0JBQ3hCLE1BQU0sQ0FBQyxDQUFDLFlBQVk7b0JBQ2xCLENBQUMsQ0FBQzt3QkFDQSxVQUFVLEVBQUUsQ0FBQzt3QkFDYixZQUFZLEVBQUUsQ0FBQzt3QkFDZixhQUFhLEVBQUUsQ0FBQzt3QkFDaEIsV0FBVyxFQUFFLENBQUM7d0JBQ2QsTUFBTSxFQUFFLE1BQU07cUJBQ2Y7b0JBQ0QsQ0FBQyxDQUFDO3dCQUNBLEtBQUssRUFBRSxNQUFNO3dCQUNiLE1BQU0sRUFBRSxNQUFNO3dCQUNkLE1BQU0sRUFBRSxDQUFDO3dCQUNULE9BQU8sRUFBRSxDQUFDO3FCQUNYLENBQUE7WUFDTCxDQUFDO1lBRUQsR0FBRyxFQUFFO2dCQUNILFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLEdBQUcsRUFBRSxLQUFLO2dCQUNWLElBQUksRUFBRSxLQUFLO2dCQUNYLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxRQUFRLENBQUMsa0JBQWtCO2dCQUN2QyxLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7Z0JBQzFCLE1BQU0sRUFBRSxRQUFRLENBQUMsT0FBTzthQUN6QjtZQUVELEVBQUUsRUFBRTtnQkFDRixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxHQUFHLEVBQUUsQ0FBQztnQkFDTixLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsTUFBTTtnQkFDZCxNQUFNLEVBQUUsUUFBUSxDQUFDLE9BQU87Z0JBQ3hCLGVBQWUsRUFBRSxRQUFRLENBQUMsWUFBWTthQUN2QztTQUNGO0tBQ0Y7SUFFRCxPQUFPLEVBQUU7UUFDUCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1FBQ3BDLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDUCxLQUFLLEVBQUUsQ0FBQyxFQUFFO1FBQ1YsU0FBUyxFQUFFLGtCQUFrQjtRQUU3QixLQUFLLEVBQUU7WUFDTCxNQUFNLEVBQUUsTUFBTTtZQUNkLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFFcEMsSUFBSSxFQUFFO2dCQUNKLE9BQU8sRUFBRSxTQUFTO2dCQUNsQixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7Z0JBQzFCLFVBQVUsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDL0IsWUFBWSxFQUFFLENBQUM7Z0JBQ2YsU0FBUyxFQUFFLFFBQVEsQ0FBQyxjQUFjO2dCQUNsQyxVQUFVLEVBQUUsUUFBUTtnQkFDcEIsVUFBVSxFQUFFLE1BQU07Z0JBQ2xCLFFBQVEsRUFBRSxFQUFFO2FBQ2I7WUFFRCxJQUFJLEVBQUU7Z0JBQ0osUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtnQkFDcEMsR0FBRyxFQUFFLENBQUM7Z0JBQ04sSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDUixNQUFNLEVBQUUsQ0FBQztnQkFDVCxLQUFLLEVBQUUsQ0FBQztnQkFDUixXQUFXLEVBQUUsQ0FBQztnQkFDZCxXQUFXLEVBQUUsT0FBTztnQkFDcEIsV0FBVyxFQUFLLFFBQVEsQ0FBQyxnQkFBZ0IsU0FBSSxRQUFRLENBQUMsVUFBVSxTQUFJLFFBQVEsQ0FBQyxnQkFBZ0IsVUFBSyxRQUFRLENBQUMsZ0JBQWtCO2dCQUM3SCxTQUFTLEVBQUUsa0JBQWtCO2FBQzlCO1NBQ0Y7S0FDRjtDQUNLLENBQUMifQ==
// CONCATENATED MODULE: ./components/container/feed-item/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};




















var shareFacebookLink = '//www.facebook.com/sharer.php?u=';
var itemStyle = feed_item_style.headerWrap.item;
var renderIconText = function (_a) {
    var _b = _a.shareLink, shareLink = _b === void 0 ? 'http://lxbtest.tk/' : _b, iconName = _a.iconName, iconColor = _a.iconColor, style = _a.style, _c = _a.onClick, onClick = _c === void 0 ? function () { } : _c, _d = _a.innerStyle, innerStyle = _d === void 0 ? {} : _d, _e = _a.isLink, isLink = _e === void 0 ? false : _e;
    var iconProps = {
        style: {
            color: iconColor,
            width: 40,
            height: 40,
        },
        name: iconName,
        innerStyle: Object.assign({}, itemStyle.inner, innerStyle),
    };
    return (isLink
        ?
            react["createElement"]("a", { style: style, href: "" + shareFacebookLink + shareLink, target: '_blank' },
                react["createElement"](icon["a" /* default */], __assign({}, iconProps)))
        :
            react["createElement"]("div", { style: style, onClick: onClick },
                react["createElement"](icon["a" /* default */], __assign({}, iconProps))));
};
var renderImageGroup = function (_a) {
    var item = _a.item, openModal = _a.openModal, isShowFullImage = _a.isShowFullImage;
    var link = Object(responsive["b" /* isDesktopVersion */])() ? '#' : routing["q" /* ROUTING_COMMUNITY_PATH */] + "/" + (item && item.id || 0);
    var pictureList = item
        && !Object(validate["l" /* isUndefined */])(item.pictures)
        && !!item.pictures.length
        ? item.pictures
        : item && !Object(validate["l" /* isUndefined */])(item.picture) ? [item.picture] : [];
    var imgUrl = item
        && !Object(validate["l" /* isUndefined */])(item.pictures)
        && Array.isArray(item.pictures)
        && item.pictures.length === 1
        ? item.pictures[0].original_url
        : item && item.picture && item.picture.original_url || '';
    return isShowFullImage
        ? react["createElement"](image_slider_community["a" /* default */], { data: pictureList })
        : item
            && !Object(validate["l" /* isUndefined */])(item.pictures)
            && Array.isArray(item.pictures)
            && item.pictures.length > 1
            ? (react["createElement"](react_router_dom["NavLink"], { to: link, style: feed_item_style.pictureList },
                react["createElement"]("div", { style: feed_item_style.pictureList.pictureWrap }, renderImageList({ item: item, openModal: openModal }))))
            : (react["createElement"](react_router_dom["NavLink"], { to: link }, renderImage({ imgUrl: imgUrl, style: itemStyle.onePicture, openModal: openModal, data: { data: Object.assign({}, item, { pictures: [{ original_url: imgUrl || '' }] }), posImg: 0 } })));
};
var renderImage = function (_a) {
    var imgUrl = _a.imgUrl, openModal = _a.openModal, data = _a.data, _b = _a.key, key = _b === void 0 ? '' : _b, _c = _a.style, style = _c === void 0 ? {} : _c;
    var handleClick = {
        MOBILE: function () { },
        DESKTOP: function () { return openModal(Object(application_modal["f" /* MODAL_FEED_ITEM_COMMUNITY */])(data)); }
    };
    return react["createElement"]("img", { onClick: function () { return handleClick[window.DEVICE_VERSION](); }, key: key, style: [itemStyle.imageContent, style], src: imgUrl });
};
var renderTwoImages = function (item, openModal) {
    var pictures = item.pictures;
    var isPictureListNotEmpty = pictures
        && Array.isArray(pictures)
        && !!pictures.length;
    var firstPicture = isPictureListNotEmpty && pictures[0] || { width: 0, height: 0 };
    var isHorizontal = firstPicture.width > firstPicture.height;
    var imgStyle = isHorizontal ? feed_item_style.pictures.twoPicture.horizontal : feed_item_style.pictures.twoPicture.vertical;
    return (react["createElement"]("div", { style: feed_item_style.pictures.twoPicture.container(isHorizontal) }, isPictureListNotEmpty && pictures.map(function (picture, index) { return react["createElement"]("div", { style: imgStyle }, renderImage({ imgUrl: picture && picture.original_url || '', key: "two-img-" + (picture && picture.id || 0), style: itemStyle.fullHeight, openModal: openModal, data: { data: item, posImg: index } })); })));
};
var renderThreeImages = function (item, openModal) {
    var pictures = item.pictures;
    var isPictureListNotEmpty = pictures
        && Array.isArray(pictures)
        && !!pictures.length;
    var firstPicture = isPictureListNotEmpty && pictures[0] || { width: 0, height: 0 };
    var isHorizontal = firstPicture.width > firstPicture.height;
    var imgStyle = isHorizontal ? feed_item_style.pictures.threePicture.horizontal : feed_item_style.pictures.threePicture.vertical;
    var imgItemGroupStyle = isHorizontal ? feed_item_style.pictures.threePicture.pictureGroup.horizontal : feed_item_style.pictures.threePicture.pictureGroup.vertical;
    return !isPictureListNotEmpty
        ? null
        : (react["createElement"]("div", { style: feed_item_style.pictures.threePicture.container(isHorizontal) },
            react["createElement"]("div", { style: imgStyle }, renderImage({ imgUrl: pictures[0].original_url, key: "three-img-" + pictures[0].id, style: itemStyle.fullHeight, openModal: openModal, data: { data: item, posImg: 0 } })),
            react["createElement"]("div", { style: feed_item_style.pictures.threePicture.pictureGroup.container(isHorizontal) }, [1, 2].map(function (index) { return react["createElement"]("div", { style: imgItemGroupStyle }, renderImage({ imgUrl: pictures[index].original_url, key: "three-img-" + pictures[index].id, openModal: openModal, data: { data: item, posImg: index } })); }))));
};
var renderFourImages = function (item, openModal, viewMoreNum) {
    if (viewMoreNum === void 0) { viewMoreNum = 0; }
    var pictures = item.pictures;
    var isPictureListNotEmpty = pictures
        && Array.isArray(pictures)
        && !!pictures.length;
    var firstPicture = isPictureListNotEmpty && pictures[0] || { width: 0, height: 0 };
    var isHorizontal = firstPicture.width > firstPicture.height;
    var imgStyle = isHorizontal ? feed_item_style.pictures.fourPicture.horizontal : feed_item_style.pictures.fourPicture.vertical;
    var imgItemGroupStyle = isHorizontal ? feed_item_style.pictures.fourPicture.pictureGroup.horizontal : feed_item_style.pictures.fourPicture.pictureGroup.vertical;
    return !isPictureListNotEmpty
        ? null
        : (react["createElement"]("div", { style: feed_item_style.pictures.fourPicture.container(isHorizontal) },
            react["createElement"]("div", { style: imgStyle }, renderImage({ imgUrl: pictures[0].original_url, key: "four-img-" + pictures[0].id, openModal: openModal, data: { data: item, posImg: 0 } })),
            react["createElement"]("div", { style: feed_item_style.pictures.fourPicture.pictureGroup.container(isHorizontal) },
                [1, 2].map(function (index) { return react["createElement"]("div", { style: imgItemGroupStyle }, renderImage({ imgUrl: pictures[index].original_url, key: "four-img-" + pictures[index].id, openModal: openModal, data: { data: item, posImg: index } })); }),
                viewMoreNum > 0
                    ?
                        react["createElement"]("div", { style: feed_item_style.pictures.viewMore.container(isHorizontal), onClick: function () { return Object(responsive["b" /* isDesktopVersion */])() ? openModal(Object(application_modal["f" /* MODAL_FEED_ITEM_COMMUNITY */])({ data: item, posImg: 3 })) : {}; } },
                            react["createElement"]("div", { style: [imgItemGroupStyle, feed_item_style.pictures.viewMore.imgViewMore(isHorizontal)] }, renderImage({ imgUrl: pictures[3].original_url, key: "four-img-" + pictures[3].id, openModal: openModal, data: { data: item, posImg: 3 } })),
                            react["createElement"]("span", { style: feed_item_style.pictures.viewMore.num },
                                "+",
                                viewMoreNum),
                            react["createElement"]("div", { style: feed_item_style.pictures.viewMore.bg }))
                    : react["createElement"]("div", { style: imgItemGroupStyle }, renderImage({ imgUrl: pictures[3].original_url, key: "four-img-" + pictures[3].id, openModal: openModal, data: { data: item, posImg: 3 } })))));
};
var renderImageList = function (_a) {
    var item = _a.item, openModal = _a.openModal;
    var pictureLength = item && item.pictures && item.pictures.length || 0;
    if (pictureLength > 0) {
        switch (pictureLength) {
            case 1: return renderImage({ imgUrl: item.pictures[0].original_url, style: feed_item_style.pictures.onePicture, openModal: openModal, data: { data: item, posImg: 0 } });
            case 2: return renderTwoImages(item, openModal);
            case 3: return renderThreeImages(item, openModal);
            case 4: return renderFourImages(item, openModal);
            default: return renderFourImages(item, openModal, pictureLength - 4);
        }
    }
    return null;
};
var getLikeProps = function (_a) {
    var iconColor = _a.iconColor, openModal = _a.openModal, handleLike = _a.handleLike, innerStyle = _a.innerStyle;
    return ({
        iconColor: iconColor,
        innerStyle: innerStyle,
        iconName: 'like',
        style: itemStyle.likeCommentIconGroup.icon,
        onClick: auth["a" /* auth */].loggedIn() ? handleLike : function () { return openModal(Object(application_modal["n" /* MODAL_SIGN_IN */])()); }
    });
};
var renderTooltip = function (text) { return (react["createElement"]("div", { className: 'tooltip', style: feed_item_style.tooltip },
    react["createElement"]("div", { style: feed_item_style.tooltip.group },
        react["createElement"]("div", { style: feed_item_style.tooltip.group.text }, text),
        react["createElement"]("div", { style: feed_item_style.tooltip.group.icon })))); };
var renderViewMore = function (_a) {
    var link = _a.link;
    return react["createElement"](react_router_dom["NavLink"], { to: link, style: feed_item_style.headerWrap.viewDetail.container }, "Xem chi ti\u1EBFt");
};
var generateLinkAds = function (_a) {
    var feedableType = _a.feedableType, feedable = _a.feedable;
    switch (feedableType) {
        case application_feedable["a" /* FEEDABLE_TYPE */].THEME: return renderViewMore({ link: routing["qb" /* ROUTING_THEME_DETAIL_PATH */] + "/" + (feedable && feedable.theme_feedable && feedable.theme_feedable.slug || '') });
        case application_feedable["a" /* FEEDABLE_TYPE */].BRAND: return renderViewMore({ link: routing["g" /* ROUTING_BRAND_DETAIL_PATH */] + "/" + (feedable && feedable.brand_feedable && feedable.brand_feedable.slug || '') });
        case application_feedable["a" /* FEEDABLE_TYPE */].BROWSENODE: return renderViewMore({ link: routing["Xa" /* ROUTING_PRODUCT_CATEGORY_PATH */] + "/" + (feedable && feedable.browse_node_feedable && feedable.browse_node_feedable.slug || '') });
        case application_feedable["a" /* FEEDABLE_TYPE */].BLOG: return renderViewMore({ link: routing["Ma" /* ROUTING_MAGAZINE_DETAIL_PATH */] + "/" + (feedable && feedable.blog_feedable && feedable.blog_feedable.slug || '') });
        case application_feedable["a" /* FEEDABLE_TYPE */].BOX: return renderViewMore({ link: routing["Za" /* ROUTING_PRODUCT_DETAIL_PATH */] + "/" + (feedable && feedable.box_feedable && feedable.box_feedable.slug || '') });
        default: return null;
    }
};
function renderView(_a) {
    var props = _a.props, state = _a.state, handleLike = _a.handleLike, handleSubmit = _a.handleSubmit, handleViewMore = _a.handleViewMore, handleShowVideo = _a.handleShowVideo, handleShowComment = _a.handleShowComment, setInputCommentRef = _a.setInputCommentRef, handleInputOnFocus = _a.handleInputOnFocus, handleInputOnChange = _a.handleInputOnChange, handleShowInputComment = _a.handleShowInputComment;
    var _b = props, item = _b.item, openModal = _b.openModal, likeProduct = _b.likeProduct, userProfile = _b.userProfile, listLikedId = _b.listLikedId, isLastChild = _b.isLastChild, isShowImage = _b.isShowImage, isFeedDetail = _b.isFeedDetail, unLikeProduct = _b.unLikeProduct, isShowFullImage = _b.isShowFullImage, _c = _b.limitTextLength, limitTextLength = _c === void 0 ? 0 : _c;
    var _d = state, isLike = _d.isLike, isViewMore = _d.isViewMore, commentList = _d.commentList, isShowVideo = _d.isShowVideo, answerComment = _d.answerComment, isShowComments = _d.isShowComments, isShowInputComment = _d.isShowInputComment, _e = _d.likeNum, likeNum = _e === void 0 ? 0 : _e;
    var renderTotalLikeComment = function () {
        var total = likeNum + item.total_comments;
        var text = " ng\u01B0\u1EDDi \u0111\u00E3 " + (!!likeNum ? 'yêu thích' : '') + " " + (!!likeNum && !!item.total_comments ? 'và' : '') + " " + (!!item.total_comments ? 'bình luận' : '');
        if (!likeNum && !item.total_comments) {
            return null;
        }
        return (react["createElement"]("div", { style: itemStyle.countingGroup },
            react["createElement"]("span", { style: itemStyle.countingGroup.userList }, [0, 1, 2, 3, 4].map(function (index) { return index < likeNum && (react["createElement"]("span", { onClick: handleShowComment, key: index, style: [itemStyle.countingGroup.userItem(index), { backgroundImage: "url(" + (item.likes[index] ? item.likes[index].avatar : '') + ")" }] })); })),
            react["createElement"]("span", { onClick: handleShowComment, style: itemStyle.countingGroup.text }, total),
            react["createElement"]("span", { onClick: handleShowComment, style: itemStyle.countingGroup.text }, text)));
    };
    var isLiked = auth["a" /* auth */].loggedIn()
        && item.box
        && listLikedId.indexOf(item.box.id || 0) >= 0;
    var renderYoutube = function (_a) {
        var isShowVideo = _a.isShowVideo, handleShowVideo = _a.handleShowVideo, videoUrl = _a.videoUrl, pictureUrl = _a.pictureUrl;
        var iconProps = {
            name: 'play',
            style: feed_item_style.icon,
            innerStyle: feed_item_style.icon.inner
        };
        var youtubeUrl = videoUrl.replace('watch?v=', 'embed/') + '?autoplay=1&showinfo=0&controls=0';
        return (react["createElement"]("div", { style: feed_item_style.videoContainer }, !isShowVideo
            ?
                react["createElement"]("div", { style: feed_item_style.videoContainer.thumbnail(pictureUrl), onClick: handleShowVideo },
                    react["createElement"](icon["a" /* default */], __assign({}, iconProps)),
                    react["createElement"]("div", { style: feed_item_style.wrapIcon }))
            :
                react["createElement"]("div", { style: feed_item_style.videoContainer.videoWrap },
                    react["createElement"]("iframe", { style: feed_item_style.videoContainer.videoWrap.video, className: 'frame-you-tube', src: youtubeUrl }))));
    };
    var inputCommentProps = {
        ref: function (ref) { return setInputCommentRef(ref); },
        autoFocus: true,
        style: itemStyle.inputCommentGroup.inputText,
        placeholder: 'Gửi bình luận của bạn...',
        type: global["d" /* INPUT_TYPE */].TEXT,
        onChange: function (e) { return handleInputOnChange(e); },
        onFocus: handleInputOnFocus,
        onSubmit: handleSubmit,
        value: answerComment || '',
    };
    var txtComment = auth["a" /* auth */].loggedIn() ? 'Gửi bình luận của bạn...' : 'Đăng nhập để bình luận';
    var actionComment = auth["a" /* auth */].loggedIn() ? handleShowInputComment : function () { return openModal(Object(application_modal["n" /* MODAL_SIGN_IN */])()); };
    var avatarUrl = auth["a" /* auth */].loggedIn() ? userProfile && userProfile.avatar && userProfile.avatar.medium_url : '';
    var avatarProps = {
        style: [
            { backgroundImage: "url('" + avatarUrl + "')" },
            itemStyle.inputCommentGroup.avatar,
        ]
    };
    var likeProps = true === isLike
        ? getLikeProps({ iconColor: variable["colorPink"], openModal: openModal, handleLike: handleLike, innerStyle: itemStyle.likeCommentIconGroup.innerIconLike })
        : getLikeProps({ iconColor: variable["color2E"], openModal: openModal, handleLike: handleLike, innerStyle: itemStyle.likeCommentIconGroup.innerIconLike });
    var userFeedUrl = routing["w" /* ROUTING_COMMUNITY_USER_FEED_PATH */] + "/" + (item && item.user && item.user.id || 0);
    var feedableType = item && item.feedable_type || '';
    return (react["createElement"]("div", { style: [itemStyle.container, isLastChild ? itemStyle.lastChild : '', !isShowImage && { paddingBottom: 0, borderBottom: 'none' }] },
        react["createElement"]("div", { style: feed_item_style.headerWrap },
            react["createElement"]("div", { style: itemStyle.info.container },
                react["createElement"](react_router_dom["NavLink"], { to: userFeedUrl },
                    react["createElement"]("div", { style: [{ backgroundImage: "url(" + (item && item.user && item.user.avatar && item.user.avatar.medium_url || '') + ")" }, itemStyle.info.avatar] })),
                react["createElement"](react_router_dom["NavLink"], { to: userFeedUrl, style: itemStyle.info.detail, className: 'icon-item' },
                    react["createElement"]("div", null,
                        react["createElement"]("span", { style: itemStyle.info.detail.username },
                            item && item.user && item.user.name || '',
                            !isFeedDetail && renderTooltip('Xem trang cá nhân'))),
                    react["createElement"]("div", { style: itemStyle.info.detail.ratingGroup },
                        item && item.rating && react["createElement"](rating_star["a" /* default */], { style: itemStyle.info.detail.ratingGroup.rating, value: item.rating }),
                        item && item.created_at && react["createElement"](react_router_dom["NavLink"], { to: routing["q" /* ROUTING_COMMUNITY_PATH */] + "/" + (item && item.id || 0), style: itemStyle.info.detail.ratingGroup.date }, Object(encode["c" /* convertUnixTime */])(item.created_at, global["b" /* DATETIME_TYPE_FORMAT */].SHORT_DATE)))),
                feedableType === application_feedable["a" /* FEEDABLE_TYPE */].FEEDBACK
                    && (react["createElement"](react_router_dom["NavLink"], { to: routing["Za" /* ROUTING_PRODUCT_DETAIL_PATH */] + "/" + (item && item.box && item.box.slug || ''), style: feed_item_style.headerWrap.imgProduct.container },
                        react["createElement"]("img", { style: feed_item_style.headerWrap.imgProduct.img, src: item && item.picture && item.picture.medium_url || '' }))),
                generateLinkAds({ feedableType: feedableType, feedable: item && item.feedable }))),
        item
            && item.message
            && limitTextLength < item.message.length
            && !isViewMore
            ?
                react["createElement"]("div", { style: itemStyle.info.description.container },
                    item && item.message && item.message.substring(0, limitTextLength),
                    react["createElement"]("span", { style: itemStyle.info.description.viewMore, onClick: handleViewMore }, "... Xem th\u00EAm"))
            : Object(html["a" /* renderHtmlContent */])(Object(format["b" /* createBreakDownLine */])(item && item.message || ''), itemStyle.info.description.container),
        !isShowImage || item && item.feedable_type === application_feedable["a" /* FEEDABLE_TYPE */].FEEDBACK
            ? null
            : Object(validate["j" /* isEmptyObject */])(item && item.video)
                ? renderImageGroup({ item: item, openModal: openModal, isShowFullImage: isShowFullImage })
                : renderYoutube({ isShowVideo: isShowVideo, handleShowVideo: handleShowVideo, videoUrl: item && item.video && item.video.url, pictureUrl: item && item.video && item.video.thumbnail && item.video.thumbnail.original_url || '' }),
        react["createElement"]("div", { style: itemStyle.likeCommentIconGroup.container },
            react["createElement"]("div", { style: itemStyle.likeCommentIconGroup.left },
                renderIconText(likeProps),
                renderIconText({
                    iconName: 'message',
                    iconColor: variable["color2E"],
                    style: itemStyle.likeCommentIconGroup.icon,
                    innerStyle: itemStyle.likeCommentIconGroup.innerIconComment,
                    onClick: handleShowComment
                }),
                renderIconText({
                    iconName: 'fly',
                    iconColor: variable["color2E"],
                    style: itemStyle.likeCommentIconGroup.icon,
                    isLink: true,
                    shareLink: item && item.share_link || '',
                    innerStyle: itemStyle.likeCommentIconGroup.innerIconFly
                })),
            item && item.feedable_type === application_feedable["a" /* FEEDABLE_TYPE */].FEEDBACK
                && (react["createElement"]("div", { style: itemStyle.likeCommentIconGroup.right }, renderIconText({
                    iconName: isLiked ? 'bookmark' : 'bookmark-line',
                    iconColor: isLiked ? variable["colorPink"] : variable["color2E"],
                    style: itemStyle.likeCommentIconGroup.icon,
                    isLink: false,
                    shareLink: item && item.share_link || '',
                    innerStyle: itemStyle.likeCommentIconGroup.innerIconHeart,
                    onClick: function () {
                        if (auth["a" /* auth */].loggedIn() && item.box) {
                            isLiked
                                ? unLikeProduct(item.box.id)
                                : likeProduct(item.box.id);
                        }
                        else {
                            openModal(Object(application_modal["n" /* MODAL_SIGN_IN */])());
                        }
                    }
                })))),
        renderTotalLikeComment(),
        isShowComments && (react["createElement"]("div", { style: itemStyle.commentGroup },
            'undefined' === typeof commentList
                /** Loading Icon */
                ? react["createElement"](loading["a" /* default */], { style: feed_item_style.customStyleLoading })
                /** Show list data */
                : Array.isArray(commentList)
                    && commentList.map(function (comment) { return (react["createElement"]("div", { key: comment.id, style: itemStyle.commentGroup.container },
                        react["createElement"]("div", { style: [{ backgroundImage: "url('" + comment.avatar.medium_url + "')" }, itemStyle.info.avatar, itemStyle.info.avatar.small] }),
                        react["createElement"]("div", { style: itemStyle.commentGroup.contenGroup },
                            react["createElement"]("span", { style: itemStyle.info.detail.username }, comment.user_name),
                            react["createElement"]("span", { style: itemStyle.commentGroup.contenGroup.comment }, comment.content)))); }),
            isShowInputComment
                ? (react["createElement"]("div", { style: itemStyle.inputCommentGroup },
                    react["createElement"]("div", __assign({}, avatarProps)),
                    react["createElement"]("div", { style: itemStyle.inputCommentGroup.commentInputGroup },
                        react["createElement"]("input", __assign({}, inputCommentProps)),
                        react["createElement"]("div", { style: itemStyle.inputCommentGroup.sendComment, onClick: handleSubmit }, "G\u1EEDi"))))
                : (react["createElement"]("div", { style: itemStyle.inputCommentGroup },
                    react["createElement"]("div", __assign({}, avatarProps)),
                    react["createElement"]("div", { style: itemStyle.inputCommentGroup.input, onClick: actionComment }, txtComment))))),
        react["createElement"](radium["Style"], { rules: INLINE_STYLE.tooltip })));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxPQUFPLElBQUksTUFBTSxlQUFlLENBQUM7QUFDakMsT0FBTyxPQUFPLE1BQU0sa0JBQWtCLENBQUM7QUFDdkMsT0FBTyxVQUFVLE1BQU0sc0JBQXNCLENBQUM7QUFDOUMsT0FBTyxvQkFBb0IsTUFBTSxzREFBc0QsQ0FBQztBQUV4RixPQUFPLEVBQUUsSUFBSSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDM0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3hELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQzdELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzVELE9BQU8sRUFBRSxhQUFhLEVBQUUsV0FBVyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFFckUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ25FLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUN4RSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsYUFBYSxFQUFFLHlCQUF5QixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDaEcsT0FBTyxFQUNMLHNCQUFzQixFQUN0Qix5QkFBeUIsRUFDekIseUJBQXlCLEVBQ3pCLDJCQUEyQixFQUMzQiw0QkFBNEIsRUFDNUIsNkJBQTZCLEVBQzdCLGdDQUFnQyxHQUNqQyxNQUFNLHdDQUF3QyxDQUFDO0FBR2hELE9BQU8sS0FBSyxFQUFFLEVBQUUsWUFBWSxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBRzlDLElBQU0saUJBQWlCLEdBQUcsa0NBQWtDLENBQUM7QUFFN0QsSUFBTSxTQUFTLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7QUFFeEMsSUFBTSxjQUFjLEdBQUcsVUFBQyxFQVF2QjtRQVBDLGlCQUFzQyxFQUF0QywyREFBc0MsRUFDdEMsc0JBQVEsRUFDUix3QkFBUyxFQUNULGdCQUFLLEVBQ0wsZUFBbUIsRUFBbkIsOENBQW1CLEVBQ25CLGtCQUFlLEVBQWYsb0NBQWUsRUFDZixjQUFjLEVBQWQsbUNBQWM7SUFHZCxJQUFNLFNBQVMsR0FBRztRQUNoQixLQUFLLEVBQUU7WUFDTCxLQUFLLEVBQUUsU0FBUztZQUNoQixLQUFLLEVBQUUsRUFBRTtZQUNULE1BQU0sRUFBRSxFQUFFO1NBQ1g7UUFDRCxJQUFJLEVBQUUsUUFBUTtRQUNkLFVBQVUsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxTQUFTLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQztLQUMzRCxDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsTUFBTTtRQUNKLENBQUM7WUFDRCwyQkFBRyxLQUFLLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxLQUFHLGlCQUFpQixHQUFHLFNBQVcsRUFBRSxNQUFNLEVBQUUsUUFBUTtnQkFDekUsb0JBQUMsSUFBSSxlQUFLLFNBQVMsRUFBSSxDQUNyQjtRQUNKLENBQUM7WUFDRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxPQUFPO2dCQUNqQyxvQkFBQyxJQUFJLGVBQUssU0FBUyxFQUFJLENBQ25CLENBQ1QsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLElBQU0sZ0JBQWdCLEdBQUcsVUFBQyxFQUFvQztRQUFsQyxjQUFJLEVBQUUsd0JBQVMsRUFBRSxvQ0FBZTtJQUMxRCxJQUFNLElBQUksR0FBRyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFJLHNCQUFzQixVQUFJLElBQUksSUFBSSxJQUFJLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBRSxDQUFDO0lBRTVGLElBQU0sV0FBVyxHQUFHLElBQUk7V0FDbkIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztXQUMzQixDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNO1FBQ3pCLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUTtRQUNmLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBRTdELElBQU0sTUFBTSxHQUFHLElBQUk7V0FDZCxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1dBQzNCLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztXQUM1QixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sS0FBSyxDQUFDO1FBQzdCLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVk7UUFDL0IsQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxJQUFJLEVBQUUsQ0FBQztJQUU1RCxNQUFNLENBQUMsZUFBZTtRQUNwQixDQUFDLENBQUMsb0JBQUMsb0JBQW9CLElBQUMsSUFBSSxFQUFFLFdBQVcsR0FBSTtRQUM3QyxDQUFDLENBQUMsSUFBSTtlQUNELENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7ZUFDM0IsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2VBQzVCLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUM7WUFDM0IsQ0FBQyxDQUFDLENBQ0Esb0JBQUMsT0FBTyxJQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXO2dCQUN6Qyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLElBQ3RDLGVBQWUsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLFNBQVMsV0FBQSxFQUFFLENBQUMsQ0FDakMsQ0FDRSxDQUNYO1lBQ0QsQ0FBQyxDQUFDLENBQ0Esb0JBQUMsT0FBTyxJQUFDLEVBQUUsRUFBRSxJQUFJLElBQ2QsV0FBVyxDQUFDLEVBQUUsTUFBTSxRQUFBLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxVQUFVLEVBQUUsU0FBUyxXQUFBLEVBQUUsSUFBSSxFQUFFLEVBQUUsSUFBSSxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLFFBQVEsRUFBRSxDQUFDLEVBQUUsWUFBWSxFQUFFLE1BQU0sSUFBSSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUM1SixDQUNYLENBQUE7QUFDUCxDQUFDLENBQUM7QUFFRixJQUFNLFdBQVcsR0FBRyxVQUFDLEVBQWlEO1FBQS9DLGtCQUFNLEVBQUUsd0JBQVMsRUFBRSxjQUFJLEVBQUUsV0FBUSxFQUFSLDZCQUFRLEVBQUUsYUFBVSxFQUFWLCtCQUFVO0lBQ2xFLElBQU0sV0FBVyxHQUFHO1FBQ2xCLE1BQU0sRUFBRSxjQUFRLENBQUM7UUFDakIsT0FBTyxFQUFFLGNBQU0sT0FBQSxTQUFTLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBMUMsQ0FBMEM7S0FDMUQsQ0FBQztJQUVGLE1BQU0sQ0FBQyw2QkFBSyxPQUFPLEVBQUUsY0FBTSxPQUFBLFdBQVcsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUUsRUFBcEMsQ0FBb0MsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLEVBQUUsR0FBRyxFQUFFLE1BQU0sR0FBSSxDQUFDO0FBQ3JJLENBQUMsQ0FBQztBQUVGLElBQU0sZUFBZSxHQUFHLFVBQUMsSUFBSSxFQUFFLFNBQVM7SUFDOUIsSUFBQSx3QkFBUSxDQUFVO0lBQzFCLElBQU0scUJBQXFCLEdBQUcsUUFBUTtXQUNqQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztXQUN2QixDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztJQUV2QixJQUFNLFlBQVksR0FBRyxxQkFBcUIsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQztJQUNyRixJQUFNLFlBQVksR0FBRyxZQUFZLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUM7SUFDOUQsSUFBTSxRQUFRLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQztJQUUxRyxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxJQUMxRCxxQkFBcUIsSUFBSSxRQUFRLENBQUMsR0FBRyxDQUFDLFVBQUMsT0FBTyxFQUFFLEtBQUssSUFBSyxPQUFBLDZCQUFLLEtBQUssRUFBRSxRQUFRLElBQUcsV0FBVyxDQUFDLEVBQUUsTUFBTSxFQUFFLE9BQU8sSUFBSSxPQUFPLENBQUMsWUFBWSxJQUFJLEVBQUUsRUFBRSxHQUFHLEVBQUUsY0FBVyxPQUFPLElBQUksT0FBTyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUUsRUFBRSxLQUFLLEVBQUUsU0FBUyxDQUFDLFVBQVUsRUFBRSxTQUFTLFdBQUEsRUFBRSxJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQU8sRUFBdk4sQ0FBdU4sQ0FBQyxDQUMvUSxDQUNQLENBQUE7QUFDSCxDQUFDLENBQUM7QUFFRixJQUFNLGlCQUFpQixHQUFHLFVBQUMsSUFBSSxFQUFFLFNBQVM7SUFDaEMsSUFBQSx3QkFBUSxDQUFVO0lBQzFCLElBQU0scUJBQXFCLEdBQUcsUUFBUTtXQUNqQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztXQUN2QixDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztJQUV2QixJQUFNLFlBQVksR0FBRyxxQkFBcUIsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQztJQUNyRixJQUFNLFlBQVksR0FBRyxZQUFZLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUM7SUFDOUQsSUFBTSxRQUFRLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQztJQUM5RyxJQUFNLGlCQUFpQixHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQztJQUVqSixNQUFNLENBQUMsQ0FBQyxxQkFBcUI7UUFDM0IsQ0FBQyxDQUFDLElBQUk7UUFDTixDQUFDLENBQUMsQ0FDQSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQztZQUM3RCw2QkFBSyxLQUFLLEVBQUUsUUFBUSxJQUFHLFdBQVcsQ0FBQyxFQUFFLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxFQUFFLEdBQUcsRUFBRSxlQUFhLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFJLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxVQUFVLEVBQUUsU0FBUyxXQUFBLEVBQUUsSUFBSSxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFPO1lBQzVMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxJQUN6RSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSw2QkFBSyxLQUFLLEVBQUUsaUJBQWlCLElBQUcsV0FBVyxDQUFDLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxZQUFZLEVBQUUsR0FBRyxFQUFFLGVBQWEsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUksRUFBRSxTQUFTLFdBQUEsRUFBRSxJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQU8sRUFBcEwsQ0FBb0wsQ0FBQyxDQUN0TSxDQUNGLENBQ1AsQ0FBQTtBQUNMLENBQUMsQ0FBQztBQUVGLElBQU0sZ0JBQWdCLEdBQUcsVUFBQyxJQUFJLEVBQUUsU0FBUyxFQUFFLFdBQWU7SUFBZiw0QkFBQSxFQUFBLGVBQWU7SUFDaEQsSUFBQSx3QkFBUSxDQUFVO0lBQzFCLElBQU0scUJBQXFCLEdBQUcsUUFBUTtXQUNqQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztXQUN2QixDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztJQUV2QixJQUFNLFlBQVksR0FBRyxxQkFBcUIsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQztJQUNyRixJQUFNLFlBQVksR0FBRyxZQUFZLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUM7SUFDOUQsSUFBTSxRQUFRLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQztJQUM1RyxJQUFNLGlCQUFpQixHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQztJQUUvSSxNQUFNLENBQUMsQ0FBQyxxQkFBcUI7UUFDM0IsQ0FBQyxDQUFDLElBQUk7UUFDTixDQUFDLENBQUMsQ0FDQSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQztZQUM1RCw2QkFBSyxLQUFLLEVBQUUsUUFBUSxJQUFHLFdBQVcsQ0FBQyxFQUFFLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxFQUFFLEdBQUcsRUFBRSxjQUFZLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFJLEVBQUUsU0FBUyxXQUFBLEVBQUUsSUFBSSxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFPO1lBQzlKLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQztnQkFDeEUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsNkJBQUssS0FBSyxFQUFFLGlCQUFpQixJQUFHLFdBQVcsQ0FBQyxFQUFFLE1BQU0sRUFBRSxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsWUFBWSxFQUFFLEdBQUcsRUFBRSxjQUFZLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFJLEVBQUUsU0FBUyxXQUFBLEVBQUUsSUFBSSxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFPLEVBQW5MLENBQW1MLENBQUM7Z0JBRXZNLFdBQVcsR0FBRyxDQUFDO29CQUNiLENBQUM7d0JBQ0QsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsRUFBRSxPQUFPLEVBQUUsY0FBTSxPQUFBLGdCQUFnQixFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyx5QkFBeUIsQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUF6RixDQUF5Rjs0QkFDbkssNkJBQUssS0FBSyxFQUFFLENBQUMsaUJBQWlCLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUcsV0FBVyxDQUFDLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLEVBQUUsR0FBRyxFQUFFLGNBQVksUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUksRUFBRSxTQUFTLFdBQUEsRUFBRSxJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQU87NEJBQzVOLDhCQUFNLEtBQUssRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHOztnQ0FBSSxXQUFXLENBQVE7NEJBQy9ELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEdBQVEsQ0FDMUM7b0JBQ04sQ0FBQyxDQUFDLDZCQUFLLEtBQUssRUFBRSxpQkFBaUIsSUFBRyxXQUFXLENBQUMsRUFBRSxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksRUFBRSxHQUFHLEVBQUUsY0FBWSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBSSxFQUFFLFNBQVMsV0FBQSxFQUFFLElBQUksRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBTyxDQUV6SyxDQUNELENBQ1IsQ0FBQTtBQUNMLENBQUMsQ0FBQztBQUVGLElBQU0sZUFBZSxHQUFHLFVBQUMsRUFBbUI7UUFBakIsY0FBSSxFQUFFLHdCQUFTO0lBQ3hDLElBQU0sYUFBYSxHQUFHLElBQUksSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQztJQUN6RSxFQUFFLENBQUMsQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN0QixNQUFNLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQ3RCLEtBQUssQ0FBQyxFQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLFNBQVMsV0FBQSxFQUFFLElBQUksRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUNwSixLQUFLLENBQUMsRUFBRSxNQUFNLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztZQUNoRCxLQUFLLENBQUMsRUFBRSxNQUFNLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBQ2xELEtBQUssQ0FBQyxFQUFFLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFDakQsU0FBUyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLFNBQVMsRUFBRSxhQUFhLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDdkUsQ0FBQztJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsSUFBSSxDQUFDO0FBQ2QsQ0FBQyxDQUFDO0FBRUYsSUFBTSxZQUFZLEdBQUcsVUFBQyxFQUFnRDtRQUE5Qyx3QkFBUyxFQUFFLHdCQUFTLEVBQUUsMEJBQVUsRUFBRSwwQkFBVTtJQUFPLE9BQUEsQ0FBQztRQUMxRSxTQUFTLFdBQUE7UUFDVCxVQUFVLFlBQUE7UUFDVixRQUFRLEVBQUUsTUFBTTtRQUNoQixLQUFLLEVBQUUsU0FBUyxDQUFDLG9CQUFvQixDQUFDLElBQUk7UUFDMUMsT0FBTyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxjQUFNLE9BQUEsU0FBUyxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQTFCLENBQTBCO0tBQ3pFLENBQUM7QUFOeUUsQ0FNekUsQ0FBQztBQUVILElBQU0sYUFBYSxHQUFHLFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FDOUIsNkJBQUssU0FBUyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU87SUFDN0MsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSztRQUM3Qiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFHLElBQUksQ0FBTztRQUNsRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFRLENBQ3hDLENBQ0YsQ0FDUCxFQVArQixDQU8vQixDQUFDO0FBRUYsSUFBTSxjQUFjLEdBQUcsVUFBQyxFQUFRO1FBQU4sY0FBSTtJQUFPLE9BQUEsb0JBQUMsT0FBTyxJQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLFNBQVMsd0JBQXdCO0FBQXZGLENBQXVGLENBQUM7QUFFN0gsSUFBTSxlQUFlLEdBQUcsVUFBQyxFQUEwQjtRQUF4Qiw4QkFBWSxFQUFFLHNCQUFRO0lBQy9DLE1BQU0sQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFDckIsS0FBSyxhQUFhLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFBRSxJQUFJLEVBQUsseUJBQXlCLFVBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxjQUFjLElBQUksUUFBUSxDQUFDLGNBQWMsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ3ZLLEtBQUssYUFBYSxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUUsSUFBSSxFQUFLLHlCQUF5QixVQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsY0FBYyxJQUFJLFFBQVEsQ0FBQyxjQUFjLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBRSxFQUFFLENBQUMsQ0FBQztRQUN2SyxLQUFLLGFBQWEsQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLGNBQWMsQ0FBQyxFQUFFLElBQUksRUFBSyw2QkFBNkIsVUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLG9CQUFvQixJQUFJLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzVMLEtBQUssYUFBYSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUUsSUFBSSxFQUFLLDRCQUE0QixVQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsYUFBYSxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBRSxFQUFFLENBQUMsQ0FBQztRQUN2SyxLQUFLLGFBQWEsQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLGNBQWMsQ0FBQyxFQUFFLElBQUksRUFBSywyQkFBMkIsVUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFlBQVksSUFBSSxRQUFRLENBQUMsWUFBWSxDQUFDLElBQUksSUFBSSxFQUFFLENBQUUsRUFBRSxDQUFDLENBQUM7UUFDbkssU0FBUyxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ3ZCLENBQUM7QUFDSCxDQUFDLENBQUE7QUFFRCxNQUFNLHFCQUFxQixFQVkxQjtRQVhDLGdCQUFLLEVBQ0wsZ0JBQUssRUFDTCwwQkFBVSxFQUNWLDhCQUFZLEVBQ1osa0NBQWMsRUFDZCxvQ0FBZSxFQUNmLHdDQUFpQixFQUNqQiwwQ0FBa0IsRUFDbEIsMENBQWtCLEVBQ2xCLDRDQUFtQixFQUNuQixrREFBc0I7SUFHaEIsSUFBQSxVQVlpQixFQVhyQixjQUFJLEVBQ0osd0JBQVMsRUFDVCw0QkFBVyxFQUNYLDRCQUFXLEVBQ1gsNEJBQVcsRUFDWCw0QkFBVyxFQUNYLDRCQUFXLEVBQ1gsOEJBQVksRUFDWixnQ0FBYSxFQUNiLG9DQUFlLEVBQ2YsdUJBQW1CLEVBQW5CLHdDQUFtQixDQUNHO0lBRWxCLElBQUEsVUFTaUIsRUFSckIsa0JBQU0sRUFDTiwwQkFBVSxFQUNWLDRCQUFXLEVBQ1gsNEJBQVcsRUFDWCxnQ0FBYSxFQUNiLGtDQUFjLEVBQ2QsMENBQWtCLEVBQ2xCLGVBQVcsRUFBWCxnQ0FBVyxDQUNXO0lBRXhCLElBQU0sc0JBQXNCLEdBQUc7UUFDN0IsSUFBTSxLQUFLLEdBQUcsT0FBTyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7UUFDNUMsSUFBTSxJQUFJLEdBQUcsb0NBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLFdBQUksQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLFdBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFFLENBQUM7UUFFdkosRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUE7UUFBQyxDQUFDO1FBRXJELE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxTQUFTLENBQUMsYUFBYTtZQUNqQyw4QkFBTSxLQUFLLEVBQUUsU0FBUyxDQUFDLGFBQWEsQ0FBQyxRQUFRLElBRXpDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssR0FBRyxPQUFPLElBQUksQ0FDOUMsOEJBQ0UsT0FBTyxFQUFFLGlCQUFpQixFQUMxQixHQUFHLEVBQUUsS0FBSyxFQUNWLEtBQUssRUFBRSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsZUFBZSxFQUFFLFVBQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsT0FBRyxFQUFFLENBQUMsR0FDNUgsQ0FDVCxFQU40QixDQU01QixDQUFDLENBRUM7WUFDUCw4QkFBTSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxhQUFhLENBQUMsSUFBSSxJQUFHLEtBQUssQ0FBUTtZQUNyRiw4QkFBTSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxhQUFhLENBQUMsSUFBSSxJQUFHLElBQUksQ0FBUSxDQUNoRixDQUNQLENBQUE7SUFDSCxDQUFDLENBQUE7SUFFRCxJQUFNLE9BQU8sR0FDWCxJQUFJLENBQUMsUUFBUSxFQUFFO1dBQ1osSUFBSSxDQUFDLEdBQUc7V0FDUixXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUVoRCxJQUFNLGFBQWEsR0FBRyxVQUFDLEVBQXNEO1lBQXBELDRCQUFXLEVBQUUsb0NBQWUsRUFBRSxzQkFBUSxFQUFFLDBCQUFVO1FBQ3pFLElBQU0sU0FBUyxHQUFHO1lBQ2hCLElBQUksRUFBRSxNQUFNO1lBQ1osS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJO1lBQ2pCLFVBQVUsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUs7U0FDN0IsQ0FBQztRQUVGLElBQU0sVUFBVSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxHQUFHLG1DQUFtQyxDQUFDO1FBQ2hHLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsY0FBYyxJQUU1QixDQUFDLFdBQVc7WUFDVixDQUFDO2dCQUNELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsRUFBRSxPQUFPLEVBQUUsZUFBZTtvQkFDOUUsb0JBQUMsSUFBSSxlQUFLLFNBQVMsRUFBSTtvQkFDdkIsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxRQUFRLEdBQVEsQ0FDOUI7WUFDTixDQUFDO2dCQUNELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsY0FBYyxDQUFDLFNBQVM7b0JBQ3hDLGdDQUFRLEtBQUssRUFBRSxLQUFLLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLGdCQUFnQixFQUFFLEdBQUcsRUFBRSxVQUFVLEdBQVcsQ0FDeEcsQ0FFTixDQUNQLENBQUE7SUFDSCxDQUFDLENBQUM7SUFFRixJQUFNLGlCQUFpQixHQUFHO1FBQ3hCLEdBQUcsRUFBRSxVQUFBLEdBQUcsSUFBSSxPQUFBLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxFQUF2QixDQUF1QjtRQUNuQyxTQUFTLEVBQUUsSUFBSTtRQUNmLEtBQUssRUFBRSxTQUFTLENBQUMsaUJBQWlCLENBQUMsU0FBUztRQUM1QyxXQUFXLEVBQUUsMEJBQTBCO1FBQ3ZDLElBQUksRUFBRSxVQUFVLENBQUMsSUFBSTtRQUNyQixRQUFRLEVBQUUsVUFBQyxDQUFDLElBQUssT0FBQSxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsRUFBdEIsQ0FBc0I7UUFDdkMsT0FBTyxFQUFFLGtCQUFrQjtRQUMzQixRQUFRLEVBQUUsWUFBWTtRQUN0QixLQUFLLEVBQUUsYUFBYSxJQUFJLEVBQUU7S0FDM0IsQ0FBQztJQUVGLElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDLHdCQUF3QixDQUFDO0lBQzNGLElBQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLGNBQU0sT0FBQSxTQUFTLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBMUIsQ0FBMEIsQ0FBQztJQUNsRyxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLFdBQVcsSUFBSSxXQUFXLENBQUMsTUFBTSxJQUFJLFdBQVcsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFFNUcsSUFBTSxXQUFXLEdBQUc7UUFDbEIsS0FBSyxFQUFFO1lBQ0wsRUFBRSxlQUFlLEVBQUUsVUFBUSxTQUFTLE9BQUksRUFBRTtZQUMxQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsTUFBTTtTQUNuQztLQUNGLENBQUM7SUFFRixJQUFNLFNBQVMsR0FBRyxJQUFJLEtBQUssTUFBTTtRQUMvQixDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUUsU0FBUyxFQUFFLFFBQVEsQ0FBQyxTQUFTLEVBQUUsU0FBUyxXQUFBLEVBQUUsVUFBVSxZQUFBLEVBQUUsVUFBVSxFQUFFLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNsSSxDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUUsU0FBUyxFQUFFLFFBQVEsQ0FBQyxPQUFPLEVBQUUsU0FBUyxXQUFBLEVBQUUsVUFBVSxZQUFBLEVBQUUsVUFBVSxFQUFFLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBRW5JLElBQU0sV0FBVyxHQUFNLGdDQUFnQyxVQUFJLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBRSxDQUFDO0lBRXBHLElBQU0sWUFBWSxHQUFHLElBQUksSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLEVBQUUsQ0FBQztJQUV0RCxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLFdBQVcsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsV0FBVyxJQUFJLEVBQUUsYUFBYSxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLENBQUM7UUFFbkksNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxVQUFVO1lBQzFCLDZCQUFLLEtBQUssRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVM7Z0JBRWxDLG9CQUFDLE9BQU8sSUFBQyxFQUFFLEVBQUUsV0FBVztvQkFDdEIsNkJBQUssS0FBSyxFQUFFLENBQUMsRUFBRSxlQUFlLEVBQUUsVUFBTyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLElBQUksRUFBRSxPQUFHLEVBQUUsRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFRLENBQzlJO2dCQUNWLG9CQUFDLE9BQU8sSUFBQyxFQUFFLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxTQUFTLEVBQUUsV0FBVztvQkFDNUU7d0JBQ0UsOEJBQU0sS0FBSyxFQUFFLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVE7NEJBQ3pDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUU7NEJBQ3hDLENBQUMsWUFBWSxJQUFJLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxDQUMvQyxDQUNIO29CQUNOLDZCQUFLLEtBQUssRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXO3dCQUMxQyxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxvQkFBQyxVQUFVLElBQUMsS0FBSyxFQUFFLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxNQUFNLEdBQUk7d0JBQzFHLElBQUksSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLG9CQUFDLE9BQU8sSUFBQyxFQUFFLEVBQUssc0JBQXNCLFVBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFFLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUcsZUFBZSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsb0JBQW9CLENBQUMsVUFBVSxDQUFDLENBQVcsQ0FDdE4sQ0FDRTtnQkFFUixZQUFZLEtBQUssYUFBYSxDQUFDLFFBQVE7dUJBQ3BDLENBQ0Qsb0JBQUMsT0FBTyxJQUFDLEVBQUUsRUFBSywyQkFBMkIsVUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsU0FBUzt3QkFDcEksNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsSUFBSSxJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRSxHQUFJLENBQ25HLENBQ1g7Z0JBRUYsZUFBZSxDQUFDLEVBQUUsWUFBWSxjQUFBLEVBQUUsUUFBUSxFQUFFLElBQUksSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FDL0QsQ0FDRjtRQUdKLElBQUk7ZUFDQyxJQUFJLENBQUMsT0FBTztlQUNaLGVBQWUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU07ZUFDckMsQ0FBQyxVQUFVO1lBQ2QsQ0FBQztnQkFDRCw2QkFBSyxLQUFLLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUztvQkFBRyxJQUFJLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsZUFBZSxDQUFDO29CQUNuSCw4QkFBTSxLQUFLLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxjQUFjLHdCQUFxQixDQUMxRjtZQUNOLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxFQUFFLENBQUMsRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUM7UUFHNUcsQ0FBQyxXQUFXLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxhQUFhLEtBQUssYUFBYSxDQUFDLFFBQVE7WUFDbkUsQ0FBQyxDQUFDLElBQUk7WUFDTixDQUFDLENBQUMsYUFBYSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDO2dCQUNqQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxTQUFTLFdBQUEsRUFBRSxlQUFlLGlCQUFBLEVBQUUsQ0FBQztnQkFDeEQsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxFQUFFLFdBQVcsYUFBQSxFQUFFLGVBQWUsaUJBQUEsRUFBRSxRQUFRLEVBQUUsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsVUFBVSxFQUFFLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLFlBQVksSUFBSSxFQUFFLEVBQUUsQ0FBQztRQUUxTSw2QkFBSyxLQUFLLEVBQUUsU0FBUyxDQUFDLG9CQUFvQixDQUFDLFNBQVM7WUFDbEQsNkJBQUssS0FBSyxFQUFFLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJO2dCQUM1QyxjQUFjLENBQUMsU0FBUyxDQUFDO2dCQUN6QixjQUFjLENBQUM7b0JBQ2QsUUFBUSxFQUFFLFNBQVM7b0JBQ25CLFNBQVMsRUFBRSxRQUFRLENBQUMsT0FBTztvQkFDM0IsS0FBSyxFQUFFLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJO29CQUMxQyxVQUFVLEVBQUUsU0FBUyxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQjtvQkFDM0QsT0FBTyxFQUFFLGlCQUFpQjtpQkFDM0IsQ0FBQztnQkFDRCxjQUFjLENBQUM7b0JBQ2QsUUFBUSxFQUFFLEtBQUs7b0JBQ2YsU0FBUyxFQUFFLFFBQVEsQ0FBQyxPQUFPO29CQUMzQixLQUFLLEVBQUUsU0FBUyxDQUFDLG9CQUFvQixDQUFDLElBQUk7b0JBQzFDLE1BQU0sRUFBRSxJQUFJO29CQUNaLFNBQVMsRUFBRSxJQUFJLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxFQUFFO29CQUN4QyxVQUFVLEVBQUUsU0FBUyxDQUFDLG9CQUFvQixDQUFDLFlBQVk7aUJBQ3hELENBQUMsQ0FDRTtZQUVKLElBQUksSUFBSSxJQUFJLENBQUMsYUFBYSxLQUFLLGFBQWEsQ0FBQyxRQUFRO21CQUNsRCxDQUNELDZCQUFLLEtBQUssRUFBRSxTQUFTLENBQUMsb0JBQW9CLENBQUMsS0FBSyxJQUM3QyxjQUFjLENBQUM7b0JBQ2QsUUFBUSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxlQUFlO29CQUNoRCxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTztvQkFDMUQsS0FBSyxFQUFFLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJO29CQUMxQyxNQUFNLEVBQUUsS0FBSztvQkFDYixTQUFTLEVBQUUsSUFBSSxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRTtvQkFDeEMsVUFBVSxFQUFFLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxjQUFjO29CQUN6RCxPQUFPLEVBQUU7d0JBQ1AsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDOzRCQUNoQyxPQUFPO2dDQUNMLENBQUMsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUM7Z0NBQzVCLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQzt3QkFDL0IsQ0FBQzt3QkFBQyxJQUFJLENBQUMsQ0FBQzs0QkFDTixTQUFTLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQTt3QkFDNUIsQ0FBQztvQkFDSCxDQUFDO2lCQUNGLENBQUMsQ0FDRSxDQUNQLENBRUM7UUFDTCxzQkFBc0IsRUFBRTtRQUV2QixjQUFjLElBQUksQ0FDaEIsNkJBQUssS0FBSyxFQUFFLFNBQVMsQ0FBQyxZQUFZO1lBRTlCLFdBQVcsS0FBSyxPQUFPLFdBQVc7Z0JBQ2hDLG1CQUFtQjtnQkFDbkIsQ0FBQyxDQUFDLG9CQUFDLE9BQU8sSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLGtCQUFrQixHQUFJO2dCQUM5QyxxQkFBcUI7Z0JBQ3JCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQzt1QkFDekIsV0FBVyxDQUFDLEdBQUcsQ0FBQyxVQUFDLE9BQU8sSUFBSyxPQUFBLENBQzlCLDZCQUFLLEdBQUcsRUFBRSxPQUFPLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsWUFBWSxDQUFDLFNBQVM7d0JBQzNELDZCQUFLLEtBQUssRUFBRSxDQUFDLEVBQUUsZUFBZSxFQUFFLFVBQVEsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLE9BQUksRUFBRSxFQUFFLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFRO3dCQUNwSSw2QkFBSyxLQUFLLEVBQUUsU0FBUyxDQUFDLFlBQVksQ0FBQyxXQUFXOzRCQUM1Qyw4QkFBTSxLQUFLLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxJQUFHLE9BQU8sQ0FBQyxTQUFTLENBQVE7NEJBQ3ZFLDhCQUFNLEtBQUssRUFBRSxTQUFTLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxPQUFPLElBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBUSxDQUM3RSxDQUNGLENBQ1AsRUFSK0IsQ0FRL0IsQ0FBQztZQUdKLGtCQUFrQjtnQkFDaEIsQ0FBQyxDQUFDLENBQ0EsNkJBQUssS0FBSyxFQUFFLFNBQVMsQ0FBQyxpQkFBaUI7b0JBQ3JDLHdDQUFTLFdBQVcsRUFBUTtvQkFDNUIsNkJBQUssS0FBSyxFQUFFLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUI7d0JBQ3ZELDBDQUFXLGlCQUFpQixFQUFVO3dCQUN0Qyw2QkFBSyxLQUFLLEVBQUUsU0FBUyxDQUFDLGlCQUFpQixDQUFDLFdBQVcsRUFBRSxPQUFPLEVBQUUsWUFBWSxlQUFXLENBQ2pGLENBQ0YsQ0FDUDtnQkFDRCxDQUFDLENBQUMsQ0FDQSw2QkFBSyxLQUFLLEVBQUUsU0FBUyxDQUFDLGlCQUFpQjtvQkFDckMsd0NBQVMsV0FBVyxFQUFRO29CQUM1Qiw2QkFBSyxLQUFLLEVBQUUsU0FBUyxDQUFDLGlCQUFpQixDQUFDLEtBQUssRUFBRSxPQUFPLEVBQUUsYUFBYSxJQUFHLFVBQVUsQ0FBTyxDQUNyRixDQUNQLENBRUQsQ0FDUDtRQUVILG9CQUFDLEtBQUssSUFBQyxLQUFLLEVBQUUsWUFBWSxDQUFDLE9BQU8sR0FBSSxDQUNsQyxDQUNQLENBQUM7QUFDSixDQUFDO0FBQUEsQ0FBQyJ9
// CONCATENATED MODULE: ./components/container/feed-item/initialize.tsx
var DEFAULT_PROPS = {
    limitTextLength: 150,
    isLastChild: false,
    showComment: false,
    isShowImage: true,
    isShowFullImage: false,
    isFeedDetail: false
};
var INITIAL_STATE = {
    isLike: false,
    isViewMore: false,
    isShowVideo: false,
    isResetInput: false,
    isShowComments: false,
    isShowInputComment: false,
    likeNum: 0,
    commentList: [],
    errorMessage: '',
    answerComment: '',
    inputComment: {
        value: '',
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixlQUFlLEVBQUUsR0FBRztJQUNwQixXQUFXLEVBQUUsS0FBSztJQUNsQixXQUFXLEVBQUUsS0FBSztJQUNsQixXQUFXLEVBQUUsSUFBSTtJQUNqQixlQUFlLEVBQUUsS0FBSztJQUN0QixZQUFZLEVBQUUsS0FBSztDQUNOLENBQUM7QUFFaEIsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHO0lBQzNCLE1BQU0sRUFBRSxLQUFLO0lBQ2IsVUFBVSxFQUFFLEtBQUs7SUFDakIsV0FBVyxFQUFFLEtBQUs7SUFDbEIsWUFBWSxFQUFFLEtBQUs7SUFDbkIsY0FBYyxFQUFFLEtBQUs7SUFDckIsa0JBQWtCLEVBQUUsS0FBSztJQUV6QixPQUFPLEVBQUUsQ0FBQztJQUVWLFdBQVcsRUFBRSxFQUFFO0lBRWYsWUFBWSxFQUFFLEVBQUU7SUFDaEIsYUFBYSxFQUFFLEVBQUU7SUFFakIsWUFBWSxFQUFFO1FBQ1osS0FBSyxFQUFFLEVBQUU7S0FDVjtDQUNZLENBQUMifQ==
// CONCATENATED MODULE: ./components/container/feed-item/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var component_FeedItem = /** @class */ (function (_super) {
    __extends(FeedItem, _super);
    function FeedItem(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    FeedItem.prototype.handleViewMore = function () {
        this.setState({ isViewMore: true });
    };
    ;
    FeedItem.prototype.handleShowComment = function () {
        this.autoShowComment(this.props);
    };
    ;
    FeedItem.prototype.autoShowComment = function (props) {
        if (props === void 0) { props = this.props; }
        var item = props.item, fecthActivityFeedCommentListAction = props.fecthActivityFeedCommentListAction, commentList = props.activityFeedStore.activityFeedCommentList.commentList;
        if (!Object(validate["j" /* isEmptyObject */])(item)) {
            var keyHash = Object(encode["h" /* objectToHash */])({ id: item.id });
            var param = { id: item.id, page: 1, perPage: 20 };
            commentList && !Object(validate["l" /* isUndefined */])(commentList[keyHash])
                ? this.setState({ commentList: commentList[keyHash] })
                : fecthActivityFeedCommentListAction(param);
            this.setState({ isShowComments: true });
        }
    };
    ;
    FeedItem.prototype.handleShowInputComment = function () {
        this.inputCommentRef && this.inputCommentRef.focus();
        this.setState({ isShowInputComment: true });
    };
    ;
    FeedItem.prototype.handleSubmit = function () {
        var answerComment = this.state.answerComment;
        var _a = this.props, item = _a.item, addActivityFeedCommentAction = _a.addActivityFeedCommentAction;
        var comment = answerComment && answerComment.trim() || '';
        if (0 === comment.length) {
            return;
        }
        addActivityFeedCommentAction({
            id: item.id,
            content: comment
        });
    };
    FeedItem.prototype.handleLike = function () {
        var _a = this.props, addActivityFeedLikeAction = _a.addActivityFeedLikeAction, deleteActivityFeedLikeAction = _a.deleteActivityFeedLikeAction;
        var likeNumber = this.state.likeNum || 0;
        var tmpLikeNum = 0;
        if (this.state.isLike) {
            tmpLikeNum = likeNumber > 0 ? likeNumber - 1 : 0;
            deleteActivityFeedLikeAction({ id: this.props.item.id });
        }
        else {
            tmpLikeNum = likeNumber + 1;
            addActivityFeedLikeAction({ id: this.props.item.id });
        }
        this.setState({
            isLike: !this.state.isLike,
            likeNum: tmpLikeNum
        });
    };
    FeedItem.prototype.handleInputOnChange = function (e) {
        this.setState({ answerComment: e.target.value });
    };
    FeedItem.prototype.handleInputOnFocus = function () {
        this.setState({ errorMessage: '' });
    };
    FeedItem.prototype.handleShowVideo = function () {
        this.setState({ isShowVideo: true });
    };
    FeedItem.prototype.setInputCommentRef = function (ref) {
        this.inputCommentRef = ref;
    };
    FeedItem.prototype.componentDidMount = function () {
        var _a = this.props, item = _a.item, showComment = _a.showComment;
        showComment && this.handleShowComment();
        this.setState({
            isLike: item.liked,
            likeNum: item.total_likes || 0
        });
    };
    FeedItem.prototype.componentWillReceiveProps = function (nextProps) {
        var _a = this.props, item = _a.item, _b = _a.activityFeedStore, isAddCommentSuccess = _b.isAddCommentSuccess, isFetchActivityFeedDetailSuccess = _b.isFetchActivityFeedDetailSuccess, isFetchCommentListSuccess = _b.activityFeedCommentList.isFetchCommentListSuccess;
        var commentList = nextProps.activityFeedStore.activityFeedCommentList.commentList;
        var keyHash = Object(encode["h" /* objectToHash */])({ id: item && item.id || 0 });
        var _commentList = commentList && !Object(validate["l" /* isUndefined */])(commentList[keyHash]) ? commentList[keyHash] : this.state.commentList;
        !isFetchCommentListSuccess
            && nextProps.activityFeedStore.activityFeedCommentList.isFetchCommentListSuccess
            && this.setState({ commentList: _commentList });
        !isAddCommentSuccess
            && nextProps.activityFeedStore.isAddCommentSuccess
            && this.setState({ commentList: _commentList, answerComment: '' });
        !isFetchActivityFeedDetailSuccess
            && nextProps.activityFeedStore.isFetchActivityFeedDetailSuccess
            && this.autoShowComment(nextProps);
    };
    FeedItem.prototype.shouldComponentUpdate = function (nextProps, nextState) {
        if ((false === this.state.isShowComments && true === nextState.isShowComments)) {
            return true;
        }
        ;
        if ((false === this.state.isViewMore && true === nextState.isViewMore)) {
            return true;
        }
        ;
        if ((false === this.state.isShowVideo && true === nextState.isShowVideo)) {
            return true;
        }
        ;
        if (this.state.likeNum !== nextState.likeNum) {
            return true;
        }
        ;
        // const keyHash = objectToHash({ id: nextProps.item.id });
        // if (true === this.state.isShowComments
        //   //&& this.props.item.id !== nextProps.item.id
        //   && nextProps.activityFeedStore.activityFeedCommentList
        //   && nextProps.activityFeedStore.activityFeedCommentList.commentList
        //   && false === isUndefined(nextProps.activityFeedStore.activityFeedCommentList.commentList[keyHash])) { return true; };
        if (true === this.state.isShowComments) {
            return true;
        }
        ;
        if (this.state.answerComment !== nextState.answerComment) {
            return true;
        }
        ;
        if (nextProps.isFeedDetail !== this.props.isFeedDetail) {
            return true;
        }
        ;
        if (!Object(validate["h" /* isCompareObject */])(this.props.item, nextState.item)) {
            return true;
        }
        ;
        if (!Object(validate["h" /* isCompareObject */])(nextProps.userProfile, this.props.userProfile)) {
            return true;
        }
        ;
        return false;
    };
    FeedItem.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state,
            handleViewMore: this.handleViewMore.bind(this),
            handleShowComment: this.handleShowComment.bind(this),
            handleShowInputComment: this.handleShowInputComment.bind(this),
            handleSubmit: this.handleSubmit.bind(this),
            handleLike: this.handleLike.bind(this),
            handleInputOnChange: this.handleInputOnChange.bind(this),
            handleInputOnFocus: this.handleInputOnFocus.bind(this),
            handleShowVideo: this.handleShowVideo.bind(this),
            setInputCommentRef: this.setInputCommentRef.bind(this)
        };
        return renderView(args);
    };
    ;
    FeedItem.defaultProps = DEFAULT_PROPS;
    FeedItem = __decorate([
        radium
    ], FeedItem);
    return FeedItem;
}(react["Component"]));
/* harmony default export */ var component = (component_FeedItem);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3JELE9BQU8sRUFBRSxXQUFXLEVBQUUsZUFBZSxFQUFFLGFBQWEsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBR3RGLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFDcEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFHNUQ7SUFBdUIsNEJBQXVDO0lBSTVELGtCQUFZLEtBQUs7UUFBakIsWUFDRSxrQkFBTSxLQUFLLENBQUMsU0FFYjtRQURDLEtBQUksQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDOztJQUM3QixDQUFDO0lBRUQsaUNBQWMsR0FBZDtRQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxVQUFVLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBQUEsQ0FBQztJQUVGLG9DQUFpQixHQUFqQjtRQUNFLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFBQSxDQUFDO0lBRUYsa0NBQWUsR0FBZixVQUFnQixLQUFrQjtRQUFsQixzQkFBQSxFQUFBLFFBQVEsSUFBSSxDQUFDLEtBQUs7UUFFOUIsSUFBQSxpQkFBSSxFQUNKLDZFQUFrQyxFQUNjLHlFQUFXLENBQ25EO1FBRVYsRUFBRSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQU0sT0FBTyxHQUFHLFlBQVksQ0FBQyxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUM5QyxJQUFNLEtBQUssR0FBRyxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDO1lBRXBELFdBQVcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQy9DLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsV0FBVyxFQUFFLFdBQVcsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDO2dCQUN0RCxDQUFDLENBQUMsa0NBQWtDLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFOUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLGNBQWMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBQzFDLENBQUM7SUFDSCxDQUFDO0lBQUEsQ0FBQztJQUVGLHlDQUFzQixHQUF0QjtRQUNFLElBQUksQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNyRCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsa0JBQWtCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBQUEsQ0FBQztJQUVGLCtCQUFZLEdBQVo7UUFDVSxJQUFBLHdDQUFhLENBQThCO1FBQzdDLElBQUEsZUFBaUUsRUFBL0QsY0FBSSxFQUFFLDhEQUE0QixDQUE4QjtRQUV4RSxJQUFNLE9BQU8sR0FBRyxhQUFhLElBQUksYUFBYSxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQztRQUM1RCxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDekIsTUFBTSxDQUFDO1FBQ1QsQ0FBQztRQUVELDRCQUE0QixDQUFDO1lBQzNCLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRTtZQUNYLE9BQU8sRUFBRSxPQUFPO1NBQ2pCLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCw2QkFBVSxHQUFWO1FBQ1EsSUFBQSxlQUF3RSxFQUF0RSx3REFBeUIsRUFBRSw4REFBNEIsQ0FBZ0I7UUFDL0UsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLElBQUksQ0FBQyxDQUFDO1FBQzNDLElBQUksVUFBVSxHQUFHLENBQUMsQ0FBQztRQUVuQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDdEIsVUFBVSxHQUFHLFVBQVUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqRCw0QkFBNEIsQ0FBQyxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzNELENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLFVBQVUsR0FBRyxVQUFVLEdBQUcsQ0FBQyxDQUFDO1lBQzVCLHlCQUF5QixDQUFDLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDeEQsQ0FBQztRQUVELElBQUksQ0FBQyxRQUFRLENBQUM7WUFDWixNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU07WUFDMUIsT0FBTyxFQUFFLFVBQVU7U0FDcEIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHNDQUFtQixHQUFuQixVQUFvQixDQUFDO1FBQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRCxxQ0FBa0IsR0FBbEI7UUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBZ0IsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRCxrQ0FBZSxHQUFmO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQWdCLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQscUNBQWtCLEdBQWxCLFVBQW1CLEdBQUc7UUFDcEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxHQUFHLENBQUM7SUFDN0IsQ0FBQztJQUVELG9DQUFpQixHQUFqQjtRQUNRLElBQUEsZUFBa0MsRUFBaEMsY0FBSSxFQUFFLDRCQUFXLENBQWdCO1FBRXpDLFdBQVcsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUV4QyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ1osTUFBTSxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2xCLE9BQU8sRUFBRSxJQUFJLENBQUMsV0FBVyxJQUFJLENBQUM7U0FDL0IsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDRDQUF5QixHQUF6QixVQUEwQixTQUFTO1FBQzNCLElBQUEsZUFPUSxFQU5aLGNBQUksRUFDSix5QkFJQyxFQUhDLDRDQUFtQixFQUNuQixzRUFBZ0MsRUFDTCxnRkFBeUIsQ0FFekM7UUFFeUMsSUFBQSw2RUFBVyxDQUFtQjtRQUV0RixJQUFNLE9BQU8sR0FBRyxZQUFZLENBQUMsRUFBRSxFQUFFLEVBQUUsSUFBSSxJQUFJLElBQUksQ0FBQyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUUzRCxJQUFNLFlBQVksR0FBRyxXQUFXLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUM7UUFFdkgsQ0FBQyx5QkFBeUI7ZUFDckIsU0FBUyxDQUFDLGlCQUFpQixDQUFDLHVCQUF1QixDQUFDLHlCQUF5QjtlQUM3RSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxDQUFDLENBQUM7UUFFbEQsQ0FBQyxtQkFBbUI7ZUFDZixTQUFTLENBQUMsaUJBQWlCLENBQUMsbUJBQW1CO2VBQy9DLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUFFLGFBQWEsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBRXJFLENBQUMsZ0NBQWdDO2VBQzVCLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxnQ0FBZ0M7ZUFDNUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRUQsd0NBQXFCLEdBQXJCLFVBQXNCLFNBQVMsRUFBRSxTQUFTO1FBRXhDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxJQUFJLElBQUksS0FBSyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUFDLENBQUM7UUFBQSxDQUFDO1FBQ2pHLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxJQUFJLElBQUksS0FBSyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUFDLENBQUM7UUFBQSxDQUFDO1FBQ3pGLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxJQUFJLElBQUksS0FBSyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUFDLENBQUM7UUFBQSxDQUFDO1FBQzNGLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxLQUFLLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUFDLENBQUM7UUFBQSxDQUFDO1FBRS9ELDJEQUEyRDtRQUMzRCx5Q0FBeUM7UUFDekMsa0RBQWtEO1FBQ2xELDJEQUEyRDtRQUMzRCx1RUFBdUU7UUFDdkUsMEhBQTBIO1FBRTFILEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQUMsQ0FBQztRQUFBLENBQUM7UUFDekQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEtBQUssU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQUMsQ0FBQztRQUFBLENBQUM7UUFDM0UsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLFlBQVksS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQUMsQ0FBQztRQUFBLENBQUM7UUFDekUsRUFBRSxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFBQyxDQUFDO1FBQUEsQ0FBQztRQUN4RSxFQUFFLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUFDLENBQUM7UUFBQSxDQUFDO1FBRXRGLE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQseUJBQU0sR0FBTjtRQUNFLElBQU0sSUFBSSxHQUFHO1lBQ1gsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixjQUFjLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzlDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ3BELHNCQUFzQixFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzlELFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUMsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUN0QyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUN4RCxrQkFBa0IsRUFBRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUN0RCxlQUFlLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2hELGtCQUFrQixFQUFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ3ZELENBQUE7UUFFRCxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFBQSxDQUFDO0lBektLLHFCQUFZLEdBQWUsYUFBYSxDQUFDO0lBRDVDLFFBQVE7UUFEYixNQUFNO09BQ0QsUUFBUSxDQTJLYjtJQUFELGVBQUM7Q0FBQSxBQTNLRCxDQUF1QixLQUFLLENBQUMsU0FBUyxHQTJLckM7QUFDRCxlQUFlLFFBQVEsQ0FBQyJ9
// CONCATENATED MODULE: ./components/container/feed-item/store.tsx
var connect = __webpack_require__(129).connect;




var mapStateToProps = function (state) { return ({
    listLikedId: state.like.liked.id,
    activityFeedStore: state.activityFeed
}); };
var mapDispatchToProps = function (dispatch) { return ({
    fecthActivityFeedCommentListAction: function (data) { return dispatch(Object(activity_feed["d" /* fecthActivityFeedCommentListAction */])(data)); },
    addActivityFeedCommentAction: function (data) { return dispatch(Object(activity_feed["a" /* addActivityFeedCommentAction */])(data)); },
    addActivityFeedLikeAction: function (data) { return dispatch(Object(activity_feed["b" /* addActivityFeedLikeAction */])(data)); },
    deleteActivityFeedLikeAction: function (data) { return dispatch(Object(activity_feed["c" /* deleteActivityFeedLikeAction */])(data)); },
    openModal: function (data) { return dispatch(Object(modal["c" /* openModalAction */])(data)); },
    likeProduct: function (productId) { return dispatch(Object(like["d" /* likeProductAction */])(productId)); },
    unLikeProduct: function (productId) { return dispatch(Object(like["a" /* UnLikeProductAction */])(productId)); },
}); };
/* harmony default export */ var store = (connect(mapStateToProps, mapDispatchToProps)(component));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLEVBQ0wsa0NBQWtDLEVBQ2xDLDRCQUE0QixFQUM1Qix5QkFBeUIsRUFDekIsNEJBQTRCLEVBQzdCLE1BQU0sK0JBQStCLENBQUM7QUFDdkMsT0FBTyxFQUFFLGlCQUFpQixFQUFFLG1CQUFtQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDOUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBRXhELE9BQU8sUUFBUSxNQUFNLGFBQWEsQ0FBQztBQUVuQyxNQUFNLENBQUMsSUFBTSxlQUFlLEdBQUcsVUFBQyxLQUFLLElBQUssT0FBQSxDQUFDO0lBQ3pDLFdBQVcsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO0lBQ2hDLGlCQUFpQixFQUFFLEtBQUssQ0FBQyxZQUFZO0NBQ3RDLENBQUMsRUFId0MsQ0FHeEMsQ0FBQztBQUVILE1BQU0sQ0FBQyxJQUFNLGtCQUFrQixHQUFHLFVBQUMsUUFBUSxJQUFLLE9BQUEsQ0FBQztJQUMvQyxrQ0FBa0MsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyxrQ0FBa0MsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFsRCxDQUFrRDtJQUNyRyw0QkFBNEIsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyw0QkFBNEIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUE1QyxDQUE0QztJQUN6Rix5QkFBeUIsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUF6QyxDQUF5QztJQUNuRiw0QkFBNEIsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyw0QkFBNEIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUE1QyxDQUE0QztJQUN6RixTQUFTLEVBQUUsVUFBQyxJQUFTLElBQUssT0FBQSxRQUFRLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQS9CLENBQStCO0lBQ3pELFdBQVcsRUFBRSxVQUFDLFNBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUF0QyxDQUFzQztJQUNsRSxhQUFhLEVBQUUsVUFBQyxTQUFTLElBQUssT0FBQSxRQUFRLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBeEMsQ0FBd0M7Q0FDdkUsQ0FBQyxFQVI4QyxDQVE5QyxDQUFDO0FBR0gsZUFBZSxPQUFPLENBQ3BCLGVBQWUsRUFDZixrQkFBa0IsQ0FDbkIsQ0FBQyxRQUFRLENBQUMsQ0FBQyJ9
// CONCATENATED MODULE: ./components/container/feed-item/index.tsx

/* harmony default export */ var feed_item = __webpack_exports__["a"] = (store);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxRQUFRLE1BQU0sU0FBUyxDQUFDO0FBQy9CLGVBQWUsUUFBUSxDQUFDIn0=

/***/ }),

/***/ 774:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FEEDABLE_TYPE; });
var FEEDABLE_TYPE = {
    FEEDBACK: 'Feedback',
    BLOG: 'Blog',
    LOVE: 'Love',
    THEME: 'Theme',
    BRAND: 'Brand',
    DISCOUNTCODE: 'DiscountCode',
    UNBOXING: 'Unboxing',
    BOX: 'Box',
    BROWSENODE: 'BrowseNode'
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmVlZGFibGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJmZWVkYWJsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUc7SUFDM0IsUUFBUSxFQUFFLFVBQVU7SUFDcEIsSUFBSSxFQUFFLE1BQU07SUFDWixJQUFJLEVBQUUsTUFBTTtJQUNaLEtBQUssRUFBRSxPQUFPO0lBQ2QsS0FBSyxFQUFFLE9BQU87SUFDZCxZQUFZLEVBQUUsY0FBYztJQUM1QixRQUFRLEVBQUUsVUFBVTtJQUNwQixHQUFHLEVBQUUsS0FBSztJQUNWLFVBQVUsRUFBRSxZQUFZO0NBQ3pCLENBQUMifQ==

/***/ }),

/***/ 778:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/image.ts
var utils_image = __webpack_require__(348);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./components/container/image-slider-community/initialize.tsx
var DEFAULT_PROPS = {
    data: [],
    column: 1,
    posImg: 0
};
var INITIAL_STATE = function (data) { return ({
    imageList: data || [],
    imageSlide: [],
    imageSlideSelected: {},
    countChangeSlide: 0,
    firstInit: false
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLE1BQU0sRUFBRSxDQUFDO0lBQ1QsTUFBTSxFQUFFLENBQUM7Q0FDQSxDQUFDO0FBRVosTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHLFVBQUMsSUFBUyxJQUFLLE9BQUEsQ0FBQztJQUMzQyxTQUFTLEVBQUUsSUFBSSxJQUFJLEVBQUU7SUFDckIsVUFBVSxFQUFFLEVBQUU7SUFDZCxrQkFBa0IsRUFBRSxFQUFFO0lBQ3RCLGdCQUFnQixFQUFFLENBQUM7SUFDbkIsU0FBUyxFQUFFLEtBQUs7Q0FDTixDQUFBLEVBTmdDLENBTWhDLENBQUMifQ==
// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// EXTERNAL MODULE: ./style/component.ts
var component = __webpack_require__(744);

// CONCATENATED MODULE: ./components/container/image-slider-item-community/initialize.tsx
var initialize_DEFAULT_PROPS = {
    item: [],
    type: '',
    column: 4
};
var initialize_INITIAL_STATE = {
    isRenderAgain: false
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLElBQUksRUFBRSxFQUFFO0lBQ1IsTUFBTSxFQUFFLENBQUM7Q0FDQSxDQUFDO0FBRVosTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHO0lBQzNCLGFBQWEsRUFBRSxLQUFLO0NBQ1gsQ0FBQyJ9
// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/container/image-slider-item-community/style.tsx


var generateSwitchStyle = function (mobile, desktop) {
    var switchStyle = {
        MOBILE: { width: mobile, minWidth: mobile },
        DESKTOP: { width: desktop, cursor: 'pointer' }
    };
    return switchStyle[window.DEVICE_VERSION];
};
/* harmony default export */ var style = ({
    heading: {
        paddingLeft: 10,
        display: variable["display"].inlineBlock,
        maxWidth: "100%",
        whiteSpace: "nowrap",
        overflow: "hidden",
        textOverflow: "ellipsis",
        fontFamily: variable["fontAvenirMedium"],
        color: variable["colorBlack"],
        fontSize: 20,
        lineHeight: "40px",
        height: 40,
        letterSpacing: -0.5,
        textTransform: "uppercase",
    },
    container: {
        width: '100%',
        overflowX: 'auto',
        whiteSpace: 'nowrap',
        paddingTop: 0,
        marginBottom: 0,
        display: variable["display"].flex,
        itemSlider: {
            width: "100%",
            height: "100%",
            display: variable["display"].inlineBlock,
            borderRadius: 5,
            overflow: 'hidden',
            boxShadow: variable["shadowBlur"],
            position: variable["position"].relative
        },
        itemSliderPanel: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    height: '100%',
                    maxWidth: '100vw',
                    width: '100vw'
                }],
            DESKTOP: [{
                    height: 'auto',
                    maxHeight: '90vh',
                    maxWidth: "calc(90vw - 400px)",
                    position: variable["position"].relative,
                    backgroundColor: variable["colorF7"],
                    display: variable["display"].block,
                    minHeight: 400,
                    minWidth: 400
                }],
            GENERAL: [{
                    objectFit: 'contain',
                }]
        }),
        videoIcon: {
            width: 70,
            height: 70,
            position: variable["position"].absolute,
            top: '50%',
            left: '55%',
            transform: 'translate(-50%, -50%)',
            borderTop: '35px solid transparent',
            boxSizing: 'border-box',
            borderLeft: '51px solid white',
            borderBottom: '35px solid transparent',
            opacity: .8
        },
        info: {
            width: '100%',
            height: 110,
            padding: 12,
            position: variable["position"].relative,
            background: variable["colorWhite"],
            display: variable["display"].flex,
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
            image: function (imgUrl) { return ({
                width: '100%',
                height: '100%',
                top: 0,
                left: 0,
                zIndex: -1,
                position: variable["position"].absolute,
                backgroundColor: variable["colorF7"],
                backgroundImage: "url(" + imgUrl + ")",
                backgroundPosition: 'bottom center',
                backgroundSize: 'cover',
                filter: 'blur(4px)',
                transform: "scaleY(-1) scale(1.1)",
                'WebkitBackfaceVisibility': 'hidden',
                'WebkitPerspective': 1000,
                'WebkitTransform': ['translate3d(0,0,0)', 'translateZ(0)'],
                'backfaceVisibility': 'hidden',
                perspective: 1000,
            }); },
            title: {
                color: variable["colorBlack"],
                whiteSpace: 'pre-wrap',
                fontFamily: variable["fontAvenirMedium"],
                fontSize: 14,
                lineHeight: '22px',
                maxHeight: '44px',
                overflow: 'hidden',
                marginBottom: 5
            },
            description: {
                fontSize: 12,
                color: variable["colorBlack"],
                fontFamily: variable["fontAvenirMedium"],
                opacity: .7,
                marginRight: 10,
                whiteSpace: 'pre-wrap',
                lineHeight: '18px',
                maxHeight: 36,
                overflow: 'hidden',
            },
            tagList: {
                width: '100%',
                display: variable["display"].flex,
                flexWrap: 'wrap',
                height: '36px',
                maxHeight: '36px',
                overflow: 'hidden'
            },
            tagItem: {
                fontSize: 12,
                lineHeight: '18px',
                color: variable["colorBlack"],
                fontFamily: variable["fontAvenirMedium"],
                opacity: .7,
                marginRight: 10,
                whiteSpace: 'pre-wrap'
            },
        }
    },
    column: {
        1: { width: '100%' },
        2: { width: '50%' },
        3: generateSwitchStyle('75%', '33.33%'),
        4: generateSwitchStyle('47%', '25%'),
        5: generateSwitchStyle('47%', '20%'),
        6: generateSwitchStyle('50%', '16.66%'),
    },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFekQsSUFBTSxtQkFBbUIsR0FBRyxVQUFDLE1BQU0sRUFBRSxPQUFPO0lBQzFDLElBQU0sV0FBVyxHQUFHO1FBQ2xCLE1BQU0sRUFBRSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRTtRQUMzQyxPQUFPLEVBQUUsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUU7S0FDL0MsQ0FBQztJQUVGLE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0FBQzVDLENBQUMsQ0FBQztBQUVGLGVBQWU7SUFDYixPQUFPLEVBQUU7UUFDUCxXQUFXLEVBQUUsRUFBRTtRQUNmLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLFdBQVc7UUFDckMsUUFBUSxFQUFFLE1BQU07UUFDaEIsVUFBVSxFQUFFLFFBQVE7UUFDcEIsUUFBUSxFQUFFLFFBQVE7UUFDbEIsWUFBWSxFQUFFLFVBQVU7UUFDeEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7UUFDckMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1FBQzFCLFFBQVEsRUFBRSxFQUFFO1FBQ1osVUFBVSxFQUFFLE1BQU07UUFDbEIsTUFBTSxFQUFFLEVBQUU7UUFDVixhQUFhLEVBQUUsQ0FBQyxHQUFHO1FBQ25CLGFBQWEsRUFBRSxXQUFXO0tBQzNCO0lBRUQsU0FBUyxFQUFFO1FBQ1QsS0FBSyxFQUFFLE1BQU07UUFDYixTQUFTLEVBQUUsTUFBTTtRQUNqQixVQUFVLEVBQUUsUUFBUTtRQUNwQixVQUFVLEVBQUUsQ0FBQztRQUNiLFlBQVksRUFBRSxDQUFDO1FBQ2YsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUU5QixVQUFVLEVBQUU7WUFDVixLQUFLLEVBQUUsTUFBTTtZQUNiLE1BQU0sRUFBRSxNQUFNO1lBQ2QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztZQUNyQyxZQUFZLEVBQUUsQ0FBQztZQUNmLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTtZQUM5QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1NBQ3JDO1FBRUQsZUFBZSxFQUFFLFlBQVksQ0FBQztZQUM1QixNQUFNLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsTUFBTTtvQkFDZCxRQUFRLEVBQUUsT0FBTztvQkFDakIsS0FBSyxFQUFFLE9BQU87aUJBQ2YsQ0FBQztZQUVGLE9BQU8sRUFBRSxDQUFDO29CQUNSLE1BQU0sRUFBRSxNQUFNO29CQUNkLFNBQVMsRUFBRSxNQUFNO29CQUNqQixRQUFRLEVBQUUsb0JBQW9CO29CQUM5QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUNwQyxlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87b0JBQ2pDLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7b0JBQy9CLFNBQVMsRUFBRSxHQUFHO29CQUNkLFFBQVEsRUFBRSxHQUFHO2lCQUNkLENBQUM7WUFFRixPQUFPLEVBQUUsQ0FBQztvQkFDUixTQUFTLEVBQUUsU0FBUztpQkFDckIsQ0FBQztTQUNILENBQUM7UUFFRixTQUFTLEVBQUU7WUFDVCxLQUFLLEVBQUUsRUFBRTtZQUNULE1BQU0sRUFBRSxFQUFFO1lBQ1YsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxHQUFHLEVBQUUsS0FBSztZQUNWLElBQUksRUFBRSxLQUFLO1lBQ1gsU0FBUyxFQUFFLHVCQUF1QjtZQUNsQyxTQUFTLEVBQUUsd0JBQXdCO1lBQ25DLFNBQVMsRUFBRSxZQUFZO1lBQ3ZCLFVBQVUsRUFBRSxrQkFBa0I7WUFDOUIsWUFBWSxFQUFFLHdCQUF3QjtZQUN0QyxPQUFPLEVBQUUsRUFBRTtTQUNaO1FBRUQsSUFBSSxFQUFFO1lBQ0osS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsR0FBRztZQUNYLE9BQU8sRUFBRSxFQUFFO1lBQ1gsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDL0IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixhQUFhLEVBQUUsUUFBUTtZQUN2QixjQUFjLEVBQUUsWUFBWTtZQUM1QixVQUFVLEVBQUUsWUFBWTtZQUV4QixLQUFLLEVBQUUsVUFBQyxNQUFNLElBQUssT0FBQSxDQUFDO2dCQUNsQixLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsTUFBTTtnQkFDZCxHQUFHLEVBQUUsQ0FBQztnQkFDTixJQUFJLEVBQUUsQ0FBQztnQkFDUCxNQUFNLEVBQUUsQ0FBQyxDQUFDO2dCQUNWLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztnQkFDakMsZUFBZSxFQUFFLFNBQU8sTUFBTSxNQUFHO2dCQUNqQyxrQkFBa0IsRUFBRSxlQUFlO2dCQUNuQyxjQUFjLEVBQUUsT0FBTztnQkFDdkIsTUFBTSxFQUFFLFdBQVc7Z0JBQ25CLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLDBCQUEwQixFQUFFLFFBQVE7Z0JBQ3BDLG1CQUFtQixFQUFFLElBQUk7Z0JBQ3pCLGlCQUFpQixFQUFFLENBQUMsb0JBQW9CLEVBQUUsZUFBZSxDQUFDO2dCQUMxRCxvQkFBb0IsRUFBRSxRQUFRO2dCQUM5QixXQUFXLEVBQUUsSUFBSTthQUNsQixDQUFDLEVBbEJpQixDQWtCakI7WUFFRixLQUFLLEVBQUU7Z0JBQ0wsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMxQixVQUFVLEVBQUUsVUFBVTtnQkFDdEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxNQUFNO2dCQUNsQixTQUFTLEVBQUUsTUFBTTtnQkFDakIsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFlBQVksRUFBRSxDQUFDO2FBQ2hCO1lBRUQsV0FBVyxFQUFFO2dCQUNYLFFBQVEsRUFBRSxFQUFFO2dCQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLE9BQU8sRUFBRSxFQUFFO2dCQUNYLFdBQVcsRUFBRSxFQUFFO2dCQUNmLFVBQVUsRUFBRSxVQUFVO2dCQUN0QixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsU0FBUyxFQUFFLEVBQUU7Z0JBQ2IsUUFBUSxFQUFFLFFBQVE7YUFDbkI7WUFFRCxPQUFPLEVBQUU7Z0JBQ1AsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsUUFBUSxFQUFFLE1BQU07Z0JBQ2hCLE1BQU0sRUFBRSxNQUFNO2dCQUNkLFNBQVMsRUFBRSxNQUFNO2dCQUNqQixRQUFRLEVBQUUsUUFBUTthQUNuQjtZQUVELE9BQU8sRUFBRTtnQkFDUCxRQUFRLEVBQUUsRUFBRTtnQkFDWixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtnQkFDckMsT0FBTyxFQUFFLEVBQUU7Z0JBQ1gsV0FBVyxFQUFFLEVBQUU7Z0JBQ2YsVUFBVSxFQUFFLFVBQVU7YUFDdkI7U0FDRjtLQUNGO0lBRUQsTUFBTSxFQUFFO1FBQ04sQ0FBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRTtRQUNwQixDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFO1FBQ25CLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDO1FBQ3ZDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDO1FBQ3BDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDO1FBQ3BDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDO0tBQ3hDO0NBQ3FCLENBQUMifQ==
// CONCATENATED MODULE: ./components/container/image-slider-item-community/view.tsx


function renderComponent(_a) {
    var props = _a.props;
    var _b = props, item = _b.item, column = _b.column;
    return (react["createElement"]("div", { style: style.column[column || 1] },
        react["createElement"]("img", { style: style.container.itemSliderPanel, src: item && item.original_url || '', id: "img-community" })));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRy9CLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixNQUFNLDBCQUEwQixFQUFTO1FBQVAsZ0JBQUs7SUFDL0IsSUFBQSxVQUFrQyxFQUFoQyxjQUFJLEVBQUUsa0JBQU0sQ0FBcUI7SUFFekMsTUFBTSxDQUFDLENBQ0wsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQztRQUNuQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxlQUFlLEVBQUUsR0FBRyxFQUFFLElBQUksSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLEVBQUUsRUFBRSxFQUFFLEVBQUUsZUFBZSxHQUFJLENBQ3RHLENBQ1AsQ0FBQztBQUNKLENBQUM7QUFBQSxDQUFDIn0=
// CONCATENATED MODULE: ./components/container/image-slider-item-community/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var component_SlideItem = /** @class */ (function (_super) {
    __extends(SlideItem, _super);
    function SlideItem(props) {
        var _this = _super.call(this, props) || this;
        _this.state = initialize_INITIAL_STATE;
        return _this;
    }
    SlideItem.prototype.componentDidMount = function () {
        Object(responsive["b" /* isDesktopVersion */])() && this.setState({ isRenderAgain: true }); // Set state that target
    };
    SlideItem.prototype.componentDidUpdate = function () {
        if (Object(responsive["b" /* isDesktopVersion */])()) {
            var el = document.getElementById('img-community');
            el && this.props.handleOmitImgHeight(el.clientHeight);
        }
    };
    SlideItem.prototype.render = function () {
        var args = {
            props: this.props
        };
        return renderComponent(args);
    };
    SlideItem.defaultProps = initialize_DEFAULT_PROPS;
    SlideItem = __decorate([
        radium
    ], SlideItem);
    return SlideItem;
}(react["Component"]));
;
/* harmony default export */ var image_slider_item_community_component = (component_SlideItem);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFN0QsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDNUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUl6QztJQUF3Qiw2QkFBK0I7SUFHckQsbUJBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUM7O0lBQzdCLENBQUM7SUFFRCxxQ0FBaUIsR0FBakI7UUFDRSxnQkFBZ0IsRUFBRSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxhQUFhLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLHdCQUF3QjtJQUN4RixDQUFDO0lBRUQsc0NBQWtCLEdBQWxCO1FBQ0UsRUFBRSxDQUFDLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDdkIsSUFBTSxFQUFFLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUNwRCxFQUFFLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDeEQsQ0FBQztJQUNILENBQUM7SUFFRCwwQkFBTSxHQUFOO1FBQ0UsSUFBTSxJQUFJLEdBQUc7WUFDWCxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7U0FDbEIsQ0FBQztRQUVGLE1BQU0sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQXhCTSxzQkFBWSxHQUFXLGFBQWEsQ0FBQztJQUR4QyxTQUFTO1FBRGQsTUFBTTtPQUNELFNBQVMsQ0EwQmQ7SUFBRCxnQkFBQztDQUFBLEFBMUJELENBQXdCLEtBQUssQ0FBQyxTQUFTLEdBMEJ0QztBQUFBLENBQUM7QUFFRixlQUFlLFNBQVMsQ0FBQyJ9
// CONCATENATED MODULE: ./components/container/image-slider-item-community/index.tsx

/* harmony default export */ var image_slider_item_community = (image_slider_item_community_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxTQUFTLE1BQU0sYUFBYSxDQUFDO0FBQ3BDLGVBQWUsU0FBUyxDQUFDIn0=
// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// CONCATENATED MODULE: ./components/container/image-slider-community/style.tsx




var INLINE_STYLE = {
    '.image-slide-container .pagination': {
        opacity: 0,
        transform: 'translateY(20px)'
    },
    '.image-slide-container:hover .pagination': {
        opacity: 1,
        transform: 'translateY(0)'
    },
    '.image-slide-container .left-nav': {
        width: 40,
        height: 60,
        opacity: 0,
        transform: 'translate(-60px, -50%)',
        visibility: 'hidden',
    },
    '.image-slide-container:hover .left-nav': {
        opacity: 1,
        transform: 'translate(1px, -50%)',
        visibility: 'visible',
    },
    '.image-slide-container .right-nav': {
        width: 40,
        height: 60,
        opacity: 0,
        transform: 'translate(60px, -50%)',
        visibility: 'hidden',
    },
    '.image-slide-container:hover .right-nav': {
        opacity: 1,
        transform: 'translate(1px, -50%)',
        visibility: 'visible',
    }
};
/* harmony default export */ var image_slider_community_style = ({
    container: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{}],
        DESKTOP: [{}],
        GENERAL: [{
                display: variable["display"].block,
                width: '100%',
                height: '100%'
            }]
    }),
    imageSlide: {
        position: 'relative',
        overflow: 'hidden',
        height: '100%',
        container: Object.assign({}, layout["a" /* flexContainer */].justify, component["c" /* block */].content, {
            paddingTop: 10,
            transition: variable["transitionOpacity"],
        }),
        pagination: [
            layout["a" /* flexContainer */].center,
            component["f" /* slidePagination */],
            {
                transition: variable["transitionNormal"],
                bottom: 0
            },
        ],
        navigation: [
            layout["a" /* flexContainer */].center,
            layout["a" /* flexContainer */].verticalCenter,
            component["e" /* slideNavigation */]
        ],
    },
    customStyleLoading: {
        height: 300
    },
    mobileWrap: {
        width: '100%',
        overflowX: 'auto',
        panel: {
            whiteSpace: 'nowrap',
            flexDirection: 'column'
        }
    },
    placeholder: {
        width: '100%',
        display: variable["display"].flex,
        marginBottom: 35,
        item: {
            flex: 1,
            paddingLeft: 10,
            paddingRight: 10,
        },
        image: {
            width: '100%',
            height: 'auto',
            paddingTop: '65%',
            marginBottom: 10,
        },
        text: {
            width: '94%',
            height: 25,
            marginBottom: 10,
        },
        lastText: {
            width: '65%',
            height: 25,
        }
    },
    desktop: {
        mainWrap: {
            display: variable["display"].block,
            height: '100%'
        },
        container: {
            width: '100%',
            height: '100%',
            overflowX: 'auto',
            whiteSpace: 'nowrap',
            paddingTop: 0,
            display: variable["display"].flex,
            overflow: 'hidden',
            itemSlider: {
                width: "100%",
                display: variable["display"].inlineBlock,
                borderRadius: 5,
                overflow: 'hidden',
                boxShadow: variable["shadowBlur"],
                position: variable["position"].relative,
            },
            itemSliderPanel: function (imgUrl) { return ({
                width: '100%',
                paddingTop: '62.5%',
                position: variable["position"].relative,
                backgroundImage: "url(" + imgUrl + ")",
                backgroundColor: variable["colorF7"],
                backgroundPosition: 'top center',
                backgroundSize: 'cover',
            }); },
            videoIcon: {
                width: 70,
                height: 70,
                position: variable["position"].absolute,
                top: '50%',
                left: '55%',
                transform: 'translate(-50%, -50%)',
                borderTop: '35px solid transparent',
                boxSizing: 'border-box',
                borderLeft: '51px solid white',
                borderBottom: '35px solid transparent',
                opacity: .8
            },
            info: {
                width: '100%',
                height: 110,
                padding: 12,
                position: variable["position"].relative,
                background: variable["colorWhite"],
                display: variable["display"].flex,
                flexDirection: 'column',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                image: function (imgUrl) { return ({
                    width: '100%',
                    height: '100%',
                    top: 0,
                    left: 0,
                    zIndex: -1,
                    position: variable["position"].absolute,
                    backgroundColor: variable["colorF7"],
                    backgroundImage: "url(" + imgUrl + ")",
                    backgroundPosition: 'bottom center',
                    backgroundSize: 'cover',
                    filter: 'blur(4px)',
                    transform: "scaleY(-1) scale(1.1)",
                    'WebkitBackfaceVisibility': 'hidden',
                    'WebkitPerspective': 1000,
                    'WebkitTransform': ['translate3d(0,0,0)', 'translateZ(0)'],
                    'backfaceVisibility': 'hidden',
                    perspective: 1000,
                }); },
                title: {
                    color: variable["colorBlack"],
                    whiteSpace: 'pre-wrap',
                    fontFamily: variable["fontAvenirMedium"],
                    fontSize: 14,
                    lineHeight: '22px',
                    maxHeight: '44px',
                    overflow: 'hidden',
                    marginBottom: 5
                },
                description: {
                    fontSize: 12,
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap',
                    lineHeight: '18px',
                    maxHeight: 36,
                    overflow: 'hidden',
                },
                tagList: {
                    width: '100%',
                    display: variable["display"].flex,
                    flexWrap: 'wrap',
                    height: '36px',
                    maxHeight: '36px',
                    overflow: 'hidden'
                },
                tagItem: {
                    fontSize: 12,
                    lineHeight: '18px',
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap'
                },
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFNBQVMsTUFBTSwwQkFBMEIsQ0FBQztBQUN0RCxPQUFPLEtBQUssTUFBTSxNQUFNLHVCQUF1QixDQUFDO0FBQ2hELE9BQU8sS0FBSyxRQUFRLE1BQU0seUJBQXlCLENBQUM7QUFDcEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRXpELE1BQU0sQ0FBQyxJQUFNLFlBQVksR0FBRztJQUMxQixvQ0FBb0MsRUFBRTtRQUNwQyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxrQkFBa0I7S0FDOUI7SUFFRCwwQ0FBMEMsRUFBRTtRQUMxQyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxlQUFlO0tBQzNCO0lBRUQsa0NBQWtDLEVBQUU7UUFDbEMsS0FBSyxFQUFFLEVBQUU7UUFDVCxNQUFNLEVBQUUsRUFBRTtRQUNWLE9BQU8sRUFBRSxDQUFDO1FBQ1YsU0FBUyxFQUFFLHdCQUF3QjtRQUNuQyxVQUFVLEVBQUUsUUFBUTtLQUNyQjtJQUVELHdDQUF3QyxFQUFFO1FBQ3hDLE9BQU8sRUFBRSxDQUFDO1FBQ1YsU0FBUyxFQUFFLHNCQUFzQjtRQUNqQyxVQUFVLEVBQUUsU0FBUztLQUN0QjtJQUVELG1DQUFtQyxFQUFFO1FBQ25DLEtBQUssRUFBRSxFQUFFO1FBQ1QsTUFBTSxFQUFFLEVBQUU7UUFDVixPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSx1QkFBdUI7UUFDbEMsVUFBVSxFQUFFLFFBQVE7S0FDckI7SUFFRCx5Q0FBeUMsRUFBRTtRQUN6QyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxzQkFBc0I7UUFDakMsVUFBVSxFQUFFLFNBQVM7S0FDdEI7Q0FDRixDQUFDO0FBRUYsZUFBZTtJQUNiLFNBQVMsRUFBRSxZQUFZLENBQUM7UUFDdEIsTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDO1FBQ1osT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDO1FBQ2IsT0FBTyxFQUFFLENBQUM7Z0JBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztnQkFDL0IsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsTUFBTSxFQUFFLE1BQU07YUFDZixDQUFDO0tBQ0gsQ0FBQztJQUVGLFVBQVUsRUFBRTtRQUNWLFFBQVEsRUFBRSxVQUFVO1FBQ3BCLFFBQVEsRUFBRSxRQUFRO1FBQ2xCLE1BQU0sRUFBRSxNQUFNO1FBRWQsU0FBUyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUN6QixNQUFNLENBQUMsYUFBYSxDQUFDLE9BQU8sRUFDNUIsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQ3ZCO1lBQ0UsVUFBVSxFQUFFLEVBQUU7WUFDZCxVQUFVLEVBQUUsUUFBUSxDQUFDLGlCQUFpQjtTQUN2QyxDQUNGO1FBRUQsVUFBVSxFQUFFO1lBQ1YsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNO1lBQzNCLFNBQVMsQ0FBQyxlQUFlO1lBQ3pCO2dCQUNFLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO2dCQUNyQyxNQUFNLEVBQUUsQ0FBQzthQUNWO1NBQ0Y7UUFFRCxVQUFVLEVBQUU7WUFDVixNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU07WUFDM0IsTUFBTSxDQUFDLGFBQWEsQ0FBQyxjQUFjO1lBQ25DLFNBQVMsQ0FBQyxlQUFlO1NBQzFCO0tBQ0Y7SUFFRCxrQkFBa0IsRUFBRTtRQUNsQixNQUFNLEVBQUUsR0FBRztLQUNaO0lBRUQsVUFBVSxFQUFFO1FBQ1YsS0FBSyxFQUFFLE1BQU07UUFDYixTQUFTLEVBQUUsTUFBTTtRQUVqQixLQUFLLEVBQUU7WUFDTCxVQUFVLEVBQUUsUUFBUTtZQUNwQixhQUFhLEVBQUUsUUFBUTtTQUN4QjtLQUNGO0lBRUQsV0FBVyxFQUFFO1FBQ1gsS0FBSyxFQUFFLE1BQU07UUFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1FBQzlCLFlBQVksRUFBRSxFQUFFO1FBRWhCLElBQUksRUFBRTtZQUNKLElBQUksRUFBRSxDQUFDO1lBQ1AsV0FBVyxFQUFFLEVBQUU7WUFDZixZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELEtBQUssRUFBRTtZQUNMLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLE1BQU07WUFDZCxVQUFVLEVBQUUsS0FBSztZQUNqQixZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELElBQUksRUFBRTtZQUNKLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLEVBQUU7WUFDVixZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELFFBQVEsRUFBRTtZQUNSLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLEVBQUU7U0FDWDtLQUNGO0lBRUQsT0FBTyxFQUFFO1FBQ1AsUUFBUSxFQUFFO1lBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztZQUMvQixNQUFNLEVBQUUsTUFBTTtTQUNmO1FBRUQsU0FBUyxFQUFFO1lBQ1QsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsTUFBTTtZQUNkLFNBQVMsRUFBRSxNQUFNO1lBQ2pCLFVBQVUsRUFBRSxRQUFRO1lBQ3BCLFVBQVUsRUFBRSxDQUFDO1lBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixRQUFRLEVBQUUsUUFBUTtZQUVsQixVQUFVLEVBQUU7Z0JBQ1YsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztnQkFDckMsWUFBWSxFQUFFLENBQUM7Z0JBQ2YsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDOUIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTthQUNyQztZQUVELGVBQWUsRUFBRSxVQUFDLE1BQU0sSUFBSyxPQUFBLENBQUM7Z0JBQzVCLEtBQUssRUFBRSxNQUFNO2dCQUNiLFVBQVUsRUFBRSxPQUFPO2dCQUNuQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxlQUFlLEVBQUUsU0FBTyxNQUFNLE1BQUc7Z0JBQ2pDLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztnQkFDakMsa0JBQWtCLEVBQUUsWUFBWTtnQkFDaEMsY0FBYyxFQUFFLE9BQU87YUFDeEIsQ0FBQyxFQVIyQixDQVEzQjtZQUVGLFNBQVMsRUFBRTtnQkFDVCxLQUFLLEVBQUUsRUFBRTtnQkFDVCxNQUFNLEVBQUUsRUFBRTtnQkFDVixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxHQUFHLEVBQUUsS0FBSztnQkFDVixJQUFJLEVBQUUsS0FBSztnQkFDWCxTQUFTLEVBQUUsdUJBQXVCO2dCQUNsQyxTQUFTLEVBQUUsd0JBQXdCO2dCQUNuQyxTQUFTLEVBQUUsWUFBWTtnQkFDdkIsVUFBVSxFQUFFLGtCQUFrQjtnQkFDOUIsWUFBWSxFQUFFLHdCQUF3QjtnQkFDdEMsT0FBTyxFQUFFLEVBQUU7YUFDWjtZQUVELElBQUksRUFBRTtnQkFDSixLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsR0FBRztnQkFDWCxPQUFPLEVBQUUsRUFBRTtnQkFDWCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVU7Z0JBQy9CLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLGFBQWEsRUFBRSxRQUFRO2dCQUN2QixjQUFjLEVBQUUsWUFBWTtnQkFDNUIsVUFBVSxFQUFFLFlBQVk7Z0JBRXhCLEtBQUssRUFBRSxVQUFDLE1BQU0sSUFBSyxPQUFBLENBQUM7b0JBQ2xCLEtBQUssRUFBRSxNQUFNO29CQUNiLE1BQU0sRUFBRSxNQUFNO29CQUNkLEdBQUcsRUFBRSxDQUFDO29CQUNOLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxDQUFDLENBQUM7b0JBQ1YsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtvQkFDcEMsZUFBZSxFQUFFLFFBQVEsQ0FBQyxPQUFPO29CQUNqQyxlQUFlLEVBQUUsU0FBTyxNQUFNLE1BQUc7b0JBQ2pDLGtCQUFrQixFQUFFLGVBQWU7b0JBQ25DLGNBQWMsRUFBRSxPQUFPO29CQUN2QixNQUFNLEVBQUUsV0FBVztvQkFDbkIsU0FBUyxFQUFFLHVCQUF1QjtvQkFDbEMsMEJBQTBCLEVBQUUsUUFBUTtvQkFDcEMsbUJBQW1CLEVBQUUsSUFBSTtvQkFDekIsaUJBQWlCLEVBQUUsQ0FBQyxvQkFBb0IsRUFBRSxlQUFlLENBQUM7b0JBQzFELG9CQUFvQixFQUFFLFFBQVE7b0JBQzlCLFdBQVcsRUFBRSxJQUFJO2lCQUNsQixDQUFDLEVBbEJpQixDQWtCakI7Z0JBRUYsS0FBSyxFQUFFO29CQUNMLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDMUIsVUFBVSxFQUFFLFVBQVU7b0JBQ3RCLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO29CQUNyQyxRQUFRLEVBQUUsRUFBRTtvQkFDWixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsU0FBUyxFQUFFLE1BQU07b0JBQ2pCLFFBQVEsRUFBRSxRQUFRO29CQUNsQixZQUFZLEVBQUUsQ0FBQztpQkFDaEI7Z0JBRUQsV0FBVyxFQUFFO29CQUNYLFFBQVEsRUFBRSxFQUFFO29CQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7b0JBQ3JDLE9BQU8sRUFBRSxFQUFFO29CQUNYLFdBQVcsRUFBRSxFQUFFO29CQUNmLFVBQVUsRUFBRSxVQUFVO29CQUN0QixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsU0FBUyxFQUFFLEVBQUU7b0JBQ2IsUUFBUSxFQUFFLFFBQVE7aUJBQ25CO2dCQUVELE9BQU8sRUFBRTtvQkFDUCxLQUFLLEVBQUUsTUFBTTtvQkFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO29CQUM5QixRQUFRLEVBQUUsTUFBTTtvQkFDaEIsTUFBTSxFQUFFLE1BQU07b0JBQ2QsU0FBUyxFQUFFLE1BQU07b0JBQ2pCLFFBQVEsRUFBRSxRQUFRO2lCQUNuQjtnQkFFRCxPQUFPLEVBQUU7b0JBQ1AsUUFBUSxFQUFFLEVBQUU7b0JBQ1osVUFBVSxFQUFFLE1BQU07b0JBQ2xCLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7b0JBQ3JDLE9BQU8sRUFBRSxFQUFFO29CQUNYLFdBQVcsRUFBRSxFQUFFO29CQUNmLFVBQVUsRUFBRSxVQUFVO2lCQUN2QjthQUNGO1NBQ0Y7S0FDRjtDQUNxQixDQUFDIn0=
// CONCATENATED MODULE: ./components/container/image-slider-community/view-desktop.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};






var renderSlider = function (_a) {
    var column = _a.column, handleOmitImgHeight = _a.handleOmitImgHeight, imageSlideSelected = _a.imageSlideSelected;
    return (react["createElement"]("community-image-slider", { style: image_slider_community_style.desktop.mainWrap },
        react["createElement"]("div", { style: image_slider_community_style.desktop.container }, imageSlideSelected
            && Array.isArray(imageSlideSelected.list)
            && imageSlideSelected.list.map(function (item, index) {
                var slideProps = {
                    item: item,
                    column: column,
                    handleOmitImgHeight: handleOmitImgHeight
                };
                return react["createElement"](image_slider_item_community, __assign({ key: "slider-item-" + index }, slideProps));
            }))));
};
var renderNavigation = function (_a) {
    var imageSlide = _a.imageSlide, navLeftSlide = _a.navLeftSlide, navRightSlide = _a.navRightSlide;
    var leftNavProps = {
        className: 'left-nav',
        onClick: navLeftSlide,
        style: [image_slider_community_style.imageSlide.navigation, component["e" /* slideNavigation */]['left'], { top: '50%', transform: 'translateY(-50%)' }]
    };
    var rightNavProps = {
        className: 'right-nav',
        onClick: navRightSlide,
        style: [image_slider_community_style.imageSlide.navigation, component["e" /* slideNavigation */]['right'], { top: '50%', transform: 'translateY(-50%)' }]
    };
    return imageSlide.length <= 1
        ? null
        : (react["createElement"]("div", null,
            react["createElement"]("div", __assign({}, leftNavProps),
                react["createElement"](icon["a" /* default */], { name: 'angle-left', style: component["e" /* slideNavigation */].icon })),
            react["createElement"]("div", __assign({}, rightNavProps),
                react["createElement"](icon["a" /* default */], { name: 'angle-right', style: component["e" /* slideNavigation */].icon }))));
};
var renderPagination = function (_a) {
    var imageSlide = _a.imageSlide, selectSlide = _a.selectSlide, countChangeSlide = _a.countChangeSlide;
    var generateItemProps = function (item, index) { return ({
        key: "product-slider-" + item.id,
        onClick: function () { return selectSlide(index); },
        style: [
            component["f" /* slidePagination */].item,
            index === countChangeSlide && component["f" /* slidePagination */].itemActive
        ]
    }); };
    return imageSlide.length <= 1
        ? null
        : (react["createElement"]("div", { style: image_slider_community_style.imageSlide.pagination, className: 'pagination' }, Array.isArray(imageSlide)
            && imageSlide.map(function (item, $index) {
                var itemProps = generateItemProps(item, $index);
                return react["createElement"]("div", __assign({}, itemProps));
            })));
};
var renderDesktop = function (_a) {
    var props = _a.props, state = _a.state, handleMouseHover = _a.handleMouseHover, selectSlide = _a.selectSlide, navLeftSlide = _a.navLeftSlide, navRightSlide = _a.navRightSlide;
    var _b = state, imageSlide = _b.imageSlide, imageSlideSelected = _b.imageSlideSelected, countChangeSlide = _b.countChangeSlide;
    var _c = props, column = _c.column, handleOmitImgHeight = _c.handleOmitImgHeight;
    var containerProps = {
        style: image_slider_community_style.imageSlide,
        onMouseEnter: handleMouseHover
    };
    return (react["createElement"]("div", __assign({}, containerProps, { className: 'image-slide-container' }),
        renderSlider({ column: column, handleOmitImgHeight: handleOmitImgHeight, imageSlideSelected: imageSlideSelected }),
        renderNavigation({ imageSlide: imageSlide, navLeftSlide: navLeftSlide, navRightSlide: navRightSlide }),
        react["createElement"](radium["Style"], { rules: INLINE_STYLE })));
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1kZXNrdG9wLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1kZXNrdG9wLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFL0IsT0FBTyxJQUFJLE1BQU0sNkJBQTZCLENBQUM7QUFDL0MsT0FBTyxLQUFLLFNBQVMsTUFBTSwwQkFBMEIsQ0FBQztBQUN0RCxPQUFPLGVBQWUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUU3RCxPQUFPLEtBQUssRUFBRSxFQUFFLFlBQVksRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUc5QyxJQUFNLFlBQVksR0FBRyxVQUFDLEVBQW1EO1FBQWpELGtCQUFNLEVBQUUsNENBQW1CLEVBQUUsMENBQWtCO0lBRXJFLE1BQU0sQ0FBQyxDQUNMLGdEQUF3QixLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRO1FBQ25ELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsSUFFL0Isa0JBQWtCO2VBQ2YsS0FBSyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUM7ZUFDdEMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLO2dCQUN6QyxJQUFNLFVBQVUsR0FBRztvQkFDakIsSUFBSSxNQUFBO29CQUNKLE1BQU0sUUFBQTtvQkFDTixtQkFBbUIscUJBQUE7aUJBQ3BCLENBQUE7Z0JBRUQsTUFBTSxDQUFDLG9CQUFDLGVBQWUsYUFBQyxHQUFHLEVBQUUsaUJBQWUsS0FBTyxJQUFNLFVBQVUsRUFBSSxDQUFBO1lBQ3pFLENBQUMsQ0FBQyxDQUVBLENBQ2lCLENBQzFCLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixJQUFNLGdCQUFnQixHQUFHLFVBQUMsRUFBMkM7UUFBekMsMEJBQVUsRUFBRSw4QkFBWSxFQUFFLGdDQUFhO0lBQ2pFLElBQU0sWUFBWSxHQUFHO1FBQ25CLFNBQVMsRUFBRSxVQUFVO1FBQ3JCLE9BQU8sRUFBRSxZQUFZO1FBQ3JCLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLFNBQVMsQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxrQkFBa0IsRUFBRSxDQUFDO0tBQ3ZILENBQUM7SUFFRixJQUFNLGFBQWEsR0FBRztRQUNwQixTQUFTLEVBQUUsV0FBVztRQUN0QixPQUFPLEVBQUUsYUFBYTtRQUN0QixLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxFQUFFLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsa0JBQWtCLEVBQUUsQ0FBQztLQUN4SCxDQUFDO0lBRUYsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLElBQUksQ0FBQztRQUMzQixDQUFDLENBQUMsSUFBSTtRQUNOLENBQUMsQ0FBQyxDQUNBO1lBQ0Usd0NBQVMsWUFBWTtnQkFDbkIsb0JBQUMsSUFBSSxJQUFDLElBQUksRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxlQUFlLENBQUMsSUFBSSxHQUFJLENBQy9EO1lBQ04sd0NBQVMsYUFBYTtnQkFDcEIsb0JBQUMsSUFBSSxJQUFDLElBQUksRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxlQUFlLENBQUMsSUFBSSxHQUFJLENBQ2hFLENBQ0YsQ0FDUCxDQUFDO0FBQ04sQ0FBQyxDQUFDO0FBRUYsSUFBTSxnQkFBZ0IsR0FBRyxVQUFDLEVBQTZDO1FBQTNDLDBCQUFVLEVBQUUsNEJBQVcsRUFBRSxzQ0FBZ0I7SUFDbkUsSUFBTSxpQkFBaUIsR0FBRyxVQUFDLElBQUksRUFBRSxLQUFLLElBQUssT0FBQSxDQUFDO1FBQzFDLEdBQUcsRUFBRSxvQkFBa0IsSUFBSSxDQUFDLEVBQUk7UUFDaEMsT0FBTyxFQUFFLGNBQU0sT0FBQSxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQWxCLENBQWtCO1FBQ2pDLEtBQUssRUFBRTtZQUNMLFNBQVMsQ0FBQyxlQUFlLENBQUMsSUFBSTtZQUM5QixLQUFLLEtBQUssZ0JBQWdCLElBQUksU0FBUyxDQUFDLGVBQWUsQ0FBQyxVQUFVO1NBQ25FO0tBQ0YsQ0FBQyxFQVB5QyxDQU96QyxDQUFDO0lBRUgsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLElBQUksQ0FBQztRQUMzQixDQUFDLENBQUMsSUFBSTtRQUNOLENBQUMsQ0FBQyxDQUNBLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRSxTQUFTLEVBQUUsWUFBWSxJQUU1RCxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQztlQUN0QixVQUFVLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBSSxFQUFFLE1BQU07Z0JBQzdCLElBQU0sU0FBUyxHQUFHLGlCQUFpQixDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztnQkFDbEQsTUFBTSxDQUFDLHdDQUFTLFNBQVMsRUFBUSxDQUFDO1lBQ3BDLENBQUMsQ0FBQyxDQUVBLENBQ1AsQ0FBQztBQUNOLENBQUMsQ0FBQztBQUVGLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRyxVQUFDLEVBQTRFO1FBQTFFLGdCQUFLLEVBQUUsZ0JBQUssRUFBRSxzQ0FBZ0IsRUFBRSw0QkFBVyxFQUFFLDhCQUFZLEVBQUUsZ0NBQWE7SUFDaEcsSUFBQSxVQUFzRSxFQUFwRSwwQkFBVSxFQUFFLDBDQUFrQixFQUFFLHNDQUFnQixDQUFxQjtJQUN2RSxJQUFBLFVBQWlELEVBQS9DLGtCQUFNLEVBQUUsNENBQW1CLENBQXFCO0lBRXhELElBQU0sY0FBYyxHQUFHO1FBQ3JCLEtBQUssRUFBRSxLQUFLLENBQUMsVUFBVTtRQUN2QixZQUFZLEVBQUUsZ0JBQWdCO0tBQy9CLENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCx3Q0FBUyxjQUFjLElBQUUsU0FBUyxFQUFFLHVCQUF1QjtRQUN4RCxZQUFZLENBQUMsRUFBRSxNQUFNLFFBQUEsRUFBRSxtQkFBbUIscUJBQUEsRUFBRSxrQkFBa0Isb0JBQUEsRUFBRSxDQUFDO1FBRWpFLGdCQUFnQixDQUFDLEVBQUUsVUFBVSxZQUFBLEVBQUUsWUFBWSxjQUFBLEVBQUUsYUFBYSxlQUFBLEVBQUUsQ0FBQztRQUM5RCxvQkFBQyxLQUFLLElBQUMsS0FBSyxFQUFFLFlBQVksR0FBSSxDQUMxQixDQUNQLENBQUM7QUFDSixDQUFDLENBQUMifQ==
// CONCATENATED MODULE: ./components/container/image-slider-community/view-mobile.tsx




var renderMobile = function (_a) {
    var imageList = _a.imageList, column = _a.column, handleOmitImgHeight = _a.handleOmitImgHeight;
    return (react["createElement"]("div", { style: image_slider_community_style.mobileWrap },
        react["createElement"]("div", { style: [layout["a" /* flexContainer */].noWrap, image_slider_community_style.mobileWrap.panel] }, Array.isArray(imageList)
            && imageList.map(function (item, index) { return react["createElement"](image_slider_item_community, { key: "image-slider-item-" + index, item: item, column: column, handleOmitImgHeight: handleOmitImgHeight }); }))));
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1tb2JpbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LW1vYmlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxlQUFlLE1BQU0sZ0NBQWdDLENBQUM7QUFFN0QsT0FBTyxLQUFLLE1BQU0sTUFBTSx1QkFBdUIsQ0FBQztBQUNoRCxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsTUFBTSxDQUFDLElBQU0sWUFBWSxHQUFHLFVBQUMsRUFBMEM7UUFBeEMsd0JBQVMsRUFBRSxrQkFBTSxFQUFFLDRDQUFtQjtJQUVuRSxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFVBQVU7UUFDMUIsNkJBQUssS0FBSyxFQUFFLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFFN0QsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUM7ZUFDckIsU0FBUyxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLLElBQUssT0FBQSxvQkFBQyxlQUFlLElBQUMsR0FBRyxFQUFFLHVCQUFxQixLQUFPLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLG1CQUFtQixFQUFFLG1CQUFtQixHQUFJLEVBQTVILENBQTRILENBQUMsQ0FFN0osQ0FDRixDQUNQLENBQUM7QUFDSixDQUFDLENBQUMifQ==
// CONCATENATED MODULE: ./components/container/image-slider-community/view.tsx




var renderView = function (_a) {
    var props = _a.props, state = _a.state, handleMouseHover = _a.handleMouseHover, selectSlide = _a.selectSlide, navLeftSlide = _a.navLeftSlide, navRightSlide = _a.navRightSlide;
    var imageList = state.imageList;
    var _b = props, column = _b.column, handleOmitImgHeight = _b.handleOmitImgHeight;
    var renderMobileProps = { imageList: imageList, column: column, handleOmitImgHeight: handleOmitImgHeight };
    var renderDesktopProps = {
        props: props,
        state: state,
        selectSlide: selectSlide,
        navLeftSlide: navLeftSlide,
        navRightSlide: navRightSlide,
        handleMouseHover: handleMouseHover
    };
    var switchStyle = {
        MOBILE: function () { return renderMobile(renderMobileProps); },
        DESKTOP: function () { return renderDesktop(renderDesktopProps); }
    };
    return (react["createElement"]("div", { style: image_slider_community_style.container }, switchStyle[window.DEVICE_VERSION]()));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRS9CLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMvQyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTdDLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixJQUFNLFVBQVUsR0FBRyxVQUFDLEVBQTRFO1FBQTFFLGdCQUFLLEVBQUUsZ0JBQUssRUFBRSxzQ0FBZ0IsRUFBRSw0QkFBVyxFQUFFLDhCQUFZLEVBQUUsZ0NBQWE7SUFDcEYsSUFBQSwyQkFBUyxDQUFxQjtJQUNoQyxJQUFBLFVBQWlELEVBQS9DLGtCQUFNLEVBQUUsNENBQW1CLENBQXFCO0lBRXhELElBQU0saUJBQWlCLEdBQUcsRUFBRSxTQUFTLFdBQUEsRUFBRSxNQUFNLFFBQUEsRUFBRSxtQkFBbUIscUJBQUEsRUFBRSxDQUFDO0lBQ3JFLElBQU0sa0JBQWtCLEdBQUc7UUFDekIsS0FBSyxPQUFBO1FBQ0wsS0FBSyxPQUFBO1FBQ0wsV0FBVyxhQUFBO1FBQ1gsWUFBWSxjQUFBO1FBQ1osYUFBYSxlQUFBO1FBQ2IsZ0JBQWdCLGtCQUFBO0tBQ2pCLENBQUM7SUFFRixJQUFNLFdBQVcsR0FBRztRQUNsQixNQUFNLEVBQUUsY0FBTSxPQUFBLFlBQVksQ0FBQyxpQkFBaUIsQ0FBQyxFQUEvQixDQUErQjtRQUM3QyxPQUFPLEVBQUUsY0FBTSxPQUFBLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFqQyxDQUFpQztLQUNqRCxDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLElBQ3hCLFdBQVcsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FDakMsQ0FDUCxDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// CONCATENATED MODULE: ./components/container/image-slider-community/component.tsx
var component_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var component_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var component_ImageSlide = /** @class */ (function (_super) {
    component_extends(ImageSlide, _super);
    function ImageSlide(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE(props.data);
        return _this;
    }
    ImageSlide.prototype.componentDidMount = function () {
        this.initDataSlide(this.state.imageList, this.props.posImg);
        document.addEventListener('keydown', this.handleKeydown.bind(this), false);
    };
    ImageSlide.prototype.componentWillUnmount = function () {
        document.removeEventListener('keydown', this.handleKeydown.bind(this), false);
    };
    /**
     * When component will receive new props -> init data for slide
     * @param nextProps prop from parent
     */
    ImageSlide.prototype.componentWillReceiveProps = function (nextProps) {
        this.initDataSlide(nextProps.data, nextProps.posImg);
    };
    /**
     * Init data for slide
     * When : Component will mount OR Component will receive new props
     */
    ImageSlide.prototype.initDataSlide = function (_newImageList, posImg) {
        var _this = this;
        if (_newImageList === void 0) { _newImageList = this.state.imageList; }
        if (posImg === void 0) { posImg = 0; }
        if (true === Object(responsive["b" /* isDesktopVersion */])()) {
            if (false === this.state.firstInit) {
                this.setState({ firstInit: true });
            }
            /**
             * On DESKTOP
             * Init data for image slide & image slide selected
             */
            var _imageSlide_1 = [];
            var groupImage_1 = {
                id: 0,
                list: []
            };
            /** Assign data into each slider */
            if (_newImageList.length > (this.props.column || 3)) {
                Array.isArray(_newImageList)
                    && _newImageList.map(function (image) {
                        groupImage_1.id = _imageSlide_1.length;
                        groupImage_1.list.push(image);
                        if (groupImage_1.list.length === _this.props.column) {
                            _imageSlide_1.push(Object.assign({}, groupImage_1));
                            groupImage_1.list = [];
                        }
                    });
            }
            else {
                _imageSlide_1 = [{ id: 0, list: _newImageList }];
            }
            this.setState({
                imageList: _newImageList,
                imageSlide: _imageSlide_1,
                imageSlideSelected: _imageSlide_1[0] || {}
            });
            posImg !== 0 && this.selectSlide(posImg);
        }
        else {
            /**
             * On Mobile
             * Only init data for list image, not apply slide animation
             */
            this.setState({ imageList: _newImageList });
        }
    };
    /**
     * Navigate slide by button left or right
     * @param _direction `LEFT` or `RIGHT`
     * Will set new index value by @param _direction
     */
    ImageSlide.prototype.navSlide = function (_direction) {
        var _a = this.state, imageSlide = _a.imageSlide, countChangeSlide = _a.countChangeSlide;
        /**
         * If navigate to right: increase index value -> set +1 by countChangeSlide
         * If vavigate to left: decrease index value -> set -1 by countChangeSlide
         */
        var newIndexValue = 'left' === _direction ? -1 : 1;
        newIndexValue += countChangeSlide;
        /**
         * Validate new value in range [0, imageSlide.length - 1]
         */
        newIndexValue = newIndexValue === imageSlide.length
            ? 0 /** If over max value -> set 0 */
            : (newIndexValue === -1
                /** If under min value -> set imageSlide.length - 1 */
                ? imageSlide.length - 1
                : newIndexValue);
        /** Change to new index value */
        this.selectSlide(newIndexValue);
    };
    ImageSlide.prototype.navLeftSlide = function () {
        this.navSlide('left');
    };
    ImageSlide.prototype.navRightSlide = function () {
        this.navSlide('right');
    };
    ImageSlide.prototype.handleKeydown = function (event) {
        if (event.keyCode === 37) {
            this.navLeftSlide();
        }
        else if (event.keyCode === 39) {
            this.navRightSlide();
        }
    };
    /**
     * Change slide by set state and setTimeout for animation
     * @param _index new index value
     */
    ImageSlide.prototype.selectSlide = function (_index) {
        var _this = this;
        /** Change background */
        setTimeout(function () { return _this.setState(function (prevState, props) { return ({
            countChangeSlide: _index,
            imageSlideSelected: prevState.imageSlide[_index],
        }); }); }, 10);
    };
    ImageSlide.prototype.handleMouseHover = function () {
        var _a = this.props, _b = _a.data, data = _b === void 0 ? [] : _b, _c = _a.column, column = _c === void 0 ? 1 : _c;
        var preLoadImageList = Array.isArray(data)
            ? data
                .filter(function (item, $index) { return $index >= column; })
                .map(function (item) { return item.original_url; })
            : [];
        Object(utils_image["b" /* preLoadImage */])(preLoadImageList);
    };
    // shouldComponentUpdate(nextProps: IProps, nextState: IState) {
    //   // if (this.props.data.length !== nextProps.data.length) { return true; }
    //   // if (this.state.countChangeSlide !== nextState.countChangeSlide) { return true; }
    //   // if (this.props.data.length > 0 && this.state.firstInit !== nextState.firstInit) { return true; }
    //   return true;
    // }
    ImageSlide.prototype.render = function () {
        var _this = this;
        var renderViewProps = {
            props: this.props,
            state: this.state,
            handleMouseHover: this.handleMouseHover.bind(this),
            selectSlide: function (index) { return _this.selectSlide(index); },
            navLeftSlide: this.navLeftSlide.bind(this),
            navRightSlide: this.navRightSlide.bind(this)
        };
        return view(renderViewProps);
    };
    ImageSlide.defaultProps = DEFAULT_PROPS;
    ImageSlide = component_decorate([
        radium
    ], ImageSlide);
    return ImageSlide;
}(react["Component"]));
;
/* harmony default export */ var image_slider_community_component = (component_ImageSlide);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRzdELE9BQU8sRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQzVELE9BQU8sVUFBVSxNQUFNLFFBQVEsQ0FBQztBQUdoQztJQUF5Qiw4QkFBK0I7SUFHdEQsb0JBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDOztJQUN6QyxDQUFDO0lBRUQsc0NBQWlCLEdBQWpCO1FBQ0UsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzVELFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDN0UsQ0FBQztJQUVELHlDQUFvQixHQUFwQjtRQUNFLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDaEYsQ0FBQztJQUVEOzs7T0FHRztJQUNILDhDQUF5QixHQUF6QixVQUEwQixTQUFTO1FBQ2pDLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVEOzs7T0FHRztJQUNILGtDQUFhLEdBQWIsVUFBYyxhQUFnRCxFQUFFLE1BQVU7UUFBMUUsaUJBNENDO1FBNUNhLDhCQUFBLEVBQUEsZ0JBQTRCLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUztRQUFFLHVCQUFBLEVBQUEsVUFBVTtRQUN4RSxFQUFFLENBQUMsQ0FBQyxJQUFJLEtBQUssZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFFaEMsRUFBRSxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBWSxDQUFDLENBQUM7WUFBQyxDQUFDO1lBQ3JGOzs7ZUFHRztZQUNILElBQUksYUFBVyxHQUFlLEVBQUUsQ0FBQztZQUNqQyxJQUFJLFlBQVUsR0FBc0M7Z0JBQ2xELEVBQUUsRUFBRSxDQUFDO2dCQUNMLElBQUksRUFBRSxFQUFFO2FBQ1QsQ0FBQztZQUVGLG1DQUFtQztZQUNuQyxFQUFFLENBQUMsQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNwRCxLQUFLLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQzt1QkFDdkIsYUFBYSxDQUFDLEdBQUcsQ0FBQyxVQUFDLEtBQUs7d0JBQ3pCLFlBQVUsQ0FBQyxFQUFFLEdBQUcsYUFBVyxDQUFDLE1BQU0sQ0FBQzt3QkFDbkMsWUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBRTVCLEVBQUUsQ0FBQyxDQUFDLFlBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxLQUFLLEtBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzs0QkFDakQsYUFBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxZQUFVLENBQUMsQ0FBQyxDQUFDOzRCQUNoRCxZQUFVLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQzt3QkFDdkIsQ0FBQztvQkFDSCxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixhQUFXLEdBQUcsQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7WUFDakQsQ0FBQztZQUVELElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ1osU0FBUyxFQUFFLGFBQWE7Z0JBQ3hCLFVBQVUsRUFBRSxhQUFXO2dCQUN2QixrQkFBa0IsRUFBRSxhQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRTthQUMvQixDQUFDLENBQUM7WUFFYixNQUFNLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDM0MsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ047OztlQUdHO1lBQ0gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQVksQ0FBQyxDQUFDO1FBQ3hELENBQUM7SUFDSCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILDZCQUFRLEdBQVIsVUFBUyxVQUFVO1FBQ1gsSUFBQSxlQUE2QyxFQUEzQywwQkFBVSxFQUFFLHNDQUFnQixDQUFnQjtRQUVwRDs7O1dBR0c7UUFDSCxJQUFJLGFBQWEsR0FBRyxNQUFNLEtBQUssVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25ELGFBQWEsSUFBSSxnQkFBZ0IsQ0FBQztRQUVsQzs7V0FFRztRQUNILGFBQWEsR0FBRyxhQUFhLEtBQUssVUFBVSxDQUFDLE1BQU07WUFDakQsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQ0FBaUM7WUFDckMsQ0FBQyxDQUFDLENBQ0EsYUFBYSxLQUFLLENBQUMsQ0FBQztnQkFDbEIsc0RBQXNEO2dCQUN0RCxDQUFDLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDO2dCQUN2QixDQUFDLENBQUMsYUFBYSxDQUNsQixDQUFDO1FBRUosZ0NBQWdDO1FBQ2hDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUVELGlDQUFZLEdBQVo7UUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3hCLENBQUM7SUFFRCxrQ0FBYSxHQUFiO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBRUQsa0NBQWEsR0FBYixVQUFjLEtBQUs7UUFDakIsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQTtRQUNyQixDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNoQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUE7UUFDdEIsQ0FBQztJQUNILENBQUM7SUFFRDs7O09BR0c7SUFDSCxnQ0FBVyxHQUFYLFVBQVksTUFBTTtRQUFsQixpQkFPQztRQUxDLHdCQUF3QjtRQUN4QixVQUFVLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxRQUFRLENBQUMsVUFBQyxTQUFTLEVBQUUsS0FBSyxJQUFLLE9BQUEsQ0FBQztZQUNwRCxnQkFBZ0IsRUFBRSxNQUFNO1lBQ3hCLGtCQUFrQixFQUFFLFNBQVMsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDO1NBQ3RDLENBQUEsRUFIeUMsQ0FHekMsQ0FBQyxFQUhJLENBR0osRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNyQixDQUFDO0lBRUQscUNBQWdCLEdBQWhCO1FBQ1EsSUFBQSxlQUFzQyxFQUFwQyxZQUFTLEVBQVQsOEJBQVMsRUFBRSxjQUFVLEVBQVYsK0JBQVUsQ0FBZ0I7UUFFN0MsSUFBTSxnQkFBZ0IsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztZQUMxQyxDQUFDLENBQUMsSUFBSTtpQkFDSCxNQUFNLENBQUMsVUFBQyxJQUFJLEVBQUUsTUFBTSxJQUFLLE9BQUEsTUFBTSxJQUFJLE1BQU0sRUFBaEIsQ0FBZ0IsQ0FBQztpQkFDMUMsR0FBRyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFlBQVksRUFBakIsQ0FBaUIsQ0FBQztZQUNqQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBRVAsWUFBWSxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVELGdFQUFnRTtJQUNoRSw4RUFBOEU7SUFDOUUsd0ZBQXdGO0lBQ3hGLHdHQUF3RztJQUV4RyxpQkFBaUI7SUFDakIsSUFBSTtJQUVKLDJCQUFNLEdBQU47UUFBQSxpQkFXQztRQVZDLElBQU0sZUFBZSxHQUFHO1lBQ3RCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDbEQsV0FBVyxFQUFFLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBdkIsQ0FBdUI7WUFDN0MsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUMxQyxhQUFhLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQzdDLENBQUM7UUFFRixNQUFNLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFyS00sdUJBQVksR0FBVyxhQUFhLENBQUM7SUFEeEMsVUFBVTtRQURmLE1BQU07T0FDRCxVQUFVLENBdUtmO0lBQUQsaUJBQUM7Q0FBQSxBQXZLRCxDQUF5QixLQUFLLENBQUMsU0FBUyxHQXVLdkM7QUFBQSxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// CONCATENATED MODULE: ./components/container/image-slider-community/index.tsx

/* harmony default export */ var image_slider_community = __webpack_exports__["a"] = (image_slider_community_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxVQUFVLE1BQU0sYUFBYSxDQUFDO0FBQ3JDLGVBQWUsVUFBVSxDQUFDIn0=

/***/ }),

/***/ 783:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// EXTERNAL MODULE: ./components/container/feed-item/index.tsx + 5 modules
var feed_item = __webpack_require__(772);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/container/feed-list/style.tsx

/* harmony default export */ var feed_list_style = ({
    width: '100%',
    paddingTop: 0,
    paddingRight: 10,
    paddingLeft: 10,
    item: {
        boxShadow: variable["shadowBlurSort"],
        marginBottom: 10,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 10,
        small: {
            boxShadow: 'none',
            border: "1px solid " + variable["colorB0"],
            marginBottom: 20,
            last: {
                marginBottom: 0,
            }
        },
        info: {
            marginBottom: 10,
            avatar: {
                width: 50,
                minWidth: 50,
                height: 50,
                borderRadius: 25,
                marginRight: 10,
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                backgroundColor: variable["colorE5"],
                small: {
                    width: 40,
                    minWidth: 40,
                    height: 40,
                    borderRadius: 0,
                }
            },
            username: {
                paddingRight: 15,
                marginBottom: 5,
            },
            detail: {
                flex: 10,
                groupUsername: {
                    display: variable["display"].flex,
                    flexDirection: 'column',
                    marginBottom: 5,
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    date: {
                        fontFamily: variable["fontAvenirRegular"],
                        fontSize: 11,
                        color: variable["color75"],
                        marginBottom: 5,
                    }
                }
            },
            description: {
                fontSize: 13,
                maxHeight: 60,
                overflow: 'hidden',
                viewMore: {
                    fontFamily: variable["fontAvenirDemiBold"],
                    fontSize: 15,
                    cursor: 'pointer',
                },
            }
        },
        image: {
            width: '100%',
            maxWidth: '100%',
            height: 'auto'
        },
        text: {
            color: variable["colorBlack06"],
        },
        inner: {
            width: 15,
        },
        likeCount: {
            display: variable["display"].flex,
            alignItems: 'center',
            borderBottom: "1px solid " + variable["colorBlack005"],
            marginBottom: 10,
        },
        likeCommentIconGroup: {
            display: variable["display"].flex,
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingBottom: 10,
            borderBottom: "1px solid " + variable["colorBlack005"],
            like: {
                display: variable["display"].flex,
                alignItems: 'center',
                justifyContent: 'center',
                width: 'calc(50% - 10px)',
                cursor: 'pointer',
            },
            comment: {
                display: variable["display"].flex,
                alignItems: 'center',
                justifyContent: 'center',
                width: '50%',
                cursor: 'pointer',
            },
        },
        commentGroup: {
            display: variable["display"].flex,
            paddingTop: 10,
            avatar: {},
            contenGroup: {
                display: variable["display"].flex,
                flexDirection: 'column',
                width: '100%',
                borderBottom: "1px solid " + variable["colorBlack005"],
                paddingBottom: 10,
                name: {},
                date: {
                    fontFamily: variable["fontAvenirRegular"],
                    fontSize: 11,
                    color: variable["color75"],
                    marginBottom: 5,
                },
                comment: {
                    fontFamily: variable["fontAvenirRegular"],
                    fontSize: 14,
                    lineHeight: '20px',
                    color: variable["color2E"],
                    textAlign: 'justify',
                },
            }
        },
        inputCommentGroup: {
            display: variable["display"].flex,
            alignItems: 'center',
            paddingTop: 10,
            avatar: {
                width: 30,
                minWidth: 30,
                height: 30,
                borderRadius: 25,
                marginRight: 20,
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                backgroundColor: variable["colorE5"],
                marginLeft: 10,
            },
            input: {
                height: 30,
                width: '100%',
                backgroundColor: '#f0f0f0',
                borderRadius: 20,
                lineHeight: '30px',
                textAlign: 'center',
                color: variable["color75"],
            },
            inputText: {
                marginBottom: 0,
                paddingTop: 0,
                paddingBottom: 0,
            },
            sendComment: {
                display: variable["display"].flex,
                alignItems: 'center',
            },
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxlQUFlO0lBQ2IsS0FBSyxFQUFFLE1BQU07SUFDYixVQUFVLEVBQUUsQ0FBQztJQUNiLFlBQVksRUFBRSxFQUFFO0lBQ2hCLFdBQVcsRUFBRSxFQUFFO0lBRWYsSUFBSSxFQUFFO1FBQ0osU0FBUyxFQUFFLFFBQVEsQ0FBQyxjQUFjO1FBQ2xDLFlBQVksRUFBRSxFQUFFO1FBQ2hCLFVBQVUsRUFBRSxFQUFFO1FBQ2QsWUFBWSxFQUFFLEVBQUU7UUFDaEIsYUFBYSxFQUFFLEVBQUU7UUFDakIsV0FBVyxFQUFFLEVBQUU7UUFFZixLQUFLLEVBQUU7WUFDTCxTQUFTLEVBQUUsTUFBTTtZQUNqQixNQUFNLEVBQUUsZUFBYSxRQUFRLENBQUMsT0FBUztZQUN2QyxZQUFZLEVBQUUsRUFBRTtZQUVoQixJQUFJLEVBQUU7Z0JBQ0osWUFBWSxFQUFFLENBQUM7YUFDaEI7U0FDRjtRQUVELElBQUksRUFBRTtZQUNKLFlBQVksRUFBRSxFQUFFO1lBRWhCLE1BQU0sRUFBRTtnQkFDTixLQUFLLEVBQUUsRUFBRTtnQkFDVCxRQUFRLEVBQUUsRUFBRTtnQkFDWixNQUFNLEVBQUUsRUFBRTtnQkFDVixZQUFZLEVBQUUsRUFBRTtnQkFDaEIsV0FBVyxFQUFFLEVBQUU7Z0JBQ2Ysa0JBQWtCLEVBQUUsUUFBUTtnQkFDNUIsY0FBYyxFQUFFLE9BQU87Z0JBQ3ZCLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztnQkFFakMsS0FBSyxFQUFFO29CQUNMLEtBQUssRUFBRSxFQUFFO29CQUNULFFBQVEsRUFBRSxFQUFFO29CQUNaLE1BQU0sRUFBRSxFQUFFO29CQUNWLFlBQVksRUFBRSxDQUFDO2lCQUNoQjthQUNGO1lBRUQsUUFBUSxFQUFFO2dCQUNSLFlBQVksRUFBRSxFQUFFO2dCQUNoQixZQUFZLEVBQUUsQ0FBQzthQUNoQjtZQUVELE1BQU0sRUFBRTtnQkFDTixJQUFJLEVBQUUsRUFBRTtnQkFFUixhQUFhLEVBQUU7b0JBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtvQkFDOUIsYUFBYSxFQUFFLFFBQVE7b0JBQ3ZCLFlBQVksRUFBRSxDQUFDO29CQUNmLFFBQVEsRUFBRSxRQUFRO29CQUNsQixZQUFZLEVBQUUsVUFBVTtvQkFFeEIsSUFBSSxFQUFFO3dCQUNKLFVBQVUsRUFBRSxRQUFRLENBQUMsaUJBQWlCO3dCQUN0QyxRQUFRLEVBQUUsRUFBRTt3QkFDWixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87d0JBQ3ZCLFlBQVksRUFBRSxDQUFDO3FCQUNoQjtpQkFDRjthQUNGO1lBRUQsV0FBVyxFQUFFO2dCQUNYLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFNBQVMsRUFBRSxFQUFFO2dCQUNiLFFBQVEsRUFBRSxRQUFRO2dCQUVsQixRQUFRLEVBQUU7b0JBQ1IsVUFBVSxFQUFFLFFBQVEsQ0FBQyxrQkFBa0I7b0JBQ3ZDLFFBQVEsRUFBRSxFQUFFO29CQUNaLE1BQU0sRUFBRSxTQUFTO2lCQUNsQjthQUNGO1NBQ0Y7UUFFRCxLQUFLLEVBQUU7WUFDTCxLQUFLLEVBQUUsTUFBTTtZQUNiLFFBQVEsRUFBRSxNQUFNO1lBQ2hCLE1BQU0sRUFBRSxNQUFNO1NBQ2Y7UUFFRCxJQUFJLEVBQUU7WUFDSixLQUFLLEVBQUUsUUFBUSxDQUFDLFlBQVk7U0FDN0I7UUFFRCxLQUFLLEVBQUU7WUFDTCxLQUFLLEVBQUUsRUFBRTtTQUNWO1FBRUQsU0FBUyxFQUFFO1lBQ1QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixVQUFVLEVBQUUsUUFBUTtZQUNwQixZQUFZLEVBQUUsZUFBYSxRQUFRLENBQUMsYUFBZTtZQUNuRCxZQUFZLEVBQUUsRUFBRTtTQUVqQjtRQUVELG9CQUFvQixFQUFFO1lBQ3BCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsVUFBVSxFQUFFLFFBQVE7WUFDcEIsY0FBYyxFQUFFLGVBQWU7WUFDL0IsYUFBYSxFQUFFLEVBQUU7WUFDakIsWUFBWSxFQUFFLGVBQWEsUUFBUSxDQUFDLGFBQWU7WUFFbkQsSUFBSSxFQUFFO2dCQUNKLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLFVBQVUsRUFBRSxRQUFRO2dCQUNwQixjQUFjLEVBQUUsUUFBUTtnQkFDeEIsS0FBSyxFQUFFLGtCQUFrQjtnQkFDekIsTUFBTSxFQUFFLFNBQVM7YUFDbEI7WUFFRCxPQUFPLEVBQUU7Z0JBQ1AsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsVUFBVSxFQUFFLFFBQVE7Z0JBQ3BCLGNBQWMsRUFBRSxRQUFRO2dCQUN4QixLQUFLLEVBQUUsS0FBSztnQkFDWixNQUFNLEVBQUUsU0FBUzthQUNsQjtTQUNGO1FBRUQsWUFBWSxFQUFFO1lBQ1osT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixVQUFVLEVBQUUsRUFBRTtZQUVkLE1BQU0sRUFBRSxFQUFFO1lBRVYsV0FBVyxFQUFFO2dCQUNYLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLGFBQWEsRUFBRSxRQUFRO2dCQUN2QixLQUFLLEVBQUUsTUFBTTtnQkFDYixZQUFZLEVBQUUsZUFBYSxRQUFRLENBQUMsYUFBZTtnQkFDbkQsYUFBYSxFQUFFLEVBQUU7Z0JBRWpCLElBQUksRUFBRSxFQUFFO2dCQUVSLElBQUksRUFBRTtvQkFDSixVQUFVLEVBQUUsUUFBUSxDQUFDLGlCQUFpQjtvQkFDdEMsUUFBUSxFQUFFLEVBQUU7b0JBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO29CQUN2QixZQUFZLEVBQUUsQ0FBQztpQkFDaEI7Z0JBRUQsT0FBTyxFQUFFO29CQUNQLFVBQVUsRUFBRSxRQUFRLENBQUMsaUJBQWlCO29CQUN0QyxRQUFRLEVBQUUsRUFBRTtvQkFDWixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO29CQUN2QixTQUFTLEVBQUUsU0FBUztpQkFDckI7YUFDRjtTQUNGO1FBRUQsaUJBQWlCLEVBQUU7WUFDakIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixVQUFVLEVBQUUsUUFBUTtZQUNwQixVQUFVLEVBQUUsRUFBRTtZQUVkLE1BQU0sRUFBRTtnQkFDTixLQUFLLEVBQUUsRUFBRTtnQkFDVCxRQUFRLEVBQUUsRUFBRTtnQkFDWixNQUFNLEVBQUUsRUFBRTtnQkFDVixZQUFZLEVBQUUsRUFBRTtnQkFDaEIsV0FBVyxFQUFFLEVBQUU7Z0JBQ2Ysa0JBQWtCLEVBQUUsUUFBUTtnQkFDNUIsY0FBYyxFQUFFLE9BQU87Z0JBQ3ZCLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztnQkFDakMsVUFBVSxFQUFFLEVBQUU7YUFDZjtZQUVELEtBQUssRUFBRTtnQkFDTCxNQUFNLEVBQUUsRUFBRTtnQkFDVixLQUFLLEVBQUUsTUFBTTtnQkFDYixlQUFlLEVBQUUsU0FBUztnQkFDMUIsWUFBWSxFQUFFLEVBQUU7Z0JBQ2hCLFVBQVUsRUFBRSxNQUFNO2dCQUNsQixTQUFTLEVBQUUsUUFBUTtnQkFDbkIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO2FBQ3hCO1lBRUQsU0FBUyxFQUFFO2dCQUNULFlBQVksRUFBRSxDQUFDO2dCQUNmLFVBQVUsRUFBRSxDQUFDO2dCQUNiLGFBQWEsRUFBRSxDQUFDO2FBQ2pCO1lBRUQsV0FBVyxFQUFFO2dCQUNYLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLFVBQVUsRUFBRSxRQUFRO2FBQ3JCO1NBQ0Y7S0FDRjtDQUNGLENBQUMifQ==
// CONCATENATED MODULE: ./components/container/feed-list/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};



function renderView(props) {
    var list = props.list, userProfile = props.userProfile, style = props.style, history = props.history, isFeedDetail = props.isFeedDetail;
    var handleRenderItem = function (item) {
        var feedItemProps = {
            item: item,
            history: history,
            userProfile: userProfile,
            isFeedDetail: isFeedDetail,
            key: "feed-detail-item-" + item.id
        };
        return (react["createElement"]("div", { key: "feed-item-" + item.id },
            react["createElement"](feed_item["a" /* default */], __assign({}, feedItemProps))));
    };
    return (react["createElement"]("div", { style: [feed_list_style, style] }, Array.isArray(list)
        && list.length > 0
        && list.map(handleRenderItem)));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxRQUFRLE1BQU0sY0FBYyxDQUFDO0FBR3BDLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixNQUFNLHFCQUFxQixLQUFpQjtJQUNsQyxJQUFBLGlCQUFJLEVBQUUsK0JBQVcsRUFBRSxtQkFBSyxFQUFFLHVCQUFPLEVBQUUsaUNBQVksQ0FBVztJQUVsRSxJQUFNLGdCQUFnQixHQUFHLFVBQUEsSUFBSTtRQUMzQixJQUFNLGFBQWEsR0FBRztZQUNwQixJQUFJLE1BQUE7WUFDSixPQUFPLFNBQUE7WUFDUCxXQUFXLGFBQUE7WUFDWCxZQUFZLGNBQUE7WUFDWixHQUFHLEVBQUUsc0JBQW9CLElBQUksQ0FBQyxFQUFJO1NBQ25DLENBQUM7UUFFRixNQUFNLENBQUMsQ0FDTCw2QkFBSyxHQUFHLEVBQUUsZUFBYSxJQUFJLENBQUMsRUFBSTtZQUM5QixvQkFBQyxRQUFRLGVBQUssYUFBYSxFQUFJLENBQzNCLENBQ1AsQ0FBQztJQUNKLENBQUMsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsSUFFdEIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7V0FDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDO1dBQ2YsSUFBSSxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUUzQixDQUNQLENBQUM7QUFDSixDQUFDO0FBQUEsQ0FBQyJ9
// CONCATENATED MODULE: ./components/container/feed-list/initialize.tsx
var DEFAULT_PROPS = {
    list: [],
    commentList: [],
    type: 'normal',
    isFeedDetail: false
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLFdBQVcsRUFBRSxFQUFFO0lBQ2YsSUFBSSxFQUFFLFFBQVE7SUFDZCxZQUFZLEVBQUUsS0FBSztDQUNOLENBQUMifQ==
// CONCATENATED MODULE: ./components/container/feed-list/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var component_Feed = /** @class */ (function (_super) {
    __extends(Feed, _super);
    function Feed(props) {
        return _super.call(this, props) || this;
    }
    Feed.prototype.shouldComponentUpdate = function (nextProps) {
        if (!Object(validate["h" /* isCompareObject */])(nextProps.list, this.props.list)) {
            return true;
        }
        ;
        if (!Object(validate["h" /* isCompareObject */])(nextProps.userProfile, this.props.userProfile)) {
            return true;
        }
        ;
        if (nextProps.isFeedDetail !== this.props.isFeedDetail) {
            return true;
        }
        ;
        return false;
    };
    Feed.prototype.render = function () {
        return renderView(this.props);
    };
    ;
    Feed.defaultProps = DEFAULT_PROPS;
    Feed = __decorate([
        radium
    ], Feed);
    return Feed;
}(react["Component"]));
/* harmony default export */ var component = (component_Feed);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBRzFELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFDcEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUc3QztJQUFtQix3QkFBZ0M7SUFHakQsY0FBWSxLQUFLO2VBQ2Ysa0JBQU0sS0FBSyxDQUFDO0lBQ2QsQ0FBQztJQUVELG9DQUFxQixHQUFyQixVQUFzQixTQUFTO1FBQzdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQUMsQ0FBQztRQUFBLENBQUM7UUFDeEUsRUFBRSxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFBQyxDQUFDO1FBQUEsQ0FBQztRQUN0RixFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsWUFBWSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFBQyxDQUFDO1FBQUEsQ0FBQztRQUV6RSxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVELHFCQUFNLEdBQU47UUFDRSxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBQUEsQ0FBQztJQWhCSyxpQkFBWSxHQUFlLGFBQWEsQ0FBQztJQUQ1QyxJQUFJO1FBRFQsTUFBTTtPQUNELElBQUksQ0FrQlQ7SUFBRCxXQUFDO0NBQUEsQUFsQkQsQ0FBbUIsS0FBSyxDQUFDLFNBQVMsR0FrQmpDO0FBQ0QsZUFBZSxJQUFJLENBQUMifQ==
// CONCATENATED MODULE: ./components/container/feed-list/index.tsx

/* harmony default export */ var feed_list = __webpack_exports__["a"] = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxxQkFBcUIsTUFBTSxhQUFhLENBQUM7QUFDaEQsZUFBZSxxQkFBcUIsQ0FBQyJ9

/***/ }),

/***/ 784:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./action/magazine.ts + 1 modules
var magazine = __webpack_require__(762);

// EXTERNAL MODULE: ./action/shop.ts + 1 modules
var shop = __webpack_require__(760);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/encode.ts
var encode = __webpack_require__(7);

// EXTERNAL MODULE: ./constants/application/magazine.ts
var application_magazine = __webpack_require__(170);

// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// CONCATENATED MODULE: ./components/community/right-bar-community/initialize.tsx
var DEFAULT_PROPS = {
    days: 7,
    hashtagSelected: ''
};
var INITIAL_STATE = {};
var tagList = [
    'Beauty Tips',
    'Lip Stick',
    'Hair',
    'Skin Care',
    'Body Bath',
    'ACNE',
    'New Brand',
    'Lixibox Flagship Store'
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsQ0FBQztJQUNQLGVBQWUsRUFBRSxFQUFFO0NBQ1YsQ0FBQztBQUVaLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRyxFQUFZLENBQUM7QUFFMUMsTUFBTSxDQUFDLElBQU0sT0FBTyxHQUFHO0lBQ3JCLGFBQWE7SUFDYixXQUFXO0lBQ1gsTUFBTTtJQUNOLFdBQVc7SUFDWCxXQUFXO0lBQ1gsTUFBTTtJQUNOLFdBQVc7SUFDWCx3QkFBd0I7Q0FDekIsQ0FBQSJ9
// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./components/product/slider/index.tsx + 6 modules
var slider = __webpack_require__(770);

// EXTERNAL MODULE: ./components/magazine/image-slider/index.tsx + 11 modules
var image_slider = __webpack_require__(764);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/community/right-bar-community/style.tsx

/* harmony default export */ var right_bar_community_style = ({
    wrapLayout: {
        paddingTop: 10
    },
    header: {
        fontSize: 20,
        paddingBottom: 20,
        fontFamily: variable["fontTrirong"],
        marginBottom: 10
    },
    headerNoBorder: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        borderBottom: 'none'
    },
    text: {
        fontSize: 16,
        marginBottom: 10,
        color: variable["colorBlack08"]
    },
    wrapElement: {
        boxShadow: variable["shadowBlur"],
        borderRadius: 5,
        backgroundColor: variable["colorWhite"],
        marginBottom: 20
    },
    hashtagGroup: {
        borderRadius: 5,
        backgroundColor: variable["colorWhite"],
        marginBottom: 20
    },
    wrapTag: {
        display: variable["display"].flex,
        flexWrap: 'wrap',
        tag: {
            backgroundColor: variable["colorWhite"],
            color: variable["colorBlack"],
            border: "1px solid " + variable["colorBlack"],
            marginRight: 10,
            marginBottom: 10,
            padding: 10,
            borderRadius: 3,
            height: 30,
            lineHeight: '10px',
            active: {
                color: variable["colorWhite"],
                backgroundColor: variable["colorBlack"],
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxlQUFlO0lBQ2IsVUFBVSxFQUFFO1FBQ1YsVUFBVSxFQUFFLEVBQUU7S0FDZjtJQUVELE1BQU0sRUFBRTtRQUNOLFFBQVEsRUFBRSxFQUFFO1FBQ1osYUFBYSxFQUFFLEVBQUU7UUFDakIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxXQUFXO1FBQ2hDLFlBQVksRUFBRSxFQUFFO0tBQ2pCO0lBRUQsY0FBYyxFQUFFO1FBQ2QsVUFBVSxFQUFFLENBQUM7UUFDYixZQUFZLEVBQUUsQ0FBQztRQUNmLGFBQWEsRUFBRSxDQUFDO1FBQ2hCLFdBQVcsRUFBRSxDQUFDO1FBQ2QsWUFBWSxFQUFFLE1BQU07S0FDckI7SUFFRCxJQUFJLEVBQUU7UUFDSixRQUFRLEVBQUUsRUFBRTtRQUNaLFlBQVksRUFBRSxFQUFFO1FBQ2hCLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTtLQUM3QjtJQUVELFdBQVcsRUFBRTtRQUNYLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTtRQUM5QixZQUFZLEVBQUUsQ0FBQztRQUNmLGVBQWUsRUFBRSxRQUFRLENBQUMsVUFBVTtRQUNwQyxZQUFZLEVBQUUsRUFBRTtLQUNqQjtJQUVELFlBQVksRUFBRTtRQUNaLFlBQVksRUFBRSxDQUFDO1FBQ2YsZUFBZSxFQUFFLFFBQVEsQ0FBQyxVQUFVO1FBQ3BDLFlBQVksRUFBRSxFQUFFO0tBQ2pCO0lBRUQsT0FBTyxFQUFFO1FBQ1AsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUM5QixRQUFRLEVBQUUsTUFBTTtRQUVoQixHQUFHLEVBQUU7WUFDSCxlQUFlLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDcEMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1lBQzFCLE1BQU0sRUFBRSxlQUFhLFFBQVEsQ0FBQyxVQUFZO1lBQzFDLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLEVBQUU7WUFDaEIsT0FBTyxFQUFFLEVBQUU7WUFDWCxZQUFZLEVBQUUsQ0FBQztZQUNmLE1BQU0sRUFBRSxFQUFFO1lBQ1YsVUFBVSxFQUFFLE1BQU07WUFFbEIsTUFBTSxFQUFFO2dCQUNOLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDMUIsZUFBZSxFQUFFLFFBQVEsQ0FBQyxVQUFVO2FBQ3JDO1NBQ0Y7S0FDRjtDQUNNLENBQUMifQ==
// CONCATENATED MODULE: ./components/community/right-bar-community/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};









var renderHeader = function (name, style) {
    if (style === void 0) { style = {}; }
    return react["createElement"]("div", { style: [right_bar_community_style.header, style] }, name);
};
var renderText = function (text, lastChild) {
    if (lastChild === void 0) { lastChild = {}; }
    return react["createElement"]("div", { style: [right_bar_community_style.text, lastChild] }, text);
};
var renderTags = function (_a) {
    var hashtagList = _a.hashtagList, hashtagSelected = _a.hashtagSelected;
    return (react["createElement"]("div", { style: right_bar_community_style.wrapTag }, Array.isArray(hashtagList)
        && hashtagList.length !== 0
        && hashtagList.map(handleRenderTags, { hashtagSelected: hashtagSelected })));
};
function handleRenderTags(item, index) {
    var tagName = item && item.name || '';
    var linkProps = {
        to: routing["t" /* ROUTING_COMMUNITY_TAG_PATH */] + "/" + tagName,
        key: "tag-item-" + index,
        style: Object.assign({}, right_bar_community_style.wrapTag.tag, this.hashtagSelected === tagName && right_bar_community_style.wrapTag.tag.active)
    };
    return react["createElement"](react_router_dom["NavLink"], __assign({}, linkProps), "# " + tagName);
}
var renderTrendingTags = function (_a) {
    var hashtagList = _a.hashtagList, hashtagSelected = _a.hashtagSelected;
    return (hashtagList
        && hashtagList.length !== 0
        && (react["createElement"]("div", { style: right_bar_community_style.hashtagGroup },
            renderHeader('Trending Tags', { paddingBottom: 10 }),
            renderTags({ hashtagList: hashtagList, hashtagSelected: hashtagSelected }))));
};
var renderSuggestionFromExperts = function (list) {
    var lastestProductProps = {
        showHeader: false,
        column: 1,
        data: list && list.slice(0, 5) || [],
        showViewMore: false,
    };
    return (list
        && 0 !== list.length
        &&
            react["createElement"]("div", null,
                renderHeader('Hot Boxes', right_bar_community_style.headerNoBorder),
                react["createElement"]("div", { style: right_bar_community_style.wrapElement },
                    react["createElement"](slider["a" /* default */], __assign({}, lastestProductProps)))));
};
var renderHotMagazine = function (data) {
    var magazineImageProps = {
        title: '',
        column: 1,
        showHeader: false,
        showViewMore: false,
        data: data && !!data.length && data.slice(0, 5) || [],
        style: { paddingBottom: 0 }
    };
    return (data && !!data.length
        &&
            react["createElement"]("div", null,
                renderHeader('Hot magazine', right_bar_community_style.headerNoBorder),
                react["createElement"]("div", { style: right_bar_community_style.wrapElement },
                    react["createElement"](image_slider["a" /* default */], __assign({}, magazineImageProps)))));
};
function renderComponent(_a) {
    var props = _a.props;
    var _b = props, days = _b.days, hashtagSelected = _b.hashtagSelected, dataHomePage = _b.shopStore.dataHomePage, hashtags = _b.communityStore.hashtags, magazineList = _b.magazineStore.magazineList;
    var defaultMagazineListHash = Object(encode["h" /* objectToHash */])({ page: 1, perPage: 12, type: application_magazine["a" /* MAGAZINE_LIST_TYPE */].DEFAULT });
    var keyHash = Object(encode["h" /* objectToHash */])({ days: days });
    var hashtagList = hashtags && !Object(validate["l" /* isUndefined */])(hashtags[keyHash]) ? hashtags[keyHash] : [];
    return (react["createElement"]("div", { style: right_bar_community_style.wrapLayout },
        renderTrendingTags({ hashtagList: hashtagList, hashtagSelected: hashtagSelected }),
        renderSuggestionFromExperts(dataHomePage && dataHomePage.latest_boxes && dataHomePage.latest_boxes.items || []),
        renderHotMagazine(magazineList[defaultMagazineListHash] || [])));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sYUFBYSxNQUFNLHNCQUFzQixDQUFDO0FBQ2pELE9BQU8sbUJBQW1CLE1BQU0sNkJBQTZCLENBQUM7QUFDOUQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3JELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUVwRixPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFHNUIsSUFBTSxZQUFZLEdBQUcsVUFBQyxJQUFJLEVBQUUsS0FBVTtJQUFWLHNCQUFBLEVBQUEsVUFBVTtJQUNwQyxNQUFNLENBQUMsNkJBQUssS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsSUFBRyxJQUFJLENBQU8sQ0FBQTtBQUN4RCxDQUFDLENBQUE7QUFFRCxJQUFNLFVBQVUsR0FBRyxVQUFDLElBQUksRUFBRSxTQUFjO0lBQWQsMEJBQUEsRUFBQSxjQUFjO0lBQ3RDLE1BQU0sQ0FBQyw2QkFBSyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxJQUFHLElBQUksQ0FBTyxDQUFBO0FBQzFELENBQUMsQ0FBQTtBQUVELElBQU0sVUFBVSxHQUFHLFVBQUMsRUFBZ0M7UUFBOUIsNEJBQVcsRUFBRSxvQ0FBZTtJQUNoRCxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU8sSUFFckIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUM7V0FDdkIsV0FBVyxDQUFDLE1BQU0sS0FBSyxDQUFDO1dBQ3hCLFdBQVcsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxlQUFlLGlCQUFBLEVBQUUsQ0FBQyxDQUV2RCxDQUNQLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRiwwQkFBMEIsSUFBSSxFQUFFLEtBQUs7SUFDbkMsSUFBTSxPQUFPLEdBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDO0lBRXhDLElBQU0sU0FBUyxHQUFHO1FBQ2hCLEVBQUUsRUFBSywwQkFBMEIsU0FBSSxPQUFTO1FBQzlDLEdBQUcsRUFBRSxjQUFZLEtBQU87UUFDeEIsS0FBSyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxlQUFlLEtBQUssT0FBTyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQztLQUMxRyxDQUFDO0lBRUYsTUFBTSxDQUFDLG9CQUFDLE9BQU8sZUFBSyxTQUFTLEdBQUcsT0FBSyxPQUFTLENBQVcsQ0FBQztBQUM1RCxDQUFDO0FBRUQsSUFBTSxrQkFBa0IsR0FBRyxVQUFDLEVBQWdDO1FBQTlCLDRCQUFXLEVBQUUsb0NBQWU7SUFDeEQsTUFBTSxDQUFDLENBQ0wsV0FBVztXQUNSLFdBQVcsQ0FBQyxNQUFNLEtBQUssQ0FBQztXQUN4QixDQUNELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWTtZQU8zQixZQUFZLENBQUMsZUFBZSxFQUFFLEVBQUUsYUFBYSxFQUFFLEVBQUUsRUFBRSxDQUFDO1lBQ3BELFVBQVUsQ0FBQyxFQUFFLFdBQVcsYUFBQSxFQUFFLGVBQWUsaUJBQUEsRUFBRSxDQUFDLENBQ3pDLENBQ1AsQ0FDRixDQUFBO0FBQ0gsQ0FBQyxDQUFBO0FBRUQsSUFBTSwyQkFBMkIsR0FBRyxVQUFDLElBQUk7SUFFdkMsSUFBTSxtQkFBbUIsR0FBRztRQUMxQixVQUFVLEVBQUUsS0FBSztRQUNqQixNQUFNLEVBQUUsQ0FBQztRQUNULElBQUksRUFBRSxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRTtRQUNwQyxZQUFZLEVBQUUsS0FBSztLQUNwQixDQUFDO0lBQ0YsTUFBTSxDQUFDLENBQ0wsSUFBSTtXQUNELENBQUMsS0FBSyxJQUFJLENBQUMsTUFBTTs7WUFFcEI7Z0JBQ0csWUFBWSxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsY0FBYyxDQUFDO2dCQUNoRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVc7b0JBQUUsb0JBQUMsYUFBYSxlQUFLLG1CQUFtQixFQUFJLENBQU0sQ0FDM0UsQ0FDUCxDQUFBO0FBRUgsQ0FBQyxDQUFBO0FBRUQsSUFBTSxpQkFBaUIsR0FBRyxVQUFDLElBQUk7SUFFN0IsSUFBTSxrQkFBa0IsR0FBRztRQUN6QixLQUFLLEVBQUUsRUFBRTtRQUNULE1BQU0sRUFBRSxDQUFDO1FBQ1QsVUFBVSxFQUFFLEtBQUs7UUFDakIsWUFBWSxFQUFFLEtBQUs7UUFDbkIsSUFBSSxFQUFFLElBQUksSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFO1FBQ3JELEtBQUssRUFBRSxFQUFFLGFBQWEsRUFBRSxDQUFDLEVBQUU7S0FDNUIsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLElBQUksSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU07O1lBRXJCO2dCQUNHLFlBQVksQ0FBQyxjQUFjLEVBQUUsS0FBSyxDQUFDLGNBQWMsQ0FBQztnQkFDbkQsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXO29CQUFFLG9CQUFDLG1CQUFtQixlQUFLLGtCQUFrQixFQUFJLENBQU0sQ0FDaEYsQ0FDUCxDQUFBO0FBRUgsQ0FBQyxDQUFBO0FBRUQsTUFBTSwwQkFBMEIsRUFBUztRQUFQLGdCQUFLO0lBQy9CLElBQUEsVUFNYSxFQUxqQixjQUFJLEVBQ0osb0NBQWUsRUFDRix3Q0FBWSxFQUNQLHFDQUFRLEVBQ1QsNENBQVksQ0FDWDtJQUVwQixJQUFNLHVCQUF1QixHQUFHLFlBQVksQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsa0JBQWtCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUV6RyxJQUFNLE9BQU8sR0FBRyxZQUFZLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDLENBQUM7SUFDdkMsSUFBTSxXQUFXLEdBQUcsUUFBUSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUV6RixNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFVBQVU7UUFDekIsa0JBQWtCLENBQUMsRUFBRSxXQUFXLGFBQUEsRUFBRSxlQUFlLGlCQUFBLEVBQUUsQ0FBQztRQUNwRCwyQkFBMkIsQ0FBQyxZQUFZLElBQUksWUFBWSxDQUFDLFlBQVksSUFBSSxZQUFZLENBQUMsWUFBWSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUM7UUFDL0csaUJBQWlCLENBQUMsWUFBWSxDQUFDLHVCQUF1QixDQUFDLElBQUksRUFBRSxDQUFDLENBQzNELENBQ1AsQ0FBQztBQUNKLENBQUM7QUFBQSxDQUFDIn0=
// CONCATENATED MODULE: ./components/community/right-bar-community/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var component_RightBarCommunity = /** @class */ (function (_super) {
    __extends(RightBarCommunity, _super);
    function RightBarCommunity(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    RightBarCommunity.prototype.componentDidMount = function () {
        var _a = this.props, fetchDataHomePage = _a.fetchDataHomePage, fetchMagazineListAction = _a.fetchMagazineListAction, dataHomePage = _a.shopStore.dataHomePage, magazineList = _a.magazineStore.magazineList;
        /** Magazine data */
        var fetchDefaultMagazineParam = {
            page: 1,
            perPage: 12,
            type: application_magazine["a" /* MAGAZINE_LIST_TYPE */].DEFAULT
        };
        var defaultMagazineListHash = Object(encode["h" /* objectToHash */])(fetchDefaultMagazineParam);
        true === Object(validate["l" /* isUndefined */])(magazineList[defaultMagazineListHash])
            && fetchMagazineListAction(fetchDefaultMagazineParam);
        /** Watched List */
        Object(validate["j" /* isEmptyObject */])(dataHomePage) && fetchDataHomePage();
    };
    RightBarCommunity.prototype.render = function () {
        var args = {
            props: this.props
        };
        return renderComponent(args);
    };
    RightBarCommunity.defaultProps = DEFAULT_PROPS;
    RightBarCommunity = __decorate([
        radium
    ], RightBarCommunity);
    return RightBarCommunity;
}(react["Component"]));
;
/* harmony default export */ var component = (component_RightBarCommunity);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3JELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxhQUFhLEVBQUUsV0FBVyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFFckUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFFNUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUd6QztJQUFnQyxxQ0FBK0I7SUFHN0QsMkJBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUF1QixDQUFDOztJQUN2QyxDQUFDO0lBRUQsNkNBQWlCLEdBQWpCO1FBQ1EsSUFBQSxlQUtrQixFQUp0Qix3Q0FBaUIsRUFDakIsb0RBQXVCLEVBQ1Ysd0NBQVksRUFDUiw0Q0FBWSxDQUNOO1FBRXpCLG9CQUFvQjtRQUNwQixJQUFNLHlCQUF5QixHQUFHO1lBQ2hDLElBQUksRUFBRSxDQUFDO1lBQ1AsT0FBTyxFQUFFLEVBQUU7WUFDWCxJQUFJLEVBQUUsa0JBQWtCLENBQUMsT0FBTztTQUNqQyxDQUFDO1FBRUYsSUFBTSx1QkFBdUIsR0FBRyxZQUFZLENBQUMseUJBQXlCLENBQUMsQ0FBQztRQUN4RSxJQUFJLEtBQUssV0FBVyxDQUFDLFlBQVksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO2VBQ3RELHVCQUF1QixDQUFDLHlCQUF5QixDQUFDLENBQUM7UUFFeEQsbUJBQW1CO1FBQ25CLGFBQWEsQ0FBQyxZQUFZLENBQUMsSUFBSSxpQkFBaUIsRUFBRSxDQUFDO0lBQ3JELENBQUM7SUFFRCxrQ0FBTSxHQUFOO1FBQ0UsSUFBTSxJQUFJLEdBQUc7WUFDWCxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7U0FDbEIsQ0FBQTtRQUVELE1BQU0sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQXBDTSw4QkFBWSxHQUFXLGFBQWEsQ0FBQztJQUR4QyxpQkFBaUI7UUFEdEIsTUFBTTtPQUNELGlCQUFpQixDQXNDdEI7SUFBRCx3QkFBQztDQUFBLEFBdENELENBQWdDLEtBQUssQ0FBQyxTQUFTLEdBc0M5QztBQUFBLENBQUM7QUFFRixlQUFlLGlCQUFpQixDQUFDIn0=
// CONCATENATED MODULE: ./components/community/right-bar-community/store.tsx
var connect = __webpack_require__(129).connect;



var mapStateToProps = function (state) { return ({
    shopStore: state.shop,
    magazineStore: state.magazine,
    communityStore: state.community
}); };
var mapDispatchToProps = function (dispatch) { return ({
    fetchDataHomePage: function () { return dispatch(Object(shop["b" /* fetchDataHomePageAction */])()); },
    fetchMagazineListAction: function (data) { return dispatch(Object(magazine["e" /* fetchMagazineListAction */])(data)); }
}); };
/* harmony default export */ var store = (connect(mapStateToProps, mapDispatchToProps)(component));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUMvQyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNuRSxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUUvRCxPQUFPLGlCQUFpQixNQUFNLGFBQWEsQ0FBQztBQUU1QyxNQUFNLENBQUMsSUFBTSxlQUFlLEdBQUcsVUFBQyxLQUFLLElBQUssT0FBQSxDQUFDO0lBQ3pDLFNBQVMsRUFBRSxLQUFLLENBQUMsSUFBSTtJQUNyQixhQUFhLEVBQUUsS0FBSyxDQUFDLFFBQVE7SUFDN0IsY0FBYyxFQUFFLEtBQUssQ0FBQyxTQUFTO0NBQ2hDLENBQUMsRUFKd0MsQ0FJeEMsQ0FBQztBQUVILE1BQU0sQ0FBQyxJQUFNLGtCQUFrQixHQUFHLFVBQUMsUUFBUSxJQUFLLE9BQUEsQ0FBQztJQUMvQyxpQkFBaUIsRUFBRSxjQUFNLE9BQUEsUUFBUSxDQUFDLHVCQUF1QixFQUFFLENBQUMsRUFBbkMsQ0FBbUM7SUFDNUQsdUJBQXVCLEVBQUUsVUFBQyxJQUFTLElBQUssT0FBQSxRQUFRLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBdkMsQ0FBdUM7Q0FDaEYsQ0FBQyxFQUg4QyxDQUc5QyxDQUFDO0FBRUgsZUFBZSxPQUFPLENBQ3BCLGVBQWUsRUFDZixrQkFBa0IsQ0FDbkIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDIn0=
// CONCATENATED MODULE: ./components/community/right-bar-community/index.tsx
// import * as React from 'react';
// import * as Loadable from 'react-loadable';
// import LazyLoading from '../../ui/lazy-loading';
// export default Loadable({
//   loader: () => import('./store'),
//   loading: () => <LazyLoading />
// });

/* harmony default export */ var right_bar_community = __webpack_exports__["a"] = (store);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsa0NBQWtDO0FBQ2xDLDhDQUE4QztBQUM5QyxtREFBbUQ7QUFFbkQsNEJBQTRCO0FBQzVCLHFDQUFxQztBQUNyQyxtQ0FBbUM7QUFDbkMsTUFBTTtBQUVOLE9BQU8sU0FBUyxNQUFNLFNBQVMsQ0FBQztBQUNoQyxlQUFlLFNBQVMsQ0FBQyJ9

/***/ }),

/***/ 786:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return DEAL_FEED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return COMMUNITY_CATEGORY_NAME; });
var DEAL_FEED = {
    ID: 150307,
};
var COMMUNITY_CATEGORY_NAME = {
    1: 'New Feeds',
    2: 'Unboxing',
    3: 'Best Deals',
    4: 'Trending Tags',
    5: 'User Feed Detail'
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbXVuaXR5LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tbXVuaXR5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE1BQU0sQ0FBQyxJQUFNLFNBQVMsR0FBRztJQUN2QixFQUFFLEVBQUUsTUFBTTtDQUNYLENBQUM7QUFFRixNQUFNLENBQUMsSUFBTSx1QkFBdUIsR0FBRztJQUNyQyxDQUFDLEVBQUUsV0FBVztJQUNkLENBQUMsRUFBRSxVQUFVO0lBQ2IsQ0FBQyxFQUFFLFlBQVk7SUFDZixDQUFDLEVBQUUsZUFBZTtJQUNsQixDQUFDLEVBQUUsa0JBQWtCO0NBQ3RCLENBQUMifQ==

/***/ }),

/***/ 792:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// CONCATENATED MODULE: ./api/community.ts

var fetchCommunityHashtagFeeds = function (_a) {
    var hashtag = _a.hashtag, _b = _a.limit, limit = _b === void 0 ? 10 : _b, _c = _a.days, days = _c === void 0 ? 30 : _c, _d = _a.currentId, currentId = _d === void 0 ? 0 : _d;
    var query = "?hashtag=" + hashtag + "&days=" + days + "&limit=" + limit + (currentId !== 0 ? "&current_id=" + currentId : '');
    return Object(restful_method["b" /* get */])({
        path: "/community/hashtag_feeds" + query,
        description: 'Fetch community hashtag feeds',
        errorMesssage: "Can't fetch community hashtag feeds. Please try again",
    });
};
var fetchCommunityHashtags = function (_a) {
    var _b = _a.days, days = _b === void 0 ? 7 : _b;
    var query = "?days=" + days;
    return Object(restful_method["b" /* get */])({
        path: "/community/hashtags" + query,
        description: 'Fetch community hashtags',
        errorMesssage: "Can't fetch community hashtags. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbXVuaXR5LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tbXVuaXR5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxHQUFHLEVBQWUsTUFBTSwwQkFBMEIsQ0FBQztBQUk1RCxNQUFNLENBQUMsSUFBTSwwQkFBMEIsR0FDckMsVUFBQyxFQUFpRDtRQUEvQyxvQkFBTyxFQUFFLGFBQVUsRUFBViwrQkFBVSxFQUFFLFlBQVMsRUFBVCw4QkFBUyxFQUFFLGlCQUFhLEVBQWIsa0NBQWE7SUFDOUMsSUFBTSxLQUFLLEdBQUcsY0FBWSxPQUFPLGNBQVMsSUFBSSxlQUFVLEtBQU8sR0FBRyxDQUFDLFNBQVMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFlLFNBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7SUFFdEgsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSw2QkFBMkIsS0FBTztRQUN4QyxXQUFXLEVBQUUsK0JBQStCO1FBQzVDLGFBQWEsRUFBRSx1REFBdUQ7S0FDdkUsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUosTUFBTSxDQUFDLElBQU0sc0JBQXNCLEdBQ2pDLFVBQUMsRUFBWTtRQUFWLFlBQVEsRUFBUiw2QkFBUTtJQUNULElBQU0sS0FBSyxHQUFHLFdBQVMsSUFBTSxDQUFDO0lBRTlCLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsd0JBQXNCLEtBQU87UUFDbkMsV0FBVyxFQUFFLDBCQUEwQjtRQUN2QyxhQUFhLEVBQUUsa0RBQWtEO0tBQ2xFLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQyJ9
// EXTERNAL MODULE: ./constants/api/community.ts
var community = __webpack_require__(112);

// CONCATENATED MODULE: ./action/community.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fetchCommunityHashtagsAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fetchCommunityHashtagFeedsAction; });


/**
 * Fetch commnunity hashtags list by days
 *
 * @param {number} days
 */
var fetchCommunityHashtagsAction = function (_a) {
    var _b = _a.days, days = _b === void 0 ? 7 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: community["a" /* FETCH_COMMUNITY_HASHTAGS */],
            payload: { promise: fetchCommunityHashtags({ days: days }).then(function (res) { return res; }) },
            meta: { days: days }
        });
    };
};
/**
* Fetch commnunity hashtags list by days
*
* @param {number} days
*/
var fetchCommunityHashtagFeedsAction = function (_a) {
    var hashtag = _a.hashtag, _b = _a.limit, limit = _b === void 0 ? 10 : _b, _c = _a.days, days = _c === void 0 ? 30 : _c, _d = _a.currentId, currentId = _d === void 0 ? 0 : _d;
    return function (dispatch, getState) {
        return dispatch({
            type: community["b" /* FETCH_COMMUNITY_HASHTAG_FEEDS */],
            payload: { promise: fetchCommunityHashtagFeeds({ hashtag: hashtag, limit: limit, days: days, currentId: currentId }).then(function (res) { return res; }) },
            meta: { hashtag: hashtag, limit: limit, days: days, currentId: currentId }
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbXVuaXR5LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tbXVuaXR5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFDTCxzQkFBc0IsRUFDdEIsMEJBQTBCLEVBQzNCLE1BQU0sa0JBQWtCLENBQUM7QUFFMUIsT0FBTyxFQUNMLHdCQUF3QixFQUN4Qiw2QkFBNkIsRUFDOUIsTUFBTSw0QkFBNEIsQ0FBQztBQUVwQzs7OztHQUlHO0FBQ0gsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsRUFBWTtRQUFWLFlBQVEsRUFBUiw2QkFBUTtJQUNULE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSx3QkFBd0I7WUFDOUIsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLHNCQUFzQixDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN2RSxJQUFJLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRTtTQUNmLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBR1Q7Ozs7RUFJRTtBQUNGLE1BQU0sQ0FBQyxJQUFNLGdDQUFnQyxHQUMzQyxVQUFDLEVBQWlEO1FBQS9DLG9CQUFPLEVBQUUsYUFBVSxFQUFWLCtCQUFVLEVBQUUsWUFBUyxFQUFULDhCQUFTLEVBQUUsaUJBQWEsRUFBYixrQ0FBYTtJQUM5QyxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsNkJBQTZCO1lBQ25DLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSwwQkFBMEIsQ0FBQyxFQUFFLE9BQU8sU0FBQSxFQUFFLEtBQUssT0FBQSxFQUFFLElBQUksTUFBQSxFQUFFLFNBQVMsV0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDdEcsSUFBSSxFQUFFLEVBQUUsT0FBTyxTQUFBLEVBQUUsS0FBSyxPQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsU0FBUyxXQUFBLEVBQUU7U0FDMUMsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUMifQ==

/***/ }),

/***/ 794:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// CONCATENATED MODULE: ./components/community/header-mobile/initialize.tsx
var DEFAULT_PROPS = {
    pathname: '',
    hashTagList: [],
    hashtagSelected: ''
};
var INITIAL_STATE = {};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixRQUFRLEVBQUUsRUFBRTtJQUNaLFdBQVcsRUFBRSxFQUFFO0lBQ2YsZUFBZSxFQUFFLEVBQUU7Q0FDVixDQUFDO0FBRVosTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHLEVBQVksQ0FBQyJ9
// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./style/component.ts
var component = __webpack_require__(744);

// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// EXTERNAL MODULE: ./utils/encode.ts
var encode = __webpack_require__(7);

// EXTERNAL MODULE: ./constants/application/community.ts
var community = __webpack_require__(786);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./style/media-queries.ts
var media_queries = __webpack_require__(76);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/community/header-mobile/style.tsx



/* harmony default export */ var style = ({
    headerMenuContainer: {
        display: variable["display"].block,
        position: variable["position"].relative,
        height: 50,
        maxHeight: 50,
        headerMenuParent: {
            width: '100%',
            height: '100%',
            display: variable["display"].flex,
            justifyContent: 'space-between',
            position: variable["position"].relative,
            zIndex: variable["zIndexMax"],
            headerMenuWrap: {
                maxWidth: '70%',
                height: '100%',
                display: variable["display"].flex,
                justifyContent: 'space-between'
            }
        },
        headerMenu: {
            display: variable["display"].flex,
            alignItems: 'center',
            justifyContent: 'flex-start',
            fontFamily: variable["fontAvenirMedium"],
            borderBottom: "1px solid " + variable["colorBlack005"],
            backdropFilter: "blur(10px)",
            WebkitBackdropFilter: "blur(10px)",
            height: '100%',
            width: '100%',
            position: variable["position"].absolute,
            top: 0,
            left: 0,
            zIndex: variable["zIndex9"],
            isTop: {
                position: variable["position"].fixed,
                top: 0,
                left: 0,
                backdropFilter: "blur(10px)",
                WebkitBackdropFilter: "blur(10px)",
                width: '100%',
                height: 50,
                backgroundColor: variable["colorWhite08"]
            },
            textBreadCrumb: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        fontSize: 15,
                        lineHeight: '50px',
                        height: '100%',
                        width: '100%',
                        flex: 10,
                        overflow: 'hidden',
                        paddingLeft: 10,
                        fontFamily: variable["fontTrirong"],
                        color: variable["colorBlack"],
                        display: variable["display"].inlineBlock,
                        textTransform: 'capitalize',
                        cursor: 'pointer',
                    }],
                DESKTOP: [{
                        fontSize: 18,
                        height: 60,
                        lineHeight: '60px',
                        textTransform: 'capitalize'
                    }],
                GENERAL: [{
                        color: variable["colorBlack"],
                        fontFamily: variable["fontAvenirMedium"],
                        cursor: 'pointer',
                    }]
            }),
            icon: function (isOpening) { return ({
                width: 50,
                height: 50,
                color: variable["colorBlack08"],
                transition: variable["transitionNormal"],
                transform: isOpening ? 'rotate(-180deg)' : 'rotate(0deg)'
            }); },
            inner: {
                width: 16,
                height: 16
            }
        },
        overlay: {
            position: variable["position"].absolute,
            width: '100vw',
            height: '100vh',
            top: 50,
            left: 0,
            zIndex: variable["zIndex9"],
            background: variable["colorBlack06"],
            transition: variable["transitionNormal"],
        }
    },
    filterMobile: {
        position: variable["position"].relative,
        borderLeft: "1px solid " + variable["colorBlack005"],
        icon: {
            width: 50,
            maxWidth: 50,
            height: 50,
            display: variable["display"].flex,
            alignItems: 'center',
            justifyContent: 'center',
            fontSize: 25
        },
        angle: {
            position: variable["position"].absolute,
            top: 30,
            right: 15,
            borderTop: "10px solid " + variable["colorTransparent"],
            borderRight: "10px solid " + variable["colorTransparent"],
            borderBottom: "10px solid " + variable["colorE5"],
            borderLeft: "10px solid " + variable["colorTransparent"],
        }
    },
    categoryList: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                height: 'calc(100vh - 50px)',
                overflowY: 'auto'
            }],
        DESKTOP: [{}],
        GENERAL: [{
                position: variable["position"].absolute,
                top: 50,
                left: 0,
                width: '100%',
                zIndex: variable["zIndex9"],
                backgroundColor: variable["colorWhite"],
                display: variable["display"].none,
                opacity: 0
            }]
    }),
    isShowingCategoryList: {
        transition: variable["transitionNormal"],
        display: variable["display"].block,
        opacity: 1,
        zIndex: variable["zIndexMax"]
    },
    categoryWrap: {
        display: variable["display"].block,
        width: '100%',
        paddingTop: 10,
        paddingBottom: 10
    },
    categoryContainer: {
        display: variable["display"].flex,
        flexDirection: 'column',
        whiteSpace: 'nowrap',
        marginLeft: 10,
        paddingRight: 10,
        width: '100%',
        category: {
            display: variable["display"].block,
            fontSize: 16,
            lineHeight: '40px',
            height: 40,
            maxHeight: 40,
            fontFamily: variable["fontAvenirMedium"],
            color: variable["colorBlack"]
        },
        lastChild: {
            marginRight: 10
        }
    },
    mobileVersion: {
        hashTagContainer: {
            position: variable["position"].absolute,
            top: 50,
            right: 0,
            backgroundColor: variable["colorWhite"],
            width: 150,
            // height: '100vh',
            overflowY: 'scroll',
            zIndex: variable["zIndex9"]
        },
        heading: {
            backgroundColor: variable["colorE5"],
            height: 30,
            maxHeight: 30,
            display: variable["display"].flex,
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingLeft: 10,
            paddingRight: 10,
            top: 0,
            zIndex: variable["zIndexMax"],
            title: {
                fontSize: 12,
                color: variable["color4D"],
                fontFamily: variable["fontAvenirMedium"],
                fontWeight: 'bolder'
            }
        },
        hashTagItem: {
            width: '100%',
            cursor: 'pointer',
            paddingTop: 10,
            paddingRight: 10,
            paddingBottom: 10,
            paddingLeft: 10,
            borderBottom: "1px solid " + variable["colorBlack01"],
            height: 40,
            minHeight: 40,
            alignItems: 'center',
            icon: {
                width: 16,
                minWidth: 16,
                height: 16,
                borderRadius: '50%',
                border: "1px solid " + variable["color97"],
                marginRight: 10,
                transition: variable["transitionBackground"],
                position: 'relative',
                backgroundColor: variable["colorWhite"]
            },
            title: {
                fontSize: 12,
                color: variable["colorBlack09"],
                transition: variable["transitionColor"],
                ':hover': {
                    color: variable["colorPink"]
                }
            }
        },
        closeGroup: {
            position: variable["position"].relative,
            right: 4,
            icon: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        width: 10,
                        height: 10
                    }],
                DESKTOP: [{
                        width: 18,
                        height: 18
                    }],
                GENERAL: [{
                        color: variable["color4D"],
                        cursor: 'pointer',
                        transform: "scale(1)",
                        transition: variable["transitionNormal"],
                        position: variable["position"].relative
                    }]
            }),
            innerIcon: {
                width: 18
            }
        },
        hashTagList: (style_a = {
                container: Object(responsive["a" /* combineStyle */])({
                    MOBILE: [{
                            paddingBottom: 0
                        }],
                    DESKTOP: [{}],
                    GENERAL: [{
                            paddingTop: 0,
                            overflowX: 'hidden',
                            overflowY: 'auto',
                        }]
                })
            },
            style_a[media_queries["a" /* default */].tablet1024] = {
                paddingLeft: 0,
                paddingRight: 0,
            },
            style_a),
    },
});
var style_a;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxhQUFhLE1BQU0sOEJBQThCLENBQUM7QUFDekQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ3pELE9BQU8sS0FBSyxRQUFRLE1BQU0seUJBQXlCLENBQUM7QUFFcEQsZUFBZTtJQUNiLG1CQUFtQixFQUFFO1FBQ25CLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7UUFDL0IsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtRQUNwQyxNQUFNLEVBQUUsRUFBRTtRQUNWLFNBQVMsRUFBRSxFQUFFO1FBRWIsZ0JBQWdCLEVBQUU7WUFDaEIsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsTUFBTTtZQUNkLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsY0FBYyxFQUFFLGVBQWU7WUFDL0IsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxNQUFNLEVBQUUsUUFBUSxDQUFDLFNBQVM7WUFFMUIsY0FBYyxFQUFFO2dCQUNkLFFBQVEsRUFBRSxLQUFLO2dCQUNmLE1BQU0sRUFBRSxNQUFNO2dCQUNkLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLGNBQWMsRUFBRSxlQUFlO2FBQ2hDO1NBQ0Y7UUFFRCxVQUFVLEVBQUU7WUFDVixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLFVBQVUsRUFBRSxRQUFRO1lBQ3BCLGNBQWMsRUFBRSxZQUFZO1lBQzVCLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO1lBQ3JDLFlBQVksRUFBRSxlQUFhLFFBQVEsQ0FBQyxhQUFlO1lBQ25ELGNBQWMsRUFBRSxZQUFZO1lBQzVCLG9CQUFvQixFQUFFLFlBQVk7WUFDbEMsTUFBTSxFQUFFLE1BQU07WUFDZCxLQUFLLEVBQUUsTUFBTTtZQUNiLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDcEMsR0FBRyxFQUFFLENBQUM7WUFDTixJQUFJLEVBQUUsQ0FBQztZQUNQLE1BQU0sRUFBRSxRQUFRLENBQUMsT0FBTztZQUV4QixLQUFLLEVBQUU7Z0JBQ0wsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSztnQkFDakMsR0FBRyxFQUFFLENBQUM7Z0JBQ04sSUFBSSxFQUFFLENBQUM7Z0JBQ1AsY0FBYyxFQUFFLFlBQVk7Z0JBQzVCLG9CQUFvQixFQUFFLFlBQVk7Z0JBQ2xDLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxFQUFFO2dCQUNWLGVBQWUsRUFBRSxRQUFRLENBQUMsWUFBWTthQUN2QztZQUVELGNBQWMsRUFBRSxZQUFZLENBQUM7Z0JBQzNCLE1BQU0sRUFBRSxDQUFDO3dCQUNQLFFBQVEsRUFBRSxFQUFFO3dCQUNaLFVBQVUsRUFBRSxNQUFNO3dCQUNsQixNQUFNLEVBQUUsTUFBTTt3QkFDZCxLQUFLLEVBQUUsTUFBTTt3QkFDYixJQUFJLEVBQUUsRUFBRTt3QkFDUixRQUFRLEVBQUUsUUFBUTt3QkFDbEIsV0FBVyxFQUFFLEVBQUU7d0JBQ2YsVUFBVSxFQUFFLFFBQVEsQ0FBQyxXQUFXO3dCQUNoQyxLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7d0JBQzFCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLFdBQVc7d0JBQ3JDLGFBQWEsRUFBRSxZQUFZO3dCQUMzQixNQUFNLEVBQUUsU0FBUztxQkFDbEIsQ0FBQztnQkFFRixPQUFPLEVBQUUsQ0FBQzt3QkFDUixRQUFRLEVBQUUsRUFBRTt3QkFDWixNQUFNLEVBQUUsRUFBRTt3QkFDVixVQUFVLEVBQUUsTUFBTTt3QkFDbEIsYUFBYSxFQUFFLFlBQVk7cUJBQzVCLENBQUM7Z0JBRUYsT0FBTyxFQUFFLENBQUM7d0JBQ1IsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO3dCQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjt3QkFDckMsTUFBTSxFQUFFLFNBQVM7cUJBQ2xCLENBQUM7YUFDSCxDQUFDO1lBRUYsSUFBSSxFQUFFLFVBQUMsU0FBUyxJQUFLLE9BQUEsQ0FBQztnQkFDcEIsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxZQUFZO2dCQUM1QixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtnQkFDckMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLGNBQWM7YUFDMUQsQ0FBQyxFQU5tQixDQU1uQjtZQUVGLEtBQUssRUFBRTtnQkFDTCxLQUFLLEVBQUUsRUFBRTtnQkFDVCxNQUFNLEVBQUUsRUFBRTthQUNYO1NBQ0Y7UUFFRCxPQUFPLEVBQUU7WUFDUCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1lBQ3BDLEtBQUssRUFBRSxPQUFPO1lBQ2QsTUFBTSxFQUFFLE9BQU87WUFDZixHQUFHLEVBQUUsRUFBRTtZQUNQLElBQUksRUFBRSxDQUFDO1lBQ1AsTUFBTSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1lBQ3hCLFVBQVUsRUFBRSxRQUFRLENBQUMsWUFBWTtZQUNqQyxVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtTQUN0QztLQUNGO0lBRUQsWUFBWSxFQUFFO1FBQ1osUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtRQUNwQyxVQUFVLEVBQUUsZUFBYSxRQUFRLENBQUMsYUFBZTtRQUVqRCxJQUFJLEVBQUU7WUFDSixLQUFLLEVBQUUsRUFBRTtZQUNULFFBQVEsRUFBRSxFQUFFO1lBQ1osTUFBTSxFQUFFLEVBQUU7WUFDVixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLFVBQVUsRUFBRSxRQUFRO1lBQ3BCLGNBQWMsRUFBRSxRQUFRO1lBQ3hCLFFBQVEsRUFBRSxFQUFFO1NBQ2I7UUFFRCxLQUFLLEVBQUU7WUFDTCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1lBQ3BDLEdBQUcsRUFBRSxFQUFFO1lBQ1AsS0FBSyxFQUFFLEVBQUU7WUFDVCxTQUFTLEVBQUUsZ0JBQWMsUUFBUSxDQUFDLGdCQUFrQjtZQUNwRCxXQUFXLEVBQUUsZ0JBQWMsUUFBUSxDQUFDLGdCQUFrQjtZQUN0RCxZQUFZLEVBQUUsZ0JBQWMsUUFBUSxDQUFDLE9BQVM7WUFDOUMsVUFBVSxFQUFFLGdCQUFjLFFBQVEsQ0FBQyxnQkFBa0I7U0FDdEQ7S0FDRjtJQUVELFlBQVksRUFBRSxZQUFZLENBQUM7UUFDekIsTUFBTSxFQUFFLENBQUM7Z0JBQ1AsTUFBTSxFQUFFLG9CQUFvQjtnQkFDNUIsU0FBUyxFQUFFLE1BQU07YUFDbEIsQ0FBQztRQUNGLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQztRQUNiLE9BQU8sRUFBRSxDQUFDO2dCQUNSLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLEdBQUcsRUFBRSxFQUFFO2dCQUNQLElBQUksRUFBRSxDQUFDO2dCQUNQLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxRQUFRLENBQUMsT0FBTztnQkFDeEIsZUFBZSxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUNwQyxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixPQUFPLEVBQUUsQ0FBQzthQUNYLENBQUM7S0FDSCxDQUFDO0lBRUYscUJBQXFCLEVBQUU7UUFDckIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7UUFDckMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztRQUMvQixPQUFPLEVBQUUsQ0FBQztRQUNWLE1BQU0sRUFBRSxRQUFRLENBQUMsU0FBUztLQUMzQjtJQUVELFlBQVksRUFBRTtRQUNaLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7UUFDL0IsS0FBSyxFQUFFLE1BQU07UUFDYixVQUFVLEVBQUUsRUFBRTtRQUNkLGFBQWEsRUFBRSxFQUFFO0tBQ2xCO0lBRUQsaUJBQWlCLEVBQUU7UUFDakIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUM5QixhQUFhLEVBQUUsUUFBUTtRQUN2QixVQUFVLEVBQUUsUUFBUTtRQUNwQixVQUFVLEVBQUUsRUFBRTtRQUNkLFlBQVksRUFBRSxFQUFFO1FBQ2hCLEtBQUssRUFBRSxNQUFNO1FBRWIsUUFBUSxFQUFFO1lBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztZQUMvQixRQUFRLEVBQUUsRUFBRTtZQUNaLFVBQVUsRUFBRSxNQUFNO1lBQ2xCLE1BQU0sRUFBRSxFQUFFO1lBQ1YsU0FBUyxFQUFFLEVBQUU7WUFDYixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtZQUNyQyxLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7U0FDM0I7UUFFRCxTQUFTLEVBQUU7WUFDVCxXQUFXLEVBQUUsRUFBRTtTQUNoQjtLQUNGO0lBRUQsYUFBYSxFQUFFO1FBQ2IsZ0JBQWdCLEVBQUU7WUFDaEIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxHQUFHLEVBQUUsRUFBRTtZQUNQLEtBQUssRUFBRSxDQUFDO1lBQ1IsZUFBZSxFQUFFLFFBQVEsQ0FBQyxVQUFVO1lBQ3BDLEtBQUssRUFBRSxHQUFHO1lBQ1YsbUJBQW1CO1lBQ25CLFNBQVMsRUFBRSxRQUFRO1lBQ25CLE1BQU0sRUFBRSxRQUFRLENBQUMsT0FBTztTQUN6QjtRQUVELE9BQU8sRUFBRTtZQUNQLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztZQUNqQyxNQUFNLEVBQUUsRUFBRTtZQUNWLFNBQVMsRUFBRSxFQUFFO1lBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixVQUFVLEVBQUUsUUFBUTtZQUNwQixjQUFjLEVBQUUsZUFBZTtZQUMvQixXQUFXLEVBQUUsRUFBRTtZQUNmLFlBQVksRUFBRSxFQUFFO1lBQ2hCLEdBQUcsRUFBRSxDQUFDO1lBQ04sTUFBTSxFQUFFLFFBQVEsQ0FBQyxTQUFTO1lBRTFCLEtBQUssRUFBRTtnQkFDTCxRQUFRLEVBQUUsRUFBRTtnQkFDWixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87Z0JBQ3ZCLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO2dCQUNyQyxVQUFVLEVBQUUsUUFBUTthQUNyQjtTQUNGO1FBRUQsV0FBVyxFQUFFO1lBQ1gsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsU0FBUztZQUNqQixVQUFVLEVBQUUsRUFBRTtZQUNkLFlBQVksRUFBRSxFQUFFO1lBQ2hCLGFBQWEsRUFBRSxFQUFFO1lBQ2pCLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLGVBQWEsUUFBUSxDQUFDLFlBQWM7WUFDbEQsTUFBTSxFQUFFLEVBQUU7WUFDVixTQUFTLEVBQUUsRUFBRTtZQUNiLFVBQVUsRUFBRSxRQUFRO1lBRXBCLElBQUksRUFBRTtnQkFDSixLQUFLLEVBQUUsRUFBRTtnQkFDVCxRQUFRLEVBQUUsRUFBRTtnQkFDWixNQUFNLEVBQUUsRUFBRTtnQkFDVixZQUFZLEVBQUUsS0FBSztnQkFDbkIsTUFBTSxFQUFFLGVBQWEsUUFBUSxDQUFDLE9BQVM7Z0JBQ3ZDLFdBQVcsRUFBRSxFQUFFO2dCQUNmLFVBQVUsRUFBRSxRQUFRLENBQUMsb0JBQW9CO2dCQUN6QyxRQUFRLEVBQUUsVUFBVTtnQkFDcEIsZUFBZSxFQUFFLFFBQVEsQ0FBQyxVQUFVO2FBQ3JDO1lBRUQsS0FBSyxFQUFFO2dCQUNMLFFBQVEsRUFBRSxFQUFFO2dCQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTtnQkFDNUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxlQUFlO2dCQUVwQyxRQUFRLEVBQUU7b0JBQ1IsS0FBSyxFQUFFLFFBQVEsQ0FBQyxTQUFTO2lCQUMxQjthQUNGO1NBQ0Y7UUFFRCxVQUFVLEVBQUU7WUFDVixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1lBQ3BDLEtBQUssRUFBRSxDQUFDO1lBRVIsSUFBSSxFQUFFLFlBQVksQ0FBQztnQkFDakIsTUFBTSxFQUFFLENBQUM7d0JBQ1AsS0FBSyxFQUFFLEVBQUU7d0JBQ1QsTUFBTSxFQUFFLEVBQUU7cUJBQ1gsQ0FBQztnQkFFRixPQUFPLEVBQUUsQ0FBQzt3QkFDUixLQUFLLEVBQUUsRUFBRTt3QkFDVCxNQUFNLEVBQUUsRUFBRTtxQkFDWCxDQUFDO2dCQUVGLE9BQU8sRUFBRSxDQUFDO3dCQUNSLEtBQUssRUFBRSxRQUFRLENBQUMsT0FBTzt3QkFDdkIsTUFBTSxFQUFFLFNBQVM7d0JBQ2pCLFNBQVMsRUFBRSxVQUFVO3dCQUNyQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjt3QkFDckMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtxQkFDckMsQ0FBQzthQUNILENBQUM7WUFFRixTQUFTLEVBQUU7Z0JBQ1QsS0FBSyxFQUFFLEVBQUU7YUFDVjtTQUNGO1FBRUQsV0FBVztnQkFDVCxTQUFTLEVBQUUsWUFBWSxDQUFDO29CQUN0QixNQUFNLEVBQUUsQ0FBQzs0QkFDUCxhQUFhLEVBQUUsQ0FBQzt5QkFDakIsQ0FBQztvQkFDRixPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUM7b0JBQ2IsT0FBTyxFQUFFLENBQUM7NEJBQ1IsVUFBVSxFQUFFLENBQUM7NEJBQ2IsU0FBUyxFQUFFLFFBQVE7NEJBQ25CLFNBQVMsRUFBRSxNQUFNO3lCQUNsQixDQUFDO2lCQUNILENBQUM7O1lBRUYsR0FBQyxhQUFhLENBQUMsVUFBVSxJQUFHO2dCQUMxQixXQUFXLEVBQUUsQ0FBQztnQkFDZCxZQUFZLEVBQUUsQ0FBQzthQUNoQjtlQUNGO0tBQ0Y7Q0FDSyxDQUFDIn0=
// CONCATENATED MODULE: ./components/community/header-mobile/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};









function handleRenderCategoryItem(item, index) {
    var linkProps = {
        key: "category-item-" + index,
        style: style.categoryContainer.category,
        to: item.slug,
        onClick: this.handleShowSubCategory
    };
    return (react["createElement"](react_router_dom["NavLink"], __assign({}, linkProps), item && item.name || ''));
}
;
var renderCategoryList = function (_a) {
    var showSubCategory = _a.showSubCategory, categoryList = _a.categoryList, handleShowSubCategory = _a.handleShowSubCategory;
    return (react["createElement"]("div", { style: [style.categoryList, showSubCategory && style.isShowingCategoryList], className: 'scroll-view' },
        react["createElement"]("div", { style: style.categoryWrap },
            react["createElement"]("div", { style: style.categoryContainer }, Array.isArray(categoryList) && categoryList.map(handleRenderCategoryItem, { handleShowSubCategory: handleShowSubCategory })))));
};
function getCategoryIdSelected(pathname) {
    if (pathname === routing["q" /* ROUTING_COMMUNITY_PATH */])
        return 1;
    if (pathname === routing["u" /* ROUTING_COMMUNITY_UNBOXING_PATH */])
        return 2;
    if (pathname === routing["m" /* ROUTING_COMMUNITY_BEST_DEALS_PATH */])
        return 3;
    if (pathname.indexOf(routing["t" /* ROUTING_COMMUNITY_TAG_PATH */]) === 0)
        return 4;
    if (pathname.indexOf(routing["w" /* ROUTING_COMMUNITY_USER_FEED_PATH */]) === 0 || pathname.indexOf(routing["q" /* ROUTING_COMMUNITY_PATH */]) === 0)
        return 5;
    return 1;
}
;
var renderHashTagList = function (_a) {
    var hashTagList = _a.hashTagList, handleShowFilter = _a.handleShowFilter;
    var closeIconProps = {
        name: 'close',
        style: style.mobileVersion.closeGroup.icon,
        innerStyle: style.mobileVersion.closeGroup.innerIcon,
        onClick: handleShowFilter
    };
    return (react["createElement"]("div", { style: style.mobileVersion.hashTagContainer },
        react["createElement"]("div", { className: 'sticky', style: style.mobileVersion.heading },
            react["createElement"]("div", { style: style.mobileVersion.heading.title }, "Trending Tags"),
            react["createElement"]("div", { style: style.mobileVersion.closeGroup },
                react["createElement"](icon["a" /* default */], __assign({}, closeIconProps)))),
        react["createElement"]("div", { style: [
                layout["a" /* flexContainer */].wrap,
                component["a" /* asideBlock */].content,
                style.mobileVersion.hashTagList.container
            ] }, hashTagList.map(function (hashTag, index) {
            return (react["createElement"](react_router_dom["NavLink"], { to: routing["t" /* ROUTING_COMMUNITY_TAG_PATH */] + "/" + hashTag.name, key: "filter-hashtag-" + index, onClick: handleShowFilter, style: Object.assign({}, layout["a" /* flexContainer */].left, style.mobileVersion.hashTagItem) },
                react["createElement"]("div", { key: "filter-hashtag-name-" + index, style: style.mobileVersion.hashTagItem.title }, Object(encode["f" /* decodeEntities */])(hashTag.name))));
        }))));
};
function renderComponent(_a) {
    var props = _a.props, state = _a.state, handleShowFilter = _a.handleShowFilter, handleShowSubCategory = _a.handleShowSubCategory;
    var _b = state, showFilter = _b.showFilter, showSubCategory = _b.showSubCategory, isSubCategoryOnTop = _b.isSubCategoryOnTop;
    var _c = props, pathname = _c.pathname, hashTagList = _c.hashTagList, hashtagSelected = _c.hashtagSelected;
    var headerStyle = style.headerMenuContainer;
    var menuIconProps = {
        name: 'angle-down',
        innerStyle: headerStyle.headerMenu.inner,
        style: headerStyle.headerMenu.icon(showSubCategory)
    };
    var categoryList = [
        {
            id: 1,
            name: community["a" /* COMMUNITY_CATEGORY_NAME */][1],
            slug: routing["q" /* ROUTING_COMMUNITY_PATH */]
        },
        {
            id: 2,
            name: community["a" /* COMMUNITY_CATEGORY_NAME */][2],
            slug: routing["u" /* ROUTING_COMMUNITY_UNBOXING_PATH */]
        },
        {
            id: 3,
            name: community["a" /* COMMUNITY_CATEGORY_NAME */][3],
            slug: routing["m" /* ROUTING_COMMUNITY_BEST_DEALS_PATH */]
        }
    ];
    var nameSelected = '';
    var categoryNoSelect = [];
    if (hashtagSelected && !!hashtagSelected.length) {
        categoryNoSelect = categoryList;
        nameSelected = hashtagSelected;
    }
    else {
        var categoryIdSelected_1 = getCategoryIdSelected(pathname);
        nameSelected = community["a" /* COMMUNITY_CATEGORY_NAME */][categoryIdSelected_1];
        categoryNoSelect = categoryList.filter(function (item) { return item.id !== categoryIdSelected_1; });
    }
    return (react["createElement"]("div", { style: headerStyle },
        react["createElement"]("div", { style: [headerStyle.headerMenu, isSubCategoryOnTop && headerStyle.headerMenu.isTop], id: 'community-mobile-menu' },
            react["createElement"]("div", { style: headerStyle.headerMenuParent },
                react["createElement"]("div", { style: headerStyle.headerMenuParent.headerMenuWrap, onClick: handleShowSubCategory },
                    react["createElement"]("div", { style: headerStyle.headerMenu.textBreadCrumb }, nameSelected),
                    react["createElement"](icon["a" /* default */], __assign({}, menuIconProps))),
                hashTagList
                    && !!hashTagList.length
                    && (react["createElement"]("div", { style: style.filterMobile },
                        react["createElement"]("div", { style: style.filterMobile.icon, onClick: handleShowFilter }, "#"),
                        showFilter && react["createElement"]("div", { style: style.filterMobile.angle }),
                        showFilter && renderHashTagList({ hashTagList: hashTagList, handleShowFilter: handleShowFilter })))),
            renderCategoryList({ categoryList: categoryNoSelect, showSubCategory: showSubCategory, handleShowSubCategory: handleShowSubCategory }),
            (showSubCategory || showFilter)
                && react["createElement"]("div", { style: headerStyle.overlay, onClick: showSubCategory ? handleShowSubCategory : handleShowFilter }))));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sS0FBSyxTQUFTLE1BQU0sMEJBQTBCLENBQUM7QUFDdEQsT0FBTyxLQUFLLE1BQU0sTUFBTSx1QkFBdUIsQ0FBQztBQUVoRCxPQUFPLElBQUksTUFBTSw2QkFBNkIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDdkQsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDbkYsT0FBTyxFQUNMLHNCQUFzQixFQUN0QiwwQkFBMEIsRUFDMUIsK0JBQStCLEVBQy9CLGdDQUFnQyxFQUNoQyxpQ0FBaUMsRUFDbEMsTUFBTSx3Q0FBd0MsQ0FBQztBQUVoRCxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFHNUIsa0NBQWtDLElBQUksRUFBRSxLQUFLO0lBQzNDLElBQU0sU0FBUyxHQUFHO1FBQ2hCLEdBQUcsRUFBRSxtQkFBaUIsS0FBTztRQUM3QixLQUFLLEVBQUUsS0FBSyxDQUFDLGlCQUFpQixDQUFDLFFBQVE7UUFDdkMsRUFBRSxFQUFFLElBQUksQ0FBQyxJQUFJO1FBQ2IsT0FBTyxFQUFFLElBQUksQ0FBQyxxQkFBcUI7S0FDcEMsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLG9CQUFDLE9BQU8sZUFBSyxTQUFTLEdBQ25CLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FDaEIsQ0FDWCxDQUFDO0FBQ0osQ0FBQztBQUFBLENBQUM7QUFFRixJQUFNLGtCQUFrQixHQUFHLFVBQUMsRUFBd0Q7UUFBdEQsb0NBQWUsRUFBRSw4QkFBWSxFQUFFLGdEQUFxQjtJQUFPLE9BQUEsQ0FDdkYsNkJBQUssS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFlBQVksRUFBRSxlQUFlLElBQUksS0FBSyxDQUFDLHFCQUFxQixDQUFDLEVBQUUsU0FBUyxFQUFFLGFBQWE7UUFDeEcsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxZQUFZO1lBQzVCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsaUJBQWlCLElBQ2hDLEtBQUssQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLElBQUksWUFBWSxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsRUFBRSxFQUFFLHFCQUFxQix1QkFBQSxFQUFFLENBQUMsQ0FDakcsQ0FDRixDQUNGLENBQ1A7QUFSd0YsQ0FReEYsQ0FBQztBQUVGLCtCQUErQixRQUFRO0lBQ3JDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsS0FBSyxzQkFBc0IsQ0FBQztRQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDbEQsRUFBRSxDQUFDLENBQUMsUUFBUSxLQUFLLCtCQUErQixDQUFDO1FBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUMzRCxFQUFFLENBQUMsQ0FBQyxRQUFRLEtBQUssaUNBQWlDLENBQUM7UUFBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO0lBQzdELEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsMEJBQTBCLENBQUMsS0FBSyxDQUFDLENBQUM7UUFBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO0lBQ2pFLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsZ0NBQWdDLENBQUMsS0FBSyxDQUFDLElBQUksUUFBUSxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDekgsTUFBTSxDQUFDLENBQUMsQ0FBQztBQUNYLENBQUM7QUFBQSxDQUFDO0FBRUYsSUFBTSxpQkFBaUIsR0FBRyxVQUFDLEVBQWlDO1FBQS9CLDRCQUFXLEVBQUUsc0NBQWdCO0lBRXhELElBQU0sY0FBYyxHQUFHO1FBQ3JCLElBQUksRUFBRSxPQUFPO1FBQ2IsS0FBSyxFQUFFLEtBQUssQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLElBQUk7UUFDMUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLFNBQVM7UUFDcEQsT0FBTyxFQUFFLGdCQUFnQjtLQUMxQixDQUFDO0lBR0YsTUFBTSxDQUFDLENBQ0wsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxhQUFhLENBQUMsZ0JBQWdCO1FBRTlDLDZCQUFLLFNBQVMsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxhQUFhLENBQUMsT0FBTztZQUMxRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsS0FBSyxvQkFBcUI7WUFDbEUsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxhQUFhLENBQUMsVUFBVTtnQkFDeEMsb0JBQUMsSUFBSSxlQUFLLGNBQWMsRUFBSSxDQUN4QixDQUNGO1FBR04sNkJBQ0UsS0FBSyxFQUFFO2dCQUNMLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSTtnQkFDekIsU0FBUyxDQUFDLFVBQVUsQ0FBQyxPQUFPO2dCQUM1QixLQUFLLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxTQUFTO2FBQzFDLElBRUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxVQUFDLE9BQU8sRUFBRSxLQUFLO1lBQzdCLE1BQU0sQ0FBQyxDQUNMLG9CQUFDLE9BQU8sSUFDTixFQUFFLEVBQUssMEJBQTBCLFNBQUksT0FBTyxDQUFDLElBQU0sRUFDbkQsR0FBRyxFQUFFLG9CQUFrQixLQUFPLEVBQzlCLE9BQU8sRUFBRSxnQkFBZ0IsRUFDekIsS0FBSyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDO2dCQUNwRiw2QkFDRSxHQUFHLEVBQUUseUJBQXVCLEtBQU8sRUFDbkMsS0FBSyxFQUFFLEtBQUssQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLEtBQUssSUFFM0MsY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FDekIsQ0FDRSxDQUNYLENBQUE7UUFDSCxDQUFDLENBQUMsQ0FFQSxDQUNGLENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLE1BQU0sMEJBQTBCLEVBQXlEO1FBQXZELGdCQUFLLEVBQUUsZ0JBQUssRUFBRSxzQ0FBZ0IsRUFBRSxnREFBcUI7SUFDL0UsSUFBQSxVQUlhLEVBSGpCLDBCQUFVLEVBQ1Ysb0NBQWUsRUFDZiwwQ0FBa0IsQ0FDQTtJQUVkLElBQUEsVUFJYSxFQUhqQixzQkFBUSxFQUNSLDRCQUFXLEVBQ1gsb0NBQWUsQ0FDRztJQUVwQixJQUFNLFdBQVcsR0FBRyxLQUFLLENBQUMsbUJBQW1CLENBQUM7SUFFOUMsSUFBTSxhQUFhLEdBQUc7UUFDcEIsSUFBSSxFQUFFLFlBQVk7UUFDbEIsVUFBVSxFQUFFLFdBQVcsQ0FBQyxVQUFVLENBQUMsS0FBSztRQUN4QyxLQUFLLEVBQUUsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDO0tBQ3BELENBQUM7SUFFRixJQUFNLFlBQVksR0FBRztRQUNuQjtZQUNFLEVBQUUsRUFBRSxDQUFDO1lBQ0wsSUFBSSxFQUFFLHVCQUF1QixDQUFDLENBQUMsQ0FBQztZQUNoQyxJQUFJLEVBQUUsc0JBQXNCO1NBQzdCO1FBQ0Q7WUFDRSxFQUFFLEVBQUUsQ0FBQztZQUNMLElBQUksRUFBRSx1QkFBdUIsQ0FBQyxDQUFDLENBQUM7WUFDaEMsSUFBSSxFQUFFLCtCQUErQjtTQUN0QztRQUNEO1lBQ0UsRUFBRSxFQUFFLENBQUM7WUFDTCxJQUFJLEVBQUUsdUJBQXVCLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLElBQUksRUFBRSxpQ0FBaUM7U0FDeEM7S0FDRixDQUFDO0lBRUYsSUFBSSxZQUFZLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLElBQUksZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO0lBRTFCLEVBQUUsQ0FBQyxDQUFDLGVBQWUsSUFBSSxDQUFDLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDaEQsZ0JBQWdCLEdBQUcsWUFBWSxDQUFDO1FBQ2hDLFlBQVksR0FBRyxlQUFlLENBQUM7SUFDakMsQ0FBQztJQUFDLElBQUksQ0FBQyxDQUFDO1FBQ04sSUFBTSxvQkFBa0IsR0FBRyxxQkFBcUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMzRCxZQUFZLEdBQUcsdUJBQXVCLENBQUMsb0JBQWtCLENBQUMsQ0FBQztRQUMzRCxnQkFBZ0IsR0FBRyxZQUFZLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLEVBQUUsS0FBSyxvQkFBa0IsRUFBOUIsQ0FBOEIsQ0FBQyxDQUFDO0lBQ2pGLENBQUM7SUFFRCxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsV0FBVztRQUNyQiw2QkFBSyxLQUFLLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLGtCQUFrQixJQUFJLFdBQVcsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxFQUFFLHVCQUF1QjtZQUNuSCw2QkFBSyxLQUFLLEVBQUUsV0FBVyxDQUFDLGdCQUFnQjtnQkFDdEMsNkJBQUssS0FBSyxFQUFFLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLEVBQUUsT0FBTyxFQUFFLHFCQUFxQjtvQkFDckYsNkJBQUssS0FBSyxFQUFFLFdBQVcsQ0FBQyxVQUFVLENBQUMsY0FBYyxJQUFHLFlBQVksQ0FBTztvQkFDdEUsb0JBQUMsSUFBSSxlQUFLLGFBQWEsRUFBSSxDQUN4QjtnQkFFSixXQUFXO3VCQUNSLENBQUMsQ0FBQyxXQUFXLENBQUMsTUFBTTt1QkFDcEIsQ0FDRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFlBQVk7d0JBQzVCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLFFBQVM7d0JBQ3RFLFVBQVUsSUFBSSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFlBQVksQ0FBQyxLQUFLLEdBQVE7d0JBQzFELFVBQVUsSUFBSSxpQkFBaUIsQ0FBQyxFQUFFLFdBQVcsYUFBQSxFQUFFLGdCQUFnQixrQkFBQSxFQUFFLENBQUMsQ0FDL0QsQ0FDUCxDQUVDO1lBQ0wsa0JBQWtCLENBQUMsRUFBRSxZQUFZLEVBQUUsZ0JBQWdCLEVBQUUsZUFBZSxpQkFBQSxFQUFFLHFCQUFxQix1QkFBQSxFQUFFLENBQUM7WUFFN0YsQ0FBQyxlQUFlLElBQUksVUFBVSxDQUFDO21CQUM1Qiw2QkFBSyxLQUFLLEVBQUUsV0FBVyxDQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUUsZUFBZSxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLEdBQVEsQ0FFN0csQ0FDRixDQUNQLENBQUE7QUFDSCxDQUFDO0FBQUEsQ0FBQyJ9
// CONCATENATED MODULE: ./components/community/header-mobile/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_HeaderMobile = /** @class */ (function (_super) {
    __extends(HeaderMobile, _super);
    function HeaderMobile(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    HeaderMobile.prototype.getScrollTop = function () {
        var el = document.scrollingElement || document.documentElement;
        return el.scrollTop;
    };
    HeaderMobile.prototype.handleScroll = function () {
        var _a = this.state, isSubCategoryOnTop = _a.isSubCategoryOnTop, heightSubCategoryToTop = _a.heightSubCategoryToTop;
        var eleInfo = this.getPositionElementById('community-mobile-menu');
        eleInfo
            && eleInfo.top <= 0
            && !isSubCategoryOnTop
            && this.setState({ isSubCategoryOnTop: true, heightSubCategoryToTop: window.scrollY });
        heightSubCategoryToTop >= window.scrollY
            && isSubCategoryOnTop
            && this.setState({ isSubCategoryOnTop: false });
    };
    HeaderMobile.prototype.getPositionElementById = function (elementId) {
        var el = document.getElementById(elementId);
        return el && el.getBoundingClientRect();
    };
    HeaderMobile.prototype.handleShowSubCategory = function () {
        var _a = this.state, showSubCategory = _a.showSubCategory, showFilter = _a.showFilter;
        this.setState({ showSubCategory: !showSubCategory });
        showFilter && this.setState({ showFilter: false });
    };
    HeaderMobile.prototype.handleShowFilter = function () {
        var _a = this.state, showSubCategory = _a.showSubCategory, showFilter = _a.showFilter;
        this.setState({ showFilter: !showFilter, showSubCategory: false });
        showSubCategory && this.setState({ showSubCategory: false });
    };
    HeaderMobile.prototype.componentDidMount = function () {
        window.addEventListener('scroll', this.handleScroll.bind(this));
    };
    HeaderMobile.prototype.componentWillUnmount = function () {
        window.addEventListener('scroll', function () { });
    };
    HeaderMobile.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state,
            handleShowFilter: this.handleShowFilter.bind(this),
            handleShowSubCategory: this.handleShowSubCategory.bind(this)
        };
        return renderComponent(args);
    };
    HeaderMobile.defaultProps = DEFAULT_PROPS;
    HeaderMobile = __decorate([
        radium
    ], HeaderMobile);
    return HeaderMobile;
}(react["Component"]));
;
/* harmony default export */ var header_mobile_component = (component_HeaderMobile);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFFNUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUd6QztJQUEyQixnQ0FBK0I7SUFHeEQsc0JBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUF1QixDQUFDOztJQUN2QyxDQUFDO0lBRUQsbUNBQVksR0FBWjtRQUNFLElBQU0sRUFBRSxHQUFHLFFBQVEsQ0FBQyxnQkFBZ0IsSUFBSSxRQUFRLENBQUMsZUFBZSxDQUFDO1FBQ2pFLE1BQU0sQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFBO0lBQ3JCLENBQUM7SUFFRCxtQ0FBWSxHQUFaO1FBQ1EsSUFBQSxlQUFxRSxFQUFuRSwwQ0FBa0IsRUFBRSxrREFBc0IsQ0FBMEI7UUFFNUUsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixDQUFDLENBQUM7UUFFbkUsT0FBTztlQUNGLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQztlQUNoQixDQUFDLGtCQUFrQjtlQUNuQixJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsa0JBQWtCLEVBQUUsSUFBSSxFQUFFLHNCQUFzQixFQUFFLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1FBRXpGLHNCQUFzQixJQUFJLE1BQU0sQ0FBQyxPQUFPO2VBQ25DLGtCQUFrQjtlQUNsQixJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsa0JBQWtCLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsNkNBQXNCLEdBQXRCLFVBQXVCLFNBQVM7UUFDOUIsSUFBTSxFQUFFLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM5QyxNQUFNLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO0lBQzFDLENBQUM7SUFFRCw0Q0FBcUIsR0FBckI7UUFDUSxJQUFBLGVBQTRDLEVBQTFDLG9DQUFlLEVBQUUsMEJBQVUsQ0FBZ0I7UUFDbkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLGVBQWUsRUFBRSxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7UUFDckQsVUFBVSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQsdUNBQWdCLEdBQWhCO1FBQ1EsSUFBQSxlQUE0QyxFQUExQyxvQ0FBZSxFQUFFLDBCQUFVLENBQWdCO1FBQ25ELElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxVQUFVLEVBQUUsQ0FBQyxVQUFVLEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7UUFDbkUsZUFBZSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBRUQsd0NBQWlCLEdBQWpCO1FBQ0UsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7SUFFRCwyQ0FBb0IsR0FBcEI7UUFDRSxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLGNBQVEsQ0FBQyxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVELDZCQUFNLEdBQU47UUFDRSxJQUFNLElBQUksR0FBRztZQUNYLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDbEQscUJBQXFCLEVBQUUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDN0QsQ0FBQTtRQUVELE1BQU0sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQTdETSx5QkFBWSxHQUFXLGFBQWEsQ0FBQztJQUR4QyxZQUFZO1FBRGpCLE1BQU07T0FDRCxZQUFZLENBK0RqQjtJQUFELG1CQUFDO0NBQUEsQUEvREQsQ0FBMkIsS0FBSyxDQUFDLFNBQVMsR0ErRHpDO0FBQUEsQ0FBQztBQUVGLGVBQWUsWUFBWSxDQUFDIn0=
// CONCATENATED MODULE: ./components/community/header-mobile/index.tsx

/* harmony default export */ var header_mobile = __webpack_exports__["a"] = (header_mobile_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxTQUFTLE1BQU0sYUFBYSxDQUFDO0FBQ3BDLGVBQWUsU0FBUyxDQUFDIn0=

/***/ }),

/***/ 814:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./action/activity-feed.ts + 1 modules
var activity_feed = __webpack_require__(761);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// CONCATENATED MODULE: ./components/community/collection-detail/initialize.tsx
var DEFAULT_PROPS = {
    limit: 10,
    collectionDetailId: 0
};
var INITIAL_STATE = {
    isLoading: false,
    feedActiveId: -1
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixLQUFLLEVBQUUsRUFBRTtJQUNULGtCQUFrQixFQUFFLENBQUM7Q0FDWixDQUFDO0FBRVosTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHO0lBQzNCLFNBQVMsRUFBRSxLQUFLO0lBQ2hCLFlBQVksRUFBRSxDQUFDLENBQUM7Q0FDUCxDQUFDIn0=
// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// EXTERNAL MODULE: ./components/container/feed-item/index.tsx + 5 modules
var feed_item = __webpack_require__(772);

// EXTERNAL MODULE: ./components/ui/loading/index.tsx + 4 modules
var loading = __webpack_require__(747);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./components/community/collection-detail/style.tsx


/* harmony default export */ var style = ({
    display: 'block',
    position: 'relative',
    container: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                paddingTop: 10,
                paddingLeft: 10,
                paddingRight: 10
            }],
        DESKTOP: [{}],
        GENERAL: [{}]
    }),
    wrapLayout: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 20,
        paddingLeft: 0
    },
    item: {
        display: variable["display"].flex,
        position: variable["position"].relative,
        boxShadow: variable["shadowBlur"],
        borderRadius: 5,
        overflow: 'hidden',
        marginBottom: 20,
        itemWithLine: {
            borderTop: "1px solid " + variable["colorF0"]
        },
        cover: {
            icon: {
                position: variable["position"].absolute,
                bottom: 0,
                right: -45,
                opacity: .25,
                width: 110,
                height: 110,
                color: variable["randomColorList"](-1)
            },
            innerIcon: {
                height: 130
            }
        },
        content: {
            padding: 20,
            title: {
                fontFamily: variable["fontTrirong"],
                fontSize: 18,
                lineHeight: '24px',
                fontWeight: 600,
                color: variable["colorBlack"]
            },
            description: {
                fontSize: 14,
                lineHeight: '22px',
                marginBottom: 10
            },
            link: {
                display: variable["display"].inlineFlex,
                justifyContent: 'center',
                alignItems: 'center',
                height: 26,
                lineHeight: 26,
                background: variable["colorF7"],
                border: "1px solid " + variable["colorF0"],
                color: variable["colorPink"],
                fontFamily: variable["fontAvenirMedium"],
                borderRadius: 2,
                paddingLeft: 13,
                icon: {
                    width: 26,
                    height: 26,
                    color: variable["colorPink"]
                },
                innerIcon: {
                    height: 10
                }
            },
        }
    },
    feedList: {
        boxShadow: variable["shadowBlurSort"],
        borderRadius: 5,
        overflow: 'hidden',
        marginBottom: 10,
    },
    feedItem: {
        heading: {
            display: variable["display"].flex,
            justifyContent: 'space-between',
            cursor: 'pointer',
            padding: 20,
            borderBottom: "1px solid " + variable["colorF0"],
            avatar: {
                width: 40,
                minWidth: 40,
                height: 40,
                borderRadius: '50%',
            },
            infoUser: {
                padding: '0 40px 0 10px',
                name: {
                    fontSize: 14,
                    lineHeight: '20px',
                    height: 20,
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    fontFamily: variable["fontAvenirDemiBold"],
                },
                message: {
                    fontSize: 14,
                    color: variable["color4D"],
                    lineHeight: '20px',
                    height: 20,
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                }
            },
            icon: {
                minWidth: 40,
                width: 40,
                height: 40,
                color: variable["color2E"],
                marginRight: -10
            },
            innerIcon: {
                height: 8
            },
        },
        content: {}
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFekQsZUFBZTtJQUNiLE9BQU8sRUFBRSxPQUFPO0lBQ2hCLFFBQVEsRUFBRSxVQUFVO0lBRXBCLFNBQVMsRUFBRSxZQUFZLENBQUM7UUFDdEIsTUFBTSxFQUFFLENBQUM7Z0JBQ1AsVUFBVSxFQUFFLEVBQUU7Z0JBQ2QsV0FBVyxFQUFFLEVBQUU7Z0JBQ2YsWUFBWSxFQUFFLEVBQUU7YUFDakIsQ0FBQztRQUNGLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQztRQUNiLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQztLQUNkLENBQUM7SUFFRixVQUFVLEVBQUU7UUFDVixVQUFVLEVBQUUsQ0FBQztRQUNiLFlBQVksRUFBRSxDQUFDO1FBQ2YsYUFBYSxFQUFFLEVBQUU7UUFDakIsV0FBVyxFQUFFLENBQUM7S0FDZjtJQUVELElBQUksRUFBRTtRQUNKLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7UUFDOUIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtRQUNwQyxTQUFTLEVBQUUsUUFBUSxDQUFDLFVBQVU7UUFDOUIsWUFBWSxFQUFFLENBQUM7UUFDZixRQUFRLEVBQUUsUUFBUTtRQUNsQixZQUFZLEVBQUUsRUFBRTtRQUVoQixZQUFZLEVBQUU7WUFDWixTQUFTLEVBQUUsZUFBYSxRQUFRLENBQUMsT0FBUztTQUMzQztRQUVELEtBQUssRUFBRTtZQUNMLElBQUksRUFBRTtnQkFDSixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxNQUFNLEVBQUUsQ0FBQztnQkFDVCxLQUFLLEVBQUUsQ0FBQyxFQUFFO2dCQUNWLE9BQU8sRUFBRSxHQUFHO2dCQUNaLEtBQUssRUFBRSxHQUFHO2dCQUNWLE1BQU0sRUFBRSxHQUFHO2dCQUNYLEtBQUssRUFBRSxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3BDO1lBRUQsU0FBUyxFQUFFO2dCQUNULE1BQU0sRUFBRSxHQUFHO2FBQ1o7U0FDRjtRQUVELE9BQU8sRUFBRTtZQUNQLE9BQU8sRUFBRSxFQUFFO1lBRVgsS0FBSyxFQUFFO2dCQUNMLFVBQVUsRUFBRSxRQUFRLENBQUMsV0FBVztnQkFDaEMsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osVUFBVSxFQUFFLE1BQU07Z0JBQ2xCLFVBQVUsRUFBRSxHQUFHO2dCQUNmLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTthQUMzQjtZQUVELFdBQVcsRUFBRTtnQkFDWCxRQUFRLEVBQUUsRUFBRTtnQkFDWixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsWUFBWSxFQUFFLEVBQUU7YUFDakI7WUFFRCxJQUFJLEVBQUU7Z0JBQ0osT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBVTtnQkFDcEMsY0FBYyxFQUFFLFFBQVE7Z0JBQ3hCLFVBQVUsRUFBRSxRQUFRO2dCQUNwQixNQUFNLEVBQUUsRUFBRTtnQkFDVixVQUFVLEVBQUUsRUFBRTtnQkFDZCxVQUFVLEVBQUUsUUFBUSxDQUFDLE9BQU87Z0JBQzVCLE1BQU0sRUFBRSxlQUFhLFFBQVEsQ0FBQyxPQUFTO2dCQUN2QyxLQUFLLEVBQUUsUUFBUSxDQUFDLFNBQVM7Z0JBQ3pCLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO2dCQUNyQyxZQUFZLEVBQUUsQ0FBQztnQkFDZixXQUFXLEVBQUUsRUFBRTtnQkFFZixJQUFJLEVBQUU7b0JBQ0osS0FBSyxFQUFFLEVBQUU7b0JBQ1QsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxTQUFTO2lCQUMxQjtnQkFFRCxTQUFTLEVBQUU7b0JBQ1QsTUFBTSxFQUFFLEVBQUU7aUJBQ1g7YUFDRjtTQUNGO0tBQ0Y7SUFFRCxRQUFRLEVBQUU7UUFDUixTQUFTLEVBQUUsUUFBUSxDQUFDLGNBQWM7UUFDbEMsWUFBWSxFQUFFLENBQUM7UUFDZixRQUFRLEVBQUUsUUFBUTtRQUNsQixZQUFZLEVBQUUsRUFBRTtLQUNqQjtJQUVELFFBQVEsRUFBRTtRQUNSLE9BQU8sRUFBRTtZQUNQLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsY0FBYyxFQUFFLGVBQWU7WUFDL0IsTUFBTSxFQUFFLFNBQVM7WUFDakIsT0FBTyxFQUFFLEVBQUU7WUFDWCxZQUFZLEVBQUUsZUFBYSxRQUFRLENBQUMsT0FBUztZQUU3QyxNQUFNLEVBQUU7Z0JBQ04sS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsWUFBWSxFQUFFLEtBQUs7YUFDcEI7WUFFRCxRQUFRLEVBQUU7Z0JBQ1IsT0FBTyxFQUFFLGVBQWU7Z0JBRXhCLElBQUksRUFBRTtvQkFDSixRQUFRLEVBQUUsRUFBRTtvQkFDWixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLFlBQVksRUFBRSxVQUFVO29CQUN4QixVQUFVLEVBQUUsUUFBUSxDQUFDLGtCQUFrQjtpQkFDeEM7Z0JBRUQsT0FBTyxFQUFFO29CQUNQLFFBQVEsRUFBRSxFQUFFO29CQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsT0FBTztvQkFDdkIsVUFBVSxFQUFFLE1BQU07b0JBQ2xCLE1BQU0sRUFBRSxFQUFFO29CQUNWLFFBQVEsRUFBRSxRQUFRO29CQUNsQixZQUFZLEVBQUUsVUFBVTtpQkFDekI7YUFDRjtZQUVELElBQUksRUFBRTtnQkFDSixRQUFRLEVBQUUsRUFBRTtnQkFDWixLQUFLLEVBQUUsRUFBRTtnQkFDVCxNQUFNLEVBQUUsRUFBRTtnQkFDVixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87Z0JBQ3ZCLFdBQVcsRUFBRSxDQUFDLEVBQUU7YUFDakI7WUFFRCxTQUFTLEVBQUU7Z0JBQ1QsTUFBTSxFQUFFLENBQUM7YUFDVjtTQUNGO1FBRUQsT0FBTyxFQUFFLEVBQUU7S0FDWjtDQUNLLENBQUMifQ==
// CONCATENATED MODULE: ./components/community/collection-detail/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};





var renderLoadingPlaceholder = function () { return (react["createElement"]("div", { style: style.placeholder.container }, [1, 2, 3].map(function (item) { return (react["createElement"]("div", { style: style.placeholder.item, key: item },
    react["createElement"]("div", { style: style.placeholder.item.top },
        react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.top.avatar }),
        react["createElement"]("div", { style: style.placeholder.item.top.info },
            react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.top.username }),
            react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.top.star }))),
    react["createElement"]("div", { style: style.placeholder.item.bottom },
        react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.bottom.firstText }),
        react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.bottom.text }),
        react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.bottom.text }),
        react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.bottom.lastText })))); }))); };
var renderContent = function (item) { return (react["createElement"]("div", { style: style.item.content },
    react["createElement"]("div", { style: style.item.content.title }, "T\u1ED5ng h\u1EE3p Feed \u0111\u01B0\u1EE3c y\u00EAu th\u00EDch nh\u1EA5t tu\u1EA7n qua"))); };
var renderCollectionItem = function (item) {
    var iconProps = {
        name: 'star-light',
        style: style.item.cover.icon,
        innerStyle: style.item.cover.innerIcon,
    };
    return (react["createElement"]("div", { style: style.item },
        react["createElement"](icon["a" /* default */], __assign({}, iconProps)),
        renderContent(item)));
};
function renderFeedItem(item, $index) {
    var _this = this;
    var iconProps = {
        name: 'angle-down',
        style: style.feedItem.heading.icon,
        innerStyle: style.feedItem.heading.innerIcon,
    };
    var feedItemProps = {
        history: history,
        item: item,
        userProfile: this.userProfile
    };
    return (react["createElement"]("div", { key: "feed-item-" + $index, style: style.feedItem },
        item.id !== this.feedActiveId
            && (react["createElement"]("div", { style: style.feedItem.heading, onClick: function () { return _this.openFeedDetail(item.id); } },
                react["createElement"]("img", { src: item.user.avatar.medium_url, style: style.feedItem.heading.avatar }),
                react["createElement"]("div", { style: style.feedItem.heading.infoUser },
                    react["createElement"]("div", { style: style.feedItem.heading.infoUser.name }, item.user.name),
                    react["createElement"]("div", { style: style.feedItem.heading.infoUser.message }, item.message)),
                react["createElement"](icon["a" /* default */], __assign({}, iconProps)))),
        item.id === this.feedActiveId && react["createElement"](feed_item["a" /* default */], __assign({}, feedItemProps))));
}
var renderFeedList = function (_a) {
    var collectionDetail = _a.collectionDetail, userProfile = _a.userProfile, feedActiveId = _a.feedActiveId, openFeedDetail = _a.openFeedDetail;
    var list = collectionDetail
        && Array.isArray(collectionDetail.feeds)
        && !!collectionDetail.feeds.length
        && collectionDetail.feeds.slice(0, 6) || [];
    return (react["createElement"]("div", { style: style.feedList }, list.map(renderFeedItem, { feedActiveId: feedActiveId, userProfile: userProfile, openFeedDetail: openFeedDetail })));
};
var renderMainContainer = function (_a) {
    var collectionDetail = _a.collectionDetail, userProfile = _a.userProfile, feedActiveId = _a.feedActiveId, openFeedDetail = _a.openFeedDetail;
    if (!collectionDetail) {
        return null;
    }
    return (react["createElement"]("div", { style: style },
        renderCollectionItem(collectionDetail.collection),
        renderFeedList({ collectionDetail: collectionDetail, userProfile: userProfile, feedActiveId: feedActiveId, openFeedDetail: openFeedDetail })));
};
var renderView = function (_a) {
    var props = _a.props, state = _a.state, openFeedDetail = _a.openFeedDetail;
    var _b = props, collectionDetailId = _b.collectionDetailId, userProfile = _b.userStore.userProfile, detail = _b.activityFeedStore.collection.detail;
    var _c = state, feedActiveId = _c.feedActiveId, isLoading = _c.isLoading;
    var collectionDetail = detail && detail[collectionDetailId];
    return (react["createElement"]("div", { style: style.container }, isLoading
        ? react["createElement"](loading["a" /* default */], null)
        : renderMainContainer({ collectionDetail: collectionDetail, userProfile: userProfile, feedActiveId: feedActiveId, openFeedDetail: openFeedDetail })));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxJQUFJLE1BQU0sNkJBQTZCLENBQUM7QUFDL0MsT0FBTyxRQUFRLE1BQU0seUNBQXlDLENBQUM7QUFDL0QsT0FBTyxPQUFPLE1BQU0sa0JBQWtCLENBQUM7QUFFdkMsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRzVCLE1BQU0sQ0FBQyxJQUFNLHdCQUF3QixHQUFHLGNBQU0sT0FBQSxDQUM1Qyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxTQUFTLElBRW5DLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxDQUNwQiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsR0FBRyxFQUFFLElBQUk7SUFDM0MsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUc7UUFDcEMsNkJBQUssU0FBUyxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBUTtRQUMxRSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUk7WUFDekMsNkJBQUssU0FBUyxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsR0FBUTtZQUM1RSw2QkFBSyxTQUFTLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxHQUFRLENBQ3BFLENBQ0Y7SUFDTiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTTtRQUN2Qyw2QkFBSyxTQUFTLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxHQUFRO1FBQ2hGLDZCQUFLLFNBQVMsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEdBQVE7UUFDM0UsNkJBQUssU0FBUyxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksR0FBUTtRQUMzRSw2QkFBSyxTQUFTLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxHQUFRLENBQzNFLENBQ0YsQ0FDUCxFQWhCcUIsQ0FnQnJCLENBQUMsQ0FFQSxDQUNQLEVBdEI2QyxDQXNCN0MsQ0FBQztBQUVGLElBQU0sYUFBYSxHQUFHLFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FDOUIsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTztJQUM1Qiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyw4RkFBa0QsQ0FFbEYsQ0FDUCxFQUwrQixDQUsvQixDQUFDO0FBRUYsSUFBTSxvQkFBb0IsR0FBRyxVQUFDLElBQUk7SUFDaEMsSUFBTSxTQUFTLEdBQUc7UUFDaEIsSUFBSSxFQUFFLFlBQVk7UUFDbEIsS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUk7UUFDNUIsVUFBVSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVM7S0FDdkMsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSTtRQUNwQixvQkFBQyxJQUFJLGVBQUssU0FBUyxFQUFJO1FBQ3RCLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FDaEIsQ0FDUCxDQUFDO0FBQ0osQ0FBQyxDQUFBO0FBRUQsd0JBQXdCLElBQUksRUFBRSxNQUFNO0lBQXBDLGlCQWdDQztJQS9CQyxJQUFNLFNBQVMsR0FBRztRQUNoQixJQUFJLEVBQUUsWUFBWTtRQUNsQixLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUNsQyxVQUFVLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsU0FBUztLQUM3QyxDQUFDO0lBRUYsSUFBTSxhQUFhLEdBQUc7UUFDcEIsT0FBTyxTQUFBO1FBQ1AsSUFBSSxNQUFBO1FBQ0osV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXO0tBQzlCLENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCw2QkFBSyxHQUFHLEVBQUUsZUFBYSxNQUFRLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxRQUFRO1FBRWxELElBQUksQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLFlBQVk7ZUFDMUIsQ0FDRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLGNBQU0sT0FBQSxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBNUIsQ0FBNEI7Z0JBQzdFLDZCQUFLLEdBQUcsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBSTtnQkFDL0UsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFFBQVE7b0JBQ3pDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxJQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFPO29CQUN4RSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLE9BQU8sSUFBRyxJQUFJLENBQUMsT0FBTyxDQUFPLENBQ3JFO2dCQUNOLG9CQUFDLElBQUksZUFBSyxTQUFTLEVBQUksQ0FDbkIsQ0FDUDtRQUdGLElBQUksQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLFlBQVksSUFBSSxvQkFBQyxRQUFRLGVBQUssYUFBYSxFQUFhLENBQ3RFLENBQ1AsQ0FBQztBQUNKLENBQUM7QUFFRCxJQUFNLGNBQWMsR0FBRyxVQUFDLEVBQStEO1FBQTdELHNDQUFnQixFQUFFLDRCQUFXLEVBQUUsOEJBQVksRUFBRSxrQ0FBYztJQUNuRixJQUFNLElBQUksR0FBRyxnQkFBZ0I7V0FDeEIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUM7V0FDckMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxNQUFNO1dBQy9CLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUU5QyxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsSUFDdkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsRUFBRSxZQUFZLGNBQUEsRUFBRSxXQUFXLGFBQUEsRUFBRSxjQUFjLGdCQUFBLEVBQUUsQ0FBQyxDQUNwRSxDQUNQLENBQUE7QUFDSCxDQUFDLENBQUE7QUFFRCxJQUFNLG1CQUFtQixHQUFHLFVBQUMsRUFBK0Q7UUFBN0Qsc0NBQWdCLEVBQUUsNEJBQVcsRUFBRSw4QkFBWSxFQUFFLGtDQUFjO0lBQ3hGLEVBQUUsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1FBQUMsTUFBTSxDQUFDLElBQUksQ0FBQTtJQUFDLENBQUM7SUFFdEMsTUFBTSxDQUFDLENBQ0wsNkJBQUssS0FBSyxFQUFFLEtBQUs7UUFDZCxvQkFBb0IsQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUM7UUFDakQsY0FBYyxDQUFDLEVBQUUsZ0JBQWdCLGtCQUFBLEVBQUUsV0FBVyxhQUFBLEVBQUUsWUFBWSxjQUFBLEVBQUUsY0FBYyxnQkFBQSxFQUFFLENBQUMsQ0FDNUUsQ0FDUCxDQUFBO0FBQ0gsQ0FBQyxDQUFBO0FBRUQsSUFBTSxVQUFVLEdBQUcsVUFBQyxFQUFnQztRQUE5QixnQkFBSyxFQUFFLGdCQUFLLEVBQUUsa0NBQWM7SUFDMUMsSUFBQSxVQUlhLEVBSGpCLDBDQUFrQixFQUNMLHNDQUFXLEVBQ1csK0NBQU0sQ0FDdkI7SUFFZCxJQUFBLFVBQTZDLEVBQTNDLDhCQUFZLEVBQUUsd0JBQVMsQ0FBcUI7SUFFcEQsSUFBTSxnQkFBZ0IsR0FBRyxNQUFNLElBQUksTUFBTSxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFFOUQsTUFBTSxDQUFDLENBQ0wsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLElBRXZCLFNBQVM7UUFDUCxDQUFDLENBQUMsb0JBQUMsT0FBTyxPQUFHO1FBQ2IsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLEVBQUUsZ0JBQWdCLGtCQUFBLEVBQUUsV0FBVyxhQUFBLEVBQUUsWUFBWSxjQUFBLEVBQUUsY0FBYyxnQkFBQSxFQUFFLENBQUMsQ0FFdEYsQ0FDUCxDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// CONCATENATED MODULE: ./components/community/collection-detail/container.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var container_CollectionDetailContainer = /** @class */ (function (_super) {
    __extends(CollectionDetailContainer, _super);
    function CollectionDetailContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    CollectionDetailContainer.prototype.componentWillMount = function () {
        this.init(this.props);
    };
    CollectionDetailContainer.prototype.init = function (props) {
        if (props === void 0) { props = this.props; }
        var _a = props, collectionDetailId = _a.collectionDetailId, getCollectionDetailAction = _a.getCollectionDetailAction;
        !!collectionDetailId && this.setState({ isLoading: true }, getCollectionDetailAction({ id: collectionDetailId }));
    };
    CollectionDetailContainer.prototype.openFeedDetail = function (feedId) {
        this.setState({ feedActiveId: feedId });
    };
    CollectionDetailContainer.prototype.componentWillReceiveProps = function (nextProps) {
        var isLoading = this.props.activityFeedStore.collection.isLoading;
        var collection = nextProps.activityFeedStore.collection;
        isLoading
            && collection
            && !collection.isLoading
            && this.setState({ isLoading: false });
    };
    CollectionDetailContainer.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state,
            openFeedDetail: this.openFeedDetail.bind(this)
        };
        return view(args);
    };
    CollectionDetailContainer.defaultProps = DEFAULT_PROPS;
    CollectionDetailContainer = __decorate([
        radium
    ], CollectionDetailContainer);
    return CollectionDetailContainer;
}(react["Component"]));
;
/* harmony default export */ var container = (container_CollectionDetailContainer);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFFNUQsT0FBTyxVQUFVLE1BQU0sUUFBUSxDQUFDO0FBR2hDO0lBQXdDLDZDQUErQjtJQUdyRSxtQ0FBWSxLQUFhO1FBQXpCLFlBQ0Usa0JBQU0sS0FBSyxDQUFDLFNBRWI7UUFEQyxLQUFJLENBQUMsS0FBSyxHQUFHLGFBQXVCLENBQUM7O0lBQ3ZDLENBQUM7SUFFRCxzREFBa0IsR0FBbEI7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN4QixDQUFDO0lBRUQsd0NBQUksR0FBSixVQUFLLEtBQWtCO1FBQWxCLHNCQUFBLEVBQUEsUUFBUSxJQUFJLENBQUMsS0FBSztRQUNmLElBQUEsVUFHYSxFQUZqQiwwQ0FBa0IsRUFDbEIsd0RBQXlCLENBQ1A7UUFFcEIsQ0FBQyxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLEVBQUUseUJBQXlCLENBQUMsRUFBRSxFQUFFLEVBQUUsa0JBQWtCLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDcEgsQ0FBQztJQUVELGtEQUFjLEdBQWQsVUFBZSxNQUFNO1FBQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQTtJQUN6QyxDQUFDO0lBRUQsNkRBQXlCLEdBQXpCLFVBQTBCLFNBQVM7UUFDVSxJQUFBLDZEQUFTLENBQThCO1FBQ3JELElBQUEsbURBQVUsQ0FBMkI7UUFFbEUsU0FBUztlQUNKLFVBQVU7ZUFDVixDQUFDLFVBQVUsQ0FBQyxTQUFTO2VBQ3JCLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRUQsMENBQU0sR0FBTjtRQUNFLElBQU0sSUFBSSxHQUFHO1lBQ1gsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixjQUFjLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQy9DLENBQUM7UUFFRixNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUExQ00sc0NBQVksR0FBVyxhQUFhLENBQUM7SUFEeEMseUJBQXlCO1FBRDlCLE1BQU07T0FDRCx5QkFBeUIsQ0E0QzlCO0lBQUQsZ0NBQUM7Q0FBQSxBQTVDRCxDQUF3QyxLQUFLLENBQUMsU0FBUyxHQTRDdEQ7QUFBQSxDQUFDO0FBRUYsZUFBZSx5QkFBeUIsQ0FBQyJ9
// CONCATENATED MODULE: ./components/community/collection-detail/store.tsx
var connect = __webpack_require__(129).connect;


var mapStateToProps = function (state) { return ({
    activityFeedStore: state.activityFeed,
    userStore: state.user,
    authStore: state.auth
}); };
var mapDispatchToProps = function (dispatch) { return ({
    getCollectionDetailAction: function (data) { return dispatch(Object(activity_feed["h" /* getCollectionDetailAction */])(data)); },
}); };
/* harmony default export */ var store = (connect(mapStateToProps, mapDispatchToProps)(container));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUUxRSxPQUFPLHlCQUF5QixNQUFNLGFBQWEsQ0FBQztBQUVwRCxNQUFNLENBQUMsSUFBTSxlQUFlLEdBQUcsVUFBQyxLQUFLLElBQUssT0FBQSxDQUFDO0lBQ3pDLGlCQUFpQixFQUFFLEtBQUssQ0FBQyxZQUFZO0lBQ3JDLFNBQVMsRUFBRSxLQUFLLENBQUMsSUFBSTtJQUNyQixTQUFTLEVBQUUsS0FBSyxDQUFDLElBQUk7Q0FDdEIsQ0FBQyxFQUp3QyxDQUl4QyxDQUFDO0FBRUgsTUFBTSxDQUFDLElBQU0sa0JBQWtCLEdBQUcsVUFBQyxRQUFRLElBQUssT0FBQSxDQUFDO0lBQy9DLHlCQUF5QixFQUFFLFVBQUMsSUFBUyxJQUFLLE9BQUEsUUFBUSxDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxDQUFDLEVBQXpDLENBQXlDO0NBQ3BGLENBQUMsRUFGOEMsQ0FFOUMsQ0FBQztBQUVILGVBQWUsT0FBTyxDQUNwQixlQUFlLEVBQ2Ysa0JBQWtCLENBQ25CLENBQUMseUJBQXlCLENBQUMsQ0FBQyJ9
// CONCATENATED MODULE: ./components/community/collection-detail/index.tsx

/* harmony default export */ var collection_detail = __webpack_exports__["a"] = (store);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxTQUFTLE1BQU0sU0FBUyxDQUFDO0FBQ2hDLGVBQWUsU0FBUyxDQUFDIn0=

/***/ }),

/***/ 899:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./action/meta.ts
var meta = __webpack_require__(754);

// EXTERNAL MODULE: ./action/community.ts + 1 modules
var community = __webpack_require__(792);

// EXTERNAL MODULE: ./action/activity-feed.ts + 1 modules
var activity_feed = __webpack_require__(761);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// CONCATENATED MODULE: ./container/app-shop/community/new-feed/initialize.tsx
var DEFAULT_PROPS = {
    limit: 10
};
var INITIAL_STATE = {
    showCommunityHashTag: false,
    isLoading: false,
    isFeebackFull: false
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixLQUFLLEVBQUUsRUFBRTtDQUNPLENBQUM7QUFFbkIsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHO0lBQzNCLG9CQUFvQixFQUFFLEtBQUs7SUFDM0IsU0FBUyxFQUFFLEtBQUs7SUFDaEIsYUFBYSxFQUFFLEtBQUs7Q0FDSixDQUFDIn0=
// EXTERNAL MODULE: ./components/ui/loading/index.tsx + 4 modules
var loading = __webpack_require__(747);

// EXTERNAL MODULE: ./components/container/feed-list/index.tsx + 4 modules
var feed_list = __webpack_require__(783);

// EXTERNAL MODULE: ./container/layout/split/index.tsx
var split = __webpack_require__(753);

// EXTERNAL MODULE: ./components/community/collection-detail/index.tsx + 5 modules
var collection_detail = __webpack_require__(814);

// EXTERNAL MODULE: ./components/community/right-bar-community/index.tsx + 5 modules
var right_bar_community = __webpack_require__(784);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./container/app-shop/community/new-feed/style.tsx


/* harmony default export */ var style = ({
    container: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{}],
        DESKTOP: [{
                paddingTop: 10
            }],
        GENERAL: [{
                display: 'block',
                position: 'relative'
            }]
    }),
    btnWrap: {
        textAlign: 'center',
        paddingTop: 10,
        marginBottom: 20,
        btn: {
            marginTop: 0,
            marginBottom: 10,
            width: '100%',
            maxWidth: '100%',
        },
        btnMobile: {
            width: 'calc(100% - 40px)',
            marginTop: 0,
            marginBottom: 0,
            marginLeft: 20,
            marginRight: 20,
        }
    },
    wrapLayout: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0
    },
    feedList: {
        paddingLeft: 0,
        paddingRight: 0
    },
    wrapMobileLayout: {
        paddingBottom: 20
    },
    placeholder: {
        container: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{}],
            DESKTOP: [{
                    paddingTop: 10
                }],
            GENERAL: [{
                    paddingBottom: 10,
                }]
        }),
        item: {
            marginBottom: 20,
            borderRadius: 3,
            border: "1px solid " + variable["colorF0"],
            background: variable["colorWhite"],
            padding: 10,
            top: {
                display: variable["display"].flex,
                paddingTop: 5,
                marginBottom: 10,
                avatar: {
                    width: 50,
                    height: 50,
                    borderRadius: '50%',
                    marginRight: 10,
                },
                info: { flex: 10 },
                username: {
                    width: 180,
                    height: 20,
                    marginBottom: 10,
                    borderRadius: 2
                },
                star: {
                    width: 100,
                    height: 20,
                    marginBottom: 10,
                    borderRadius: 2
                },
            },
            bottom: {
                firstText: {
                    width: '72%',
                    height: 20,
                    marginBottom: 10,
                    borderRadius: 2
                },
                text: {
                    width: '65%',
                    height: 20,
                    marginBottom: 10,
                    borderRadius: 2
                },
                lastText: {
                    width: '40%',
                    height: 20,
                    borderRadius: 2
                },
            },
        },
    },
    collection: {
        mobile: {
            margin: 0
        },
        desktop: {
            marginBottom: 20
        },
    },
    hashTagTitle: {
        container: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{}],
            DESKTOP: [{}],
            GENERAL: [{
                    display: variable["display"].flex,
                    alignItems: 'center',
                    paddingLeft: 20,
                    paddingRight: 20
                }]
        }),
        character: {
            display: variable["display"].flex,
            alignItems: 'center',
            justifyContent: 'center',
            width: 40,
            fontSize: 40,
            marginRight: 10
        },
        content: {
            fontSize: 38,
            fontFamily: variable["fontAvenirDemiBold"]
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLFFBQVEsTUFBTSw0QkFBNEIsQ0FBQztBQUN2RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFFNUQsZUFBZTtJQUNiLFNBQVMsRUFBRSxZQUFZLENBQUM7UUFDdEIsTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDO1FBQ1osT0FBTyxFQUFFLENBQUM7Z0JBQ1IsVUFBVSxFQUFFLEVBQUU7YUFDZixDQUFDO1FBQ0YsT0FBTyxFQUFFLENBQUM7Z0JBQ1IsT0FBTyxFQUFFLE9BQU87Z0JBQ2hCLFFBQVEsRUFBRSxVQUFVO2FBQ3JCLENBQUM7S0FDSCxDQUFDO0lBRUYsT0FBTyxFQUFFO1FBQ1AsU0FBUyxFQUFFLFFBQVE7UUFDbkIsVUFBVSxFQUFFLEVBQUU7UUFDZCxZQUFZLEVBQUUsRUFBRTtRQUVoQixHQUFHLEVBQUU7WUFDSCxTQUFTLEVBQUUsQ0FBQztZQUNaLFlBQVksRUFBRSxFQUFFO1lBQ2hCLEtBQUssRUFBRSxNQUFNO1lBQ2IsUUFBUSxFQUFFLE1BQU07U0FDakI7UUFFRCxTQUFTLEVBQUU7WUFDVCxLQUFLLEVBQUUsbUJBQW1CO1lBQzFCLFNBQVMsRUFBRSxDQUFDO1lBQ1osWUFBWSxFQUFFLENBQUM7WUFDZixVQUFVLEVBQUUsRUFBRTtZQUNkLFdBQVcsRUFBRSxFQUFFO1NBQ2hCO0tBQ0Y7SUFFRCxVQUFVLEVBQUU7UUFDVixXQUFXLEVBQUUsQ0FBQztRQUNkLFlBQVksRUFBRSxDQUFDO1FBQ2YsVUFBVSxFQUFFLENBQUM7UUFDYixhQUFhLEVBQUUsQ0FBQztLQUNqQjtJQUVELFFBQVEsRUFBRTtRQUNSLFdBQVcsRUFBRSxDQUFDO1FBQ2QsWUFBWSxFQUFFLENBQUM7S0FDaEI7SUFFRCxnQkFBZ0IsRUFBRTtRQUNoQixhQUFhLEVBQUUsRUFBRTtLQUNsQjtJQUVELFdBQVcsRUFBRTtRQUNYLFNBQVMsRUFBRSxZQUFZLENBQUM7WUFDdEIsTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDO1lBQ1osT0FBTyxFQUFFLENBQUM7b0JBQ1IsVUFBVSxFQUFFLEVBQUU7aUJBQ2YsQ0FBQztZQUNGLE9BQU8sRUFBRSxDQUFDO29CQUNSLGFBQWEsRUFBRSxFQUFFO2lCQUNsQixDQUFDO1NBQ0gsQ0FBQztRQUVGLElBQUksRUFBRTtZQUNKLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFlBQVksRUFBRSxDQUFDO1lBQ2YsTUFBTSxFQUFFLGVBQWEsUUFBUSxDQUFDLE9BQVM7WUFDdkMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxVQUFVO1lBQy9CLE9BQU8sRUFBRSxFQUFFO1lBRVgsR0FBRyxFQUFFO2dCQUNILE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLFVBQVUsRUFBRSxDQUFDO2dCQUNiLFlBQVksRUFBRSxFQUFFO2dCQUVoQixNQUFNLEVBQUU7b0JBQ04sS0FBSyxFQUFFLEVBQUU7b0JBQ1QsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsWUFBWSxFQUFFLEtBQUs7b0JBQ25CLFdBQVcsRUFBRSxFQUFFO2lCQUNoQjtnQkFFRCxJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFO2dCQUVsQixRQUFRLEVBQUU7b0JBQ1IsS0FBSyxFQUFFLEdBQUc7b0JBQ1YsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLFlBQVksRUFBRSxDQUFDO2lCQUNoQjtnQkFFRCxJQUFJLEVBQUU7b0JBQ0osS0FBSyxFQUFFLEdBQUc7b0JBQ1YsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLFlBQVksRUFBRSxDQUFDO2lCQUNoQjthQUNGO1lBRUQsTUFBTSxFQUFFO2dCQUNOLFNBQVMsRUFBRTtvQkFDVCxLQUFLLEVBQUUsS0FBSztvQkFDWixNQUFNLEVBQUUsRUFBRTtvQkFDVixZQUFZLEVBQUUsRUFBRTtvQkFDaEIsWUFBWSxFQUFFLENBQUM7aUJBQ2hCO2dCQUVELElBQUksRUFBRTtvQkFDSixLQUFLLEVBQUUsS0FBSztvQkFDWixNQUFNLEVBQUUsRUFBRTtvQkFDVixZQUFZLEVBQUUsRUFBRTtvQkFDaEIsWUFBWSxFQUFFLENBQUM7aUJBQ2hCO2dCQUVELFFBQVEsRUFBRTtvQkFDUixLQUFLLEVBQUUsS0FBSztvQkFDWixNQUFNLEVBQUUsRUFBRTtvQkFDVixZQUFZLEVBQUUsQ0FBQztpQkFDaEI7YUFDRjtTQUNGO0tBQ0Y7SUFFRCxVQUFVLEVBQUU7UUFDVixNQUFNLEVBQUU7WUFDTixNQUFNLEVBQUUsQ0FBQztTQUNWO1FBRUQsT0FBTyxFQUFFO1lBQ1AsWUFBWSxFQUFFLEVBQUU7U0FDakI7S0FDRjtJQUVELFlBQVksRUFBRTtRQUNaLFNBQVMsRUFBRSxZQUFZLENBQUM7WUFDdEIsTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDO1lBQ1osT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDO1lBQ2IsT0FBTyxFQUFFLENBQUM7b0JBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtvQkFDOUIsVUFBVSxFQUFFLFFBQVE7b0JBQ3BCLFdBQVcsRUFBRSxFQUFFO29CQUNmLFlBQVksRUFBRSxFQUFFO2lCQUNqQixDQUFDO1NBQ0gsQ0FBQztRQUVGLFNBQVMsRUFBRTtZQUNULE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsVUFBVSxFQUFFLFFBQVE7WUFDcEIsY0FBYyxFQUFFLFFBQVE7WUFDeEIsS0FBSyxFQUFFLEVBQUU7WUFDVCxRQUFRLEVBQUUsRUFBRTtZQUNaLFdBQVcsRUFBRSxFQUFFO1NBQ2hCO1FBRUQsT0FBTyxFQUFFO1lBQ1AsUUFBUSxFQUFFLEVBQUU7WUFDWixVQUFVLEVBQUUsUUFBUSxDQUFDLGtCQUFrQjtTQUN4QztLQUNGO0NBQ0ssQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/community/new-feed/view-desktop.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};








var loadMoreButton = function () { return (react["createElement"]("div", { style: style.btnWrap, id: 'load-view-more' },
    react["createElement"](loading["a" /* default */], null))); };
var renderHashTagTitle = function (_a) {
    var hashtag = _a.hashtag;
    return (react["createElement"]("div", { style: style.hashTagTitle.container },
        react["createElement"]("div", { style: style.hashTagTitle.character }, "#"),
        react["createElement"]("div", { style: style.hashTagTitle.content }, hashtag)));
};
var renderMainContainer = function (_a) {
    var props = _a.props, list = _a.list, userProfile = _a.userProfile, collection = _a.collection, showCommunityHashTag = _a.showCommunityHashTag, _b = _a.isHiddenLoadMore, isHiddenLoadMore = _b === void 0 ? false : _b;
    var hashtag = props.match.params.hashtag;
    var feedListProps = {
        list: list,
        history: history,
        userProfile: userProfile,
        style: style.feedList
    };
    var isShow = Array.isArray(list) && list.length > 0;
    return (react["createElement"]("div", { style: style.container, className: 'user-select-all' },
        react["createElement"]("div", { className: 'new-feeds-desktop' }, isShow
            ? (react["createElement"]("div", null,
                !showCommunityHashTag && react["createElement"](collection_detail["a" /* default */], { collectionDetailId: collection && Array.isArray(collection.list) && !!collection.list.length && collection.list[0].id || 0 }),
                showCommunityHashTag && renderHashTagTitle({ hashtag: hashtag }),
                react["createElement"](feed_list["a" /* default */], __assign({}, feedListProps))))
            : renderLoadingPlaceholder()),
        !isHiddenLoadMore && loadMoreButton()));
};
function renderComponent(_a) {
    var props = _a.props, state = _a.state;
    var _b = props, userInfo = _b.authStore.userInfo, hashtag = _b.match.params.hashtag, hashtagFeeds = _b.communityStore.hashtagFeeds, _c = _b.activityFeedStore, list = _c.list, collection = _c.collection;
    var _d = state, showCommunityHashTag = _d.showCommunityHashTag, isFeebackFull = _d.isFeebackFull;
    var feedList = showCommunityHashTag ? hashtagFeeds : list;
    var splitLayoutProps = {
        type: 'right',
        size: 'larger',
        subContainer: react["createElement"](right_bar_community["a" /* default */], { hashtagSelected: hashtag }),
        mainContainer: renderMainContainer({
            props: props,
            collection: collection,
            showCommunityHashTag: showCommunityHashTag,
            list: feedList || [],
            userProfile: userInfo,
            isHiddenLoadMore: isFeebackFull
        })
    };
    return (react["createElement"]("div", { style: style.wrapLayout },
        react["createElement"](split["a" /* default */], __assign({}, splitLayoutProps))));
}
;
/* harmony default export */ var view_desktop = (renderComponent);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1kZXNrdG9wLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1kZXNrdG9wLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRS9CLE9BQU8sT0FBTyxNQUFNLG1DQUFtQyxDQUFDO0FBQ3hELE9BQU8sUUFBUSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2xFLE9BQU8sV0FBVyxNQUFNLHVCQUF1QixDQUFDO0FBQ2hELE9BQU8sZ0JBQWdCLE1BQU0sb0RBQW9ELENBQUM7QUFDbEYsT0FBTyxpQkFBaUIsTUFBTSxzREFBc0QsQ0FBQztBQUdyRixPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFDbEQsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRTVCLElBQU0sY0FBYyxHQUFHLGNBQU0sT0FBQSxDQUMzQiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU8sRUFBRSxFQUFFLEVBQUUsZ0JBQWdCO0lBQzdDLG9CQUFDLE9BQU8sT0FBRyxDQUNQLENBQ1AsRUFKNEIsQ0FJNUIsQ0FBQztBQUVGLElBQU0sa0JBQWtCLEdBQUcsVUFBQyxFQUFXO1FBQVQsb0JBQU87SUFBTyxPQUFBLENBQzFDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWSxDQUFDLFNBQVM7UUFDdEMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxZQUFZLENBQUMsU0FBUyxRQUFTO1FBQ2pELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sSUFBRyxPQUFPLENBQU8sQ0FDbkQsQ0FDUDtBQUwyQyxDQUszQyxDQUFDO0FBRUYsSUFBTSxtQkFBbUIsR0FBRyxVQUFDLEVBQXdGO1FBQXRGLGdCQUFLLEVBQUUsY0FBSSxFQUFFLDRCQUFXLEVBQUUsMEJBQVUsRUFBRSw4Q0FBb0IsRUFBRSx3QkFBd0IsRUFBeEIsNkNBQXdCO0lBRXRGLElBQUEsb0NBQU8sQ0FBZ0M7SUFFbEUsSUFBTSxhQUFhLEdBQUc7UUFDcEIsSUFBSSxNQUFBO1FBQ0osT0FBTyxTQUFBO1FBQ1AsV0FBVyxhQUFBO1FBQ1gsS0FBSyxFQUFFLEtBQUssQ0FBQyxRQUFRO0tBQ3RCLENBQUM7SUFFRixJQUFNLE1BQU0sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBRXRELE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxFQUFFLFNBQVMsRUFBRSxpQkFBaUI7UUFDdkQsNkJBQUssU0FBUyxFQUFFLG1CQUFtQixJQUUvQixNQUFNO1lBQ0osQ0FBQyxDQUFDLENBQ0E7Z0JBQ0csQ0FBQyxvQkFBb0IsSUFBSSxvQkFBQyxnQkFBZ0IsSUFBQyxrQkFBa0IsRUFBRSxVQUFVLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsR0FBSTtnQkFDekssb0JBQW9CLElBQUksa0JBQWtCLENBQUMsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDO2dCQUN4RCxvQkFBQyxRQUFRLGVBQUssYUFBYSxFQUFJLENBQzNCLENBQ1A7WUFDRCxDQUFDLENBQUMsd0JBQXdCLEVBQUUsQ0FFNUI7UUFDTCxDQUFDLGdCQUFnQixJQUFJLGNBQWMsRUFBRSxDQUNsQyxDQUNQLENBQUM7QUFDSixDQUFDLENBQUE7QUFFRCxNQUFNLDBCQUEwQixFQUFnQjtRQUFkLGdCQUFLLEVBQUUsZ0JBQUs7SUFDdEMsSUFBQSxVQUtvQixFQUpYLGdDQUFRLEVBQ0YsaUNBQU8sRUFDUiw2Q0FBWSxFQUM5Qix5QkFBdUMsRUFBbEIsY0FBSSxFQUFFLDBCQUFVLENBQ1o7SUFFckIsSUFBQSxVQUFnRSxFQUE5RCw4Q0FBb0IsRUFBRSxnQ0FBYSxDQUE0QjtJQUV2RSxJQUFNLFFBQVEsR0FBRyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7SUFFNUQsSUFBTSxnQkFBZ0IsR0FBRztRQUN2QixJQUFJLEVBQUUsT0FBTztRQUNiLElBQUksRUFBRSxRQUFRO1FBQ2QsWUFBWSxFQUFFLG9CQUFDLGlCQUFpQixJQUFDLGVBQWUsRUFBRSxPQUFPLEdBQUk7UUFDN0QsYUFBYSxFQUFFLG1CQUFtQixDQUFDO1lBQ2pDLEtBQUssT0FBQTtZQUNMLFVBQVUsWUFBQTtZQUNWLG9CQUFvQixzQkFBQTtZQUNwQixJQUFJLEVBQUUsUUFBUSxJQUFJLEVBQUU7WUFDcEIsV0FBVyxFQUFFLFFBQVE7WUFDckIsZ0JBQWdCLEVBQUUsYUFBYTtTQUNoQyxDQUFDO0tBQ0gsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsVUFBVTtRQUMxQixvQkFBQyxXQUFXLGVBQUssZ0JBQWdCLEVBQUksQ0FDakMsQ0FDUCxDQUFDO0FBQ0osQ0FBQztBQUFBLENBQUM7QUFFRixlQUFlLGVBQWUsQ0FBQyJ9
// EXTERNAL MODULE: ./components/community/header-mobile/index.tsx + 4 modules
var header_mobile = __webpack_require__(794);

// EXTERNAL MODULE: ./utils/encode.ts
var encode = __webpack_require__(7);

// CONCATENATED MODULE: ./container/app-shop/community/new-feed/view-mobile.tsx
var view_mobile_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};









var view_mobile_loadMoreButton = function () { return (react["createElement"]("div", { style: style.btnWrap, id: 'load-view-more' },
    react["createElement"](loading["a" /* default */], null))); };
var renderView = function (_a) {
    var state = _a.state, props = _a.props, feedList = _a.feedList, collection = _a.collection, userProfile = _a.userProfile, _b = _a.isHiddenLoadMore, isHiddenLoadMore = _b === void 0 ? false : _b;
    var feedListProps = {
        history: history,
        userProfile: userProfile,
        list: feedList,
        style: style.feedList
    };
    var _c = props, pathname = _c.location.pathname, hashtags = _c.communityStore.hashtags, hashtag = _c.match.params.hashtag;
    var showCommunityHashTag = state.showCommunityHashTag;
    var keyHash = Object(encode["h" /* objectToHash */])({ days: 7 });
    var hashTagList = hashtags && !Object(validate["l" /* isUndefined */])(hashtags[keyHash]) ? hashtags[keyHash] : [];
    var headerMobileProps = {
        pathname: pathname,
        hashTagList: hashTagList,
        hashtagSelected: showCommunityHashTag ? "# " + hashtag : ''
    };
    var isShow = Array.isArray(feedList) && feedList.length > 0;
    return (react["createElement"]("div", { style: style.container },
        react["createElement"]("div", null, isShow
            ? (react["createElement"]("div", null,
                react["createElement"](header_mobile["a" /* default */], view_mobile_assign({}, headerMobileProps)),
                !showCommunityHashTag && react["createElement"](collection_detail["a" /* default */], { collectionDetailId: collection && collection.list && !!collection.list.length && collection.list[0].id || 0 }),
                react["createElement"](feed_list["a" /* default */], view_mobile_assign({}, feedListProps))))
            : renderLoadingPlaceholder()),
        !isHiddenLoadMore && view_mobile_loadMoreButton()));
};
function view_mobile_renderComponent(_a) {
    var props = _a.props, state = _a.state;
    var _b = props, activityFeedStore = _b.activityFeedStore, userInfo = _b.authStore.userInfo, list = _b.activityFeedStore.list, hashtagFeeds = _b.communityStore.hashtagFeeds;
    var _c = state, isFeebackFull = _c.isFeebackFull, showCommunityHashTag = _c.showCommunityHashTag;
    var feedList = showCommunityHashTag ? hashtagFeeds : list;
    return (react["createElement"]("div", { style: style.wrapMobileLayout }, renderView({
        state: state,
        props: props,
        feedList: feedList,
        userProfile: userInfo,
        isHiddenLoadMore: isFeebackFull,
        collection: activityFeedStore && activityFeedStore.collection
    })));
}
;
/* harmony default export */ var view_mobile = (view_mobile_renderComponent);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1tb2JpbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LW1vYmlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUUvQixPQUFPLE9BQU8sTUFBTSxtQ0FBbUMsQ0FBQztBQUN4RCxPQUFPLFFBQVEsTUFBTSw0Q0FBNEMsQ0FBQztBQUNsRSxPQUFPLGdCQUFnQixNQUFNLG9EQUFvRCxDQUFDO0FBQ2xGLE9BQU8sWUFBWSxNQUFNLGdEQUFnRCxDQUFDO0FBQzFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFeEQsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRTVCLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUVsRCxJQUFNLGNBQWMsR0FBRyxjQUFNLE9BQUEsQ0FDM0IsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxPQUFPLEVBQUUsRUFBRSxFQUFFLGdCQUFnQjtJQUM3QyxvQkFBQyxPQUFPLE9BQUcsQ0FDUCxDQUNQLEVBSjRCLENBSTVCLENBQUM7QUFFRixJQUFNLFVBQVUsR0FBRyxVQUFDLEVBT25CO1FBTkMsZ0JBQUssRUFDTCxnQkFBSyxFQUNMLHNCQUFRLEVBQ1IsMEJBQVUsRUFDViw0QkFBVyxFQUNYLHdCQUF3QixFQUF4Qiw2Q0FBd0I7SUFHeEIsSUFBTSxhQUFhLEdBQUc7UUFDcEIsT0FBTyxTQUFBO1FBQ1AsV0FBVyxhQUFBO1FBQ1gsSUFBSSxFQUFFLFFBQVE7UUFDZCxLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVE7S0FDdEIsQ0FBQztJQUVJLElBQUEsVUFJb0IsRUFIWiwrQkFBUSxFQUNGLHFDQUFRLEVBQ1AsaUNBQU8sQ0FDRDtJQUVuQixJQUFBLGlEQUFvQixDQUE0QjtJQUV4RCxJQUFNLE9BQU8sR0FBRyxZQUFZLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUMxQyxJQUFNLFdBQVcsR0FBRyxRQUFRLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBRXpGLElBQU0saUJBQWlCLEdBQUc7UUFDeEIsUUFBUSxVQUFBO1FBQ1IsV0FBVyxhQUFBO1FBQ1gsZUFBZSxFQUFFLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxPQUFLLE9BQVMsQ0FBQyxDQUFDLENBQUMsRUFBRTtLQUM1RCxDQUFDO0lBRUYsSUFBTSxNQUFNLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUU5RCxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFNBQVM7UUFDekIsaUNBRUksTUFBTTtZQUNKLENBQUMsQ0FBQyxDQUNBO2dCQUNFLG9CQUFDLFlBQVksZUFBSyxpQkFBaUIsRUFBSTtnQkFDdEMsQ0FBQyxvQkFBb0IsSUFBSSxvQkFBQyxnQkFBZ0IsSUFBQyxrQkFBa0IsRUFBRSxVQUFVLElBQUksVUFBVSxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxHQUFJO2dCQUMzSixvQkFBQyxRQUFRLGVBQUssYUFBYSxFQUFJLENBQzNCLENBQ1A7WUFDRCxDQUFDLENBQUMsd0JBQXdCLEVBQUUsQ0FFNUI7UUFDTCxDQUFDLGdCQUFnQixJQUFJLGNBQWMsRUFBRSxDQUNsQyxDQUNQLENBQUM7QUFDSixDQUFDLENBQUE7QUFFRCxNQUFNLDBCQUEwQixFQUFnQjtRQUFkLGdCQUFLLEVBQUUsZ0JBQUs7SUFDdEMsSUFBQSxVQUtvQixFQUp4Qix3Q0FBaUIsRUFDSixnQ0FBUSxFQUNBLGdDQUFJLEVBQ1AsNkNBQVksQ0FDTDtJQUVyQixJQUFBLFVBQWdFLEVBQTlELGdDQUFhLEVBQUUsOENBQW9CLENBQTRCO0lBRXZFLElBQU0sUUFBUSxHQUFHLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUU1RCxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGdCQUFnQixJQUU5QixVQUFVLENBQUM7UUFDVCxLQUFLLE9BQUE7UUFDTCxLQUFLLE9BQUE7UUFDTCxRQUFRLFVBQUE7UUFDUixXQUFXLEVBQUUsUUFBUTtRQUNyQixnQkFBZ0IsRUFBRSxhQUFhO1FBQy9CLFVBQVUsRUFBRSxpQkFBaUIsSUFBSSxpQkFBaUIsQ0FBQyxVQUFVO0tBQzlELENBQUMsQ0FFQSxDQUNQLENBQUM7QUFDSixDQUFDO0FBQUEsQ0FBQztBQUVGLGVBQWUsZUFBZSxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/community/new-feed/view.tsx




var renderLoadingPlaceholder = function () { return (react["createElement"]("div", { style: style.placeholder.container }, [1, 2, 3].map(function (item) { return (react["createElement"]("div", { style: style.placeholder.item, key: item },
    react["createElement"]("div", { style: style.placeholder.item.top },
        react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.top.avatar }),
        react["createElement"]("div", { style: style.placeholder.item.top.info },
            react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.top.username }),
            react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.top.star }))),
    react["createElement"]("div", { style: style.placeholder.item.bottom },
        react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.bottom.firstText }),
        react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.bottom.text }),
        react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.bottom.text }),
        react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.bottom.lastText })))); }))); };
var view_renderView = function (_a) {
    var props = _a.props, state = _a.state;
    var switchView = {
        MOBILE: function () { return view_mobile({ props: props, state: state }); },
        DESKTOP: function () { return view_desktop({ props: props, state: state }); }
    };
    return switchView[window.DEVICE_VERSION]();
};
/* harmony default export */ var view = (view_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sYUFBYSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sWUFBWSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsTUFBTSxDQUFDLElBQU0sd0JBQXdCLEdBQUcsY0FBTSxPQUFBLENBQzVDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFNBQVMsSUFFbkMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLENBQ3BCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSTtJQUMzQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRztRQUNwQyw2QkFBSyxTQUFTLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFRO1FBQzFFLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSTtZQUN6Qyw2QkFBSyxTQUFTLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFRO1lBQzVFLDZCQUFLLFNBQVMsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEdBQVEsQ0FDcEUsQ0FDRjtJQUNOLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNO1FBQ3ZDLDZCQUFLLFNBQVMsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQVE7UUFDaEYsNkJBQUssU0FBUyxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksR0FBUTtRQUMzRSw2QkFBSyxTQUFTLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFRO1FBQzNFLDZCQUFLLFNBQVMsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQVEsQ0FDM0UsQ0FDRixDQUNQLEVBaEJxQixDQWdCckIsQ0FBQyxDQUVBLENBQ1AsRUF0QjZDLENBc0I3QyxDQUFDO0FBRUYsSUFBTSxVQUFVLEdBQUcsVUFBQyxFQUFnQjtRQUFkLGdCQUFLLEVBQUUsZ0JBQUs7SUFDaEMsSUFBTSxVQUFVLEdBQUc7UUFDakIsTUFBTSxFQUFFLGNBQU0sT0FBQSxZQUFZLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxLQUFLLE9BQUEsRUFBRSxDQUFDLEVBQTlCLENBQThCO1FBQzVDLE9BQU8sRUFBRSxjQUFNLE9BQUEsYUFBYSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQyxFQUEvQixDQUErQjtLQUMvQyxDQUFDO0lBRUYsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQTtBQUM1QyxDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/community/new-feed/container.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var connect = __webpack_require__(129).connect;




var container_NewFeedContainer = /** @class */ (function (_super) {
    __extends(NewFeedContainer, _super);
    function NewFeedContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    NewFeedContainer.prototype.handleSubmit = function () {
        var _a = this.props, limit = _a.limit, list = _a.activityFeedStore.list, fecthActivityFeedListAction = _a.fecthActivityFeedListAction, hashtag = _a.match.params.hashtag, hashtagFeeds = _a.communityStore.hashtagFeeds, fetchCommunityHashtagFeedsAction = _a.fetchCommunityHashtagFeedsAction;
        var showCommunityHashTag = this.state.showCommunityHashTag;
        var feedList = showCommunityHashTag ? hashtagFeeds : list;
        var length = feedList && feedList.length || 0;
        var currentId = length > 0 ? feedList[length - 1].id : 0;
        showCommunityHashTag
            ? fetchCommunityHashtagFeedsAction({ hashtag: hashtag, limit: limit, currentId: currentId })
            : fecthActivityFeedListAction({ limit: limit, currentId: currentId });
    };
    NewFeedContainer.prototype.handleLoadViewMore = function () {
        var _a = this.state, isLoading = _a.isLoading, isFeebackFull = _a.isFeebackFull;
        var eleInfo = this.getPositionElementById('load-view-more');
        var height = window.innerHeight;
        !isFeebackFull
            && !isLoading
            && eleInfo
            && (eleInfo.top - 100) <= height
            && this.setState({ isLoading: true }, this.handleSubmit);
    };
    NewFeedContainer.prototype.getScrollTop = function () {
        var el = document.scrollingElement || document.documentElement;
        return el.scrollTop;
    };
    NewFeedContainer.prototype.getPositionElementById = function (elementId) {
        var el = document.getElementById(elementId);
        return el && el.getBoundingClientRect();
    };
    NewFeedContainer.prototype.init = function (props) {
        if (props === void 0) { props = this.props; }
        var _a = props, limit = _a.limit, getCollectionAction = _a.getCollectionAction, fecthActivityFeedListAction = _a.fecthActivityFeedListAction, hashtag = _a.match.params.hashtag, fetchCommunityHashtagFeedsAction = _a.fetchCommunityHashtagFeedsAction;
        getCollectionAction({});
        hashtag
            ? this.setState({ showCommunityHashTag: true }, fetchCommunityHashtagFeedsAction({ hashtag: hashtag, limit: limit }))
            : this.setState({ showCommunityHashTag: false }, fecthActivityFeedListAction({ limit: limit }));
    };
    NewFeedContainer.prototype.componentDidMount = function () {
        this.init();
        window.addEventListener('scroll', this.handleLoadViewMore.bind(this));
    };
    NewFeedContainer.prototype.componentWillUnmount = function () {
        window.addEventListener('scroll', function () { });
    };
    NewFeedContainer.prototype.componentWillReceiveProps = function (nextProps) {
        var _a = this.props, activityFeedStore = _a.activityFeedStore, updateMetaInfoAction = _a.updateMetaInfoAction, hashtag = _a.match.params.hashtag, _b = _a.communityStore, isFetchFeedbackSuccessful = _b.isFetchFeedbackSuccessful, isFetchedAllFeedback = _b.isFetchedAllFeedback, hashtagFeeds = _b.hashtagFeeds;
        if (hashtag !== nextProps.match.params.hashtag) {
            this.setState({ isLoading: false, isFeebackFull: false });
            this.init(nextProps);
        }
        !isFetchFeedbackSuccessful
            && nextProps.communityStore.isFetchFeedbackSuccessful
            && this.setState({ isLoading: false });
        !activityFeedStore.isFetchFeedbackSuccessful
            && nextProps.activityFeedStore.isFetchFeedbackSuccessful
            && this.setState({ isLoading: false });
        !isFetchedAllFeedback
            && nextProps.communityStore.isFetchedAllFeedback
            && this.setState({ isFeebackFull: true });
        !activityFeedStore.isFetchedAllFeedback
            && nextProps.activityFeedStore.isFetchedAllFeedback
            && this.setState({ isFeebackFull: true });
        // Set meta for SEO
        var showCommunityHashTag = this.state.showCommunityHashTag;
        var feedList = showCommunityHashTag ? hashtagFeeds : activityFeedStore.list;
        var nextFeedList = showCommunityHashTag ? nextProps.communityStore.hashtagFeeds : nextProps.activityFeedStore.list;
        var tag = showCommunityHashTag && nextProps.match.params.hashtag || '';
        (Object(validate["l" /* isUndefined */])(feedList) || feedList.length === 0)
            && !Object(validate["l" /* isUndefined */])(nextFeedList)
            && nextFeedList.length > 0
            && updateMetaInfoAction({
                info: {
                    url: "http://lxbtest.tk/community/" + tag,
                    type: "article",
                    title: 'LixiBox Community | Cộng đồng chia sẻ kiến thức làm đẹp',
                    description: 'Cộng đồng chia sẻ kiến thức sản phẩm và các kinh nghiệm làm đẹp',
                    keyword: 'lixibox, community, halio',
                    image: ''
                },
                structuredData: {
                    breadcrumbList: [{
                            position: 2,
                            name: 'LixiBox Community',
                            item: "http://lxbtest.tk/community/" + tag
                        }]
                }
            });
    };
    /*
    shouldComponentUpdate(nextProps, nextState) {
      const {
        location: { pathname },
        authStore: { userInfo },
        match: { params: { hashtag } },
        communityStore: { hashtagFeeds },
        activityFeedStore: { list, collection }
      } = this.props;
  
      const {
        isLoading,
        isFeebackFull,
        showCommunityHashTag
      } = this.state;
  
      if (isLoading !== nextState.isLoading) { return true; };
      if (isFeebackFull !== nextState.isFeebackFull) { return true; };
  
      if (pathname !== nextProps.location.pathname) { return true; };
      if (hashtag !== nextProps.match.params.hashtag) { return true; };
  
      if (showCommunityHashTag
        && hashtagFeeds
        && !isCompareObject(hashtagFeeds, nextProps.communityStore.hashtagFeeds)) { return true; };
  
      if (!showCommunityHashTag
        && list
        && !isCompareObject(list, nextProps.activityFeedStore.list)) { return true; };
  
      if (collection
        && collection.list
        && nextProps.activityFeedStore.collection
        && nextProps.activityFeedStore.collection.list
        && !isCompareObject(collection.list, nextProps.activityFeedStore.collection.list)) { return true; };
  
      if (!isCompareObject(userInfo, nextProps.authStore.userInfo)) { return true; };
  
      return false;
    }
    */
    NewFeedContainer.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state,
            handleSubmit: this.handleSubmit.bind(this)
        };
        return view(args);
    };
    NewFeedContainer.defaultProps = DEFAULT_PROPS;
    NewFeedContainer = __decorate([
        radium
    ], NewFeedContainer);
    return NewFeedContainer;
}(react["Component"]));
;
/* harmony default export */ var container = (connect(mapStateToProps, mapDispatchToProps)(container_NewFeedContainer));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFDakMsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFFekQsT0FBTyxFQUFFLGVBQWUsRUFBRSxrQkFBa0IsRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUM5RCxPQUFPLEVBQUUsYUFBYSxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUU1RCxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFHaEM7SUFBK0Isb0NBQTZDO0lBRzFFLDBCQUFZLEtBQW9CO1FBQWhDLFlBQ0Usa0JBQU0sS0FBSyxDQUFDLFNBRWI7UUFEQyxLQUFJLENBQUMsS0FBSyxHQUFHLGFBQThCLENBQUM7O0lBQzlDLENBQUM7SUFFRCx1Q0FBWSxHQUFaO1FBQ1EsSUFBQSxlQU9RLEVBTlosZ0JBQUssRUFDZ0IsZ0NBQUksRUFDekIsNERBQTJCLEVBQ1IsaUNBQU8sRUFDUiw2Q0FBWSxFQUM5QixzRUFBZ0MsQ0FDbkI7UUFFUCxJQUFBLHNEQUFvQixDQUFpQztRQUU3RCxJQUFNLFFBQVEsR0FBRyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDNUQsSUFBTSxNQUFNLEdBQUcsUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFBO1FBQy9DLElBQU0sU0FBUyxHQUFHLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFM0Qsb0JBQW9CO1lBQ2xCLENBQUMsQ0FBQyxnQ0FBZ0MsQ0FBQyxFQUFFLE9BQU8sU0FBQSxFQUFFLEtBQUssT0FBQSxFQUFFLFNBQVMsV0FBQSxFQUFFLENBQUM7WUFDakUsQ0FBQyxDQUFDLDJCQUEyQixDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsU0FBUyxXQUFBLEVBQUUsQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFFRCw2Q0FBa0IsR0FBbEI7UUFDUSxJQUFBLGVBQTBELEVBQXhELHdCQUFTLEVBQUUsZ0NBQWEsQ0FBaUM7UUFFakUsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDNUQsSUFBTSxNQUFNLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztRQUVsQyxDQUFDLGFBQWE7ZUFDVCxDQUFDLFNBQVM7ZUFDVixPQUFPO2VBQ1AsQ0FBQyxPQUFPLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxJQUFJLE1BQU07ZUFDN0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDN0QsQ0FBQztJQUVELHVDQUFZLEdBQVo7UUFDRSxJQUFNLEVBQUUsR0FBRyxRQUFRLENBQUMsZ0JBQWdCLElBQUksUUFBUSxDQUFDLGVBQWUsQ0FBQztRQUNqRSxNQUFNLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQTtJQUNyQixDQUFDO0lBRUQsaURBQXNCLEdBQXRCLFVBQXVCLFNBQVM7UUFDOUIsSUFBTSxFQUFFLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM5QyxNQUFNLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO0lBQzFDLENBQUM7SUFFRCwrQkFBSSxHQUFKLFVBQUssS0FBa0I7UUFBbEIsc0JBQUEsRUFBQSxRQUFRLElBQUksQ0FBQyxLQUFLO1FBQ2YsSUFBQSxVQU1vQixFQUx4QixnQkFBSyxFQUNMLDRDQUFtQixFQUNuQiw0REFBMkIsRUFDUixpQ0FBTyxFQUMxQixzRUFBZ0MsQ0FDUDtRQUUzQixtQkFBbUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUV4QixPQUFPO1lBQ0wsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxvQkFBb0IsRUFBRSxJQUFJLEVBQUUsRUFBRSxnQ0FBZ0MsQ0FBQyxFQUFFLE9BQU8sU0FBQSxFQUFFLEtBQUssT0FBQSxFQUFFLENBQUMsQ0FBQztZQUNyRyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLG9CQUFvQixFQUFFLEtBQUssRUFBRSxFQUFFLDJCQUEyQixDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDN0YsQ0FBQztJQUVELDRDQUFpQixHQUFqQjtRQUNFLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNaLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQ3hFLENBQUM7SUFFRCwrQ0FBb0IsR0FBcEI7UUFDRSxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLGNBQVEsQ0FBQyxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVELG9EQUF5QixHQUF6QixVQUEwQixTQUFTO1FBQzNCLElBQUEsZUFLeUIsRUFKN0Isd0NBQWlCLEVBQ2pCLDhDQUFvQixFQUNELGlDQUFPLEVBQzFCLHNCQUFpRixFQUEvRCx3REFBeUIsRUFBRSw4Q0FBb0IsRUFBRSw4QkFBWSxDQUNqRDtRQUVoQyxFQUFFLENBQUMsQ0FBQyxPQUFPLEtBQUssU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUMvQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztZQUMxRCxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3ZCLENBQUM7UUFFRCxDQUFDLHlCQUF5QjtlQUNyQixTQUFTLENBQUMsY0FBYyxDQUFDLHlCQUF5QjtlQUNsRCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7UUFFekMsQ0FBQyxpQkFBaUIsQ0FBQyx5QkFBeUI7ZUFDdkMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLHlCQUF5QjtlQUNyRCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7UUFFekMsQ0FBQyxvQkFBb0I7ZUFDaEIsU0FBUyxDQUFDLGNBQWMsQ0FBQyxvQkFBb0I7ZUFDN0MsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLGFBQWEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBRTVDLENBQUMsaUJBQWlCLENBQUMsb0JBQW9CO2VBQ2xDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxvQkFBb0I7ZUFDaEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLGFBQWEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBRTVDLG1CQUFtQjtRQUNYLElBQUEsc0RBQW9CLENBQWlDO1FBRTdELElBQU0sUUFBUSxHQUFHLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQztRQUM5RSxJQUFNLFlBQVksR0FBRyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUM7UUFDckgsSUFBTSxHQUFHLEdBQUcsb0JBQW9CLElBQUksU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQztRQUV6RSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQztlQUMzQyxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUM7ZUFDMUIsWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDO2VBQ3ZCLG9CQUFvQixDQUFDO2dCQUN0QixJQUFJLEVBQUU7b0JBQ0osR0FBRyxFQUFFLHVDQUFxQyxHQUFLO29CQUMvQyxJQUFJLEVBQUUsU0FBUztvQkFDZixLQUFLLEVBQUUseURBQXlEO29CQUNoRSxXQUFXLEVBQUUsaUVBQWlFO29CQUM5RSxPQUFPLEVBQUUsMkJBQTJCO29CQUNwQyxLQUFLLEVBQUUsRUFBRTtpQkFDVjtnQkFDRCxjQUFjLEVBQUU7b0JBQ2QsY0FBYyxFQUFFLENBQUM7NEJBQ2YsUUFBUSxFQUFFLENBQUM7NEJBQ1gsSUFBSSxFQUFFLG1CQUFtQjs0QkFDekIsSUFBSSxFQUFFLHVDQUFxQyxHQUFLO3lCQUNqRCxDQUFDO2lCQUNIO2FBQ0YsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O01Bd0NFO0lBRUYsaUNBQU0sR0FBTjtRQUNFLElBQU0sSUFBSSxHQUFHO1lBQ1gsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQzNDLENBQUM7UUFFRixNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUF4TE0sNkJBQVksR0FBa0IsYUFBYSxDQUFDO0lBRC9DLGdCQUFnQjtRQURyQixNQUFNO09BQ0QsZ0JBQWdCLENBMExyQjtJQUFELHVCQUFDO0NBQUEsQUExTEQsQ0FBK0IsS0FBSyxDQUFDLFNBQVMsR0EwTDdDO0FBQUEsQ0FBQztBQUVGLGVBQWUsT0FBTyxDQUFDLGVBQWUsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/community/new-feed/store.tsx
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapStateToProps", function() { return mapStateToProps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapDispatchToProps", function() { return mapDispatchToProps; });
var store_connect = __webpack_require__(129).connect;




var mapStateToProps = function (state) { return ({
    authStore: state.auth,
    communityStore: state.community,
    activityFeedStore: state.activityFeed,
}); };
var mapDispatchToProps = function (dispatch) { return ({
    getCollectionAction: function (data) { return dispatch(Object(activity_feed["g" /* getCollectionAction */])(data)); },
    fecthActivityFeedListAction: function (data) { return dispatch(Object(activity_feed["e" /* fecthActivityFeedListAction */])(data)); },
    fetchCommunityHashtagFeedsAction: function (data) { return dispatch(Object(community["a" /* fetchCommunityHashtagFeedsAction */])(data)); },
    updateMetaInfoAction: function (data) { return dispatch(Object(meta["a" /* updateMetaInfoAction */])(data)); }
}); };
/* harmony default export */ var store = __webpack_exports__["default"] = (store_connect(mapStateToProps, mapDispatchToProps)(container));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUMvRCxPQUFPLEVBQUUsZ0NBQWdDLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUNoRixPQUFPLEVBQUUsbUJBQW1CLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUVwRyxPQUFPLE9BQU8sTUFBTSxhQUFhLENBQUM7QUFFbEMsTUFBTSxDQUFDLElBQU0sZUFBZSxHQUFHLFVBQUMsS0FBSyxJQUFLLE9BQUEsQ0FBQztJQUN6QyxTQUFTLEVBQUUsS0FBSyxDQUFDLElBQUk7SUFDckIsY0FBYyxFQUFFLEtBQUssQ0FBQyxTQUFTO0lBQy9CLGlCQUFpQixFQUFFLEtBQUssQ0FBQyxZQUFZO0NBQ3RDLENBQUMsRUFKd0MsQ0FJeEMsQ0FBQztBQUVILE1BQU0sQ0FBQyxJQUFNLGtCQUFrQixHQUFHLFVBQUMsUUFBUSxJQUFLLE9BQUEsQ0FBQztJQUMvQyxtQkFBbUIsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFuQyxDQUFtQztJQUN2RSwyQkFBMkIsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUEzQyxDQUEyQztJQUN2RixnQ0FBZ0MsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyxnQ0FBZ0MsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFoRCxDQUFnRDtJQUNqRyxvQkFBb0IsRUFBRSxVQUFDLElBQUksSUFBSyxPQUFBLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFwQyxDQUFvQztDQUNyRSxDQUFDLEVBTDhDLENBSzlDLENBQUM7QUFFSCxlQUFlLE9BQU8sQ0FDcEIsZUFBZSxFQUNmLGtCQUFrQixDQUNuQixDQUFDLE9BQU8sQ0FBQyxDQUFDIn0=

/***/ })

}]);