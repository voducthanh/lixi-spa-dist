(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[82],{

/***/ 743:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// EXTERNAL MODULE: ./utils/uri.ts
var uri = __webpack_require__(347);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./container/404/style.tsx

/* harmony default export */ var style = ({
    wrap: {
        display: variable["display"].flex,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        maxWidth: 550,
        paddingTop: 50,
        paddingLeft: 0,
        paddingRight: 0,
        paddingBottom: 30,
        margin: '0 auto'
    },
    image: {
        display: variable["display"].block,
        width: 300,
        height: 'auto',
        minWidth: 100,
        minHeight: 100,
        marginBottom: 10,
        pointerEvents: 'none'
    },
    emptyContent: {
        marginBottom: 40,
        title: {
            fontSize: 24,
            lineHeight: '32px',
            marginBottom: 10,
            fontFamily: variable["fontTrirong"],
            fontWeight: 600,
            textAlign: 'center',
            color: variable["color97"],
        },
        description: {
            fontSize: 16,
            color: variable["color97"],
            textAlign: 'center',
            width: '100%',
            margin: '0 auto',
        }
    },
    linkNav: {
        width: '100%',
        display: variable["display"].flex,
        flexWrap: 'wrap',
        justifyContent: 'center'
    },
    iconShop: {
        width: 40,
        height: 40,
        color: variable["colorA2"]
    },
    iconMagazine: {
        width: 40,
        height: 40,
        color: variable["colorA2"]
    },
    iconInner: {
        height: 18
    },
    linkShop: {
        display: variable["display"].flex,
        border: "1px solid " + variable["colorA2"],
        margin: '5px 10px',
        borderRadius: 22,
        paddingRight: 25,
        paddingLeft: 10,
        height: 44,
        transition: variable["transitionNormal"],
        ':hover': {
            boxShadow: variable["shadow3"]
        }
    },
    textShop: {
        display: variable["display"].block,
        color: variable["colorA2"],
        lineHeight: '42px',
        fontFamily: variable["fontAvenirMedium"],
        whiteSpace: 'nowrap',
        fontSize: 16,
        textTransform: 'uppercase'
    },
    linkMagazine: {
        display: variable["display"].flex,
        border: "1px solid " + variable["colorA2"],
        margin: '5px 10px',
        borderRadius: 22,
        paddingRight: 25,
        paddingLeft: 10,
        height: 44,
        transition: variable["transitionNormal"],
        ':hover': {
            boxShadow: variable["shadow3"]
        }
    },
    textMagazine: {
        display: variable["display"].block,
        color: variable["colorA2"],
        lineHeight: '42px',
        fontFamily: variable["fontAvenirMedium"],
        whiteSpace: 'nowrap',
        fontSize: 16,
        textTransform: 'uppercase'
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSxzQkFBc0IsQ0FBQztBQUVqRCxlQUFlO0lBQ2IsSUFBSSxFQUFFO1FBQ0osT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUM5QixhQUFhLEVBQUUsUUFBUTtRQUN2QixjQUFjLEVBQUUsUUFBUTtRQUN4QixVQUFVLEVBQUUsUUFBUTtRQUNwQixLQUFLLEVBQUUsTUFBTTtRQUNiLFFBQVEsRUFBRSxHQUFHO1FBQ2IsVUFBVSxFQUFFLEVBQUU7UUFDZCxXQUFXLEVBQUUsQ0FBQztRQUNkLFlBQVksRUFBRSxDQUFDO1FBQ2YsYUFBYSxFQUFFLEVBQUU7UUFDakIsTUFBTSxFQUFFLFFBQVE7S0FDakI7SUFFRCxLQUFLLEVBQUU7UUFDTCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1FBQy9CLEtBQUssRUFBRSxHQUFHO1FBQ1YsTUFBTSxFQUFFLE1BQU07UUFDZCxRQUFRLEVBQUUsR0FBRztRQUNiLFNBQVMsRUFBRSxHQUFHO1FBQ2QsWUFBWSxFQUFFLEVBQUU7UUFDaEIsYUFBYSxFQUFFLE1BQU07S0FDdEI7SUFFRCxZQUFZLEVBQUU7UUFDWixZQUFZLEVBQUUsRUFBRTtRQUVoQixLQUFLLEVBQUU7WUFDTCxRQUFRLEVBQUUsRUFBRTtZQUNaLFVBQVUsRUFBRSxNQUFNO1lBQ2xCLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFVBQVUsRUFBRSxRQUFRLENBQUMsV0FBVztZQUNoQyxVQUFVLEVBQUUsR0FBRztZQUNmLFNBQVMsRUFBRSxRQUFRO1lBQ25CLEtBQUssRUFBRSxRQUFRLENBQUMsT0FBTztTQUN4QjtRQUVELFdBQVcsRUFBRTtZQUNYLFFBQVEsRUFBRSxFQUFFO1lBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO1lBQ3ZCLFNBQVMsRUFBRSxRQUFRO1lBQ25CLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLFFBQVE7U0FDakI7S0FDRjtJQUVELE9BQU8sRUFBRTtRQUNQLEtBQUssRUFBRSxNQUFNO1FBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUM5QixRQUFRLEVBQUUsTUFBTTtRQUNoQixjQUFjLEVBQUUsUUFBUTtLQUN6QjtJQUVELFFBQVEsRUFBRTtRQUNSLEtBQUssRUFBRSxFQUFFO1FBQ1QsTUFBTSxFQUFFLEVBQUU7UUFDVixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87S0FDeEI7SUFFRCxZQUFZLEVBQUU7UUFDWixLQUFLLEVBQUUsRUFBRTtRQUNULE1BQU0sRUFBRSxFQUFFO1FBQ1YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO0tBQ3hCO0lBRUQsU0FBUyxFQUFFO1FBQ1QsTUFBTSxFQUFFLEVBQUU7S0FDWDtJQUVELFFBQVEsRUFBRTtRQUNSLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7UUFDOUIsTUFBTSxFQUFFLGVBQWEsUUFBUSxDQUFDLE9BQVM7UUFDdkMsTUFBTSxFQUFFLFVBQVU7UUFDbEIsWUFBWSxFQUFFLEVBQUU7UUFDaEIsWUFBWSxFQUFFLEVBQUU7UUFDaEIsV0FBVyxFQUFFLEVBQUU7UUFDZixNQUFNLEVBQUUsRUFBRTtRQUNWLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO1FBRXJDLFFBQVEsRUFBRTtZQUNSLFNBQVMsRUFBRSxRQUFRLENBQUMsT0FBTztTQUM1QjtLQUNGO0lBRUQsUUFBUSxFQUFFO1FBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztRQUMvQixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87UUFDdkIsVUFBVSxFQUFFLE1BQU07UUFDbEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7UUFDckMsVUFBVSxFQUFFLFFBQVE7UUFDcEIsUUFBUSxFQUFFLEVBQUU7UUFDWixhQUFhLEVBQUUsV0FBVztLQUMzQjtJQUVELFlBQVksRUFBRTtRQUNaLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7UUFDOUIsTUFBTSxFQUFFLGVBQWEsUUFBUSxDQUFDLE9BQVM7UUFDdkMsTUFBTSxFQUFFLFVBQVU7UUFDbEIsWUFBWSxFQUFFLEVBQUU7UUFDaEIsWUFBWSxFQUFFLEVBQUU7UUFDaEIsV0FBVyxFQUFFLEVBQUU7UUFDZixNQUFNLEVBQUUsRUFBRTtRQUNWLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO1FBRXJDLFFBQVEsRUFBRTtZQUNSLFNBQVMsRUFBRSxRQUFRLENBQUMsT0FBTztTQUM1QjtLQUNGO0lBRUQsWUFBWSxFQUFFO1FBQ1osT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztRQUMvQixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87UUFDdkIsVUFBVSxFQUFFLE1BQU07UUFDbEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7UUFDckMsVUFBVSxFQUFFLFFBQVE7UUFDcEIsUUFBUSxFQUFFLEVBQUU7UUFDWixhQUFhLEVBQUUsV0FBVztLQUMzQjtDQUNNLENBQUMifQ==
// CONCATENATED MODULE: ./container/404/view.tsx




var background = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/404/404.png';

var renderView = function () {
    return (react["createElement"]("div", { style: style.wrap },
        react["createElement"]("img", { src: background, style: style.image }),
        react["createElement"]("div", { style: style.emptyContent },
            react["createElement"]("div", { style: style.emptyContent.title }, "L\u1ED9n ti\u1EC7m r\u1ED3i"),
            react["createElement"]("div", { style: style.emptyContent.description }, "C\u00F3 v\u1EBB nh\u01B0 b\u1EA1n \u0111\u00E3 v\u00E0o \u0111\u1ECBa ch\u1EC9 kh\u00F4ng \u0111\u00FAng"),
            react["createElement"]("div", { style: style.emptyContent.description }, "ho\u1EB7c s\u1EA3n ph\u1EA9m kh\u00F4ng c\u00F2n b\u00E1n tr\u00EAn LIXIBOX")),
        react["createElement"]("div", { style: style.linkNav },
            react["createElement"](react_router_dom["NavLink"], { style: style.linkShop, to: '/' },
                react["createElement"](icon["a" /* default */], { name: 'cart-line', style: style.iconShop, innerStyle: style.iconInner }),
                react["createElement"]("span", { style: style.textShop }, "Lixi Shopping")),
            react["createElement"](react_router_dom["NavLink"], { style: style.linkMagazine, to: '/magazine/' },
                react["createElement"](icon["a" /* default */], { name: 'magazine', style: style.iconMagazine, innerStyle: style.iconInner }),
                react["createElement"]("span", { style: style.textMagazine }, "Lixi Magazine")))));
};
/* harmony default export */ var view = __webpack_exports__["default"] = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUUzQyxPQUFPLElBQUksTUFBTSwwQkFBMEIsQ0FBQztBQUM1QyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUNwRCxJQUFNLFVBQVUsR0FBRyxpQkFBaUIsR0FBRyw0QkFBNEIsQ0FBQztBQUNwRSxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsSUFBTSxVQUFVLEdBQUc7SUFDakIsTUFBTSxDQUFDLENBQ0wsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJO1FBQ3BCLDZCQUFLLEdBQUcsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLLEdBQUk7UUFDNUMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxZQUFZO1lBQzVCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWSxDQUFDLEtBQUssa0NBQW9CO1lBQ3hELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWSxDQUFDLFdBQVcsK0dBQStDO1lBQ3pGLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWSxDQUFDLFdBQVcsa0ZBQWdELENBQ3RGO1FBQ04sNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxPQUFPO1lBQ3ZCLG9CQUFDLE9BQU8sSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxFQUFFLEVBQUUsR0FBRztnQkFDckMsb0JBQUMsSUFBSSxJQUFDLElBQUksRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxRQUFRLEVBQUUsVUFBVSxFQUFFLEtBQUssQ0FBQyxTQUFTLEdBQUk7Z0JBQy9FLDhCQUFNLEtBQUssRUFBRSxLQUFLLENBQUMsUUFBUSxvQkFBc0IsQ0FDekM7WUFDVixvQkFBQyxPQUFPLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxZQUFZLEVBQUUsRUFBRSxFQUFFLFlBQVk7Z0JBQ2xELG9CQUFDLElBQUksSUFBQyxJQUFJLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWSxFQUFFLFVBQVUsRUFBRSxLQUFLLENBQUMsU0FBUyxHQUFJO2dCQUNsRiw4QkFBTSxLQUFLLEVBQUUsS0FBSyxDQUFDLFlBQVksb0JBQXNCLENBQzdDLENBQ04sQ0FDRixDQUNQLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9

/***/ })

}]);