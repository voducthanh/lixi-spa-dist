(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[72,20],{

/***/ 755:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 96).then(__webpack_require__.bind(null, 777)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 758:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return renderHtmlContent; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var renderHtmlContent = function (message_html, style) {
    if (style === void 0) { style = {}; }
    return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { style: style, dangerouslySetInnerHTML: { __html: message_html } }));
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHRtbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImh0bWwudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRS9CLE1BQU0sQ0FBQyxJQUFNLGlCQUFpQixHQUFHLFVBQUMsWUFBb0IsRUFBRSxLQUFVO0lBQVYsc0JBQUEsRUFBQSxVQUFVO0lBQUssT0FBQSxDQUNyRSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxFQUFFLHVCQUF1QixFQUFFLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxHQUFJLENBQ3pFO0FBRnNFLENBRXRFLENBQUMifQ==

/***/ }),

/***/ 760:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// EXTERNAL MODULE: ./utils/auth.ts + 1 modules
var auth = __webpack_require__(20);

// CONCATENATED MODULE: ./api/shop.ts


var fetchDataHotDeal = function (_a) {
    var _b = _a.limit, limit = _b === void 0 ? 20 : _b;
    return Object(restful_method["b" /* get */])({
        path: "/shop/hot_deal?limit=" + limit,
        description: 'Get data hot deal in home page',
        errorMesssage: "Can't get data. Please try again",
    });
};
var fetchDataHomePage = function () { return Object(restful_method["b" /* get */])({
    path: '/shop',
    description: 'Get group data in home page',
    errorMesssage: "Can't get latest boxs data. Please try again",
}); };
var fetchProductByCategory = function (idCategory, query) {
    if (query === void 0) { query = ''; }
    return Object(restful_method["b" /* get */])({
        path: "/browse_nodes/" + idCategory + ".json" + query,
        description: 'Fetch list product in category | DANH SÁCH SẢN PHẨM TRONG CATEGORY',
        errorMesssage: "Can't get list product by category. Please try again",
    });
};
var getProductDetail = function (idProduct) { return Object(restful_method["b" /* get */])({
    path: "/boxes/" + idProduct + ".json",
    description: 'Get Product Detail | LẤY CHI TIẾT SẢN PHẨM',
    errorMesssage: "Can't get product detail. Please try again",
}); };
;
var generateParams = function (key, value) { return value ? "&" + key + "=" + value : ''; };
var fetchRedeemBoxes = function (_a) {
    var _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 72 : _c, filter = _a.filter, sort = _a.sort;
    var query = "?page=" + page + "&per_page=" + perPage
        + generateParams('filter[brand]', filter && filter.brand)
        + generateParams('filter[category]', filter && filter.category)
        + generateParams('filter[coins_price]', filter && filter.coinsPrice)
        + generateParams('sort[coins_price]', filter && sort && sort.coinsPrice);
    return Object(restful_method["b" /* get */])({
        path: "/boxes/redeems" + query,
        description: 'Fetch list redeem boxes',
        errorMesssage: "Can't fetch data. Please try again",
    });
};
var addWaitList = function (_a) {
    var boxId = _a.boxId;
    var data = {
        csrf_token: Object(auth["c" /* getCsrfToken */])(),
    };
    return Object(restful_method["d" /* post */])({
        path: "/boxes/" + boxId + "/waitlist",
        data: data,
        description: 'Add item intowait list | BOXES - WAIT LIST',
        errorMesssage: "Can't Get listwait list. Please try again",
    });
};
var fetchFeedbackBoxes = function (_a) {
    var productId = _a.productId, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c;
    var query = "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/boxes/" + productId + "/feedbacks" + query,
        description: 'Fetch list feedbacks boxes',
        errorMesssage: "Can't fetch data. Please try again",
    });
};
var fetchSavingSetsBoxes = function (_a) {
    var productId = _a.productId, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c;
    var query = "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/boxes/" + productId + "/saving_sets" + query,
        description: 'Fetch list saving sets boxes',
        errorMesssage: "Can't fetch data. Please try again",
    });
};
var fetchMagazinesBoxes = function (_a) {
    var productId = _a.productId, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c;
    var query = "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/boxes/" + productId + "/magazines" + query,
        description: 'Fetch magazine boxes',
        errorMesssage: "Can't fetch data. Please try again",
    });
};
var fetchRelatedBoxes = function (_a) {
    var productId = _a.productId, _b = _a.limit, limit = _b === void 0 ? 10 : _b;
    var query = "?limit=" + limit;
    return Object(restful_method["b" /* get */])({
        path: "/boxes/" + productId + "/related_boxes" + query,
        description: 'Fetch related boxes',
        errorMesssage: "Can't fetch data. Please try again",
    });
};
var fetchMakeups = function (_a) {
    var boxId = _a.boxId, _b = _a.limit, limit = _b === void 0 ? 10 : _b;
    return Object(restful_method["b" /* get */])({
        path: "/makeups/" + boxId + "?limit=" + limit,
        description: 'Fetch makeups',
        errorMesssage: "Can't fetch data. Please try again",
    });
};
var fetchStoreBoxes = function (_a) {
    var productId = _a.productId;
    return Object(restful_method["b" /* get */])({
        path: "/boxes/" + productId + "/store_boxes",
        description: 'Fetch store boxes',
        errorMesssage: "Can't fetch store boxes. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hvcC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNob3AudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTdDLE1BQU0sQ0FBQyxJQUFNLGdCQUFnQixHQUFHLFVBQUMsRUFBYztRQUFaLGFBQVUsRUFBViwrQkFBVTtJQUFPLE9BQUEsR0FBRyxDQUFDO1FBQ3RELElBQUksRUFBRSwwQkFBd0IsS0FBTztRQUNyQyxXQUFXLEVBQUUsZ0NBQWdDO1FBQzdDLGFBQWEsRUFBRSxrQ0FBa0M7S0FDbEQsQ0FBQztBQUprRCxDQUlsRCxDQUFDO0FBRUgsTUFBTSxDQUFDLElBQU0saUJBQWlCLEdBQUcsY0FBTSxPQUFBLEdBQUcsQ0FBQztJQUN6QyxJQUFJLEVBQUUsT0FBTztJQUNiLFdBQVcsRUFBRSw2QkFBNkI7SUFDMUMsYUFBYSxFQUFFLDhDQUE4QztDQUM5RCxDQUFDLEVBSnFDLENBSXJDLENBQUM7QUFFSCxNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FDakMsVUFBQyxVQUFrQixFQUFFLEtBQVU7SUFBVixzQkFBQSxFQUFBLFVBQVU7SUFBSyxPQUFBLEdBQUcsQ0FBQztRQUN0QyxJQUFJLEVBQUUsbUJBQWlCLFVBQVUsYUFBUSxLQUFPO1FBQ2hELFdBQVcsRUFBRSxvRUFBb0U7UUFDakYsYUFBYSxFQUFFLHNEQUFzRDtLQUN0RSxDQUFDO0FBSmtDLENBSWxDLENBQUM7QUFFTCxNQUFNLENBQUMsSUFBTSxnQkFBZ0IsR0FDM0IsVUFBQyxTQUFpQixJQUFLLE9BQUEsR0FBRyxDQUFDO0lBQ3pCLElBQUksRUFBRSxZQUFVLFNBQVMsVUFBTztJQUNoQyxXQUFXLEVBQUUsNENBQTRDO0lBQ3pELGFBQWEsRUFBRSw0Q0FBNEM7Q0FDNUQsQ0FBQyxFQUpxQixDQUlyQixDQUFDO0FBYUosQ0FBQztBQUVGLElBQU0sY0FBYyxHQUFHLFVBQUMsR0FBRyxFQUFFLEtBQUssSUFBSyxPQUFBLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBSSxHQUFHLFNBQUksS0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQS9CLENBQStCLENBQUM7QUFFdkUsTUFBTSxDQUFDLElBQU0sZ0JBQWdCLEdBQzNCLFVBQUMsRUFBZ0U7UUFBOUQsWUFBUSxFQUFSLDZCQUFRLEVBQUUsZUFBWSxFQUFaLGlDQUFZLEVBQUUsa0JBQU0sRUFBRSxjQUFJO0lBRXJDLElBQU0sS0FBSyxHQUNULFdBQVMsSUFBSSxrQkFBYSxPQUFTO1VBQ2pDLGNBQWMsQ0FBQyxlQUFlLEVBQUUsTUFBTSxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUM7VUFDdkQsY0FBYyxDQUFDLGtCQUFrQixFQUFFLE1BQU0sSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDO1VBQzdELGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxNQUFNLElBQUksTUFBTSxDQUFDLFVBQVUsQ0FBQztVQUNsRSxjQUFjLENBQUMsbUJBQW1CLEVBQUUsTUFBTSxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQ3ZFO0lBRUgsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSxtQkFBaUIsS0FBTztRQUM5QixXQUFXLEVBQUUseUJBQXlCO1FBQ3RDLGFBQWEsRUFBRSxvQ0FBb0M7S0FDcEQsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUosTUFBTSxDQUFDLElBQU0sV0FBVyxHQUN0QixVQUFDLEVBQVM7UUFBUCxnQkFBSztJQUNOLElBQU0sSUFBSSxHQUFHO1FBQ1gsVUFBVSxFQUFFLFlBQVksRUFBRTtLQUMzQixDQUFDO0lBRUYsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNWLElBQUksRUFBRSxZQUFVLEtBQUssY0FBVztRQUNoQyxJQUFJLE1BQUE7UUFDSixXQUFXLEVBQUUsNENBQTRDO1FBQ3pELGFBQWEsRUFBRSwyQ0FBMkM7S0FDM0QsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUosTUFBTSxDQUFDLElBQU0sa0JBQWtCLEdBQzdCLFVBQUMsRUFBcUM7UUFBbkMsd0JBQVMsRUFBRSxZQUFRLEVBQVIsNkJBQVEsRUFBRSxlQUFZLEVBQVosaUNBQVk7SUFDbEMsSUFBTSxLQUFLLEdBQUcsV0FBUyxJQUFJLGtCQUFhLE9BQVMsQ0FBQztJQUVsRCxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLFlBQVUsU0FBUyxrQkFBYSxLQUFPO1FBQzdDLFdBQVcsRUFBRSw0QkFBNEI7UUFDekMsYUFBYSxFQUFFLG9DQUFvQztLQUNwRCxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSixNQUFNLENBQUMsSUFBTSxvQkFBb0IsR0FDL0IsVUFBQyxFQUFxQztRQUFuQyx3QkFBUyxFQUFFLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUNsQyxJQUFNLEtBQUssR0FBRyxXQUFTLElBQUksa0JBQWEsT0FBUyxDQUFDO0lBRWxELE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsWUFBVSxTQUFTLG9CQUFlLEtBQU87UUFDL0MsV0FBVyxFQUFFLDhCQUE4QjtRQUMzQyxhQUFhLEVBQUUsb0NBQW9DO0tBQ3BELENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQztBQUVKLE1BQU0sQ0FBQyxJQUFNLG1CQUFtQixHQUM5QixVQUFDLEVBQXFDO1FBQW5DLHdCQUFTLEVBQUUsWUFBUSxFQUFSLDZCQUFRLEVBQUUsZUFBWSxFQUFaLGlDQUFZO0lBQ2xDLElBQU0sS0FBSyxHQUFHLFdBQVMsSUFBSSxrQkFBYSxPQUFTLENBQUM7SUFFbEQsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSxZQUFVLFNBQVMsa0JBQWEsS0FBTztRQUM3QyxXQUFXLEVBQUUsc0JBQXNCO1FBQ25DLGFBQWEsRUFBRSxvQ0FBb0M7S0FDcEQsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUosTUFBTSxDQUFDLElBQU0saUJBQWlCLEdBQzVCLFVBQUMsRUFBeUI7UUFBdkIsd0JBQVMsRUFBRSxhQUFVLEVBQVYsK0JBQVU7SUFDdEIsSUFBTSxLQUFLLEdBQUcsWUFBVSxLQUFPLENBQUM7SUFFaEMsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSxZQUFVLFNBQVMsc0JBQWlCLEtBQU87UUFDakQsV0FBVyxFQUFFLHFCQUFxQjtRQUNsQyxhQUFhLEVBQUUsb0NBQW9DO0tBQ3BELENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQztBQUVKLE1BQU0sQ0FBQyxJQUFNLFlBQVksR0FBRyxVQUFDLEVBQXFCO1FBQW5CLGdCQUFLLEVBQUUsYUFBVSxFQUFWLCtCQUFVO0lBQU8sT0FBQSxHQUFHLENBQUM7UUFDekQsSUFBSSxFQUFFLGNBQVksS0FBSyxlQUFVLEtBQU87UUFDeEMsV0FBVyxFQUFFLGVBQWU7UUFDNUIsYUFBYSxFQUFFLG9DQUFvQztLQUNwRCxDQUFDO0FBSnFELENBSXJELENBQUM7QUFFSCxNQUFNLENBQUMsSUFBTSxlQUFlLEdBQUcsVUFBQyxFQUFhO1FBQVgsd0JBQVM7SUFBTyxPQUFBLEdBQUcsQ0FBQztRQUNwRCxJQUFJLEVBQUUsWUFBVSxTQUFTLGlCQUFjO1FBQ3ZDLFdBQVcsRUFBRSxtQkFBbUI7UUFDaEMsYUFBYSxFQUFFLDJDQUEyQztLQUMzRCxDQUFDO0FBSmdELENBSWhELENBQUMifQ==
// EXTERNAL MODULE: ./constants/api/shop.ts
var shop = __webpack_require__(13);

// CONCATENATED MODULE: ./action/shop.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fetchDataHomePageAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return fetchProductByCategoryAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "m", function() { return updateProductNameMobileAction; });
/* unused harmony export updateCategoryFilterStateAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "l", function() { return getProductDetailAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return fetchRedeemBoxesAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return addToWaitListAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return fetchFeedbackBoxesAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "j", function() { return fetchSavingSetsBoxesAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return fetchMagazinesBoxesAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return fetchRelatedBoxesAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return fetchDataHotDealAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return fetchMakeupsAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "k", function() { return fetchStoreBoxesAction; });


/** Get collection data in home page */
var fetchDataHomePageAction = function () { return function (dispatch, getState) {
    return dispatch({
        type: shop["b" /* FECTH_DATA_HOME_PAGE */],
        payload: { promise: fetchDataHomePage().then(function (res) { return res; }) },
    });
}; };
/** Get Product list by cagegory old version */
/*
export const fetchProductByCategoryAction =
  (categoryFilter: any) => {
    const [idCategory, params] = categoryFilterApiFormat(categoryFilter);

    return (dispatch, getState) => dispatch({
      type: FETCH_PRODUCT_BY_CATEGORY,
      payload: { promise: fetchProductByCategory(idCategory, params).then(res => res) },
      meta: { metaFilter: { idCategory, params } }
    });
  };
  */
/** Get Product list by cagegory new version */
var fetchProductByCategoryAction = function (_a) {
    var idCategory = _a.idCategory, searchQuery = _a.searchQuery;
    return function (dispatch, getState) { return dispatch({
        type: shop["g" /* FETCH_PRODUCT_BY_CATEGORY */],
        payload: { promise: fetchProductByCategory(idCategory, searchQuery).then(function (res) { return res; }) },
        meta: { metaFilter: { idCategory: idCategory, searchQuery: searchQuery } }
    }); };
};
/**  Update Product name cho Product detail trên Mobile */
var updateProductNameMobileAction = function (productName) { return ({
    type: shop["n" /* UPDATE_PRODUCT_NAME_MOBILE */],
    payload: productName
}); };
/**  Update filter for category */
var updateCategoryFilterStateAction = function (categoryFilter) { return ({
    type: shop["m" /* UPDATE_CATEGORY_FILTER_STATE */],
    payload: categoryFilter
}); };
/** Get product detail */
var getProductDetailAction = function (productId) {
    return function (dispatch, getState) {
        return dispatch({
            type: shop["l" /* GET_PRODUCT_DETAIL */],
            payload: { promise: getProductDetail(productId).then(function (res) { return res; }) },
            meta: { productId: productId }
        });
    };
};
/** Fetch redeem boxes list */
var fetchRedeemBoxesAction = function (_a) {
    var page = _a.page, _b = _a.perPage, perPage = _b === void 0 ? 72 : _b, filter = _a.filter, sort = _a.sort;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["h" /* FETCH_REDEEM_BOXES */],
            payload: { promise: fetchRedeemBoxes({ page: page, perPage: perPage, filter: filter, sort: sort }).then(function (res) { return res; }) },
            meta: { page: page, perPage: perPage }
        });
    };
};
/** Add product into wait list */
var addToWaitListAction = function (_a) {
    var boxId = _a.boxId, _b = _a.slug, slug = _b === void 0 ? '' : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["a" /* ADD_WAIT_LIST */],
            payload: { promise: addWaitList({ boxId: boxId }).then(function (res) { return res; }) },
            dispatch: dispatch,
            meta: { slug: slug }
        });
    };
};
/**
 * Fetch feebbacks boxes list by id or slug of product
 *
 * @param{string} id or slug of product
 * @param{number} page
 * @param{string} perPage
 */
var fetchFeedbackBoxesAction = function (_a) {
    var productId = _a.productId, page = _a.page, _b = _a.perPage, perPage = _b === void 0 ? 10 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["d" /* FETCH_FEEDBACK_BOXES */],
            payload: { promise: fetchFeedbackBoxes({ productId: productId, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { productId: productId, page: page, perPage: perPage }
        });
    };
};
/**
 * Fetch saving sets boxes list by id or slug of product
 *
 * @param{string} id or slug of product
 * @param{number} page
 * @param{string} perPage
 */
var fetchSavingSetsBoxesAction = function (_a) {
    var productId = _a.productId, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["j" /* FETCH_SAVING_SETS_BOXES */],
            payload: { promise: fetchSavingSetsBoxes({ productId: productId, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { productId: productId, page: page, perPage: perPage }
        });
    };
};
/**
 * Fetch magazine boxes list by id or slug of product
 *
 * @param{string} id or slug of product
 * @param{number} page
 * @param{number} perPage
 */
var fetchMagazinesBoxesAction = function (_a) {
    var productId = _a.productId, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["e" /* FETCH_MAGAZINES_BOXES */],
            payload: { promise: fetchMagazinesBoxes({ productId: productId, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { productId: productId, page: page, perPage: perPage }
        });
    };
};
/**
* Fetch related boxes list by id or slug of product
*
* @param{string} id or slug of product
* @param{limit} number
*/
var fetchRelatedBoxesAction = function (_a) {
    var productId = _a.productId, _b = _a.limit, limit = _b === void 0 ? 10 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["i" /* FETCH_RELATED_BOXES */],
            payload: { promise: fetchRelatedBoxes({ productId: productId, limit: limit }).then(function (res) { return res; }) },
            meta: { productId: productId }
        });
    };
};
var fetchDataHotDealAction = function (_a) {
    var limit = _a.limit;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["c" /* FECTH_DATA_HOT_DEAL */],
            payload: { promise: fetchDataHotDeal({ limit: limit }).then(function (res) { return res; }) },
        });
    };
};
/**
* Fetch makeups by id or slug of product
*
* @param{string} id or slug of product
* @param{limit} number
*/
var fetchMakeupsAction = function (_a) {
    var boxId = _a.boxId, _b = _a.limit, limit = _b === void 0 ? 10 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["f" /* FETCH_MAKEUPS */],
            payload: { promise: fetchMakeups({ boxId: boxId, limit: limit }).then(function (res) { return res; }) },
            meta: { boxId: boxId }
        });
    };
};
/**
* Fetch store boxes of box detail by product id
*
* @param{string} id or slug of product
*/
var fetchStoreBoxesAction = function (_a) {
    var productId = _a.productId;
    return function (dispatch, getState) {
        return dispatch({
            type: shop["k" /* FETCH_STORE_BOXES */],
            payload: { promise: fetchStoreBoxes({ productId: productId }).then(function (res) { return res; }) },
            meta: { productId: productId }
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hvcC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNob3AudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUEsT0FBTyxFQUNMLGdCQUFnQixFQUNoQixzQkFBc0IsRUFDdEIsaUJBQWlCLEVBQ2pCLGdCQUFnQixFQUVoQixnQkFBZ0IsRUFDaEIsV0FBVyxFQUNYLGtCQUFrQixFQUNsQixvQkFBb0IsRUFDcEIsbUJBQW1CLEVBQ25CLGlCQUFpQixFQUNqQixZQUFZLEVBQ1osZUFBZSxFQUNoQixNQUFNLGFBQWEsQ0FBQztBQUVyQixPQUFPLEVBQ0wsbUJBQW1CLEVBQ25CLG9CQUFvQixFQUNwQix5QkFBeUIsRUFDekIsNEJBQTRCLEVBQzVCLDBCQUEwQixFQUMxQixrQkFBa0IsRUFDbEIsa0JBQWtCLEVBQ2xCLGFBQWEsRUFDYixvQkFBb0IsRUFDcEIsdUJBQXVCLEVBQ3ZCLHFCQUFxQixFQUNyQixtQkFBbUIsRUFDbkIsYUFBYSxFQUNiLGlCQUFpQixFQUNsQixNQUFNLHVCQUF1QixDQUFDO0FBRS9CLHVDQUF1QztBQUN2QyxNQUFNLENBQUMsSUFBTSx1QkFBdUIsR0FDbEMsY0FBTSxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7SUFDdkIsT0FBQSxRQUFRLENBQUM7UUFDUCxJQUFJLEVBQUUsb0JBQW9CO1FBQzFCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtLQUMzRCxDQUFDO0FBSEYsQ0FHRSxFQUpFLENBSUYsQ0FBQztBQUVQLCtDQUErQztBQUMvQzs7Ozs7Ozs7Ozs7SUFXSTtBQUVKLCtDQUErQztBQUMvQyxNQUFNLENBQUMsSUFBTSw0QkFBNEIsR0FBRyxVQUFDLEVBQTJCO1FBQXpCLDBCQUFVLEVBQUUsNEJBQVc7SUFFcEUsTUFBTSxDQUFDLFVBQUMsUUFBUSxFQUFFLFFBQVEsSUFBSyxPQUFBLFFBQVEsQ0FBQztRQUN0QyxJQUFJLEVBQUUseUJBQXlCO1FBQy9CLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxzQkFBc0IsQ0FBQyxVQUFVLEVBQUUsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1FBQ3RGLElBQUksRUFBRSxFQUFFLFVBQVUsRUFBRSxFQUFFLFVBQVUsWUFBQSxFQUFFLFdBQVcsYUFBQSxFQUFFLEVBQUU7S0FDbEQsQ0FBQyxFQUo2QixDQUk3QixDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUYsMERBQTBEO0FBQzFELE1BQU0sQ0FBQyxJQUFNLDZCQUE2QixHQUN4QyxVQUFDLFdBQWdCLElBQUssT0FBQSxDQUFDO0lBQ3JCLElBQUksRUFBRSwwQkFBMEI7SUFDaEMsT0FBTyxFQUFFLFdBQVc7Q0FDckIsQ0FBQyxFQUhvQixDQUdwQixDQUFDO0FBRUwsa0NBQWtDO0FBQ2xDLE1BQU0sQ0FBQyxJQUFNLCtCQUErQixHQUMxQyxVQUFDLGNBQW1CLElBQUssT0FBQSxDQUFDO0lBQ3hCLElBQUksRUFBRSw0QkFBNEI7SUFDbEMsT0FBTyxFQUFFLGNBQWM7Q0FDeEIsQ0FBQyxFQUh1QixDQUd2QixDQUFDO0FBRUwseUJBQXlCO0FBQ3pCLE1BQU0sQ0FBQyxJQUFNLHNCQUFzQixHQUNqQyxVQUFDLFNBQWlCO0lBQ2hCLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxrQkFBa0I7WUFDeEIsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUNsRSxJQUFJLEVBQUUsRUFBRSxTQUFTLFdBQUEsRUFBRTtTQUNwQixDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUVULDhCQUE4QjtBQUM5QixNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FDakMsVUFBQyxFQUE0RDtRQUExRCxjQUFJLEVBQUUsZUFBWSxFQUFaLGlDQUFZLEVBQUUsa0JBQU0sRUFBRSxjQUFJO0lBQ2pDLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxrQkFBa0I7WUFDeEIsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLGdCQUFnQixDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsTUFBTSxRQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN4RixJQUFJLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRTtTQUN4QixDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUVULGlDQUFpQztBQUNqQyxNQUFNLENBQUMsSUFBTSxtQkFBbUIsR0FDOUIsVUFBQyxFQUFvQjtRQUFsQixnQkFBSyxFQUFFLFlBQVMsRUFBVCw4QkFBUztJQUNqQixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsYUFBYTtZQUNuQixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsV0FBVyxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUM3RCxRQUFRLFVBQUE7WUFDUixJQUFJLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRTtTQUNmLENBQUM7SUFMRixDQUtFO0FBTkosQ0FNSSxDQUFDO0FBRVQ7Ozs7OztHQU1HO0FBQ0gsTUFBTSxDQUFDLElBQU0sd0JBQXdCLEdBQ25DLFVBQUMsRUFBaUM7UUFBL0Isd0JBQVMsRUFBRSxjQUFJLEVBQUUsZUFBWSxFQUFaLGlDQUFZO0lBQzlCLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxvQkFBb0I7WUFDMUIsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLGtCQUFrQixDQUFDLEVBQUUsU0FBUyxXQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN2RixJQUFJLEVBQUUsRUFBRSxTQUFTLFdBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRTtTQUNuQyxDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUVUOzs7Ozs7R0FNRztBQUNILE1BQU0sQ0FBQyxJQUFNLDBCQUEwQixHQUNyQyxVQUFDLEVBQXFDO1FBQW5DLHdCQUFTLEVBQUUsWUFBUSxFQUFSLDZCQUFRLEVBQUUsZUFBWSxFQUFaLGlDQUFZO0lBQ2xDLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSx1QkFBdUI7WUFDN0IsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLG9CQUFvQixDQUFDLEVBQUUsU0FBUyxXQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN6RixJQUFJLEVBQUUsRUFBRSxTQUFTLFdBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRTtTQUNuQyxDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUVUOzs7Ozs7R0FNRztBQUNILE1BQU0sQ0FBQyxJQUFNLHlCQUF5QixHQUNwQyxVQUFDLEVBQXFDO1FBQW5DLHdCQUFTLEVBQUUsWUFBUSxFQUFSLDZCQUFRLEVBQUUsZUFBWSxFQUFaLGlDQUFZO0lBQ2xDLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxxQkFBcUI7WUFDM0IsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLG1CQUFtQixDQUFDLEVBQUUsU0FBUyxXQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN4RixJQUFJLEVBQUUsRUFBRSxTQUFTLFdBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRTtTQUNuQyxDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUVUOzs7OztFQUtFO0FBQ0YsTUFBTSxDQUFDLElBQU0sdUJBQXVCLEdBQ2xDLFVBQUMsRUFBeUI7UUFBdkIsd0JBQVMsRUFBRSxhQUFVLEVBQVYsK0JBQVU7SUFDdEIsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLG1CQUFtQjtZQUN6QixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsaUJBQWlCLENBQUMsRUFBRSxTQUFTLFdBQUEsRUFBRSxLQUFLLE9BQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQzlFLElBQUksRUFBRSxFQUFFLFNBQVMsV0FBQSxFQUFFO1NBQ3BCLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQsTUFBTSxDQUFDLElBQU0sc0JBQXNCLEdBQUcsVUFBQyxFQUFTO1FBQVAsZ0JBQUs7SUFDNUMsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLG1CQUFtQjtZQUN6QixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1NBQ25FLENBQUM7SUFIRixDQUdFO0FBSkosQ0FJSSxDQUFDO0FBRVA7Ozs7O0VBS0U7QUFDRixNQUFNLENBQUMsSUFBTSxrQkFBa0IsR0FDN0IsVUFBQyxFQUFxQjtRQUFuQixnQkFBSyxFQUFFLGFBQVUsRUFBViwrQkFBVTtJQUNsQixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsYUFBYTtZQUNuQixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsWUFBWSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUNyRSxJQUFJLEVBQUUsRUFBRSxLQUFLLE9BQUEsRUFBRTtTQUNoQixDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUVUOzs7O0VBSUU7QUFDRixNQUFNLENBQUMsSUFBTSxxQkFBcUIsR0FBRyxVQUFDLEVBQWE7UUFBWCx3QkFBUztJQUMvQyxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsaUJBQWlCO1lBQ3ZCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxlQUFlLENBQUMsRUFBRSxTQUFTLFdBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQ3JFLElBQUksRUFBRSxFQUFFLFNBQVMsV0FBQSxFQUFFO1NBQ3BCLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDIn0=

/***/ }),

/***/ 761:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// CONCATENATED MODULE: ./api/magazine.ts

;
var fetchMagazineList = function (_a) {
    var _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c, _d = _a.type, type = _d === void 0 ? 'default' : _d;
    var query = "?page=" + page + "&per_page=" + perPage + "&filter[post_type]=" + type;
    return Object(restful_method["b" /* get */])({
        path: "/magazines" + query,
        description: 'Get magazine list',
        errorMesssage: "Can't get magazine list. Please try again",
    });
};
var fetchMagazineDashboard = function () { return Object(restful_method["b" /* get */])({
    path: "/magazines/dashboard",
    description: 'Get magazine dashboard list',
    errorMesssage: "Can't get magazine dashboard list. Please try again",
}); };
/**
 * Fetch magazine category by slug (id)
 */
var fetchMagazineCategory = function (_a) {
    var slug = _a.slug, page = _a.page, perPage = _a.perPage;
    var query = slug + "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/category/" + query,
        description: 'Get magazine category by slug (id)',
        errorMesssage: "Can't get magazine category by slug (id). Please try again",
    });
};
/**
* Fetch magazine by slug (id)
*/
var fetchMagazineBySlug = function (_a) {
    var slug = _a.slug;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/" + slug,
        description: 'Get magazine by slug (id)',
        errorMesssage: "Can't get magazine by slug (id). Please try again",
    });
};
/**
* Fetch magazine related blogs by slug (id)
*/
var fetchMagazineRelatedBlog = function (_a) {
    var slug = _a.slug;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/" + slug + "/related_blogs",
        description: 'Get magazine related blogs by slug (id)',
        errorMesssage: "Can't get magazine related blogs by slug (id). Please try again",
    });
};
/**
* Fetch magazine related boxes by slug (id)
*/
var fetchMagazineRelatedBox = function (_a) {
    var slug = _a.slug, limit = _a.limit;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/" + slug + "/related_boxes?limit=" + limit,
        description: 'Get magazine related boxes by slug (id)',
        errorMesssage: "Can't get magazine related boxes by slug (id). Please try again",
    });
};
/**
* Fetch magazine by tag name
*/
var fetchMagazineByTagName = function (_a) {
    var slug = _a.slug, page = _a.page, perPage = _a.perPage;
    var query = slug + "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/tag/" + query,
        description: 'Get magazine by tag name',
        errorMesssage: "Can't get magazine by tag name. Please try again",
    });
};
/**
* Fetch search magazine by keyword
*/
var fetchMagazineByKeyword = function (_a) {
    var keyword = _a.keyword, page = _a.page, perPage = _a.perPage;
    var query = keyword + "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/search/" + query,
        description: 'Get search magazine by keyword',
        errorMesssage: "Can't get search magazine by keyword. Please try again",
    });
};
/**
* Fetch search suggestion magazine by keyword
*/
var fetchMagazineSuggestionByKeyword = function (_a) {
    var keyword = _a.keyword, limit = _a.limit;
    var query = keyword + "?limit=" + limit;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/search_suggestion/" + query,
        description: 'Get search suggestion magazine by keyword',
        errorMesssage: "Can't get search suggestion magazine by keyword. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFnYXppbmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtYWdhemluZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFNOUMsQ0FBQztBQUVGLE1BQU0sQ0FBQyxJQUFNLGlCQUFpQixHQUM1QixVQUFDLEVBQXFFO1FBQW5FLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWSxFQUFFLFlBQWdCLEVBQWhCLHFDQUFnQjtJQUN6QyxJQUFNLEtBQUssR0FBRyxXQUFTLElBQUksa0JBQWEsT0FBTywyQkFBc0IsSUFBTSxDQUFDO0lBRTVFLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsZUFBYSxLQUFPO1FBQzFCLFdBQVcsRUFBRSxtQkFBbUI7UUFDaEMsYUFBYSxFQUFFLDJDQUEyQztLQUMzRCxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSixNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FBRyxjQUFNLE9BQUEsR0FBRyxDQUFDO0lBQzlDLElBQUksRUFBRSxzQkFBc0I7SUFDNUIsV0FBVyxFQUFFLDZCQUE2QjtJQUMxQyxhQUFhLEVBQUUscURBQXFEO0NBQ3JFLENBQUMsRUFKMEMsQ0FJMUMsQ0FBQztBQUVIOztHQUVHO0FBQ0gsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQ2hDLFVBQUMsRUFBdUI7UUFBckIsY0FBSSxFQUFFLGNBQUksRUFBRSxvQkFBTztJQUNwQixJQUFNLEtBQUssR0FBTSxJQUFJLGNBQVMsSUFBSSxrQkFBYSxPQUFTLENBQUM7SUFDekQsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSx5QkFBdUIsS0FBTztRQUNwQyxXQUFXLEVBQUUsb0NBQW9DO1FBQ2pELGFBQWEsRUFBRSw0REFBNEQ7S0FDNUUsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUo7O0VBRUU7QUFDRixNQUFNLENBQUMsSUFBTSxtQkFBbUIsR0FBRyxVQUFDLEVBQVE7UUFBTixjQUFJO0lBQU8sT0FBQSxHQUFHLENBQUM7UUFDbkQsSUFBSSxFQUFFLGdCQUFjLElBQU07UUFDMUIsV0FBVyxFQUFFLDJCQUEyQjtRQUN4QyxhQUFhLEVBQUUsbURBQW1EO0tBQ25FLENBQUM7QUFKK0MsQ0FJL0MsQ0FBQztBQUVIOztFQUVFO0FBQ0YsTUFBTSxDQUFDLElBQU0sd0JBQXdCLEdBQUcsVUFBQyxFQUFRO1FBQU4sY0FBSTtJQUFPLE9BQUEsR0FBRyxDQUFDO1FBQ3hELElBQUksRUFBRSxnQkFBYyxJQUFJLG1CQUFnQjtRQUN4QyxXQUFXLEVBQUUseUNBQXlDO1FBQ3RELGFBQWEsRUFBRSxpRUFBaUU7S0FDakYsQ0FBQztBQUpvRCxDQUlwRCxDQUFDO0FBRUg7O0VBRUU7QUFDRixNQUFNLENBQUMsSUFBTSx1QkFBdUIsR0FBRyxVQUFDLEVBQWU7UUFBYixjQUFJLEVBQUUsZ0JBQUs7SUFBTyxPQUFBLEdBQUcsQ0FBQztRQUM5RCxJQUFJLEVBQUUsZ0JBQWMsSUFBSSw2QkFBd0IsS0FBTztRQUN2RCxXQUFXLEVBQUUseUNBQXlDO1FBQ3RELGFBQWEsRUFBRSxpRUFBaUU7S0FDakYsQ0FBQztBQUowRCxDQUkxRCxDQUFDO0FBRUg7O0VBRUU7QUFDRixNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FDakMsVUFBQyxFQUF1QjtRQUFyQixjQUFJLEVBQUUsY0FBSSxFQUFFLG9CQUFPO0lBQ3BCLElBQU0sS0FBSyxHQUFNLElBQUksY0FBUyxJQUFJLGtCQUFhLE9BQVMsQ0FBQztJQUN6RCxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLG9CQUFrQixLQUFPO1FBQy9CLFdBQVcsRUFBRSwwQkFBMEI7UUFDdkMsYUFBYSxFQUFFLGtEQUFrRDtLQUNsRSxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSjs7RUFFRTtBQUNGLE1BQU0sQ0FBQyxJQUFNLHNCQUFzQixHQUNqQyxVQUFDLEVBQTBCO1FBQXhCLG9CQUFPLEVBQUUsY0FBSSxFQUFFLG9CQUFPO0lBQ3ZCLElBQU0sS0FBSyxHQUFNLE9BQU8sY0FBUyxJQUFJLGtCQUFhLE9BQVMsQ0FBQztJQUM1RCxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLHVCQUFxQixLQUFPO1FBQ2xDLFdBQVcsRUFBRSxnQ0FBZ0M7UUFDN0MsYUFBYSxFQUFFLHdEQUF3RDtLQUN4RSxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSjs7RUFFRTtBQUNGLE1BQU0sQ0FBQyxJQUFNLGdDQUFnQyxHQUMzQyxVQUFDLEVBQWtCO1FBQWhCLG9CQUFPLEVBQUUsZ0JBQUs7SUFDZixJQUFNLEtBQUssR0FBTSxPQUFPLGVBQVUsS0FBTyxDQUFDO0lBQzFDLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsa0NBQWdDLEtBQU87UUFDN0MsV0FBVyxFQUFFLDJDQUEyQztRQUN4RCxhQUFhLEVBQUUsbUVBQW1FO0tBQ25GLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQyJ9
// EXTERNAL MODULE: ./constants/api/magazine.ts
var magazine = __webpack_require__(23);

// CONCATENATED MODULE: ./action/magazine.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return fetchMagazineListAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return fetchMagazineDashboardAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return fetchMagazineCategoryAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fetchMagazineBySlugAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return fetchMagazineRelatedBlogAction; });
/* unused harmony export fetchMagazineRelatedBoxAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fetchMagazineByTagNameAction; });
/* unused harmony export showHideMagazineSearchAction */
/* unused harmony export fetchMagazineByKeywordAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return fetchMagazineSuggestionByKeywordAction; });


/**
 * Fetch love list by filter paramsx
 *
 * @param {number} page ex: 1, 2, 3, 4
 * @param {number} perPage ex: 10, 15, 20
 * @param {'default' | 'video-post' | 'quote-post'} type
 */
var fetchMagazineListAction = function (_a) {
    var page = _a.page, perPage = _a.perPage, type = _a.type;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["f" /* FETCH_MAGAZINE_LIST */],
            payload: { promise: fetchMagazineList({ page: page, perPage: perPage, type: type }).then(function (res) { return res; }) },
            meta: { page: page, perPage: perPage, type: type }
        });
    };
};
var fetchMagazineDashboardAction = function () {
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["e" /* FETCH_MAGAZINE_DASHBOARD */],
            payload: { promise: fetchMagazineDashboard().then(function (res) { return res; }) },
            meta: {}
        });
    };
};
/**
* Fetch magazine category by slug (id)
*
* @param {string} slug ex: makeup
* @param {number} page ex: 1, 2, 3, 4
* @param {number} perPage ex: 10, 15, 20
*/
var fetchMagazineCategoryAction = function (_a) {
    var slug = _a.slug, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["d" /* FETCH_MAGAZINE_CATEGORY */],
            payload: { promise: fetchMagazineCategory({ slug: slug, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { slug: slug, page: page, perPage: perPage }
        });
    };
};
/**
* Fetch magazine by slug (id)
*
* @param {string} slug ex: makeup
*/
var fetchMagazineBySlugAction = function (_a) {
    var slug = _a.slug;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["c" /* FETCH_MAGAZINE_BY_SLUG */],
            payload: { promise: fetchMagazineBySlug({ slug: slug }).then(function (res) { return res; }) },
            meta: { slug: slug }
        });
    };
};
/**
* Fetch magazine related blogs by slug (id)
*
* @param {string} slug ex: makeup
*/
var fetchMagazineRelatedBlogAction = function (_a) {
    var slug = _a.slug;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["g" /* FETCH_MAGAZINE_RELATED_BLOGS */],
            payload: { promise: fetchMagazineRelatedBlog({ slug: slug }).then(function (res) { return res; }) },
            meta: { slug: slug }
        });
    };
};
/**
* Fetch magazine related boxes by slug (id)
*
* @param {string} slug ex: makeup
* @param {number} limit ex: 1, 2, 3, 4
*/
var fetchMagazineRelatedBoxAction = function (_a) {
    var slug = _a.slug, _b = _a.limit, limit = _b === void 0 ? 6 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["h" /* FETCH_MAGAZINE_RELATED_BOXES */],
            payload: { promise: fetchMagazineRelatedBox({ slug: slug, limit: limit }).then(function (res) { return res; }) },
            meta: { slug: slug }
        });
    };
};
/**
* Fetch magazine by tag name
*
* @param {string} tagName ex: halio
* @param {number} page ex: 1, 2, 3, 4
* @param {number} perPage ex: 10, 15, 20
*/
var fetchMagazineByTagNameAction = function (_a) {
    var slug = _a.slug, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["j" /* FETCH_MAGAZINE_TAG */],
            payload: { promise: fetchMagazineByTagName({ slug: slug, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { slug: slug, page: page, perPage: perPage }
        });
    };
};
/**
* Show / Hide search suggest with state param
*
* @param {boolean} state
*/
var showHideMagazineSearchAction = function (state) {
    if (state === void 0) { state = false; }
    return ({
        type: magazine["a" /* DISPLAY_MAGAZINE_SEARCH_SUGGESTION */],
        payload: state
    });
};
/**
* Fetch magazine by keyword
*
* @param {string} keyword ex: halio
* @param {number} page ex: 1, 2, 3, 4
* @param {number} perPage ex: 10, 15, 20
*/
var fetchMagazineByKeywordAction = function (_a) {
    var keyword = _a.keyword, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["b" /* FETCH_MAGAZINE_BY_KEWORD */],
            payload: { promise: fetchMagazineByKeyword({ keyword: keyword, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { keyword: keyword }
        });
    };
};
/**
* Fetch magazine suggestion by keyword
*
* @param {string} keyword ex: halio
* @param {number} limit ex: 1, 2, 3, 4
*/
var fetchMagazineSuggestionByKeywordAction = function (_a) {
    var keyword = _a.keyword, _b = _a.limit, limit = _b === void 0 ? 10 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["i" /* FETCH_MAGAZINE_SUGGESTION_BY_KEWORD */],
            payload: { promise: fetchMagazineSuggestionByKeyword({ keyword: keyword, limit: limit }).then(function (res) { return res; }) },
            meta: { keyword: keyword }
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFnYXppbmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtYWdhemluZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBRUwsaUJBQWlCLEVBQ2pCLHNCQUFzQixFQUN0QixxQkFBcUIsRUFDckIsbUJBQW1CLEVBQ25CLHdCQUF3QixFQUN4Qix1QkFBdUIsRUFDdkIsc0JBQXNCLEVBQ3RCLHNCQUFzQixFQUN0QixnQ0FBZ0MsRUFDakMsTUFBTSxpQkFBaUIsQ0FBQztBQUV6QixPQUFPLEVBQ0wsbUJBQW1CLEVBQ25CLHdCQUF3QixFQUN4Qix1QkFBdUIsRUFDdkIsc0JBQXNCLEVBQ3RCLDRCQUE0QixFQUM1Qiw0QkFBNEIsRUFDNUIsa0JBQWtCLEVBQ2xCLGtDQUFrQyxFQUNsQyx3QkFBd0IsRUFDeEIsbUNBQW1DLEVBQ3BDLE1BQU0sMkJBQTJCLENBQUM7QUFFbkM7Ozs7OztHQU1HO0FBQ0gsTUFBTSxDQUFDLElBQU0sdUJBQXVCLEdBQ2xDLFVBQUMsRUFBZ0Q7UUFBOUMsY0FBSSxFQUFFLG9CQUFPLEVBQUUsY0FBSTtJQUNwQixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsbUJBQW1CO1lBQ3pCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDakYsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUU7U0FDOUIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVCxNQUFNLENBQUMsSUFBTSw0QkFBNEIsR0FDdkM7SUFDRSxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsd0JBQXdCO1lBQzlCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUMvRCxJQUFJLEVBQUUsRUFBRTtTQUNULENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7OztFQU1FO0FBQ0YsTUFBTSxDQUFDLElBQU0sMkJBQTJCLEdBQ3RDLFVBQUMsRUFBZ0M7UUFBOUIsY0FBSSxFQUFFLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUM3QixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsdUJBQXVCO1lBQzdCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxxQkFBcUIsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDckYsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDOUIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0seUJBQXlCLEdBQ3BDLFVBQUMsRUFBUTtRQUFOLGNBQUk7SUFDTCxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsc0JBQXNCO1lBQzVCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDcEUsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUU7U0FDZixDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUdUOzs7O0VBSUU7QUFDRixNQUFNLENBQUMsSUFBTSw4QkFBOEIsR0FDekMsVUFBQyxFQUFRO1FBQU4sY0FBSTtJQUNMLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSw0QkFBNEI7WUFDbEMsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLHdCQUF3QixDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN6RSxJQUFJLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRTtTQUNmLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7O0VBS0U7QUFDRixNQUFNLENBQUMsSUFBTSw2QkFBNkIsR0FDeEMsVUFBQyxFQUFtQjtRQUFqQixjQUFJLEVBQUUsYUFBUyxFQUFULDhCQUFTO0lBQ2hCLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSw0QkFBNEI7WUFDbEMsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLHVCQUF1QixDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUMvRSxJQUFJLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRTtTQUNmLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBR1Q7Ozs7OztFQU1FO0FBQ0YsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsRUFBZ0M7UUFBOUIsY0FBSSxFQUFFLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUM3QixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsa0JBQWtCO1lBQ3hCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxzQkFBc0IsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDdEYsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDOUIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsS0FBYTtJQUFiLHNCQUFBLEVBQUEsYUFBYTtJQUFLLE9BQUEsQ0FBQztRQUNsQixJQUFJLEVBQUUsa0NBQWtDO1FBQ3hDLE9BQU8sRUFBRSxLQUFLO0tBQ2YsQ0FBQztBQUhpQixDQUdqQixDQUFDO0FBRUw7Ozs7OztFQU1FO0FBQ0YsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsRUFBbUM7UUFBakMsb0JBQU8sRUFBRSxZQUFRLEVBQVIsNkJBQVEsRUFBRSxlQUFZLEVBQVosaUNBQVk7SUFDaEMsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLHdCQUF3QjtZQUM5QixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsc0JBQXNCLENBQUMsRUFBRSxPQUFPLFNBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQ3pGLElBQUksRUFBRSxFQUFFLE9BQU8sU0FBQSxFQUFFO1NBQ2xCLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7O0VBS0U7QUFDRixNQUFNLENBQUMsSUFBTSxzQ0FBc0MsR0FDakQsVUFBQyxFQUF1QjtRQUFyQixvQkFBTyxFQUFFLGFBQVUsRUFBViwrQkFBVTtJQUNwQixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsbUNBQW1DO1lBQ3pDLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxnQ0FBZ0MsQ0FBQyxFQUFFLE9BQU8sU0FBQSxFQUFFLEtBQUssT0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDM0YsSUFBSSxFQUFFLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDbEIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUMifQ==

/***/ }),

/***/ 765:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// EXTERNAL MODULE: ./utils/auth.ts + 1 modules
var auth = __webpack_require__(20);

// CONCATENATED MODULE: ./api/activity-feed.ts


;
var fecthActivityFeedList = function (_a) {
    var _b = _a.userId, userId = _b === void 0 ? 0 : _b, _c = _a.limit, limit = _c === void 0 ? 20 : _c, _d = _a.currentId, currentId = _d === void 0 ? 0 : _d, _e = _a.feedType, feedType = _e === void 0 ? '' : _e;
    var query = "?limit=" + limit
        + (!!currentId ? "&current_id=" + currentId : '')
        + (!!userId ? "&user_id=" + userId : '')
        + (!!feedType ? "&feed_type=" + feedType : '');
    return Object(restful_method["b" /* get */])({
        path: "/activity_feeds" + query,
        description: "Fetch list activity feed",
        errorMesssage: "Can't fetch data activity feeds. Please try again",
    });
};
;
var fecthActivityFeedCommentList = function (_a) {
    var id = _a.id, lastCommentId = _a.lastCommentId, page = _a.page, perPage = _a.perPage;
    var lastCommentParams = 'undefined' !== typeof lastCommentId ? "&last_comment_id=" + lastCommentId : '';
    var query = "?&page=" + page + "&per_page=" + perPage + lastCommentParams;
    return Object(restful_method["b" /* get */])({
        path: "/activity_feeds/" + id + "/comments" + query,
        description: "Fetch activity feed comment list",
        errorMesssage: "Can't fetch activity feed comment list. Please try again",
    });
};
;
var addActivityFeedComment = function (_a) {
    var id = _a.id, content = _a.content, lastCommentId = _a.lastCommentId;
    return Object(restful_method["d" /* post */])({
        path: "/activity_feeds/" + id + "/comments",
        data: {
            csrf_token: Object(auth["c" /* getCsrfToken */])(),
            content: content,
            last_comment_id: lastCommentId,
        },
        description: "Add activity feed comment",
        errorMesssage: "Can't add activity feed comment. Please try again",
    });
};
;
var addActivityFeedLike = function (_a) {
    var id = _a.id;
    return Object(restful_method["d" /* post */])({
        path: "/activity_feeds/" + id + "/like",
        data: {
            csrf_token: Object(auth["c" /* getCsrfToken */])()
        },
        description: "Add activity feed like",
        errorMesssage: "Can't add activity feed like. Please try again",
    });
};
;
var deleteActivityFeedLike = function (_a) {
    var id = _a.id;
    return Object(restful_method["a" /* del */])({
        path: "/activity_feeds/" + id + "/unlike",
        data: { csrf_token: Object(auth["c" /* getCsrfToken */])() },
        description: "Delete activity feed like",
        errorMesssage: "Can't delete activity feed like. Please try again",
    });
};
var fetchActivityFeedDetail = function (_a) {
    var feedId = _a.feedId;
    return Object(restful_method["b" /* get */])({
        path: "/activity_feeds/" + feedId,
        description: "Fetch activity feed detail",
        errorMesssage: "Can't fetch activity feed detail. Please try again",
    });
};
/**
 * Get Collection (Top feed)
 *
 * @param {number} page ex: 1
 * @param {number} perPage ex: 12
 */
var getCollection = function (_a) {
    var _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    var query = "?&page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/community" + query,
        description: "Get collection",
        errorMesssage: "Can't get collection. Please try again",
    });
};
/**
* Get Collection detail (Top feed)
*
* @param {number} id ex: 1
*/
var getCollectionDetail = function (_a) {
    var id = _a.id;
    return Object(restful_method["b" /* get */])({
        path: "/community/" + id,
        description: "Get collection detail",
        errorMesssage: "Can't get collection detail. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0aXZpdHktZmVlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFjdGl2aXR5LWZlZWQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDMUQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQVE1QyxDQUFDO0FBRUYsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQ2hDLFVBQUMsRUFBZ0Y7UUFBOUUsY0FBVSxFQUFWLCtCQUFVLEVBQUUsYUFBVSxFQUFWLCtCQUFVLEVBQUUsaUJBQWEsRUFBYixrQ0FBYSxFQUFFLGdCQUFhLEVBQWIsa0NBQWE7SUFDckQsSUFBTSxLQUFLLEdBQUcsWUFBVSxLQUFPO1VBQzNCLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsaUJBQWUsU0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7VUFDL0MsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxjQUFZLE1BQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1VBQ3RDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsZ0JBQWMsUUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUVqRCxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLG9CQUFrQixLQUFPO1FBQy9CLFdBQVcsRUFBRSwwQkFBMEI7UUFDdkMsYUFBYSxFQUFFLG1EQUFtRDtLQUNuRSxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFRSCxDQUFDO0FBRUYsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsRUFBbUU7UUFBakUsVUFBRSxFQUFFLGdDQUFhLEVBQUUsY0FBSSxFQUFFLG9CQUFPO0lBQ2pDLElBQU0saUJBQWlCLEdBQUcsV0FBVyxLQUFLLE9BQU8sYUFBYSxDQUFDLENBQUMsQ0FBQyxzQkFBb0IsYUFBZSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDMUcsSUFBTSxLQUFLLEdBQUcsWUFBVSxJQUFJLGtCQUFhLE9BQU8sR0FBRyxpQkFBbUIsQ0FBQztJQUV2RSxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLHFCQUFtQixFQUFFLGlCQUFZLEtBQU87UUFDOUMsV0FBVyxFQUFFLGtDQUFrQztRQUMvQyxhQUFhLEVBQUUsMERBQTBEO0tBQzFFLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQztBQU9ILENBQUM7QUFFRixNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FDakMsVUFBQyxFQUE0RDtRQUExRCxVQUFFLEVBQUUsb0JBQU8sRUFBRSxnQ0FBYTtJQUFxQyxPQUFBLElBQUksQ0FBQztRQUNyRSxJQUFJLEVBQUUscUJBQW1CLEVBQUUsY0FBVztRQUN0QyxJQUFJLEVBQUU7WUFDSixVQUFVLEVBQUUsWUFBWSxFQUFFO1lBQzFCLE9BQU8sRUFBRSxPQUFPO1lBQ2hCLGVBQWUsRUFBRSxhQUFhO1NBQy9CO1FBQ0QsV0FBVyxFQUFFLDJCQUEyQjtRQUN4QyxhQUFhLEVBQUUsbURBQW1EO0tBQ25FLENBQUM7QUFUZ0UsQ0FTaEUsQ0FBQztBQUtKLENBQUM7QUFFRixNQUFNLENBQUMsSUFBTSxtQkFBbUIsR0FBRyxVQUFDLEVBQWlDO1FBQS9CLFVBQUU7SUFBa0MsT0FBQSxJQUFJLENBQUM7UUFDN0UsSUFBSSxFQUFFLHFCQUFtQixFQUFFLFVBQU87UUFDbEMsSUFBSSxFQUFFO1lBQ0osVUFBVSxFQUFFLFlBQVksRUFBRTtTQUMzQjtRQUNELFdBQVcsRUFBRSx3QkFBd0I7UUFDckMsYUFBYSxFQUFFLGdEQUFnRDtLQUNoRSxDQUFDO0FBUHdFLENBT3hFLENBQUM7QUFLRixDQUFDO0FBRUYsTUFBTSxDQUFDLElBQU0sc0JBQXNCLEdBQUcsVUFBQyxFQUFvQztRQUFsQyxVQUFFO0lBQXFDLE9BQUEsR0FBRyxDQUFDO1FBQ2xGLElBQUksRUFBRSxxQkFBbUIsRUFBRSxZQUFTO1FBQ3BDLElBQUksRUFBRSxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsRUFBRTtRQUNwQyxXQUFXLEVBQUUsMkJBQTJCO1FBQ3hDLGFBQWEsRUFBRSxtREFBbUQ7S0FDbkUsQ0FBQztBQUw4RSxDQUs5RSxDQUFDO0FBRUgsTUFBTSxDQUFDLElBQU0sdUJBQXVCLEdBQUcsVUFBQyxFQUFVO1FBQVIsa0JBQU07SUFBTyxPQUFBLEdBQUcsQ0FBQztRQUN6RCxJQUFJLEVBQUUscUJBQW1CLE1BQVE7UUFDakMsV0FBVyxFQUFFLDRCQUE0QjtRQUN6QyxhQUFhLEVBQUUsb0RBQW9EO0tBQ3BFLENBQUM7QUFKcUQsQ0FJckQsQ0FBQztBQUVIOzs7OztHQUtHO0FBQ0gsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUN4QixVQUFDLEVBQTBCO1FBQXhCLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUN2QixJQUFNLEtBQUssR0FBRyxZQUFVLElBQUksa0JBQWEsT0FBUyxDQUFDO0lBRW5ELE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsZUFBYSxLQUFPO1FBQzFCLFdBQVcsRUFBRSxnQkFBZ0I7UUFDN0IsYUFBYSxFQUFFLHdDQUF3QztLQUN4RCxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSjs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0sbUJBQW1CLEdBQUcsVUFBQyxFQUFNO1FBQUosVUFBRTtJQUFPLE9BQUEsR0FBRyxDQUFDO1FBQ2pELElBQUksRUFBRSxnQkFBYyxFQUFJO1FBQ3hCLFdBQVcsRUFBRSx1QkFBdUI7UUFDcEMsYUFBYSxFQUFFLCtDQUErQztLQUMvRCxDQUFDO0FBSjZDLENBSTdDLENBQUMifQ==
// EXTERNAL MODULE: ./constants/api/activity-feed.ts
var activity_feed = __webpack_require__(27);

// CONCATENATED MODULE: ./action/activity-feed.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return fecthActivityFeedListAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return fecthActivityFeedCommentListAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return addActivityFeedCommentAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return addActivityFeedLikeAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return deleteActivityFeedLikeAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return fetchActivityFeedDetailAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return getCollectionAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return getCollectionDetailAction; });


/**
 * Fetch activity feed list by filter (limit & current_id)
 *
 * @param {number} limit default 20
 * @param {number} currentId
 */
var fecthActivityFeedListAction = function (_a) {
    var _b = _a.limit, limit = _b === void 0 ? 20 : _b, _c = _a.currentId, currentId = _c === void 0 ? 0 : _c, _d = _a.userId, userId = _d === void 0 ? 0 : _d, _e = _a.feedType, feedType = _e === void 0 ? '' : _e;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["f" /* FETCH_ACTIVITY_FEED_LIST */],
            payload: { promise: fecthActivityFeedList({ limit: limit, currentId: currentId, userId: userId, feedType: feedType }).then(function (res) { return res; }) },
            meta: { metaFilter: { limit: limit, currentId: currentId } }
        });
    };
};
/**
* Fetch activity feed comment list
*
* @param {number} id
* @param {number} lastCommentId
* @param {number} page
* @param {number} perPage
*/
var fecthActivityFeedCommentListAction = function (_a) {
    var id = _a.id, lastCommentId = _a.lastCommentId, page = _a.page, perPage = _a.perPage;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["d" /* FETCH_ACTIVITY_FEED_COMMENT_LIST */],
            payload: { promise: fecthActivityFeedCommentList({ id: id, lastCommentId: lastCommentId, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
/**
* Delete activity feed comment
*
* @param {number} id
* @param {string} content
* @param {number} lastCommentId
*/
var addActivityFeedCommentAction = function (_a) {
    var id = _a.id, content = _a.content, lastCommentId = _a.lastCommentId;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["a" /* ADD_ACTIVITY_FEED_COMMENT */],
            payload: { promise: addActivityFeedComment({ id: id, content: content, lastCommentId: lastCommentId }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
/**
* Add activity feed like
*
* @param {number} id
*/
var addActivityFeedLikeAction = function (_a) {
    var id = _a.id;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["b" /* ADD_ACTIVITY_FEED_LIKE */],
            payload: { promise: addActivityFeedLike({ id: id }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
/**
* Delete activity feed like
*
* @param {number} id
*/
var deleteActivityFeedLikeAction = function (_a) {
    var id = _a.id;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["c" /* DELETE_ACTIVITY_FEED_LIKE */],
            payload: { promise: deleteActivityFeedLike({ id: id }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
/**
* Fetch activity feed detail by id
*
* @param {number} feedId
*/
var fetchActivityFeedDetailAction = function (_a) {
    var feedId = _a.feedId;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["e" /* FETCH_ACTIVITY_FEED_DETAIL */],
            payload: { promise: fetchActivityFeedDetail({ feedId: feedId }).then(function (res) { return res; }) },
            meta: { feedId: feedId }
        });
    };
};
/**
 * Get Collection Action (Top feed)
 *
 * @param {number} page ex: 1
 * @param {number} perPage ex: 12
 */
var getCollectionAction = function (_a) {
    var _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["g" /* GET_COLLECTION */],
            payload: { promise: getCollection({ page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: {}
        });
    };
};
/**
* Get Collection Detail Action (Top feed)
*
* @param {number} id ex: 1
*/
var getCollectionDetailAction = function (_a) {
    var id = _a.id;
    return function (dispatch, getState) {
        return dispatch({
            type: activity_feed["h" /* GET_COLLECTION_DETAIL */],
            payload: { promise: getCollectionDetail({ id: id }).then(function (res) { return res; }) },
            meta: { id: id }
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0aXZpdHktZmVlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFjdGl2aXR5LWZlZWQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUVMLHFCQUFxQixFQUVyQiw0QkFBNEIsRUFFNUIsc0JBQXNCLEVBRXRCLG1CQUFtQixFQUVuQixzQkFBc0IsRUFDdEIsdUJBQXVCLEVBQ3ZCLGFBQWEsRUFDYixtQkFBbUIsRUFDcEIsTUFBTSxzQkFBc0IsQ0FBQztBQUU5QixPQUFPLEVBQ0wsd0JBQXdCLEVBQ3hCLGdDQUFnQyxFQUNoQyx5QkFBeUIsRUFDekIsc0JBQXNCLEVBQ3RCLHlCQUF5QixFQUN6QiwwQkFBMEIsRUFDMUIsY0FBYyxFQUNkLHFCQUFxQixHQUN0QixNQUFNLGdDQUFnQyxDQUFDO0FBR3hDOzs7OztHQUtHO0FBQ0gsTUFBTSxDQUFDLElBQU0sMkJBQTJCLEdBQ3RDLFVBQUMsRUFBZ0Y7UUFBOUUsYUFBVSxFQUFWLCtCQUFVLEVBQUUsaUJBQWEsRUFBYixrQ0FBYSxFQUFFLGNBQVUsRUFBViwrQkFBVSxFQUFFLGdCQUFhLEVBQWIsa0NBQWE7SUFDckQsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLHdCQUF3QjtZQUM5QixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUscUJBQXFCLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxTQUFTLFdBQUEsRUFBRSxNQUFNLFFBQUEsRUFBRSxRQUFRLFVBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQ3BHLElBQUksRUFBRSxFQUFFLFVBQVUsRUFBRSxFQUFFLEtBQUssT0FBQSxFQUFFLFNBQVMsV0FBQSxFQUFFLEVBQUU7U0FDM0MsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7Ozs7OztFQU9FO0FBQ0YsTUFBTSxDQUFDLElBQU0sa0NBQWtDLEdBQzdDLFVBQUMsRUFBbUU7UUFBakUsVUFBRSxFQUFFLGdDQUFhLEVBQUUsY0FBSSxFQUFFLG9CQUFPO0lBQ2pDLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxnQ0FBZ0M7WUFDdEMsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLDRCQUE0QixDQUFDLEVBQUUsRUFBRSxJQUFBLEVBQUUsYUFBYSxlQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN6RyxJQUFJLEVBQUUsRUFBRSxFQUFFLElBQUEsRUFBRTtTQUNiLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7OztFQU1FO0FBQ0YsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsRUFBNEQ7UUFBMUQsVUFBRSxFQUFFLG9CQUFPLEVBQUUsZ0NBQWE7SUFDM0IsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLHlCQUF5QjtZQUMvQixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsc0JBQXNCLENBQUMsRUFBRSxFQUFFLElBQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxhQUFhLGVBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQzdGLElBQUksRUFBRSxFQUFFLEVBQUUsSUFBQSxFQUFFO1NBQ2IsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0seUJBQXlCLEdBQ3BDLFVBQUMsRUFBaUM7UUFBL0IsVUFBRTtJQUNILE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxzQkFBc0I7WUFDNUIsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLG1CQUFtQixDQUFDLEVBQUUsRUFBRSxJQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUNsRSxJQUFJLEVBQUUsRUFBRSxFQUFFLElBQUEsRUFBRTtTQUNiLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7RUFJRTtBQUNGLE1BQU0sQ0FBQyxJQUFNLDRCQUE0QixHQUN2QyxVQUFDLEVBQW9DO1FBQWxDLFVBQUU7SUFDSCxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUseUJBQXlCO1lBQy9CLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxzQkFBc0IsQ0FBQyxFQUFFLEVBQUUsSUFBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDckUsSUFBSSxFQUFFLEVBQUUsRUFBRSxJQUFBLEVBQUU7U0FDYixDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUdUOzs7O0VBSUU7QUFDRixNQUFNLENBQUMsSUFBTSw2QkFBNkIsR0FDeEMsVUFBQyxFQUFVO1FBQVIsa0JBQU07SUFDUCxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsMEJBQTBCO1lBQ2hDLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSx1QkFBdUIsQ0FBQyxFQUFFLE1BQU0sUUFBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDMUUsSUFBSSxFQUFFLEVBQUUsTUFBTSxRQUFBLEVBQUU7U0FDakIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFHVDs7Ozs7R0FLRztBQUVILE1BQU0sQ0FBQyxJQUFNLG1CQUFtQixHQUM5QixVQUFDLEVBQTBCO1FBQXhCLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUFPLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqRCxPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxjQUFjO1lBQ3BCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxhQUFhLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQ3ZFLElBQUksRUFBRSxFQUFFO1NBQ1QsQ0FBQztJQUpGLENBSUU7QUFMNEIsQ0FLNUIsQ0FBQztBQUVQOzs7O0VBSUU7QUFFRixNQUFNLENBQUMsSUFBTSx5QkFBeUIsR0FDcEMsVUFBQyxFQUFNO1FBQUosVUFBRTtJQUFPLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUM3QixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxxQkFBcUI7WUFDM0IsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLG1CQUFtQixDQUFDLEVBQUUsRUFBRSxJQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUNsRSxJQUFJLEVBQUUsRUFBRSxFQUFFLElBQUEsRUFBRTtTQUNiLENBQUM7SUFKRixDQUlFO0FBTFEsQ0FLUixDQUFDIn0=

/***/ }),

/***/ 766:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/image.ts
var utils_image = __webpack_require__(349);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./components/magazine/image-slider/initialize.tsx
var DEFAULT_PROPS = {
    data: [],
    type: 'full',
    title: '',
    column: 3,
    showViewMore: false,
    showHeader: true,
    isCustomTitle: false,
    style: {},
    titleStyle: {}
};
var INITIAL_STATE = function (data) { return ({
    magazineList: data || [],
    magazineSlide: [],
    magazineSlideSelected: {},
    countChangeSlide: 0,
    firstInit: false
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLElBQUksRUFBRSxNQUFNO0lBQ1osS0FBSyxFQUFFLEVBQUU7SUFDVCxNQUFNLEVBQUUsQ0FBQztJQUNULFlBQVksRUFBRSxLQUFLO0lBQ25CLFVBQVUsRUFBRSxJQUFJO0lBQ2hCLGFBQWEsRUFBRSxLQUFLO0lBQ3BCLEtBQUssRUFBRSxFQUFFO0lBQ1QsVUFBVSxFQUFFLEVBQUU7Q0FDTCxDQUFDO0FBRVosTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHLFVBQUMsSUFBUyxJQUFLLE9BQUEsQ0FBQztJQUMzQyxZQUFZLEVBQUUsSUFBSSxJQUFJLEVBQUU7SUFDeEIsYUFBYSxFQUFFLEVBQUU7SUFDakIscUJBQXFCLEVBQUUsRUFBRTtJQUN6QixnQkFBZ0IsRUFBRSxDQUFDO0lBQ25CLFNBQVMsRUFBRSxLQUFLO0NBQ04sQ0FBQSxFQU5nQyxDQU1oQyxDQUFDIn0=
// EXTERNAL MODULE: ./container/layout/main-block/index.tsx
var main_block = __webpack_require__(747);

// EXTERNAL MODULE: ./components/ui/loading-placeholder/index.tsx + 4 modules
var loading_placeholder = __webpack_require__(751);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(347);

// EXTERNAL MODULE: ./style/component.ts
var component = __webpack_require__(745);

// CONCATENATED MODULE: ./components/magazine/image-slider-item/initialize.tsx
var initialize_DEFAULT_PROPS = {
    item: [],
    type: '',
    column: 4
};
var initialize_INITIAL_STATE = {
    isLoadedImage: false
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLElBQUksRUFBRSxFQUFFO0lBQ1IsTUFBTSxFQUFFLENBQUM7Q0FDQSxDQUFDO0FBRVosTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHO0lBQzNCLGFBQWEsRUFBRSxLQUFLO0NBQ1gsQ0FBQyJ9
// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ../node_modules/react-on-screen/lib/index.js
var lib = __webpack_require__(759);
var lib_default = /*#__PURE__*/__webpack_require__.n(lib);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/magazine/image-slider-item/style.tsx

var generateSwitchStyle = function (mobile, desktop) {
    var switchStyle = {
        MOBILE: { paddingRight: 10, width: mobile, minWidth: mobile },
        DESKTOP: { width: desktop }
    };
    return switchStyle[window.DEVICE_VERSION];
};
/* harmony default export */ var image_slider_item_style = ({
    mainWrap: {
        paddingLeft: 10,
        paddingRight: 10
    },
    heading: {
        paddingLeft: 10,
        display: variable["display"].inlineBlock,
        maxWidth: "100%",
        whiteSpace: "nowrap",
        overflow: "hidden",
        textOverflow: "ellipsis",
        fontFamily: variable["fontAvenirMedium"],
        color: variable["colorBlack"],
        fontSize: 20,
        lineHeight: "40px",
        height: 40,
        letterSpacing: -0.5,
        textTransform: "uppercase",
    },
    container: {
        width: '100%',
        overflowX: 'auto',
        whiteSpace: 'nowrap',
        paddingTop: 0,
        marginBottom: 0,
        display: variable["display"].flex,
        itemSlider: {
            width: "100%",
            height: "100%",
            display: variable["display"].inlineBlock,
            borderRadius: 5,
            overflow: 'hidden',
            boxShadow: variable["shadowBlur"],
            position: variable["position"].relative
        },
        itemSliderPanel: function (imgUrl) { return ({
            width: '100%',
            paddingTop: '62.5%',
            position: variable["position"].relative,
            backgroundImage: "url(" + imgUrl + ")",
            backgroundColor: variable["colorF7"],
            backgroundPosition: 'top center',
            transition: variable["transitionOpacity"],
            backgroundSize: 'cover',
        }); },
        videoIcon: {
            width: 70,
            height: 70,
            position: variable["position"].absolute,
            top: '50%',
            left: '55%',
            transform: 'translate(-50%, -50%)',
            borderTop: '35px solid transparent',
            boxSizing: 'border-box',
            borderLeft: '51px solid white',
            borderBottom: '35px solid transparent',
            opacity: .8
        },
        info: {
            width: '100%',
            height: 110,
            padding: 12,
            position: variable["position"].relative,
            background: variable["colorWhite"],
            display: variable["display"].flex,
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
            image: function (imgUrl) { return ({
                width: '100%',
                height: '100%',
                top: 0,
                left: 0,
                zIndex: -1,
                position: variable["position"].absolute,
                backgroundColor: variable["colorF7"],
                backgroundImage: "url(" + imgUrl + ")",
                backgroundPosition: 'bottom center',
                backgroundSize: 'cover',
                filter: 'blur(4px)',
                transform: "scaleY(-1) scale(1.1)",
                'WebkitBackfaceVisibility': 'hidden',
                'WebkitPerspective': 1000,
                'WebkitTransform': ['translate3d(0,0,0)', 'translateZ(0)'],
                'backfaceVisibility': 'hidden',
                perspective: 1000,
            }); },
            title: {
                color: variable["colorBlack"],
                whiteSpace: 'pre-wrap',
                fontFamily: variable["fontAvenirMedium"],
                fontSize: 14,
                lineHeight: '22px',
                maxHeight: '44px',
                overflow: 'hidden',
                marginBottom: 5
            },
            description: {
                fontSize: 12,
                color: variable["colorBlack"],
                fontFamily: variable["fontAvenirMedium"],
                opacity: .7,
                marginRight: 10,
                whiteSpace: 'pre-wrap',
                lineHeight: '18px',
                maxHeight: 36,
                overflow: 'hidden',
            },
            tagList: {
                width: '100%',
                display: variable["display"].flex,
                flexWrap: 'wrap',
                height: '36px',
                maxHeight: '36px',
                overflow: 'hidden'
            },
            tagItem: {
                fontSize: 12,
                lineHeight: '18px',
                color: variable["colorBlack"],
                fontFamily: variable["fontAvenirMedium"],
                opacity: .7,
                marginRight: 10,
                whiteSpace: 'pre-wrap'
            },
        }
    },
    column: {
        1: { width: '100%' },
        2: { width: '50%' },
        3: generateSwitchStyle('75%', '33.33%'),
        4: generateSwitchStyle('47%', '25%'),
        5: generateSwitchStyle('47%', '20%'),
        6: generateSwitchStyle('50%', '16.66%'),
    },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxJQUFNLG1CQUFtQixHQUFHLFVBQUMsTUFBTSxFQUFFLE9BQU87SUFDMUMsSUFBTSxXQUFXLEdBQUc7UUFDbEIsTUFBTSxFQUFFLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUU7UUFDN0QsT0FBTyxFQUFFLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRTtLQUM1QixDQUFDO0lBRUYsTUFBTSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUM7QUFDNUMsQ0FBQyxDQUFDO0FBRUYsZUFBZTtJQUNiLFFBQVEsRUFBRTtRQUNSLFdBQVcsRUFBRSxFQUFFO1FBQ2YsWUFBWSxFQUFFLEVBQUU7S0FDakI7SUFFRCxPQUFPLEVBQUU7UUFDUCxXQUFXLEVBQUUsRUFBRTtRQUNmLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLFdBQVc7UUFDckMsUUFBUSxFQUFFLE1BQU07UUFDaEIsVUFBVSxFQUFFLFFBQVE7UUFDcEIsUUFBUSxFQUFFLFFBQVE7UUFDbEIsWUFBWSxFQUFFLFVBQVU7UUFDeEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7UUFDckMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1FBQzFCLFFBQVEsRUFBRSxFQUFFO1FBQ1osVUFBVSxFQUFFLE1BQU07UUFDbEIsTUFBTSxFQUFFLEVBQUU7UUFDVixhQUFhLEVBQUUsQ0FBQyxHQUFHO1FBQ25CLGFBQWEsRUFBRSxXQUFXO0tBQzNCO0lBRUQsU0FBUyxFQUFFO1FBQ1QsS0FBSyxFQUFFLE1BQU07UUFDYixTQUFTLEVBQUUsTUFBTTtRQUNqQixVQUFVLEVBQUUsUUFBUTtRQUNwQixVQUFVLEVBQUUsQ0FBQztRQUNiLFlBQVksRUFBRSxDQUFDO1FBQ2YsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUU5QixVQUFVLEVBQUU7WUFDVixLQUFLLEVBQUUsTUFBTTtZQUNiLE1BQU0sRUFBRSxNQUFNO1lBQ2QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztZQUNyQyxZQUFZLEVBQUUsQ0FBQztZQUNmLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTtZQUM5QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1NBQ3JDO1FBRUQsZUFBZSxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztZQUM1QixLQUFLLEVBQUUsTUFBTTtZQUNiLFVBQVUsRUFBRSxPQUFPO1lBQ25CLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDcEMsZUFBZSxFQUFFLFNBQU8sTUFBTSxNQUFHO1lBQ2pDLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztZQUNqQyxrQkFBa0IsRUFBRSxZQUFZO1lBQ2hDLFVBQVUsRUFBRSxRQUFRLENBQUMsaUJBQWlCO1lBQ3RDLGNBQWMsRUFBRSxPQUFPO1NBQ3hCLENBQUMsRUFUMkIsQ0FTM0I7UUFFRixTQUFTLEVBQUU7WUFDVCxLQUFLLEVBQUUsRUFBRTtZQUNULE1BQU0sRUFBRSxFQUFFO1lBQ1YsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxHQUFHLEVBQUUsS0FBSztZQUNWLElBQUksRUFBRSxLQUFLO1lBQ1gsU0FBUyxFQUFFLHVCQUF1QjtZQUNsQyxTQUFTLEVBQUUsd0JBQXdCO1lBQ25DLFNBQVMsRUFBRSxZQUFZO1lBQ3ZCLFVBQVUsRUFBRSxrQkFBa0I7WUFDOUIsWUFBWSxFQUFFLHdCQUF3QjtZQUN0QyxPQUFPLEVBQUUsRUFBRTtTQUNaO1FBRUQsSUFBSSxFQUFFO1lBQ0osS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsR0FBRztZQUNYLE9BQU8sRUFBRSxFQUFFO1lBQ1gsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDL0IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixhQUFhLEVBQUUsUUFBUTtZQUN2QixjQUFjLEVBQUUsWUFBWTtZQUM1QixVQUFVLEVBQUUsWUFBWTtZQUV4QixLQUFLLEVBQUUsVUFBQyxNQUFNLElBQUssT0FBQSxDQUFDO2dCQUNsQixLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsTUFBTTtnQkFDZCxHQUFHLEVBQUUsQ0FBQztnQkFDTixJQUFJLEVBQUUsQ0FBQztnQkFDUCxNQUFNLEVBQUUsQ0FBQyxDQUFDO2dCQUNWLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztnQkFDakMsZUFBZSxFQUFFLFNBQU8sTUFBTSxNQUFHO2dCQUNqQyxrQkFBa0IsRUFBRSxlQUFlO2dCQUNuQyxjQUFjLEVBQUUsT0FBTztnQkFDdkIsTUFBTSxFQUFFLFdBQVc7Z0JBQ25CLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLDBCQUEwQixFQUFFLFFBQVE7Z0JBQ3BDLG1CQUFtQixFQUFFLElBQUk7Z0JBQ3pCLGlCQUFpQixFQUFFLENBQUMsb0JBQW9CLEVBQUUsZUFBZSxDQUFDO2dCQUMxRCxvQkFBb0IsRUFBRSxRQUFRO2dCQUM5QixXQUFXLEVBQUUsSUFBSTthQUNsQixDQUFDLEVBbEJpQixDQWtCakI7WUFFRixLQUFLLEVBQUU7Z0JBQ0wsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMxQixVQUFVLEVBQUUsVUFBVTtnQkFDdEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxNQUFNO2dCQUNsQixTQUFTLEVBQUUsTUFBTTtnQkFDakIsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFlBQVksRUFBRSxDQUFDO2FBQ2hCO1lBRUQsV0FBVyxFQUFFO2dCQUNYLFFBQVEsRUFBRSxFQUFFO2dCQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLE9BQU8sRUFBRSxFQUFFO2dCQUNYLFdBQVcsRUFBRSxFQUFFO2dCQUNmLFVBQVUsRUFBRSxVQUFVO2dCQUN0QixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsU0FBUyxFQUFFLEVBQUU7Z0JBQ2IsUUFBUSxFQUFFLFFBQVE7YUFDbkI7WUFFRCxPQUFPLEVBQUU7Z0JBQ1AsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsUUFBUSxFQUFFLE1BQU07Z0JBQ2hCLE1BQU0sRUFBRSxNQUFNO2dCQUNkLFNBQVMsRUFBRSxNQUFNO2dCQUNqQixRQUFRLEVBQUUsUUFBUTthQUNuQjtZQUVELE9BQU8sRUFBRTtnQkFDUCxRQUFRLEVBQUUsRUFBRTtnQkFDWixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtnQkFDckMsT0FBTyxFQUFFLEVBQUU7Z0JBQ1gsV0FBVyxFQUFFLEVBQUU7Z0JBQ2YsVUFBVSxFQUFFLFVBQVU7YUFDdkI7U0FDRjtLQUNGO0lBRUQsTUFBTSxFQUFFO1FBQ04sQ0FBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRTtRQUNwQixDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFO1FBQ25CLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDO1FBQ3ZDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDO1FBQ3BDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDO1FBQ3BDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDO0tBQ3hDO0NBQ00sQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider-item/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};





function renderComponent(_a) {
    var state = _a.state, props = _a.props, handleLoadImage = _a.handleLoadImage;
    var _b = props, item = _b.item, type = _b.type, column = _b.column;
    var isLoadedImage = state.isLoadedImage;
    var linkProps = {
        to: routing["Ma" /* ROUTING_MAGAZINE_DETAIL_PATH */] + "/" + (item && item.slug || ''),
        style: image_slider_item_style.container.itemSlider
    };
    return (react["createElement"](lib_default.a, { style: Object.assign({}, image_slider_item_style.column[column || 4], image_slider_item_style.mainWrap), offset: 200 }, function (_a) {
        var isVisible = _a.isVisible;
        if (!!isVisible) {
            handleLoadImage();
        }
        return (react["createElement"](react_router_dom["NavLink"], __assign({}, linkProps),
            react["createElement"]("div", { style: [
                    image_slider_item_style.container.itemSliderPanel(isLoadedImage && item ? item.cover_image && item.cover_image.medium_url : ''),
                    { opacity: isLoadedImage ? 1 : 0 }
                ] }, 'video' === type && (react["createElement"]("div", { style: image_slider_item_style.container.videoIcon }))),
            react["createElement"]("div", { style: image_slider_item_style.container.info },
                react["createElement"]("div", { style: image_slider_item_style.container.info.title }, item && item.title || ''),
                react["createElement"]("div", { style: image_slider_item_style.container.info.description }, item && item.description || ''))));
    }));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQ3RGLE9BQU8sZUFBZSxNQUFNLGlCQUFpQixDQUFDO0FBRTlDLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixNQUFNLDBCQUEwQixFQUFpQztRQUEvQixnQkFBSyxFQUFFLGdCQUFLLEVBQUUsb0NBQWU7SUFDdkQsSUFBQSxVQUF3QyxFQUF0QyxjQUFJLEVBQUUsY0FBSSxFQUFFLGtCQUFNLENBQXFCO0lBQ3ZDLElBQUEsbUNBQWEsQ0FBVztJQUVoQyxJQUFNLFNBQVMsR0FBRztRQUNoQixFQUFFLEVBQUssNEJBQTRCLFVBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFFO1FBQ2hFLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLFVBQVU7S0FDbEMsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLG9CQUFDLGVBQWUsSUFBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxFQUFFLE1BQU0sRUFBRSxHQUFHLElBRTVGLFVBQUMsRUFBYTtZQUFYLHdCQUFTO1FBQ1YsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFBQyxlQUFlLEVBQUUsQ0FBQTtRQUFBLENBQUM7UUFFckMsTUFBTSxDQUFDLENBQ0wsb0JBQUMsT0FBTyxlQUFLLFNBQVM7WUFDcEIsNkJBQUssS0FBSyxFQUFFO29CQUNSLEtBQUssQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFFLGFBQWEsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDOUcsRUFBRSxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtpQkFDbkMsSUFFQyxPQUFPLEtBQUssSUFBSSxJQUFJLENBQ2xCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBUSxDQUM5QyxDQUVDO1lBQ04sNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSTtnQkFDOUIsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBRyxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQU87Z0JBQ3hFLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLElBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksRUFBRSxDQUFPLENBQ2hGLENBQ0UsQ0FDWCxDQUFBO0lBQ0gsQ0FBQyxDQUVhLENBQ25CLENBQUM7QUFDSixDQUFDO0FBQUEsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider-item/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_SlideItem = /** @class */ (function (_super) {
    __extends(SlideItem, _super);
    function SlideItem(props) {
        var _this = _super.call(this, props) || this;
        _this.state = initialize_INITIAL_STATE;
        return _this;
    }
    // shouldComponentUpdate(nextProps: IProps, nextState: IState) {
    //   // if (true === isEmptyObject(this.props.data) && false === isEmptyObject(nextProps.data)) { return true; }
    //   // if (this.props.listLikedId.length !== nextProps.listLikedId.length) { return true; }
    //   return true;
    // }
    SlideItem.prototype.handleLoadImage = function () {
        var isLoadedImage = this.state.isLoadedImage;
        if (!!isLoadedImage) {
            return;
        }
        this.setState({ isLoadedImage: true });
    };
    SlideItem.prototype.render = function () {
        var renderViewProps = {
            props: this.props,
            state: this.state,
            handleLoadImage: this.handleLoadImage.bind(this)
        };
        return renderComponent(renderViewProps);
    };
    SlideItem.defaultProps = initialize_DEFAULT_PROPS;
    SlideItem = __decorate([
        radium
    ], SlideItem);
    return SlideItem;
}(react["Component"]));
;
/* harmony default export */ var image_slider_item_component = (component_SlideItem);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDNUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUl6QztJQUF3Qiw2QkFBK0I7SUFHckQsbUJBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUM7O0lBQzdCLENBQUM7SUFFRCxnRUFBZ0U7SUFDaEUsZ0hBQWdIO0lBQ2hILDRGQUE0RjtJQUU1RixpQkFBaUI7SUFDakIsSUFBSTtJQUVKLG1DQUFlLEdBQWY7UUFDVSxJQUFBLHdDQUFhLENBQWdCO1FBQ3JDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sQ0FBQztRQUNULENBQUM7UUFFRCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUMsYUFBYSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELDBCQUFNLEdBQU47UUFDRSxJQUFNLGVBQWUsR0FBRztZQUN0QixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLGVBQWUsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDakQsQ0FBQztRQUVGLE1BQU0sQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQS9CTSxzQkFBWSxHQUFXLGFBQWEsQ0FBQztJQUR4QyxTQUFTO1FBRGQsTUFBTTtPQUNELFNBQVMsQ0FpQ2Q7SUFBRCxnQkFBQztDQUFBLEFBakNELENBQXdCLEtBQUssQ0FBQyxTQUFTLEdBaUN0QztBQUFBLENBQUM7QUFFRixlQUFlLFNBQVMsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider-item/index.tsx

/* harmony default export */ var image_slider_item = (image_slider_item_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxTQUFTLE1BQU0sYUFBYSxDQUFDO0FBQ3BDLGVBQWUsU0FBUyxDQUFDIn0=
// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// CONCATENATED MODULE: ./components/magazine/image-slider/style.tsx




var INLINE_STYLE = {
    '.magazine-slide-container .pagination': {
        opacity: 0,
        transform: 'translateY(20px)'
    },
    '.magazine-slide-container:hover .pagination': {
        opacity: 1,
        transform: 'translateY(0)'
    },
    '.magazine-slide-container .left-nav': {
        width: 40,
        height: 60,
        opacity: 0,
        transform: 'translate(-60px, -50%)',
        visibility: 'hidden',
    },
    '.magazine-slide-container:hover .left-nav': {
        opacity: 1,
        transform: 'translate(1px, -50%)',
        visibility: 'visible',
    },
    '.magazine-slide-container .right-nav': {
        width: 40,
        height: 60,
        opacity: 0,
        transform: 'translate(60px, -50%)',
        visibility: 'hidden',
    },
    '.magazine-slide-container:hover .right-nav': {
        opacity: 1,
        transform: 'translate(1px, -50%)',
        visibility: 'visible',
    }
};
/* harmony default export */ var image_slider_style = ({
    container: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{ marginBottom: 10 }],
        DESKTOP: [{ marginBottom: 30 }],
        GENERAL: [{ display: variable["display"].block, width: '100%' }]
    }),
    magazineSlide: {
        position: 'relative',
        overflow: 'hidden',
        container: Object.assign({}, layout["a" /* flexContainer */].justify, component["c" /* block */].content, {
            paddingTop: 10,
            transition: variable["transitionOpacity"],
        }),
        pagination: [
            layout["a" /* flexContainer */].center,
            component["f" /* slidePagination */],
            {
                transition: variable["transitionNormal"],
                bottom: 0
            },
        ],
        navigation: [
            layout["a" /* flexContainer */].center,
            layout["a" /* flexContainer */].verticalCenter,
            component["e" /* slideNavigation */]
        ],
    },
    customStyleLoading: {
        height: 300
    },
    mobileWrap: {
        width: '100%',
        overflowX: 'auto',
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        panel: {
            flexWrap: 'wrap'
        },
        item: {
            width: '100%',
            marginBottom: 5
        }
    },
    placeholder: {
        width: '100%',
        display: variable["display"].flex,
        marginBottom: 35,
        item: {
            flex: 1,
            paddingLeft: 10,
            paddingRight: 10,
        },
        image: {
            width: '100%',
            height: 282,
            marginBottom: 10,
        },
        text: {
            width: '94%',
            height: 25,
            marginBottom: 10,
        },
        lastText: {
            width: '65%',
            height: 25,
        }
    },
    desktop: {
        mainWrap: {
            display: variable["display"].block,
            marginLeft: -9,
            marginRight: -9
        },
        container: {
            width: '100%',
            overflowX: 'auto',
            whiteSpace: 'nowrap',
            paddingTop: 0,
            paddingBottom: 15,
            display: variable["display"].flex,
            overflow: 'hidden',
            itemSlider: {
                width: "100%",
                display: variable["display"].inlineBlock,
                borderRadius: 5,
                overflow: 'hidden',
                boxShadow: variable["shadowBlur"],
                position: variable["position"].relative,
            },
            itemSliderPanel: function (imgUrl) { return ({
                width: '100%',
                paddingTop: '62.5%',
                position: variable["position"].relative,
                backgroundImage: "url(" + imgUrl + ")",
                backgroundColor: variable["colorF7"],
                backgroundPosition: 'top center',
                backgroundSize: 'cover',
            }); },
            videoIcon: {
                width: 70,
                height: 70,
                position: variable["position"].absolute,
                top: '50%',
                left: '55%',
                transform: 'translate(-50%, -50%)',
                borderTop: '35px solid transparent',
                boxSizing: 'border-box',
                borderLeft: '51px solid white',
                borderBottom: '35px solid transparent',
                opacity: .8
            },
            info: {
                width: '100%',
                height: 110,
                padding: 12,
                position: variable["position"].relative,
                background: variable["colorWhite"],
                display: variable["display"].flex,
                flexDirection: 'column',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                image: function (imgUrl) { return ({
                    width: '100%',
                    height: '100%',
                    top: 0,
                    left: 0,
                    zIndex: -1,
                    position: variable["position"].absolute,
                    backgroundColor: variable["colorF7"],
                    backgroundImage: "url(" + imgUrl + ")",
                    backgroundPosition: 'bottom center',
                    backgroundSize: 'cover',
                    filter: 'blur(4px)',
                    transform: "scaleY(-1) scale(1.1)",
                    'WebkitBackfaceVisibility': 'hidden',
                    'WebkitPerspective': 1000,
                    'WebkitTransform': ['translate3d(0,0,0)', 'translateZ(0)'],
                    'backfaceVisibility': 'hidden',
                    perspective: 1000,
                }); },
                title: {
                    color: variable["colorBlack"],
                    whiteSpace: 'pre-wrap',
                    fontFamily: variable["fontAvenirMedium"],
                    fontSize: 14,
                    lineHeight: '22px',
                    maxHeight: '44px',
                    overflow: 'hidden',
                    marginBottom: 5
                },
                description: {
                    fontSize: 12,
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap',
                    lineHeight: '18px',
                    maxHeight: 36,
                    overflow: 'hidden',
                },
                tagList: {
                    width: '100%',
                    display: variable["display"].flex,
                    flexWrap: 'wrap',
                    height: '36px',
                    maxHeight: '36px',
                    overflow: 'hidden'
                },
                tagItem: {
                    fontSize: 12,
                    lineHeight: '18px',
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap'
                },
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFNBQVMsTUFBTSwwQkFBMEIsQ0FBQztBQUN0RCxPQUFPLEtBQUssTUFBTSxNQUFNLHVCQUF1QixDQUFDO0FBQ2hELE9BQU8sS0FBSyxRQUFRLE1BQU0seUJBQXlCLENBQUM7QUFDcEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRXpELE1BQU0sQ0FBQyxJQUFNLFlBQVksR0FBRztJQUMxQix1Q0FBdUMsRUFBRTtRQUN2QyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxrQkFBa0I7S0FDOUI7SUFFRCw2Q0FBNkMsRUFBRTtRQUM3QyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxlQUFlO0tBQzNCO0lBRUQscUNBQXFDLEVBQUU7UUFDckMsS0FBSyxFQUFFLEVBQUU7UUFDVCxNQUFNLEVBQUUsRUFBRTtRQUNWLE9BQU8sRUFBRSxDQUFDO1FBQ1YsU0FBUyxFQUFFLHdCQUF3QjtRQUNuQyxVQUFVLEVBQUUsUUFBUTtLQUNyQjtJQUVELDJDQUEyQyxFQUFFO1FBQzNDLE9BQU8sRUFBRSxDQUFDO1FBQ1YsU0FBUyxFQUFFLHNCQUFzQjtRQUNqQyxVQUFVLEVBQUUsU0FBUztLQUN0QjtJQUVELHNDQUFzQyxFQUFFO1FBQ3RDLEtBQUssRUFBRSxFQUFFO1FBQ1QsTUFBTSxFQUFFLEVBQUU7UUFDVixPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSx1QkFBdUI7UUFDbEMsVUFBVSxFQUFFLFFBQVE7S0FDckI7SUFFRCw0Q0FBNEMsRUFBRTtRQUM1QyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxzQkFBc0I7UUFDakMsVUFBVSxFQUFFLFNBQVM7S0FDdEI7Q0FDRixDQUFDO0FBRUYsZUFBZTtJQUNiLFNBQVMsRUFBRSxZQUFZLENBQUM7UUFDdEIsTUFBTSxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLENBQUM7UUFDOUIsT0FBTyxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLENBQUM7UUFDL0IsT0FBTyxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxDQUFDO0tBQzlELENBQUM7SUFFRixhQUFhLEVBQUU7UUFDYixRQUFRLEVBQUUsVUFBVTtRQUNwQixRQUFRLEVBQUUsUUFBUTtRQUVsQixTQUFTLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQ3pCLE1BQU0sQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUM1QixTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFDdkI7WUFDRSxVQUFVLEVBQUUsRUFBRTtZQUNkLFVBQVUsRUFBRSxRQUFRLENBQUMsaUJBQWlCO1NBQ3ZDLENBQ0Y7UUFFRCxVQUFVLEVBQUU7WUFDVixNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU07WUFDM0IsU0FBUyxDQUFDLGVBQWU7WUFDekI7Z0JBQ0UsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLE1BQU0sRUFBRSxDQUFDO2FBQ1Y7U0FDRjtRQUVELFVBQVUsRUFBRTtZQUNWLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTTtZQUMzQixNQUFNLENBQUMsYUFBYSxDQUFDLGNBQWM7WUFDbkMsU0FBUyxDQUFDLGVBQWU7U0FDMUI7S0FDRjtJQUVELGtCQUFrQixFQUFFO1FBQ2xCLE1BQU0sRUFBRSxHQUFHO0tBQ1o7SUFFRCxVQUFVLEVBQUU7UUFDVixLQUFLLEVBQUUsTUFBTTtRQUNiLFNBQVMsRUFBRSxNQUFNO1FBQ2pCLFVBQVUsRUFBRSxDQUFDO1FBQ2IsWUFBWSxFQUFFLENBQUM7UUFDZixhQUFhLEVBQUUsQ0FBQztRQUNoQixXQUFXLEVBQUUsQ0FBQztRQUVkLEtBQUssRUFBRTtZQUNMLFFBQVEsRUFBRSxNQUFNO1NBQ2pCO1FBRUQsSUFBSSxFQUFFO1lBQ0osS0FBSyxFQUFFLE1BQU07WUFDYixZQUFZLEVBQUUsQ0FBQztTQUNoQjtLQUNGO0lBRUQsV0FBVyxFQUFFO1FBQ1gsS0FBSyxFQUFFLE1BQU07UUFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1FBQzlCLFlBQVksRUFBRSxFQUFFO1FBRWhCLElBQUksRUFBRTtZQUNKLElBQUksRUFBRSxDQUFDO1lBQ1AsV0FBVyxFQUFFLEVBQUU7WUFDZixZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELEtBQUssRUFBRTtZQUNMLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLEdBQUc7WUFDWCxZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELElBQUksRUFBRTtZQUNKLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLEVBQUU7WUFDVixZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELFFBQVEsRUFBRTtZQUNSLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLEVBQUU7U0FDWDtLQUNGO0lBRUQsT0FBTyxFQUFFO1FBQ1AsUUFBUSxFQUFFO1lBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztZQUMvQixVQUFVLEVBQUUsQ0FBQyxDQUFDO1lBQ2QsV0FBVyxFQUFFLENBQUMsQ0FBQztTQUNoQjtRQUVELFNBQVMsRUFBRTtZQUNULEtBQUssRUFBRSxNQUFNO1lBQ2IsU0FBUyxFQUFFLE1BQU07WUFDakIsVUFBVSxFQUFFLFFBQVE7WUFDcEIsVUFBVSxFQUFFLENBQUM7WUFDYixhQUFhLEVBQUUsRUFBRTtZQUNqQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLFFBQVEsRUFBRSxRQUFRO1lBRWxCLFVBQVUsRUFBRTtnQkFDVixLQUFLLEVBQUUsTUFBTTtnQkFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxXQUFXO2dCQUNyQyxZQUFZLEVBQUUsQ0FBQztnQkFDZixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUM5QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2FBQ3JDO1lBRUQsZUFBZSxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztnQkFDNUIsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsVUFBVSxFQUFFLE9BQU87Z0JBQ25CLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztnQkFDakMsZUFBZSxFQUFFLFFBQVEsQ0FBQyxPQUFPO2dCQUNqQyxrQkFBa0IsRUFBRSxZQUFZO2dCQUNoQyxjQUFjLEVBQUUsT0FBTzthQUN4QixDQUFDLEVBUjJCLENBUTNCO1lBRUYsU0FBUyxFQUFFO2dCQUNULEtBQUssRUFBRSxFQUFFO2dCQUNULE1BQU0sRUFBRSxFQUFFO2dCQUNWLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLEdBQUcsRUFBRSxLQUFLO2dCQUNWLElBQUksRUFBRSxLQUFLO2dCQUNYLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLFNBQVMsRUFBRSx3QkFBd0I7Z0JBQ25DLFNBQVMsRUFBRSxZQUFZO2dCQUN2QixVQUFVLEVBQUUsa0JBQWtCO2dCQUM5QixZQUFZLEVBQUUsd0JBQXdCO2dCQUN0QyxPQUFPLEVBQUUsRUFBRTthQUNaO1lBRUQsSUFBSSxFQUFFO2dCQUNKLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxHQUFHO2dCQUNYLE9BQU8sRUFBRSxFQUFFO2dCQUNYLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLFVBQVUsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDL0IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsYUFBYSxFQUFFLFFBQVE7Z0JBQ3ZCLGNBQWMsRUFBRSxZQUFZO2dCQUM1QixVQUFVLEVBQUUsWUFBWTtnQkFFeEIsS0FBSyxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztvQkFDbEIsS0FBSyxFQUFFLE1BQU07b0JBQ2IsTUFBTSxFQUFFLE1BQU07b0JBQ2QsR0FBRyxFQUFFLENBQUM7b0JBQ04sSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLENBQUMsQ0FBQztvQkFDVixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUNwQyxlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87b0JBQ2pDLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztvQkFDakMsa0JBQWtCLEVBQUUsZUFBZTtvQkFDbkMsY0FBYyxFQUFFLE9BQU87b0JBQ3ZCLE1BQU0sRUFBRSxXQUFXO29CQUNuQixTQUFTLEVBQUUsdUJBQXVCO29CQUNsQywwQkFBMEIsRUFBRSxRQUFRO29CQUNwQyxtQkFBbUIsRUFBRSxJQUFJO29CQUN6QixpQkFBaUIsRUFBRSxDQUFDLG9CQUFvQixFQUFFLGVBQWUsQ0FBQztvQkFDMUQsb0JBQW9CLEVBQUUsUUFBUTtvQkFDOUIsV0FBVyxFQUFFLElBQUk7aUJBQ2xCLENBQUMsRUFsQmlCLENBa0JqQjtnQkFFRixLQUFLLEVBQUU7b0JBQ0wsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsVUFBVTtvQkFDdEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7b0JBQ3JDLFFBQVEsRUFBRSxFQUFFO29CQUNaLFVBQVUsRUFBRSxNQUFNO29CQUNsQixTQUFTLEVBQUUsTUFBTTtvQkFDakIsUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLFlBQVksRUFBRSxDQUFDO2lCQUNoQjtnQkFFRCxXQUFXLEVBQUU7b0JBQ1gsUUFBUSxFQUFFLEVBQUU7b0JBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsVUFBVSxFQUFFLFVBQVU7b0JBQ3RCLFVBQVUsRUFBRSxNQUFNO29CQUNsQixTQUFTLEVBQUUsRUFBRTtvQkFDYixRQUFRLEVBQUUsUUFBUTtpQkFDbkI7Z0JBRUQsT0FBTyxFQUFFO29CQUNQLEtBQUssRUFBRSxNQUFNO29CQUNiLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7b0JBQzlCLFFBQVEsRUFBRSxNQUFNO29CQUNoQixNQUFNLEVBQUUsTUFBTTtvQkFDZCxTQUFTLEVBQUUsTUFBTTtvQkFDakIsUUFBUSxFQUFFLFFBQVE7aUJBQ25CO2dCQUVELE9BQU8sRUFBRTtvQkFDUCxRQUFRLEVBQUUsRUFBRTtvQkFDWixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsVUFBVSxFQUFFLFVBQVU7aUJBQ3ZCO2FBQ0Y7U0FDRjtLQUNGO0NBQ0ssQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider/view-desktop.tsx
var view_desktop_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};






var renderSlider = function (_a) {
    var column = _a.column, type = _a.type, magazineSlideSelected = _a.magazineSlideSelected, style = _a.style;
    return (react["createElement"]("magazine-category", { style: image_slider_style.desktop.mainWrap },
        react["createElement"]("div", { style: [image_slider_style.desktop.container, style] }, magazineSlideSelected
            && Array.isArray(magazineSlideSelected.list)
            && magazineSlideSelected.list.map(function (item) {
                var slideProps = {
                    item: item,
                    type: type,
                    column: column
                };
                return react["createElement"](image_slider_item, view_desktop_assign({ key: "slider-item-" + item.id }, slideProps));
            }))));
};
var renderNavigation = function (_a) {
    var magazineSlide = _a.magazineSlide, navLeftSlide = _a.navLeftSlide, navRightSlide = _a.navRightSlide;
    var leftNavProps = {
        className: 'left-nav',
        onClick: navLeftSlide,
        style: [image_slider_style.magazineSlide.navigation, component["e" /* slideNavigation */]['left'], { top: "calc(50% - 65px)" }]
    };
    var rightNavProps = {
        className: 'right-nav',
        onClick: navRightSlide,
        style: [image_slider_style.magazineSlide.navigation, component["e" /* slideNavigation */]['right'], { top: "calc(50% - 65px)" }]
    };
    return magazineSlide.length <= 1
        ? null
        : (react["createElement"]("div", null,
            react["createElement"]("div", view_desktop_assign({}, leftNavProps),
                react["createElement"](icon["a" /* default */], { name: 'angle-left', style: component["e" /* slideNavigation */].icon })),
            react["createElement"]("div", view_desktop_assign({}, rightNavProps),
                react["createElement"](icon["a" /* default */], { name: 'angle-right', style: component["e" /* slideNavigation */].icon }))));
};
var renderPagination = function (_a) {
    var magazineSlide = _a.magazineSlide, selectSlide = _a.selectSlide, countChangeSlide = _a.countChangeSlide;
    var generateItemProps = function (item, index) { return ({
        key: "product-slider-" + item.id,
        onClick: function () { return selectSlide(index); },
        style: [
            component["f" /* slidePagination */].item,
            index === countChangeSlide && component["f" /* slidePagination */].itemActive
        ]
    }); };
    return magazineSlide.length <= 1
        ? null
        : (react["createElement"]("div", { style: image_slider_style.magazineSlide.pagination, className: 'pagination' }, Array.isArray(magazineSlide)
            && magazineSlide.map(function (item, $index) {
                var itemProps = generateItemProps(item, $index);
                return react["createElement"]("div", view_desktop_assign({}, itemProps));
            })));
};
var renderDesktop = function (_a) {
    var props = _a.props, state = _a.state, handleMouseHover = _a.handleMouseHover, selectSlide = _a.selectSlide, navLeftSlide = _a.navLeftSlide, navRightSlide = _a.navRightSlide;
    var _b = state, magazineSlide = _b.magazineSlide, magazineSlideSelected = _b.magazineSlideSelected, countChangeSlide = _b.countChangeSlide;
    var _c = props, column = _c.column, type = _c.type, style = _c.style;
    var containerProps = {
        style: image_slider_style.magazineSlide,
        onMouseEnter: handleMouseHover
    };
    return (react["createElement"]("div", view_desktop_assign({}, containerProps, { className: 'magazine-slide-container' }),
        renderSlider({ column: column, type: type, magazineSlideSelected: magazineSlideSelected, style: style }),
        renderPagination({ magazineSlide: magazineSlide, selectSlide: selectSlide, countChangeSlide: countChangeSlide }),
        renderNavigation({ magazineSlide: magazineSlide, navLeftSlide: navLeftSlide, navRightSlide: navRightSlide }),
        react["createElement"](radium["Style"], { rules: INLINE_STYLE })));
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1kZXNrdG9wLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1kZXNrdG9wLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFL0IsT0FBTyxJQUFJLE1BQU0sZUFBZSxDQUFDO0FBQ2pDLE9BQU8sS0FBSyxTQUFTLE1BQU0sMEJBQTBCLENBQUM7QUFFdEQsT0FBTyxlQUFlLE1BQU0sc0JBQXNCLENBQUM7QUFFbkQsT0FBTyxLQUFLLEVBQUUsRUFBRSxZQUFZLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFHOUMsSUFBTSxZQUFZLEdBQUcsVUFBQyxFQUE4QztRQUE1QyxrQkFBTSxFQUFFLGNBQUksRUFBRSxnREFBcUIsRUFBRSxnQkFBSztJQUFPLE9BQUEsQ0FDdkUsMkNBQW1CLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVE7UUFDOUMsNkJBQUssS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLElBRXhDLHFCQUFxQjtlQUNsQixLQUFLLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQztlQUN6QyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSTtnQkFDcEMsSUFBTSxVQUFVLEdBQUc7b0JBQ2pCLElBQUksTUFBQTtvQkFDSixJQUFJLE1BQUE7b0JBQ0osTUFBTSxRQUFBO2lCQUNQLENBQUE7Z0JBRUQsTUFBTSxDQUFDLG9CQUFDLGVBQWUsYUFBQyxHQUFHLEVBQUUsaUJBQWUsSUFBSSxDQUFDLEVBQUksSUFBTSxVQUFVLEVBQUksQ0FBQTtZQUMzRSxDQUFDLENBQUMsQ0FFQSxDQUNZLENBQ3JCO0FBbEJ3RSxDQWtCeEUsQ0FBQztBQUVGLElBQU0sZ0JBQWdCLEdBQUcsVUFBQyxFQUE4QztRQUE1QyxnQ0FBYSxFQUFFLDhCQUFZLEVBQUUsZ0NBQWE7SUFDcEUsSUFBTSxZQUFZLEdBQUc7UUFDbkIsU0FBUyxFQUFFLFVBQVU7UUFDckIsT0FBTyxFQUFFLFlBQVk7UUFDckIsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxrQkFBa0IsRUFBRSxDQUFDO0tBQ3hHLENBQUM7SUFFRixJQUFNLGFBQWEsR0FBRztRQUNwQixTQUFTLEVBQUUsV0FBVztRQUN0QixPQUFPLEVBQUUsYUFBYTtRQUN0QixLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxFQUFFLEVBQUUsR0FBRyxFQUFFLGtCQUFrQixFQUFFLENBQUM7S0FDekcsQ0FBQztJQUVGLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxJQUFJLENBQUM7UUFDOUIsQ0FBQyxDQUFDLElBQUk7UUFDTixDQUFDLENBQUMsQ0FDQTtZQUNFLHdDQUFTLFlBQVk7Z0JBQ25CLG9CQUFDLElBQUksSUFBQyxJQUFJLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsZUFBZSxDQUFDLElBQUksR0FBSSxDQUMvRDtZQUNOLHdDQUFTLGFBQWE7Z0JBQ3BCLG9CQUFDLElBQUksSUFBQyxJQUFJLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsZUFBZSxDQUFDLElBQUksR0FBSSxDQUNoRSxDQUNGLENBQ1AsQ0FBQztBQUNOLENBQUMsQ0FBQztBQUVGLElBQU0sZ0JBQWdCLEdBQUcsVUFBQyxFQUFnRDtRQUE5QyxnQ0FBYSxFQUFFLDRCQUFXLEVBQUUsc0NBQWdCO0lBQ3RFLElBQU0saUJBQWlCLEdBQUcsVUFBQyxJQUFJLEVBQUUsS0FBSyxJQUFLLE9BQUEsQ0FBQztRQUMxQyxHQUFHLEVBQUUsb0JBQWtCLElBQUksQ0FBQyxFQUFJO1FBQ2hDLE9BQU8sRUFBRSxjQUFNLE9BQUEsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFsQixDQUFrQjtRQUNqQyxLQUFLLEVBQUU7WUFDTCxTQUFTLENBQUMsZUFBZSxDQUFDLElBQUk7WUFDOUIsS0FBSyxLQUFLLGdCQUFnQixJQUFJLFNBQVMsQ0FBQyxlQUFlLENBQUMsVUFBVTtTQUNuRTtLQUNGLENBQUMsRUFQeUMsQ0FPekMsQ0FBQztJQUVILE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxJQUFJLENBQUM7UUFDOUIsQ0FBQyxDQUFDLElBQUk7UUFDTixDQUFDLENBQUMsQ0FDQSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsU0FBUyxFQUFFLFlBQVksSUFFL0QsS0FBSyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUM7ZUFDekIsYUFBYSxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxNQUFNO2dCQUNoQyxJQUFNLFNBQVMsR0FBRyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7Z0JBQ2xELE1BQU0sQ0FBQyx3Q0FBUyxTQUFTLEVBQVEsQ0FBQztZQUNwQyxDQUFDLENBQUMsQ0FFQSxDQUNQLENBQUM7QUFDTixDQUFDLENBQUM7QUFFRixNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUcsVUFBQyxFQUE0RTtRQUExRSxnQkFBSyxFQUFFLGdCQUFLLEVBQUUsc0NBQWdCLEVBQUUsNEJBQVcsRUFBRSw4QkFBWSxFQUFFLGdDQUFhO0lBQ2hHLElBQUEsVUFBNEUsRUFBMUUsZ0NBQWEsRUFBRSxnREFBcUIsRUFBRSxzQ0FBZ0IsQ0FBcUI7SUFDN0UsSUFBQSxVQUF5QyxFQUF2QyxrQkFBTSxFQUFFLGNBQUksRUFBRSxnQkFBSyxDQUFxQjtJQUVoRCxJQUFNLGNBQWMsR0FBRztRQUNyQixLQUFLLEVBQUUsS0FBSyxDQUFDLGFBQWE7UUFDMUIsWUFBWSxFQUFFLGdCQUFnQjtLQUMvQixDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsd0NBQVMsY0FBYyxJQUFFLFNBQVMsRUFBRSwwQkFBMEI7UUFDM0QsWUFBWSxDQUFDLEVBQUUsTUFBTSxRQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUscUJBQXFCLHVCQUFBLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQztRQUM1RCxnQkFBZ0IsQ0FBQyxFQUFFLGFBQWEsZUFBQSxFQUFFLFdBQVcsYUFBQSxFQUFFLGdCQUFnQixrQkFBQSxFQUFFLENBQUM7UUFDbEUsZ0JBQWdCLENBQUMsRUFBRSxhQUFhLGVBQUEsRUFBRSxZQUFZLGNBQUEsRUFBRSxhQUFhLGVBQUEsRUFBRSxDQUFDO1FBQ2pFLG9CQUFDLEtBQUssSUFBQyxLQUFLLEVBQUUsWUFBWSxHQUFJLENBQzFCLENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider/view-mobile.tsx





var renderMobile = function (_a) {
    var type = _a.type, magazineList = _a.magazineList, column = _a.column;
    return (react["createElement"]("div", { style: [component["c" /* block */].content, image_slider_style.mobileWrap] },
        react["createElement"]("div", { style: [layout["a" /* flexContainer */].noWrap, image_slider_style.mobileWrap.panel] }, Array.isArray(magazineList)
            && magazineList.map(function (magazine, index) { return react["createElement"]("div", { style: image_slider_style.mobileWrap.item },
                react["createElement"](image_slider_item, { key: "image-slide-item-" + index, item: magazine, type: type, column: column })); }))));
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1tb2JpbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LW1vYmlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxlQUFlLE1BQU0sc0JBQXNCLENBQUM7QUFFbkQsT0FBTyxLQUFLLFNBQVMsTUFBTSwwQkFBMEIsQ0FBQztBQUN0RCxPQUFPLEtBQUssTUFBTSxNQUFNLHVCQUF1QixDQUFDO0FBQ2hELE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixNQUFNLENBQUMsSUFBTSxZQUFZLEdBQUcsVUFBQyxFQUE4QjtRQUE1QixjQUFJLEVBQUUsOEJBQVksRUFBRSxrQkFBTTtJQUV2RCxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsVUFBVSxDQUFDO1FBQ3JELDZCQUFLLEtBQUssRUFBRSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLElBRTdELEtBQUssQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDO2VBQ3hCLFlBQVksQ0FBQyxHQUFHLENBQUMsVUFBQyxRQUFRLEVBQUUsS0FBSyxJQUFLLE9BQUEsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSTtnQkFBRSxvQkFBQyxlQUFlLElBQUMsR0FBRyxFQUFFLHNCQUFvQixLQUFPLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxNQUFNLEdBQUksQ0FBTSxFQUExSSxDQUEwSSxDQUFDLENBRWxMLENBQ0YsQ0FDUCxDQUFDO0FBQ0osQ0FBQyxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/image-slider/view.tsx
var view_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};







var renderItemPlaceholder = function (item) { return (react["createElement"]("div", { style: image_slider_style.placeholder.item, key: item },
    react["createElement"](loading_placeholder["a" /* default */], { style: image_slider_style.placeholder.image }))); };
var renderLoadingPlaceholder = function () { return (react["createElement"]("div", { style: image_slider_style.placeholder }, [1, 2, 3, 4].map(renderItemPlaceholder))); };
var renderCustomTitle = function (_a) {
    var title = _a.title, isCustomTitle = _a.isCustomTitle, _b = _a.titleStyle, titleStyle = _b === void 0 ? {} : _b;
    return false === isCustomTitle
        ? null
        : (react["createElement"]("div", null,
            react["createElement"]("div", { style: [component["c" /* block */].heading, component["c" /* block */].heading.multiLine, image_slider_style.desktopTitle, titleStyle] },
                react["createElement"]("div", { style: component["c" /* block */].heading.title.multiLine },
                    react["createElement"]("span", { style: [component["c" /* block */].heading.title.text, component["c" /* block */].heading.title.text.multiLine] }, title)))));
};
var renderView = function (_a) {
    var props = _a.props, state = _a.state, handleMouseHover = _a.handleMouseHover, selectSlide = _a.selectSlide, navLeftSlide = _a.navLeftSlide, navRightSlide = _a.navRightSlide;
    var magazineList = state.magazineList;
    var _b = props, type = _b.type, title = _b.title, showViewMore = _b.showViewMore, column = _b.column, showHeader = _b.showHeader, isCustomTitle = _b.isCustomTitle, titleStyle = _b.titleStyle;
    var renderMobileProps = { type: type, magazineList: magazineList, column: column };
    var renderDesktopProps = {
        props: props,
        state: state,
        navLeftSlide: navLeftSlide,
        navRightSlide: navRightSlide,
        selectSlide: selectSlide,
        handleMouseHover: handleMouseHover
    };
    var switchStyle = {
        MOBILE: function () { return renderMobile(renderMobileProps); },
        DESKTOP: function () { return renderDesktop(renderDesktopProps); }
    };
    var mainBlockProps = {
        title: isCustomTitle ? '' : title,
        showHeader: showHeader,
        showViewMore: showViewMore,
        content: 0 === magazineList.length
            ? renderLoadingPlaceholder()
            : switchStyle[window.DEVICE_VERSION](),
        style: {}
    };
    return (react["createElement"]("div", { style: image_slider_style.container },
        isCustomTitle && renderCustomTitle({ title: title, isCustomTitle: isCustomTitle, titleStyle: titleStyle }),
        react["createElement"](main_block["a" /* default */], view_assign({}, mainBlockProps))));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFHL0IsT0FBTyxTQUFTLE1BQU0sc0NBQXNDLENBQUM7QUFFN0QsT0FBTyxrQkFBa0IsTUFBTSw4QkFBOEIsQ0FBQztBQUU5RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDL0MsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUU3QyxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsT0FBTyxLQUFLLFNBQVMsTUFBTSwwQkFBMEIsQ0FBQztBQUV0RCxJQUFNLHFCQUFxQixHQUFHLFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FDdEMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJO0lBQzNDLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBSSxDQUNsRCxDQUNQLEVBSnVDLENBSXZDLENBQUM7QUFFRixJQUFNLHdCQUF3QixHQUFHLGNBQU0sT0FBQSxDQUNyQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsSUFDMUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FDcEMsQ0FDUCxFQUpzQyxDQUl0QyxDQUFDO0FBRUYsSUFBTSxpQkFBaUIsR0FBRyxVQUFDLEVBQXlDO1FBQXZDLGdCQUFLLEVBQUUsZ0NBQWEsRUFBRSxrQkFBZSxFQUFmLG9DQUFlO0lBQU8sT0FBQSxLQUFLLEtBQUssYUFBYTtRQUM5RixDQUFDLENBQUMsSUFBSTtRQUNOLENBQUMsQ0FBQyxDQUNBO1lBQ0UsNkJBQUssS0FBSyxFQUFFLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxZQUFZLEVBQUUsVUFBVSxDQUFDO2dCQUN0Ryw2QkFBSyxLQUFLLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFNBQVM7b0JBQ2pELDhCQUFNLEtBQUssRUFBRSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFDNUYsS0FBSyxDQUNELENBQ0gsQ0FDRixDQUNGLENBQ1A7QUFac0UsQ0FZdEUsQ0FBQztBQUVKLElBQU0sVUFBVSxHQUFHLFVBQUMsRUFBNEU7UUFBMUUsZ0JBQUssRUFBRSxnQkFBSyxFQUFFLHNDQUFnQixFQUFFLDRCQUFXLEVBQUUsOEJBQVksRUFBRSxnQ0FBYTtJQUNwRixJQUFBLGlDQUFZLENBQXFCO0lBQ25DLElBQUEsVUFBOEYsRUFBNUYsY0FBSSxFQUFFLGdCQUFLLEVBQUUsOEJBQVksRUFBRSxrQkFBTSxFQUFFLDBCQUFVLEVBQUUsZ0NBQWEsRUFBRSwwQkFBVSxDQUFxQjtJQUVyRyxJQUFNLGlCQUFpQixHQUFHLEVBQUUsSUFBSSxNQUFBLEVBQUUsWUFBWSxjQUFBLEVBQUUsTUFBTSxRQUFBLEVBQUUsQ0FBQztJQUN6RCxJQUFNLGtCQUFrQixHQUFHO1FBQ3pCLEtBQUssT0FBQTtRQUNMLEtBQUssT0FBQTtRQUNMLFlBQVksY0FBQTtRQUNaLGFBQWEsZUFBQTtRQUNiLFdBQVcsYUFBQTtRQUNYLGdCQUFnQixrQkFBQTtLQUNqQixDQUFDO0lBRUYsSUFBTSxXQUFXLEdBQUc7UUFDbEIsTUFBTSxFQUFFLGNBQU0sT0FBQSxZQUFZLENBQUMsaUJBQWlCLENBQUMsRUFBL0IsQ0FBK0I7UUFDN0MsT0FBTyxFQUFFLGNBQU0sT0FBQSxhQUFhLENBQUMsa0JBQWtCLENBQUMsRUFBakMsQ0FBaUM7S0FDakQsQ0FBQztJQUVGLElBQU0sY0FBYyxHQUFHO1FBQ3JCLEtBQUssRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSztRQUNqQyxVQUFVLFlBQUE7UUFDVixZQUFZLGNBQUE7UUFDWixPQUFPLEVBQUUsQ0FBQyxLQUFLLFlBQVksQ0FBQyxNQUFNO1lBQ2hDLENBQUMsQ0FBQyx3QkFBd0IsRUFBRTtZQUM1QixDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFBRTtRQUN4QyxLQUFLLEVBQUUsRUFBRTtLQUNWLENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFNBQVM7UUFDeEIsYUFBYSxJQUFJLGlCQUFpQixDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsYUFBYSxlQUFBLEVBQUUsVUFBVSxZQUFBLEVBQUUsQ0FBQztRQUN6RSxvQkFBQyxTQUFTLGVBQUssY0FBYyxFQUFJLENBQzdCLENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/image-slider/component.tsx
var component_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var component_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var component_ImageSlide = /** @class */ (function (_super) {
    component_extends(ImageSlide, _super);
    function ImageSlide(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE(props.data);
        return _this;
    }
    ImageSlide.prototype.componentDidMount = function () {
        this.initDataSlide();
    };
    /**
     * When component will receive new props -> init data for slide
     * @param nextProps prop from parent
     */
    ImageSlide.prototype.componentWillReceiveProps = function (nextProps) {
        this.initDataSlide(nextProps.data);
    };
    /**
     * Init data for slide
     * When : Component will mount OR Component will receive new props
     */
    ImageSlide.prototype.initDataSlide = function (_newMagazineList) {
        var _this = this;
        if (_newMagazineList === void 0) { _newMagazineList = this.state.magazineList; }
        if (true === Object(responsive["b" /* isDesktopVersion */])()) {
            if (false === this.state.firstInit) {
                this.setState({ firstInit: true });
            }
            /**
             * On DESKTOP
             * Init data for magazine slide & magazine slide selected
             */
            var _magazineSlide_1 = [];
            var groupMagazine_1 = {
                id: 0,
                list: []
            };
            /** Assign data into each slider */
            if (_newMagazineList.length > (this.props.column || 3)) {
                Array.isArray(_newMagazineList)
                    && _newMagazineList.map(function (magazine, $index) {
                        groupMagazine_1.id = _magazineSlide_1.length;
                        groupMagazine_1.list.push(magazine);
                        if (groupMagazine_1.list.length === _this.props.column) {
                            _magazineSlide_1.push(Object.assign({}, groupMagazine_1));
                            groupMagazine_1.list = [];
                        }
                    });
            }
            else {
                _magazineSlide_1 = [{ id: 0, list: _newMagazineList }];
            }
            this.setState({
                magazineList: _newMagazineList,
                magazineSlide: _magazineSlide_1,
                magazineSlideSelected: _magazineSlide_1[0] || {}
            });
        }
        else {
            /**
             * On Mobile
             * Only init data for list magazine, not apply slide animation
             */
            this.setState({ magazineList: _newMagazineList });
        }
    };
    /**
     * Navigate slide by button left or right
     * @param _direction `LEFT` or `RIGHT`
     * Will set new index value by @param _direction
     */
    ImageSlide.prototype.navSlide = function (_direction) {
        var _a = this.state, magazineSlide = _a.magazineSlide, countChangeSlide = _a.countChangeSlide;
        /**
         * If navigate to right: increase index value -> set +1 by countChangeSlide
         * If vavigate to left: decrease index value -> set -1 by countChangeSlide
         */
        var newIndexValue = 'left' === _direction ? -1 : 1;
        newIndexValue += countChangeSlide;
        /**
         * Validate new value in range [0, magazineSlide.length - 1]
         */
        newIndexValue = newIndexValue === magazineSlide.length
            ? 0 /** If over max value -> set 0 */
            : (newIndexValue === -1
                /** If under min value -> set magazineSlide.length - 1 */
                ? magazineSlide.length - 1
                : newIndexValue);
        /** Change to new index value */
        this.selectSlide(newIndexValue);
    };
    ImageSlide.prototype.navLeftSlide = function () {
        this.navSlide('left');
    };
    ImageSlide.prototype.navRightSlide = function () {
        this.navSlide('right');
    };
    /**
     * Change slide by set state and setTimeout for animation
     * @param _index new index value
     */
    ImageSlide.prototype.selectSlide = function (_index) {
        var _this = this;
        /** Change background */
        setTimeout(function () { return _this.setState(function (prevState, props) { return ({
            countChangeSlide: _index,
            magazineSlideSelected: prevState.magazineSlide[_index],
        }); }); }, 10);
    };
    ImageSlide.prototype.handleMouseHover = function () {
        var _a = this.props, _b = _a.data, data = _b === void 0 ? [] : _b, _c = _a.column, column = _c === void 0 ? 3 : _c;
        var preLoadImageList = Array.isArray(data)
            ? data
                .filter(function (item, $index) { return $index >= column; })
                .map(function (item) { return item.cover_image.original_url; })
            : [];
        Object(utils_image["b" /* preLoadImage */])(preLoadImageList);
    };
    // shouldComponentUpdate(nextProps: IProps, nextState: IState) {
    //   // if (this.props.data.length !== nextProps.data.length) { return true; }
    //   // if (this.state.countChangeSlide !== nextState.countChangeSlide) { return true; }
    //   // if (this.props.data.length > 0 && this.state.firstInit !== nextState.firstInit) { return true; }
    //   return true;
    // }
    ImageSlide.prototype.render = function () {
        var _this = this;
        var renderViewProps = {
            props: this.props,
            state: this.state,
            handleMouseHover: this.handleMouseHover.bind(this),
            selectSlide: function (index) { return _this.selectSlide(index); },
            navLeftSlide: this.navLeftSlide.bind(this),
            navRightSlide: this.navRightSlide.bind(this)
        };
        return view(renderViewProps);
    };
    ImageSlide.defaultProps = DEFAULT_PROPS;
    ImageSlide = component_decorate([
        radium
    ], ImageSlide);
    return ImageSlide;
}(react["Component"]));
;
/* harmony default export */ var image_slider_component = (component_ImageSlide);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRzdELE9BQU8sRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQzVELE9BQU8sVUFBVSxNQUFNLFFBQVEsQ0FBQztBQUdoQztJQUF5Qiw4QkFBK0I7SUFHdEQsb0JBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDOztJQUN6QyxDQUFDO0lBRUQsc0NBQWlCLEdBQWpCO1FBQ0UsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFFRDs7O09BR0c7SUFDSCw4Q0FBeUIsR0FBekIsVUFBMEIsU0FBUztRQUNqQyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsa0NBQWEsR0FBYixVQUFjLGdCQUFzRDtRQUFwRSxpQkEwQ0M7UUExQ2EsaUNBQUEsRUFBQSxtQkFBK0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZO1FBQ2xFLEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUVoQyxFQUFFLENBQUMsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFZLENBQUMsQ0FBQztZQUFDLENBQUM7WUFDckY7OztlQUdHO1lBQ0gsSUFBSSxnQkFBYyxHQUFlLEVBQUUsQ0FBQztZQUNwQyxJQUFJLGVBQWEsR0FBc0M7Z0JBQ3JELEVBQUUsRUFBRSxDQUFDO2dCQUNMLElBQUksRUFBRSxFQUFFO2FBQ1QsQ0FBQztZQUVGLG1DQUFtQztZQUNuQyxFQUFFLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZELEtBQUssQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUM7dUJBQzFCLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxVQUFDLFFBQVEsRUFBRSxNQUFNO3dCQUN2QyxlQUFhLENBQUMsRUFBRSxHQUFHLGdCQUFjLENBQUMsTUFBTSxDQUFDO3dCQUN6QyxlQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFFbEMsRUFBRSxDQUFDLENBQUMsZUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssS0FBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDOzRCQUNwRCxnQkFBYyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxlQUFhLENBQUMsQ0FBQyxDQUFDOzRCQUN0RCxlQUFhLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQzt3QkFDMUIsQ0FBQztvQkFDSCxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixnQkFBYyxHQUFHLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7WUFDdkQsQ0FBQztZQUVELElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ1osWUFBWSxFQUFFLGdCQUFnQjtnQkFDOUIsYUFBYSxFQUFFLGdCQUFjO2dCQUM3QixxQkFBcUIsRUFBRSxnQkFBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUU7YUFDckMsQ0FBQyxDQUFDO1FBQ2YsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ047OztlQUdHO1lBQ0gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLFlBQVksRUFBRSxnQkFBZ0IsRUFBWSxDQUFDLENBQUM7UUFDOUQsQ0FBQztJQUNILENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsNkJBQVEsR0FBUixVQUFTLFVBQVU7UUFDWCxJQUFBLGVBQWdELEVBQTlDLGdDQUFhLEVBQUUsc0NBQWdCLENBQWdCO1FBRXZEOzs7V0FHRztRQUNILElBQUksYUFBYSxHQUFHLE1BQU0sS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbkQsYUFBYSxJQUFJLGdCQUFnQixDQUFDO1FBRWxDOztXQUVHO1FBQ0gsYUFBYSxHQUFHLGFBQWEsS0FBSyxhQUFhLENBQUMsTUFBTTtZQUNwRCxDQUFDLENBQUMsQ0FBQyxDQUFDLGlDQUFpQztZQUNyQyxDQUFDLENBQUMsQ0FDQSxhQUFhLEtBQUssQ0FBQyxDQUFDO2dCQUNsQix5REFBeUQ7Z0JBQ3pELENBQUMsQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUM7Z0JBQzFCLENBQUMsQ0FBQyxhQUFhLENBQ2xCLENBQUM7UUFFSixnQ0FBZ0M7UUFDaEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRUQsaUNBQVksR0FBWjtRQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDeEIsQ0FBQztJQUVELGtDQUFhLEdBQWI7UUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxnQ0FBVyxHQUFYLFVBQVksTUFBTTtRQUFsQixpQkFPQztRQUxDLHdCQUF3QjtRQUN4QixVQUFVLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxRQUFRLENBQUMsVUFBQyxTQUFTLEVBQUUsS0FBSyxJQUFLLE9BQUEsQ0FBQztZQUNwRCxnQkFBZ0IsRUFBRSxNQUFNO1lBQ3hCLHFCQUFxQixFQUFFLFNBQVMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDO1NBQzVDLENBQUEsRUFIeUMsQ0FHekMsQ0FBQyxFQUhJLENBR0osRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNyQixDQUFDO0lBRUQscUNBQWdCLEdBQWhCO1FBQ1EsSUFBQSxlQUFzQyxFQUFwQyxZQUFTLEVBQVQsOEJBQVMsRUFBRSxjQUFVLEVBQVYsK0JBQVUsQ0FBZ0I7UUFFN0MsSUFBTSxnQkFBZ0IsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztZQUMxQyxDQUFDLENBQUMsSUFBSTtpQkFDSCxNQUFNLENBQUMsVUFBQyxJQUFJLEVBQUUsTUFBTSxJQUFLLE9BQUEsTUFBTSxJQUFJLE1BQU0sRUFBaEIsQ0FBZ0IsQ0FBQztpQkFDMUMsR0FBRyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQTdCLENBQTZCLENBQUM7WUFDN0MsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUVQLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFRCxnRUFBZ0U7SUFDaEUsOEVBQThFO0lBQzlFLHdGQUF3RjtJQUN4Rix3R0FBd0c7SUFFeEcsaUJBQWlCO0lBQ2pCLElBQUk7SUFFSiwyQkFBTSxHQUFOO1FBQUEsaUJBV0M7UUFWQyxJQUFNLGVBQWUsR0FBRztZQUN0QixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLGdCQUFnQixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2xELFdBQVcsRUFBRSxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQXZCLENBQXVCO1lBQzdDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUMsYUFBYSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztTQUM3QyxDQUFDO1FBRUYsTUFBTSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBdEpNLHVCQUFZLEdBQVcsYUFBYSxDQUFDO0lBRHhDLFVBQVU7UUFEZixNQUFNO09BQ0QsVUFBVSxDQXdKZjtJQUFELGlCQUFDO0NBQUEsQUF4SkQsQ0FBeUIsS0FBSyxDQUFDLFNBQVMsR0F3SnZDO0FBQUEsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/image-slider/index.tsx

/* harmony default export */ var image_slider = __webpack_exports__["a"] = (image_slider_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxVQUFVLE1BQU0sYUFBYSxDQUFDO0FBQ3JDLGVBQWUsVUFBVSxDQUFDIn0=

/***/ }),

/***/ 772:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./action/activity-feed.ts + 1 modules
var activity_feed = __webpack_require__(765);

// EXTERNAL MODULE: ./action/like.ts + 1 modules
var like = __webpack_require__(776);

// EXTERNAL MODULE: ./action/modal.ts
var modal = __webpack_require__(84);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/encode.ts
var encode = __webpack_require__(7);

// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(347);

// EXTERNAL MODULE: ./components/ui/loading/index.tsx + 4 modules
var loading = __webpack_require__(748);

// EXTERNAL MODULE: ./components/ui/rating-star/index.tsx + 4 modules
var rating_star = __webpack_require__(763);

// EXTERNAL MODULE: ./components/container/image-slider-community/index.tsx + 11 modules
var image_slider_community = __webpack_require__(778);

// EXTERNAL MODULE: ./utils/auth.ts + 1 modules
var auth = __webpack_require__(20);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// EXTERNAL MODULE: ./utils/html.tsx
var html = __webpack_require__(758);

// EXTERNAL MODULE: ./utils/format.ts
var format = __webpack_require__(116);

// EXTERNAL MODULE: ./constants/application/global.ts
var global = __webpack_require__(24);

// EXTERNAL MODULE: ./constants/application/feedable.ts
var application_feedable = __webpack_require__(774);

// EXTERNAL MODULE: ./constants/application/modal.ts
var application_modal = __webpack_require__(750);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// CONCATENATED MODULE: ./components/container/feed-item/style.tsx



var INLINE_STYLE = {
    tooltip: {
        '.icon-item .tooltip': {
            opacity: 0,
            visibility: 'hidden'
        },
        '.icon-item:hover .tooltip': {
            opacity: 1,
            visibility: 'visible'
        }
    }
};
/* harmony default export */ var feed_item_style = ({
    width: '100%',
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    headerWrap: {
        display: variable["display"].flex,
        marginBottom: 15,
        item: {
            container: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        paddingTop: 10,
                        paddingBottom: 10,
                        marginBottom: 10,
                        borderBottom: "1px solid " + variable["colorF0"],
                    }],
                DESKTOP: [{
                        paddingTop: 20,
                        paddingBottom: 10,
                        marginBottom: 10,
                        borderBottom: "1px solid " + variable["colorF0"],
                    }],
                GENERAL: [{
                        background: variable["colorWhite"]
                    }]
            }),
            lastChild: {
                marginBottom: 0
            },
            small: {
                boxShadow: 'none',
                border: "1px solid " + variable["colorB0"],
                marginBottom: 20,
                last: {
                    marginBottom: 0,
                }
            },
            info: {
                container: [
                    layout["a" /* flexContainer */].left, {
                        width: '100%',
                        paddingLeft: 20,
                        paddingRight: 20,
                    }
                ],
                avatar: {
                    width: 40,
                    minWidth: 40,
                    height: 40,
                    borderRadius: 25,
                    marginRight: 10,
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    backgroundColor: variable["colorE5"],
                    cursor: 'pointer',
                    display: variable["display"].block,
                    small: {
                        width: 30,
                        minWidth: 30,
                        height: 30,
                        borderRadius: '50%',
                    }
                },
                username: {
                    paddingRight: 15,
                    marginBottom: 5,
                    textAlign: 'left',
                },
                detail: {
                    flex: 10,
                    display: variable["display"].flex,
                    flexDirection: 'column',
                    textOverflow: 'ellipsis',
                    justifyContent: 'center',
                    username: {
                        fontFamily: variable["fontAvenirDemiBold"],
                        textOverflow: 'ellipsis',
                        whiteSpace: 'nowrap',
                        fontSize: 14,
                        lineHeight: '22px',
                        marginRight: 5,
                        cursor: 'pointer',
                        color: variable["colorBlack"],
                        position: variable["position"].relative
                    },
                    ratingGroup: {
                        display: variable["display"].flex,
                        alignItems: 'center',
                        rating: {
                            marginLeft: -3,
                            marginRight: 5
                        },
                        date: {
                            display: variable["display"].block,
                            lineHeight: '23px',
                            height: 18,
                            color: variable["color97"],
                            cursor: 'pointer'
                        }
                    }
                },
                descGroup: {
                    paddingLeft: 20,
                    paddingRight: 20,
                    marginBottom: 10,
                },
                description: {
                    container: {
                        fontSize: 14,
                        overflow: 'hidden',
                        lineHeight: '22px',
                        textAlign: 'justify',
                        display: variable["display"].inline
                    },
                    viewMore: {
                        fontFamily: variable["fontAvenirDemiBold"],
                        fontSize: 15,
                        cursor: 'pointer'
                    },
                }
            },
            image: function (imgUrl) {
                if (imgUrl === void 0) { imgUrl = ''; }
                return {
                    backgroundImage: "url(" + imgUrl + ")",
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                    width: '100%',
                    height: '100%'
                };
            },
            imageContent: {
                // maxWidth: '100%',
                // minWidth: '100%',
                // height: 'auto',
                // marginTop: 10,
                // marginRight: 10,
                // marginBottom: 10,
                // marginLeft: 10,
                display: variable["display"].block,
                width: '100%',
                height: '100%',
                objectFit: 'cover',
                cursor: 'pointer'
            },
            fullHeight: {
                height: '100%'
            },
            onePicture: {
                height: 'auto'
            },
            text: {
                color: variable["colorBlack06"]
            },
            inner: {
                width: 15
            },
            likeCommentCount: {
                marginBottom: 10,
                display: variable["display"].flex,
                borderBottom: "1px solid " + variable["colorBlack005"],
                width: '100%',
                alignItems: 'center'
            },
            likeCount: {
                display: variable["display"].flex,
                flex: 1,
                alignItems: 'center'
            },
            likeCountEmpty: {
                flex: 1
            },
            commentCount: {
                fontFamily: variable["fontAvenirRegular"],
                fontSize: 13,
                lineHeight: '30px',
                color: variable["colorBlack06"],
                flex: 1,
                textAlign: 'right',
                paddingLeft: 3,
                paddingRight: 3,
            },
            likeCommentIconGroup: {
                left: {
                    display: variable["display"].flex
                },
                right: {},
                container: {
                    paddingLeft: 13,
                    paddingRight: 13,
                    display: variable["display"].flex,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                },
                icon: {
                    display: variable["display"].flex,
                    alignItems: 'center',
                    justifyContent: 'center',
                    flex: 1,
                    cursor: 'pointer'
                },
                innerIconLike: {
                    width: 22
                },
                innerIconComment: {
                    width: 21,
                    position: variable["position"].relative,
                    top: 3
                },
                innerIconFly: {
                    width: 24,
                    top: 2,
                    position: variable["position"].relative
                },
                innerIconHeart: {
                    width: 16,
                    top: 3,
                    right: -4,
                    position: variable["position"].relative
                }
            },
            countingGroup: {
                paddingLeft: 20,
                paddingRight: 20,
                paddingTop: 10,
                paddingBottom: 10,
                userList: {
                    height: 18,
                    display: variable["display"].inlineFlex,
                    alignItems: 'center',
                    justifyContent: 'center',
                    verticalAlign: 'top',
                    marginRight: 10,
                },
                userItem: function (index) { return ({
                    width: 15,
                    height: 15,
                    backgroundColor: variable["randomColorList"](-1),
                    backgroundSize: 'cover',
                    borderRadius: '50%',
                    display: variable["display"].inlineBlock,
                    marginRight: -7,
                    border: "1px solid " + variable["colorWhite"],
                    cursor: 'pointer',
                }); },
                text: {
                    fontSize: 12,
                    lineHeight: '18px',
                    color: variable["color97"],
                    cursor: 'pointer',
                }
            },
            commentGroup: {
                paddingTop: 10,
                container: {
                    paddingLeft: 20,
                    paddingRight: 20,
                    display: variable["display"].flex,
                    paddingTop: 10,
                },
                contenGroup: {
                    display: variable["display"].block,
                    padding: '4px 10px',
                    background: variable["colorF0"],
                    borderRadius: 15,
                    date: {
                        fontFamily: variable["fontAvenirRegular"],
                        fontSize: 11,
                        color: variable["color75"],
                        marginBottom: 5
                    },
                    comment: {
                        fontFamily: variable["fontAvenirRegular"],
                        fontSize: 14,
                        lineHeight: '22px',
                        color: variable["color2E"],
                        textAlign: 'justify',
                        wordBreak: "break-word"
                    }
                }
            },
            inputCommentGroup: {
                display: variable["display"].flex,
                alignItems: 'center',
                paddingTop: 10,
                paddingBottom: 10,
                marginLeft: 20,
                marginRight: 20,
                avatar: {
                    width: 30,
                    minWidth: 30,
                    height: 30,
                    borderRadius: 25,
                    marginRight: 10,
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    backgroundColor: variable["colorE5"],
                    marginLeft: 0
                },
                input: {
                    height: 30,
                    width: '100%',
                    lineHeight: '30px',
                    textAlign: 'left',
                    color: variable["color4D"],
                    fontSize: 12,
                    fontFamily: variable["fontAvenirMedium"]
                },
                inputText: {
                    width: '100%',
                    border: 'none',
                    outline: 'none',
                    boxShadow: 'none',
                    paddingLeft: 10,
                    paddingRight: 10,
                    height: 28,
                    lineHeight: '28px',
                    fontSize: 12,
                    color: variable["colorBlack"],
                    background: 'transparent',
                    whiteSpace: 'nowrap',
                    maxWidth: '100%',
                    overflow: 'hidden',
                    margin: 0,
                    borderRadius: 0
                },
                sendComment: {
                    display: variable["display"].flex,
                    alignItems: 'center',
                    width: 50,
                    minWidth: 50,
                    fontSize: 12,
                    justifyContent: 'center',
                    cursor: 'pointer',
                    fontFamily: variable["fontAvenirMedium"],
                    lineHeight: '28px',
                    color: variable["colorPink"],
                    textTransform: 'uppercase'
                },
                commentInputGroup: {
                    width: '100%',
                    height: 30,
                    border: "1px solid " + variable["colorD2"],
                    borderRadius: 15,
                    display: variable["display"].flex
                },
            }
        },
        imgProduct: {
            container: {
                height: 40,
                display: variable["display"].flex,
                alignItems: 'center',
                justifyContent: 'center'
            },
            img: {
                height: '100%',
                width: 'auto'
            }
        },
        viewDetail: {
            container: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{}],
                DESKTOP: [{}],
                GENERAL: [{
                        height: 30,
                        border: "1px solid " + variable["colorBlack03"],
                        borderRadius: 5,
                        display: variable["display"].flex,
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingLeft: 10,
                        paddingRight: 10,
                        color: variable["colorBlack08"]
                    }]
            })
        }
    },
    videoContainer: {
        position: variable["position"].relative,
        width: '100%',
        paddingTop: '56.25%',
        cursor: 'pointer',
        thumbnail: function (backgroundImageUrl) { return ({
            backgroundImage: "url(" + backgroundImageUrl,
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            width: '100%',
            height: '100%',
            backgroundColor: variable["colorF7"],
            position: variable["position"].absolute,
            top: 0,
            marginBottom: 10
        }); },
        videoWrap: {
            width: '100%',
            height: '100%',
            marginBottom: 10,
            position: variable["position"].absolute,
            top: 0,
            video: {
                transition: variable["transitionNormal"],
                backgroundColor: variable["colorF7"],
                width: '100%',
                height: '100%'
            }
        }
    },
    icon: {
        position: variable["position"].absolute,
        top: '50%',
        left: '50%',
        color: variable["colorWhite"],
        transform: 'translate(-50%, -50%)',
        zIndex: variable["zIndex2"],
        inner: {
            width: 60,
            height: 60
        }
    },
    imgSlider: {
        display: variable["display"].block,
        whiteSpace: 'nowrap',
        overflow: 'auto hidden',
        img: {
            width: 'auto',
            maxHeight: 300,
            minHeight: 300,
            display: variable["display"].inline,
            marginTop: 10,
            marginBottom: 10,
            marginRight: 20,
            borderRadius: 3,
            boxShadow: variable["shadowBlurSort"]
        }
    },
    customStyleLoading: {
        height: 80
    },
    pictureList: {
        paddingTop: '100%',
        position: variable["position"].relative,
        marginBottom: 10,
        display: variable["display"].block,
        pictureWrap: {
            position: variable["position"].absolute,
            top: 0,
            left: 0,
            width: '100%',
            height: '100%'
        }
    },
    pictures: {
        onePicture: {
            minWith: '100%',
            maxWidth: '100%',
            height: '100%'
        },
        twoPicture: {
            container: function (isHorizontal) { return ({
                display: variable["display"].flex,
                flexDirection: isHorizontal ? 'column' : 'row',
                marginLeft: isHorizontal ? 0 : -1,
                marginRight: isHorizontal ? 0 : -1,
                marginTop: isHorizontal ? -1 : 0,
                marginBottom: isHorizontal ? -1 : 0,
                height: '100%'
            }); },
            horizontal: {
                width: '100%',
                height: '50%',
                paddingTop: 1,
                paddingBottom: 1
            },
            vertical: {
                width: '50%',
                paddingLeft: 1,
                paddingRight: 1
            }
        },
        threePicture: {
            container: function (isHorizontal) { return ({
                display: variable["display"].flex,
                flexDirection: isHorizontal ? 'column' : 'row',
                justifyContent: isHorizontal ? 'center' : 'space-between',
                width: '100%',
                height: '100%'
            }); },
            horizontal: {
                flex: 1,
                marginBottom: 1,
                height: '50%',
            },
            vertical: {
                flex: 1,
                marginRight: 1
            },
            pictureGroup: {
                container: function (isHorizontal) { return ({
                    display: variable["display"].flex,
                    flexDirection: isHorizontal ? 'row' : 'column',
                    flex: 1,
                    justifyContent: isHorizontal ? 'space-between' : 'center',
                    height: isHorizontal ? '' : '100%',
                    marginLeft: isHorizontal ? -1 : 1,
                    marginRight: isHorizontal ? -1 : 0,
                    marginTop: isHorizontal ? 1 : -1,
                    marginBottom: isHorizontal ? 0 : -1,
                }); },
                horizontal: {
                    flex: 1,
                    marginLeft: 1,
                    marginRight: 1
                },
                vertical: {
                    width: '100%',
                    height: '50%',
                    flex: 1,
                    paddingBottom: 1,
                    paddingTop: 1
                }
            },
        },
        fourPicture: {
            container: function (isHorizontal) { return ({
                display: variable["display"].flex,
                flexDirection: isHorizontal ? 'column' : 'row',
                justifyContent: isHorizontal ? 'center' : 'space-between',
                width: '100%',
                height: '100%'
            }); },
            horizontal: {
                flex: 2,
                marginBottom: 2
            },
            vertical: {
                flex: 2,
                border: "1px solid " + variable["colorWhite"]
            },
            pictureGroup: {
                container: function (isHorizontal) { return ({
                    display: variable["display"].flex,
                    flexDirection: isHorizontal ? 'row' : 'column',
                    flex: 1,
                    justifyContent: isHorizontal ? 'space-between' : 'center',
                    height: isHorizontal ? '' : '100%',
                    marginLeft: isHorizontal ? -1 : 0,
                    marginRight: isHorizontal ? -1 : 0,
                }); },
                horizontal: {
                    flex: 1,
                    marginLeft: 1,
                    marginRight: 1
                },
                vertical: {
                    width: '100%',
                    height: '33.33%',
                    flex: 1,
                    border: "1px solid " + variable["colorWhite"]
                }
            },
        },
        viewMore: {
            container: function (isHorizontal) { return ({
                position: variable["position"].relative,
                flex: isHorizontal ? 1 : '',
                width: '100%',
                marginLeft: isHorizontal ? 1 : 0,
                marginRight: isHorizontal ? 1 : 0,
                height: isHorizontal ? 'auto' : '33.33%',
                paddingTop: isHorizontal ? '' : 1,
                paddingBottom: isHorizontal ? '' : 1,
                cursor: 'pointer'
            }); },
            imgViewMore: function (isHorizontal) {
                return !isHorizontal
                    ? {
                        paddingTop: 0,
                        paddingRight: 0,
                        paddingBottom: 0,
                        paddingLeft: 0,
                        height: '100%'
                    }
                    : {
                        width: '100%',
                        height: '100%',
                        margin: 0,
                        padding: 0
                    };
            },
            num: {
                position: variable["position"].absolute,
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                fontSize: 36,
                fontFamily: variable["fontAvenirDemiBold"],
                color: variable["colorWhite"],
                zIndex: variable["zIndex9"],
            },
            bg: {
                position: variable["position"].absolute,
                top: 0,
                width: '100%',
                height: '100%',
                zIndex: variable["zIndex8"],
                backgroundColor: variable["colorBlack04"]
            }
        }
    },
    tooltip: {
        position: variable["position"].absolute,
        top: -3,
        right: -10,
        transform: 'translateX(100%)',
        group: {
            height: '100%',
            position: variable["position"].relative,
            text: {
                padding: '3px 8px',
                color: variable["colorWhite"],
                background: variable["colorBlack"],
                borderRadius: 3,
                boxShadow: variable["shadowBlurSort"],
                whiteSpace: 'nowrap',
                lineHeight: '20px',
                fontSize: 12
            },
            icon: {
                position: variable["position"].absolute,
                top: 7,
                left: -5,
                height: 5,
                width: 5,
                borderWidth: 6,
                borderStyle: 'solid',
                borderColor: variable["colorTransparent"] + " " + variable["colorBlack"] + " " + variable["colorTransparent"] + "  " + variable["colorTransparent"],
                transform: 'translateX(-50%)'
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEtBQUssTUFBTSxNQUFNLHVCQUF1QixDQUFDO0FBQ2hELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUV6RCxNQUFNLENBQUMsSUFBTSxZQUFZLEdBQUc7SUFDMUIsT0FBTyxFQUFFO1FBQ1AscUJBQXFCLEVBQUU7WUFDckIsT0FBTyxFQUFFLENBQUM7WUFDVixVQUFVLEVBQUUsUUFBUTtTQUNyQjtRQUVELDJCQUEyQixFQUFFO1lBQzNCLE9BQU8sRUFBRSxDQUFDO1lBQ1YsVUFBVSxFQUFFLFNBQVM7U0FDdEI7S0FDRjtDQUNGLENBQUM7QUFFRixlQUFlO0lBQ2IsS0FBSyxFQUFFLE1BQU07SUFDYixVQUFVLEVBQUUsRUFBRTtJQUNkLFlBQVksRUFBRSxFQUFFO0lBQ2hCLGFBQWEsRUFBRSxFQUFFO0lBQ2pCLFdBQVcsRUFBRSxFQUFFO0lBRWYsVUFBVSxFQUFFO1FBQ1YsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUM5QixZQUFZLEVBQUUsRUFBRTtRQUVoQixJQUFJLEVBQUU7WUFDSixTQUFTLEVBQUUsWUFBWSxDQUFDO2dCQUN0QixNQUFNLEVBQUUsQ0FBQzt3QkFDUCxVQUFVLEVBQUUsRUFBRTt3QkFDZCxhQUFhLEVBQUUsRUFBRTt3QkFDakIsWUFBWSxFQUFFLEVBQUU7d0JBQ2hCLFlBQVksRUFBRSxlQUFhLFFBQVEsQ0FBQyxPQUFTO3FCQUM5QyxDQUFDO2dCQUVGLE9BQU8sRUFBRSxDQUFDO3dCQUNSLFVBQVUsRUFBRSxFQUFFO3dCQUNkLGFBQWEsRUFBRSxFQUFFO3dCQUNqQixZQUFZLEVBQUUsRUFBRTt3QkFDaEIsWUFBWSxFQUFFLGVBQWEsUUFBUSxDQUFDLE9BQVM7cUJBQzlDLENBQUM7Z0JBRUYsT0FBTyxFQUFFLENBQUM7d0JBQ1IsVUFBVSxFQUFFLFFBQVEsQ0FBQyxVQUFVO3FCQUNoQyxDQUFDO2FBQ0gsQ0FBQztZQUVGLFNBQVMsRUFBRTtnQkFDVCxZQUFZLEVBQUUsQ0FBQzthQUNoQjtZQUVELEtBQUssRUFBRTtnQkFDTCxTQUFTLEVBQUUsTUFBTTtnQkFDakIsTUFBTSxFQUFFLGVBQWEsUUFBUSxDQUFDLE9BQVM7Z0JBQ3ZDLFlBQVksRUFBRSxFQUFFO2dCQUVoQixJQUFJLEVBQUU7b0JBQ0osWUFBWSxFQUFFLENBQUM7aUJBQ2hCO2FBQ0Y7WUFFRCxJQUFJLEVBQUU7Z0JBQ0osU0FBUyxFQUFFO29CQUNULE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFO3dCQUN6QixLQUFLLEVBQUUsTUFBTTt3QkFDYixXQUFXLEVBQUUsRUFBRTt3QkFDZixZQUFZLEVBQUUsRUFBRTtxQkFDakI7aUJBQUM7Z0JBRUosTUFBTSxFQUFFO29CQUNOLEtBQUssRUFBRSxFQUFFO29CQUNULFFBQVEsRUFBRSxFQUFFO29CQUNaLE1BQU0sRUFBRSxFQUFFO29CQUNWLFlBQVksRUFBRSxFQUFFO29CQUNoQixXQUFXLEVBQUUsRUFBRTtvQkFDZixrQkFBa0IsRUFBRSxRQUFRO29CQUM1QixjQUFjLEVBQUUsT0FBTztvQkFDdkIsZUFBZSxFQUFFLFFBQVEsQ0FBQyxPQUFPO29CQUNqQyxNQUFNLEVBQUUsU0FBUztvQkFDakIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztvQkFFL0IsS0FBSyxFQUFFO3dCQUNMLEtBQUssRUFBRSxFQUFFO3dCQUNULFFBQVEsRUFBRSxFQUFFO3dCQUNaLE1BQU0sRUFBRSxFQUFFO3dCQUNWLFlBQVksRUFBRSxLQUFLO3FCQUNwQjtpQkFDRjtnQkFFRCxRQUFRLEVBQUU7b0JBQ1IsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLFlBQVksRUFBRSxDQUFDO29CQUNmLFNBQVMsRUFBRSxNQUFNO2lCQUNsQjtnQkFFRCxNQUFNLEVBQUU7b0JBQ04sSUFBSSxFQUFFLEVBQUU7b0JBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtvQkFDOUIsYUFBYSxFQUFFLFFBQVE7b0JBQ3ZCLFlBQVksRUFBRSxVQUFVO29CQUN4QixjQUFjLEVBQUUsUUFBUTtvQkFFeEIsUUFBUSxFQUFFO3dCQUNSLFVBQVUsRUFBRSxRQUFRLENBQUMsa0JBQWtCO3dCQUN2QyxZQUFZLEVBQUUsVUFBVTt3QkFDeEIsVUFBVSxFQUFFLFFBQVE7d0JBQ3BCLFFBQVEsRUFBRSxFQUFFO3dCQUNaLFVBQVUsRUFBRSxNQUFNO3dCQUNsQixXQUFXLEVBQUUsQ0FBQzt3QkFDZCxNQUFNLEVBQUUsU0FBUzt3QkFDakIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO3dCQUMxQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO3FCQUNyQztvQkFFRCxXQUFXLEVBQUU7d0JBQ1gsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTt3QkFDOUIsVUFBVSxFQUFFLFFBQVE7d0JBRXBCLE1BQU0sRUFBRTs0QkFDTixVQUFVLEVBQUUsQ0FBQyxDQUFDOzRCQUNkLFdBQVcsRUFBRSxDQUFDO3lCQUNmO3dCQUVELElBQUksRUFBRTs0QkFDSixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLOzRCQUMvQixVQUFVLEVBQUUsTUFBTTs0QkFDbEIsTUFBTSxFQUFFLEVBQUU7NEJBQ1YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPOzRCQUN2QixNQUFNLEVBQUUsU0FBUzt5QkFDbEI7cUJBQ0Y7aUJBQ0Y7Z0JBRUQsU0FBUyxFQUFFO29CQUNULFdBQVcsRUFBRSxFQUFFO29CQUNmLFlBQVksRUFBRSxFQUFFO29CQUNoQixZQUFZLEVBQUUsRUFBRTtpQkFDakI7Z0JBRUQsV0FBVyxFQUFFO29CQUNYLFNBQVMsRUFBRTt3QkFDVCxRQUFRLEVBQUUsRUFBRTt3QkFDWixRQUFRLEVBQUUsUUFBUTt3QkFDbEIsVUFBVSxFQUFFLE1BQU07d0JBQ2xCLFNBQVMsRUFBRSxTQUFTO3dCQUNwQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNO3FCQUNqQztvQkFFRCxRQUFRLEVBQUU7d0JBQ1IsVUFBVSxFQUFFLFFBQVEsQ0FBQyxrQkFBa0I7d0JBQ3ZDLFFBQVEsRUFBRSxFQUFFO3dCQUNaLE1BQU0sRUFBRSxTQUFTO3FCQUNsQjtpQkFDRjthQUNGO1lBRUQsS0FBSyxFQUFFLFVBQUMsTUFBVztnQkFBWCx1QkFBQSxFQUFBLFdBQVc7Z0JBQ2pCLE1BQU0sQ0FBQztvQkFDTCxlQUFlLEVBQUUsU0FBTyxNQUFNLE1BQUc7b0JBQ2pDLGtCQUFrQixFQUFFLFFBQVE7b0JBQzVCLGNBQWMsRUFBRSxPQUFPO29CQUN2QixnQkFBZ0IsRUFBRSxXQUFXO29CQUM3QixLQUFLLEVBQUUsTUFBTTtvQkFDYixNQUFNLEVBQUUsTUFBTTtpQkFDZixDQUFBO1lBQ0gsQ0FBQztZQUVELFlBQVksRUFBRTtnQkFDWixvQkFBb0I7Z0JBQ3BCLG9CQUFvQjtnQkFDcEIsa0JBQWtCO2dCQUNsQixpQkFBaUI7Z0JBQ2pCLG1CQUFtQjtnQkFDbkIsb0JBQW9CO2dCQUNwQixrQkFBa0I7Z0JBQ2xCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7Z0JBQy9CLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxNQUFNO2dCQUNkLFNBQVMsRUFBRSxPQUFPO2dCQUNsQixNQUFNLEVBQUUsU0FBUzthQUNsQjtZQUVELFVBQVUsRUFBRTtnQkFDVixNQUFNLEVBQUUsTUFBTTthQUNmO1lBRUQsVUFBVSxFQUFFO2dCQUNWLE1BQU0sRUFBRSxNQUFNO2FBQ2Y7WUFFRCxJQUFJLEVBQUU7Z0JBQ0osS0FBSyxFQUFFLFFBQVEsQ0FBQyxZQUFZO2FBQzdCO1lBRUQsS0FBSyxFQUFFO2dCQUNMLEtBQUssRUFBRSxFQUFFO2FBQ1Y7WUFFRCxnQkFBZ0IsRUFBRTtnQkFDaEIsWUFBWSxFQUFFLEVBQUU7Z0JBQ2hCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLFlBQVksRUFBRSxlQUFhLFFBQVEsQ0FBQyxhQUFlO2dCQUNuRCxLQUFLLEVBQUUsTUFBTTtnQkFDYixVQUFVLEVBQUUsUUFBUTthQUNyQjtZQUVELFNBQVMsRUFBRTtnQkFDVCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixJQUFJLEVBQUUsQ0FBQztnQkFDUCxVQUFVLEVBQUUsUUFBUTthQUNyQjtZQUVELGNBQWMsRUFBRTtnQkFDZCxJQUFJLEVBQUUsQ0FBQzthQUNSO1lBRUQsWUFBWSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxRQUFRLENBQUMsaUJBQWlCO2dCQUN0QyxRQUFRLEVBQUUsRUFBRTtnQkFDWixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxZQUFZO2dCQUM1QixJQUFJLEVBQUUsQ0FBQztnQkFDUCxTQUFTLEVBQUUsT0FBTztnQkFDbEIsV0FBVyxFQUFFLENBQUM7Z0JBQ2QsWUFBWSxFQUFFLENBQUM7YUFDaEI7WUFFRCxvQkFBb0IsRUFBRTtnQkFDcEIsSUFBSSxFQUFFO29CQUNKLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7aUJBQy9CO2dCQUVELEtBQUssRUFBRSxFQUFFO2dCQUVULFNBQVMsRUFBRTtvQkFDVCxXQUFXLEVBQUUsRUFBRTtvQkFDZixZQUFZLEVBQUUsRUFBRTtvQkFDaEIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtvQkFDOUIsVUFBVSxFQUFFLFFBQVE7b0JBQ3BCLGNBQWMsRUFBRSxlQUFlO2lCQUNoQztnQkFFRCxJQUFJLEVBQUU7b0JBQ0osT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtvQkFDOUIsVUFBVSxFQUFFLFFBQVE7b0JBQ3BCLGNBQWMsRUFBRSxRQUFRO29CQUN4QixJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsU0FBUztpQkFDbEI7Z0JBRUQsYUFBYSxFQUFFO29CQUNiLEtBQUssRUFBRSxFQUFFO2lCQUNWO2dCQUVELGdCQUFnQixFQUFFO29CQUNoQixLQUFLLEVBQUUsRUFBRTtvQkFDVCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUNwQyxHQUFHLEVBQUUsQ0FBQztpQkFDUDtnQkFFRCxZQUFZLEVBQUU7b0JBQ1osS0FBSyxFQUFFLEVBQUU7b0JBQ1QsR0FBRyxFQUFFLENBQUM7b0JBQ04sUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtpQkFDckM7Z0JBRUQsY0FBYyxFQUFFO29CQUNkLEtBQUssRUFBRSxFQUFFO29CQUNULEdBQUcsRUFBRSxDQUFDO29CQUNOLEtBQUssRUFBRSxDQUFDLENBQUM7b0JBQ1QsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtpQkFDckM7YUFDRjtZQUVELGFBQWEsRUFBRTtnQkFDYixXQUFXLEVBQUUsRUFBRTtnQkFDZixZQUFZLEVBQUUsRUFBRTtnQkFDaEIsVUFBVSxFQUFFLEVBQUU7Z0JBQ2QsYUFBYSxFQUFFLEVBQUU7Z0JBRWpCLFFBQVEsRUFBRTtvQkFDUixNQUFNLEVBQUUsRUFBRTtvQkFDVixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFVO29CQUNwQyxVQUFVLEVBQUUsUUFBUTtvQkFDcEIsY0FBYyxFQUFFLFFBQVE7b0JBQ3hCLGFBQWEsRUFBRSxLQUFLO29CQUNwQixXQUFXLEVBQUUsRUFBRTtpQkFDaEI7Z0JBRUQsUUFBUSxFQUFFLFVBQUMsS0FBSyxJQUFLLE9BQUEsQ0FBQztvQkFDcEIsS0FBSyxFQUFFLEVBQUU7b0JBQ1QsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsZUFBZSxFQUFFLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzdDLGNBQWMsRUFBRSxPQUFPO29CQUN2QixZQUFZLEVBQUUsS0FBSztvQkFDbkIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztvQkFDckMsV0FBVyxFQUFFLENBQUMsQ0FBQztvQkFDZixNQUFNLEVBQUUsZUFBYSxRQUFRLENBQUMsVUFBWTtvQkFDMUMsTUFBTSxFQUFFLFNBQVM7aUJBQ2xCLENBQUMsRUFWbUIsQ0FVbkI7Z0JBRUYsSUFBSSxFQUFFO29CQUNKLFFBQVEsRUFBRSxFQUFFO29CQUNaLFVBQVUsRUFBRSxNQUFNO29CQUNsQixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87b0JBQ3ZCLE1BQU0sRUFBRSxTQUFTO2lCQUNsQjthQUNGO1lBRUQsWUFBWSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxFQUFFO2dCQUVkLFNBQVMsRUFBRTtvQkFDVCxXQUFXLEVBQUUsRUFBRTtvQkFDZixZQUFZLEVBQUUsRUFBRTtvQkFDaEIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtvQkFDOUIsVUFBVSxFQUFFLEVBQUU7aUJBQ2Y7Z0JBRUQsV0FBVyxFQUFFO29CQUNYLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7b0JBQy9CLE9BQU8sRUFBRSxVQUFVO29CQUNuQixVQUFVLEVBQUUsUUFBUSxDQUFDLE9BQU87b0JBQzVCLFlBQVksRUFBRSxFQUFFO29CQUVoQixJQUFJLEVBQUU7d0JBQ0osVUFBVSxFQUFFLFFBQVEsQ0FBQyxpQkFBaUI7d0JBQ3RDLFFBQVEsRUFBRSxFQUFFO3dCQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsT0FBTzt3QkFDdkIsWUFBWSxFQUFFLENBQUM7cUJBQ2hCO29CQUVELE9BQU8sRUFBRTt3QkFDUCxVQUFVLEVBQUUsUUFBUSxDQUFDLGlCQUFpQjt3QkFDdEMsUUFBUSxFQUFFLEVBQUU7d0JBQ1osVUFBVSxFQUFFLE1BQU07d0JBQ2xCLEtBQUssRUFBRSxRQUFRLENBQUMsT0FBTzt3QkFDdkIsU0FBUyxFQUFFLFNBQVM7d0JBQ3BCLFNBQVMsRUFBRSxZQUFZO3FCQUN4QjtpQkFDRjthQUNGO1lBRUQsaUJBQWlCLEVBQUU7Z0JBQ2pCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLFVBQVUsRUFBRSxRQUFRO2dCQUNwQixVQUFVLEVBQUUsRUFBRTtnQkFDZCxhQUFhLEVBQUUsRUFBRTtnQkFDakIsVUFBVSxFQUFFLEVBQUU7Z0JBQ2QsV0FBVyxFQUFFLEVBQUU7Z0JBRWYsTUFBTSxFQUFFO29CQUNOLEtBQUssRUFBRSxFQUFFO29CQUNULFFBQVEsRUFBRSxFQUFFO29CQUNaLE1BQU0sRUFBRSxFQUFFO29CQUNWLFlBQVksRUFBRSxFQUFFO29CQUNoQixXQUFXLEVBQUUsRUFBRTtvQkFDZixrQkFBa0IsRUFBRSxRQUFRO29CQUM1QixjQUFjLEVBQUUsT0FBTztvQkFDdkIsZUFBZSxFQUFFLFFBQVEsQ0FBQyxPQUFPO29CQUNqQyxVQUFVLEVBQUUsQ0FBQztpQkFDZDtnQkFFRCxLQUFLLEVBQUU7b0JBQ0wsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsS0FBSyxFQUFFLE1BQU07b0JBQ2IsVUFBVSxFQUFFLE1BQU07b0JBQ2xCLFNBQVMsRUFBRSxNQUFNO29CQUNqQixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87b0JBQ3ZCLFFBQVEsRUFBRSxFQUFFO29CQUNaLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO2lCQUN0QztnQkFFRCxTQUFTLEVBQUU7b0JBQ1QsS0FBSyxFQUFFLE1BQU07b0JBQ2IsTUFBTSxFQUFFLE1BQU07b0JBQ2QsT0FBTyxFQUFFLE1BQU07b0JBQ2YsU0FBUyxFQUFFLE1BQU07b0JBQ2pCLFdBQVcsRUFBRSxFQUFFO29CQUNmLFlBQVksRUFBRSxFQUFFO29CQUNoQixNQUFNLEVBQUUsRUFBRTtvQkFDVixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsUUFBUSxFQUFFLEVBQUU7b0JBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsYUFBYTtvQkFDekIsVUFBVSxFQUFFLFFBQVE7b0JBQ3BCLFFBQVEsRUFBRSxNQUFNO29CQUNoQixRQUFRLEVBQUUsUUFBUTtvQkFDbEIsTUFBTSxFQUFFLENBQUM7b0JBQ1QsWUFBWSxFQUFFLENBQUM7aUJBQ2hCO2dCQUVELFdBQVcsRUFBRTtvQkFDWCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO29CQUM5QixVQUFVLEVBQUUsUUFBUTtvQkFDcEIsS0FBSyxFQUFFLEVBQUU7b0JBQ1QsUUFBUSxFQUFFLEVBQUU7b0JBQ1osUUFBUSxFQUFFLEVBQUU7b0JBQ1osY0FBYyxFQUFFLFFBQVE7b0JBQ3hCLE1BQU0sRUFBRSxTQUFTO29CQUNqQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsVUFBVSxFQUFFLE1BQU07b0JBQ2xCLEtBQUssRUFBRSxRQUFRLENBQUMsU0FBUztvQkFDekIsYUFBYSxFQUFFLFdBQVc7aUJBQzNCO2dCQUVELGlCQUFpQixFQUFFO29CQUNqQixLQUFLLEVBQUUsTUFBTTtvQkFDYixNQUFNLEVBQUUsRUFBRTtvQkFDVixNQUFNLEVBQUUsZUFBYSxRQUFRLENBQUMsT0FBUztvQkFDdkMsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7aUJBQy9CO2FBQ0Y7U0FDRjtRQUVELFVBQVUsRUFBRTtZQUNWLFNBQVMsRUFBRTtnQkFDVCxNQUFNLEVBQUUsRUFBRTtnQkFDVixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixVQUFVLEVBQUUsUUFBUTtnQkFDcEIsY0FBYyxFQUFFLFFBQVE7YUFDekI7WUFFRCxHQUFHLEVBQUU7Z0JBQ0gsTUFBTSxFQUFFLE1BQU07Z0JBQ2QsS0FBSyxFQUFFLE1BQU07YUFDZDtTQUNGO1FBRUQsVUFBVSxFQUFFO1lBQ1YsU0FBUyxFQUFFLFlBQVksQ0FBQztnQkFDdEIsTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDO2dCQUNaLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQztnQkFDYixPQUFPLEVBQUUsQ0FBQzt3QkFDUixNQUFNLEVBQUUsRUFBRTt3QkFDVixNQUFNLEVBQUUsZUFBYSxRQUFRLENBQUMsWUFBYzt3QkFDNUMsWUFBWSxFQUFFLENBQUM7d0JBQ2YsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTt3QkFDOUIsVUFBVSxFQUFFLFFBQVE7d0JBQ3BCLGNBQWMsRUFBRSxRQUFRO3dCQUN4QixXQUFXLEVBQUUsRUFBRTt3QkFDZixZQUFZLEVBQUUsRUFBRTt3QkFDaEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxZQUFZO3FCQUM3QixDQUFDO2FBQ0gsQ0FBQztTQUNIO0tBQ0Y7SUFFRCxjQUFjLEVBQUU7UUFDZCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1FBQ3BDLEtBQUssRUFBRSxNQUFNO1FBQ2IsVUFBVSxFQUFFLFFBQVE7UUFDcEIsTUFBTSxFQUFFLFNBQVM7UUFFakIsU0FBUyxFQUFFLFVBQUMsa0JBQTBCLElBQUssT0FBQSxDQUFDO1lBQzFDLGVBQWUsRUFBRSxTQUFPLGtCQUFvQjtZQUM1QyxrQkFBa0IsRUFBRSxRQUFRO1lBQzVCLGNBQWMsRUFBRSxPQUFPO1lBQ3ZCLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLE1BQU07WUFDZCxlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87WUFDakMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxHQUFHLEVBQUUsQ0FBQztZQUNOLFlBQVksRUFBRSxFQUFFO1NBQ2pCLENBQUMsRUFWeUMsQ0FVekM7UUFFRixTQUFTLEVBQUU7WUFDVCxLQUFLLEVBQUUsTUFBTTtZQUNiLE1BQU0sRUFBRSxNQUFNO1lBQ2QsWUFBWSxFQUFFLEVBQUU7WUFDaEIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxHQUFHLEVBQUUsQ0FBQztZQUVOLEtBQUssRUFBRTtnQkFDTCxVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtnQkFDckMsZUFBZSxFQUFFLFFBQVEsQ0FBQyxPQUFPO2dCQUNqQyxLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsTUFBTTthQUNmO1NBQ0Y7S0FDRjtJQUVELElBQUksRUFBRTtRQUNKLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7UUFDcEMsR0FBRyxFQUFFLEtBQUs7UUFDVixJQUFJLEVBQUUsS0FBSztRQUNYLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtRQUMxQixTQUFTLEVBQUUsdUJBQXVCO1FBQ2xDLE1BQU0sRUFBRSxRQUFRLENBQUMsT0FBTztRQUV4QixLQUFLLEVBQUU7WUFDTCxLQUFLLEVBQUUsRUFBRTtZQUNULE1BQU0sRUFBRSxFQUFFO1NBQ1g7S0FDRjtJQUVELFNBQVMsRUFBRTtRQUNULE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7UUFDL0IsVUFBVSxFQUFFLFFBQVE7UUFDcEIsUUFBUSxFQUFFLGFBQWE7UUFFdkIsR0FBRyxFQUFFO1lBQ0gsS0FBSyxFQUFFLE1BQU07WUFDYixTQUFTLEVBQUUsR0FBRztZQUNkLFNBQVMsRUFBRSxHQUFHO1lBQ2QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTTtZQUNoQyxTQUFTLEVBQUUsRUFBRTtZQUNiLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLENBQUM7WUFDZixTQUFTLEVBQUUsUUFBUSxDQUFDLGNBQWM7U0FDbkM7S0FDRjtJQUVELGtCQUFrQixFQUFFO1FBQ2xCLE1BQU0sRUFBRSxFQUFFO0tBQ1g7SUFFRCxXQUFXLEVBQUU7UUFDWCxVQUFVLEVBQUUsTUFBTTtRQUNsQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1FBQ3BDLFlBQVksRUFBRSxFQUFFO1FBQ2hCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7UUFFL0IsV0FBVyxFQUFFO1lBQ1gsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxHQUFHLEVBQUUsQ0FBQztZQUNOLElBQUksRUFBRSxDQUFDO1lBQ1AsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsTUFBTTtTQUNmO0tBQ0Y7SUFFRCxRQUFRLEVBQUU7UUFDUixVQUFVLEVBQUU7WUFDVixPQUFPLEVBQUUsTUFBTTtZQUNmLFFBQVEsRUFBRSxNQUFNO1lBQ2hCLE1BQU0sRUFBRSxNQUFNO1NBQ2Y7UUFFRCxVQUFVLEVBQUU7WUFDVixTQUFTLEVBQUUsVUFBQyxZQUFZLElBQUssT0FBQSxDQUFDO2dCQUM1QixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixhQUFhLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUs7Z0JBQzlDLFVBQVUsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNqQyxXQUFXLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEMsU0FBUyxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hDLFlBQVksRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNuQyxNQUFNLEVBQUUsTUFBTTthQUNmLENBQUMsRUFSMkIsQ0FRM0I7WUFFRixVQUFVLEVBQUU7Z0JBQ1YsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsTUFBTSxFQUFFLEtBQUs7Z0JBQ2IsVUFBVSxFQUFFLENBQUM7Z0JBQ2IsYUFBYSxFQUFFLENBQUM7YUFDakI7WUFFRCxRQUFRLEVBQUU7Z0JBQ1IsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osV0FBVyxFQUFFLENBQUM7Z0JBQ2QsWUFBWSxFQUFFLENBQUM7YUFDaEI7U0FDRjtRQUVELFlBQVksRUFBRTtZQUNaLFNBQVMsRUFBRSxVQUFDLFlBQVksSUFBSyxPQUFBLENBQUM7Z0JBQzVCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLGFBQWEsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSztnQkFDOUMsY0FBYyxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxlQUFlO2dCQUN6RCxLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsTUFBTTthQUNmLENBQUMsRUFOMkIsQ0FNM0I7WUFFRixVQUFVLEVBQUU7Z0JBQ1YsSUFBSSxFQUFFLENBQUM7Z0JBQ1AsWUFBWSxFQUFFLENBQUM7Z0JBQ2YsTUFBTSxFQUFFLEtBQUs7YUFDZDtZQUVELFFBQVEsRUFBRTtnQkFDUixJQUFJLEVBQUUsQ0FBQztnQkFDUCxXQUFXLEVBQUUsQ0FBQzthQUNmO1lBRUQsWUFBWSxFQUFFO2dCQUNaLFNBQVMsRUFBRSxVQUFDLFlBQVksSUFBSyxPQUFBLENBQUM7b0JBQzVCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7b0JBQzlCLGFBQWEsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsUUFBUTtvQkFDOUMsSUFBSSxFQUFFLENBQUM7b0JBQ1AsY0FBYyxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxRQUFRO29CQUN6RCxNQUFNLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU07b0JBQ2xDLFVBQVUsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNqQyxXQUFXLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDbEMsU0FBUyxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ2hDLFlBQVksRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNwQyxDQUFDLEVBVjJCLENBVTNCO2dCQUVGLFVBQVUsRUFBRTtvQkFDVixJQUFJLEVBQUUsQ0FBQztvQkFDUCxVQUFVLEVBQUUsQ0FBQztvQkFDYixXQUFXLEVBQUUsQ0FBQztpQkFDZjtnQkFFRCxRQUFRLEVBQUU7b0JBQ1IsS0FBSyxFQUFFLE1BQU07b0JBQ2IsTUFBTSxFQUFFLEtBQUs7b0JBQ2IsSUFBSSxFQUFFLENBQUM7b0JBQ1AsYUFBYSxFQUFFLENBQUM7b0JBQ2hCLFVBQVUsRUFBRSxDQUFDO2lCQUNkO2FBQ0Y7U0FDRjtRQUVELFdBQVcsRUFBRTtZQUNYLFNBQVMsRUFBRSxVQUFDLFlBQVksSUFBSyxPQUFBLENBQUM7Z0JBQzVCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLGFBQWEsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSztnQkFDOUMsY0FBYyxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxlQUFlO2dCQUN6RCxLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsTUFBTTthQUNmLENBQUMsRUFOMkIsQ0FNM0I7WUFFRixVQUFVLEVBQUU7Z0JBQ1YsSUFBSSxFQUFFLENBQUM7Z0JBQ1AsWUFBWSxFQUFFLENBQUM7YUFDaEI7WUFFRCxRQUFRLEVBQUU7Z0JBQ1IsSUFBSSxFQUFFLENBQUM7Z0JBQ1AsTUFBTSxFQUFFLGVBQWEsUUFBUSxDQUFDLFVBQVk7YUFDM0M7WUFFRCxZQUFZLEVBQUU7Z0JBQ1osU0FBUyxFQUFFLFVBQUMsWUFBWSxJQUFLLE9BQUEsQ0FBQztvQkFDNUIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtvQkFDOUIsYUFBYSxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxRQUFRO29CQUM5QyxJQUFJLEVBQUUsQ0FBQztvQkFDUCxjQUFjLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLFFBQVE7b0JBQ3pELE1BQU0sRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTTtvQkFDbEMsVUFBVSxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ2pDLFdBQVcsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNuQyxDQUFDLEVBUjJCLENBUTNCO2dCQUVGLFVBQVUsRUFBRTtvQkFDVixJQUFJLEVBQUUsQ0FBQztvQkFDUCxVQUFVLEVBQUUsQ0FBQztvQkFDYixXQUFXLEVBQUUsQ0FBQztpQkFDZjtnQkFFRCxRQUFRLEVBQUU7b0JBQ1IsS0FBSyxFQUFFLE1BQU07b0JBQ2IsTUFBTSxFQUFFLFFBQVE7b0JBQ2hCLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxlQUFhLFFBQVEsQ0FBQyxVQUFZO2lCQUMzQzthQUNGO1NBQ0Y7UUFFRCxRQUFRLEVBQUU7WUFDUixTQUFTLEVBQUUsVUFBQyxZQUFZLElBQUssT0FBQSxDQUFDO2dCQUM1QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxJQUFJLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQzNCLEtBQUssRUFBRSxNQUFNO2dCQUNiLFVBQVUsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDaEMsV0FBVyxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNqQyxNQUFNLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLFFBQVE7Z0JBQ3hDLFVBQVUsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDakMsYUFBYSxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNwQyxNQUFNLEVBQUUsU0FBUzthQUNsQixDQUFDLEVBVjJCLENBVTNCO1lBRUYsV0FBVyxFQUFFLFVBQUMsWUFBWTtnQkFDeEIsTUFBTSxDQUFDLENBQUMsWUFBWTtvQkFDbEIsQ0FBQyxDQUFDO3dCQUNBLFVBQVUsRUFBRSxDQUFDO3dCQUNiLFlBQVksRUFBRSxDQUFDO3dCQUNmLGFBQWEsRUFBRSxDQUFDO3dCQUNoQixXQUFXLEVBQUUsQ0FBQzt3QkFDZCxNQUFNLEVBQUUsTUFBTTtxQkFDZjtvQkFDRCxDQUFDLENBQUM7d0JBQ0EsS0FBSyxFQUFFLE1BQU07d0JBQ2IsTUFBTSxFQUFFLE1BQU07d0JBQ2QsTUFBTSxFQUFFLENBQUM7d0JBQ1QsT0FBTyxFQUFFLENBQUM7cUJBQ1gsQ0FBQTtZQUNMLENBQUM7WUFFRCxHQUFHLEVBQUU7Z0JBQ0gsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtnQkFDcEMsR0FBRyxFQUFFLEtBQUs7Z0JBQ1YsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsU0FBUyxFQUFFLHVCQUF1QjtnQkFDbEMsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osVUFBVSxFQUFFLFFBQVEsQ0FBQyxrQkFBa0I7Z0JBQ3ZDLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDMUIsTUFBTSxFQUFFLFFBQVEsQ0FBQyxPQUFPO2FBQ3pCO1lBRUQsRUFBRSxFQUFFO2dCQUNGLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLEdBQUcsRUFBRSxDQUFDO2dCQUNOLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxNQUFNO2dCQUNkLE1BQU0sRUFBRSxRQUFRLENBQUMsT0FBTztnQkFDeEIsZUFBZSxFQUFFLFFBQVEsQ0FBQyxZQUFZO2FBQ3ZDO1NBQ0Y7S0FDRjtJQUVELE9BQU8sRUFBRTtRQUNQLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7UUFDcEMsR0FBRyxFQUFFLENBQUMsQ0FBQztRQUNQLEtBQUssRUFBRSxDQUFDLEVBQUU7UUFDVixTQUFTLEVBQUUsa0JBQWtCO1FBRTdCLEtBQUssRUFBRTtZQUNMLE1BQU0sRUFBRSxNQUFNO1lBQ2QsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUVwQyxJQUFJLEVBQUU7Z0JBQ0osT0FBTyxFQUFFLFNBQVM7Z0JBQ2xCLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMvQixZQUFZLEVBQUUsQ0FBQztnQkFDZixTQUFTLEVBQUUsUUFBUSxDQUFDLGNBQWM7Z0JBQ2xDLFVBQVUsRUFBRSxRQUFRO2dCQUNwQixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsUUFBUSxFQUFFLEVBQUU7YUFDYjtZQUVELElBQUksRUFBRTtnQkFDSixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxHQUFHLEVBQUUsQ0FBQztnQkFDTixJQUFJLEVBQUUsQ0FBQyxDQUFDO2dCQUNSLE1BQU0sRUFBRSxDQUFDO2dCQUNULEtBQUssRUFBRSxDQUFDO2dCQUNSLFdBQVcsRUFBRSxDQUFDO2dCQUNkLFdBQVcsRUFBRSxPQUFPO2dCQUNwQixXQUFXLEVBQUssUUFBUSxDQUFDLGdCQUFnQixTQUFJLFFBQVEsQ0FBQyxVQUFVLFNBQUksUUFBUSxDQUFDLGdCQUFnQixVQUFLLFFBQVEsQ0FBQyxnQkFBa0I7Z0JBQzdILFNBQVMsRUFBRSxrQkFBa0I7YUFDOUI7U0FDRjtLQUNGO0NBQ0ssQ0FBQyJ9
// CONCATENATED MODULE: ./components/container/feed-item/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};




















var shareFacebookLink = '//www.facebook.com/sharer.php?u=';
var itemStyle = feed_item_style.headerWrap.item;
var renderIconText = function (_a) {
    var _b = _a.shareLink, shareLink = _b === void 0 ? 'http://lxbtest.tk/' : _b, iconName = _a.iconName, iconColor = _a.iconColor, style = _a.style, _c = _a.onClick, onClick = _c === void 0 ? function () { } : _c, _d = _a.innerStyle, innerStyle = _d === void 0 ? {} : _d, _e = _a.isLink, isLink = _e === void 0 ? false : _e;
    var iconProps = {
        style: {
            color: iconColor,
            width: 40,
            height: 40,
        },
        name: iconName,
        innerStyle: Object.assign({}, itemStyle.inner, innerStyle),
    };
    return (isLink
        ?
            react["createElement"]("a", { style: style, href: "" + shareFacebookLink + shareLink, target: '_blank' },
                react["createElement"](icon["a" /* default */], __assign({}, iconProps)))
        :
            react["createElement"]("div", { style: style, onClick: onClick },
                react["createElement"](icon["a" /* default */], __assign({}, iconProps))));
};
var renderImageGroup = function (_a) {
    var item = _a.item, openModal = _a.openModal, isShowFullImage = _a.isShowFullImage;
    var link = Object(responsive["b" /* isDesktopVersion */])() ? '#' : routing["q" /* ROUTING_COMMUNITY_PATH */] + "/" + (item && item.id || 0);
    var pictureList = item
        && !Object(validate["l" /* isUndefined */])(item.pictures)
        && !!item.pictures.length
        ? item.pictures
        : item && !Object(validate["l" /* isUndefined */])(item.picture) ? [item.picture] : [];
    var imgUrl = item
        && !Object(validate["l" /* isUndefined */])(item.pictures)
        && Array.isArray(item.pictures)
        && item.pictures.length === 1
        ? item.pictures[0].original_url
        : item && item.picture && item.picture.original_url || '';
    return isShowFullImage
        ? react["createElement"](image_slider_community["a" /* default */], { data: pictureList })
        : item
            && !Object(validate["l" /* isUndefined */])(item.pictures)
            && Array.isArray(item.pictures)
            && item.pictures.length > 1
            ? (react["createElement"](react_router_dom["NavLink"], { to: link, style: feed_item_style.pictureList },
                react["createElement"]("div", { style: feed_item_style.pictureList.pictureWrap }, renderImageList({ item: item, openModal: openModal }))))
            : (react["createElement"](react_router_dom["NavLink"], { to: link }, renderImage({ imgUrl: imgUrl, style: itemStyle.onePicture, openModal: openModal, data: { data: Object.assign({}, item, { pictures: [{ original_url: imgUrl || '' }] }), posImg: 0 } })));
};
var renderImage = function (_a) {
    var imgUrl = _a.imgUrl, openModal = _a.openModal, data = _a.data, _b = _a.key, key = _b === void 0 ? '' : _b, _c = _a.style, style = _c === void 0 ? {} : _c;
    var handleClick = {
        MOBILE: function () { },
        DESKTOP: function () { return openModal(Object(application_modal["f" /* MODAL_FEED_ITEM_COMMUNITY */])(data)); }
    };
    return react["createElement"]("img", { onClick: function () { return handleClick[window.DEVICE_VERSION](); }, key: key, style: [itemStyle.imageContent, style], src: imgUrl });
};
var renderTwoImages = function (item, openModal) {
    var pictures = item.pictures;
    var isPictureListNotEmpty = pictures
        && Array.isArray(pictures)
        && !!pictures.length;
    var firstPicture = isPictureListNotEmpty && pictures[0] || { width: 0, height: 0 };
    var isHorizontal = firstPicture.width > firstPicture.height;
    var imgStyle = isHorizontal ? feed_item_style.pictures.twoPicture.horizontal : feed_item_style.pictures.twoPicture.vertical;
    return (react["createElement"]("div", { style: feed_item_style.pictures.twoPicture.container(isHorizontal) }, isPictureListNotEmpty && pictures.map(function (picture, index) { return react["createElement"]("div", { style: imgStyle }, renderImage({ imgUrl: picture && picture.original_url || '', key: "two-img-" + (picture && picture.id || 0), style: itemStyle.fullHeight, openModal: openModal, data: { data: item, posImg: index } })); })));
};
var renderThreeImages = function (item, openModal) {
    var pictures = item.pictures;
    var isPictureListNotEmpty = pictures
        && Array.isArray(pictures)
        && !!pictures.length;
    var firstPicture = isPictureListNotEmpty && pictures[0] || { width: 0, height: 0 };
    var isHorizontal = firstPicture.width > firstPicture.height;
    var imgStyle = isHorizontal ? feed_item_style.pictures.threePicture.horizontal : feed_item_style.pictures.threePicture.vertical;
    var imgItemGroupStyle = isHorizontal ? feed_item_style.pictures.threePicture.pictureGroup.horizontal : feed_item_style.pictures.threePicture.pictureGroup.vertical;
    return !isPictureListNotEmpty
        ? null
        : (react["createElement"]("div", { style: feed_item_style.pictures.threePicture.container(isHorizontal) },
            react["createElement"]("div", { style: imgStyle }, renderImage({ imgUrl: pictures[0].original_url, key: "three-img-" + pictures[0].id, style: itemStyle.fullHeight, openModal: openModal, data: { data: item, posImg: 0 } })),
            react["createElement"]("div", { style: feed_item_style.pictures.threePicture.pictureGroup.container(isHorizontal) }, [1, 2].map(function (index) { return react["createElement"]("div", { style: imgItemGroupStyle }, renderImage({ imgUrl: pictures[index].original_url, key: "three-img-" + pictures[index].id, openModal: openModal, data: { data: item, posImg: index } })); }))));
};
var renderFourImages = function (item, openModal, viewMoreNum) {
    if (viewMoreNum === void 0) { viewMoreNum = 0; }
    var pictures = item.pictures;
    var isPictureListNotEmpty = pictures
        && Array.isArray(pictures)
        && !!pictures.length;
    var firstPicture = isPictureListNotEmpty && pictures[0] || { width: 0, height: 0 };
    var isHorizontal = firstPicture.width > firstPicture.height;
    var imgStyle = isHorizontal ? feed_item_style.pictures.fourPicture.horizontal : feed_item_style.pictures.fourPicture.vertical;
    var imgItemGroupStyle = isHorizontal ? feed_item_style.pictures.fourPicture.pictureGroup.horizontal : feed_item_style.pictures.fourPicture.pictureGroup.vertical;
    return !isPictureListNotEmpty
        ? null
        : (react["createElement"]("div", { style: feed_item_style.pictures.fourPicture.container(isHorizontal) },
            react["createElement"]("div", { style: imgStyle }, renderImage({ imgUrl: pictures[0].original_url, key: "four-img-" + pictures[0].id, openModal: openModal, data: { data: item, posImg: 0 } })),
            react["createElement"]("div", { style: feed_item_style.pictures.fourPicture.pictureGroup.container(isHorizontal) },
                [1, 2].map(function (index) { return react["createElement"]("div", { style: imgItemGroupStyle }, renderImage({ imgUrl: pictures[index].original_url, key: "four-img-" + pictures[index].id, openModal: openModal, data: { data: item, posImg: index } })); }),
                viewMoreNum > 0
                    ?
                        react["createElement"]("div", { style: feed_item_style.pictures.viewMore.container(isHorizontal), onClick: function () { return Object(responsive["b" /* isDesktopVersion */])() ? openModal(Object(application_modal["f" /* MODAL_FEED_ITEM_COMMUNITY */])({ data: item, posImg: 3 })) : {}; } },
                            react["createElement"]("div", { style: [imgItemGroupStyle, feed_item_style.pictures.viewMore.imgViewMore(isHorizontal)] }, renderImage({ imgUrl: pictures[3].original_url, key: "four-img-" + pictures[3].id, openModal: openModal, data: { data: item, posImg: 3 } })),
                            react["createElement"]("span", { style: feed_item_style.pictures.viewMore.num },
                                "+",
                                viewMoreNum),
                            react["createElement"]("div", { style: feed_item_style.pictures.viewMore.bg }))
                    : react["createElement"]("div", { style: imgItemGroupStyle }, renderImage({ imgUrl: pictures[3].original_url, key: "four-img-" + pictures[3].id, openModal: openModal, data: { data: item, posImg: 3 } })))));
};
var renderImageList = function (_a) {
    var item = _a.item, openModal = _a.openModal;
    var pictureLength = item && item.pictures && item.pictures.length || 0;
    if (pictureLength > 0) {
        switch (pictureLength) {
            case 1: return renderImage({ imgUrl: item.pictures[0].original_url, style: feed_item_style.pictures.onePicture, openModal: openModal, data: { data: item, posImg: 0 } });
            case 2: return renderTwoImages(item, openModal);
            case 3: return renderThreeImages(item, openModal);
            case 4: return renderFourImages(item, openModal);
            default: return renderFourImages(item, openModal, pictureLength - 4);
        }
    }
    return null;
};
var getLikeProps = function (_a) {
    var iconColor = _a.iconColor, openModal = _a.openModal, handleLike = _a.handleLike, innerStyle = _a.innerStyle;
    return ({
        iconColor: iconColor,
        innerStyle: innerStyle,
        iconName: 'like',
        style: itemStyle.likeCommentIconGroup.icon,
        onClick: auth["a" /* auth */].loggedIn() ? handleLike : function () { return openModal(Object(application_modal["n" /* MODAL_SIGN_IN */])()); }
    });
};
var renderTooltip = function (text) { return (react["createElement"]("div", { className: 'tooltip', style: feed_item_style.tooltip },
    react["createElement"]("div", { style: feed_item_style.tooltip.group },
        react["createElement"]("div", { style: feed_item_style.tooltip.group.text }, text),
        react["createElement"]("div", { style: feed_item_style.tooltip.group.icon })))); };
var renderViewMore = function (_a) {
    var link = _a.link;
    return react["createElement"](react_router_dom["NavLink"], { to: link, style: feed_item_style.headerWrap.viewDetail.container }, "Xem chi ti\u1EBFt");
};
var generateLinkAds = function (_a) {
    var feedableType = _a.feedableType, feedable = _a.feedable;
    switch (feedableType) {
        case application_feedable["a" /* FEEDABLE_TYPE */].THEME: return renderViewMore({ link: routing["qb" /* ROUTING_THEME_DETAIL_PATH */] + "/" + (feedable && feedable.theme_feedable && feedable.theme_feedable.slug || '') });
        case application_feedable["a" /* FEEDABLE_TYPE */].BRAND: return renderViewMore({ link: routing["g" /* ROUTING_BRAND_DETAIL_PATH */] + "/" + (feedable && feedable.brand_feedable && feedable.brand_feedable.slug || '') });
        case application_feedable["a" /* FEEDABLE_TYPE */].BROWSENODE: return renderViewMore({ link: routing["Xa" /* ROUTING_PRODUCT_CATEGORY_PATH */] + "/" + (feedable && feedable.browse_node_feedable && feedable.browse_node_feedable.slug || '') });
        case application_feedable["a" /* FEEDABLE_TYPE */].BLOG: return renderViewMore({ link: routing["Ma" /* ROUTING_MAGAZINE_DETAIL_PATH */] + "/" + (feedable && feedable.blog_feedable && feedable.blog_feedable.slug || '') });
        case application_feedable["a" /* FEEDABLE_TYPE */].BOX: return renderViewMore({ link: routing["Za" /* ROUTING_PRODUCT_DETAIL_PATH */] + "/" + (feedable && feedable.box_feedable && feedable.box_feedable.slug || '') });
        default: return null;
    }
};
function renderView(_a) {
    var props = _a.props, state = _a.state, handleLike = _a.handleLike, handleSubmit = _a.handleSubmit, handleViewMore = _a.handleViewMore, handleShowVideo = _a.handleShowVideo, handleShowComment = _a.handleShowComment, setInputCommentRef = _a.setInputCommentRef, handleInputOnFocus = _a.handleInputOnFocus, handleInputOnChange = _a.handleInputOnChange, handleShowInputComment = _a.handleShowInputComment;
    var _b = props, item = _b.item, openModal = _b.openModal, likeProduct = _b.likeProduct, userProfile = _b.userProfile, listLikedId = _b.listLikedId, isLastChild = _b.isLastChild, isShowImage = _b.isShowImage, isFeedDetail = _b.isFeedDetail, unLikeProduct = _b.unLikeProduct, isShowFullImage = _b.isShowFullImage, _c = _b.limitTextLength, limitTextLength = _c === void 0 ? 0 : _c;
    var _d = state, isLike = _d.isLike, isViewMore = _d.isViewMore, commentList = _d.commentList, isShowVideo = _d.isShowVideo, answerComment = _d.answerComment, isShowComments = _d.isShowComments, isShowInputComment = _d.isShowInputComment, _e = _d.likeNum, likeNum = _e === void 0 ? 0 : _e;
    var renderTotalLikeComment = function () {
        var total = likeNum + item.total_comments;
        var text = " ng\u01B0\u1EDDi \u0111\u00E3 " + (!!likeNum ? 'yêu thích' : '') + " " + (!!likeNum && !!item.total_comments ? 'và' : '') + " " + (!!item.total_comments ? 'bình luận' : '');
        if (!likeNum && !item.total_comments) {
            return null;
        }
        return (react["createElement"]("div", { style: itemStyle.countingGroup },
            react["createElement"]("span", { style: itemStyle.countingGroup.userList }, [0, 1, 2, 3, 4].map(function (index) { return index < likeNum && (react["createElement"]("span", { onClick: handleShowComment, key: index, style: [itemStyle.countingGroup.userItem(index), { backgroundImage: "url(" + (item.likes[index] ? item.likes[index].avatar : '') + ")" }] })); })),
            react["createElement"]("span", { onClick: handleShowComment, style: itemStyle.countingGroup.text }, total),
            react["createElement"]("span", { onClick: handleShowComment, style: itemStyle.countingGroup.text }, text)));
    };
    var isLiked = auth["a" /* auth */].loggedIn()
        && item.box
        && listLikedId.indexOf(item.box.id || 0) >= 0;
    var renderYoutube = function (_a) {
        var isShowVideo = _a.isShowVideo, handleShowVideo = _a.handleShowVideo, videoUrl = _a.videoUrl, pictureUrl = _a.pictureUrl;
        var iconProps = {
            name: 'play',
            style: feed_item_style.icon,
            innerStyle: feed_item_style.icon.inner
        };
        var youtubeUrl = videoUrl.replace('watch?v=', 'embed/') + '?autoplay=1&showinfo=0&controls=0';
        return (react["createElement"]("div", { style: feed_item_style.videoContainer }, !isShowVideo
            ?
                react["createElement"]("div", { style: feed_item_style.videoContainer.thumbnail(pictureUrl), onClick: handleShowVideo },
                    react["createElement"](icon["a" /* default */], __assign({}, iconProps)),
                    react["createElement"]("div", { style: feed_item_style.wrapIcon }))
            :
                react["createElement"]("div", { style: feed_item_style.videoContainer.videoWrap },
                    react["createElement"]("iframe", { style: feed_item_style.videoContainer.videoWrap.video, className: 'frame-you-tube', src: youtubeUrl }))));
    };
    var inputCommentProps = {
        ref: function (ref) { return setInputCommentRef(ref); },
        autoFocus: true,
        style: itemStyle.inputCommentGroup.inputText,
        placeholder: 'Gửi bình luận của bạn...',
        type: global["d" /* INPUT_TYPE */].TEXT,
        onChange: function (e) { return handleInputOnChange(e); },
        onFocus: handleInputOnFocus,
        onSubmit: handleSubmit,
        value: answerComment || '',
    };
    var txtComment = auth["a" /* auth */].loggedIn() ? 'Gửi bình luận của bạn...' : 'Đăng nhập để bình luận';
    var actionComment = auth["a" /* auth */].loggedIn() ? handleShowInputComment : function () { return openModal(Object(application_modal["n" /* MODAL_SIGN_IN */])()); };
    var avatarUrl = auth["a" /* auth */].loggedIn() ? userProfile && userProfile.avatar && userProfile.avatar.medium_url : '';
    var avatarProps = {
        style: [
            { backgroundImage: "url('" + avatarUrl + "')" },
            itemStyle.inputCommentGroup.avatar,
        ]
    };
    var likeProps = true === isLike
        ? getLikeProps({ iconColor: variable["colorPink"], openModal: openModal, handleLike: handleLike, innerStyle: itemStyle.likeCommentIconGroup.innerIconLike })
        : getLikeProps({ iconColor: variable["color2E"], openModal: openModal, handleLike: handleLike, innerStyle: itemStyle.likeCommentIconGroup.innerIconLike });
    var userFeedUrl = routing["w" /* ROUTING_COMMUNITY_USER_FEED_PATH */] + "/" + (item && item.user && item.user.id || 0);
    var feedableType = item && item.feedable_type || '';
    var description = Object(format["b" /* createBreakDownLine */])(item && item.message || '');
    var hiddenDesc = !isViewMore && limitTextLength < description.length;
    description = hiddenDesc
        ? description.substring(0, limitTextLength)
        : description;
    return (react["createElement"]("div", { style: [itemStyle.container, isLastChild ? itemStyle.lastChild : '', !isShowImage && { paddingBottom: 0, borderBottom: 'none' }] },
        react["createElement"]("div", { style: feed_item_style.headerWrap },
            react["createElement"]("div", { style: itemStyle.info.container },
                react["createElement"](react_router_dom["NavLink"], { to: userFeedUrl },
                    react["createElement"]("div", { style: [{ backgroundImage: "url(" + (item && item.user && item.user.avatar && item.user.avatar.medium_url || '') + ")" }, itemStyle.info.avatar] })),
                react["createElement"](react_router_dom["NavLink"], { to: userFeedUrl, style: itemStyle.info.detail, className: 'icon-item' },
                    react["createElement"]("div", null,
                        react["createElement"]("span", { style: itemStyle.info.detail.username },
                            item && item.user && item.user.name || '',
                            !isFeedDetail && renderTooltip('Xem trang cá nhân'))),
                    react["createElement"]("div", { style: itemStyle.info.detail.ratingGroup },
                        item && item.rating && react["createElement"](rating_star["a" /* default */], { style: itemStyle.info.detail.ratingGroup.rating, value: item.rating }),
                        item && item.created_at && react["createElement"](react_router_dom["NavLink"], { to: routing["q" /* ROUTING_COMMUNITY_PATH */] + "/" + (item && item.id || 0), style: itemStyle.info.detail.ratingGroup.date }, Object(encode["d" /* convertUnixTime */])(item.created_at, global["b" /* DATETIME_TYPE_FORMAT */].SHORT_DATE)))),
                feedableType === application_feedable["a" /* FEEDABLE_TYPE */].FEEDBACK
                    && (react["createElement"](react_router_dom["NavLink"], { to: routing["Za" /* ROUTING_PRODUCT_DETAIL_PATH */] + "/" + (item && item.box && item.box.slug || ''), style: feed_item_style.headerWrap.imgProduct.container },
                        react["createElement"]("img", { style: feed_item_style.headerWrap.imgProduct.img, src: item && item.picture && item.picture.medium_url || '' }))),
                generateLinkAds({ feedableType: feedableType, feedable: item && item.feedable }))),
        react["createElement"]("div", { style: itemStyle.info.descGroup }, hiddenDesc
            ?
                react["createElement"]("div", null,
                    Object(html["a" /* renderHtmlContent */])(description, itemStyle.info.description.container),
                    react["createElement"]("span", { style: itemStyle.info.description.viewMore, onClick: handleViewMore }, "... Xem th\u00EAm"))
            : Object(html["a" /* renderHtmlContent */])(description, itemStyle.info.description.container)),
        !isShowImage || item && item.feedable_type === application_feedable["a" /* FEEDABLE_TYPE */].FEEDBACK
            ? null
            : Object(validate["j" /* isEmptyObject */])(item && item.video)
                ? renderImageGroup({ item: item, openModal: openModal, isShowFullImage: isShowFullImage })
                : renderYoutube({ isShowVideo: isShowVideo, handleShowVideo: handleShowVideo, videoUrl: item && item.video && item.video.url, pictureUrl: item && item.video && item.video.thumbnail && item.video.thumbnail.original_url || '' }),
        react["createElement"]("div", { style: itemStyle.likeCommentIconGroup.container },
            react["createElement"]("div", { style: itemStyle.likeCommentIconGroup.left },
                renderIconText(likeProps),
                renderIconText({
                    iconName: 'message',
                    iconColor: variable["color2E"],
                    style: itemStyle.likeCommentIconGroup.icon,
                    innerStyle: itemStyle.likeCommentIconGroup.innerIconComment,
                    onClick: handleShowComment
                }),
                renderIconText({
                    iconName: 'fly',
                    iconColor: variable["color2E"],
                    style: itemStyle.likeCommentIconGroup.icon,
                    isLink: true,
                    shareLink: item && item.share_link || '',
                    innerStyle: itemStyle.likeCommentIconGroup.innerIconFly
                })),
            item && item.feedable_type === application_feedable["a" /* FEEDABLE_TYPE */].FEEDBACK
                && (react["createElement"]("div", { style: itemStyle.likeCommentIconGroup.right }, renderIconText({
                    iconName: isLiked ? 'bookmark' : 'bookmark-line',
                    iconColor: isLiked ? variable["colorPink"] : variable["color2E"],
                    style: itemStyle.likeCommentIconGroup.icon,
                    isLink: false,
                    shareLink: item && item.share_link || '',
                    innerStyle: itemStyle.likeCommentIconGroup.innerIconHeart,
                    onClick: function () {
                        if (auth["a" /* auth */].loggedIn() && item.box) {
                            isLiked
                                ? unLikeProduct(item.box.id)
                                : likeProduct(item.box.id);
                        }
                        else {
                            openModal(Object(application_modal["n" /* MODAL_SIGN_IN */])());
                        }
                    }
                })))),
        renderTotalLikeComment(),
        isShowComments && (react["createElement"]("div", { style: itemStyle.commentGroup },
            'undefined' === typeof commentList
                /** Loading Icon */
                ? react["createElement"](loading["a" /* default */], { style: feed_item_style.customStyleLoading })
                /** Show list data */
                : Array.isArray(commentList)
                    && commentList.map(function (comment) { return (react["createElement"]("div", { key: comment.id, style: itemStyle.commentGroup.container },
                        react["createElement"]("div", { style: [{ backgroundImage: "url('" + comment.avatar.medium_url + "')" }, itemStyle.info.avatar, itemStyle.info.avatar.small] }),
                        react["createElement"]("div", { style: itemStyle.commentGroup.contenGroup },
                            react["createElement"]("span", { style: itemStyle.info.detail.username }, comment.user_name),
                            react["createElement"]("span", { style: itemStyle.commentGroup.contenGroup.comment }, comment.content)))); }),
            isShowInputComment
                ? (react["createElement"]("div", { style: itemStyle.inputCommentGroup },
                    react["createElement"]("div", __assign({}, avatarProps)),
                    react["createElement"]("div", { style: itemStyle.inputCommentGroup.commentInputGroup },
                        react["createElement"]("input", __assign({}, inputCommentProps)),
                        react["createElement"]("div", { style: itemStyle.inputCommentGroup.sendComment, onClick: handleSubmit }, "G\u1EEDi"))))
                : (react["createElement"]("div", { style: itemStyle.inputCommentGroup },
                    react["createElement"]("div", __assign({}, avatarProps)),
                    react["createElement"]("div", { style: itemStyle.inputCommentGroup.input, onClick: actionComment }, txtComment))))),
        react["createElement"](radium["Style"], { rules: INLINE_STYLE.tooltip })));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxPQUFPLElBQUksTUFBTSxlQUFlLENBQUM7QUFDakMsT0FBTyxPQUFPLE1BQU0sa0JBQWtCLENBQUM7QUFDdkMsT0FBTyxVQUFVLE1BQU0sc0JBQXNCLENBQUM7QUFDOUMsT0FBTyxvQkFBb0IsTUFBTSxzREFBc0QsQ0FBQztBQUV4RixPQUFPLEVBQUUsSUFBSSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDM0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3hELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQzdELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzVELE9BQU8sRUFBRSxhQUFhLEVBQUUsV0FBVyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFFckUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ25FLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUN4RSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsYUFBYSxFQUFFLHlCQUF5QixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDaEcsT0FBTyxFQUNMLHNCQUFzQixFQUN0Qix5QkFBeUIsRUFDekIseUJBQXlCLEVBQ3pCLDJCQUEyQixFQUMzQiw0QkFBNEIsRUFDNUIsNkJBQTZCLEVBQzdCLGdDQUFnQyxHQUNqQyxNQUFNLHdDQUF3QyxDQUFDO0FBR2hELE9BQU8sS0FBSyxFQUFFLEVBQUUsWUFBWSxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBRzlDLElBQU0saUJBQWlCLEdBQUcsa0NBQWtDLENBQUM7QUFFN0QsSUFBTSxTQUFTLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7QUFFeEMsSUFBTSxjQUFjLEdBQUcsVUFBQyxFQVF2QjtRQVBDLGlCQUFzQyxFQUF0QywyREFBc0MsRUFDdEMsc0JBQVEsRUFDUix3QkFBUyxFQUNULGdCQUFLLEVBQ0wsZUFBbUIsRUFBbkIsOENBQW1CLEVBQ25CLGtCQUFlLEVBQWYsb0NBQWUsRUFDZixjQUFjLEVBQWQsbUNBQWM7SUFHZCxJQUFNLFNBQVMsR0FBRztRQUNoQixLQUFLLEVBQUU7WUFDTCxLQUFLLEVBQUUsU0FBUztZQUNoQixLQUFLLEVBQUUsRUFBRTtZQUNULE1BQU0sRUFBRSxFQUFFO1NBQ1g7UUFDRCxJQUFJLEVBQUUsUUFBUTtRQUNkLFVBQVUsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxTQUFTLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQztLQUMzRCxDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsTUFBTTtRQUNKLENBQUM7WUFDRCwyQkFBRyxLQUFLLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxLQUFHLGlCQUFpQixHQUFHLFNBQVcsRUFBRSxNQUFNLEVBQUUsUUFBUTtnQkFDekUsb0JBQUMsSUFBSSxlQUFLLFNBQVMsRUFBSSxDQUNyQjtRQUNKLENBQUM7WUFDRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxPQUFPO2dCQUNqQyxvQkFBQyxJQUFJLGVBQUssU0FBUyxFQUFJLENBQ25CLENBQ1QsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLElBQU0sZ0JBQWdCLEdBQUcsVUFBQyxFQUFvQztRQUFsQyxjQUFJLEVBQUUsd0JBQVMsRUFBRSxvQ0FBZTtJQUMxRCxJQUFNLElBQUksR0FBRyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFJLHNCQUFzQixVQUFJLElBQUksSUFBSSxJQUFJLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBRSxDQUFDO0lBRTVGLElBQU0sV0FBVyxHQUFHLElBQUk7V0FDbkIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztXQUMzQixDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNO1FBQ3pCLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUTtRQUNmLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBRTdELElBQU0sTUFBTSxHQUFHLElBQUk7V0FDZCxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1dBQzNCLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztXQUM1QixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sS0FBSyxDQUFDO1FBQzdCLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVk7UUFDL0IsQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxJQUFJLEVBQUUsQ0FBQztJQUU1RCxNQUFNLENBQUMsZUFBZTtRQUNwQixDQUFDLENBQUMsb0JBQUMsb0JBQW9CLElBQUMsSUFBSSxFQUFFLFdBQVcsR0FBSTtRQUM3QyxDQUFDLENBQUMsSUFBSTtlQUNELENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7ZUFDM0IsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2VBQzVCLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUM7WUFDM0IsQ0FBQyxDQUFDLENBQ0Esb0JBQUMsT0FBTyxJQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXO2dCQUN6Qyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLElBQ3RDLGVBQWUsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLFNBQVMsV0FBQSxFQUFFLENBQUMsQ0FDakMsQ0FDRSxDQUNYO1lBQ0QsQ0FBQyxDQUFDLENBQ0Esb0JBQUMsT0FBTyxJQUFDLEVBQUUsRUFBRSxJQUFJLElBQ2QsV0FBVyxDQUFDLEVBQUUsTUFBTSxRQUFBLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxVQUFVLEVBQUUsU0FBUyxXQUFBLEVBQUUsSUFBSSxFQUFFLEVBQUUsSUFBSSxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLFFBQVEsRUFBRSxDQUFDLEVBQUUsWUFBWSxFQUFFLE1BQU0sSUFBSSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUM1SixDQUNYLENBQUE7QUFDUCxDQUFDLENBQUM7QUFFRixJQUFNLFdBQVcsR0FBRyxVQUFDLEVBQWlEO1FBQS9DLGtCQUFNLEVBQUUsd0JBQVMsRUFBRSxjQUFJLEVBQUUsV0FBUSxFQUFSLDZCQUFRLEVBQUUsYUFBVSxFQUFWLCtCQUFVO0lBQ2xFLElBQU0sV0FBVyxHQUFHO1FBQ2xCLE1BQU0sRUFBRSxjQUFRLENBQUM7UUFDakIsT0FBTyxFQUFFLGNBQU0sT0FBQSxTQUFTLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBMUMsQ0FBMEM7S0FDMUQsQ0FBQztJQUVGLE1BQU0sQ0FBQyw2QkFBSyxPQUFPLEVBQUUsY0FBTSxPQUFBLFdBQVcsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUUsRUFBcEMsQ0FBb0MsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLEVBQUUsR0FBRyxFQUFFLE1BQU0sR0FBSSxDQUFDO0FBQ3JJLENBQUMsQ0FBQztBQUVGLElBQU0sZUFBZSxHQUFHLFVBQUMsSUFBSSxFQUFFLFNBQVM7SUFDOUIsSUFBQSx3QkFBUSxDQUFVO0lBQzFCLElBQU0scUJBQXFCLEdBQUcsUUFBUTtXQUNqQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztXQUN2QixDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztJQUV2QixJQUFNLFlBQVksR0FBRyxxQkFBcUIsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQztJQUNyRixJQUFNLFlBQVksR0FBRyxZQUFZLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUM7SUFDOUQsSUFBTSxRQUFRLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQztJQUUxRyxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxJQUMxRCxxQkFBcUIsSUFBSSxRQUFRLENBQUMsR0FBRyxDQUFDLFVBQUMsT0FBTyxFQUFFLEtBQUssSUFBSyxPQUFBLDZCQUFLLEtBQUssRUFBRSxRQUFRLElBQUcsV0FBVyxDQUFDLEVBQUUsTUFBTSxFQUFFLE9BQU8sSUFBSSxPQUFPLENBQUMsWUFBWSxJQUFJLEVBQUUsRUFBRSxHQUFHLEVBQUUsY0FBVyxPQUFPLElBQUksT0FBTyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUUsRUFBRSxLQUFLLEVBQUUsU0FBUyxDQUFDLFVBQVUsRUFBRSxTQUFTLFdBQUEsRUFBRSxJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQU8sRUFBdk4sQ0FBdU4sQ0FBQyxDQUMvUSxDQUNQLENBQUE7QUFDSCxDQUFDLENBQUM7QUFFRixJQUFNLGlCQUFpQixHQUFHLFVBQUMsSUFBSSxFQUFFLFNBQVM7SUFDaEMsSUFBQSx3QkFBUSxDQUFVO0lBQzFCLElBQU0scUJBQXFCLEdBQUcsUUFBUTtXQUNqQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztXQUN2QixDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztJQUV2QixJQUFNLFlBQVksR0FBRyxxQkFBcUIsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQztJQUNyRixJQUFNLFlBQVksR0FBRyxZQUFZLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUM7SUFDOUQsSUFBTSxRQUFRLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQztJQUM5RyxJQUFNLGlCQUFpQixHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQztJQUVqSixNQUFNLENBQUMsQ0FBQyxxQkFBcUI7UUFDM0IsQ0FBQyxDQUFDLElBQUk7UUFDTixDQUFDLENBQUMsQ0FDQSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQztZQUM3RCw2QkFBSyxLQUFLLEVBQUUsUUFBUSxJQUFHLFdBQVcsQ0FBQyxFQUFFLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxFQUFFLEdBQUcsRUFBRSxlQUFhLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFJLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxVQUFVLEVBQUUsU0FBUyxXQUFBLEVBQUUsSUFBSSxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFPO1lBQzVMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxJQUN6RSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSw2QkFBSyxLQUFLLEVBQUUsaUJBQWlCLElBQUcsV0FBVyxDQUFDLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxZQUFZLEVBQUUsR0FBRyxFQUFFLGVBQWEsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUksRUFBRSxTQUFTLFdBQUEsRUFBRSxJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQU8sRUFBcEwsQ0FBb0wsQ0FBQyxDQUN0TSxDQUNGLENBQ1AsQ0FBQTtBQUNMLENBQUMsQ0FBQztBQUVGLElBQU0sZ0JBQWdCLEdBQUcsVUFBQyxJQUFJLEVBQUUsU0FBUyxFQUFFLFdBQWU7SUFBZiw0QkFBQSxFQUFBLGVBQWU7SUFDaEQsSUFBQSx3QkFBUSxDQUFVO0lBQzFCLElBQU0scUJBQXFCLEdBQUcsUUFBUTtXQUNqQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztXQUN2QixDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztJQUV2QixJQUFNLFlBQVksR0FBRyxxQkFBcUIsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQztJQUNyRixJQUFNLFlBQVksR0FBRyxZQUFZLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUM7SUFDOUQsSUFBTSxRQUFRLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQztJQUM1RyxJQUFNLGlCQUFpQixHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQztJQUUvSSxNQUFNLENBQUMsQ0FBQyxxQkFBcUI7UUFDM0IsQ0FBQyxDQUFDLElBQUk7UUFDTixDQUFDLENBQUMsQ0FDQSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQztZQUM1RCw2QkFBSyxLQUFLLEVBQUUsUUFBUSxJQUFHLFdBQVcsQ0FBQyxFQUFFLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxFQUFFLEdBQUcsRUFBRSxjQUFZLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFJLEVBQUUsU0FBUyxXQUFBLEVBQUUsSUFBSSxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFPO1lBQzlKLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQztnQkFDeEUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsNkJBQUssS0FBSyxFQUFFLGlCQUFpQixJQUFHLFdBQVcsQ0FBQyxFQUFFLE1BQU0sRUFBRSxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsWUFBWSxFQUFFLEdBQUcsRUFBRSxjQUFZLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFJLEVBQUUsU0FBUyxXQUFBLEVBQUUsSUFBSSxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFPLEVBQW5MLENBQW1MLENBQUM7Z0JBRXZNLFdBQVcsR0FBRyxDQUFDO29CQUNiLENBQUM7d0JBQ0QsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsRUFBRSxPQUFPLEVBQUUsY0FBTSxPQUFBLGdCQUFnQixFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyx5QkFBeUIsQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUF6RixDQUF5Rjs0QkFDbkssNkJBQUssS0FBSyxFQUFFLENBQUMsaUJBQWlCLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUcsV0FBVyxDQUFDLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLEVBQUUsR0FBRyxFQUFFLGNBQVksUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUksRUFBRSxTQUFTLFdBQUEsRUFBRSxJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQU87NEJBQzVOLDhCQUFNLEtBQUssRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHOztnQ0FBSSxXQUFXLENBQVE7NEJBQy9ELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEdBQVEsQ0FDMUM7b0JBQ04sQ0FBQyxDQUFDLDZCQUFLLEtBQUssRUFBRSxpQkFBaUIsSUFBRyxXQUFXLENBQUMsRUFBRSxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksRUFBRSxHQUFHLEVBQUUsY0FBWSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBSSxFQUFFLFNBQVMsV0FBQSxFQUFFLElBQUksRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBTyxDQUV6SyxDQUNELENBQ1IsQ0FBQTtBQUNMLENBQUMsQ0FBQztBQUVGLElBQU0sZUFBZSxHQUFHLFVBQUMsRUFBbUI7UUFBakIsY0FBSSxFQUFFLHdCQUFTO0lBQ3hDLElBQU0sYUFBYSxHQUFHLElBQUksSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQztJQUN6RSxFQUFFLENBQUMsQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN0QixNQUFNLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQ3RCLEtBQUssQ0FBQyxFQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLFNBQVMsV0FBQSxFQUFFLElBQUksRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUNwSixLQUFLLENBQUMsRUFBRSxNQUFNLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztZQUNoRCxLQUFLLENBQUMsRUFBRSxNQUFNLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBQ2xELEtBQUssQ0FBQyxFQUFFLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFDakQsU0FBUyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLFNBQVMsRUFBRSxhQUFhLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDdkUsQ0FBQztJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsSUFBSSxDQUFDO0FBQ2QsQ0FBQyxDQUFDO0FBRUYsSUFBTSxZQUFZLEdBQUcsVUFBQyxFQUFnRDtRQUE5Qyx3QkFBUyxFQUFFLHdCQUFTLEVBQUUsMEJBQVUsRUFBRSwwQkFBVTtJQUFPLE9BQUEsQ0FBQztRQUMxRSxTQUFTLFdBQUE7UUFDVCxVQUFVLFlBQUE7UUFDVixRQUFRLEVBQUUsTUFBTTtRQUNoQixLQUFLLEVBQUUsU0FBUyxDQUFDLG9CQUFvQixDQUFDLElBQUk7UUFDMUMsT0FBTyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxjQUFNLE9BQUEsU0FBUyxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQTFCLENBQTBCO0tBQ3pFLENBQUM7QUFOeUUsQ0FNekUsQ0FBQztBQUVILElBQU0sYUFBYSxHQUFHLFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FDOUIsNkJBQUssU0FBUyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU87SUFDN0MsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSztRQUM3Qiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFHLElBQUksQ0FBTztRQUNsRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFRLENBQ3hDLENBQ0YsQ0FDUCxFQVArQixDQU8vQixDQUFDO0FBRUYsSUFBTSxjQUFjLEdBQUcsVUFBQyxFQUFRO1FBQU4sY0FBSTtJQUFPLE9BQUEsb0JBQUMsT0FBTyxJQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLFNBQVMsd0JBQXdCO0FBQXZGLENBQXVGLENBQUM7QUFFN0gsSUFBTSxlQUFlLEdBQUcsVUFBQyxFQUEwQjtRQUF4Qiw4QkFBWSxFQUFFLHNCQUFRO0lBQy9DLE1BQU0sQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFDckIsS0FBSyxhQUFhLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFBRSxJQUFJLEVBQUsseUJBQXlCLFVBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxjQUFjLElBQUksUUFBUSxDQUFDLGNBQWMsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ3ZLLEtBQUssYUFBYSxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUUsSUFBSSxFQUFLLHlCQUF5QixVQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsY0FBYyxJQUFJLFFBQVEsQ0FBQyxjQUFjLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBRSxFQUFFLENBQUMsQ0FBQztRQUN2SyxLQUFLLGFBQWEsQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLGNBQWMsQ0FBQyxFQUFFLElBQUksRUFBSyw2QkFBNkIsVUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLG9CQUFvQixJQUFJLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzVMLEtBQUssYUFBYSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUUsSUFBSSxFQUFLLDRCQUE0QixVQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsYUFBYSxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBRSxFQUFFLENBQUMsQ0FBQztRQUN2SyxLQUFLLGFBQWEsQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLGNBQWMsQ0FBQyxFQUFFLElBQUksRUFBSywyQkFBMkIsVUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFlBQVksSUFBSSxRQUFRLENBQUMsWUFBWSxDQUFDLElBQUksSUFBSSxFQUFFLENBQUUsRUFBRSxDQUFDLENBQUM7UUFDbkssU0FBUyxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ3ZCLENBQUM7QUFDSCxDQUFDLENBQUE7QUFFRCxNQUFNLHFCQUFxQixFQVkxQjtRQVhDLGdCQUFLLEVBQ0wsZ0JBQUssRUFDTCwwQkFBVSxFQUNWLDhCQUFZLEVBQ1osa0NBQWMsRUFDZCxvQ0FBZSxFQUNmLHdDQUFpQixFQUNqQiwwQ0FBa0IsRUFDbEIsMENBQWtCLEVBQ2xCLDRDQUFtQixFQUNuQixrREFBc0I7SUFHaEIsSUFBQSxVQVlpQixFQVhyQixjQUFJLEVBQ0osd0JBQVMsRUFDVCw0QkFBVyxFQUNYLDRCQUFXLEVBQ1gsNEJBQVcsRUFDWCw0QkFBVyxFQUNYLDRCQUFXLEVBQ1gsOEJBQVksRUFDWixnQ0FBYSxFQUNiLG9DQUFlLEVBQ2YsdUJBQW1CLEVBQW5CLHdDQUFtQixDQUNHO0lBRWxCLElBQUEsVUFTaUIsRUFSckIsa0JBQU0sRUFDTiwwQkFBVSxFQUNWLDRCQUFXLEVBQ1gsNEJBQVcsRUFDWCxnQ0FBYSxFQUNiLGtDQUFjLEVBQ2QsMENBQWtCLEVBQ2xCLGVBQVcsRUFBWCxnQ0FBVyxDQUNXO0lBRXhCLElBQU0sc0JBQXNCLEdBQUc7UUFDN0IsSUFBTSxLQUFLLEdBQUcsT0FBTyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7UUFDNUMsSUFBTSxJQUFJLEdBQUcsb0NBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLFdBQUksQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLFdBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFFLENBQUM7UUFFdkosRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUE7UUFBQyxDQUFDO1FBRXJELE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxTQUFTLENBQUMsYUFBYTtZQUNqQyw4QkFBTSxLQUFLLEVBQUUsU0FBUyxDQUFDLGFBQWEsQ0FBQyxRQUFRLElBRXpDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssR0FBRyxPQUFPLElBQUksQ0FDOUMsOEJBQ0UsT0FBTyxFQUFFLGlCQUFpQixFQUMxQixHQUFHLEVBQUUsS0FBSyxFQUNWLEtBQUssRUFBRSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsZUFBZSxFQUFFLFVBQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsT0FBRyxFQUFFLENBQUMsR0FDNUgsQ0FDVCxFQU40QixDQU01QixDQUFDLENBRUM7WUFDUCw4QkFBTSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxhQUFhLENBQUMsSUFBSSxJQUFHLEtBQUssQ0FBUTtZQUNyRiw4QkFBTSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxhQUFhLENBQUMsSUFBSSxJQUFHLElBQUksQ0FBUSxDQUNoRixDQUNQLENBQUE7SUFDSCxDQUFDLENBQUE7SUFFRCxJQUFNLE9BQU8sR0FDWCxJQUFJLENBQUMsUUFBUSxFQUFFO1dBQ1osSUFBSSxDQUFDLEdBQUc7V0FDUixXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUVoRCxJQUFNLGFBQWEsR0FBRyxVQUFDLEVBQXNEO1lBQXBELDRCQUFXLEVBQUUsb0NBQWUsRUFBRSxzQkFBUSxFQUFFLDBCQUFVO1FBQ3pFLElBQU0sU0FBUyxHQUFHO1lBQ2hCLElBQUksRUFBRSxNQUFNO1lBQ1osS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJO1lBQ2pCLFVBQVUsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUs7U0FDN0IsQ0FBQztRQUVGLElBQU0sVUFBVSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxHQUFHLG1DQUFtQyxDQUFDO1FBQ2hHLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsY0FBYyxJQUU1QixDQUFDLFdBQVc7WUFDVixDQUFDO2dCQUNELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsRUFBRSxPQUFPLEVBQUUsZUFBZTtvQkFDOUUsb0JBQUMsSUFBSSxlQUFLLFNBQVMsRUFBSTtvQkFDdkIsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxRQUFRLEdBQVEsQ0FDOUI7WUFDTixDQUFDO2dCQUNELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsY0FBYyxDQUFDLFNBQVM7b0JBQ3hDLGdDQUFRLEtBQUssRUFBRSxLQUFLLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLGdCQUFnQixFQUFFLEdBQUcsRUFBRSxVQUFVLEdBQVcsQ0FDeEcsQ0FFTixDQUNQLENBQUE7SUFDSCxDQUFDLENBQUM7SUFFRixJQUFNLGlCQUFpQixHQUFHO1FBQ3hCLEdBQUcsRUFBRSxVQUFBLEdBQUcsSUFBSSxPQUFBLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxFQUF2QixDQUF1QjtRQUNuQyxTQUFTLEVBQUUsSUFBSTtRQUNmLEtBQUssRUFBRSxTQUFTLENBQUMsaUJBQWlCLENBQUMsU0FBUztRQUM1QyxXQUFXLEVBQUUsMEJBQTBCO1FBQ3ZDLElBQUksRUFBRSxVQUFVLENBQUMsSUFBSTtRQUNyQixRQUFRLEVBQUUsVUFBQyxDQUFDLElBQUssT0FBQSxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsRUFBdEIsQ0FBc0I7UUFDdkMsT0FBTyxFQUFFLGtCQUFrQjtRQUMzQixRQUFRLEVBQUUsWUFBWTtRQUN0QixLQUFLLEVBQUUsYUFBYSxJQUFJLEVBQUU7S0FDM0IsQ0FBQztJQUVGLElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDLHdCQUF3QixDQUFDO0lBQzNGLElBQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLGNBQU0sT0FBQSxTQUFTLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBMUIsQ0FBMEIsQ0FBQztJQUNsRyxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLFdBQVcsSUFBSSxXQUFXLENBQUMsTUFBTSxJQUFJLFdBQVcsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFFNUcsSUFBTSxXQUFXLEdBQUc7UUFDbEIsS0FBSyxFQUFFO1lBQ0wsRUFBRSxlQUFlLEVBQUUsVUFBUSxTQUFTLE9BQUksRUFBRTtZQUMxQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsTUFBTTtTQUNuQztLQUNGLENBQUM7SUFFRixJQUFNLFNBQVMsR0FBRyxJQUFJLEtBQUssTUFBTTtRQUMvQixDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUUsU0FBUyxFQUFFLFFBQVEsQ0FBQyxTQUFTLEVBQUUsU0FBUyxXQUFBLEVBQUUsVUFBVSxZQUFBLEVBQUUsVUFBVSxFQUFFLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNsSSxDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUUsU0FBUyxFQUFFLFFBQVEsQ0FBQyxPQUFPLEVBQUUsU0FBUyxXQUFBLEVBQUUsVUFBVSxZQUFBLEVBQUUsVUFBVSxFQUFFLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBRW5JLElBQU0sV0FBVyxHQUFNLGdDQUFnQyxVQUFJLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBRSxDQUFDO0lBRXBHLElBQU0sWUFBWSxHQUFHLElBQUksSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLEVBQUUsQ0FBQztJQUV0RCxJQUFJLFdBQVcsR0FBRyxtQkFBbUIsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxFQUFFLENBQUMsQ0FBQztJQUNsRSxJQUFNLFVBQVUsR0FBRyxDQUFDLFVBQVUsSUFBSSxlQUFlLEdBQUcsV0FBVyxDQUFDLE1BQU0sQ0FBQztJQUN2RSxXQUFXLEdBQUcsVUFBVTtRQUN0QixDQUFDLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsZUFBZSxDQUFDO1FBQzNDLENBQUMsQ0FBQyxXQUFXLENBQUM7SUFFaEIsTUFBTSxDQUFDLENBQ0wsNkJBQUssS0FBSyxFQUFFLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLFdBQVcsSUFBSSxFQUFFLGFBQWEsRUFBRSxDQUFDLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxDQUFDO1FBRW5JLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsVUFBVTtZQUMxQiw2QkFBSyxLQUFLLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTO2dCQUVsQyxvQkFBQyxPQUFPLElBQUMsRUFBRSxFQUFFLFdBQVc7b0JBQ3RCLDZCQUFLLEtBQUssRUFBRSxDQUFDLEVBQUUsZUFBZSxFQUFFLFVBQU8sSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxJQUFJLEVBQUUsT0FBRyxFQUFFLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBUSxDQUM5STtnQkFDVixvQkFBQyxPQUFPLElBQUMsRUFBRSxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsU0FBUyxFQUFFLFdBQVc7b0JBQzVFO3dCQUNFLDhCQUFNLEtBQUssRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFROzRCQUN6QyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxFQUFFOzRCQUN4QyxDQUFDLFlBQVksSUFBSSxhQUFhLENBQUMsbUJBQW1CLENBQUMsQ0FDL0MsQ0FDSDtvQkFDTiw2QkFBSyxLQUFLLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVzt3QkFDMUMsSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksb0JBQUMsVUFBVSxJQUFDLEtBQUssRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsTUFBTSxHQUFJO3dCQUMxRyxJQUFJLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxvQkFBQyxPQUFPLElBQUMsRUFBRSxFQUFLLHNCQUFzQixVQUFJLElBQUksSUFBSSxJQUFJLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBRSxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxJQUFHLGVBQWUsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxDQUFXLENBQ3ROLENBQ0U7Z0JBRVIsWUFBWSxLQUFLLGFBQWEsQ0FBQyxRQUFRO3VCQUNwQyxDQUNELG9CQUFDLE9BQU8sSUFBQyxFQUFFLEVBQUssMkJBQTJCLFVBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFFLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLFNBQVM7d0JBQ3BJLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUUsR0FBSSxDQUNuRyxDQUNYO2dCQUVGLGVBQWUsQ0FBQyxFQUFFLFlBQVksY0FBQSxFQUFFLFFBQVEsRUFBRSxJQUFJLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQy9ELENBQ0Y7UUFFTiw2QkFBSyxLQUFLLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLElBRWxDLFVBQVU7WUFDUixDQUFDO2dCQUNDO29CQUNHLGlCQUFpQixDQUFDLFdBQVcsRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUM7b0JBQ3JFLDhCQUFNLEtBQUssRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsT0FBTyxFQUFFLGNBQWMsd0JBQXFCLENBQzFGO1lBQ1IsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLFdBQVcsRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FFcEU7UUFHSixDQUFDLFdBQVcsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLGFBQWEsS0FBSyxhQUFhLENBQUMsUUFBUTtZQUNuRSxDQUFDLENBQUMsSUFBSTtZQUNOLENBQUMsQ0FBQyxhQUFhLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUM7Z0JBQ2pDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLFNBQVMsV0FBQSxFQUFFLGVBQWUsaUJBQUEsRUFBRSxDQUFDO2dCQUN4RCxDQUFDLENBQUMsYUFBYSxDQUFDLEVBQUUsV0FBVyxhQUFBLEVBQUUsZUFBZSxpQkFBQSxFQUFFLFFBQVEsRUFBRSxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxVQUFVLEVBQUUsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsWUFBWSxJQUFJLEVBQUUsRUFBRSxDQUFDO1FBRTFNLDZCQUFLLEtBQUssRUFBRSxTQUFTLENBQUMsb0JBQW9CLENBQUMsU0FBUztZQUNsRCw2QkFBSyxLQUFLLEVBQUUsU0FBUyxDQUFDLG9CQUFvQixDQUFDLElBQUk7Z0JBQzVDLGNBQWMsQ0FBQyxTQUFTLENBQUM7Z0JBQ3pCLGNBQWMsQ0FBQztvQkFDZCxRQUFRLEVBQUUsU0FBUztvQkFDbkIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxPQUFPO29CQUMzQixLQUFLLEVBQUUsU0FBUyxDQUFDLG9CQUFvQixDQUFDLElBQUk7b0JBQzFDLFVBQVUsRUFBRSxTQUFTLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCO29CQUMzRCxPQUFPLEVBQUUsaUJBQWlCO2lCQUMzQixDQUFDO2dCQUNELGNBQWMsQ0FBQztvQkFDZCxRQUFRLEVBQUUsS0FBSztvQkFDZixTQUFTLEVBQUUsUUFBUSxDQUFDLE9BQU87b0JBQzNCLEtBQUssRUFBRSxTQUFTLENBQUMsb0JBQW9CLENBQUMsSUFBSTtvQkFDMUMsTUFBTSxFQUFFLElBQUk7b0JBQ1osU0FBUyxFQUFFLElBQUksSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLEVBQUU7b0JBQ3hDLFVBQVUsRUFBRSxTQUFTLENBQUMsb0JBQW9CLENBQUMsWUFBWTtpQkFDeEQsQ0FBQyxDQUNFO1lBRUosSUFBSSxJQUFJLElBQUksQ0FBQyxhQUFhLEtBQUssYUFBYSxDQUFDLFFBQVE7bUJBQ2xELENBQ0QsNkJBQUssS0FBSyxFQUFFLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLElBQzdDLGNBQWMsQ0FBQztvQkFDZCxRQUFRLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLGVBQWU7b0JBQ2hELFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPO29CQUMxRCxLQUFLLEVBQUUsU0FBUyxDQUFDLG9CQUFvQixDQUFDLElBQUk7b0JBQzFDLE1BQU0sRUFBRSxLQUFLO29CQUNiLFNBQVMsRUFBRSxJQUFJLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxFQUFFO29CQUN4QyxVQUFVLEVBQUUsU0FBUyxDQUFDLG9CQUFvQixDQUFDLGNBQWM7b0JBQ3pELE9BQU8sRUFBRTt3QkFDUCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7NEJBQ2hDLE9BQU87Z0NBQ0wsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQztnQ0FDNUIsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO3dCQUMvQixDQUFDO3dCQUFDLElBQUksQ0FBQyxDQUFDOzRCQUNOLFNBQVMsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFBO3dCQUM1QixDQUFDO29CQUNILENBQUM7aUJBQ0YsQ0FBQyxDQUNFLENBQ1AsQ0FFQztRQUNMLHNCQUFzQixFQUFFO1FBRXZCLGNBQWMsSUFBSSxDQUNoQiw2QkFBSyxLQUFLLEVBQUUsU0FBUyxDQUFDLFlBQVk7WUFFOUIsV0FBVyxLQUFLLE9BQU8sV0FBVztnQkFDaEMsbUJBQW1CO2dCQUNuQixDQUFDLENBQUMsb0JBQUMsT0FBTyxJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsa0JBQWtCLEdBQUk7Z0JBQzlDLHFCQUFxQjtnQkFDckIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDO3VCQUN6QixXQUFXLENBQUMsR0FBRyxDQUFDLFVBQUMsT0FBTyxJQUFLLE9BQUEsQ0FDOUIsNkJBQUssR0FBRyxFQUFFLE9BQU8sQ0FBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxZQUFZLENBQUMsU0FBUzt3QkFDM0QsNkJBQUssS0FBSyxFQUFFLENBQUMsRUFBRSxlQUFlLEVBQUUsVUFBUSxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQVUsT0FBSSxFQUFFLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQVE7d0JBQ3BJLDZCQUFLLEtBQUssRUFBRSxTQUFTLENBQUMsWUFBWSxDQUFDLFdBQVc7NEJBQzVDLDhCQUFNLEtBQUssRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLElBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBUTs0QkFDdkUsOEJBQU0sS0FBSyxFQUFFLFNBQVMsQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLE9BQU8sSUFBRyxPQUFPLENBQUMsT0FBTyxDQUFRLENBQzdFLENBQ0YsQ0FDUCxFQVIrQixDQVEvQixDQUFDO1lBR0osa0JBQWtCO2dCQUNoQixDQUFDLENBQUMsQ0FDQSw2QkFBSyxLQUFLLEVBQUUsU0FBUyxDQUFDLGlCQUFpQjtvQkFDckMsd0NBQVMsV0FBVyxFQUFRO29CQUM1Qiw2QkFBSyxLQUFLLEVBQUUsU0FBUyxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQjt3QkFDdkQsMENBQVcsaUJBQWlCLEVBQVU7d0JBQ3RDLDZCQUFLLEtBQUssRUFBRSxTQUFTLENBQUMsaUJBQWlCLENBQUMsV0FBVyxFQUFFLE9BQU8sRUFBRSxZQUFZLGVBQVcsQ0FDakYsQ0FDRixDQUNQO2dCQUNELENBQUMsQ0FBQyxDQUNBLDZCQUFLLEtBQUssRUFBRSxTQUFTLENBQUMsaUJBQWlCO29CQUNyQyx3Q0FBUyxXQUFXLEVBQVE7b0JBQzVCLDZCQUFLLEtBQUssRUFBRSxTQUFTLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxhQUFhLElBQUcsVUFBVSxDQUFPLENBQ3JGLENBQ1AsQ0FFRCxDQUNQO1FBRUgsb0JBQUMsS0FBSyxJQUFDLEtBQUssRUFBRSxZQUFZLENBQUMsT0FBTyxHQUFJLENBQ2xDLENBQ1AsQ0FBQztBQUNKLENBQUM7QUFBQSxDQUFDIn0=
// CONCATENATED MODULE: ./components/container/feed-item/initialize.tsx
var DEFAULT_PROPS = {
    limitTextLength: 150,
    isLastChild: false,
    showComment: false,
    isShowImage: true,
    isShowFullImage: false,
    isFeedDetail: false
};
var INITIAL_STATE = {
    isLike: false,
    isViewMore: false,
    isShowVideo: false,
    isResetInput: false,
    isShowComments: false,
    isShowInputComment: false,
    likeNum: 0,
    commentList: [],
    errorMessage: '',
    answerComment: '',
    inputComment: {
        value: '',
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixlQUFlLEVBQUUsR0FBRztJQUNwQixXQUFXLEVBQUUsS0FBSztJQUNsQixXQUFXLEVBQUUsS0FBSztJQUNsQixXQUFXLEVBQUUsSUFBSTtJQUNqQixlQUFlLEVBQUUsS0FBSztJQUN0QixZQUFZLEVBQUUsS0FBSztDQUNOLENBQUM7QUFFaEIsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHO0lBQzNCLE1BQU0sRUFBRSxLQUFLO0lBQ2IsVUFBVSxFQUFFLEtBQUs7SUFDakIsV0FBVyxFQUFFLEtBQUs7SUFDbEIsWUFBWSxFQUFFLEtBQUs7SUFDbkIsY0FBYyxFQUFFLEtBQUs7SUFDckIsa0JBQWtCLEVBQUUsS0FBSztJQUV6QixPQUFPLEVBQUUsQ0FBQztJQUVWLFdBQVcsRUFBRSxFQUFFO0lBRWYsWUFBWSxFQUFFLEVBQUU7SUFDaEIsYUFBYSxFQUFFLEVBQUU7SUFFakIsWUFBWSxFQUFFO1FBQ1osS0FBSyxFQUFFLEVBQUU7S0FDVjtDQUNZLENBQUMifQ==
// CONCATENATED MODULE: ./components/container/feed-item/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var component_FeedItem = /** @class */ (function (_super) {
    __extends(FeedItem, _super);
    function FeedItem(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    FeedItem.prototype.handleViewMore = function () {
        this.setState({ isViewMore: true });
    };
    ;
    FeedItem.prototype.handleShowComment = function () {
        this.autoShowComment(this.props);
    };
    ;
    FeedItem.prototype.autoShowComment = function (props) {
        if (props === void 0) { props = this.props; }
        var item = props.item, fecthActivityFeedCommentListAction = props.fecthActivityFeedCommentListAction, commentList = props.activityFeedStore.activityFeedCommentList.commentList;
        if (!Object(validate["j" /* isEmptyObject */])(item)) {
            var keyHash = Object(encode["j" /* objectToHash */])({ id: item.id });
            var param = { id: item.id, page: 1, perPage: 20 };
            commentList && !Object(validate["l" /* isUndefined */])(commentList[keyHash])
                ? this.setState({ commentList: commentList[keyHash] })
                : fecthActivityFeedCommentListAction(param);
            this.setState({ isShowComments: true });
        }
    };
    ;
    FeedItem.prototype.handleShowInputComment = function () {
        this.inputCommentRef && this.inputCommentRef.focus();
        this.setState({ isShowInputComment: true });
    };
    ;
    FeedItem.prototype.handleSubmit = function () {
        var answerComment = this.state.answerComment;
        var _a = this.props, item = _a.item, addActivityFeedCommentAction = _a.addActivityFeedCommentAction;
        var comment = answerComment && answerComment.trim() || '';
        if (0 === comment.length) {
            return;
        }
        addActivityFeedCommentAction({
            id: item.id,
            content: comment
        });
    };
    FeedItem.prototype.handleLike = function () {
        var _a = this.props, addActivityFeedLikeAction = _a.addActivityFeedLikeAction, deleteActivityFeedLikeAction = _a.deleteActivityFeedLikeAction;
        var likeNumber = this.state.likeNum || 0;
        var tmpLikeNum = 0;
        if (this.state.isLike) {
            tmpLikeNum = likeNumber > 0 ? likeNumber - 1 : 0;
            deleteActivityFeedLikeAction({ id: this.props.item.id });
        }
        else {
            tmpLikeNum = likeNumber + 1;
            addActivityFeedLikeAction({ id: this.props.item.id });
        }
        this.setState({
            isLike: !this.state.isLike,
            likeNum: tmpLikeNum
        });
    };
    FeedItem.prototype.handleInputOnChange = function (e) {
        this.setState({ answerComment: e.target.value });
    };
    FeedItem.prototype.handleInputOnFocus = function () {
        this.setState({ errorMessage: '' });
    };
    FeedItem.prototype.handleShowVideo = function () {
        this.setState({ isShowVideo: true });
    };
    FeedItem.prototype.setInputCommentRef = function (ref) {
        this.inputCommentRef = ref;
    };
    FeedItem.prototype.componentDidMount = function () {
        var _a = this.props, item = _a.item, showComment = _a.showComment;
        showComment && this.handleShowComment();
        this.setState({
            isLike: item.liked,
            likeNum: item.total_likes || 0
        });
    };
    FeedItem.prototype.componentWillReceiveProps = function (nextProps) {
        var _a = this.props, item = _a.item, _b = _a.activityFeedStore, isAddCommentSuccess = _b.isAddCommentSuccess, isFetchActivityFeedDetailSuccess = _b.isFetchActivityFeedDetailSuccess, isFetchCommentListSuccess = _b.activityFeedCommentList.isFetchCommentListSuccess;
        var commentList = nextProps.activityFeedStore.activityFeedCommentList.commentList;
        var keyHash = Object(encode["j" /* objectToHash */])({ id: item && item.id || 0 });
        var _commentList = commentList && !Object(validate["l" /* isUndefined */])(commentList[keyHash]) ? commentList[keyHash] : this.state.commentList;
        !isFetchCommentListSuccess
            && nextProps.activityFeedStore.activityFeedCommentList.isFetchCommentListSuccess
            && this.setState({ commentList: _commentList });
        !isAddCommentSuccess
            && nextProps.activityFeedStore.isAddCommentSuccess
            && this.setState({ commentList: _commentList, answerComment: '' });
        !isFetchActivityFeedDetailSuccess
            && nextProps.activityFeedStore.isFetchActivityFeedDetailSuccess
            && this.autoShowComment(nextProps);
    };
    FeedItem.prototype.shouldComponentUpdate = function (nextProps, nextState) {
        if ((false === this.state.isShowComments && true === nextState.isShowComments)) {
            return true;
        }
        ;
        if ((false === this.state.isViewMore && true === nextState.isViewMore)) {
            return true;
        }
        ;
        if ((false === this.state.isShowVideo && true === nextState.isShowVideo)) {
            return true;
        }
        ;
        if (this.state.likeNum !== nextState.likeNum) {
            return true;
        }
        ;
        // const keyHash = objectToHash({ id: nextProps.item.id });
        // if (true === this.state.isShowComments
        //   //&& this.props.item.id !== nextProps.item.id
        //   && nextProps.activityFeedStore.activityFeedCommentList
        //   && nextProps.activityFeedStore.activityFeedCommentList.commentList
        //   && false === isUndefined(nextProps.activityFeedStore.activityFeedCommentList.commentList[keyHash])) { return true; };
        if (true === this.state.isShowComments) {
            return true;
        }
        ;
        if (this.state.answerComment !== nextState.answerComment) {
            return true;
        }
        ;
        if (nextProps.isFeedDetail !== this.props.isFeedDetail) {
            return true;
        }
        ;
        if (!Object(validate["h" /* isCompareObject */])(this.props.item, nextState.item)) {
            return true;
        }
        ;
        if (!Object(validate["h" /* isCompareObject */])(nextProps.userProfile, this.props.userProfile)) {
            return true;
        }
        ;
        return false;
    };
    FeedItem.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state,
            handleViewMore: this.handleViewMore.bind(this),
            handleShowComment: this.handleShowComment.bind(this),
            handleShowInputComment: this.handleShowInputComment.bind(this),
            handleSubmit: this.handleSubmit.bind(this),
            handleLike: this.handleLike.bind(this),
            handleInputOnChange: this.handleInputOnChange.bind(this),
            handleInputOnFocus: this.handleInputOnFocus.bind(this),
            handleShowVideo: this.handleShowVideo.bind(this),
            setInputCommentRef: this.setInputCommentRef.bind(this)
        };
        return renderView(args);
    };
    ;
    FeedItem.defaultProps = DEFAULT_PROPS;
    FeedItem = __decorate([
        radium
    ], FeedItem);
    return FeedItem;
}(react["Component"]));
/* harmony default export */ var component = (component_FeedItem);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3JELE9BQU8sRUFBRSxXQUFXLEVBQUUsZUFBZSxFQUFFLGFBQWEsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBR3RGLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFDcEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFHNUQ7SUFBdUIsNEJBQXVDO0lBSTVELGtCQUFZLEtBQUs7UUFBakIsWUFDRSxrQkFBTSxLQUFLLENBQUMsU0FFYjtRQURDLEtBQUksQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDOztJQUM3QixDQUFDO0lBRUQsaUNBQWMsR0FBZDtRQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxVQUFVLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBQUEsQ0FBQztJQUVGLG9DQUFpQixHQUFqQjtRQUNFLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFBQSxDQUFDO0lBRUYsa0NBQWUsR0FBZixVQUFnQixLQUFrQjtRQUFsQixzQkFBQSxFQUFBLFFBQVEsSUFBSSxDQUFDLEtBQUs7UUFFOUIsSUFBQSxpQkFBSSxFQUNKLDZFQUFrQyxFQUNjLHlFQUFXLENBQ25EO1FBRVYsRUFBRSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQU0sT0FBTyxHQUFHLFlBQVksQ0FBQyxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUM5QyxJQUFNLEtBQUssR0FBRyxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDO1lBRXBELFdBQVcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQy9DLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsV0FBVyxFQUFFLFdBQVcsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDO2dCQUN0RCxDQUFDLENBQUMsa0NBQWtDLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFOUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLGNBQWMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBQzFDLENBQUM7SUFDSCxDQUFDO0lBQUEsQ0FBQztJQUVGLHlDQUFzQixHQUF0QjtRQUNFLElBQUksQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNyRCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsa0JBQWtCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBQUEsQ0FBQztJQUVGLCtCQUFZLEdBQVo7UUFDVSxJQUFBLHdDQUFhLENBQThCO1FBQzdDLElBQUEsZUFBaUUsRUFBL0QsY0FBSSxFQUFFLDhEQUE0QixDQUE4QjtRQUV4RSxJQUFNLE9BQU8sR0FBRyxhQUFhLElBQUksYUFBYSxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQztRQUM1RCxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDekIsTUFBTSxDQUFDO1FBQ1QsQ0FBQztRQUVELDRCQUE0QixDQUFDO1lBQzNCLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRTtZQUNYLE9BQU8sRUFBRSxPQUFPO1NBQ2pCLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCw2QkFBVSxHQUFWO1FBQ1EsSUFBQSxlQUF3RSxFQUF0RSx3REFBeUIsRUFBRSw4REFBNEIsQ0FBZ0I7UUFDL0UsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLElBQUksQ0FBQyxDQUFDO1FBQzNDLElBQUksVUFBVSxHQUFHLENBQUMsQ0FBQztRQUVuQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDdEIsVUFBVSxHQUFHLFVBQVUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqRCw0QkFBNEIsQ0FBQyxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzNELENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLFVBQVUsR0FBRyxVQUFVLEdBQUcsQ0FBQyxDQUFDO1lBQzVCLHlCQUF5QixDQUFDLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDeEQsQ0FBQztRQUVELElBQUksQ0FBQyxRQUFRLENBQUM7WUFDWixNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU07WUFDMUIsT0FBTyxFQUFFLFVBQVU7U0FDcEIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHNDQUFtQixHQUFuQixVQUFvQixDQUFDO1FBQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRCxxQ0FBa0IsR0FBbEI7UUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBZ0IsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRCxrQ0FBZSxHQUFmO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQWdCLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQscUNBQWtCLEdBQWxCLFVBQW1CLEdBQUc7UUFDcEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxHQUFHLENBQUM7SUFDN0IsQ0FBQztJQUVELG9DQUFpQixHQUFqQjtRQUNRLElBQUEsZUFBa0MsRUFBaEMsY0FBSSxFQUFFLDRCQUFXLENBQWdCO1FBRXpDLFdBQVcsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUV4QyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ1osTUFBTSxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2xCLE9BQU8sRUFBRSxJQUFJLENBQUMsV0FBVyxJQUFJLENBQUM7U0FDL0IsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDRDQUF5QixHQUF6QixVQUEwQixTQUFTO1FBQzNCLElBQUEsZUFPUSxFQU5aLGNBQUksRUFDSix5QkFJQyxFQUhDLDRDQUFtQixFQUNuQixzRUFBZ0MsRUFDTCxnRkFBeUIsQ0FFekM7UUFFeUMsSUFBQSw2RUFBVyxDQUFtQjtRQUV0RixJQUFNLE9BQU8sR0FBRyxZQUFZLENBQUMsRUFBRSxFQUFFLEVBQUUsSUFBSSxJQUFJLElBQUksQ0FBQyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUUzRCxJQUFNLFlBQVksR0FBRyxXQUFXLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUM7UUFFdkgsQ0FBQyx5QkFBeUI7ZUFDckIsU0FBUyxDQUFDLGlCQUFpQixDQUFDLHVCQUF1QixDQUFDLHlCQUF5QjtlQUM3RSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxDQUFDLENBQUM7UUFFbEQsQ0FBQyxtQkFBbUI7ZUFDZixTQUFTLENBQUMsaUJBQWlCLENBQUMsbUJBQW1CO2VBQy9DLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUFFLGFBQWEsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBRXJFLENBQUMsZ0NBQWdDO2VBQzVCLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxnQ0FBZ0M7ZUFDNUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRUQsd0NBQXFCLEdBQXJCLFVBQXNCLFNBQVMsRUFBRSxTQUFTO1FBRXhDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxJQUFJLElBQUksS0FBSyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUFDLENBQUM7UUFBQSxDQUFDO1FBQ2pHLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxJQUFJLElBQUksS0FBSyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUFDLENBQUM7UUFBQSxDQUFDO1FBQ3pGLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxJQUFJLElBQUksS0FBSyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUFDLENBQUM7UUFBQSxDQUFDO1FBQzNGLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxLQUFLLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUFDLENBQUM7UUFBQSxDQUFDO1FBRS9ELDJEQUEyRDtRQUMzRCx5Q0FBeUM7UUFDekMsa0RBQWtEO1FBQ2xELDJEQUEyRDtRQUMzRCx1RUFBdUU7UUFDdkUsMEhBQTBIO1FBRTFILEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQUMsQ0FBQztRQUFBLENBQUM7UUFDekQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEtBQUssU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQUMsQ0FBQztRQUFBLENBQUM7UUFDM0UsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLFlBQVksS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQUMsQ0FBQztRQUFBLENBQUM7UUFDekUsRUFBRSxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFBQyxDQUFDO1FBQUEsQ0FBQztRQUN4RSxFQUFFLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUFDLENBQUM7UUFBQSxDQUFDO1FBRXRGLE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQseUJBQU0sR0FBTjtRQUNFLElBQU0sSUFBSSxHQUFHO1lBQ1gsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixjQUFjLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzlDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ3BELHNCQUFzQixFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzlELFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUMsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUN0QyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUN4RCxrQkFBa0IsRUFBRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUN0RCxlQUFlLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2hELGtCQUFrQixFQUFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ3ZELENBQUE7UUFFRCxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFBQSxDQUFDO0lBektLLHFCQUFZLEdBQWUsYUFBYSxDQUFDO0lBRDVDLFFBQVE7UUFEYixNQUFNO09BQ0QsUUFBUSxDQTJLYjtJQUFELGVBQUM7Q0FBQSxBQTNLRCxDQUF1QixLQUFLLENBQUMsU0FBUyxHQTJLckM7QUFDRCxlQUFlLFFBQVEsQ0FBQyJ9
// CONCATENATED MODULE: ./components/container/feed-item/store.tsx
var connect = __webpack_require__(129).connect;




var mapStateToProps = function (state) { return ({
    listLikedId: state.like.liked.id,
    activityFeedStore: state.activityFeed
}); };
var mapDispatchToProps = function (dispatch) { return ({
    fecthActivityFeedCommentListAction: function (data) { return dispatch(Object(activity_feed["d" /* fecthActivityFeedCommentListAction */])(data)); },
    addActivityFeedCommentAction: function (data) { return dispatch(Object(activity_feed["a" /* addActivityFeedCommentAction */])(data)); },
    addActivityFeedLikeAction: function (data) { return dispatch(Object(activity_feed["b" /* addActivityFeedLikeAction */])(data)); },
    deleteActivityFeedLikeAction: function (data) { return dispatch(Object(activity_feed["c" /* deleteActivityFeedLikeAction */])(data)); },
    openModal: function (data) { return dispatch(Object(modal["c" /* openModalAction */])(data)); },
    likeProduct: function (productId) { return dispatch(Object(like["d" /* likeProductAction */])(productId)); },
    unLikeProduct: function (productId) { return dispatch(Object(like["a" /* UnLikeProductAction */])(productId)); },
}); };
/* harmony default export */ var store = (connect(mapStateToProps, mapDispatchToProps)(component));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLEVBQ0wsa0NBQWtDLEVBQ2xDLDRCQUE0QixFQUM1Qix5QkFBeUIsRUFDekIsNEJBQTRCLEVBQzdCLE1BQU0sK0JBQStCLENBQUM7QUFDdkMsT0FBTyxFQUFFLGlCQUFpQixFQUFFLG1CQUFtQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDOUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBRXhELE9BQU8sUUFBUSxNQUFNLGFBQWEsQ0FBQztBQUVuQyxNQUFNLENBQUMsSUFBTSxlQUFlLEdBQUcsVUFBQyxLQUFLLElBQUssT0FBQSxDQUFDO0lBQ3pDLFdBQVcsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO0lBQ2hDLGlCQUFpQixFQUFFLEtBQUssQ0FBQyxZQUFZO0NBQ3RDLENBQUMsRUFId0MsQ0FHeEMsQ0FBQztBQUVILE1BQU0sQ0FBQyxJQUFNLGtCQUFrQixHQUFHLFVBQUMsUUFBUSxJQUFLLE9BQUEsQ0FBQztJQUMvQyxrQ0FBa0MsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyxrQ0FBa0MsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFsRCxDQUFrRDtJQUNyRyw0QkFBNEIsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyw0QkFBNEIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUE1QyxDQUE0QztJQUN6Rix5QkFBeUIsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUF6QyxDQUF5QztJQUNuRiw0QkFBNEIsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyw0QkFBNEIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUE1QyxDQUE0QztJQUN6RixTQUFTLEVBQUUsVUFBQyxJQUFTLElBQUssT0FBQSxRQUFRLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQS9CLENBQStCO0lBQ3pELFdBQVcsRUFBRSxVQUFDLFNBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUF0QyxDQUFzQztJQUNsRSxhQUFhLEVBQUUsVUFBQyxTQUFTLElBQUssT0FBQSxRQUFRLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBeEMsQ0FBd0M7Q0FDdkUsQ0FBQyxFQVI4QyxDQVE5QyxDQUFDO0FBR0gsZUFBZSxPQUFPLENBQ3BCLGVBQWUsRUFDZixrQkFBa0IsQ0FDbkIsQ0FBQyxRQUFRLENBQUMsQ0FBQyJ9
// CONCATENATED MODULE: ./components/container/feed-item/index.tsx

/* harmony default export */ var feed_item = __webpack_exports__["a"] = (store);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxRQUFRLE1BQU0sU0FBUyxDQUFDO0FBQy9CLGVBQWUsUUFBUSxDQUFDIn0=

/***/ }),

/***/ 774:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FEEDABLE_TYPE; });
var FEEDABLE_TYPE = {
    FEEDBACK: 'Feedback',
    BLOG: 'Blog',
    LOVE: 'Love',
    THEME: 'Theme',
    BRAND: 'Brand',
    DISCOUNTCODE: 'DiscountCode',
    UNBOXING: 'Unboxing',
    BOX: 'Box',
    BROWSENODE: 'BrowseNode'
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmVlZGFibGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJmZWVkYWJsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUc7SUFDM0IsUUFBUSxFQUFFLFVBQVU7SUFDcEIsSUFBSSxFQUFFLE1BQU07SUFDWixJQUFJLEVBQUUsTUFBTTtJQUNaLEtBQUssRUFBRSxPQUFPO0lBQ2QsS0FBSyxFQUFFLE9BQU87SUFDZCxZQUFZLEVBQUUsY0FBYztJQUM1QixRQUFRLEVBQUUsVUFBVTtJQUNwQixHQUFHLEVBQUUsS0FBSztJQUNWLFVBQVUsRUFBRSxZQUFZO0NBQ3pCLENBQUMifQ==

/***/ }),

/***/ 778:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/image.ts
var utils_image = __webpack_require__(349);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./components/container/image-slider-community/initialize.tsx
var DEFAULT_PROPS = {
    data: [],
    column: 1,
    posImg: 0
};
var INITIAL_STATE = function (data) { return ({
    imageList: data || [],
    imageSlide: [],
    imageSlideSelected: {},
    countChangeSlide: 0,
    firstInit: false
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLE1BQU0sRUFBRSxDQUFDO0lBQ1QsTUFBTSxFQUFFLENBQUM7Q0FDQSxDQUFDO0FBRVosTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHLFVBQUMsSUFBUyxJQUFLLE9BQUEsQ0FBQztJQUMzQyxTQUFTLEVBQUUsSUFBSSxJQUFJLEVBQUU7SUFDckIsVUFBVSxFQUFFLEVBQUU7SUFDZCxrQkFBa0IsRUFBRSxFQUFFO0lBQ3RCLGdCQUFnQixFQUFFLENBQUM7SUFDbkIsU0FBUyxFQUFFLEtBQUs7Q0FDTixDQUFBLEVBTmdDLENBTWhDLENBQUMifQ==
// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(347);

// EXTERNAL MODULE: ./style/component.ts
var component = __webpack_require__(745);

// CONCATENATED MODULE: ./components/container/image-slider-item-community/initialize.tsx
var initialize_DEFAULT_PROPS = {
    item: [],
    type: '',
    column: 4
};
var initialize_INITIAL_STATE = {
    isRenderAgain: false
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLElBQUksRUFBRSxFQUFFO0lBQ1IsTUFBTSxFQUFFLENBQUM7Q0FDQSxDQUFDO0FBRVosTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHO0lBQzNCLGFBQWEsRUFBRSxLQUFLO0NBQ1gsQ0FBQyJ9
// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/container/image-slider-item-community/style.tsx


var generateSwitchStyle = function (mobile, desktop) {
    var switchStyle = {
        MOBILE: { width: mobile, minWidth: mobile },
        DESKTOP: { width: desktop, cursor: 'pointer' }
    };
    return switchStyle[window.DEVICE_VERSION];
};
/* harmony default export */ var style = ({
    heading: {
        paddingLeft: 10,
        display: variable["display"].inlineBlock,
        maxWidth: "100%",
        whiteSpace: "nowrap",
        overflow: "hidden",
        textOverflow: "ellipsis",
        fontFamily: variable["fontAvenirMedium"],
        color: variable["colorBlack"],
        fontSize: 20,
        lineHeight: "40px",
        height: 40,
        letterSpacing: -0.5,
        textTransform: "uppercase",
    },
    container: {
        width: '100%',
        overflowX: 'auto',
        whiteSpace: 'nowrap',
        paddingTop: 0,
        marginBottom: 0,
        display: variable["display"].flex,
        itemSlider: {
            width: "100%",
            height: "100%",
            display: variable["display"].inlineBlock,
            borderRadius: 5,
            overflow: 'hidden',
            boxShadow: variable["shadowBlur"],
            position: variable["position"].relative
        },
        itemSliderPanel: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    height: '100%',
                    maxWidth: '100vw',
                    width: '100vw'
                }],
            DESKTOP: [{
                    height: 'auto',
                    maxHeight: '90vh',
                    maxWidth: "calc(90vw - 400px)",
                    position: variable["position"].relative,
                    backgroundColor: variable["colorF7"],
                    display: variable["display"].block,
                    minHeight: 400,
                    minWidth: 400
                }],
            GENERAL: [{
                    objectFit: 'contain',
                }]
        }),
        videoIcon: {
            width: 70,
            height: 70,
            position: variable["position"].absolute,
            top: '50%',
            left: '55%',
            transform: 'translate(-50%, -50%)',
            borderTop: '35px solid transparent',
            boxSizing: 'border-box',
            borderLeft: '51px solid white',
            borderBottom: '35px solid transparent',
            opacity: .8
        },
        info: {
            width: '100%',
            height: 110,
            padding: 12,
            position: variable["position"].relative,
            background: variable["colorWhite"],
            display: variable["display"].flex,
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
            image: function (imgUrl) { return ({
                width: '100%',
                height: '100%',
                top: 0,
                left: 0,
                zIndex: -1,
                position: variable["position"].absolute,
                backgroundColor: variable["colorF7"],
                backgroundImage: "url(" + imgUrl + ")",
                backgroundPosition: 'bottom center',
                backgroundSize: 'cover',
                filter: 'blur(4px)',
                transform: "scaleY(-1) scale(1.1)",
                'WebkitBackfaceVisibility': 'hidden',
                'WebkitPerspective': 1000,
                'WebkitTransform': ['translate3d(0,0,0)', 'translateZ(0)'],
                'backfaceVisibility': 'hidden',
                perspective: 1000,
            }); },
            title: {
                color: variable["colorBlack"],
                whiteSpace: 'pre-wrap',
                fontFamily: variable["fontAvenirMedium"],
                fontSize: 14,
                lineHeight: '22px',
                maxHeight: '44px',
                overflow: 'hidden',
                marginBottom: 5
            },
            description: {
                fontSize: 12,
                color: variable["colorBlack"],
                fontFamily: variable["fontAvenirMedium"],
                opacity: .7,
                marginRight: 10,
                whiteSpace: 'pre-wrap',
                lineHeight: '18px',
                maxHeight: 36,
                overflow: 'hidden',
            },
            tagList: {
                width: '100%',
                display: variable["display"].flex,
                flexWrap: 'wrap',
                height: '36px',
                maxHeight: '36px',
                overflow: 'hidden'
            },
            tagItem: {
                fontSize: 12,
                lineHeight: '18px',
                color: variable["colorBlack"],
                fontFamily: variable["fontAvenirMedium"],
                opacity: .7,
                marginRight: 10,
                whiteSpace: 'pre-wrap'
            },
        }
    },
    column: {
        1: { width: '100%' },
        2: { width: '50%' },
        3: generateSwitchStyle('75%', '33.33%'),
        4: generateSwitchStyle('47%', '25%'),
        5: generateSwitchStyle('47%', '20%'),
        6: generateSwitchStyle('50%', '16.66%'),
    },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFekQsSUFBTSxtQkFBbUIsR0FBRyxVQUFDLE1BQU0sRUFBRSxPQUFPO0lBQzFDLElBQU0sV0FBVyxHQUFHO1FBQ2xCLE1BQU0sRUFBRSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRTtRQUMzQyxPQUFPLEVBQUUsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUU7S0FDL0MsQ0FBQztJQUVGLE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0FBQzVDLENBQUMsQ0FBQztBQUVGLGVBQWU7SUFDYixPQUFPLEVBQUU7UUFDUCxXQUFXLEVBQUUsRUFBRTtRQUNmLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLFdBQVc7UUFDckMsUUFBUSxFQUFFLE1BQU07UUFDaEIsVUFBVSxFQUFFLFFBQVE7UUFDcEIsUUFBUSxFQUFFLFFBQVE7UUFDbEIsWUFBWSxFQUFFLFVBQVU7UUFDeEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7UUFDckMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1FBQzFCLFFBQVEsRUFBRSxFQUFFO1FBQ1osVUFBVSxFQUFFLE1BQU07UUFDbEIsTUFBTSxFQUFFLEVBQUU7UUFDVixhQUFhLEVBQUUsQ0FBQyxHQUFHO1FBQ25CLGFBQWEsRUFBRSxXQUFXO0tBQzNCO0lBRUQsU0FBUyxFQUFFO1FBQ1QsS0FBSyxFQUFFLE1BQU07UUFDYixTQUFTLEVBQUUsTUFBTTtRQUNqQixVQUFVLEVBQUUsUUFBUTtRQUNwQixVQUFVLEVBQUUsQ0FBQztRQUNiLFlBQVksRUFBRSxDQUFDO1FBQ2YsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUU5QixVQUFVLEVBQUU7WUFDVixLQUFLLEVBQUUsTUFBTTtZQUNiLE1BQU0sRUFBRSxNQUFNO1lBQ2QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztZQUNyQyxZQUFZLEVBQUUsQ0FBQztZQUNmLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTtZQUM5QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1NBQ3JDO1FBRUQsZUFBZSxFQUFFLFlBQVksQ0FBQztZQUM1QixNQUFNLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsTUFBTTtvQkFDZCxRQUFRLEVBQUUsT0FBTztvQkFDakIsS0FBSyxFQUFFLE9BQU87aUJBQ2YsQ0FBQztZQUVGLE9BQU8sRUFBRSxDQUFDO29CQUNSLE1BQU0sRUFBRSxNQUFNO29CQUNkLFNBQVMsRUFBRSxNQUFNO29CQUNqQixRQUFRLEVBQUUsb0JBQW9CO29CQUM5QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUNwQyxlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87b0JBQ2pDLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7b0JBQy9CLFNBQVMsRUFBRSxHQUFHO29CQUNkLFFBQVEsRUFBRSxHQUFHO2lCQUNkLENBQUM7WUFFRixPQUFPLEVBQUUsQ0FBQztvQkFDUixTQUFTLEVBQUUsU0FBUztpQkFDckIsQ0FBQztTQUNILENBQUM7UUFFRixTQUFTLEVBQUU7WUFDVCxLQUFLLEVBQUUsRUFBRTtZQUNULE1BQU0sRUFBRSxFQUFFO1lBQ1YsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxHQUFHLEVBQUUsS0FBSztZQUNWLElBQUksRUFBRSxLQUFLO1lBQ1gsU0FBUyxFQUFFLHVCQUF1QjtZQUNsQyxTQUFTLEVBQUUsd0JBQXdCO1lBQ25DLFNBQVMsRUFBRSxZQUFZO1lBQ3ZCLFVBQVUsRUFBRSxrQkFBa0I7WUFDOUIsWUFBWSxFQUFFLHdCQUF3QjtZQUN0QyxPQUFPLEVBQUUsRUFBRTtTQUNaO1FBRUQsSUFBSSxFQUFFO1lBQ0osS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsR0FBRztZQUNYLE9BQU8sRUFBRSxFQUFFO1lBQ1gsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDL0IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixhQUFhLEVBQUUsUUFBUTtZQUN2QixjQUFjLEVBQUUsWUFBWTtZQUM1QixVQUFVLEVBQUUsWUFBWTtZQUV4QixLQUFLLEVBQUUsVUFBQyxNQUFNLElBQUssT0FBQSxDQUFDO2dCQUNsQixLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsTUFBTTtnQkFDZCxHQUFHLEVBQUUsQ0FBQztnQkFDTixJQUFJLEVBQUUsQ0FBQztnQkFDUCxNQUFNLEVBQUUsQ0FBQyxDQUFDO2dCQUNWLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztnQkFDakMsZUFBZSxFQUFFLFNBQU8sTUFBTSxNQUFHO2dCQUNqQyxrQkFBa0IsRUFBRSxlQUFlO2dCQUNuQyxjQUFjLEVBQUUsT0FBTztnQkFDdkIsTUFBTSxFQUFFLFdBQVc7Z0JBQ25CLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLDBCQUEwQixFQUFFLFFBQVE7Z0JBQ3BDLG1CQUFtQixFQUFFLElBQUk7Z0JBQ3pCLGlCQUFpQixFQUFFLENBQUMsb0JBQW9CLEVBQUUsZUFBZSxDQUFDO2dCQUMxRCxvQkFBb0IsRUFBRSxRQUFRO2dCQUM5QixXQUFXLEVBQUUsSUFBSTthQUNsQixDQUFDLEVBbEJpQixDQWtCakI7WUFFRixLQUFLLEVBQUU7Z0JBQ0wsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMxQixVQUFVLEVBQUUsVUFBVTtnQkFDdEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxNQUFNO2dCQUNsQixTQUFTLEVBQUUsTUFBTTtnQkFDakIsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFlBQVksRUFBRSxDQUFDO2FBQ2hCO1lBRUQsV0FBVyxFQUFFO2dCQUNYLFFBQVEsRUFBRSxFQUFFO2dCQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLE9BQU8sRUFBRSxFQUFFO2dCQUNYLFdBQVcsRUFBRSxFQUFFO2dCQUNmLFVBQVUsRUFBRSxVQUFVO2dCQUN0QixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsU0FBUyxFQUFFLEVBQUU7Z0JBQ2IsUUFBUSxFQUFFLFFBQVE7YUFDbkI7WUFFRCxPQUFPLEVBQUU7Z0JBQ1AsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsUUFBUSxFQUFFLE1BQU07Z0JBQ2hCLE1BQU0sRUFBRSxNQUFNO2dCQUNkLFNBQVMsRUFBRSxNQUFNO2dCQUNqQixRQUFRLEVBQUUsUUFBUTthQUNuQjtZQUVELE9BQU8sRUFBRTtnQkFDUCxRQUFRLEVBQUUsRUFBRTtnQkFDWixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtnQkFDckMsT0FBTyxFQUFFLEVBQUU7Z0JBQ1gsV0FBVyxFQUFFLEVBQUU7Z0JBQ2YsVUFBVSxFQUFFLFVBQVU7YUFDdkI7U0FDRjtLQUNGO0lBRUQsTUFBTSxFQUFFO1FBQ04sQ0FBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRTtRQUNwQixDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFO1FBQ25CLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDO1FBQ3ZDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDO1FBQ3BDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDO1FBQ3BDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDO0tBQ3hDO0NBQ3FCLENBQUMifQ==
// CONCATENATED MODULE: ./components/container/image-slider-item-community/view.tsx


function renderComponent(_a) {
    var props = _a.props;
    var _b = props, item = _b.item, column = _b.column;
    return (react["createElement"]("div", { style: style.column[column || 1] },
        react["createElement"]("img", { style: style.container.itemSliderPanel, src: item && item.original_url || '', id: "img-community" })));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRy9CLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixNQUFNLDBCQUEwQixFQUFTO1FBQVAsZ0JBQUs7SUFDL0IsSUFBQSxVQUFrQyxFQUFoQyxjQUFJLEVBQUUsa0JBQU0sQ0FBcUI7SUFFekMsTUFBTSxDQUFDLENBQ0wsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQztRQUNuQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxlQUFlLEVBQUUsR0FBRyxFQUFFLElBQUksSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLEVBQUUsRUFBRSxFQUFFLEVBQUUsZUFBZSxHQUFJLENBQ3RHLENBQ1AsQ0FBQztBQUNKLENBQUM7QUFBQSxDQUFDIn0=
// CONCATENATED MODULE: ./components/container/image-slider-item-community/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var component_SlideItem = /** @class */ (function (_super) {
    __extends(SlideItem, _super);
    function SlideItem(props) {
        var _this = _super.call(this, props) || this;
        _this.state = initialize_INITIAL_STATE;
        return _this;
    }
    SlideItem.prototype.componentDidMount = function () {
        Object(responsive["b" /* isDesktopVersion */])() && this.setState({ isRenderAgain: true }); // Set state that target
    };
    SlideItem.prototype.componentDidUpdate = function () {
        if (Object(responsive["b" /* isDesktopVersion */])()) {
            var el = document.getElementById('img-community');
            el && this.props.handleOmitImgHeight(el.clientHeight);
        }
    };
    SlideItem.prototype.render = function () {
        var args = {
            props: this.props
        };
        return renderComponent(args);
    };
    SlideItem.defaultProps = initialize_DEFAULT_PROPS;
    SlideItem = __decorate([
        radium
    ], SlideItem);
    return SlideItem;
}(react["Component"]));
;
/* harmony default export */ var image_slider_item_community_component = (component_SlideItem);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFN0QsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDNUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUl6QztJQUF3Qiw2QkFBK0I7SUFHckQsbUJBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUM7O0lBQzdCLENBQUM7SUFFRCxxQ0FBaUIsR0FBakI7UUFDRSxnQkFBZ0IsRUFBRSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxhQUFhLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLHdCQUF3QjtJQUN4RixDQUFDO0lBRUQsc0NBQWtCLEdBQWxCO1FBQ0UsRUFBRSxDQUFDLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDdkIsSUFBTSxFQUFFLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUNwRCxFQUFFLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDeEQsQ0FBQztJQUNILENBQUM7SUFFRCwwQkFBTSxHQUFOO1FBQ0UsSUFBTSxJQUFJLEdBQUc7WUFDWCxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7U0FDbEIsQ0FBQztRQUVGLE1BQU0sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQXhCTSxzQkFBWSxHQUFXLGFBQWEsQ0FBQztJQUR4QyxTQUFTO1FBRGQsTUFBTTtPQUNELFNBQVMsQ0EwQmQ7SUFBRCxnQkFBQztDQUFBLEFBMUJELENBQXdCLEtBQUssQ0FBQyxTQUFTLEdBMEJ0QztBQUFBLENBQUM7QUFFRixlQUFlLFNBQVMsQ0FBQyJ9
// CONCATENATED MODULE: ./components/container/image-slider-item-community/index.tsx

/* harmony default export */ var image_slider_item_community = (image_slider_item_community_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxTQUFTLE1BQU0sYUFBYSxDQUFDO0FBQ3BDLGVBQWUsU0FBUyxDQUFDIn0=
// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// CONCATENATED MODULE: ./components/container/image-slider-community/style.tsx




var INLINE_STYLE = {
    '.image-slide-container .pagination': {
        opacity: 0,
        transform: 'translateY(20px)'
    },
    '.image-slide-container:hover .pagination': {
        opacity: 1,
        transform: 'translateY(0)'
    },
    '.image-slide-container .left-nav': {
        width: 40,
        height: 60,
        opacity: 0,
        transform: 'translate(-60px, -50%)',
        visibility: 'hidden',
    },
    '.image-slide-container:hover .left-nav': {
        opacity: 1,
        transform: 'translate(1px, -50%)',
        visibility: 'visible',
    },
    '.image-slide-container .right-nav': {
        width: 40,
        height: 60,
        opacity: 0,
        transform: 'translate(60px, -50%)',
        visibility: 'hidden',
    },
    '.image-slide-container:hover .right-nav': {
        opacity: 1,
        transform: 'translate(1px, -50%)',
        visibility: 'visible',
    }
};
/* harmony default export */ var image_slider_community_style = ({
    container: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{}],
        DESKTOP: [{}],
        GENERAL: [{
                display: variable["display"].block,
                width: '100%',
                height: '100%'
            }]
    }),
    imageSlide: {
        position: 'relative',
        overflow: 'hidden',
        height: '100%',
        container: Object.assign({}, layout["a" /* flexContainer */].justify, component["c" /* block */].content, {
            paddingTop: 10,
            transition: variable["transitionOpacity"],
        }),
        pagination: [
            layout["a" /* flexContainer */].center,
            component["f" /* slidePagination */],
            {
                transition: variable["transitionNormal"],
                bottom: 0
            },
        ],
        navigation: [
            layout["a" /* flexContainer */].center,
            layout["a" /* flexContainer */].verticalCenter,
            component["e" /* slideNavigation */]
        ],
    },
    customStyleLoading: {
        height: 300
    },
    mobileWrap: {
        width: '100%',
        overflowX: 'auto',
        panel: {
            whiteSpace: 'nowrap',
            flexDirection: 'column'
        }
    },
    placeholder: {
        width: '100%',
        display: variable["display"].flex,
        marginBottom: 35,
        item: {
            flex: 1,
            paddingLeft: 10,
            paddingRight: 10,
        },
        image: {
            width: '100%',
            height: 'auto',
            paddingTop: '65%',
            marginBottom: 10,
        },
        text: {
            width: '94%',
            height: 25,
            marginBottom: 10,
        },
        lastText: {
            width: '65%',
            height: 25,
        }
    },
    desktop: {
        mainWrap: {
            display: variable["display"].block,
            height: '100%'
        },
        container: {
            width: '100%',
            height: '100%',
            overflowX: 'auto',
            whiteSpace: 'nowrap',
            paddingTop: 0,
            display: variable["display"].flex,
            overflow: 'hidden',
            itemSlider: {
                width: "100%",
                display: variable["display"].inlineBlock,
                borderRadius: 5,
                overflow: 'hidden',
                boxShadow: variable["shadowBlur"],
                position: variable["position"].relative,
            },
            itemSliderPanel: function (imgUrl) { return ({
                width: '100%',
                paddingTop: '62.5%',
                position: variable["position"].relative,
                backgroundImage: "url(" + imgUrl + ")",
                backgroundColor: variable["colorF7"],
                backgroundPosition: 'top center',
                backgroundSize: 'cover',
            }); },
            videoIcon: {
                width: 70,
                height: 70,
                position: variable["position"].absolute,
                top: '50%',
                left: '55%',
                transform: 'translate(-50%, -50%)',
                borderTop: '35px solid transparent',
                boxSizing: 'border-box',
                borderLeft: '51px solid white',
                borderBottom: '35px solid transparent',
                opacity: .8
            },
            info: {
                width: '100%',
                height: 110,
                padding: 12,
                position: variable["position"].relative,
                background: variable["colorWhite"],
                display: variable["display"].flex,
                flexDirection: 'column',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                image: function (imgUrl) { return ({
                    width: '100%',
                    height: '100%',
                    top: 0,
                    left: 0,
                    zIndex: -1,
                    position: variable["position"].absolute,
                    backgroundColor: variable["colorF7"],
                    backgroundImage: "url(" + imgUrl + ")",
                    backgroundPosition: 'bottom center',
                    backgroundSize: 'cover',
                    filter: 'blur(4px)',
                    transform: "scaleY(-1) scale(1.1)",
                    'WebkitBackfaceVisibility': 'hidden',
                    'WebkitPerspective': 1000,
                    'WebkitTransform': ['translate3d(0,0,0)', 'translateZ(0)'],
                    'backfaceVisibility': 'hidden',
                    perspective: 1000,
                }); },
                title: {
                    color: variable["colorBlack"],
                    whiteSpace: 'pre-wrap',
                    fontFamily: variable["fontAvenirMedium"],
                    fontSize: 14,
                    lineHeight: '22px',
                    maxHeight: '44px',
                    overflow: 'hidden',
                    marginBottom: 5
                },
                description: {
                    fontSize: 12,
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap',
                    lineHeight: '18px',
                    maxHeight: 36,
                    overflow: 'hidden',
                },
                tagList: {
                    width: '100%',
                    display: variable["display"].flex,
                    flexWrap: 'wrap',
                    height: '36px',
                    maxHeight: '36px',
                    overflow: 'hidden'
                },
                tagItem: {
                    fontSize: 12,
                    lineHeight: '18px',
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap'
                },
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFNBQVMsTUFBTSwwQkFBMEIsQ0FBQztBQUN0RCxPQUFPLEtBQUssTUFBTSxNQUFNLHVCQUF1QixDQUFDO0FBQ2hELE9BQU8sS0FBSyxRQUFRLE1BQU0seUJBQXlCLENBQUM7QUFDcEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRXpELE1BQU0sQ0FBQyxJQUFNLFlBQVksR0FBRztJQUMxQixvQ0FBb0MsRUFBRTtRQUNwQyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxrQkFBa0I7S0FDOUI7SUFFRCwwQ0FBMEMsRUFBRTtRQUMxQyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxlQUFlO0tBQzNCO0lBRUQsa0NBQWtDLEVBQUU7UUFDbEMsS0FBSyxFQUFFLEVBQUU7UUFDVCxNQUFNLEVBQUUsRUFBRTtRQUNWLE9BQU8sRUFBRSxDQUFDO1FBQ1YsU0FBUyxFQUFFLHdCQUF3QjtRQUNuQyxVQUFVLEVBQUUsUUFBUTtLQUNyQjtJQUVELHdDQUF3QyxFQUFFO1FBQ3hDLE9BQU8sRUFBRSxDQUFDO1FBQ1YsU0FBUyxFQUFFLHNCQUFzQjtRQUNqQyxVQUFVLEVBQUUsU0FBUztLQUN0QjtJQUVELG1DQUFtQyxFQUFFO1FBQ25DLEtBQUssRUFBRSxFQUFFO1FBQ1QsTUFBTSxFQUFFLEVBQUU7UUFDVixPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSx1QkFBdUI7UUFDbEMsVUFBVSxFQUFFLFFBQVE7S0FDckI7SUFFRCx5Q0FBeUMsRUFBRTtRQUN6QyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxzQkFBc0I7UUFDakMsVUFBVSxFQUFFLFNBQVM7S0FDdEI7Q0FDRixDQUFDO0FBRUYsZUFBZTtJQUNiLFNBQVMsRUFBRSxZQUFZLENBQUM7UUFDdEIsTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDO1FBQ1osT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDO1FBQ2IsT0FBTyxFQUFFLENBQUM7Z0JBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztnQkFDL0IsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsTUFBTSxFQUFFLE1BQU07YUFDZixDQUFDO0tBQ0gsQ0FBQztJQUVGLFVBQVUsRUFBRTtRQUNWLFFBQVEsRUFBRSxVQUFVO1FBQ3BCLFFBQVEsRUFBRSxRQUFRO1FBQ2xCLE1BQU0sRUFBRSxNQUFNO1FBRWQsU0FBUyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUN6QixNQUFNLENBQUMsYUFBYSxDQUFDLE9BQU8sRUFDNUIsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQ3ZCO1lBQ0UsVUFBVSxFQUFFLEVBQUU7WUFDZCxVQUFVLEVBQUUsUUFBUSxDQUFDLGlCQUFpQjtTQUN2QyxDQUNGO1FBRUQsVUFBVSxFQUFFO1lBQ1YsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNO1lBQzNCLFNBQVMsQ0FBQyxlQUFlO1lBQ3pCO2dCQUNFLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO2dCQUNyQyxNQUFNLEVBQUUsQ0FBQzthQUNWO1NBQ0Y7UUFFRCxVQUFVLEVBQUU7WUFDVixNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU07WUFDM0IsTUFBTSxDQUFDLGFBQWEsQ0FBQyxjQUFjO1lBQ25DLFNBQVMsQ0FBQyxlQUFlO1NBQzFCO0tBQ0Y7SUFFRCxrQkFBa0IsRUFBRTtRQUNsQixNQUFNLEVBQUUsR0FBRztLQUNaO0lBRUQsVUFBVSxFQUFFO1FBQ1YsS0FBSyxFQUFFLE1BQU07UUFDYixTQUFTLEVBQUUsTUFBTTtRQUVqQixLQUFLLEVBQUU7WUFDTCxVQUFVLEVBQUUsUUFBUTtZQUNwQixhQUFhLEVBQUUsUUFBUTtTQUN4QjtLQUNGO0lBRUQsV0FBVyxFQUFFO1FBQ1gsS0FBSyxFQUFFLE1BQU07UUFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1FBQzlCLFlBQVksRUFBRSxFQUFFO1FBRWhCLElBQUksRUFBRTtZQUNKLElBQUksRUFBRSxDQUFDO1lBQ1AsV0FBVyxFQUFFLEVBQUU7WUFDZixZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELEtBQUssRUFBRTtZQUNMLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLE1BQU07WUFDZCxVQUFVLEVBQUUsS0FBSztZQUNqQixZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELElBQUksRUFBRTtZQUNKLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLEVBQUU7WUFDVixZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELFFBQVEsRUFBRTtZQUNSLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLEVBQUU7U0FDWDtLQUNGO0lBRUQsT0FBTyxFQUFFO1FBQ1AsUUFBUSxFQUFFO1lBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztZQUMvQixNQUFNLEVBQUUsTUFBTTtTQUNmO1FBRUQsU0FBUyxFQUFFO1lBQ1QsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsTUFBTTtZQUNkLFNBQVMsRUFBRSxNQUFNO1lBQ2pCLFVBQVUsRUFBRSxRQUFRO1lBQ3BCLFVBQVUsRUFBRSxDQUFDO1lBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixRQUFRLEVBQUUsUUFBUTtZQUVsQixVQUFVLEVBQUU7Z0JBQ1YsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztnQkFDckMsWUFBWSxFQUFFLENBQUM7Z0JBQ2YsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDOUIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTthQUNyQztZQUVELGVBQWUsRUFBRSxVQUFDLE1BQU0sSUFBSyxPQUFBLENBQUM7Z0JBQzVCLEtBQUssRUFBRSxNQUFNO2dCQUNiLFVBQVUsRUFBRSxPQUFPO2dCQUNuQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxlQUFlLEVBQUUsU0FBTyxNQUFNLE1BQUc7Z0JBQ2pDLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztnQkFDakMsa0JBQWtCLEVBQUUsWUFBWTtnQkFDaEMsY0FBYyxFQUFFLE9BQU87YUFDeEIsQ0FBQyxFQVIyQixDQVEzQjtZQUVGLFNBQVMsRUFBRTtnQkFDVCxLQUFLLEVBQUUsRUFBRTtnQkFDVCxNQUFNLEVBQUUsRUFBRTtnQkFDVixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxHQUFHLEVBQUUsS0FBSztnQkFDVixJQUFJLEVBQUUsS0FBSztnQkFDWCxTQUFTLEVBQUUsdUJBQXVCO2dCQUNsQyxTQUFTLEVBQUUsd0JBQXdCO2dCQUNuQyxTQUFTLEVBQUUsWUFBWTtnQkFDdkIsVUFBVSxFQUFFLGtCQUFrQjtnQkFDOUIsWUFBWSxFQUFFLHdCQUF3QjtnQkFDdEMsT0FBTyxFQUFFLEVBQUU7YUFDWjtZQUVELElBQUksRUFBRTtnQkFDSixLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsR0FBRztnQkFDWCxPQUFPLEVBQUUsRUFBRTtnQkFDWCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVU7Z0JBQy9CLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLGFBQWEsRUFBRSxRQUFRO2dCQUN2QixjQUFjLEVBQUUsWUFBWTtnQkFDNUIsVUFBVSxFQUFFLFlBQVk7Z0JBRXhCLEtBQUssRUFBRSxVQUFDLE1BQU0sSUFBSyxPQUFBLENBQUM7b0JBQ2xCLEtBQUssRUFBRSxNQUFNO29CQUNiLE1BQU0sRUFBRSxNQUFNO29CQUNkLEdBQUcsRUFBRSxDQUFDO29CQUNOLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxDQUFDLENBQUM7b0JBQ1YsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtvQkFDcEMsZUFBZSxFQUFFLFFBQVEsQ0FBQyxPQUFPO29CQUNqQyxlQUFlLEVBQUUsU0FBTyxNQUFNLE1BQUc7b0JBQ2pDLGtCQUFrQixFQUFFLGVBQWU7b0JBQ25DLGNBQWMsRUFBRSxPQUFPO29CQUN2QixNQUFNLEVBQUUsV0FBVztvQkFDbkIsU0FBUyxFQUFFLHVCQUF1QjtvQkFDbEMsMEJBQTBCLEVBQUUsUUFBUTtvQkFDcEMsbUJBQW1CLEVBQUUsSUFBSTtvQkFDekIsaUJBQWlCLEVBQUUsQ0FBQyxvQkFBb0IsRUFBRSxlQUFlLENBQUM7b0JBQzFELG9CQUFvQixFQUFFLFFBQVE7b0JBQzlCLFdBQVcsRUFBRSxJQUFJO2lCQUNsQixDQUFDLEVBbEJpQixDQWtCakI7Z0JBRUYsS0FBSyxFQUFFO29CQUNMLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDMUIsVUFBVSxFQUFFLFVBQVU7b0JBQ3RCLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO29CQUNyQyxRQUFRLEVBQUUsRUFBRTtvQkFDWixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsU0FBUyxFQUFFLE1BQU07b0JBQ2pCLFFBQVEsRUFBRSxRQUFRO29CQUNsQixZQUFZLEVBQUUsQ0FBQztpQkFDaEI7Z0JBRUQsV0FBVyxFQUFFO29CQUNYLFFBQVEsRUFBRSxFQUFFO29CQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7b0JBQ3JDLE9BQU8sRUFBRSxFQUFFO29CQUNYLFdBQVcsRUFBRSxFQUFFO29CQUNmLFVBQVUsRUFBRSxVQUFVO29CQUN0QixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsU0FBUyxFQUFFLEVBQUU7b0JBQ2IsUUFBUSxFQUFFLFFBQVE7aUJBQ25CO2dCQUVELE9BQU8sRUFBRTtvQkFDUCxLQUFLLEVBQUUsTUFBTTtvQkFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO29CQUM5QixRQUFRLEVBQUUsTUFBTTtvQkFDaEIsTUFBTSxFQUFFLE1BQU07b0JBQ2QsU0FBUyxFQUFFLE1BQU07b0JBQ2pCLFFBQVEsRUFBRSxRQUFRO2lCQUNuQjtnQkFFRCxPQUFPLEVBQUU7b0JBQ1AsUUFBUSxFQUFFLEVBQUU7b0JBQ1osVUFBVSxFQUFFLE1BQU07b0JBQ2xCLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7b0JBQ3JDLE9BQU8sRUFBRSxFQUFFO29CQUNYLFdBQVcsRUFBRSxFQUFFO29CQUNmLFVBQVUsRUFBRSxVQUFVO2lCQUN2QjthQUNGO1NBQ0Y7S0FDRjtDQUNxQixDQUFDIn0=
// CONCATENATED MODULE: ./components/container/image-slider-community/view-desktop.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};






var renderSlider = function (_a) {
    var column = _a.column, handleOmitImgHeight = _a.handleOmitImgHeight, imageSlideSelected = _a.imageSlideSelected;
    return (react["createElement"]("community-image-slider", { style: image_slider_community_style.desktop.mainWrap },
        react["createElement"]("div", { style: image_slider_community_style.desktop.container }, imageSlideSelected
            && Array.isArray(imageSlideSelected.list)
            && imageSlideSelected.list.map(function (item, index) {
                var slideProps = {
                    item: item,
                    column: column,
                    handleOmitImgHeight: handleOmitImgHeight
                };
                return react["createElement"](image_slider_item_community, __assign({ key: "slider-item-" + index }, slideProps));
            }))));
};
var renderNavigation = function (_a) {
    var imageSlide = _a.imageSlide, navLeftSlide = _a.navLeftSlide, navRightSlide = _a.navRightSlide;
    var leftNavProps = {
        className: 'left-nav',
        onClick: navLeftSlide,
        style: [image_slider_community_style.imageSlide.navigation, component["e" /* slideNavigation */]['left'], { top: '50%', transform: 'translateY(-50%)' }]
    };
    var rightNavProps = {
        className: 'right-nav',
        onClick: navRightSlide,
        style: [image_slider_community_style.imageSlide.navigation, component["e" /* slideNavigation */]['right'], { top: '50%', transform: 'translateY(-50%)' }]
    };
    return imageSlide.length <= 1
        ? null
        : (react["createElement"]("div", null,
            react["createElement"]("div", __assign({}, leftNavProps),
                react["createElement"](icon["a" /* default */], { name: 'angle-left', style: component["e" /* slideNavigation */].icon })),
            react["createElement"]("div", __assign({}, rightNavProps),
                react["createElement"](icon["a" /* default */], { name: 'angle-right', style: component["e" /* slideNavigation */].icon }))));
};
var renderPagination = function (_a) {
    var imageSlide = _a.imageSlide, selectSlide = _a.selectSlide, countChangeSlide = _a.countChangeSlide;
    var generateItemProps = function (item, index) { return ({
        key: "product-slider-" + item.id,
        onClick: function () { return selectSlide(index); },
        style: [
            component["f" /* slidePagination */].item,
            index === countChangeSlide && component["f" /* slidePagination */].itemActive
        ]
    }); };
    return imageSlide.length <= 1
        ? null
        : (react["createElement"]("div", { style: image_slider_community_style.imageSlide.pagination, className: 'pagination' }, Array.isArray(imageSlide)
            && imageSlide.map(function (item, $index) {
                var itemProps = generateItemProps(item, $index);
                return react["createElement"]("div", __assign({}, itemProps));
            })));
};
var renderDesktop = function (_a) {
    var props = _a.props, state = _a.state, handleMouseHover = _a.handleMouseHover, selectSlide = _a.selectSlide, navLeftSlide = _a.navLeftSlide, navRightSlide = _a.navRightSlide;
    var _b = state, imageSlide = _b.imageSlide, imageSlideSelected = _b.imageSlideSelected, countChangeSlide = _b.countChangeSlide;
    var _c = props, column = _c.column, handleOmitImgHeight = _c.handleOmitImgHeight;
    var containerProps = {
        style: image_slider_community_style.imageSlide,
        onMouseEnter: handleMouseHover
    };
    return (react["createElement"]("div", __assign({}, containerProps, { className: 'image-slide-container' }),
        renderSlider({ column: column, handleOmitImgHeight: handleOmitImgHeight, imageSlideSelected: imageSlideSelected }),
        renderNavigation({ imageSlide: imageSlide, navLeftSlide: navLeftSlide, navRightSlide: navRightSlide }),
        react["createElement"](radium["Style"], { rules: INLINE_STYLE })));
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1kZXNrdG9wLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1kZXNrdG9wLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFL0IsT0FBTyxJQUFJLE1BQU0sNkJBQTZCLENBQUM7QUFDL0MsT0FBTyxLQUFLLFNBQVMsTUFBTSwwQkFBMEIsQ0FBQztBQUN0RCxPQUFPLGVBQWUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUU3RCxPQUFPLEtBQUssRUFBRSxFQUFFLFlBQVksRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUc5QyxJQUFNLFlBQVksR0FBRyxVQUFDLEVBQW1EO1FBQWpELGtCQUFNLEVBQUUsNENBQW1CLEVBQUUsMENBQWtCO0lBRXJFLE1BQU0sQ0FBQyxDQUNMLGdEQUF3QixLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRO1FBQ25ELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsSUFFL0Isa0JBQWtCO2VBQ2YsS0FBSyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUM7ZUFDdEMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLO2dCQUN6QyxJQUFNLFVBQVUsR0FBRztvQkFDakIsSUFBSSxNQUFBO29CQUNKLE1BQU0sUUFBQTtvQkFDTixtQkFBbUIscUJBQUE7aUJBQ3BCLENBQUE7Z0JBRUQsTUFBTSxDQUFDLG9CQUFDLGVBQWUsYUFBQyxHQUFHLEVBQUUsaUJBQWUsS0FBTyxJQUFNLFVBQVUsRUFBSSxDQUFBO1lBQ3pFLENBQUMsQ0FBQyxDQUVBLENBQ2lCLENBQzFCLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixJQUFNLGdCQUFnQixHQUFHLFVBQUMsRUFBMkM7UUFBekMsMEJBQVUsRUFBRSw4QkFBWSxFQUFFLGdDQUFhO0lBQ2pFLElBQU0sWUFBWSxHQUFHO1FBQ25CLFNBQVMsRUFBRSxVQUFVO1FBQ3JCLE9BQU8sRUFBRSxZQUFZO1FBQ3JCLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLFNBQVMsQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxrQkFBa0IsRUFBRSxDQUFDO0tBQ3ZILENBQUM7SUFFRixJQUFNLGFBQWEsR0FBRztRQUNwQixTQUFTLEVBQUUsV0FBVztRQUN0QixPQUFPLEVBQUUsYUFBYTtRQUN0QixLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxFQUFFLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsa0JBQWtCLEVBQUUsQ0FBQztLQUN4SCxDQUFDO0lBRUYsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLElBQUksQ0FBQztRQUMzQixDQUFDLENBQUMsSUFBSTtRQUNOLENBQUMsQ0FBQyxDQUNBO1lBQ0Usd0NBQVMsWUFBWTtnQkFDbkIsb0JBQUMsSUFBSSxJQUFDLElBQUksRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxlQUFlLENBQUMsSUFBSSxHQUFJLENBQy9EO1lBQ04sd0NBQVMsYUFBYTtnQkFDcEIsb0JBQUMsSUFBSSxJQUFDLElBQUksRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxlQUFlLENBQUMsSUFBSSxHQUFJLENBQ2hFLENBQ0YsQ0FDUCxDQUFDO0FBQ04sQ0FBQyxDQUFDO0FBRUYsSUFBTSxnQkFBZ0IsR0FBRyxVQUFDLEVBQTZDO1FBQTNDLDBCQUFVLEVBQUUsNEJBQVcsRUFBRSxzQ0FBZ0I7SUFDbkUsSUFBTSxpQkFBaUIsR0FBRyxVQUFDLElBQUksRUFBRSxLQUFLLElBQUssT0FBQSxDQUFDO1FBQzFDLEdBQUcsRUFBRSxvQkFBa0IsSUFBSSxDQUFDLEVBQUk7UUFDaEMsT0FBTyxFQUFFLGNBQU0sT0FBQSxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQWxCLENBQWtCO1FBQ2pDLEtBQUssRUFBRTtZQUNMLFNBQVMsQ0FBQyxlQUFlLENBQUMsSUFBSTtZQUM5QixLQUFLLEtBQUssZ0JBQWdCLElBQUksU0FBUyxDQUFDLGVBQWUsQ0FBQyxVQUFVO1NBQ25FO0tBQ0YsQ0FBQyxFQVB5QyxDQU96QyxDQUFDO0lBRUgsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLElBQUksQ0FBQztRQUMzQixDQUFDLENBQUMsSUFBSTtRQUNOLENBQUMsQ0FBQyxDQUNBLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRSxTQUFTLEVBQUUsWUFBWSxJQUU1RCxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQztlQUN0QixVQUFVLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBSSxFQUFFLE1BQU07Z0JBQzdCLElBQU0sU0FBUyxHQUFHLGlCQUFpQixDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztnQkFDbEQsTUFBTSxDQUFDLHdDQUFTLFNBQVMsRUFBUSxDQUFDO1lBQ3BDLENBQUMsQ0FBQyxDQUVBLENBQ1AsQ0FBQztBQUNOLENBQUMsQ0FBQztBQUVGLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRyxVQUFDLEVBQTRFO1FBQTFFLGdCQUFLLEVBQUUsZ0JBQUssRUFBRSxzQ0FBZ0IsRUFBRSw0QkFBVyxFQUFFLDhCQUFZLEVBQUUsZ0NBQWE7SUFDaEcsSUFBQSxVQUFzRSxFQUFwRSwwQkFBVSxFQUFFLDBDQUFrQixFQUFFLHNDQUFnQixDQUFxQjtJQUN2RSxJQUFBLFVBQWlELEVBQS9DLGtCQUFNLEVBQUUsNENBQW1CLENBQXFCO0lBRXhELElBQU0sY0FBYyxHQUFHO1FBQ3JCLEtBQUssRUFBRSxLQUFLLENBQUMsVUFBVTtRQUN2QixZQUFZLEVBQUUsZ0JBQWdCO0tBQy9CLENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCx3Q0FBUyxjQUFjLElBQUUsU0FBUyxFQUFFLHVCQUF1QjtRQUN4RCxZQUFZLENBQUMsRUFBRSxNQUFNLFFBQUEsRUFBRSxtQkFBbUIscUJBQUEsRUFBRSxrQkFBa0Isb0JBQUEsRUFBRSxDQUFDO1FBRWpFLGdCQUFnQixDQUFDLEVBQUUsVUFBVSxZQUFBLEVBQUUsWUFBWSxjQUFBLEVBQUUsYUFBYSxlQUFBLEVBQUUsQ0FBQztRQUM5RCxvQkFBQyxLQUFLLElBQUMsS0FBSyxFQUFFLFlBQVksR0FBSSxDQUMxQixDQUNQLENBQUM7QUFDSixDQUFDLENBQUMifQ==
// CONCATENATED MODULE: ./components/container/image-slider-community/view-mobile.tsx




var renderMobile = function (_a) {
    var imageList = _a.imageList, column = _a.column, handleOmitImgHeight = _a.handleOmitImgHeight;
    return (react["createElement"]("div", { style: image_slider_community_style.mobileWrap },
        react["createElement"]("div", { style: [layout["a" /* flexContainer */].noWrap, image_slider_community_style.mobileWrap.panel] }, Array.isArray(imageList)
            && imageList.map(function (item, index) { return react["createElement"](image_slider_item_community, { key: "image-slider-item-" + index, item: item, column: column, handleOmitImgHeight: handleOmitImgHeight }); }))));
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1tb2JpbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LW1vYmlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxlQUFlLE1BQU0sZ0NBQWdDLENBQUM7QUFFN0QsT0FBTyxLQUFLLE1BQU0sTUFBTSx1QkFBdUIsQ0FBQztBQUNoRCxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsTUFBTSxDQUFDLElBQU0sWUFBWSxHQUFHLFVBQUMsRUFBMEM7UUFBeEMsd0JBQVMsRUFBRSxrQkFBTSxFQUFFLDRDQUFtQjtJQUVuRSxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFVBQVU7UUFDMUIsNkJBQUssS0FBSyxFQUFFLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFFN0QsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUM7ZUFDckIsU0FBUyxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLLElBQUssT0FBQSxvQkFBQyxlQUFlLElBQUMsR0FBRyxFQUFFLHVCQUFxQixLQUFPLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLG1CQUFtQixFQUFFLG1CQUFtQixHQUFJLEVBQTVILENBQTRILENBQUMsQ0FFN0osQ0FDRixDQUNQLENBQUM7QUFDSixDQUFDLENBQUMifQ==
// CONCATENATED MODULE: ./components/container/image-slider-community/view.tsx




var renderView = function (_a) {
    var props = _a.props, state = _a.state, handleMouseHover = _a.handleMouseHover, selectSlide = _a.selectSlide, navLeftSlide = _a.navLeftSlide, navRightSlide = _a.navRightSlide;
    var imageList = state.imageList;
    var _b = props, column = _b.column, handleOmitImgHeight = _b.handleOmitImgHeight;
    var renderMobileProps = { imageList: imageList, column: column, handleOmitImgHeight: handleOmitImgHeight };
    var renderDesktopProps = {
        props: props,
        state: state,
        selectSlide: selectSlide,
        navLeftSlide: navLeftSlide,
        navRightSlide: navRightSlide,
        handleMouseHover: handleMouseHover
    };
    var switchStyle = {
        MOBILE: function () { return renderMobile(renderMobileProps); },
        DESKTOP: function () { return renderDesktop(renderDesktopProps); }
    };
    return (react["createElement"]("div", { style: image_slider_community_style.container }, switchStyle[window.DEVICE_VERSION]()));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRS9CLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMvQyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTdDLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixJQUFNLFVBQVUsR0FBRyxVQUFDLEVBQTRFO1FBQTFFLGdCQUFLLEVBQUUsZ0JBQUssRUFBRSxzQ0FBZ0IsRUFBRSw0QkFBVyxFQUFFLDhCQUFZLEVBQUUsZ0NBQWE7SUFDcEYsSUFBQSwyQkFBUyxDQUFxQjtJQUNoQyxJQUFBLFVBQWlELEVBQS9DLGtCQUFNLEVBQUUsNENBQW1CLENBQXFCO0lBRXhELElBQU0saUJBQWlCLEdBQUcsRUFBRSxTQUFTLFdBQUEsRUFBRSxNQUFNLFFBQUEsRUFBRSxtQkFBbUIscUJBQUEsRUFBRSxDQUFDO0lBQ3JFLElBQU0sa0JBQWtCLEdBQUc7UUFDekIsS0FBSyxPQUFBO1FBQ0wsS0FBSyxPQUFBO1FBQ0wsV0FBVyxhQUFBO1FBQ1gsWUFBWSxjQUFBO1FBQ1osYUFBYSxlQUFBO1FBQ2IsZ0JBQWdCLGtCQUFBO0tBQ2pCLENBQUM7SUFFRixJQUFNLFdBQVcsR0FBRztRQUNsQixNQUFNLEVBQUUsY0FBTSxPQUFBLFlBQVksQ0FBQyxpQkFBaUIsQ0FBQyxFQUEvQixDQUErQjtRQUM3QyxPQUFPLEVBQUUsY0FBTSxPQUFBLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFqQyxDQUFpQztLQUNqRCxDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLElBQ3hCLFdBQVcsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FDakMsQ0FDUCxDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// CONCATENATED MODULE: ./components/container/image-slider-community/component.tsx
var component_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var component_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var component_ImageSlide = /** @class */ (function (_super) {
    component_extends(ImageSlide, _super);
    function ImageSlide(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE(props.data);
        return _this;
    }
    ImageSlide.prototype.componentDidMount = function () {
        this.initDataSlide(this.state.imageList, this.props.posImg);
        document.addEventListener('keydown', this.handleKeydown.bind(this), false);
    };
    ImageSlide.prototype.componentWillUnmount = function () {
        document.removeEventListener('keydown', this.handleKeydown.bind(this), false);
    };
    /**
     * When component will receive new props -> init data for slide
     * @param nextProps prop from parent
     */
    ImageSlide.prototype.componentWillReceiveProps = function (nextProps) {
        this.initDataSlide(nextProps.data, nextProps.posImg);
    };
    /**
     * Init data for slide
     * When : Component will mount OR Component will receive new props
     */
    ImageSlide.prototype.initDataSlide = function (_newImageList, posImg) {
        var _this = this;
        if (_newImageList === void 0) { _newImageList = this.state.imageList; }
        if (posImg === void 0) { posImg = 0; }
        if (true === Object(responsive["b" /* isDesktopVersion */])()) {
            if (false === this.state.firstInit) {
                this.setState({ firstInit: true });
            }
            /**
             * On DESKTOP
             * Init data for image slide & image slide selected
             */
            var _imageSlide_1 = [];
            var groupImage_1 = {
                id: 0,
                list: []
            };
            /** Assign data into each slider */
            if (_newImageList.length > (this.props.column || 3)) {
                Array.isArray(_newImageList)
                    && _newImageList.map(function (image) {
                        groupImage_1.id = _imageSlide_1.length;
                        groupImage_1.list.push(image);
                        if (groupImage_1.list.length === _this.props.column) {
                            _imageSlide_1.push(Object.assign({}, groupImage_1));
                            groupImage_1.list = [];
                        }
                    });
            }
            else {
                _imageSlide_1 = [{ id: 0, list: _newImageList }];
            }
            this.setState({
                imageList: _newImageList,
                imageSlide: _imageSlide_1,
                imageSlideSelected: _imageSlide_1[0] || {}
            });
            posImg !== 0 && this.selectSlide(posImg);
        }
        else {
            /**
             * On Mobile
             * Only init data for list image, not apply slide animation
             */
            this.setState({ imageList: _newImageList });
        }
    };
    /**
     * Navigate slide by button left or right
     * @param _direction `LEFT` or `RIGHT`
     * Will set new index value by @param _direction
     */
    ImageSlide.prototype.navSlide = function (_direction) {
        var _a = this.state, imageSlide = _a.imageSlide, countChangeSlide = _a.countChangeSlide;
        /**
         * If navigate to right: increase index value -> set +1 by countChangeSlide
         * If vavigate to left: decrease index value -> set -1 by countChangeSlide
         */
        var newIndexValue = 'left' === _direction ? -1 : 1;
        newIndexValue += countChangeSlide;
        /**
         * Validate new value in range [0, imageSlide.length - 1]
         */
        newIndexValue = newIndexValue === imageSlide.length
            ? 0 /** If over max value -> set 0 */
            : (newIndexValue === -1
                /** If under min value -> set imageSlide.length - 1 */
                ? imageSlide.length - 1
                : newIndexValue);
        /** Change to new index value */
        this.selectSlide(newIndexValue);
    };
    ImageSlide.prototype.navLeftSlide = function () {
        this.navSlide('left');
    };
    ImageSlide.prototype.navRightSlide = function () {
        this.navSlide('right');
    };
    ImageSlide.prototype.handleKeydown = function (event) {
        if (event.keyCode === 37) {
            this.navLeftSlide();
        }
        else if (event.keyCode === 39) {
            this.navRightSlide();
        }
    };
    /**
     * Change slide by set state and setTimeout for animation
     * @param _index new index value
     */
    ImageSlide.prototype.selectSlide = function (_index) {
        var _this = this;
        /** Change background */
        setTimeout(function () { return _this.setState(function (prevState, props) { return ({
            countChangeSlide: _index,
            imageSlideSelected: prevState.imageSlide[_index],
        }); }); }, 10);
    };
    ImageSlide.prototype.handleMouseHover = function () {
        var _a = this.props, _b = _a.data, data = _b === void 0 ? [] : _b, _c = _a.column, column = _c === void 0 ? 1 : _c;
        var preLoadImageList = Array.isArray(data)
            ? data
                .filter(function (item, $index) { return $index >= column; })
                .map(function (item) { return item.original_url; })
            : [];
        Object(utils_image["b" /* preLoadImage */])(preLoadImageList);
    };
    // shouldComponentUpdate(nextProps: IProps, nextState: IState) {
    //   // if (this.props.data.length !== nextProps.data.length) { return true; }
    //   // if (this.state.countChangeSlide !== nextState.countChangeSlide) { return true; }
    //   // if (this.props.data.length > 0 && this.state.firstInit !== nextState.firstInit) { return true; }
    //   return true;
    // }
    ImageSlide.prototype.render = function () {
        var _this = this;
        var renderViewProps = {
            props: this.props,
            state: this.state,
            handleMouseHover: this.handleMouseHover.bind(this),
            selectSlide: function (index) { return _this.selectSlide(index); },
            navLeftSlide: this.navLeftSlide.bind(this),
            navRightSlide: this.navRightSlide.bind(this)
        };
        return view(renderViewProps);
    };
    ImageSlide.defaultProps = DEFAULT_PROPS;
    ImageSlide = component_decorate([
        radium
    ], ImageSlide);
    return ImageSlide;
}(react["Component"]));
;
/* harmony default export */ var image_slider_community_component = (component_ImageSlide);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRzdELE9BQU8sRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQzVELE9BQU8sVUFBVSxNQUFNLFFBQVEsQ0FBQztBQUdoQztJQUF5Qiw4QkFBK0I7SUFHdEQsb0JBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDOztJQUN6QyxDQUFDO0lBRUQsc0NBQWlCLEdBQWpCO1FBQ0UsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzVELFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDN0UsQ0FBQztJQUVELHlDQUFvQixHQUFwQjtRQUNFLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDaEYsQ0FBQztJQUVEOzs7T0FHRztJQUNILDhDQUF5QixHQUF6QixVQUEwQixTQUFTO1FBQ2pDLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVEOzs7T0FHRztJQUNILGtDQUFhLEdBQWIsVUFBYyxhQUFnRCxFQUFFLE1BQVU7UUFBMUUsaUJBNENDO1FBNUNhLDhCQUFBLEVBQUEsZ0JBQTRCLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUztRQUFFLHVCQUFBLEVBQUEsVUFBVTtRQUN4RSxFQUFFLENBQUMsQ0FBQyxJQUFJLEtBQUssZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFFaEMsRUFBRSxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBWSxDQUFDLENBQUM7WUFBQyxDQUFDO1lBQ3JGOzs7ZUFHRztZQUNILElBQUksYUFBVyxHQUFlLEVBQUUsQ0FBQztZQUNqQyxJQUFJLFlBQVUsR0FBc0M7Z0JBQ2xELEVBQUUsRUFBRSxDQUFDO2dCQUNMLElBQUksRUFBRSxFQUFFO2FBQ1QsQ0FBQztZQUVGLG1DQUFtQztZQUNuQyxFQUFFLENBQUMsQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNwRCxLQUFLLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQzt1QkFDdkIsYUFBYSxDQUFDLEdBQUcsQ0FBQyxVQUFDLEtBQUs7d0JBQ3pCLFlBQVUsQ0FBQyxFQUFFLEdBQUcsYUFBVyxDQUFDLE1BQU0sQ0FBQzt3QkFDbkMsWUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBRTVCLEVBQUUsQ0FBQyxDQUFDLFlBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxLQUFLLEtBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzs0QkFDakQsYUFBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxZQUFVLENBQUMsQ0FBQyxDQUFDOzRCQUNoRCxZQUFVLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQzt3QkFDdkIsQ0FBQztvQkFDSCxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixhQUFXLEdBQUcsQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7WUFDakQsQ0FBQztZQUVELElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ1osU0FBUyxFQUFFLGFBQWE7Z0JBQ3hCLFVBQVUsRUFBRSxhQUFXO2dCQUN2QixrQkFBa0IsRUFBRSxhQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRTthQUMvQixDQUFDLENBQUM7WUFFYixNQUFNLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDM0MsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ047OztlQUdHO1lBQ0gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQVksQ0FBQyxDQUFDO1FBQ3hELENBQUM7SUFDSCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILDZCQUFRLEdBQVIsVUFBUyxVQUFVO1FBQ1gsSUFBQSxlQUE2QyxFQUEzQywwQkFBVSxFQUFFLHNDQUFnQixDQUFnQjtRQUVwRDs7O1dBR0c7UUFDSCxJQUFJLGFBQWEsR0FBRyxNQUFNLEtBQUssVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25ELGFBQWEsSUFBSSxnQkFBZ0IsQ0FBQztRQUVsQzs7V0FFRztRQUNILGFBQWEsR0FBRyxhQUFhLEtBQUssVUFBVSxDQUFDLE1BQU07WUFDakQsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQ0FBaUM7WUFDckMsQ0FBQyxDQUFDLENBQ0EsYUFBYSxLQUFLLENBQUMsQ0FBQztnQkFDbEIsc0RBQXNEO2dCQUN0RCxDQUFDLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDO2dCQUN2QixDQUFDLENBQUMsYUFBYSxDQUNsQixDQUFDO1FBRUosZ0NBQWdDO1FBQ2hDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUVELGlDQUFZLEdBQVo7UUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3hCLENBQUM7SUFFRCxrQ0FBYSxHQUFiO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBRUQsa0NBQWEsR0FBYixVQUFjLEtBQUs7UUFDakIsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQTtRQUNyQixDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNoQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUE7UUFDdEIsQ0FBQztJQUNILENBQUM7SUFFRDs7O09BR0c7SUFDSCxnQ0FBVyxHQUFYLFVBQVksTUFBTTtRQUFsQixpQkFPQztRQUxDLHdCQUF3QjtRQUN4QixVQUFVLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxRQUFRLENBQUMsVUFBQyxTQUFTLEVBQUUsS0FBSyxJQUFLLE9BQUEsQ0FBQztZQUNwRCxnQkFBZ0IsRUFBRSxNQUFNO1lBQ3hCLGtCQUFrQixFQUFFLFNBQVMsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDO1NBQ3RDLENBQUEsRUFIeUMsQ0FHekMsQ0FBQyxFQUhJLENBR0osRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNyQixDQUFDO0lBRUQscUNBQWdCLEdBQWhCO1FBQ1EsSUFBQSxlQUFzQyxFQUFwQyxZQUFTLEVBQVQsOEJBQVMsRUFBRSxjQUFVLEVBQVYsK0JBQVUsQ0FBZ0I7UUFFN0MsSUFBTSxnQkFBZ0IsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztZQUMxQyxDQUFDLENBQUMsSUFBSTtpQkFDSCxNQUFNLENBQUMsVUFBQyxJQUFJLEVBQUUsTUFBTSxJQUFLLE9BQUEsTUFBTSxJQUFJLE1BQU0sRUFBaEIsQ0FBZ0IsQ0FBQztpQkFDMUMsR0FBRyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFlBQVksRUFBakIsQ0FBaUIsQ0FBQztZQUNqQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBRVAsWUFBWSxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVELGdFQUFnRTtJQUNoRSw4RUFBOEU7SUFDOUUsd0ZBQXdGO0lBQ3hGLHdHQUF3RztJQUV4RyxpQkFBaUI7SUFDakIsSUFBSTtJQUVKLDJCQUFNLEdBQU47UUFBQSxpQkFXQztRQVZDLElBQU0sZUFBZSxHQUFHO1lBQ3RCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDbEQsV0FBVyxFQUFFLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBdkIsQ0FBdUI7WUFDN0MsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUMxQyxhQUFhLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQzdDLENBQUM7UUFFRixNQUFNLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFyS00sdUJBQVksR0FBVyxhQUFhLENBQUM7SUFEeEMsVUFBVTtRQURmLE1BQU07T0FDRCxVQUFVLENBdUtmO0lBQUQsaUJBQUM7Q0FBQSxBQXZLRCxDQUF5QixLQUFLLENBQUMsU0FBUyxHQXVLdkM7QUFBQSxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// CONCATENATED MODULE: ./components/container/image-slider-community/index.tsx

/* harmony default export */ var image_slider_community = __webpack_exports__["a"] = (image_slider_community_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxVQUFVLE1BQU0sYUFBYSxDQUFDO0FBQ3JDLGVBQWUsVUFBVSxDQUFDIn0=

/***/ }),

/***/ 784:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./action/magazine.ts + 1 modules
var magazine = __webpack_require__(761);

// EXTERNAL MODULE: ./action/shop.ts + 1 modules
var shop = __webpack_require__(760);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/encode.ts
var encode = __webpack_require__(7);

// EXTERNAL MODULE: ./constants/application/magazine.ts
var application_magazine = __webpack_require__(170);

// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// CONCATENATED MODULE: ./components/community/right-bar-community/initialize.tsx
var DEFAULT_PROPS = {
    days: 7,
    hashtagSelected: ''
};
var INITIAL_STATE = {};
var tagList = [
    'Beauty Tips',
    'Lip Stick',
    'Hair',
    'Skin Care',
    'Body Bath',
    'ACNE',
    'New Brand',
    'Lixibox Flagship Store'
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsQ0FBQztJQUNQLGVBQWUsRUFBRSxFQUFFO0NBQ1YsQ0FBQztBQUVaLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRyxFQUFZLENBQUM7QUFFMUMsTUFBTSxDQUFDLElBQU0sT0FBTyxHQUFHO0lBQ3JCLGFBQWE7SUFDYixXQUFXO0lBQ1gsTUFBTTtJQUNOLFdBQVc7SUFDWCxXQUFXO0lBQ1gsTUFBTTtJQUNOLFdBQVc7SUFDWCx3QkFBd0I7Q0FDekIsQ0FBQSJ9
// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./components/product/slider/index.tsx + 6 modules
var slider = __webpack_require__(771);

// EXTERNAL MODULE: ./components/magazine/image-slider/index.tsx + 11 modules
var image_slider = __webpack_require__(766);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/community/right-bar-community/style.tsx

/* harmony default export */ var right_bar_community_style = ({
    wrapLayout: {
        paddingTop: 10
    },
    header: {
        fontSize: 20,
        paddingBottom: 20,
        fontFamily: variable["fontTrirong"],
        marginBottom: 10
    },
    headerNoBorder: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        borderBottom: 'none'
    },
    text: {
        fontSize: 16,
        marginBottom: 10,
        color: variable["colorBlack08"]
    },
    wrapElement: {
        boxShadow: variable["shadowBlur"],
        borderRadius: 5,
        backgroundColor: variable["colorWhite"],
        marginBottom: 20
    },
    hashtagGroup: {
        borderRadius: 5,
        backgroundColor: variable["colorWhite"],
        marginBottom: 20
    },
    wrapTag: {
        display: variable["display"].flex,
        flexWrap: 'wrap',
        tag: {
            backgroundColor: variable["colorWhite"],
            color: variable["colorBlack"],
            border: "1px solid " + variable["colorBlack"],
            marginRight: 10,
            marginBottom: 10,
            padding: 10,
            borderRadius: 3,
            height: 30,
            lineHeight: '10px',
            active: {
                color: variable["colorWhite"],
                backgroundColor: variable["colorBlack"],
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxlQUFlO0lBQ2IsVUFBVSxFQUFFO1FBQ1YsVUFBVSxFQUFFLEVBQUU7S0FDZjtJQUVELE1BQU0sRUFBRTtRQUNOLFFBQVEsRUFBRSxFQUFFO1FBQ1osYUFBYSxFQUFFLEVBQUU7UUFDakIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxXQUFXO1FBQ2hDLFlBQVksRUFBRSxFQUFFO0tBQ2pCO0lBRUQsY0FBYyxFQUFFO1FBQ2QsVUFBVSxFQUFFLENBQUM7UUFDYixZQUFZLEVBQUUsQ0FBQztRQUNmLGFBQWEsRUFBRSxDQUFDO1FBQ2hCLFdBQVcsRUFBRSxDQUFDO1FBQ2QsWUFBWSxFQUFFLE1BQU07S0FDckI7SUFFRCxJQUFJLEVBQUU7UUFDSixRQUFRLEVBQUUsRUFBRTtRQUNaLFlBQVksRUFBRSxFQUFFO1FBQ2hCLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTtLQUM3QjtJQUVELFdBQVcsRUFBRTtRQUNYLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTtRQUM5QixZQUFZLEVBQUUsQ0FBQztRQUNmLGVBQWUsRUFBRSxRQUFRLENBQUMsVUFBVTtRQUNwQyxZQUFZLEVBQUUsRUFBRTtLQUNqQjtJQUVELFlBQVksRUFBRTtRQUNaLFlBQVksRUFBRSxDQUFDO1FBQ2YsZUFBZSxFQUFFLFFBQVEsQ0FBQyxVQUFVO1FBQ3BDLFlBQVksRUFBRSxFQUFFO0tBQ2pCO0lBRUQsT0FBTyxFQUFFO1FBQ1AsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUM5QixRQUFRLEVBQUUsTUFBTTtRQUVoQixHQUFHLEVBQUU7WUFDSCxlQUFlLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDcEMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1lBQzFCLE1BQU0sRUFBRSxlQUFhLFFBQVEsQ0FBQyxVQUFZO1lBQzFDLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLEVBQUU7WUFDaEIsT0FBTyxFQUFFLEVBQUU7WUFDWCxZQUFZLEVBQUUsQ0FBQztZQUNmLE1BQU0sRUFBRSxFQUFFO1lBQ1YsVUFBVSxFQUFFLE1BQU07WUFFbEIsTUFBTSxFQUFFO2dCQUNOLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDMUIsZUFBZSxFQUFFLFFBQVEsQ0FBQyxVQUFVO2FBQ3JDO1NBQ0Y7S0FDRjtDQUNNLENBQUMifQ==
// CONCATENATED MODULE: ./components/community/right-bar-community/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};









var renderHeader = function (name, style) {
    if (style === void 0) { style = {}; }
    return react["createElement"]("div", { style: [right_bar_community_style.header, style] }, name);
};
var renderText = function (text, lastChild) {
    if (lastChild === void 0) { lastChild = {}; }
    return react["createElement"]("div", { style: [right_bar_community_style.text, lastChild] }, text);
};
var renderTags = function (_a) {
    var hashtagList = _a.hashtagList, hashtagSelected = _a.hashtagSelected;
    return (react["createElement"]("div", { style: right_bar_community_style.wrapTag }, Array.isArray(hashtagList)
        && hashtagList.length !== 0
        && hashtagList.map(handleRenderTags, { hashtagSelected: hashtagSelected })));
};
function handleRenderTags(item, index) {
    var tagName = item && item.name || '';
    var linkProps = {
        to: routing["t" /* ROUTING_COMMUNITY_TAG_PATH */] + "/" + tagName,
        key: "tag-item-" + index,
        style: Object.assign({}, right_bar_community_style.wrapTag.tag, this.hashtagSelected === tagName && right_bar_community_style.wrapTag.tag.active)
    };
    return react["createElement"](react_router_dom["NavLink"], __assign({}, linkProps), "# " + tagName);
}
var renderTrendingTags = function (_a) {
    var hashtagList = _a.hashtagList, hashtagSelected = _a.hashtagSelected;
    return (hashtagList
        && hashtagList.length !== 0
        && (react["createElement"]("div", { style: right_bar_community_style.hashtagGroup },
            renderHeader('Trending Tags', { paddingBottom: 10 }),
            renderTags({ hashtagList: hashtagList, hashtagSelected: hashtagSelected }))));
};
var renderSuggestionFromExperts = function (list) {
    var lastestProductProps = {
        showHeader: false,
        column: 1,
        data: list && list.slice(0, 5) || [],
        showViewMore: false,
    };
    return (list
        && 0 !== list.length
        &&
            react["createElement"]("div", null,
                renderHeader('Hot Boxes', right_bar_community_style.headerNoBorder),
                react["createElement"]("div", { style: right_bar_community_style.wrapElement },
                    react["createElement"](slider["a" /* default */], __assign({}, lastestProductProps)))));
};
var renderHotMagazine = function (data) {
    var magazineImageProps = {
        title: '',
        column: 1,
        showHeader: false,
        showViewMore: false,
        data: data && !!data.length && data.slice(0, 5) || [],
        style: { paddingBottom: 0 }
    };
    return (data && !!data.length
        &&
            react["createElement"]("div", null,
                renderHeader('Hot magazine', right_bar_community_style.headerNoBorder),
                react["createElement"]("div", { style: right_bar_community_style.wrapElement },
                    react["createElement"](image_slider["a" /* default */], __assign({}, magazineImageProps)))));
};
function renderComponent(_a) {
    var props = _a.props;
    var _b = props, days = _b.days, hashtagSelected = _b.hashtagSelected, dataHomePage = _b.shopStore.dataHomePage, hashtags = _b.communityStore.hashtags, magazineList = _b.magazineStore.magazineList;
    var defaultMagazineListHash = Object(encode["j" /* objectToHash */])({ page: 1, perPage: 12, type: application_magazine["a" /* MAGAZINE_LIST_TYPE */].DEFAULT });
    var keyHash = Object(encode["j" /* objectToHash */])({ days: days });
    var hashtagList = hashtags && !Object(validate["l" /* isUndefined */])(hashtags[keyHash]) ? hashtags[keyHash] : [];
    return (react["createElement"]("div", { style: right_bar_community_style.wrapLayout },
        renderTrendingTags({ hashtagList: hashtagList, hashtagSelected: hashtagSelected }),
        renderSuggestionFromExperts(dataHomePage && dataHomePage.latest_boxes && dataHomePage.latest_boxes.items || []),
        renderHotMagazine(magazineList[defaultMagazineListHash] || [])));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sYUFBYSxNQUFNLHNCQUFzQixDQUFDO0FBQ2pELE9BQU8sbUJBQW1CLE1BQU0sNkJBQTZCLENBQUM7QUFDOUQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3JELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUVwRixPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFHNUIsSUFBTSxZQUFZLEdBQUcsVUFBQyxJQUFJLEVBQUUsS0FBVTtJQUFWLHNCQUFBLEVBQUEsVUFBVTtJQUNwQyxNQUFNLENBQUMsNkJBQUssS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsSUFBRyxJQUFJLENBQU8sQ0FBQTtBQUN4RCxDQUFDLENBQUE7QUFFRCxJQUFNLFVBQVUsR0FBRyxVQUFDLElBQUksRUFBRSxTQUFjO0lBQWQsMEJBQUEsRUFBQSxjQUFjO0lBQ3RDLE1BQU0sQ0FBQyw2QkFBSyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxJQUFHLElBQUksQ0FBTyxDQUFBO0FBQzFELENBQUMsQ0FBQTtBQUVELElBQU0sVUFBVSxHQUFHLFVBQUMsRUFBZ0M7UUFBOUIsNEJBQVcsRUFBRSxvQ0FBZTtJQUNoRCxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU8sSUFFckIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUM7V0FDdkIsV0FBVyxDQUFDLE1BQU0sS0FBSyxDQUFDO1dBQ3hCLFdBQVcsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxlQUFlLGlCQUFBLEVBQUUsQ0FBQyxDQUV2RCxDQUNQLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRiwwQkFBMEIsSUFBSSxFQUFFLEtBQUs7SUFDbkMsSUFBTSxPQUFPLEdBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDO0lBRXhDLElBQU0sU0FBUyxHQUFHO1FBQ2hCLEVBQUUsRUFBSywwQkFBMEIsU0FBSSxPQUFTO1FBQzlDLEdBQUcsRUFBRSxjQUFZLEtBQU87UUFDeEIsS0FBSyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxlQUFlLEtBQUssT0FBTyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQztLQUMxRyxDQUFDO0lBRUYsTUFBTSxDQUFDLG9CQUFDLE9BQU8sZUFBSyxTQUFTLEdBQUcsT0FBSyxPQUFTLENBQVcsQ0FBQztBQUM1RCxDQUFDO0FBRUQsSUFBTSxrQkFBa0IsR0FBRyxVQUFDLEVBQWdDO1FBQTlCLDRCQUFXLEVBQUUsb0NBQWU7SUFDeEQsTUFBTSxDQUFDLENBQ0wsV0FBVztXQUNSLFdBQVcsQ0FBQyxNQUFNLEtBQUssQ0FBQztXQUN4QixDQUNELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWTtZQU8zQixZQUFZLENBQUMsZUFBZSxFQUFFLEVBQUUsYUFBYSxFQUFFLEVBQUUsRUFBRSxDQUFDO1lBQ3BELFVBQVUsQ0FBQyxFQUFFLFdBQVcsYUFBQSxFQUFFLGVBQWUsaUJBQUEsRUFBRSxDQUFDLENBQ3pDLENBQ1AsQ0FDRixDQUFBO0FBQ0gsQ0FBQyxDQUFBO0FBRUQsSUFBTSwyQkFBMkIsR0FBRyxVQUFDLElBQUk7SUFFdkMsSUFBTSxtQkFBbUIsR0FBRztRQUMxQixVQUFVLEVBQUUsS0FBSztRQUNqQixNQUFNLEVBQUUsQ0FBQztRQUNULElBQUksRUFBRSxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRTtRQUNwQyxZQUFZLEVBQUUsS0FBSztLQUNwQixDQUFDO0lBQ0YsTUFBTSxDQUFDLENBQ0wsSUFBSTtXQUNELENBQUMsS0FBSyxJQUFJLENBQUMsTUFBTTs7WUFFcEI7Z0JBQ0csWUFBWSxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsY0FBYyxDQUFDO2dCQUNoRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVc7b0JBQUUsb0JBQUMsYUFBYSxlQUFLLG1CQUFtQixFQUFJLENBQU0sQ0FDM0UsQ0FDUCxDQUFBO0FBRUgsQ0FBQyxDQUFBO0FBRUQsSUFBTSxpQkFBaUIsR0FBRyxVQUFDLElBQUk7SUFFN0IsSUFBTSxrQkFBa0IsR0FBRztRQUN6QixLQUFLLEVBQUUsRUFBRTtRQUNULE1BQU0sRUFBRSxDQUFDO1FBQ1QsVUFBVSxFQUFFLEtBQUs7UUFDakIsWUFBWSxFQUFFLEtBQUs7UUFDbkIsSUFBSSxFQUFFLElBQUksSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFO1FBQ3JELEtBQUssRUFBRSxFQUFFLGFBQWEsRUFBRSxDQUFDLEVBQUU7S0FDNUIsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLElBQUksSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU07O1lBRXJCO2dCQUNHLFlBQVksQ0FBQyxjQUFjLEVBQUUsS0FBSyxDQUFDLGNBQWMsQ0FBQztnQkFDbkQsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXO29CQUFFLG9CQUFDLG1CQUFtQixlQUFLLGtCQUFrQixFQUFJLENBQU0sQ0FDaEYsQ0FDUCxDQUFBO0FBRUgsQ0FBQyxDQUFBO0FBRUQsTUFBTSwwQkFBMEIsRUFBUztRQUFQLGdCQUFLO0lBQy9CLElBQUEsVUFNYSxFQUxqQixjQUFJLEVBQ0osb0NBQWUsRUFDRix3Q0FBWSxFQUNQLHFDQUFRLEVBQ1QsNENBQVksQ0FDWDtJQUVwQixJQUFNLHVCQUF1QixHQUFHLFlBQVksQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsa0JBQWtCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUV6RyxJQUFNLE9BQU8sR0FBRyxZQUFZLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDLENBQUM7SUFDdkMsSUFBTSxXQUFXLEdBQUcsUUFBUSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUV6RixNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFVBQVU7UUFDekIsa0JBQWtCLENBQUMsRUFBRSxXQUFXLGFBQUEsRUFBRSxlQUFlLGlCQUFBLEVBQUUsQ0FBQztRQUNwRCwyQkFBMkIsQ0FBQyxZQUFZLElBQUksWUFBWSxDQUFDLFlBQVksSUFBSSxZQUFZLENBQUMsWUFBWSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUM7UUFDL0csaUJBQWlCLENBQUMsWUFBWSxDQUFDLHVCQUF1QixDQUFDLElBQUksRUFBRSxDQUFDLENBQzNELENBQ1AsQ0FBQztBQUNKLENBQUM7QUFBQSxDQUFDIn0=
// CONCATENATED MODULE: ./components/community/right-bar-community/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var component_RightBarCommunity = /** @class */ (function (_super) {
    __extends(RightBarCommunity, _super);
    function RightBarCommunity(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    RightBarCommunity.prototype.componentDidMount = function () {
        var _a = this.props, fetchDataHomePage = _a.fetchDataHomePage, fetchMagazineListAction = _a.fetchMagazineListAction, dataHomePage = _a.shopStore.dataHomePage, magazineList = _a.magazineStore.magazineList;
        /** Magazine data */
        var fetchDefaultMagazineParam = {
            page: 1,
            perPage: 12,
            type: application_magazine["a" /* MAGAZINE_LIST_TYPE */].DEFAULT
        };
        var defaultMagazineListHash = Object(encode["j" /* objectToHash */])(fetchDefaultMagazineParam);
        true === Object(validate["l" /* isUndefined */])(magazineList[defaultMagazineListHash])
            && fetchMagazineListAction(fetchDefaultMagazineParam);
        /** Watched List */
        Object(validate["j" /* isEmptyObject */])(dataHomePage) && fetchDataHomePage();
    };
    RightBarCommunity.prototype.render = function () {
        var args = {
            props: this.props
        };
        return renderComponent(args);
    };
    RightBarCommunity.defaultProps = DEFAULT_PROPS;
    RightBarCommunity = __decorate([
        radium
    ], RightBarCommunity);
    return RightBarCommunity;
}(react["Component"]));
;
/* harmony default export */ var component = (component_RightBarCommunity);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3JELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxhQUFhLEVBQUUsV0FBVyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFFckUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFFNUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUd6QztJQUFnQyxxQ0FBK0I7SUFHN0QsMkJBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUF1QixDQUFDOztJQUN2QyxDQUFDO0lBRUQsNkNBQWlCLEdBQWpCO1FBQ1EsSUFBQSxlQUtrQixFQUp0Qix3Q0FBaUIsRUFDakIsb0RBQXVCLEVBQ1Ysd0NBQVksRUFDUiw0Q0FBWSxDQUNOO1FBRXpCLG9CQUFvQjtRQUNwQixJQUFNLHlCQUF5QixHQUFHO1lBQ2hDLElBQUksRUFBRSxDQUFDO1lBQ1AsT0FBTyxFQUFFLEVBQUU7WUFDWCxJQUFJLEVBQUUsa0JBQWtCLENBQUMsT0FBTztTQUNqQyxDQUFDO1FBRUYsSUFBTSx1QkFBdUIsR0FBRyxZQUFZLENBQUMseUJBQXlCLENBQUMsQ0FBQztRQUN4RSxJQUFJLEtBQUssV0FBVyxDQUFDLFlBQVksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO2VBQ3RELHVCQUF1QixDQUFDLHlCQUF5QixDQUFDLENBQUM7UUFFeEQsbUJBQW1CO1FBQ25CLGFBQWEsQ0FBQyxZQUFZLENBQUMsSUFBSSxpQkFBaUIsRUFBRSxDQUFDO0lBQ3JELENBQUM7SUFFRCxrQ0FBTSxHQUFOO1FBQ0UsSUFBTSxJQUFJLEdBQUc7WUFDWCxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7U0FDbEIsQ0FBQTtRQUVELE1BQU0sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQXBDTSw4QkFBWSxHQUFXLGFBQWEsQ0FBQztJQUR4QyxpQkFBaUI7UUFEdEIsTUFBTTtPQUNELGlCQUFpQixDQXNDdEI7SUFBRCx3QkFBQztDQUFBLEFBdENELENBQWdDLEtBQUssQ0FBQyxTQUFTLEdBc0M5QztBQUFBLENBQUM7QUFFRixlQUFlLGlCQUFpQixDQUFDIn0=
// CONCATENATED MODULE: ./components/community/right-bar-community/store.tsx
var connect = __webpack_require__(129).connect;



var mapStateToProps = function (state) { return ({
    shopStore: state.shop,
    magazineStore: state.magazine,
    communityStore: state.community
}); };
var mapDispatchToProps = function (dispatch) { return ({
    fetchDataHomePage: function () { return dispatch(Object(shop["b" /* fetchDataHomePageAction */])()); },
    fetchMagazineListAction: function (data) { return dispatch(Object(magazine["e" /* fetchMagazineListAction */])(data)); }
}); };
/* harmony default export */ var store = (connect(mapStateToProps, mapDispatchToProps)(component));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUMvQyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNuRSxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUUvRCxPQUFPLGlCQUFpQixNQUFNLGFBQWEsQ0FBQztBQUU1QyxNQUFNLENBQUMsSUFBTSxlQUFlLEdBQUcsVUFBQyxLQUFLLElBQUssT0FBQSxDQUFDO0lBQ3pDLFNBQVMsRUFBRSxLQUFLLENBQUMsSUFBSTtJQUNyQixhQUFhLEVBQUUsS0FBSyxDQUFDLFFBQVE7SUFDN0IsY0FBYyxFQUFFLEtBQUssQ0FBQyxTQUFTO0NBQ2hDLENBQUMsRUFKd0MsQ0FJeEMsQ0FBQztBQUVILE1BQU0sQ0FBQyxJQUFNLGtCQUFrQixHQUFHLFVBQUMsUUFBUSxJQUFLLE9BQUEsQ0FBQztJQUMvQyxpQkFBaUIsRUFBRSxjQUFNLE9BQUEsUUFBUSxDQUFDLHVCQUF1QixFQUFFLENBQUMsRUFBbkMsQ0FBbUM7SUFDNUQsdUJBQXVCLEVBQUUsVUFBQyxJQUFTLElBQUssT0FBQSxRQUFRLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBdkMsQ0FBdUM7Q0FDaEYsQ0FBQyxFQUg4QyxDQUc5QyxDQUFDO0FBRUgsZUFBZSxPQUFPLENBQ3BCLGVBQWUsRUFDZixrQkFBa0IsQ0FDbkIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDIn0=
// CONCATENATED MODULE: ./components/community/right-bar-community/index.tsx
// import * as React from 'react';
// import * as Loadable from 'react-loadable';
// import LazyLoading from '../../ui/lazy-loading';
// export default Loadable({
//   loader: () => import('./store'),
//   loading: () => <LazyLoading />
// });

/* harmony default export */ var right_bar_community = __webpack_exports__["a"] = (store);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsa0NBQWtDO0FBQ2xDLDhDQUE4QztBQUM5QyxtREFBbUQ7QUFFbkQsNEJBQTRCO0FBQzVCLHFDQUFxQztBQUNyQyxtQ0FBbUM7QUFDbkMsTUFBTTtBQUVOLE9BQU8sU0FBUyxNQUFNLFNBQVMsQ0FBQztBQUNoQyxlQUFlLFNBQVMsQ0FBQyJ9

/***/ }),

/***/ 901:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./action/activity-feed.ts + 1 modules
var activity_feed = __webpack_require__(765);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// CONCATENATED MODULE: ./container/app-shop/community/collection-detail/initialize.tsx
var DEFAULT_PROPS = {
    limit: 10
};
var INITIAL_STATE = {
    feedActiveId: -1
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixLQUFLLEVBQUUsRUFBRTtDQUNBLENBQUM7QUFFWixNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUc7SUFDM0IsWUFBWSxFQUFFLENBQUMsQ0FBQztDQUNQLENBQUMifQ==
// EXTERNAL MODULE: ./components/community/right-bar-community/index.tsx + 5 modules
var right_bar_community = __webpack_require__(784);

// EXTERNAL MODULE: ./container/layout/split/index.tsx
var split = __webpack_require__(755);

// EXTERNAL MODULE: ./components/container/feed-item/index.tsx + 5 modules
var feed_item = __webpack_require__(772);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(347);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./container/app-shop/community/collection-detail/style.tsx

/* harmony default export */ var style = ({
    display: 'block',
    position: 'relative',
    wrapLayout: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 20,
        paddingLeft: 0
    },
    mobileWrapLayout: {
        padding: '10px 10px 0'
    },
    item: {
        display: variable["display"].flex,
        position: variable["position"].relative,
        boxShadow: variable["shadowBlur"],
        borderRadius: 5,
        overflow: 'hidden',
        marginBottom: 20,
        itemWithLine: {
            borderTop: "1px solid " + variable["colorF0"]
        },
        cover: {
            icon: {
                position: variable["position"].absolute,
                bottom: 0,
                right: -45,
                opacity: .25,
                width: 110,
                height: 110,
                color: variable["randomColorList"](-1)
            },
            innerIcon: {
                height: 130
            }
        },
        content: {
            padding: 20,
            title: {
                fontFamily: variable["fontTrirong"],
                fontSize: 18,
                lineHeight: '24px',
                fontWeight: 600,
                color: variable["colorBlack"],
                marginBottom: 10
            },
            description: {
                fontSize: 14,
                lineHeight: '22px',
                marginBottom: 10
            },
            link: {
                display: variable["display"].inlineFlex,
                justifyContent: 'center',
                alignItems: 'center',
                height: 26,
                lineHeight: 26,
                background: variable["colorF7"],
                border: "1px solid " + variable["colorF0"],
                color: variable["colorPink"],
                fontFamily: variable["fontAvenirMedium"],
                borderRadius: 2,
                paddingLeft: 13,
                icon: {
                    width: 26,
                    height: 26,
                    color: variable["colorPink"]
                },
                innerIcon: {
                    height: 10
                }
            },
        }
    },
    feedList: {
        boxShadow: variable["shadowBlurSort"],
        borderRadius: 5,
        overflow: 'hidden',
        marginBottom: 10,
    },
    feedItem: {
        heading: {
            display: variable["display"].flex,
            justifyContent: 'space-between',
            cursor: 'pointer',
            padding: 20,
            borderBottom: "1px solid " + variable["colorF0"],
            avatar: {
                width: 40,
                minWidth: 40,
                height: 40,
                borderRadius: '50%',
            },
            infoUser: {
                padding: '0 40px 0 10px',
                name: {
                    fontSize: 14,
                    lineHeight: '20px',
                    height: 20,
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    fontFamily: variable["fontAvenirDemiBold"],
                },
                message: {
                    fontSize: 14,
                    color: variable["color4D"],
                    lineHeight: '20px',
                    height: 20,
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                }
            },
            icon: {
                minWidth: 40,
                width: 40,
                height: 40,
                color: variable["color2E"],
                marginRight: -10
            },
            innerIcon: {
                height: 8
            },
        },
        content: {}
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSw0QkFBNEIsQ0FBQztBQUV2RCxlQUFlO0lBQ2IsT0FBTyxFQUFFLE9BQU87SUFDaEIsUUFBUSxFQUFFLFVBQVU7SUFFcEIsVUFBVSxFQUFFO1FBQ1YsVUFBVSxFQUFFLENBQUM7UUFDYixZQUFZLEVBQUUsQ0FBQztRQUNmLGFBQWEsRUFBRSxFQUFFO1FBQ2pCLFdBQVcsRUFBRSxDQUFDO0tBQ2Y7SUFFRCxnQkFBZ0IsRUFBRTtRQUNoQixPQUFPLEVBQUUsYUFBYTtLQUN2QjtJQUVELElBQUksRUFBRTtRQUNKLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7UUFDOUIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtRQUNwQyxTQUFTLEVBQUUsUUFBUSxDQUFDLFVBQVU7UUFDOUIsWUFBWSxFQUFFLENBQUM7UUFDZixRQUFRLEVBQUUsUUFBUTtRQUNsQixZQUFZLEVBQUUsRUFBRTtRQUVoQixZQUFZLEVBQUU7WUFDWixTQUFTLEVBQUUsZUFBYSxRQUFRLENBQUMsT0FBUztTQUMzQztRQUVELEtBQUssRUFBRTtZQUNMLElBQUksRUFBRTtnQkFDSixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxNQUFNLEVBQUUsQ0FBQztnQkFDVCxLQUFLLEVBQUUsQ0FBQyxFQUFFO2dCQUNWLE9BQU8sRUFBRSxHQUFHO2dCQUNaLEtBQUssRUFBRSxHQUFHO2dCQUNWLE1BQU0sRUFBRSxHQUFHO2dCQUNYLEtBQUssRUFBRSxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3BDO1lBRUQsU0FBUyxFQUFFO2dCQUNULE1BQU0sRUFBRSxHQUFHO2FBQ1o7U0FDRjtRQUVELE9BQU8sRUFBRTtZQUNQLE9BQU8sRUFBRSxFQUFFO1lBRVgsS0FBSyxFQUFFO2dCQUNMLFVBQVUsRUFBRSxRQUFRLENBQUMsV0FBVztnQkFDaEMsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osVUFBVSxFQUFFLE1BQU07Z0JBQ2xCLFVBQVUsRUFBRSxHQUFHO2dCQUNmLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDMUIsWUFBWSxFQUFFLEVBQUU7YUFDakI7WUFFRCxXQUFXLEVBQUU7Z0JBQ1gsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osVUFBVSxFQUFFLE1BQU07Z0JBQ2xCLFlBQVksRUFBRSxFQUFFO2FBQ2pCO1lBRUQsSUFBSSxFQUFFO2dCQUNKLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQVU7Z0JBQ3BDLGNBQWMsRUFBRSxRQUFRO2dCQUN4QixVQUFVLEVBQUUsUUFBUTtnQkFDcEIsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsVUFBVSxFQUFFLEVBQUU7Z0JBQ2QsVUFBVSxFQUFFLFFBQVEsQ0FBQyxPQUFPO2dCQUM1QixNQUFNLEVBQUUsZUFBYSxRQUFRLENBQUMsT0FBUztnQkFDdkMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxTQUFTO2dCQUN6QixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtnQkFDckMsWUFBWSxFQUFFLENBQUM7Z0JBQ2YsV0FBVyxFQUFFLEVBQUU7Z0JBRWYsSUFBSSxFQUFFO29CQUNKLEtBQUssRUFBRSxFQUFFO29CQUNULE1BQU0sRUFBRSxFQUFFO29CQUNWLEtBQUssRUFBRSxRQUFRLENBQUMsU0FBUztpQkFDMUI7Z0JBRUQsU0FBUyxFQUFFO29CQUNULE1BQU0sRUFBRSxFQUFFO2lCQUNYO2FBQ0Y7U0FDRjtLQUNGO0lBRUQsUUFBUSxFQUFFO1FBQ1IsU0FBUyxFQUFFLFFBQVEsQ0FBQyxjQUFjO1FBQ2xDLFlBQVksRUFBRSxDQUFDO1FBQ2YsUUFBUSxFQUFFLFFBQVE7UUFDbEIsWUFBWSxFQUFFLEVBQUU7S0FDakI7SUFFRCxRQUFRLEVBQUU7UUFDUixPQUFPLEVBQUU7WUFDUCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLGNBQWMsRUFBRSxlQUFlO1lBQy9CLE1BQU0sRUFBRSxTQUFTO1lBQ2pCLE9BQU8sRUFBRSxFQUFFO1lBQ1gsWUFBWSxFQUFFLGVBQWEsUUFBUSxDQUFDLE9BQVM7WUFFN0MsTUFBTSxFQUFFO2dCQUNOLEtBQUssRUFBRSxFQUFFO2dCQUNULFFBQVEsRUFBRSxFQUFFO2dCQUNaLE1BQU0sRUFBRSxFQUFFO2dCQUNWLFlBQVksRUFBRSxLQUFLO2FBQ3BCO1lBRUQsUUFBUSxFQUFFO2dCQUNSLE9BQU8sRUFBRSxlQUFlO2dCQUV4QixJQUFJLEVBQUU7b0JBQ0osUUFBUSxFQUFFLEVBQUU7b0JBQ1osVUFBVSxFQUFFLE1BQU07b0JBQ2xCLE1BQU0sRUFBRSxFQUFFO29CQUNWLFFBQVEsRUFBRSxRQUFRO29CQUNsQixZQUFZLEVBQUUsVUFBVTtvQkFDeEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxrQkFBa0I7aUJBQ3hDO2dCQUVELE9BQU8sRUFBRTtvQkFDUCxRQUFRLEVBQUUsRUFBRTtvQkFDWixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87b0JBQ3ZCLFVBQVUsRUFBRSxNQUFNO29CQUNsQixNQUFNLEVBQUUsRUFBRTtvQkFDVixRQUFRLEVBQUUsUUFBUTtvQkFDbEIsWUFBWSxFQUFFLFVBQVU7aUJBQ3pCO2FBQ0Y7WUFFRCxJQUFJLEVBQUU7Z0JBQ0osUUFBUSxFQUFFLEVBQUU7Z0JBQ1osS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO2dCQUN2QixXQUFXLEVBQUUsQ0FBQyxFQUFFO2FBQ2pCO1lBRUQsU0FBUyxFQUFFO2dCQUNULE1BQU0sRUFBRSxDQUFDO2FBQ1Y7U0FDRjtRQUVELE9BQU8sRUFBRSxFQUFFO0tBQ1o7Q0FDTSxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/community/collection-detail/view-general.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};





var renderContent = function (item) {
    var iconProps = {
        name: 'arrow-right',
        style: style.item.content.link.icon,
        innerStyle: style.item.content.link.innerIcon,
    };
    var linkProps = {
        to: routing["o" /* ROUTING_COMMUNITY_COLLECTION_PATH */] + "/" + item.id,
        style: style.item.content.link,
    };
    return (react["createElement"]("div", { style: style.item.content },
        react["createElement"]("div", { style: style.item.content.title }, item.name),
        react["createElement"]("div", { style: style.item.content.description }, item.description)));
};
var renderCollectionItem = function (item) {
    var iconProps = {
        name: 'star-light',
        style: style.item.cover.icon,
        innerStyle: style.item.cover.innerIcon,
    };
    return (react["createElement"]("div", { style: style.item },
        react["createElement"](icon["a" /* default */], __assign({}, iconProps)),
        renderContent(item)));
};
function renderFeedItem(item, $index) {
    var _this = this;
    var iconProps = {
        name: 'angle-down',
        style: style.feedItem.heading.icon,
        innerStyle: style.feedItem.heading.innerIcon,
    };
    var feedItemProps = {
        history: history,
        item: item,
        userProfile: this.userProfile
    };
    return (react["createElement"]("div", { key: "feed-item-" + $index, style: style.feedItem },
        item.id !== this.feedActiveId
            && (react["createElement"]("div", { style: style.feedItem.heading, onClick: function () { return _this.openFeedDetail(item.id); } },
                react["createElement"]("img", { src: item.user.avatar.medium_url, style: style.feedItem.heading.avatar }),
                react["createElement"]("div", { style: style.feedItem.heading.infoUser },
                    react["createElement"]("div", { style: style.feedItem.heading.infoUser.name }, item.user.name),
                    react["createElement"]("div", { style: style.feedItem.heading.infoUser.message }, item.message)),
                react["createElement"](icon["a" /* default */], __assign({}, iconProps)))),
        item.id === this.feedActiveId && react["createElement"](feed_item["a" /* default */], __assign({}, feedItemProps))));
}
var renderFeedList = function (_a) {
    var collectionDetail = _a.collectionDetail, userProfile = _a.userProfile, feedActiveId = _a.feedActiveId, openFeedDetail = _a.openFeedDetail;
    var feedListProps = {
        history: history,
        list: collectionDetail.feeds,
        userProfile: userProfile,
        style: style.feedList
    };
    // return <FeedList {...feedListProps} />;
    return (react["createElement"]("div", { style: style.feedList }, collectionDetail.feeds.map(renderFeedItem, { feedActiveId: feedActiveId, userProfile: userProfile, openFeedDetail: openFeedDetail })));
};
var renderMainContainer = function (_a) {
    var collectionDetail = _a.collectionDetail, userProfile = _a.userProfile, feedActiveId = _a.feedActiveId, openFeedDetail = _a.openFeedDetail;
    if (!collectionDetail) {
        return null;
    }
    return (react["createElement"]("div", { style: style },
        renderCollectionItem(collectionDetail.collection),
        renderFeedList({ collectionDetail: collectionDetail, userProfile: userProfile, feedActiveId: feedActiveId, openFeedDetail: openFeedDetail })));
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1nZW5lcmFsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1nZW5lcmFsLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBSy9CLE9BQU8sUUFBUSxNQUFNLDRDQUE0QyxDQUFDO0FBRWxFLE9BQU8sSUFBSSxNQUFNLGdDQUFnQyxDQUFDO0FBS2xELE9BQU8sRUFBRSxpQ0FBaUMsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBRTlGLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUc1QixJQUFNLGFBQWEsR0FBRyxVQUFDLElBQUk7SUFDekIsSUFBTSxTQUFTLEdBQUc7UUFDaEIsSUFBSSxFQUFFLGFBQWE7UUFDbkIsS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJO1FBQ25DLFVBQVUsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUztLQUM5QyxDQUFDO0lBRUYsSUFBTSxTQUFTLEdBQUc7UUFDaEIsRUFBRSxFQUFLLGlDQUFpQyxTQUFJLElBQUksQ0FBQyxFQUFJO1FBQ3JELEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJO0tBQy9CLENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPO1FBQzVCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLElBQUcsSUFBSSxDQUFDLElBQUksQ0FBTztRQUN2RCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxJQUFHLElBQUksQ0FBQyxXQUFXLENBQU8sQ0FDaEUsQ0FDUCxDQUFDO0FBQ0osQ0FBQyxDQUFBO0FBRUQsSUFBTSxvQkFBb0IsR0FBRyxVQUFDLElBQUk7SUFDaEMsSUFBTSxTQUFTLEdBQUc7UUFDaEIsSUFBSSxFQUFFLFlBQVk7UUFDbEIsS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUk7UUFDNUIsVUFBVSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVM7S0FDdkMsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSTtRQUNwQixvQkFBQyxJQUFJLGVBQUssU0FBUyxFQUFJO1FBQ3RCLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FDaEIsQ0FDUCxDQUFDO0FBQ0osQ0FBQyxDQUFBO0FBRUQsd0JBQXdCLElBQUksRUFBRSxNQUFNO0lBQXBDLGlCQWdDQztJQS9CQyxJQUFNLFNBQVMsR0FBRztRQUNoQixJQUFJLEVBQUUsWUFBWTtRQUNsQixLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUNsQyxVQUFVLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsU0FBUztLQUM3QyxDQUFDO0lBRUYsSUFBTSxhQUFhLEdBQUc7UUFDcEIsT0FBTyxTQUFBO1FBQ1AsSUFBSSxNQUFBO1FBQ0osV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXO0tBQzlCLENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCw2QkFBSyxHQUFHLEVBQUUsZUFBYSxNQUFRLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxRQUFRO1FBRWxELElBQUksQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLFlBQVk7ZUFDMUIsQ0FDRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLGNBQU0sT0FBQSxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBNUIsQ0FBNEI7Z0JBQzdFLDZCQUFLLEdBQUcsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBSTtnQkFDL0UsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFFBQVE7b0JBQ3pDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxJQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFPO29CQUN4RSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLE9BQU8sSUFBRyxJQUFJLENBQUMsT0FBTyxDQUFPLENBQ3JFO2dCQUNOLG9CQUFDLElBQUksZUFBSyxTQUFTLEVBQUksQ0FDbkIsQ0FDUDtRQUdGLElBQUksQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLFlBQVksSUFBSSxvQkFBQyxRQUFRLGVBQUssYUFBYSxFQUFhLENBQ3JFLENBQ1IsQ0FBQztBQUNKLENBQUM7QUFFRCxJQUFNLGNBQWMsR0FBRyxVQUFDLEVBQStEO1FBQTdELHNDQUFnQixFQUFFLDRCQUFXLEVBQUUsOEJBQVksRUFBRSxrQ0FBYztJQUNuRixJQUFNLGFBQWEsR0FBRztRQUNwQixPQUFPLFNBQUE7UUFDUCxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsS0FBSztRQUM1QixXQUFXLGFBQUE7UUFDWCxLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVE7S0FDdEIsQ0FBQztJQUVGLDBDQUEwQztJQUMxQyxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsSUFDdkIsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsRUFBRSxZQUFZLGNBQUEsRUFBRSxXQUFXLGFBQUEsRUFBRSxjQUFjLGdCQUFBLEVBQUUsQ0FBQyxDQUN0RixDQUNQLENBQUE7QUFDSCxDQUFDLENBQUE7QUFFRCxNQUFNLENBQUMsSUFBTSxtQkFBbUIsR0FBRyxVQUFDLEVBQStEO1FBQTdELHNDQUFnQixFQUFFLDRCQUFXLEVBQUUsOEJBQVksRUFBRSxrQ0FBYztJQUMvRixFQUFFLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztRQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUE7SUFBQyxDQUFDO0lBRXRDLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLO1FBQ2Qsb0JBQW9CLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDO1FBQ2pELGNBQWMsQ0FBQyxFQUFFLGdCQUFnQixrQkFBQSxFQUFFLFdBQVcsYUFBQSxFQUFFLFlBQVksY0FBQSxFQUFFLGNBQWMsZ0JBQUEsRUFBRSxDQUFDLENBQzNFLENBQ1IsQ0FBQztBQUNKLENBQUMsQ0FBQSJ9
// CONCATENATED MODULE: ./container/app-shop/community/collection-detail/view-desktop.tsx
var view_desktop_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};





function renderDesktop(_a) {
    var props = _a.props, state = _a.state, openFeedDetail = _a.openFeedDetail;
    var _b = props, collectionId = _b.match.params.collectionId, detail = _b.activityFeedStore.collection.detail, userProfile = _b.userStore.userProfile;
    var feedActiveId = state.feedActiveId;
    var collectionDetail = detail && detail[collectionId];
    var splitLayoutProps = {
        type: 'right',
        size: 'larger',
        subContainer: react["createElement"](right_bar_community["a" /* default */], null),
        mainContainer: renderMainContainer({ collectionDetail: collectionDetail, userProfile: userProfile, feedActiveId: feedActiveId, openFeedDetail: openFeedDetail })
    };
    return (react["createElement"]("div", { style: style.wrapLayout },
        react["createElement"](split["a" /* default */], view_desktop_assign({}, splitLayoutProps))));
}
;
/* harmony default export */ var view_desktop = (renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1kZXNrdG9wLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1kZXNrdG9wLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBSS9CLE9BQU8saUJBQWlCLE1BQU0sc0RBQXNELENBQUM7QUFLckYsT0FBTyxXQUFXLE1BQU0sdUJBQXVCLENBQUM7QUFJaEQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDckQsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBSTVCLHVCQUF1QixFQUFnQztRQUE5QixnQkFBSyxFQUFFLGdCQUFLLEVBQUUsa0NBQWM7SUFDN0MsSUFBQSxVQUlhLEVBSEUsMkNBQVksRUFDSSwrQ0FBTSxFQUM1QixzQ0FBVyxDQUNOO0lBRVosSUFBQSxpQ0FBWSxDQUFXO0lBRS9CLElBQU0sZ0JBQWdCLEdBQUcsTUFBTSxJQUFJLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUV4RCxJQUFNLGdCQUFnQixHQUFHO1FBQ3ZCLElBQUksRUFBRSxPQUFPO1FBQ2IsSUFBSSxFQUFFLFFBQVE7UUFDZCxZQUFZLEVBQUUsb0JBQUMsaUJBQWlCLE9BQUc7UUFDbkMsYUFBYSxFQUFFLG1CQUFtQixDQUFDLEVBQUUsZ0JBQWdCLGtCQUFBLEVBQUUsV0FBVyxhQUFBLEVBQUUsWUFBWSxjQUFBLEVBQUUsY0FBYyxnQkFBQSxFQUFFLENBQUM7S0FDcEcsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsVUFBVTtRQUMxQixvQkFBQyxXQUFXLGVBQUssZ0JBQWdCLEVBQUksQ0FDakMsQ0FDUCxDQUFDO0FBQ0osQ0FBQztBQUFBLENBQUM7QUFFRixlQUFlLGFBQWEsQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/community/collection-detail/view-mobile.tsx



function renderMobile(_a) {
    var props = _a.props, state = _a.state, openFeedDetail = _a.openFeedDetail;
    var _b = props, collectionId = _b.match.params.collectionId, detail = _b.activityFeedStore.collection.detail, userProfile = _b.userStore.userProfile;
    var feedActiveId = state.feedActiveId;
    var collectionDetail = detail && detail[collectionId];
    return (react["createElement"]("div", { style: style.mobileWrapLayout }, renderMainContainer({ collectionDetail: collectionDetail, userProfile: userProfile, feedActiveId: feedActiveId, openFeedDetail: openFeedDetail })));
}
;
/* harmony default export */ var view_mobile = (renderMobile);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1tb2JpbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LW1vYmlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFjL0IsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRTVCLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXJELHNCQUFzQixFQUFnQztRQUE5QixnQkFBSyxFQUFFLGdCQUFLLEVBQUUsa0NBQWM7SUFDNUMsSUFBQSxVQUlhLEVBSEUsMkNBQVksRUFDSSwrQ0FBTSxFQUM1QixzQ0FBVyxDQUNOO0lBRVosSUFBQSxpQ0FBWSxDQUFXO0lBRS9CLElBQU0sZ0JBQWdCLEdBQUcsTUFBTSxJQUFJLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUV4RCxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGdCQUFnQixJQUMvQixtQkFBbUIsQ0FBQyxFQUFFLGdCQUFnQixrQkFBQSxFQUFFLFdBQVcsYUFBQSxFQUFFLFlBQVksY0FBQSxFQUFFLGNBQWMsZ0JBQUEsRUFBRSxDQUFDLENBQ2pGLENBQ1AsQ0FBQztBQUNKLENBQUM7QUFBQSxDQUFDO0FBRUYsZUFBZSxZQUFZLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/community/collection-detail/view.tsx




var renderLoadingPlaceholder = function () { return (react["createElement"]("div", { style: style.placeholder.container }, [1, 2, 3].map(function (item) { return (react["createElement"]("div", { style: style.placeholder.item, key: item },
    react["createElement"]("div", { style: style.placeholder.item.top },
        react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.top.avatar }),
        react["createElement"]("div", { style: style.placeholder.item.top.info },
            react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.top.username }),
            react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.top.star }))),
    react["createElement"]("div", { style: style.placeholder.item.bottom },
        react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.bottom.firstText }),
        react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.bottom.text }),
        react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.bottom.text }),
        react["createElement"]("div", { className: 'ani-bg', style: style.placeholder.item.bottom.lastText })))); }))); };
var renderView = function (_a) {
    var props = _a.props, state = _a.state, openFeedDetail = _a.openFeedDetail;
    var switchView = {
        MOBILE: function () { return view_mobile({ props: props, state: state, openFeedDetail: openFeedDetail }); },
        DESKTOP: function () { return view_desktop({ props: props, state: state, openFeedDetail: openFeedDetail }); }
    };
    return switchView[window.DEVICE_VERSION]();
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sYUFBYSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sWUFBWSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsTUFBTSxDQUFDLElBQU0sd0JBQXdCLEdBQUcsY0FBTSxPQUFBLENBQzVDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFNBQVMsSUFFbkMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLENBQ3BCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSTtJQUMzQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRztRQUNwQyw2QkFBSyxTQUFTLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFRO1FBQzFFLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSTtZQUN6Qyw2QkFBSyxTQUFTLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFRO1lBQzVFLDZCQUFLLFNBQVMsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEdBQVEsQ0FDcEUsQ0FDRjtJQUNOLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNO1FBQ3ZDLDZCQUFLLFNBQVMsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQVE7UUFDaEYsNkJBQUssU0FBUyxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksR0FBUTtRQUMzRSw2QkFBSyxTQUFTLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFRO1FBQzNFLDZCQUFLLFNBQVMsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQVEsQ0FDM0UsQ0FDRixDQUNQLEVBaEJxQixDQWdCckIsQ0FBQyxDQUVBLENBQ1AsRUF0QjZDLENBc0I3QyxDQUFDO0FBRUYsSUFBTSxVQUFVLEdBQUcsVUFBQyxFQUFnQztRQUE5QixnQkFBSyxFQUFFLGdCQUFLLEVBQUUsa0NBQWM7SUFDaEQsSUFBTSxVQUFVLEdBQUc7UUFDakIsTUFBTSxFQUFFLGNBQU0sT0FBQSxZQUFZLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxLQUFLLE9BQUEsRUFBRSxjQUFjLGdCQUFBLEVBQUUsQ0FBQyxFQUE5QyxDQUE4QztRQUM1RCxPQUFPLEVBQUUsY0FBTSxPQUFBLGFBQWEsQ0FBQyxFQUFFLEtBQUssT0FBQSxFQUFFLEtBQUssT0FBQSxFQUFFLGNBQWMsZ0JBQUEsRUFBRSxDQUFDLEVBQS9DLENBQStDO0tBQy9ELENBQUM7SUFFRixNQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFBO0FBQzVDLENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/community/collection-detail/container.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var container_CollectionDetailContainer = /** @class */ (function (_super) {
    __extends(CollectionDetailContainer, _super);
    function CollectionDetailContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    CollectionDetailContainer.prototype.componentDidMount = function () {
        this.init(this.props);
    };
    CollectionDetailContainer.prototype.init = function (props) {
        if (props === void 0) { props = this.props; }
        var collectionId = props.match.params.collectionId, getCollectionDetailAction = props.getCollectionDetailAction;
        getCollectionDetailAction({ id: collectionId });
    };
    CollectionDetailContainer.prototype.shouldComponentUpdate = function (nextProps) {
        // if (this.props.activityFeedStore
        //   && this.props.activityFeedStore.list
        //   && this.props.activityFeedStore.list.length !== nextProps.activityFeedStore.list.length) { return true; };
        return true;
    };
    CollectionDetailContainer.prototype.openFeedDetail = function (feedId) {
        this.setState({ feedActiveId: feedId });
    };
    CollectionDetailContainer.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state,
            openFeedDetail: this.openFeedDetail.bind(this)
        };
        return view(args);
    };
    CollectionDetailContainer.defaultProps = DEFAULT_PROPS;
    CollectionDetailContainer = __decorate([
        radium
    ], CollectionDetailContainer);
    return CollectionDetailContainer;
}(react["Component"]));
;
/* harmony default export */ var container = (container_CollectionDetailContainer);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFHakMsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFFNUQsT0FBTyxVQUFVLE1BQU0sUUFBUSxDQUFDO0FBR2hDO0lBQXdDLDZDQUErQjtJQUdyRSxtQ0FBWSxLQUFhO1FBQXpCLFlBQ0Usa0JBQU0sS0FBSyxDQUFDLFNBRWI7UUFEQyxLQUFJLENBQUMsS0FBSyxHQUFHLGFBQXVCLENBQUM7O0lBQ3ZDLENBQUM7SUFFRCxxREFBaUIsR0FBakI7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN4QixDQUFDO0lBRUQsd0NBQUksR0FBSixVQUFLLEtBQWtCO1FBQWxCLHNCQUFBLEVBQUEsUUFBUSxJQUFJLENBQUMsS0FBSztRQUVBLElBQUEsOENBQVksRUFDL0IsMkRBQXlCLENBQ2pCO1FBRVYseUJBQXlCLENBQUMsRUFBRSxFQUFFLEVBQUUsWUFBWSxFQUFFLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBRUQseURBQXFCLEdBQXJCLFVBQXNCLFNBQVM7UUFDN0IsbUNBQW1DO1FBQ25DLHlDQUF5QztRQUN6QywrR0FBK0c7UUFDL0csTUFBTSxDQUFDLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCxrREFBYyxHQUFkLFVBQWUsTUFBTTtRQUNuQixJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUE7SUFDekMsQ0FBQztJQUVELDBDQUFNLEdBQU47UUFDRSxJQUFNLElBQUksR0FBRztZQUNYLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsY0FBYyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztTQUMvQyxDQUFDO1FBRUYsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBdkNNLHNDQUFZLEdBQVcsYUFBYSxDQUFDO0lBRHhDLHlCQUF5QjtRQUQ5QixNQUFNO09BQ0QseUJBQXlCLENBeUM5QjtJQUFELGdDQUFDO0NBQUEsQUF6Q0QsQ0FBd0MsS0FBSyxDQUFDLFNBQVMsR0F5Q3REO0FBQUEsQ0FBQztBQUVGLGVBQWUseUJBQXlCLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/community/collection-detail/store.tsx
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapStateToProps", function() { return mapStateToProps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapDispatchToProps", function() { return mapDispatchToProps; });
var connect = __webpack_require__(129).connect;


var mapStateToProps = function (state) { return ({
    activityFeedStore: state.activityFeed,
    userStore: state.user,
    authStore: state.auth
}); };
var mapDispatchToProps = function (dispatch) { return ({
    getCollectionDetailAction: function (data) { return dispatch(Object(activity_feed["h" /* getCollectionDetailAction */])(data)); },
}); };
/* harmony default export */ var store = __webpack_exports__["default"] = (connect(mapStateToProps, mapDispatchToProps)(container));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUc3RSxPQUFPLHlCQUF5QixNQUFNLGFBQWEsQ0FBQztBQUVwRCxNQUFNLENBQUMsSUFBTSxlQUFlLEdBQUcsVUFBQyxLQUFLLElBQUssT0FBQSxDQUFDO0lBQ3pDLGlCQUFpQixFQUFFLEtBQUssQ0FBQyxZQUFZO0lBQ3JDLFNBQVMsRUFBRSxLQUFLLENBQUMsSUFBSTtJQUNyQixTQUFTLEVBQUUsS0FBSyxDQUFDLElBQUk7Q0FDdEIsQ0FBQyxFQUp3QyxDQUl4QyxDQUFDO0FBRUgsTUFBTSxDQUFDLElBQU0sa0JBQWtCLEdBQUcsVUFBQyxRQUFRLElBQUssT0FBQSxDQUFDO0lBQy9DLHlCQUF5QixFQUFFLFVBQUMsSUFBUyxJQUFLLE9BQUEsUUFBUSxDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxDQUFDLEVBQXpDLENBQXlDO0NBQ3BGLENBQUMsRUFGOEMsQ0FFOUMsQ0FBQztBQUVILGVBQWUsT0FBTyxDQUNwQixlQUFlLEVBQ2Ysa0JBQWtCLENBQ25CLENBQUMseUJBQXlCLENBQUMsQ0FBQyJ9

/***/ })

}]);