(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[86],{

/***/ 752:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return currenyFormat; });
/**
 * Format currency to VND
 * @param {number} currency value number of currency
 * @return {string} curency with format
 */
var currenyFormat = function (currency, type) {
    if (type === void 0) { type = 'currency'; }
    /**
     * Validate value
     * - NULL
     * - UNDEFINED
     * NOT A NUMBER
     */
    if (null === currency || 'undefined' === typeof currency || true === isNaN(currency)) {
        return '0';
    }
    /**
     * Validate value
     * < 0
     */
    if (currency < 0) {
        return '0';
    }
    var suffix = 'currency' === type
        ? ''
        : 'coin' === type ? ' coin' : '';
    return currency.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + suffix;
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VycmVuY3kuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJjdXJyZW5jeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7OztHQUlHO0FBQ0gsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHLFVBQUMsUUFBZ0IsRUFBRSxJQUFpQjtJQUFqQixxQkFBQSxFQUFBLGlCQUFpQjtJQUMvRDs7Ozs7T0FLRztJQUNILEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxRQUFRLElBQUksV0FBVyxLQUFLLE9BQU8sUUFBUSxJQUFJLElBQUksS0FBSyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3JGLE1BQU0sQ0FBQyxHQUFHLENBQUM7SUFDYixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsRUFBRSxDQUFDLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFBQyxNQUFNLENBQUMsR0FBRyxDQUFDO0lBQUMsQ0FBQztJQUVqQyxJQUFNLE1BQU0sR0FBRyxVQUFVLEtBQUssSUFBSTtRQUNoQyxDQUFDLENBQUMsRUFBRTtRQUNKLENBQUMsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUVuQyxNQUFNLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLE9BQU8sQ0FBQyx5QkFBeUIsRUFBRSxLQUFLLENBQUMsR0FBRyxNQUFNLENBQUM7QUFDaEYsQ0FBQyxDQUFDIn0=

/***/ }),

/***/ 754:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return updateMetaInfoAction; });
/* harmony import */ var _constants_api_meta__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(352);

var updateMetaInfoAction = function (data) { return ({
    type: _constants_api_meta__WEBPACK_IMPORTED_MODULE_0__[/* UPDATE_META_INFO */ "a"],
    payload: data
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWV0YS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1ldGEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLGdCQUFnQixFQUNqQixNQUFNLHVCQUF1QixDQUFDO0FBRS9CLE1BQU0sQ0FBQyxJQUFNLG9CQUFvQixHQUFHLFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FBQztJQUM3QyxJQUFJLEVBQUUsZ0JBQWdCO0lBQ3RCLE9BQU8sRUFBRSxJQUFJO0NBQ2QsQ0FBQyxFQUg0QyxDQUc1QyxDQUFDIn0=

/***/ }),

/***/ 807:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// CONCATENATED MODULE: ./api/game.ts

var loadGame = function () { return Object(restful_method["d" /* post */])({
    path: "/games/load_game",
    description: 'Load game',
    errorMesssage: "Can't load game. Please try again",
}); };
var getUserGift = function () { return Object(restful_method["b" /* get */])({
    path: "/games/user_gifts",
    description: 'load user gift',
    errorMesssage: "Can't load game. Please try again",
}); };
var getTodayGift = function () { return Object(restful_method["b" /* get */])({
    path: "/games/today_gifts",
    description: 'load today gift',
    errorMesssage: "Can't load game. Please try again",
}); };
var playGame = function (_a) {
    var id = _a.id;
    return Object(restful_method["c" /* patch */])({
        path: "/games/" + id + "/play",
        description: 'load today gift',
        errorMesssage: "Can't load game. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2FtZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImdhbWUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFNUQsTUFBTSxDQUFDLElBQU0sUUFBUSxHQUFHLGNBQU8sT0FBQSxJQUFJLENBQUM7SUFDbEMsSUFBSSxFQUFFLGtCQUFrQjtJQUN4QixXQUFXLEVBQUUsV0FBVztJQUN4QixhQUFhLEVBQUUsbUNBQW1DO0NBQ25ELENBQUMsRUFKNkIsQ0FJN0IsQ0FBQztBQUVILE1BQU0sQ0FBQyxJQUFNLFdBQVcsR0FBRyxjQUFPLE9BQUEsR0FBRyxDQUFDO0lBQ3BDLElBQUksRUFBRSxtQkFBbUI7SUFDekIsV0FBVyxFQUFFLGdCQUFnQjtJQUM3QixhQUFhLEVBQUUsbUNBQW1DO0NBQ25ELENBQUMsRUFKZ0MsQ0FJaEMsQ0FBQztBQUVILE1BQU0sQ0FBQyxJQUFNLFlBQVksR0FBRyxjQUFPLE9BQUEsR0FBRyxDQUFDO0lBQ3JDLElBQUksRUFBRSxvQkFBb0I7SUFDMUIsV0FBVyxFQUFFLGlCQUFpQjtJQUM5QixhQUFhLEVBQUUsbUNBQW1DO0NBQ25ELENBQUMsRUFKaUMsQ0FJakMsQ0FBQztBQUVILE1BQU0sQ0FBQyxJQUFNLFFBQVEsR0FBRyxVQUFDLEVBQU07UUFBSixVQUFFO0lBQVEsT0FBQSxLQUFLLENBQUM7UUFDekMsSUFBSSxFQUFFLFlBQVUsRUFBRSxVQUFPO1FBQ3pCLFdBQVcsRUFBRSxpQkFBaUI7UUFDOUIsYUFBYSxFQUFFLG1DQUFtQztLQUNuRCxDQUFDO0FBSm1DLENBSW5DLENBQUMifQ==
// EXTERNAL MODULE: ./constants/api/game.ts
var game = __webpack_require__(50);

// CONCATENATED MODULE: ./action/game.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return loadGameAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getUserGiftAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getTodayGiftAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return playGameAction; });


var loadGameAction = function () {
    return function (dispatch, getState) {
        return dispatch({
            type: game["c" /* LOAD_GAME */],
            payload: { promise: loadGame().then(function (res) { return res; }) },
        });
    };
};
var getUserGiftAction = function () {
    return function (dispatch, getState) {
        return dispatch({
            type: game["b" /* GET_USER_GIFT */],
            payload: { promise: getUserGift().then(function (res) { return res; }) },
        });
    };
};
var getTodayGiftAction = function () {
    return function (dispatch, getState) {
        return dispatch({
            type: game["a" /* GET_TODAY_GIFT */],
            payload: { promise: getTodayGift().then(function (res) { return res; }) },
        });
    };
};
var playGameAction = function (_a) {
    var id = _a.id;
    return function (dispatch, getState) {
        return dispatch({
            type: game["d" /* PLAY_GAME */],
            payload: { promise: playGame({ id: id }).then(function (res) { return res; }) },
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2FtZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImdhbWUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUEsT0FBTyxFQUNMLFFBQVEsRUFDUixXQUFXLEVBQ1gsWUFBWSxFQUNaLFFBQVEsRUFDVCxNQUFNLGFBQWEsQ0FBQztBQUVyQixPQUFPLEVBQ0wsU0FBUyxFQUNULGFBQWEsRUFDYixjQUFjLEVBQ2QsU0FBUyxFQUNWLE1BQU0sdUJBQXVCLENBQUM7QUFFL0IsTUFBTSxDQUFDLElBQU0sY0FBYyxHQUFHO0lBQzVCLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxTQUFTO1lBQ2YsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtTQUNsRCxDQUFDO0lBSEYsQ0FHRTtBQUpKLENBSUksQ0FBQztBQUVQLE1BQU0sQ0FBQyxJQUFNLGlCQUFpQixHQUFHO0lBQy9CLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSxhQUFhO1lBQ25CLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7U0FDckQsQ0FBQztJQUhGLENBR0U7QUFKSixDQUlJLENBQUM7QUFFUCxNQUFNLENBQUMsSUFBTSxrQkFBa0IsR0FBRztJQUNoQyxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsY0FBYztZQUNwQixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsWUFBWSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1NBQ3RELENBQUM7SUFIRixDQUdFO0FBSkosQ0FJSSxDQUFDO0FBRVAsTUFBTSxDQUFDLElBQU0sY0FBYyxHQUFHLFVBQUMsRUFBSTtRQUFILFVBQUU7SUFDOUIsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLFNBQVM7WUFDZixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDLEVBQUMsRUFBRSxJQUFBLEVBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtTQUN0RCxDQUFDO0lBSEYsQ0FHRTtBQUpKLENBSUksQ0FBQyJ9

/***/ }),

/***/ 881:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./utils/uri.ts
var uri = __webpack_require__(347);

// EXTERNAL MODULE: ./action/game.ts + 1 modules
var game = __webpack_require__(807);

// EXTERNAL MODULE: ./action/meta.ts
var meta = __webpack_require__(754);

// CONCATENATED MODULE: ./container/game/beauty-hunter/step-3/store.tsx


var mapStateToProps = function (state) { return ({
    gameStore: state.game,
    shopStore: state.shop,
    userStore: state.user,
    likeStore: state.like,
    cartStore: state.cart,
    authStore: state.auth,
    provinceStore: state.province,
    trackingStore: state.tracking,
    listLikedId: state.like.liked.id,
    signInStatus: state.auth.signInStatus
}); };
var mapDispatchToProps = function (dispatch) { return ({
    loadGameAction: function () { return dispatch(Object(game["c" /* loadGameAction */])()); },
    getUserGiftAction: function () { return dispatch(Object(game["b" /* getUserGiftAction */])()); },
    getTodayGiftAction: function () { return dispatch(Object(game["a" /* getTodayGiftAction */])()); },
    updateMetaInfoAction: function (data) { return dispatch(Object(meta["a" /* updateMetaInfoAction */])(data)); },
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLGNBQWMsRUFDZCxpQkFBaUIsRUFDakIsa0JBQWtCLEdBQ25CLE1BQU0seUJBQXlCLENBQUM7QUFDakMsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFFL0QsTUFBTSxDQUFDLElBQU0sZUFBZSxHQUFHLFVBQUMsS0FBSyxJQUFLLE9BQUEsQ0FBQztJQUN6QyxTQUFTLEVBQUUsS0FBSyxDQUFDLElBQUk7SUFDckIsU0FBUyxFQUFFLEtBQUssQ0FBQyxJQUFJO0lBQ3JCLFNBQVMsRUFBRSxLQUFLLENBQUMsSUFBSTtJQUNyQixTQUFTLEVBQUUsS0FBSyxDQUFDLElBQUk7SUFDckIsU0FBUyxFQUFFLEtBQUssQ0FBQyxJQUFJO0lBQ3JCLFNBQVMsRUFBRSxLQUFLLENBQUMsSUFBSTtJQUNyQixhQUFhLEVBQUUsS0FBSyxDQUFDLFFBQVE7SUFDN0IsYUFBYSxFQUFFLEtBQUssQ0FBQyxRQUFRO0lBQzdCLFdBQVcsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO0lBQ2hDLFlBQVksRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVk7Q0FDdEMsQ0FBQyxFQVh3QyxDQVd4QyxDQUFDO0FBRUgsTUFBTSxDQUFDLElBQU0sa0JBQWtCLEdBQUcsVUFBQyxRQUFRLElBQUssT0FBQSxDQUFDO0lBQy9DLGNBQWMsRUFBRSxjQUFNLE9BQUEsUUFBUSxDQUFDLGNBQWMsRUFBRSxDQUFDLEVBQTFCLENBQTBCO0lBQ2hELGlCQUFpQixFQUFFLGNBQU0sT0FBQSxRQUFRLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxFQUE3QixDQUE2QjtJQUN0RCxrQkFBa0IsRUFBRSxjQUFNLE9BQUEsUUFBUSxDQUFDLGtCQUFrQixFQUFFLENBQUMsRUFBOUIsQ0FBOEI7SUFDeEQsb0JBQW9CLEVBQUUsVUFBQyxJQUFJLElBQUssT0FBQSxRQUFRLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBcEMsQ0FBb0M7Q0FDckUsQ0FBQyxFQUw4QyxDQUs5QyxDQUFDIn0=
// CONCATENATED MODULE: ./container/game/beauty-hunter/step-3/initialize.tsx
var DEFAULT_PROPS = {};
var INITIAL_STATE = {};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRyxFQUFZLENBQUM7QUFDMUMsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHLEVBQVksQ0FBQyJ9
// EXTERNAL MODULE: ./utils/currency.ts
var currency = __webpack_require__(752);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./container/game/beauty-hunter/step-3/style.tsx


var pattern = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/pattern.png';
var style_top = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/top.png';
var banner = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/banner.png';
var bgr2 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/bgr-2.png';
var diamond = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/diamond.png';
var s1Phone = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s1-phone.png';
var s1Play = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s1-play.png';
var s2Flora = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s2-flora.png';
var s2Time = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s2-time.png';
var s3Info = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s3-info.png';
var s3Share1 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s3-share-1.png';
var s3Share2 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s3-share-2.png';
var s3Share3 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s3-share-3.png';
var s2p1 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s2-p-1.png';
var s2p2 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s2-p-2.png';
var s2p3 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s2-p-3.png';
var s2p4 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s2-p-4.png';
var s3Text = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/game/beauty-hunter-assets/s3-text.png';
/* harmony default export */ var style = ({
    container: {
        position: 'relative',
        background: '#FFFFFF',
        minHeight: '100vh',
        height: '100vh',
        width: '100vw',
        display: 'flex',
        flexDirection: 'column'
    },
    bg: {
        backgroundImage: "url(" + pattern + ")",
        backgroundPosition: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        backgroundSize: '100px 100px',
        opacity: .05,
        zIndex: -1,
    },
    top: {
        backgroundImage: "url(" + style_top + ")",
        backgroundPosition: 'center',
        width: '100%',
        paddingTop: '33.33%',
        backgroundSize: 'cover'
    },
    content: {
        padding: 30
    },
    banner: {
        backgroundImage: "url(" + banner + ")",
        backgroundPosition: 'center',
        width: '100%',
        paddingTop: '46.666%',
        backgroundSize: 'cover',
        marginBottom: 30
    },
    countdown: {
        margin: '0 auto',
        width: 130,
        height: 30,
        textAlign: 'center',
        lineHeight: '30px',
        background: '#FFF',
        marginBottom: 30,
        fontSize: 24,
        fontFamily: 'arial',
        fontWeight: 'bold',
        color: 'orange',
        border: '1px solid orange',
    },
    button: {
        background: '#ffab00',
        color: '#FFF',
        height: 40,
        lineHeight: '38px',
        fontSize: 18,
        textTransform: 'uppercase',
        textAlign: 'center',
        fontWeight: '800',
        borderRadius: 50,
        border: "2px dotted #f24a00",
        width: 220,
        display: 'block',
        margin: "0 auto 10px"
    },
    info: {
        flex: 1,
        position: 'relative',
        zIndex: 0,
        display: 'flex',
        flexDirection: 'column',
        bgLight: {
            backgroundImage: "url(" + bgr2 + ")",
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            position: 'fixed',
            width: '120vh',
            height: '120vh',
            top: '42%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            marginLeft: '-60vh',
            marginTop: '-60vh',
        },
        bgGradient: {
            width: '100%',
            height: '100%',
            position: 'absolute',
        },
        bgFlora: {
            backgroundImage: "url(" + s2Flora + ")",
            backgroundSize: 'contain',
            width: '100%',
            paddingTop: '44%',
            position: 'relative',
            zIndex: 5,
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'bottom center',
        },
        countdown: {
            backgroundImage: "url(" + s2Time + ")",
            backgroundSize: 'contain',
            width: 200,
            height: 70,
            position: 'absolute',
            zIndex: 10,
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'bottom center',
            top: 0,
            left: '50%',
            marginLeft: -100,
            textAlign: 'center',
            color: '#FFF',
            lineHeight: '80px',
            fontSize: '29px',
            fontWeight: '800',
            textShadow: '1px 1px 0px rgba(0,0,0,.25), 1px 1px 2px rgba(0,0,0,.25)'
        },
        content: {
            width: '100vw',
            flex: 1,
            position: 'relative',
            zIndex: 10,
            display: 'flex',
            justifyContent: 'center',
            alignitems: 'center'
        },
        contentInfo: {
            backgroundImage: "url(" + s3Info + ")",
            width: '100%',
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        },
        contentInfoPanel: {
            width: '100%',
            paddingTop: '90%',
            position: 'relative',
            top: 70,
        },
        phone: {
            backgroundImage: "url(" + s1Phone + ")",
            width: 110,
            height: 110,
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            position: 'absolute',
            top: '34%',
            left: '50%',
            marginLeft: -55,
            marginTop: -55
        },
        slogan: {
            backgroundImage: "url(" + s3Text + ")",
            width: 280,
            height: 56,
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            position: 'absolute',
            top: '32%',
            left: '50%',
            marginLeft: -140,
            marginTop: -65
        },
        reward: {
            position: 'absolute',
            top: '32%',
            left: '50%',
            width: 280,
            height: 250,
            marginLeft: -140,
            marginTop: 0,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'center'
        },
        rewardCoin: {
            fontFamily: variable["fontAvenirDemiBold"],
            fontSize: 32,
            color: '#ff95aa',
            textShadow: '1px 1px 0 #FFF, 2px 2px 0 #ff9eb0',
            border: '3px double rgb(255, 149, 170)',
            borderRadius: 10,
            paddingLeft: 15,
            paddingRight: 15,
            marginBottom: 20
        },
        rewardCoinText: {
            fontFamily: variable["fontAvenirDemiBold"],
            paddingLeft: 15,
            paddingRight: 15,
            textAlign: 'center',
            fontSize: 18,
            lineHeight: '24px',
            color: '#ff95aa',
            textShadow: '1px 1px 0 #FFF, 2px 2px 0 #ecd2d7',
        },
        rewardAvatar: {
            maxWidth: '40%',
            marginBottom: 10,
        },
        rewardName: {
            maxWidth: '80%',
            fontFamily: variable["fontAvenirDemiBold"],
            color: '#ff95aa',
            paddingLeft: 15,
            paddingRight: 15,
            textShadow: '1px 1px 0 #FFF, 2px 2px 0 #ecd2d7',
            fontSize: 18,
            textAlign: 'center',
        },
        pig: {
            width: 300,
            height: 300,
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)'
        },
        pig1: {
            backgroundImage: "url(" + s2p1 + ")",
        },
        pig2: {
            backgroundImage: "url(" + s2p2 + ")",
        },
        pig3: {
            backgroundImage: "url(" + s2p3 + ")",
        },
        pig4: {
            backgroundImage: "url(" + s2p4 + ")",
        },
        play: {
            backgroundImage: "url(" + s1Play + ")",
            width: 150,
            height: 50,
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            position: 'absolute',
            bottom: -6,
            left: '50%',
            marginLeft: -75,
            marginTop: -55,
            textAlign: 'center',
            color: '#FFF',
            lineHeight: '50px',
            textTransform: 'uppercase',
            fontWeight: 'bolder',
            letterSpacing: '1px',
            textShadow: '1px 1px 0px rgba(0,0,0,0.75), 1px 1px 1px rgba(0,0,0,0.15)',
        },
        playAgain: {
            width: 200,
            height: 24,
            position: 'absolute',
            bottom: -46,
            left: '50%',
            marginLeft: -100,
            marginTop: -55,
            textAlign: 'center',
            lineHeight: '22px',
            fontWeight: 'bolder',
            letterSpacing: '1px',
            backgroundColor: '#e8f4fb',
            color: 'rgb(45, 123, 160)',
            boxShadow: 'rgb(81, 196, 238) 0px 0px 0px 2px, 0 0 0 2.5px #1682ab',
            fontSize: 12,
            borderRadius: 12,
        },
        social: {
            display: 'flex',
            justifyContent: 'flex-start',
            alignItems: 'center',
            position: 'absolute',
            right: 0,
            width: '84%',
            bottom: 32,
        },
        socialItem: {
            width: 50,
            height: 50,
            margin: 5,
            backgroundSize: 'contain'
        },
        socialItemFb: {
            backgroundImage: "url(" + s3Share1 + ")"
        },
        socialItemTw: {
            backgroundImage: "url(" + s3Share2 + ")"
        },
        socialItemGp: {
            backgroundImage: "url(" + s3Share3 + ")"
        },
        diamond: {
            position: 'absolute',
            backgroundImage: "url(" + diamond + ")",
            width: 50,
            height: 50,
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            zIndex: 10,
        },
        diamond1: {
            top: '14%',
            right: '13%',
            transform: 'scale(.8)'
        },
        diamond2: {
            top: '19%',
            left: '21%',
            transform: 'scale(.8) rotate(-45deg)',
        },
        diamond3: {
            bottom: '10%',
            right: '47%',
            transform: 'scale(1.5) rotate(-45deg)',
        },
        diamond4: {
            top: '37%',
            left: '1%',
            transform: 'scale(1.2)',
        },
        diamond5: {
            left: '15%',
            bottom: '32%',
            transform: 'scale(1.3) rotate(10deg)'
        },
    },
    gift: {
        background: '#FFF',
        height: 90,
        position: 'relative',
        zIndex: 5,
        tab: {
            position: 'absolute',
            top: -40,
            left: 0,
            width: '100%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            paddingLeft: 10,
            paddingRight: 10,
        },
        tabItem: {
            height: 40,
            display: 'flex',
            flex: 1,
            marginLeft: 2,
            marginRight: 2,
            justifyContent: 'center',
            alignItems: 'center',
            border: "2px solid #e7c1d1",
            borderRadius: '6px 6px 0 0',
            background: '#f1d5e1',
            position: 'relative',
            zIndex: 1,
            boxShadow: '1px 1px 0 .5px rgba(255,255,255,.5) inset'
        },
        tabActive: {
            boxShadow: 'none',
            background: '#FFFFFF',
            borderBottom: "none",
        },
        tabImage: {
            height: 20,
            width: 'auto',
        },
        tabLine: {
            zIndex: 0,
            background: '#e7c1d1',
            left: 0,
            bottom: 0,
            position: 'absolute',
            width: '100%',
            height: 2,
        },
        list: {
            width: '100%',
            overflowX: 'scroll',
            overflowY: 'hidden',
        },
        listPanel: {
            whiteSpace: 'nowrap',
            padding: 10,
        },
        listItem: {
            display: 'inline-block',
            width: 50,
            height: 50,
            margin: 10,
            boxShadow: '1px 1px 0 .5px inset #ffffff, 0 0 0px 2px #f1d5e1, 1px 1px 0 2px #e7c1d1',
            borderRadius: 4,
            background: '#f7e6ed',
        },
        lastItem: {
            marginRight: 20
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSw0QkFBNEIsQ0FBQztBQUd2RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUUxRCxJQUFNLE9BQU8sR0FBRyxpQkFBaUIsR0FBRyxzREFBc0QsQ0FBQztBQUMzRixJQUFNLEdBQUcsR0FBRyxpQkFBaUIsR0FBRyxrREFBa0QsQ0FBQztBQUNuRixJQUFNLE1BQU0sR0FBRyxpQkFBaUIsR0FBRyxxREFBcUQsQ0FBQztBQUV6RixJQUFNLElBQUksR0FBRyxpQkFBaUIsR0FBRyxvREFBb0QsQ0FBQztBQUN0RixJQUFNLE9BQU8sR0FBRyxpQkFBaUIsR0FBRyxzREFBc0QsQ0FBQztBQUMzRixJQUFNLE9BQU8sR0FBRyxpQkFBaUIsR0FBRyx1REFBdUQsQ0FBQztBQUM1RixJQUFNLE1BQU0sR0FBRyxpQkFBaUIsR0FBRyxzREFBc0QsQ0FBQztBQUMxRixJQUFNLE9BQU8sR0FBRyxpQkFBaUIsR0FBRyx1REFBdUQsQ0FBQztBQUM1RixJQUFNLE1BQU0sR0FBRyxpQkFBaUIsR0FBRyxzREFBc0QsQ0FBQztBQUMxRixJQUFNLE1BQU0sR0FBRyxpQkFBaUIsR0FBRyxzREFBc0QsQ0FBQztBQUMxRixJQUFNLFFBQVEsR0FBRyxpQkFBaUIsR0FBRyx5REFBeUQsQ0FBQztBQUMvRixJQUFNLFFBQVEsR0FBRyxpQkFBaUIsR0FBRyx5REFBeUQsQ0FBQztBQUMvRixJQUFNLFFBQVEsR0FBRyxpQkFBaUIsR0FBRyx5REFBeUQsQ0FBQztBQUMvRixJQUFNLElBQUksR0FBRyxpQkFBaUIsR0FBRyxxREFBcUQsQ0FBQztBQUN2RixJQUFNLElBQUksR0FBRyxpQkFBaUIsR0FBRyxxREFBcUQsQ0FBQztBQUN2RixJQUFNLElBQUksR0FBRyxpQkFBaUIsR0FBRyxxREFBcUQsQ0FBQztBQUN2RixJQUFNLElBQUksR0FBRyxpQkFBaUIsR0FBRyxxREFBcUQsQ0FBQztBQUN2RixJQUFNLE1BQU0sR0FBRyxpQkFBaUIsR0FBRyxzREFBc0QsQ0FBQztBQUUxRixlQUFlO0lBQ2IsU0FBUyxFQUFFO1FBQ1QsUUFBUSxFQUFFLFVBQVU7UUFDcEIsVUFBVSxFQUFFLFNBQVM7UUFDckIsU0FBUyxFQUFFLE9BQU87UUFDbEIsTUFBTSxFQUFFLE9BQU87UUFDZixLQUFLLEVBQUUsT0FBTztRQUNkLE9BQU8sRUFBRSxNQUFNO1FBQ2YsYUFBYSxFQUFFLFFBQVE7S0FDeEI7SUFFRCxFQUFFLEVBQUU7UUFDRixlQUFlLEVBQUUsU0FBTyxPQUFPLE1BQUc7UUFDbEMsa0JBQWtCLEVBQUUsUUFBUTtRQUM1QixRQUFRLEVBQUUsVUFBVTtRQUNwQixHQUFHLEVBQUUsQ0FBQztRQUNOLElBQUksRUFBRSxDQUFDO1FBQ1AsS0FBSyxFQUFFLE1BQU07UUFDYixNQUFNLEVBQUUsTUFBTTtRQUNkLGNBQWMsRUFBRSxhQUFhO1FBQzdCLE9BQU8sRUFBRSxHQUFHO1FBQ1osTUFBTSxFQUFFLENBQUMsQ0FBQztLQUNYO0lBRUQsR0FBRyxFQUFFO1FBQ0gsZUFBZSxFQUFFLFNBQU8sR0FBRyxNQUFHO1FBQzlCLGtCQUFrQixFQUFFLFFBQVE7UUFDNUIsS0FBSyxFQUFFLE1BQU07UUFDYixVQUFVLEVBQUUsUUFBUTtRQUNwQixjQUFjLEVBQUUsT0FBTztLQUN4QjtJQUVELE9BQU8sRUFBRTtRQUNMLE9BQU8sRUFBRSxFQUFFO0tBQ2Q7SUFFRCxNQUFNLEVBQUU7UUFDTixlQUFlLEVBQUUsU0FBTyxNQUFNLE1BQUc7UUFDakMsa0JBQWtCLEVBQUUsUUFBUTtRQUM1QixLQUFLLEVBQUUsTUFBTTtRQUNiLFVBQVUsRUFBRSxTQUFTO1FBQ3JCLGNBQWMsRUFBRSxPQUFPO1FBQ3ZCLFlBQVksRUFBRSxFQUFFO0tBQ2pCO0lBRUQsU0FBUyxFQUFFO1FBQ1QsTUFBTSxFQUFFLFFBQVE7UUFDaEIsS0FBSyxFQUFFLEdBQUc7UUFDVixNQUFNLEVBQUUsRUFBRTtRQUNWLFNBQVMsRUFBRSxRQUFRO1FBQ25CLFVBQVUsRUFBRSxNQUFNO1FBQ2xCLFVBQVUsRUFBRSxNQUFNO1FBQ2xCLFlBQVksRUFBRSxFQUFFO1FBQ2hCLFFBQVEsRUFBRSxFQUFFO1FBQ1osVUFBVSxFQUFFLE9BQU87UUFDbkIsVUFBVSxFQUFFLE1BQU07UUFDbEIsS0FBSyxFQUFFLFFBQVE7UUFDZixNQUFNLEVBQUUsa0JBQWtCO0tBQzNCO0lBRUQsTUFBTSxFQUFFO1FBQ04sVUFBVSxFQUFFLFNBQVM7UUFDckIsS0FBSyxFQUFFLE1BQU07UUFDYixNQUFNLEVBQUUsRUFBRTtRQUNWLFVBQVUsRUFBRSxNQUFNO1FBQ2xCLFFBQVEsRUFBRSxFQUFFO1FBQ1osYUFBYSxFQUFFLFdBQVc7UUFDMUIsU0FBUyxFQUFFLFFBQVE7UUFDbkIsVUFBVSxFQUFFLEtBQUs7UUFDakIsWUFBWSxFQUFFLEVBQUU7UUFDaEIsTUFBTSxFQUFFLG9CQUFvQjtRQUM1QixLQUFLLEVBQUUsR0FBRztRQUNWLE9BQU8sRUFBRSxPQUFPO1FBQ2hCLE1BQU0sRUFBRSxhQUFhO0tBQ3RCO0lBRUQsSUFBSSxFQUFFO1FBQ0osSUFBSSxFQUFFLENBQUM7UUFDUCxRQUFRLEVBQUUsVUFBVTtRQUNwQixNQUFNLEVBQUUsQ0FBQztRQUNULE9BQU8sRUFBRSxNQUFNO1FBQ2YsYUFBYSxFQUFFLFFBQVE7UUFFdkIsT0FBTyxFQUFFO1lBQ1AsZUFBZSxFQUFFLFNBQU8sSUFBSSxNQUFHO1lBQy9CLGtCQUFrQixFQUFFLFFBQVE7WUFDNUIsY0FBYyxFQUFFLE9BQU87WUFDdkIsUUFBUSxFQUFFLE9BQU87WUFDakIsS0FBSyxFQUFFLE9BQU87WUFDZCxNQUFNLEVBQUUsT0FBTztZQUNmLEdBQUcsRUFBRSxLQUFLO1lBQ1YsSUFBSSxFQUFFLEtBQUs7WUFDWCxTQUFTLEVBQUUsdUJBQXVCO1lBQ2xDLFVBQVUsRUFBRSxPQUFPO1lBQ25CLFNBQVMsRUFBRSxPQUFPO1NBQ25CO1FBRUQsVUFBVSxFQUFFO1lBQ1YsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsTUFBTTtZQUNkLFFBQVEsRUFBRSxVQUFVO1NBQ3JCO1FBRUQsT0FBTyxFQUFFO1lBQ1AsZUFBZSxFQUFFLFNBQU8sT0FBTyxNQUFHO1lBQ2xDLGNBQWMsRUFBRSxTQUFTO1lBQ3pCLEtBQUssRUFBRSxNQUFNO1lBQ2IsVUFBVSxFQUFFLEtBQUs7WUFDakIsUUFBUSxFQUFFLFVBQVU7WUFDcEIsTUFBTSxFQUFFLENBQUM7WUFDVCxnQkFBZ0IsRUFBRSxXQUFXO1lBQzdCLGtCQUFrQixFQUFFLGVBQWU7U0FDcEM7UUFFRCxTQUFTLEVBQUU7WUFDVCxlQUFlLEVBQUUsU0FBTyxNQUFNLE1BQUc7WUFDakMsY0FBYyxFQUFFLFNBQVM7WUFDekIsS0FBSyxFQUFFLEdBQUc7WUFDVixNQUFNLEVBQUUsRUFBRTtZQUNWLFFBQVEsRUFBRSxVQUFVO1lBQ3BCLE1BQU0sRUFBRSxFQUFFO1lBQ1YsZ0JBQWdCLEVBQUUsV0FBVztZQUM3QixrQkFBa0IsRUFBRSxlQUFlO1lBQ25DLEdBQUcsRUFBRSxDQUFDO1lBQ04sSUFBSSxFQUFFLEtBQUs7WUFDWCxVQUFVLEVBQUUsQ0FBQyxHQUFHO1lBQ2hCLFNBQVMsRUFBRSxRQUFRO1lBQ25CLEtBQUssRUFBRSxNQUFNO1lBQ2IsVUFBVSxFQUFFLE1BQU07WUFDbEIsUUFBUSxFQUFFLE1BQU07WUFDaEIsVUFBVSxFQUFFLEtBQUs7WUFDakIsVUFBVSxFQUFFLDBEQUEwRDtTQUN2RTtRQUVELE9BQU8sRUFBRTtZQUNQLEtBQUssRUFBRSxPQUFPO1lBQ2QsSUFBSSxFQUFFLENBQUM7WUFDUCxRQUFRLEVBQUUsVUFBVTtZQUNwQixNQUFNLEVBQUUsRUFBRTtZQUNWLE9BQU8sRUFBRSxNQUFNO1lBQ2YsY0FBYyxFQUFFLFFBQVE7WUFDeEIsVUFBVSxFQUFFLFFBQVE7U0FDckI7UUFFRCxXQUFXLEVBQUU7WUFDWCxlQUFlLEVBQUUsU0FBTyxNQUFNLE1BQUc7WUFDakMsS0FBSyxFQUFFLE1BQU07WUFDYixjQUFjLEVBQUUsU0FBUztZQUN6QixnQkFBZ0IsRUFBRSxXQUFXO1lBQzdCLGtCQUFrQixFQUFFLFFBQVE7WUFDNUIsT0FBTyxFQUFFLE1BQU07WUFDZixjQUFjLEVBQUUsUUFBUTtZQUN4QixVQUFVLEVBQUUsUUFBUTtTQUNyQjtRQUVELGdCQUFnQixFQUFFO1lBQ2hCLEtBQUssRUFBRSxNQUFNO1lBQ2IsVUFBVSxFQUFFLEtBQUs7WUFDakIsUUFBUSxFQUFFLFVBQVU7WUFDcEIsR0FBRyxFQUFFLEVBQUU7U0FDUjtRQUVELEtBQUssRUFBRTtZQUNMLGVBQWUsRUFBRSxTQUFPLE9BQU8sTUFBRztZQUNsQyxLQUFLLEVBQUUsR0FBRztZQUNWLE1BQU0sRUFBRSxHQUFHO1lBQ1gsY0FBYyxFQUFFLFNBQVM7WUFDekIsZ0JBQWdCLEVBQUUsV0FBVztZQUM3QixrQkFBa0IsRUFBRSxRQUFRO1lBQzVCLFFBQVEsRUFBRSxVQUFVO1lBQ3BCLEdBQUcsRUFBRSxLQUFLO1lBQ1YsSUFBSSxFQUFFLEtBQUs7WUFDWCxVQUFVLEVBQUUsQ0FBQyxFQUFFO1lBQ2YsU0FBUyxFQUFFLENBQUMsRUFBRTtTQUNmO1FBRUQsTUFBTSxFQUFFO1lBQ04sZUFBZSxFQUFFLFNBQU8sTUFBTSxNQUFHO1lBQ2pDLEtBQUssRUFBRSxHQUFHO1lBQ1YsTUFBTSxFQUFFLEVBQUU7WUFDVixjQUFjLEVBQUUsU0FBUztZQUN6QixnQkFBZ0IsRUFBRSxXQUFXO1lBQzdCLGtCQUFrQixFQUFFLFFBQVE7WUFDNUIsUUFBUSxFQUFFLFVBQVU7WUFDcEIsR0FBRyxFQUFFLEtBQUs7WUFDVixJQUFJLEVBQUUsS0FBSztZQUNYLFVBQVUsRUFBRSxDQUFDLEdBQUc7WUFDaEIsU0FBUyxFQUFFLENBQUMsRUFBRTtTQUNmO1FBRUQsTUFBTSxFQUFFO1lBQ04sUUFBUSxFQUFFLFVBQVU7WUFDcEIsR0FBRyxFQUFFLEtBQUs7WUFDVixJQUFJLEVBQUUsS0FBSztZQUNYLEtBQUssRUFBRSxHQUFHO1lBQ1YsTUFBTSxFQUFFLEdBQUc7WUFDWCxVQUFVLEVBQUUsQ0FBQyxHQUFHO1lBQ2hCLFNBQVMsRUFBRSxDQUFDO1lBQ1osT0FBTyxFQUFFLE1BQU07WUFDZixhQUFhLEVBQUUsUUFBUTtZQUN2QixjQUFjLEVBQUUsWUFBWTtZQUM1QixVQUFVLEVBQUUsUUFBUTtTQUNyQjtRQUVELFVBQVUsRUFBRTtZQUNWLFVBQVUsRUFBRSxRQUFRLENBQUMsa0JBQWtCO1lBQ3ZDLFFBQVEsRUFBRSxFQUFFO1lBQ1osS0FBSyxFQUFFLFNBQVM7WUFDaEIsVUFBVSxFQUFFLG1DQUFtQztZQUMvQyxNQUFNLEVBQUUsK0JBQStCO1lBQ3ZDLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLEVBQUU7WUFDaEIsWUFBWSxFQUFFLEVBQUU7U0FDakI7UUFFRCxjQUFjLEVBQUU7WUFDZCxVQUFVLEVBQUUsUUFBUSxDQUFDLGtCQUFrQjtZQUN2QyxXQUFXLEVBQUUsRUFBRTtZQUNmLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFNBQVMsRUFBRSxRQUFRO1lBQ25CLFFBQVEsRUFBRSxFQUFFO1lBQ1osVUFBVSxFQUFFLE1BQU07WUFDbEIsS0FBSyxFQUFFLFNBQVM7WUFDaEIsVUFBVSxFQUFFLG1DQUFtQztTQUNoRDtRQUVELFlBQVksRUFBRTtZQUNaLFFBQVEsRUFBRSxLQUFLO1lBQ2YsWUFBWSxFQUFFLEVBQUU7U0FDakI7UUFFRCxVQUFVLEVBQUU7WUFDVixRQUFRLEVBQUUsS0FBSztZQUNmLFVBQVUsRUFBRSxRQUFRLENBQUMsa0JBQWtCO1lBQ3ZDLEtBQUssRUFBRSxTQUFTO1lBQ2hCLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLEVBQUU7WUFDaEIsVUFBVSxFQUFFLG1DQUFtQztZQUMvQyxRQUFRLEVBQUUsRUFBRTtZQUNaLFNBQVMsRUFBRSxRQUFRO1NBQ3BCO1FBRUQsR0FBRyxFQUFFO1lBQ0gsS0FBSyxFQUFFLEdBQUc7WUFDVixNQUFNLEVBQUUsR0FBRztZQUNYLGNBQWMsRUFBRSxTQUFTO1lBQ3pCLGdCQUFnQixFQUFFLFdBQVc7WUFDN0Isa0JBQWtCLEVBQUUsUUFBUTtZQUM1QixRQUFRLEVBQUUsVUFBVTtZQUNwQixHQUFHLEVBQUUsS0FBSztZQUNWLElBQUksRUFBRSxLQUFLO1lBQ1gsU0FBUyxFQUFFLHVCQUF1QjtTQUNuQztRQUVELElBQUksRUFBRTtZQUNKLGVBQWUsRUFBRSxTQUFPLElBQUksTUFBRztTQUNoQztRQUVELElBQUksRUFBRTtZQUNKLGVBQWUsRUFBRSxTQUFPLElBQUksTUFBRztTQUNoQztRQUVELElBQUksRUFBRTtZQUNKLGVBQWUsRUFBRSxTQUFPLElBQUksTUFBRztTQUNoQztRQUVELElBQUksRUFBRTtZQUNKLGVBQWUsRUFBRSxTQUFPLElBQUksTUFBRztTQUNoQztRQUVELElBQUksRUFBRTtZQUNKLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztZQUNqQyxLQUFLLEVBQUUsR0FBRztZQUNWLE1BQU0sRUFBRSxFQUFFO1lBQ1YsY0FBYyxFQUFFLFNBQVM7WUFDekIsZ0JBQWdCLEVBQUUsV0FBVztZQUM3QixrQkFBa0IsRUFBRSxRQUFRO1lBQzVCLFFBQVEsRUFBRSxVQUFVO1lBQ3BCLE1BQU0sRUFBRSxDQUFDLENBQUM7WUFDVixJQUFJLEVBQUUsS0FBSztZQUNYLFVBQVUsRUFBRSxDQUFDLEVBQUU7WUFDZixTQUFTLEVBQUUsQ0FBQyxFQUFFO1lBQ2QsU0FBUyxFQUFFLFFBQVE7WUFDbkIsS0FBSyxFQUFFLE1BQU07WUFDYixVQUFVLEVBQUUsTUFBTTtZQUNsQixhQUFhLEVBQUUsV0FBVztZQUMxQixVQUFVLEVBQUUsUUFBUTtZQUNwQixhQUFhLEVBQUUsS0FBSztZQUNwQixVQUFVLEVBQUUsNERBQTREO1NBQ3pFO1FBQ0QsU0FBUyxFQUFFO1lBQ1QsS0FBSyxFQUFFLEdBQUc7WUFDVixNQUFNLEVBQUUsRUFBRTtZQUNWLFFBQVEsRUFBRSxVQUFVO1lBQ3BCLE1BQU0sRUFBRSxDQUFDLEVBQUU7WUFDWCxJQUFJLEVBQUUsS0FBSztZQUNYLFVBQVUsRUFBRSxDQUFDLEdBQUc7WUFDaEIsU0FBUyxFQUFFLENBQUMsRUFBRTtZQUNkLFNBQVMsRUFBRSxRQUFRO1lBQ25CLFVBQVUsRUFBRSxNQUFNO1lBQ2xCLFVBQVUsRUFBRSxRQUFRO1lBQ3BCLGFBQWEsRUFBRSxLQUFLO1lBQ3BCLGVBQWUsRUFBRSxTQUFTO1lBQzFCLEtBQUssRUFBRSxtQkFBbUI7WUFDMUIsU0FBUyxFQUFFLHdEQUF3RDtZQUNuRSxRQUFRLEVBQUUsRUFBRTtZQUNaLFlBQVksRUFBRSxFQUFFO1NBQ2pCO1FBQ0QsTUFBTSxFQUFFO1lBQ04sT0FBTyxFQUFFLE1BQU07WUFDZixjQUFjLEVBQUUsWUFBWTtZQUM1QixVQUFVLEVBQUUsUUFBUTtZQUNwQixRQUFRLEVBQUUsVUFBVTtZQUNwQixLQUFLLEVBQUUsQ0FBQztZQUNSLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLEVBQUU7U0FDWDtRQUNELFVBQVUsRUFBRTtZQUNWLEtBQUssRUFBRSxFQUFFO1lBQ1QsTUFBTSxFQUFFLEVBQUU7WUFDVixNQUFNLEVBQUUsQ0FBQztZQUNULGNBQWMsRUFBRSxTQUFTO1NBQzFCO1FBQ0QsWUFBWSxFQUFFO1lBQ1osZUFBZSxFQUFFLFNBQU8sUUFBUSxNQUFHO1NBQ3BDO1FBQ0QsWUFBWSxFQUFFO1lBQ1osZUFBZSxFQUFFLFNBQU8sUUFBUSxNQUFHO1NBQ3BDO1FBQ0QsWUFBWSxFQUFFO1lBQ1osZUFBZSxFQUFFLFNBQU8sUUFBUSxNQUFHO1NBQ3BDO1FBRUQsT0FBTyxFQUFFO1lBQ1AsUUFBUSxFQUFFLFVBQVU7WUFDcEIsZUFBZSxFQUFFLFNBQU8sT0FBTyxNQUFHO1lBQ2xDLEtBQUssRUFBRSxFQUFFO1lBQ1QsTUFBTSxFQUFFLEVBQUU7WUFDVixjQUFjLEVBQUUsU0FBUztZQUN6QixnQkFBZ0IsRUFBRSxXQUFXO1lBQzdCLGtCQUFrQixFQUFFLFFBQVE7WUFDNUIsTUFBTSxFQUFFLEVBQUU7U0FDWDtRQUNELFFBQVEsRUFBRTtZQUNSLEdBQUcsRUFBRSxLQUFLO1lBQ1YsS0FBSyxFQUFFLEtBQUs7WUFDWixTQUFTLEVBQUUsV0FBVztTQUN2QjtRQUNELFFBQVEsRUFBRTtZQUNSLEdBQUcsRUFBRSxLQUFLO1lBQ1YsSUFBSSxFQUFFLEtBQUs7WUFDWCxTQUFTLEVBQUUsMEJBQTBCO1NBQ3RDO1FBQ0QsUUFBUSxFQUFFO1lBQ1IsTUFBTSxFQUFFLEtBQUs7WUFDYixLQUFLLEVBQUUsS0FBSztZQUNaLFNBQVMsRUFBRSwyQkFBMkI7U0FDdkM7UUFDRCxRQUFRLEVBQUU7WUFDUixHQUFHLEVBQUUsS0FBSztZQUNWLElBQUksRUFBRSxJQUFJO1lBQ1YsU0FBUyxFQUFFLFlBQVk7U0FDeEI7UUFDRCxRQUFRLEVBQUU7WUFDUixJQUFJLEVBQUUsS0FBSztZQUNYLE1BQU0sRUFBRSxLQUFLO1lBQ2IsU0FBUyxFQUFFLDBCQUEwQjtTQUN0QztLQUNGO0lBRUQsSUFBSSxFQUFFO1FBQ0osVUFBVSxFQUFFLE1BQU07UUFDbEIsTUFBTSxFQUFFLEVBQUU7UUFDVixRQUFRLEVBQUUsVUFBVTtRQUNwQixNQUFNLEVBQUUsQ0FBQztRQUVULEdBQUcsRUFBRTtZQUNILFFBQVEsRUFBRSxVQUFVO1lBQ3BCLEdBQUcsRUFBRSxDQUFDLEVBQUU7WUFDUixJQUFJLEVBQUUsQ0FBQztZQUNQLEtBQUssRUFBRSxNQUFNO1lBQ2IsT0FBTyxFQUFFLE1BQU07WUFDZixjQUFjLEVBQUUsUUFBUTtZQUN4QixVQUFVLEVBQUUsUUFBUTtZQUNwQixXQUFXLEVBQUUsRUFBRTtZQUNmLFlBQVksRUFBRSxFQUFFO1NBQ2pCO1FBRUQsT0FBTyxFQUFFO1lBQ1AsTUFBTSxFQUFFLEVBQUU7WUFDVixPQUFPLEVBQUUsTUFBTTtZQUNmLElBQUksRUFBRSxDQUFDO1lBQ1AsVUFBVSxFQUFFLENBQUM7WUFDYixXQUFXLEVBQUUsQ0FBQztZQUNkLGNBQWMsRUFBRSxRQUFRO1lBQ3hCLFVBQVUsRUFBRSxRQUFRO1lBQ3BCLE1BQU0sRUFBRSxtQkFBbUI7WUFDM0IsWUFBWSxFQUFFLGFBQWE7WUFDM0IsVUFBVSxFQUFFLFNBQVM7WUFDckIsUUFBUSxFQUFFLFVBQVU7WUFDcEIsTUFBTSxFQUFFLENBQUM7WUFDVCxTQUFTLEVBQUUsMkNBQTJDO1NBQ3ZEO1FBRUQsU0FBUyxFQUFFO1lBQ1QsU0FBUyxFQUFFLE1BQU07WUFDakIsVUFBVSxFQUFFLFNBQVM7WUFDckIsWUFBWSxFQUFFLE1BQU07U0FDckI7UUFFRCxRQUFRLEVBQUU7WUFDUixNQUFNLEVBQUUsRUFBRTtZQUNWLEtBQUssRUFBRSxNQUFNO1NBQ2Q7UUFFRCxPQUFPLEVBQUU7WUFDUCxNQUFNLEVBQUUsQ0FBQztZQUNULFVBQVUsRUFBRSxTQUFTO1lBQ3JCLElBQUksRUFBRSxDQUFDO1lBQ1AsTUFBTSxFQUFFLENBQUM7WUFDVCxRQUFRLEVBQUUsVUFBVTtZQUNwQixLQUFLLEVBQUUsTUFBTTtZQUNiLE1BQU0sRUFBRSxDQUFDO1NBQ1Y7UUFFRCxJQUFJLEVBQUU7WUFDSixLQUFLLEVBQUUsTUFBTTtZQUNiLFNBQVMsRUFBRSxRQUFRO1lBQ25CLFNBQVMsRUFBRSxRQUFRO1NBQ3BCO1FBQ0QsU0FBUyxFQUFFO1lBQ1QsVUFBVSxFQUFFLFFBQVE7WUFDcEIsT0FBTyxFQUFFLEVBQUU7U0FDWjtRQUNELFFBQVEsRUFBRTtZQUNSLE9BQU8sRUFBRSxjQUFjO1lBQ3ZCLEtBQUssRUFBRSxFQUFFO1lBQ1QsTUFBTSxFQUFFLEVBQUU7WUFDVixNQUFNLEVBQUUsRUFBRTtZQUNWLFNBQVMsRUFBRSwwRUFBMEU7WUFDckYsWUFBWSxFQUFFLENBQUM7WUFDZixVQUFVLEVBQUUsU0FBUztTQUN0QjtRQUNELFFBQVEsRUFBRTtZQUNSLFdBQVcsRUFBRSxFQUFFO1NBQ2hCO0tBQ0Y7Q0FDTSxDQUFDIn0=
// CONCATENATED MODULE: ./container/game/beauty-hunter/step-3/view.tsx




var item = function () { return (react["createElement"]("div", { style: { background: '#FFF', width: '100%', padding: 10, borderBottom: '1px solid #eee', display: 'flex', justifyContent: 'center', alignItems: 'center' } },
    react["createElement"]("img", { style: { width: 60 }, src: "https://upload.lixibox.com/system/pictures/files/000/026/092/medium/1523329786.jpg" }),
    react["createElement"]("span", { style: { fontSize: 14 } }, "Lixibox Bamboo Charcoal Oil Control Paper"))); };
var renderView = function (_a) {
    var props = _a.props, state = _a.state, handleBack = _a.handleBack;
    var playGame = props.gameStore.playGame;
    return (react["createElement"]("div", { style: style.container },
        react["createElement"]("link", { rel: 'stylesheet', type: 'text/css', href: 'http://lxbtest.tk/assets/css/game.css' }),
        react["createElement"]("div", { style: style.info },
            react["createElement"]("div", { className: 'bgrgame', style: style.info.bgLight }),
            react["createElement"]("div", { className: 'gamegrd', style: style.info.bgGradient }),
            react["createElement"]("div", { style: style.info.content },
                react["createElement"]("div", { style: style.info.contentInfo },
                    react["createElement"]("div", { style: style.info.contentInfoPanel },
                        react["createElement"]("div", { style: style.info.slogan }),
                        !!playGame && !!playGame.reward && !!playGame.reward.reward_type
                            && ('coin' === playGame.reward.reward_type || 'balance' === playGame.reward.reward_type)
                            && (react["createElement"]("div", { style: style.info.reward },
                                react["createElement"]("img", { src: uri["a" /* CDN_ASSETS_PREFIX */] + "/assets/images/game/beauty-hunter-assets/" + playGame.reward.reward_type + "-" + playGame.reward.value + ".png", style: style.info.rewardAvatar }),
                                react["createElement"]("div", { style: style.info.rewardName },
                                    "Ch\u00FAc m\u1EEBng b\u1EA1n \u0111\u00E3 nh\u1EADn \u0111\u01B0\u1EE3c ",
                                    Object(currency["a" /* currenyFormat */])(playGame.reward.value),
                                    " ",
                                    'coin' === playGame.reward.reward_type && 'lixicoin',
                                    'balance' === playGame.reward.reward_type && 'vnđ'))),
                        !!playGame && !!playGame.reward && !!playGame.reward.reward_type
                            && 'gift' === playGame.reward.reward_type
                            && (react["createElement"]("div", { style: style.info.reward },
                                react["createElement"]("img", { src: playGame.reward.linked_object.box_basic.primary_picture.medium_url, style: style.info.rewardAvatar }),
                                playGame.reward.linked_object.box_basic.slug.indexOf('vnd') < 0
                                    ? react["createElement"]("div", { style: style.info.rewardName },
                                        playGame.reward.linked_object.box_basic.name,
                                        " ",
                                        ' trị giá ',
                                        " ",
                                        Object(currency["a" /* currenyFormat */])(playGame.reward.linked_object.box_basic.price))
                                    : react["createElement"]("div", { style: style.info.rewardName }, playGame.reward.linked_object.box_basic.name))),
                        react["createElement"]("div", { style: style.info.social }),
                        react["createElement"]("div", { className: 'play', style: style.info.play, onClick: function () { return handleBack(); } }, "Qu\u00E0 \u0111\u00E3 nh\u1EADn"),
                        react["createElement"]("div", { style: style.info.playAgain }, "H\u1EB9n g\u1EB7p l\u1EA1i v\u00E0o ng\u00E0y mai")))),
            react["createElement"]("div", { style: style.info.bgFlora }),
            react["createElement"]("div", { className: 'diamond', style: [style.info.diamond, style.info.diamond1] }),
            react["createElement"]("div", { className: 'diamond', style: [style.info.diamond, style.info.diamond2] }),
            react["createElement"]("div", { className: 'diamond', style: [style.info.diamond, style.info.diamond3] }),
            react["createElement"]("div", { className: 'diamond', style: [style.info.diamond, style.info.diamond4] }),
            react["createElement"]("div", { className: 'diamond', style: [style.info.diamond, style.info.diamond5] }),
            react["createElement"]("div", { style: { width: 0, height: 0, opacity: 0, visibility: 'hidden', overflow: 'hidden' } }))));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRS9CLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzFELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUMzRCxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFHNUIsSUFBTSxJQUFJLEdBQUcsY0FBTSxPQUFBLENBQ2pCLDZCQUFLLEtBQUssRUFBRSxFQUFLLFVBQVUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLFlBQVksRUFBRSxnQkFBZ0IsRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLGNBQWMsRUFBRSxRQUFRLEVBQUMsVUFBVSxFQUFFLFFBQVEsRUFBQztJQUM5Siw2QkFBSyxLQUFLLEVBQUUsRUFBQyxLQUFLLEVBQUUsRUFBRSxFQUFDLEVBQUUsR0FBRyxFQUFDLG9GQUFvRixHQUFHO0lBQ3BILDhCQUFNLEtBQUssRUFBRSxFQUFDLFFBQVEsRUFBRSxFQUFFLEVBQUMsZ0RBQWtELENBQ3pFLENBQ1AsRUFMa0IsQ0FLbEIsQ0FBQTtBQUVELElBQU0sVUFBVSxHQUFHLFVBQUMsRUFJbkI7UUFIQyxnQkFBSyxFQUNMLGdCQUFLLEVBQ0wsMEJBQVU7SUFFVSxJQUFBLG1DQUFRLENBQVc7SUFDdkMsTUFBTSxDQUFDLENBQ0wsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTO1FBQ3pCLDhCQUFNLEdBQUcsRUFBQyxZQUFZLEVBQUMsSUFBSSxFQUFDLFVBQVUsRUFBQyxJQUFJLEVBQUMsNkNBQTZDLEdBQUc7UUFDNUYsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJO1lBQ3BCLDZCQUFLLFNBQVMsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFRO1lBQzVELDZCQUFLLFNBQVMsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFRO1lBQy9ELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU87Z0JBQzVCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLFdBQVc7b0JBQ2hDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLGdCQUFnQjt3QkFDckMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFRO3dCQUVuQyxDQUFDLENBQUMsUUFBUSxJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFdBQVc7K0JBQzdELENBQUMsTUFBTSxLQUFLLFFBQVEsQ0FBQyxNQUFNLENBQUMsV0FBVyxJQUFJLFNBQVMsS0FBSyxRQUFRLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQzsrQkFDckYsQ0FDRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNO2dDQUMzQiw2QkFDRSxHQUFHLEVBQUssaUJBQWlCLGlEQUE0QyxRQUFRLENBQUMsTUFBTSxDQUFDLFdBQVcsU0FBSSxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssU0FBTSxFQUMvSCxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLEdBQUc7Z0NBQ25DLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVU7O29DQUNILGFBQWEsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQzs7b0NBQUcsTUFBTSxLQUFLLFFBQVEsQ0FBQyxNQUFNLENBQUMsV0FBVyxJQUFJLFVBQVU7b0NBQUUsU0FBUyxLQUFLLFFBQVEsQ0FBQyxNQUFNLENBQUMsV0FBVyxJQUFJLEtBQUssQ0FDdkssQ0FDRixDQUNQO3dCQUdELENBQUMsQ0FBQyxRQUFRLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsV0FBVzsrQkFDN0QsTUFBTSxLQUFLLFFBQVEsQ0FBQyxNQUFNLENBQUMsV0FBVzsrQkFDdEMsQ0FDRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNO2dDQUMzQiw2QkFDRSxHQUFHLEVBQUUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQ3ZFLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksR0FBRztnQ0FFakMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztvQ0FDL0QsQ0FBQyxDQUFDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVU7d0NBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLElBQUk7O3dDQUFHLFdBQVc7O3dDQUFHLGFBQWEsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQU87b0NBQ3RLLENBQUMsQ0FBQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBTyxDQUV2RixDQUNQO3dCQVFILDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FFdkI7d0JBQ04sNkJBQUssU0FBUyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsT0FBTyxFQUFFLGNBQU0sT0FBQSxVQUFVLEVBQUUsRUFBWixDQUFZLHNDQUFtQjt3QkFDOUYsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyx3REFBZ0MsQ0FDNUQsQ0FDRixDQUNGO1lBQ04sNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFRO1lBQ3RDLDZCQUFLLFNBQVMsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBUTtZQUNuRiw2QkFBSyxTQUFTLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQVE7WUFDbkYsNkJBQUssU0FBUyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFRO1lBQ25GLDZCQUFLLFNBQVMsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBUTtZQUNuRiw2QkFBSyxTQUFTLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQVE7WUFDbkYsNkJBQUssS0FBSyxFQUFFLEVBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFDLEdBRWpGLENBQ0YsQ0FDRixDQUNQLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./container/game/beauty-hunter/step-3/container.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var connect = __webpack_require__(129).connect;






var container_HalioLandingPageContainer = /** @class */ (function (_super) {
    __extends(HalioLandingPageContainer, _super);
    function HalioLandingPageContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    HalioLandingPageContainer.prototype.componentWillMount = function () {
        Object(responsive["b" /* isDesktopVersion */])() && this.props.history.push("" + routing["kb" /* ROUTING_SHOP_INDEX */]);
        this.props.updateMetaInfoAction({
            info: {
                url: "https://www.lixibox.com",
                type: "article",
                title: 'Beauty Hunter - Lixibox Game',
                description: 'Lixibox shop box mỹ phẩm cao cấp, trị mụn, dưỡng da thiết kế bởi các chuyên gia mailovesbeauty, love at first shine, changmakeup, skincare junkie ngo nhi',
                keyword: 'mỹ phẩm, dưỡng da, trị mụn, skincare, makeup, halio, lustre',
                image: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/meta/cover.jpeg'
            },
            structuredData: {
                breadcrumbList: []
            }
        });
    };
    HalioLandingPageContainer.prototype.componentWillUnmount = function () {
        // this.aud1.pause();
    };
    HalioLandingPageContainer.prototype.componentDidMount = function () {
        var appContainer = document.getElementsByTagName('app-container')[0];
        appContainer.style.position = 'fixed';
        var _a = this.props, loadGameAction = _a.loadGameAction, getUserGiftAction = _a.getUserGiftAction, getTodayGiftAction = _a.getTodayGiftAction;
        getUserGiftAction();
        loadGameAction();
        getTodayGiftAction();
        try {
            setInterval(function () {
                var aud = document.getElementById("audio");
                !!aud && aud.play();
            }, 1000);
        }
        catch (e) {
        }
    };
    HalioLandingPageContainer.prototype.handleBack = function () {
        window.location.href = '/games/beauty-hunter?tab=user-gift';
    };
    HalioLandingPageContainer.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state,
            handleBack: this.handleBack.bind(this)
        };
        return view(args);
    };
    HalioLandingPageContainer.defaultProps = DEFAULT_PROPS;
    HalioLandingPageContainer = __decorate([
        radium
    ], HalioLandingPageContainer);
    return HalioLandingPageContainer;
}(react["Component"]));
;
/* harmony default export */ var container = __webpack_exports__["default"] = (connect(mapStateToProps, mapDispatchToProps)(container_HalioLandingPageContainer));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFDakMsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUNoRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUMvRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUcxRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsZUFBZSxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBQzlELE9BQU8sRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQzVELE9BQU8sVUFBVSxNQUFNLFFBQVEsQ0FBQztBQUdoQztJQUF3Qyw2Q0FBK0I7SUFHckUsbUNBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUM7O0lBQzdCLENBQUM7SUFFRCxzREFBa0IsR0FBbEI7UUFDRSxnQkFBZ0IsRUFBRSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFHLGtCQUFvQixDQUFDLENBQUM7UUFFdkUsSUFBSSxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQztZQUM5QixJQUFJLEVBQUU7Z0JBQ0osR0FBRyxFQUFFLHlCQUF5QjtnQkFDOUIsSUFBSSxFQUFFLFNBQVM7Z0JBQ2YsS0FBSyxFQUFFLDhCQUE4QjtnQkFDckMsV0FBVyxFQUFFLDJKQUEySjtnQkFDeEssT0FBTyxFQUFFLDZEQUE2RDtnQkFDdEUsS0FBSyxFQUFFLGlCQUFpQixHQUFHLGdDQUFnQzthQUM1RDtZQUNELGNBQWMsRUFBRTtnQkFDZCxjQUFjLEVBQUUsRUFBRTthQUNuQjtTQUNGLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCx3REFBb0IsR0FBcEI7UUFDRSxxQkFBcUI7SUFDdkIsQ0FBQztJQUVELHFEQUFpQixHQUFqQjtRQUNFLElBQU0sWUFBWSxHQUFPLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMzRSxZQUFZLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7UUFFaEMsSUFBQSxlQUlRLEVBSFosa0NBQWMsRUFDZCx3Q0FBaUIsRUFDakIsMENBQWtCLENBQ0w7UUFDZixpQkFBaUIsRUFBRSxDQUFDO1FBQ3BCLGNBQWMsRUFBRSxDQUFDO1FBQ2pCLGtCQUFrQixFQUFFLENBQUM7UUFFckIsSUFBSSxDQUFDO1lBQ0gsV0FBVyxDQUFDO2dCQUVOLElBQU0sR0FBRyxHQUFRLFFBQVEsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ2xELENBQUMsQ0FBQyxHQUFHLElBQUksR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDO1lBRTFCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNYLENBQUM7UUFBQyxLQUFLLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRVosQ0FBQztJQUNILENBQUM7SUFFRCw4Q0FBVSxHQUFWO1FBQ0UsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsb0NBQW9DLENBQUM7SUFDOUQsQ0FBQztJQUVELDBDQUFNLEdBQU47UUFDRSxJQUFNLElBQUksR0FBRztZQUNYLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztTQUN2QyxDQUFDO1FBRUYsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBbEVNLHNDQUFZLEdBQVcsYUFBYSxDQUFDO0lBRHhDLHlCQUF5QjtRQUQ5QixNQUFNO09BQ0QseUJBQXlCLENBb0U5QjtJQUFELGdDQUFDO0NBQUEsQUFwRUQsQ0FBd0MsS0FBSyxDQUFDLFNBQVMsR0FvRXREO0FBQUEsQ0FBQztBQUVGLGVBQWUsT0FBTyxDQUNwQixlQUFlLEVBQ2Ysa0JBQWtCLENBQ25CLENBQUMseUJBQXlCLENBQUMsQ0FBQyJ9

/***/ })

}]);