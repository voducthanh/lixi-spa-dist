(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[80],{

/***/ 745:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 100).then(__webpack_require__.bind(null, 755)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 818:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return FB_CHAT_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return SOCIAL_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DOWNLOAD_APP_URL; });
var FB_CHAT_URL = '//www.facebook.com/plugins/page.php?href=https://www.facebook.com/1450884048490991&tabs=messages&width=300&height=300&adapt_container_width=true&show_facepile=false&hide_cta=false&appId&small_header=true';
var SOCIAL_URL = {
    facebook: '//www.facebook.com/lixiboxvn/',
    instagram: '//www.instagram.com/lixibox/',
    youtube: '//www.youtube.com/channel/UCW0CxHcD9jN1lhFeKLHwCvQ',
    pinterest: '//www.pinterest.com/lixibox/',
};
var DOWNLOAD_APP_URL = {
    shopping: {
        ios: '//itunes.apple.com/us/app/id1078181334',
        android: '//play.google.com/store/apps/details?id=com.lixibox&hl=vi'
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic29jaWFsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic29jaWFsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE1BQU0sQ0FBQyxJQUFNLFdBQVcsR0FBRyw2TUFBNk0sQ0FBQztBQUV6TyxNQUFNLENBQUMsSUFBTSxVQUFVLEdBQUc7SUFDeEIsUUFBUSxFQUFFLCtCQUErQjtJQUN6QyxTQUFTLEVBQUUsOEJBQThCO0lBQ3pDLE9BQU8sRUFBRSxvREFBb0Q7SUFDN0QsU0FBUyxFQUFFLDhCQUE4QjtDQUMxQyxDQUFDO0FBRUYsTUFBTSxDQUFDLElBQU0sZ0JBQWdCLEdBQUc7SUFDOUIsUUFBUSxFQUFFO1FBQ1IsR0FBRyxFQUFFLHdDQUF3QztRQUM3QyxPQUFPLEVBQUUsMkRBQTJEO0tBQ3JFO0NBQ0YsQ0FBQyJ9

/***/ }),

/***/ 863:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// CONCATENATED MODULE: ./container/app-shop/mobile/store.tsx
var mapStateToProps = function (state) { return ({}); };
var mapDispatchToProps = function (dispatch) { return ({}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBSUEsTUFBTSxDQUFDLElBQU0sZUFBZSxHQUFHLFVBQUMsS0FBSyxJQUFLLE9BQUEsQ0FBQyxFQUFhLENBQUEsRUFBZCxDQUFjLENBQUM7QUFDekQsTUFBTSxDQUFDLElBQU0sa0JBQWtCLEdBQUcsVUFBQyxRQUFRLElBQUssT0FBQSxDQUFDLEVBQWEsQ0FBQSxFQUFkLENBQWMsQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/mobile/initialize.tsx
var DEFAULT_PROPS = {};
var INITIAL_STATE = {};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRyxFQUFZLENBQUM7QUFFMUMsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHLEVBQVksQ0FBQyJ9
// EXTERNAL MODULE: ./constants/application/social.ts
var social = __webpack_require__(818);

// EXTERNAL MODULE: ./container/layout/wrap/index.tsx
var wrap = __webpack_require__(745);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./style/media-queries.ts
var media_queries = __webpack_require__(76);

// EXTERNAL MODULE: ./utils/uri.ts
var uri = __webpack_require__(347);

// CONCATENATED MODULE: ./container/app-shop/mobile/style.tsx



var bannerImg = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/mobile/top-banner.jpg';
var leftImg = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/mobile/left.jpg';
var rightImg = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/mobile/right.jpg';
var bottomImg = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/mobile/bottom.jpg';
var quoteImg = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/mobile/quote.png';
/* harmony default export */ var style = ({
    container: {},
    wrap: {
        position: variable["position"].relative,
        background: variable["colorWhite"]
    },
    header: {
        backgroundImage: "url(" + bannerImg + ")",
        backgroundSize: 'cover',
        backgroundPosition: 'top left',
        padding: 20,
        logo: (_a = {
                display: variable["display"].flex,
                marginBottom: 30,
                filter: "drop-shadow(0px 2px 5px #fff)"
            },
            _a[media_queries["a" /* default */].tablet960] = {
                display: 'none'
            },
            _a.icon = {
                width: 40,
                height: 40,
                color: variable["colorPink"],
                marginRight: 10,
            },
            _a.iconInner = {
                height: 36,
            },
            _a.line = {
                width: 'auto',
                height: 40,
                color: variable["colorPink"]
            },
            _a.lineInner = {
                height: 20,
            },
            _a),
        content: (_b = {},
            _b[media_queries["a" /* default */].tablet960] = {
                padding: '100px 60px 70px'
            },
            _b.title = {
                fontSize: 40,
                fontFamily: variable["fontPlayFairRegular"],
                fontWeight: 500,
                lineHeight: '40px',
                textShadow: '0 2px 10px #fff',
                color: variable["colorBlack"],
                maxWidth: '70%',
                marginBottom: 20,
            },
            _b.description = {
                fontSize: 16,
                textTransform: 'uppercase',
                fontFamily: variable["fontAvenirDemiBold"],
                textShadow: '0 2px 10px #fff',
                maxWidth: '90%',
                marginBottom: 20,
            },
            _b.link = {
                display: variable["display"].flex,
                item: {
                    display: variable["display"].block,
                    marginBottom: 10,
                    marginRight: 10,
                    image: {
                        display: variable["display"].block,
                        height: 44,
                        minWidth: 120,
                        border: "0px solid #FFF",
                        background: variable["colorBlack"],
                        paddingLeft: 5,
                        paddingTop: 3,
                        paddingBottom: 3,
                        borderRadius: 5,
                    }
                }
            },
            _b)
    },
    content: (_c = {
            padding: '25px 20px'
        },
        _c[media_queries["a" /* default */].tablet960] = {
            padding: '50px 80px 50px',
            display: variable["display"].flex,
            justifyContent: 'space-between'
        },
        _c.left = (_d = {},
            _d[media_queries["a" /* default */].tablet960] = {
                width: '31%',
            },
            _d),
        _c.right = (_e = {},
            _e[media_queries["a" /* default */].tablet960] = {
                width: '31%',
            },
            _e),
        _c.phone = (_f = {
                display: 'none',
                position: variable["position"].absolute,
                width: '24%',
                height: 'auto',
                left: '50%',
                transform: 'translate(-50%, -110px)'
            },
            _f[media_queries["a" /* default */].tablet960] = {
                display: variable["display"].block,
            },
            _f),
        _c.textBlock = {
            marginBottom: 25,
            header: {
                fontFamily: variable["fontPlayFairRegular"],
                fontWeight: 300,
                fontSize: 24,
                marginBottom: 10,
            },
            description: {
                fontSize: 16,
                lineHeight: '22px',
            }
        },
        _c.quoteBlock = {
            backgroundImage: "url(" + quoteImg + ")",
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'left 5px',
            backgroundSize: '34px 23px',
            fontSize: 20,
            lineHeight: '30px',
            fontFamily: variable["fontPlayFairRegular"],
            fontWeight: 300,
            fontStyle: 'italic',
            paddingLeft: 42,
        },
        _c),
    bg: {
        left: {
            backgroundImage: "url(" + leftImg + ")",
            width: 130,
            height: '100%',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'top right',
            position: variable["position"].absolute,
            left: -110,
            top: 0,
        },
        right: {
            backgroundImage: "url(" + rightImg + ")",
            width: 130,
            height: '100%',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'top right',
            position: variable["position"].absolute,
            right: -110,
            top: 0,
        },
        bottom: {
            backgroundImage: "url(" + bottomImg + ")",
            width: '100%',
            height: 80,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center left',
        },
    }
});
var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUEsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLGFBQWEsTUFBTSw4QkFBOEIsQ0FBQztBQUV6RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUV2RCxJQUFNLFNBQVMsR0FBRyxpQkFBaUIsR0FBRyxzQ0FBc0MsQ0FBQztBQUM3RSxJQUFNLE9BQU8sR0FBRyxpQkFBaUIsR0FBRyxnQ0FBZ0MsQ0FBQztBQUNyRSxJQUFNLFFBQVEsR0FBRyxpQkFBaUIsR0FBRyxpQ0FBaUMsQ0FBQztBQUN2RSxJQUFNLFNBQVMsR0FBRyxpQkFBaUIsR0FBRyxrQ0FBa0MsQ0FBQztBQUN6RSxJQUFNLFFBQVEsR0FBRyxpQkFBaUIsR0FBRyxpQ0FBaUMsQ0FBQztBQUV2RSxlQUFlO0lBQ2IsU0FBUyxFQUFFLEVBQ1Y7SUFFRCxJQUFJLEVBQUU7UUFDSixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1FBQ3BDLFVBQVUsRUFBRSxRQUFRLENBQUMsVUFBVTtLQUNoQztJQUVELE1BQU0sRUFBRTtRQUNOLGVBQWUsRUFBRSxTQUFPLFNBQVMsTUFBRztRQUNwQyxjQUFjLEVBQUUsT0FBTztRQUN2QixrQkFBa0IsRUFBRSxVQUFVO1FBQzlCLE9BQU8sRUFBRSxFQUFFO1FBRVgsSUFBSTtnQkFDRixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixZQUFZLEVBQUUsRUFBRTtnQkFDaEIsTUFBTSxFQUFFLCtCQUErQjs7WUFFdkMsR0FBQyxhQUFhLENBQUMsU0FBUyxJQUFHO2dCQUN6QixPQUFPLEVBQUUsTUFBTTthQUNoQjtZQUVELE9BQUksR0FBRTtnQkFDSixLQUFLLEVBQUUsRUFBRTtnQkFDVCxNQUFNLEVBQUUsRUFBRTtnQkFDVixLQUFLLEVBQUUsUUFBUSxDQUFDLFNBQVM7Z0JBQ3pCLFdBQVcsRUFBRSxFQUFFO2FBQ2hCO1lBQ0QsWUFBUyxHQUFFO2dCQUNULE1BQU0sRUFBRSxFQUFFO2FBQ1g7WUFDRCxPQUFJLEdBQUU7Z0JBQ0osS0FBSyxFQUFFLE1BQU07Z0JBQ2IsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxTQUFTO2FBQzFCO1lBQ0QsWUFBUyxHQUFFO2dCQUNULE1BQU0sRUFBRSxFQUFFO2FBQ1g7ZUFDRjtRQUVELE9BQU87WUFDTCxHQUFDLGFBQWEsQ0FBQyxTQUFTLElBQUc7Z0JBQ3pCLE9BQU8sRUFBRSxpQkFBaUI7YUFDM0I7WUFFRCxRQUFLLEdBQUU7Z0JBQ0wsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osVUFBVSxFQUFFLFFBQVEsQ0FBQyxtQkFBbUI7Z0JBQ3hDLFVBQVUsRUFBRSxHQUFHO2dCQUNmLFVBQVUsRUFBRSxNQUFNO2dCQUNsQixVQUFVLEVBQUUsaUJBQWlCO2dCQUM3QixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7Z0JBQzFCLFFBQVEsRUFBRSxLQUFLO2dCQUNmLFlBQVksRUFBRSxFQUFFO2FBQ2pCO1lBRUQsY0FBVyxHQUFFO2dCQUNYLFFBQVEsRUFBRSxFQUFFO2dCQUNaLGFBQWEsRUFBRSxXQUFXO2dCQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGtCQUFrQjtnQkFDdkMsVUFBVSxFQUFFLGlCQUFpQjtnQkFDN0IsUUFBUSxFQUFFLEtBQUs7Z0JBQ2YsWUFBWSxFQUFFLEVBQUU7YUFDakI7WUFFRCxPQUFJLEdBQUU7Z0JBQ0osT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFFOUIsSUFBSSxFQUFFO29CQUNKLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7b0JBQy9CLFlBQVksRUFBRSxFQUFFO29CQUNoQixXQUFXLEVBQUUsRUFBRTtvQkFFZixLQUFLLEVBQUU7d0JBQ0wsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSzt3QkFDL0IsTUFBTSxFQUFFLEVBQUU7d0JBQ1YsUUFBUSxFQUFFLEdBQUc7d0JBQ2IsTUFBTSxFQUFFLGdCQUFnQjt3QkFDeEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxVQUFVO3dCQUMvQixXQUFXLEVBQUUsQ0FBQzt3QkFDZCxVQUFVLEVBQUUsQ0FBQzt3QkFDYixhQUFhLEVBQUUsQ0FBQzt3QkFDaEIsWUFBWSxFQUFFLENBQUM7cUJBQ2hCO2lCQUNGO2FBQ0Y7ZUFDRjtLQUNGO0lBRUQsT0FBTztZQUNMLE9BQU8sRUFBRSxXQUFXOztRQUVwQixHQUFDLGFBQWEsQ0FBQyxTQUFTLElBQUc7WUFDekIsT0FBTyxFQUFFLGdCQUFnQjtZQUN6QixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLGNBQWMsRUFBRSxlQUFlO1NBQ2hDO1FBRUQsT0FBSTtZQUNGLEdBQUMsYUFBYSxDQUFDLFNBQVMsSUFBRztnQkFDekIsS0FBSyxFQUFFLEtBQUs7YUFDYjtlQUNGO1FBQ0QsUUFBSztZQUNILEdBQUMsYUFBYSxDQUFDLFNBQVMsSUFBRztnQkFDekIsS0FBSyxFQUFFLEtBQUs7YUFDYjtlQUNGO1FBRUQsUUFBSztnQkFDSCxPQUFPLEVBQUUsTUFBTTtnQkFDZixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxLQUFLLEVBQUUsS0FBSztnQkFDWixNQUFNLEVBQUUsTUFBTTtnQkFDZCxJQUFJLEVBQUUsS0FBSztnQkFDWCxTQUFTLEVBQUUseUJBQXlCOztZQUVwQyxHQUFDLGFBQWEsQ0FBQyxTQUFTLElBQUc7Z0JBQ3pCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7YUFDaEM7ZUFDRjtRQUVELFlBQVMsR0FBRTtZQUNULFlBQVksRUFBRSxFQUFFO1lBRWhCLE1BQU0sRUFBRTtnQkFDTixVQUFVLEVBQUUsUUFBUSxDQUFDLG1CQUFtQjtnQkFDeEMsVUFBVSxFQUFFLEdBQUc7Z0JBQ2YsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osWUFBWSxFQUFFLEVBQUU7YUFDakI7WUFFRCxXQUFXLEVBQUU7Z0JBQ1gsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osVUFBVSxFQUFFLE1BQU07YUFDbkI7U0FDRjtRQUVELGFBQVUsR0FBRTtZQUNWLGVBQWUsRUFBRSxTQUFPLFFBQVEsTUFBRztZQUNuQyxnQkFBZ0IsRUFBRSxXQUFXO1lBQzdCLGtCQUFrQixFQUFFLFVBQVU7WUFDOUIsY0FBYyxFQUFFLFdBQVc7WUFDM0IsUUFBUSxFQUFFLEVBQUU7WUFDWixVQUFVLEVBQUUsTUFBTTtZQUNsQixVQUFVLEVBQUUsUUFBUSxDQUFDLG1CQUFtQjtZQUN4QyxVQUFVLEVBQUUsR0FBRztZQUNmLFNBQVMsRUFBRSxRQUFRO1lBQ25CLFdBQVcsRUFBRSxFQUFFO1NBQ2hCO1dBQ0Y7SUFFRCxFQUFFLEVBQUU7UUFDRixJQUFJLEVBQUU7WUFDSixlQUFlLEVBQUUsU0FBTyxPQUFPLE1BQUc7WUFDbEMsS0FBSyxFQUFFLEdBQUc7WUFDVixNQUFNLEVBQUUsTUFBTTtZQUNkLGNBQWMsRUFBRSxPQUFPO1lBQ3ZCLGdCQUFnQixFQUFFLFdBQVc7WUFDN0Isa0JBQWtCLEVBQUUsV0FBVztZQUMvQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1lBQ3BDLElBQUksRUFBRSxDQUFDLEdBQUc7WUFDVixHQUFHLEVBQUUsQ0FBQztTQUNQO1FBRUQsS0FBSyxFQUFFO1lBQ0wsZUFBZSxFQUFFLFNBQU8sUUFBUSxNQUFHO1lBQ25DLEtBQUssRUFBRSxHQUFHO1lBQ1YsTUFBTSxFQUFFLE1BQU07WUFDZCxjQUFjLEVBQUUsT0FBTztZQUN2QixnQkFBZ0IsRUFBRSxXQUFXO1lBQzdCLGtCQUFrQixFQUFFLFdBQVc7WUFDL0IsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxLQUFLLEVBQUUsQ0FBQyxHQUFHO1lBQ1gsR0FBRyxFQUFFLENBQUM7U0FDUDtRQUVELE1BQU0sRUFBRTtZQUNOLGVBQWUsRUFBRSxTQUFPLFNBQVMsTUFBRztZQUNwQyxLQUFLLEVBQUUsTUFBTTtZQUNiLE1BQU0sRUFBRSxFQUFFO1lBQ1YsY0FBYyxFQUFFLE9BQU87WUFDdkIsZ0JBQWdCLEVBQUUsV0FBVztZQUM3QixrQkFBa0IsRUFBRSxhQUFhO1NBQ2xDO0tBQ0Y7Q0FDTSxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/mobile/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};







var iosDownload = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/app-icon/ios-app.png';
var androidDownload = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/app-icon/android-app.png';
var ipImg = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/mobile/ip.png';
var renderHeader = function () {
    var iconProps = {
        style: style.header.logo.icon,
        innerStyle: style.header.logo.iconInner,
        name: 'logo-line',
    };
    var lineProps = {
        style: style.header.logo.line,
        innerStyle: style.header.logo.lineInner,
        name: 'logo-text',
    };
    return (react["createElement"]("div", { style: style.header },
        react["createElement"]("a", { href: '/', style: style.header.logo },
            react["createElement"](icon["a" /* default */], __assign({}, iconProps)),
            react["createElement"](icon["a" /* default */], __assign({}, lineProps))),
        react["createElement"]("div", { style: style.header.content },
            react["createElement"]("div", { style: style.header.content.title }, "Lixibox in your pocket"),
            react["createElement"]("div", { style: style.header.content.description }, "T\u1EA2I \u1EE8NG D\u1EE4NG LIXIBOX \u0110\u1EC2 LU\u00D4N \u0110\u1EB8P M\u1ECCI L\u00DAC, M\u1ECCI N\u01A0I!"),
            react["createElement"]("div", { style: style.header.content.link },
                react["createElement"]("a", { style: style.header.content.link.item, href: social["a" /* DOWNLOAD_APP_URL */].shopping.ios },
                    react["createElement"]("img", { src: iosDownload, style: style.header.content.link.item.image })),
                react["createElement"]("a", { style: style.header.content.link.item, href: social["a" /* DOWNLOAD_APP_URL */].shopping.android },
                    react["createElement"]("img", { src: androidDownload, style: style.header.content.link.item.image }))))));
};
var renderTextBlock = function (title, description) { return (react["createElement"]("div", { style: style.content.textBlock },
    react["createElement"]("div", { style: style.content.textBlock.header }, title),
    react["createElement"]("div", { style: style.content.textBlock.description }, description))); };
var renderQuoteBlock = function () { return (react["createElement"]("div", { style: style.content.quoteBlock }, "C\u00F2n ch\u1EDD g\u00EC n\u1EEFa, t\u1EA3i \u1EE9ng d\u1EE5ng Lixibox v\u00E0 ngay l\u1EADp t\u1EE9c kh\u00E1m ph\u00E1 \u0111i\u1EC1u th\u00FA v\u1ECB \u0111ang ch\u1EDD b\u1EA1n.")); };
var renderContent = function () { return (react["createElement"]("div", { style: style.content },
    react["createElement"]("div", { style: style.content.left },
        renderTextBlock('Mua nhanh', 'Mua sắm tất cả sản phẩm lẻ, box có sẵn tại Lixibox nhanh chóng và tiện lợi.'),
        renderTextBlock('Quản lý gọn', 'Quản lý thông tin profile làm đẹp cá nhân, thông tin đơn hàng, thanh toán, vận chuyển... Lixibox sẽ gởi thông báo chi tiết trong ứng dụng để bạn an tâm với dịch vụ và chỉ cần quan tâm đến việc làm đẹp mà thôi!'),
        renderTextBlock('Giới thiệu bạn bè', 'Nhận ngay 50,000đ và 200 Lixicoin cho mỗi đơn hàng giới thiệu thành công trên mobile app.')),
    react["createElement"]("div", { style: style.content.right },
        renderTextBlock('Dẫn đầu xu hướng', 'Sẵn sàng khám phá sản phẩm mới, mẹo làm đẹp, xu hướng làm đẹp mới nhất mỗi ngày từ các chuyên gia, beauty bloggers và cộng đồng.'),
        renderTextBlock('Đặc quyền ưu tiên', 'Nhận thông báo mã giảm giá, chương trình khuyến mãi/ưu đãi  mới nhất và đặc biệt là ưu đãi giới hạn chỉ dành riêng cho ứng dụng Lixibox.'),
        renderQuoteBlock()),
    react["createElement"]("img", { src: ipImg, style: style.content.phone }))); };
var renderBackground = function () { return (react["createElement"]("div", null,
    react["createElement"]("div", { style: style.bg.left }),
    react["createElement"]("div", { style: style.bg.right }),
    react["createElement"]("div", { style: style.bg.bottom }))); };
var renderView = function () {
    return (react["createElement"]("div", { style: style.container, className: 'mobile-deeplink-gradient' },
        react["createElement"](wrap["a" /* default */], { style: style.wrap },
            renderHeader(),
            renderContent(),
            Object(responsive["b" /* isDesktopVersion */])() && renderBackground())));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDekUsT0FBTyxVQUFVLE1BQU0sbUJBQW1CLENBQUM7QUFDM0MsT0FBTyxJQUFJLE1BQU0sNkJBQTZCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFN0QsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRTVCLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXZELElBQU0sV0FBVyxHQUFHLGlCQUFpQixHQUFHLHFDQUFxQyxDQUFDO0FBQzlFLElBQU0sZUFBZSxHQUFHLGlCQUFpQixHQUFHLHlDQUF5QyxDQUFDO0FBQ3RGLElBQU0sS0FBSyxHQUFHLGlCQUFpQixHQUFHLDhCQUE4QixDQUFDO0FBR2pFLElBQU0sWUFBWSxHQUFHO0lBQ25CLElBQU0sU0FBUyxHQUFHO1FBQ2hCLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJO1FBQzdCLFVBQVUsRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTO1FBQ3ZDLElBQUksRUFBRSxXQUFXO0tBQ2xCLENBQUM7SUFFRixJQUFNLFNBQVMsR0FBRztRQUNoQixLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSTtRQUM3QixVQUFVLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUztRQUN2QyxJQUFJLEVBQUUsV0FBVztLQUNsQixDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNO1FBQ3RCLDJCQUFHLElBQUksRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSTtZQUNwQyxvQkFBQyxJQUFJLGVBQUssU0FBUyxFQUFJO1lBQ3ZCLG9CQUFDLElBQUksZUFBSyxTQUFTLEVBQUksQ0FDckI7UUFFSiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPO1lBQzlCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLDZCQUVoQztZQUNOLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxXQUFXLHFIQUV0QztZQUNOLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUNuQywyQkFDRSxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksRUFDckMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxHQUFHO29CQUNuQyw2QkFBSyxHQUFHLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBSSxDQUNwRTtnQkFDSiwyQkFDRSxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksRUFDckMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxPQUFPO29CQUN2Qyw2QkFBSyxHQUFHLEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBSSxDQUFJLENBQzVFLENBQ0YsQ0FDRixDQUNQLENBQUE7QUFDSCxDQUFDLENBQUE7QUFFRCxJQUFNLGVBQWUsR0FBRyxVQUFDLEtBQUssRUFBRSxXQUFXLElBQUssT0FBQSxDQUM5Qyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTO0lBQ2pDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFNLElBQUcsS0FBSyxDQUFPO0lBQ3pELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxXQUFXLElBQUcsV0FBVyxDQUFPLENBQ2hFLENBQ1AsRUFMK0MsQ0FLL0MsQ0FBQTtBQUVELElBQU0sZ0JBQWdCLEdBQUcsY0FBTSxPQUFBLENBQzdCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsNkxBRTlCLENBQ1AsRUFKOEIsQ0FJOUIsQ0FBQTtBQUVELElBQU0sYUFBYSxHQUFHLGNBQU0sT0FBQSxDQUMxQiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU87SUFDdkIsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUMzQixlQUFlLENBQUMsV0FBVyxFQUFFLDZFQUE2RSxDQUFDO1FBQzNHLGVBQWUsQ0FBQyxhQUFhLEVBQUUsbU5BQW1OLENBQUM7UUFDblAsZUFBZSxDQUFDLG1CQUFtQixFQUFFLDJGQUEyRixDQUFDLENBQzlIO0lBQ04sNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSztRQUM1QixlQUFlLENBQUMsa0JBQWtCLEVBQUUsa0lBQWtJLENBQUM7UUFDdkssZUFBZSxDQUFDLG1CQUFtQixFQUFFLDBJQUEwSSxDQUFDO1FBQ2hMLGdCQUFnQixFQUFFLENBQ2Y7SUFDTiw2QkFBSyxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBSSxDQUMzQyxDQUNQLEVBZDJCLENBYzNCLENBQUE7QUFFRCxJQUFNLGdCQUFnQixHQUFHLGNBQU0sT0FBQSxDQUM3QjtJQUNFLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsRUFBRSxDQUFDLElBQUksR0FBUTtJQUNqQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLEVBQUUsQ0FBQyxLQUFLLEdBQVE7SUFDbEMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxFQUFFLENBQUMsTUFBTSxHQUFRLENBQy9CLENBQ1AsRUFOOEIsQ0FNOUIsQ0FBQTtBQUVELElBQU0sVUFBVSxHQUFHO0lBQ2pCLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxFQUFFLFNBQVMsRUFBRSwwQkFBMEI7UUFDaEUsb0JBQUMsVUFBVSxJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSTtZQUMxQixZQUFZLEVBQUU7WUFDZCxhQUFhLEVBQUU7WUFDZixnQkFBZ0IsRUFBRSxJQUFJLGdCQUFnQixFQUFFLENBQzlCLENBQ1IsQ0FDUixDQUFBO0FBQ0gsQ0FBQyxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/mobile/container.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var connect = __webpack_require__(129).connect;



var container_MobileContainer = /** @class */ (function (_super) {
    __extends(MobileContainer, _super);
    function MobileContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    MobileContainer.prototype.render = function () {
        return view();
    };
    MobileContainer.defaultProps = DEFAULT_PROPS;
    MobileContainer = __decorate([
        radium
    ], MobileContainer);
    return MobileContainer;
}(react["Component"]));
;
/* harmony default export */ var container = __webpack_exports__["default"] = (connect(mapStateToProps, mapDispatchToProps)(container_MobileContainer));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFDakMsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQU0vQyxPQUFPLEVBQUUsZUFBZSxFQUFFLGtCQUFrQixFQUFFLE1BQU0sU0FBUyxDQUFDO0FBQzlELE9BQU8sRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQzVELE9BQU8sVUFBVSxNQUFNLFFBQVEsQ0FBQztBQUdoQztJQUE4QixtQ0FBK0I7SUFHM0QseUJBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUF1QixDQUFDOztJQUN2QyxDQUFDO0lBRUQsZ0NBQU0sR0FBTjtRQUNFLE1BQU0sQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBVE0sNEJBQVksR0FBVyxhQUFhLENBQUM7SUFEeEMsZUFBZTtRQURwQixNQUFNO09BQ0QsZUFBZSxDQVdwQjtJQUFELHNCQUFDO0NBQUEsQUFYRCxDQUE4QixLQUFLLENBQUMsU0FBUyxHQVc1QztBQUFBLENBQUM7QUFFRixlQUFlLE9BQU8sQ0FDcEIsZUFBZSxFQUNmLGtCQUFrQixDQUNuQixDQUFDLGVBQWUsQ0FBQyxDQUFDIn0=

/***/ })

}]);