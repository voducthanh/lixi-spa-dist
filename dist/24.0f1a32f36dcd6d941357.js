(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[24],{

/***/ 756:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 96).then(__webpack_require__.bind(null, 779)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 914:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./action/meta.ts
var meta = __webpack_require__(754);

// EXTERNAL MODULE: ./action/magazine.ts + 1 modules
var magazine = __webpack_require__(762);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// EXTERNAL MODULE: ./utils/encode.ts
var encode = __webpack_require__(7);

// EXTERNAL MODULE: ./utils/uri.ts
var uri = __webpack_require__(347);

// EXTERNAL MODULE: ../node_modules/react-on-screen/lib/index.js
var lib = __webpack_require__(758);
var lib_default = /*#__PURE__*/__webpack_require__.n(lib);

// EXTERNAL MODULE: ./container/layout/fade-in/index.tsx
var fade_in = __webpack_require__(756);

// EXTERNAL MODULE: ./components/ui/loading/index.tsx + 4 modules
var loading = __webpack_require__(747);

// EXTERNAL MODULE: ./container/layout/wrap/index.tsx
var wrap = __webpack_require__(745);

// EXTERNAL MODULE: ./container/layout/split/index.tsx
var split = __webpack_require__(753);

// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/banner/magazine/style.tsx

/* harmony default export */ var magazine_style = ({
    desktop: {
        container: {
            itemSlider: function (imgUrl, type) {
                if (type === void 0) { type = 'normral'; }
                return ({
                    width: '100%',
                    display: variable["display"].block,
                    marginBottom: 20,
                    borderRadius: 5,
                    overflow: 'hidden',
                    boxShadow: variable["shadowBlur"],
                    backgroundColor: variable["colorF7"],
                    backgroundImage: "url(" + imgUrl + ")",
                    backgroundPosition: 'top center',
                    backgroundSize: 'cover',
                    position: variable["position"].relative,
                });
            },
        }
    },
    mobile: {
        container: {
            width: '100%',
            overflowX: 'auto',
            whiteSpace: 'nowrap',
            paddingTop: 5,
            paddingLeft: 5,
            paddingRight: 5,
            itemSlider: function (imgUrl) { return ({
                margin: "5px 5px 5px 5px",
                width: '85%',
                maxWidth: 300,
                display: variable["display"].inlineBlock,
                borderRadius: 5,
                overflow: 'hidden',
                boxShadow: variable["shadowBlur"],
                backgroundColor: variable["colorF7"],
                backgroundImage: "url(" + imgUrl + ")",
                backgroundPosition: 'top center',
                backgroundSize: 'cover',
                position: variable["position"].relative,
            }); },
            itemSliderPanel: {
                wdith: '100%',
                paddingTop: '62.5%',
                position: variable["position"].relative,
            },
            info: {
                width: '100%',
                padding: 12,
                position: variable["position"].absolute,
                bottom: 0,
                left: 0,
                background: variable["colorBlack03"],
                height: '100%',
                display: variable["display"].flex,
                flexDirection: 'column',
                justifyContent: 'flex-end',
                alignItems: 'flex-start',
                large: {
                    padding: 30
                },
                category: function (type) {
                    if (type === void 0) { type = 'normal'; }
                    return Object.assign({}, {
                        background: variable["colorWhite"],
                        display: variable["display"].block,
                        float: 'left',
                        color: variable["colorBlack"],
                        height: 22,
                        lineHeight: '24px',
                        padding: '0 6px',
                        fontSize: 12,
                        marginBottom: 5,
                    }, 'large' === type && {
                        fontFamily: variable["fontAvenirDemiBold"],
                        height: 34,
                        lineHeight: '36px',
                        padding: '0 10px',
                        fontSize: 18,
                        marginBottom: 15,
                    });
                },
                title: function (type) {
                    if (type === void 0) { type = 'normal'; }
                    return Object.assign({}, {
                        color: variable["colorWhite"],
                        whiteSpace: 'pre-wrap',
                        fontFamily: variable["fontAvenirMedium"],
                        textShadow: variable["shadowTextBlur"],
                        overflow: 'hidden',
                        fontSize: 20,
                        lineHeight: '26px',
                        maxHeight: '78px',
                        maxWidth: '100%'
                    }, 'large' === type && {
                        fontFamily: variable["fontAvenirDemiBold"],
                        fontSize: 40,
                        lineHeight: '50px',
                        maxHeight: '150px',
                        maxWidth: '80%',
                    });
                }
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUdwRCxlQUFlO0lBQ2IsT0FBTyxFQUFFO1FBQ1AsU0FBUyxFQUFFO1lBQ1QsVUFBVSxFQUFFLFVBQUMsTUFBTSxFQUFFLElBQWdCO2dCQUFoQixxQkFBQSxFQUFBLGdCQUFnQjtnQkFBSyxPQUFBLENBQUM7b0JBQ3pDLEtBQUssRUFBRSxNQUFNO29CQUNiLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7b0JBQy9CLFlBQVksRUFBRSxFQUFFO29CQUNoQixZQUFZLEVBQUUsQ0FBQztvQkFDZixRQUFRLEVBQUUsUUFBUTtvQkFDbEIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUM5QixlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87b0JBQ2pDLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztvQkFDakMsa0JBQWtCLEVBQUUsWUFBWTtvQkFDaEMsY0FBYyxFQUFFLE9BQU87b0JBQ3ZCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7aUJBQ3JDLENBQUM7WUFad0MsQ0FZeEM7U0FDSDtLQUNGO0lBRUQsTUFBTSxFQUFFO1FBQ04sU0FBUyxFQUFFO1lBQ1QsS0FBSyxFQUFFLE1BQU07WUFDYixTQUFTLEVBQUUsTUFBTTtZQUNqQixVQUFVLEVBQUUsUUFBUTtZQUNwQixVQUFVLEVBQUUsQ0FBQztZQUNiLFdBQVcsRUFBRSxDQUFDO1lBQ2QsWUFBWSxFQUFFLENBQUM7WUFFZixVQUFVLEVBQUUsVUFBQyxNQUFNLElBQUssT0FBQSxDQUFDO2dCQUN2QixNQUFNLEVBQUUsaUJBQWlCO2dCQUN6QixLQUFLLEVBQUUsS0FBSztnQkFDWixRQUFRLEVBQUUsR0FBRztnQkFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxXQUFXO2dCQUNyQyxZQUFZLEVBQUUsQ0FBQztnQkFDZixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUM5QixlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87Z0JBQ2pDLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztnQkFDakMsa0JBQWtCLEVBQUUsWUFBWTtnQkFDaEMsY0FBYyxFQUFFLE9BQU87Z0JBQ3ZCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7YUFDckMsQ0FBQyxFQWJzQixDQWF0QjtZQUVGLGVBQWUsRUFBRTtnQkFDZixLQUFLLEVBQUUsTUFBTTtnQkFDYixVQUFVLEVBQUUsT0FBTztnQkFDbkIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTthQUNyQztZQUVELElBQUksRUFBRTtnQkFDSixLQUFLLEVBQUUsTUFBTTtnQkFDYixPQUFPLEVBQUUsRUFBRTtnQkFDWCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxNQUFNLEVBQUUsQ0FBQztnQkFDVCxJQUFJLEVBQUUsQ0FBQztnQkFDUCxVQUFVLEVBQUUsUUFBUSxDQUFDLFlBQVk7Z0JBQ2pDLE1BQU0sRUFBRSxNQUFNO2dCQUNkLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLGFBQWEsRUFBRSxRQUFRO2dCQUN2QixjQUFjLEVBQUUsVUFBVTtnQkFDMUIsVUFBVSxFQUFFLFlBQVk7Z0JBRXhCLEtBQUssRUFBRTtvQkFDTCxPQUFPLEVBQUUsRUFBRTtpQkFDWjtnQkFFRCxRQUFRLEVBQUUsVUFBQyxJQUFlO29CQUFmLHFCQUFBLEVBQUEsZUFBZTtvQkFBSyxPQUFBLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFO3dCQUMvQyxVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVU7d0JBQy9CLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7d0JBQy9CLEtBQUssRUFBRSxNQUFNO3dCQUNiLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTt3QkFDMUIsTUFBTSxFQUFFLEVBQUU7d0JBQ1YsVUFBVSxFQUFFLE1BQU07d0JBQ2xCLE9BQU8sRUFBRSxPQUFPO3dCQUNoQixRQUFRLEVBQUUsRUFBRTt3QkFDWixZQUFZLEVBQUUsQ0FBQztxQkFDaEIsRUFDQyxPQUFPLEtBQUssSUFBSSxJQUFJO3dCQUNsQixVQUFVLEVBQUUsUUFBUSxDQUFDLGtCQUFrQjt3QkFDdkMsTUFBTSxFQUFFLEVBQUU7d0JBQ1YsVUFBVSxFQUFFLE1BQU07d0JBQ2xCLE9BQU8sRUFBRSxRQUFRO3dCQUNqQixRQUFRLEVBQUUsRUFBRTt3QkFDWixZQUFZLEVBQUUsRUFBRTtxQkFDakIsQ0FDRjtnQkFuQjhCLENBbUI5QjtnQkFFRCxLQUFLLEVBQUUsVUFBQyxJQUFlO29CQUFmLHFCQUFBLEVBQUEsZUFBZTtvQkFBSyxPQUFBLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFO3dCQUM1QyxLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7d0JBQzFCLFVBQVUsRUFBRSxVQUFVO3dCQUN0QixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjt3QkFDckMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxjQUFjO3dCQUNuQyxRQUFRLEVBQUUsUUFBUTt3QkFDbEIsUUFBUSxFQUFFLEVBQUU7d0JBQ1osVUFBVSxFQUFFLE1BQU07d0JBQ2xCLFNBQVMsRUFBRSxNQUFNO3dCQUNqQixRQUFRLEVBQUUsTUFBTTtxQkFDakIsRUFDQyxPQUFPLEtBQUssSUFBSSxJQUFJO3dCQUNsQixVQUFVLEVBQUUsUUFBUSxDQUFDLGtCQUFrQjt3QkFDdkMsUUFBUSxFQUFFLEVBQUU7d0JBQ1osVUFBVSxFQUFFLE1BQU07d0JBQ2xCLFNBQVMsRUFBRSxPQUFPO3dCQUNsQixRQUFRLEVBQUUsS0FBSztxQkFDaEIsQ0FBQztnQkFqQndCLENBaUJ4QjthQUNMO1NBQ0Y7S0FDRjtDQUNNLENBQUMifQ==
// CONCATENATED MODULE: ./components/banner/magazine/view-desktop.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};





var renderList = function (_a) {
    var list = _a.list, _b = _a.type, type = _b === void 0 ? 'normal' : _b;
    return (react["createElement"]("div", null, Array.isArray(list)
        && list.map(function (item, index) {
            var linkProps = {
                key: "img-" + index,
                to: routing["Ma" /* ROUTING_MAGAZINE_DETAIL_PATH */] + "/" + item.slug,
                style: magazine_style.desktop.container.itemSlider(item.cover_image.large_url, type)
            };
            return (react["createElement"](react_router_dom["NavLink"], __assign({}, linkProps),
                react["createElement"]("div", { style: magazine_style.mobile.container.itemSliderPanel },
                    react["createElement"]("div", { style: [magazine_style.mobile.container.info, 'large' === type && magazine_style.mobile.container.info.large] },
                        react["createElement"]("div", { style: magazine_style.mobile.container.info.title(type) }, item.title)))));
        })));
};
var renderView = function (_a) {
    var props = _a.props;
    var list = props.list;
    var splitLayoutProps = {
        type: 'right',
        mainContainer: renderList({ list: list.slice(0, 1), type: 'large' }),
        subContainer: renderList({ list: list.slice(1, 4), type: 'normal' })
    };
    return (react["createElement"]("div", { className: 'magazine-banner', style: magazine_style.desktop },
        react["createElement"](split["a" /* default */], __assign({}, splitLayoutProps))));
};
/* harmony default export */ var view_desktop = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1kZXNrdG9wLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1kZXNrdG9wLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUUzQyxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUd0RixPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsT0FBTyxXQUFXLE1BQU0saUNBQWlDLENBQUM7QUFFMUQsSUFBTSxVQUFVLEdBQUcsVUFBQyxFQUF5QjtRQUF2QixjQUFJLEVBQUUsWUFBZSxFQUFmLG9DQUFlO0lBQU8sT0FBQSxDQUNoRCxpQ0FFSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztXQUNoQixJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBSSxFQUFFLEtBQUs7WUFDdEIsSUFBTSxTQUFTLEdBQUc7Z0JBQ2hCLEdBQUcsRUFBRSxTQUFPLEtBQU87Z0JBQ25CLEVBQUUsRUFBSyw0QkFBNEIsU0FBSSxJQUFJLENBQUMsSUFBTTtnQkFDbEQsS0FBSyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FDdkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQzFCLElBQUksQ0FDTDthQUNGLENBQUM7WUFFRixNQUFNLENBQUMsQ0FDTCxvQkFBQyxPQUFPLGVBQUssU0FBUztnQkFDcEIsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLGVBQWU7b0JBQ2hELDZCQUFLLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxPQUFPLEtBQUssSUFBSSxJQUFJLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7d0JBQzlGLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFHLElBQUksQ0FBQyxLQUFLLENBQU8sQ0FDbkUsQ0FDRixDQUNFLENBQ1gsQ0FBQztRQUNKLENBQUMsQ0FBQyxDQUVBLENBQ1A7QUExQmlELENBMEJqRCxDQUFDO0FBRUYsSUFBTSxVQUFVLEdBQUcsVUFBQyxFQUFTO1FBQVAsZ0JBQUs7SUFDakIsSUFBQSxpQkFBSSxDQUFxQjtJQUVqQyxJQUFNLGdCQUFnQixHQUFHO1FBQ3ZCLElBQUksRUFBRSxPQUFPO1FBQ2IsYUFBYSxFQUFFLFVBQVUsQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLENBQUM7UUFDcEUsWUFBWSxFQUFFLFVBQVUsQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLENBQUM7S0FDckUsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLFNBQVMsRUFBRSxpQkFBaUIsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU87UUFDckQsb0JBQUMsV0FBVyxlQUFLLGdCQUFnQixFQUFJLENBQ2pDLENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./components/banner/magazine/view-mobile.tsx
var view_mobile_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};




var view_mobile_renderView = function (_a) {
    var props = _a.props, state = _a.state, handleClick = _a.handleClick;
    var list = props.list;
    return (react["createElement"]("div", { className: 'magazine-banner', style: magazine_style.mobile },
        react["createElement"]("div", { style: magazine_style.mobile.container }, Array.isArray(list)
            && list.map(function (item, index) {
                var linkProps = {
                    key: "img-" + index,
                    to: routing["Ma" /* ROUTING_MAGAZINE_DETAIL_PATH */] + "/" + item.slug,
                    style: magazine_style.mobile.container.itemSlider(item.cover_image.large_url)
                };
                return (react["createElement"](react_router_dom["NavLink"], view_mobile_assign({}, linkProps),
                    react["createElement"]("div", { style: magazine_style.mobile.container.itemSliderPanel },
                        react["createElement"]("div", { style: magazine_style.mobile.container.info },
                            react["createElement"]("div", { style: magazine_style.mobile.container.info.title() }, item.title)))));
            }))));
};
/* harmony default export */ var view_mobile = (view_mobile_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1tb2JpbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LW1vYmlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUMvQixPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFJM0MsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFFdEYsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRzVCLElBQU0sVUFBVSxHQUFHLFVBQUMsRUFBNkI7UUFBM0IsZ0JBQUssRUFBRSxnQkFBSyxFQUFFLDRCQUFXO0lBQ3JDLElBQUEsaUJBQUksQ0FBcUI7SUFFakMsTUFBTSxDQUFDLENBQ0wsNkJBQUssU0FBUyxFQUFFLGlCQUFpQixFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTTtRQUNwRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTLElBRTlCLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO2VBQ2hCLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFJLEVBQUUsS0FBSztnQkFDdEIsSUFBTSxTQUFTLEdBQUc7b0JBQ2hCLEdBQUcsRUFBRSxTQUFPLEtBQU87b0JBQ25CLEVBQUUsRUFBSyw0QkFBNEIsU0FBSSxJQUFJLENBQUMsSUFBTTtvQkFDbEQsS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQztpQkFDckUsQ0FBQztnQkFFRixNQUFNLENBQUMsQ0FDTCxvQkFBQyxPQUFPLGVBQUssU0FBUztvQkFDcEIsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLGVBQWU7d0JBQ2hELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJOzRCQUNyQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFHLElBQUksQ0FBQyxLQUFLLENBQU8sQ0FDL0QsQ0FDRixDQUNFLENBQ1gsQ0FBQztZQUNKLENBQUMsQ0FBQyxDQUVBLENBQ0YsQ0FDUCxDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// CONCATENATED MODULE: ./components/banner/magazine/view.tsx


var view_renderView = function (_a) {
    var props = _a.props, state = _a.state, handleClick = _a.handleClick;
    var switchView = {
        MOBILE: function () { return view_mobile({ props: props, state: state, handleClick: handleClick }); },
        DESKTOP: function () { return view_desktop({ props: props }); }
    };
    return switchView[window.DEVICE_VERSION]();
};
/* harmony default export */ var view = (view_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sYUFBYSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sWUFBWSxNQUFNLGVBQWUsQ0FBQztBQUV6QyxJQUFNLFVBQVUsR0FBRyxVQUFDLEVBQTZCO1FBQTNCLGdCQUFLLEVBQUUsZ0JBQUssRUFBRSw0QkFBVztJQUM3QyxJQUFNLFVBQVUsR0FBRztRQUNqQixNQUFNLEVBQUUsY0FBTSxPQUFBLFlBQVksQ0FBQyxFQUFFLEtBQUssT0FBQSxFQUFFLEtBQUssT0FBQSxFQUFFLFdBQVcsYUFBQSxFQUFFLENBQUMsRUFBM0MsQ0FBMkM7UUFDekQsT0FBTyxFQUFFLGNBQU0sT0FBQSxhQUFhLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxDQUFDLEVBQXhCLENBQXdCO0tBQ3hDLENBQUM7SUFFRixNQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDO0FBQzdDLENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./components/banner/magazine/initialize.tsx
var DEFAULT_PROPS = {
    list: []
};
var INITIAL_STATE = {
    imgNum: 0
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtDQUNDLENBQUM7QUFFWixNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUc7SUFDM0IsTUFBTSxFQUFFLENBQUM7Q0FDQSxDQUFDIn0=
// CONCATENATED MODULE: ./components/banner/magazine/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_BannerMagazine = /** @class */ (function (_super) {
    __extends(BannerMagazine, _super);
    function BannerMagazine(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    BannerMagazine.prototype.handleClick = function (isNext) {
        var list = this.props.list;
        var imgNum = this.state.imgNum || 0;
        var num = true === isNext
            ? imgNum === list.length - 1 ? 0 : imgNum + 1
            : imgNum === 0 ? list.length - 1 : imgNum - 1;
        this.setState({ imgNum: num });
    };
    BannerMagazine.prototype.shouldComponentUpdate = function (nextProps, nextState) {
        var listLen = this.props.list && this.props.list.length || 0;
        var nextListLen = nextProps.list && nextProps.list.length || 0;
        if (listLen !== nextListLen) {
            return true;
        }
        if (this.state.imgNum !== nextState.imgNum) {
            return true;
        }
        return false;
    };
    BannerMagazine.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state,
            handleClick: this.handleClick.bind(this)
        };
        return view(args);
    };
    ;
    BannerMagazine.defaultProps = DEFAULT_PROPS;
    BannerMagazine = __decorate([
        radium
    ], BannerMagazine);
    return BannerMagazine;
}(react["Component"]));
/* harmony default export */ var component = (component_BannerMagazine);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUNsQyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFFaEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFHNUQ7SUFBNkIsa0NBQXlCO0lBRXBELHdCQUFZLEtBQWE7UUFBekIsWUFDRSxrQkFBTSxLQUFLLENBQUMsU0FFYjtRQURDLEtBQUksQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDOztJQUM3QixDQUFDO0lBRUQsb0NBQVcsR0FBWCxVQUFZLE1BQU07UUFDUixJQUFBLHNCQUFJLENBQWdCO1FBQzVCLElBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQztRQUN0QyxJQUFNLEdBQUcsR0FBRyxJQUFJLEtBQUssTUFBTTtZQUN6QixDQUFDLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDO1lBQzdDLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVELDhDQUFxQixHQUFyQixVQUFzQixTQUFpQixFQUFFLFNBQWlCO1FBQ3hELElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUM7UUFDL0QsSUFBTSxXQUFXLEdBQUcsU0FBUyxDQUFDLElBQUksSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUM7UUFDakUsRUFBRSxDQUFDLENBQUMsT0FBTyxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQUMsQ0FBQztRQUM3QyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFBQyxDQUFDO1FBRTVELE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQsK0JBQU0sR0FBTjtRQUNFLElBQU0sSUFBSSxHQUFHO1lBQ1gsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ3pDLENBQUM7UUFFRixNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFBQSxDQUFDO0lBaENLLDJCQUFZLEdBQVcsYUFBYSxDQUFDO0lBRHhDLGNBQWM7UUFEbkIsTUFBTTtPQUNELGNBQWMsQ0FrQ25CO0lBQUQscUJBQUM7Q0FBQSxBQWxDRCxDQUE2QixTQUFTLEdBa0NyQztBQUVELGVBQWUsY0FBYyxDQUFDIn0=
// CONCATENATED MODULE: ./components/banner/magazine/index.tsx

/* harmony default export */ var banner_magazine = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxjQUFjLE1BQU0sYUFBYSxDQUFDO0FBQ3pDLGVBQWUsY0FBYyxDQUFDIn0=
// EXTERNAL MODULE: ./components/magazine/category/index.tsx + 7 modules
var category = __webpack_require__(797);

// EXTERNAL MODULE: ./components/magazine/right-widget-group/index.tsx + 10 modules
var right_widget_group = __webpack_require__(845);

// EXTERNAL MODULE: ./components/ui/loading-placeholder/index.tsx + 4 modules
var loading_placeholder = __webpack_require__(750);

// CONCATENATED MODULE: ./components/container/mobile-category-slide/style.tsx

/* harmony default export */ var mobile_category_slide_style = ({
    display: variable["display"].block,
    overflowX: 'auto',
    paddingTop: 10,
    paddingBottom: 10,
    margin: 0,
    container: {
        display: variable["display"].block,
        whiteSpace: 'nowrap',
        width: '100%'
    },
    categoryWrap: function (url, id, type) {
        if (id === void 0) { id = 0; }
        if (type === void 0) { type = ''; }
        var typeList = {
            first: { marginLeft: 10 },
            last: { marginRight: 10 }
        };
        return Object.assign({}, {
            marginTop: 0,
            marginBottom: 0,
            marginLeft: 5,
            marginRight: 5,
            display: variable["display"].inlineBlock,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            height: 50,
            lineHeight: '52px',
            borderRadius: 5,
            backgroundColor: variable["randomColorList"](-1),
            alignItems: 'center',
            justifyContent: 'center',
            color: variable["colorWhite"],
            fontSize: 20,
            paddingLeft: 10,
            paddingRight: 10,
            textTransform: 'uppercase',
            fontFamily: variable["fontAvenirDemiBold"],
            boxShadow: variable["shadowBlur"],
        }, '' !== type && typeList[type]);
    },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxlQUFlO0lBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztJQUMvQixTQUFTLEVBQUUsTUFBTTtJQUNqQixVQUFVLEVBQUUsRUFBRTtJQUNkLGFBQWEsRUFBRSxFQUFFO0lBQ2pCLE1BQU0sRUFBRSxDQUFDO0lBRVQsU0FBUyxFQUFFO1FBQ1QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztRQUMvQixVQUFVLEVBQUUsUUFBUTtRQUNwQixLQUFLLEVBQUUsTUFBTTtLQUNkO0lBRUQsWUFBWSxFQUFFLFVBQUMsR0FBRyxFQUFFLEVBQU0sRUFBRSxJQUFTO1FBQWpCLG1CQUFBLEVBQUEsTUFBTTtRQUFFLHFCQUFBLEVBQUEsU0FBUztRQUVuQyxJQUFNLFFBQVEsR0FBRztZQUNmLEtBQUssRUFBRSxFQUFFLFVBQVUsRUFBRSxFQUFFLEVBQUU7WUFDekIsSUFBSSxFQUFFLEVBQUUsV0FBVyxFQUFFLEVBQUUsRUFBRTtTQUMxQixDQUFDO1FBRUYsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUNyQjtZQUNFLFNBQVMsRUFBRSxDQUFDO1lBQ1osWUFBWSxFQUFFLENBQUM7WUFDZixVQUFVLEVBQUUsQ0FBQztZQUNiLFdBQVcsRUFBRSxDQUFDO1lBQ2QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztZQUNyQyxjQUFjLEVBQUUsT0FBTztZQUN2QixnQkFBZ0IsRUFBRSxXQUFXO1lBQzdCLE1BQU0sRUFBRSxFQUFFO1lBQ1YsVUFBVSxFQUFFLE1BQU07WUFDbEIsWUFBWSxFQUFFLENBQUM7WUFDZixlQUFlLEVBQUUsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3QyxVQUFVLEVBQUUsUUFBUTtZQUNwQixjQUFjLEVBQUUsUUFBUTtZQUN4QixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDMUIsUUFBUSxFQUFFLEVBQUU7WUFDWixXQUFXLEVBQUUsRUFBRTtZQUNmLFlBQVksRUFBRSxFQUFFO1lBQ2hCLGFBQWEsRUFBRSxXQUFXO1lBQzFCLFVBQVUsRUFBRSxRQUFRLENBQUMsa0JBQWtCO1lBQ3ZDLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTtTQUMvQixFQUNELEVBQUUsS0FBSyxJQUFJLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxDQUM5QixDQUFBO0lBQ0gsQ0FBQztDQUdNLENBQUMifQ==
// CONCATENATED MODULE: ./components/container/mobile-category-slide/view.tsx
var view_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};




function renderComponent(_a) {
    var props = _a.props, state = _a.state;
    var list = props.list, style = props.style, path = props.path;
    var renderItem = function (item, index) {
        var navLinkProps = {
            to: path + "/" + item.slug,
            key: "category-slide-" + index,
            style: mobile_category_slide_style.categoryWrap(item.img_url, index, 0 === index ? 'first' : (list.length - 1 === index ? 'last' : '')),
            className: 'mobile-category-gradient'
        };
        return (react["createElement"](react_router_dom["NavLink"], view_assign({}, navLinkProps), Object(encode["f" /* decodeEntities */])(item.name)));
    };
    return (react["createElement"]("mobile-category-slide", { style: [mobile_category_slide_style, style] },
        react["createElement"]("div", { style: mobile_category_slide_style.container }, Array.isArray(list) && list.map(renderItem))));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUV2RCxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFHNUIsTUFBTSwwQkFBMEIsRUFBZ0I7UUFBZCxnQkFBSyxFQUFFLGdCQUFLO0lBQ3BDLElBQUEsaUJBQUksRUFBRSxtQkFBSyxFQUFFLGlCQUFJLENBQVc7SUFFcEMsSUFBTSxVQUFVLEdBQUcsVUFBQyxJQUFJLEVBQUUsS0FBSztRQUM3QixJQUFNLFlBQVksR0FBRztZQUNuQixFQUFFLEVBQUssSUFBSSxTQUFJLElBQUksQ0FBQyxJQUFNO1lBQzFCLEdBQUcsRUFBRSxvQkFBa0IsS0FBTztZQUM5QixLQUFLLEVBQUUsS0FBSyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEtBQUssRUFBRSxDQUFDLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2pILFNBQVMsRUFBRSwwQkFBMEI7U0FDdEMsQ0FBQTtRQUVELE1BQU0sQ0FBQyxDQUNMLG9CQUFDLE9BQU8sZUFBSyxZQUFZLEdBQ3RCLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQ2xCLENBQ1gsQ0FBQTtJQUNILENBQUMsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLCtDQUF1QixLQUFLLEVBQUUsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDO1FBQzFDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxJQUN4QixLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQ3hDLENBQ2dCLENBQ3pCLENBQUM7QUFDSixDQUFDO0FBQUEsQ0FBQyJ9
// CONCATENATED MODULE: ./components/container/mobile-category-slide/initialize.tsx
var initialize_DEFAULT_PROPS = {
    list: [],
    style: {},
    path: ''
};
var initialize_INITIAL_STATE = {};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLEtBQUssRUFBRSxFQUFFO0lBQ1QsSUFBSSxFQUFFLEVBQUU7Q0FDVCxDQUFDO0FBRUYsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHLEVBQUUsQ0FBQyJ9
// CONCATENATED MODULE: ./components/container/mobile-category-slide/component.tsx
var component_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var component_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_MobileCategorySlide = /** @class */ (function (_super) {
    component_extends(MobileCategorySlide, _super);
    function MobileCategorySlide(props) {
        var _this = _super.call(this, props) || this;
        _this.state = initialize_INITIAL_STATE;
        return _this;
    }
    MobileCategorySlide.prototype.shouldComponentUpdate = function (nextProps) {
        var currentListCount = this.props.list.length || 0;
        var nextListCount = nextProps.list.length || 0;
        if (0 === nextListCount) {
            return false;
        }
        if (currentListCount !== nextListCount) {
            return true;
        }
        return false;
    };
    MobileCategorySlide.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state,
        };
        return renderComponent(args);
    };
    MobileCategorySlide.defaultProps = initialize_DEFAULT_PROPS;
    MobileCategorySlide = component_decorate([
        radium
    ], MobileCategorySlide);
    return MobileCategorySlide;
}(react["Component"]));
;
/* harmony default export */ var mobile_category_slide_component = (component_MobileCategorySlide);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUNsQyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sUUFBUSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBSTVEO0lBQWtDLHVDQUF5QjtJQUd6RCw2QkFBWSxLQUFLO1FBQWpCLFlBQ0Usa0JBQU0sS0FBSyxDQUFDLFNBRWI7UUFEQyxLQUFJLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQzs7SUFDN0IsQ0FBQztJQUVELG1EQUFxQixHQUFyQixVQUFzQixTQUFTO1FBQzdCLElBQU0sZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQztRQUNyRCxJQUFNLGFBQWEsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUM7UUFFakQsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUMsS0FBSyxDQUFBO1FBQUMsQ0FBQztRQUN6QyxFQUFFLENBQUMsQ0FBQyxnQkFBZ0IsS0FBSyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQTtRQUFDLENBQUM7UUFFdkQsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFRCxvQ0FBTSxHQUFOO1FBQ0UsSUFBTSxJQUFJLEdBQUc7WUFDWCxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1NBQ2xCLENBQUE7UUFDRCxNQUFNLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUF2Qk0sZ0NBQVksR0FBRyxhQUF1QixDQUFDO0lBRDFDLG1CQUFtQjtRQUR4QixNQUFNO09BQ0QsbUJBQW1CLENBeUJ4QjtJQUFELDBCQUFDO0NBQUEsQUF6QkQsQ0FBa0MsU0FBUyxHQXlCMUM7QUFBQSxDQUFDO0FBRUYsZUFBZSxtQkFBbUIsQ0FBQyJ9
// CONCATENATED MODULE: ./components/container/mobile-category-slide/index.tsx

/* harmony default export */ var mobile_category_slide = (mobile_category_slide_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxtQkFBbUIsTUFBTSxhQUFhLENBQUM7QUFDOUMsZUFBZSxtQkFBbUIsQ0FBQyJ9
// EXTERNAL MODULE: ./constants/application/magazine-widget.ts
var magazine_widget = __webpack_require__(842);

// EXTERNAL MODULE: ./constants/application/magazine-category.ts
var magazine_category = __webpack_require__(790);

// CONCATENATED MODULE: ./container/app-shop/magazine/index/style.tsx

/* harmony default export */ var index_style = ({
    container: {
        paddingTop: 20
    },
    tempText: {
        width: '100%',
        textAlign: 'center',
        fontSize: 20,
        lineHeight: '150px'
    },
    placeholder: {
        width: '100%',
        paddingTop: 20,
        imgCover: {
            width: '100%',
            height: 510,
            backgroundColor: variable["colorE5"],
            position: variable["position"].relative,
            marginBottom: 30,
            item: {
                position: variable["position"].absolute,
                left: 0,
                bottom: 25,
                backgroundColor: variable["colorWhite08"],
                paddingTop: 20,
                paddingRight: 20,
                paddingBottom: 20,
                paddingLeft: 20,
                width: '50%',
                height: 125,
                display: variable["display"].flex,
                justifyContent: 'space-between',
                alignItems: 'center',
                flexDirection: 'column',
                text: {
                    width: '100%',
                    height: 20,
                }
            }
        },
        title: {
            background: variable["colorF0"],
            display: variable["display"].block,
            width: 100,
            height: 40,
            margin: '0 auto 30px'
        },
        titleMobile: {
            margin: 0,
            textAlign: 'left'
        },
        control: {
            width: '100%',
            display: variable["display"].flex,
            justifyContent: 'space-between',
            paddingLeft: 10,
            paddingRight: 10,
            marginBottom: 20,
        },
        controlItem: {
            width: 150,
            height: 30,
            background: variable["colorF7"]
        },
        productList: {
            display: variable["display"].flex,
            flexWrap: 'wrap'
        },
        productMobileItem: {
            minWidth: '50%',
            width: '50%',
        },
        productItem: {
            flex: 1,
            paddingLeft: 10,
            paddingRight: 10,
            marginBottom: 20,
            minWidth: '20%',
            width: '20%',
            image: {
                width: '100%',
                height: 'auto',
                paddingTop: '82%',
                marginBottom: 10,
            },
            text: {
                width: '94%',
                height: 25,
                marginBottom: 10,
            },
            lastText: {
                width: '65%',
                height: 25,
            }
        },
        mobile: {
            width: '100%',
            image: {
                width: '100%',
                height: 192,
                marginBottom: 30
            },
            text: {
                height: 48,
                paddingTop: 0,
                paddingRight: 10,
                paddingBottom: 10,
                paddingLeft: 0,
                marginBottom: 10,
                marginLeft: 10,
                marginRight: 10
            }
        },
        categorySlideWrap: {
            display: variable["display"].flex,
            padding: '0 10px',
            marginBottom: 20,
            justifyContent: 'space-between',
            categorySlide: {
                height: 50,
                width: 'calc(50% - 5px)'
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSw0QkFBNEIsQ0FBQztBQUN2RCxlQUFlO0lBQ2IsU0FBUyxFQUFFO1FBQ1QsVUFBVSxFQUFFLEVBQUU7S0FDZjtJQUVELFFBQVEsRUFBRTtRQUNSLEtBQUssRUFBRSxNQUFNO1FBQ2IsU0FBUyxFQUFFLFFBQVE7UUFDbkIsUUFBUSxFQUFFLEVBQUU7UUFDWixVQUFVLEVBQUUsT0FBTztLQUNwQjtJQUVELFdBQVcsRUFBRTtRQUNYLEtBQUssRUFBRSxNQUFNO1FBQ2IsVUFBVSxFQUFFLEVBQUU7UUFFZCxRQUFRLEVBQUU7WUFDUixLQUFLLEVBQUUsTUFBTTtZQUNiLE1BQU0sRUFBRSxHQUFHO1lBQ1gsZUFBZSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1lBQ2pDLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDcEMsWUFBWSxFQUFFLEVBQUU7WUFFaEIsSUFBSSxFQUFFO2dCQUNKLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLElBQUksRUFBRSxDQUFDO2dCQUNQLE1BQU0sRUFBRSxFQUFFO2dCQUNWLGVBQWUsRUFBRSxRQUFRLENBQUMsWUFBWTtnQkFDdEMsVUFBVSxFQUFFLEVBQUU7Z0JBQ2QsWUFBWSxFQUFFLEVBQUU7Z0JBQ2hCLGFBQWEsRUFBRSxFQUFFO2dCQUNqQixXQUFXLEVBQUUsRUFBRTtnQkFDZixLQUFLLEVBQUUsS0FBSztnQkFDWixNQUFNLEVBQUUsR0FBRztnQkFDWCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixjQUFjLEVBQUUsZUFBZTtnQkFDL0IsVUFBVSxFQUFFLFFBQVE7Z0JBQ3BCLGFBQWEsRUFBRSxRQUFRO2dCQUV2QixJQUFJLEVBQUU7b0JBQ0osS0FBSyxFQUFFLE1BQU07b0JBQ2IsTUFBTSxFQUFFLEVBQUU7aUJBQ1g7YUFDRjtTQUNGO1FBRUQsS0FBSyxFQUFFO1lBQ0wsVUFBVSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1lBQzVCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDL0IsS0FBSyxFQUFFLEdBQUc7WUFDVixNQUFNLEVBQUUsRUFBRTtZQUNWLE1BQU0sRUFBRSxhQUFhO1NBQ3RCO1FBRUQsV0FBVyxFQUFFO1lBQ1gsTUFBTSxFQUFFLENBQUM7WUFDVCxTQUFTLEVBQUUsTUFBTTtTQUNsQjtRQUVELE9BQU8sRUFBRTtZQUNQLEtBQUssRUFBRSxNQUFNO1lBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixjQUFjLEVBQUUsZUFBZTtZQUMvQixXQUFXLEVBQUUsRUFBRTtZQUNmLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFlBQVksRUFBRSxFQUFFO1NBQ2pCO1FBRUQsV0FBVyxFQUFFO1lBQ1gsS0FBSyxFQUFFLEdBQUc7WUFDVixNQUFNLEVBQUUsRUFBRTtZQUNWLFVBQVUsRUFBRSxRQUFRLENBQUMsT0FBTztTQUM3QjtRQUVELFdBQVcsRUFBRTtZQUNYLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsUUFBUSxFQUFFLE1BQU07U0FDakI7UUFFRCxpQkFBaUIsRUFBRTtZQUNqQixRQUFRLEVBQUUsS0FBSztZQUNmLEtBQUssRUFBRSxLQUFLO1NBQ2I7UUFFRCxXQUFXLEVBQUU7WUFDWCxJQUFJLEVBQUUsQ0FBQztZQUNQLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLEVBQUU7WUFDaEIsWUFBWSxFQUFFLEVBQUU7WUFDaEIsUUFBUSxFQUFFLEtBQUs7WUFDZixLQUFLLEVBQUUsS0FBSztZQUVaLEtBQUssRUFBRTtnQkFDTCxLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsTUFBTTtnQkFDZCxVQUFVLEVBQUUsS0FBSztnQkFDakIsWUFBWSxFQUFFLEVBQUU7YUFDakI7WUFFRCxJQUFJLEVBQUU7Z0JBQ0osS0FBSyxFQUFFLEtBQUs7Z0JBQ1osTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsWUFBWSxFQUFFLEVBQUU7YUFDakI7WUFFRCxRQUFRLEVBQUU7Z0JBQ1IsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osTUFBTSxFQUFFLEVBQUU7YUFDWDtTQUNGO1FBRUQsTUFBTSxFQUFFO1lBQ04sS0FBSyxFQUFFLE1BQU07WUFFYixLQUFLLEVBQUU7Z0JBQ0wsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsTUFBTSxFQUFFLEdBQUc7Z0JBQ1gsWUFBWSxFQUFFLEVBQUU7YUFDakI7WUFFRCxJQUFJLEVBQUU7Z0JBQ0osTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsVUFBVSxFQUFFLENBQUM7Z0JBQ2IsWUFBWSxFQUFFLEVBQUU7Z0JBQ2hCLGFBQWEsRUFBRSxFQUFFO2dCQUNqQixXQUFXLEVBQUUsQ0FBQztnQkFDZCxZQUFZLEVBQUUsRUFBRTtnQkFDaEIsVUFBVSxFQUFFLEVBQUU7Z0JBQ2QsV0FBVyxFQUFFLEVBQUU7YUFDaEI7U0FDRjtRQUVELGlCQUFpQixFQUFFO1lBQ2pCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsT0FBTyxFQUFFLFFBQVE7WUFDakIsWUFBWSxFQUFFLEVBQUU7WUFDaEIsY0FBYyxFQUFFLGVBQWU7WUFFL0IsYUFBYSxFQUFFO2dCQUNiLE1BQU0sRUFBRSxFQUFFO2dCQUNWLEtBQUssRUFBRSxpQkFBaUI7YUFDekI7U0FDRjtLQUNGO0NBQ00sQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/magazine/index/view.tsx
var index_view_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};

















var renderItemPlaceholder = function (item) { return (react["createElement"]("div", { style: [
        index_style.placeholder.productItem,
        'MOBILE' === window.DEVICE_VERSION && index_style.placeholder.productMobileItem
    ], key: item },
    react["createElement"](loading_placeholder["a" /* default */], { style: index_style.placeholder.productItem.image }),
    react["createElement"](loading_placeholder["a" /* default */], { style: index_style.placeholder.productItem.text }),
    react["createElement"](loading_placeholder["a" /* default */], { style: index_style.placeholder.productItem.text }),
    react["createElement"](loading_placeholder["a" /* default */], { style: index_style.placeholder.productItem.lastText }))); };
var renderSubContainer = function (_a) {
    var magazineDefaultList = _a.magazineDefaultList, widgetList = _a.widgetList, expertsReviewList = _a.expertsReviewList;
    var widgetProps = {
        dataList: [
            {
                list: magazineDefaultList,
                type: magazine_widget["a" /* MAGAZINE_WIDGET_TYPE */].ONE.type,
                title: 'Đừng bỏ lỡ'
            },
            {
                list: widgetList,
                type: magazine_widget["a" /* MAGAZINE_WIDGET_TYPE */].TWO.type,
                title: 'Ingredient'
            },
            {
                list: expertsReviewList,
                type: magazine_widget["a" /* MAGAZINE_WIDGET_TYPE */].TWO.type,
                title: magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].EXPERTS_REVIEW.title
            }
        ]
    };
    return react["createElement"](right_widget_group["a" /* default */], index_view_assign({}, widgetProps));
};
var renderMagazineCategory = function (_a) {
    var list = _a.list;
    return Array.isArray(list)
        && list.map(function (item, index) {
            if (magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].THREE.type !== index && magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].EXPERTS_REVIEW.type !== index) {
                var magazineCategoryProps = {
                    key: "magazine-category-" + index,
                    title: item.category_name,
                    list: item.magazines,
                    type: 1,
                    url: routing["Ka" /* ROUTING_MAGAZINE_CATEGORY_PATH */] + "/" + item.category_slug
                };
                return react["createElement"](category["a" /* default */], index_view_assign({}, magazineCategoryProps));
            }
        });
};
var renderMainContainer = function (_a) {
    var magazineList = _a.magazineList, _b = _a.categories, categories = _b === void 0 ? [] : _b;
    var categorySlideProps = {
        list: categories || [],
        style: { marginBottom: 0, paddingTop: 0 },
        path: routing["Ka" /* ROUTING_MAGAZINE_CATEGORY_PATH */]
    };
    return (react["createElement"](fade_in["a" /* default */], null,
        react["createElement"](mobile_category_slide, index_view_assign({}, categorySlideProps)),
        renderMagazineCategory({ list: magazineList })));
};
var index_view_renderView = function (_a) {
    var props = _a.props, state = _a.state, handleFetchMagazineVideo = _a.handleFetchMagazineVideo;
    var _b = props, magazineVideoParams = _b.magazineVideoParams, magazineDefaultParams = _b.magazineDefaultParams, _c = _b.magazineStore, magazineDashboard = _c.magazineDashboard, magazineList = _c.magazineList;
    var _d = state, isPriorityBlock = _d.isPriorityBlock, isFetchMagazineVideo = _d.isFetchMagazineVideo;
    var widgetList = !Object(validate["l" /* isUndefined */])(magazineDashboard.magazines) ? magazineDashboard.magazines[magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].THREE.type].magazines : [];
    var expertsReviewList = !Object(validate["l" /* isUndefined */])(magazineDashboard.magazines) ? magazineDashboard.magazines[magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].EXPERTS_REVIEW.type].magazines : [];
    var keyHashMagazineVideo = Object(encode["h" /* objectToHash */])(magazineVideoParams);
    var videoList = !Object(validate["l" /* isUndefined */])(magazineList[keyHashMagazineVideo]) ? magazineList[keyHashMagazineVideo] : [];
    var keyHashMagazineDefault = Object(encode["h" /* objectToHash */])(magazineDefaultParams);
    var magazineDefaultList = !Object(validate["l" /* isUndefined */])(magazineList[keyHashMagazineDefault]) ? magazineList[keyHashMagazineDefault] : [];
    var splitLayoutProps = {
        type: 'right',
        subContainer: renderSubContainer({ magazineDefaultList: magazineDefaultList, widgetList: widgetList, expertsReviewList: expertsReviewList }),
        mainContainer: renderMagazineCategory({ list: magazineDashboard.magazines || [] })
    };
    var renderMagazineVideos = function (isVisible) {
        !!isVisible
            && !isFetchMagazineVideo
            && !!handleFetchMagazineVideo
            && handleFetchMagazineVideo();
        return react["createElement"](category["a" /* default */], { list: videoList, type: magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].VIDEO.type });
    };
    var switchView = {
        MOBILE: function () { return (react["createElement"]("div", null,
            react["createElement"](banner_magazine, { list: magazineDefaultList }),
            renderMainContainer({ magazineList: magazineDashboard.magazines || [], categories: magazineDashboard.categories || [] }),
            !isPriorityBlock
                ? (react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                    var isVisible = _a.isVisible;
                    return renderMagazineVideos(isVisible);
                })) : react["createElement"](loading["a" /* default */], { style: { height: 400 } }),
            react["createElement"](category["a" /* default */], { list: expertsReviewList, type: magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].EXPERTS_REVIEW.type, title: magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].EXPERTS_REVIEW.title }))); },
        DESKTOP: function () { return (react["createElement"](wrap["a" /* default */], { style: index_style.container },
            react["createElement"](banner_magazine, { list: magazineDefaultList }),
            react["createElement"](split["a" /* default */], index_view_assign({}, splitLayoutProps)),
            !isPriorityBlock
                ? (react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                    var isVisible = _a.isVisible;
                    return renderMagazineVideos(isVisible);
                })) : react["createElement"](loading["a" /* default */], { style: { height: 400 } }))); }
    };
    return (react["createElement"]("magazine-index-container", null, switchView[window.DEVICE_VERSION]()));
};
/* harmony default export */ var index_view = (index_view_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxlQUFlLE1BQU0saUJBQWlCLENBQUM7QUFFOUMsT0FBTyxNQUFNLE1BQU0seUJBQXlCLENBQUM7QUFDN0MsT0FBTyxPQUFPLE1BQU0sbUNBQW1DLENBQUM7QUFDeEQsT0FBTyxVQUFVLE1BQU0sc0JBQXNCLENBQUM7QUFDOUMsT0FBTyxXQUFXLE1BQU0sdUJBQXVCLENBQUM7QUFDaEQsT0FBTyxjQUFjLE1BQU0sd0NBQXdDLENBQUM7QUFDcEUsT0FBTyxnQkFBZ0IsTUFBTSwwQ0FBMEMsQ0FBQztBQUN4RSxPQUFPLGdCQUFnQixNQUFNLG9EQUFvRCxDQUFDO0FBQ2xGLE9BQU8sa0JBQWtCLE1BQU0sK0NBQStDLENBQUM7QUFDL0UsT0FBTyxtQkFBbUIsTUFBTSx3REFBd0QsQ0FBQztBQUV6RixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDekQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLG1EQUFtRCxDQUFDO0FBQ3pGLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHFEQUFxRCxDQUFDO0FBQzdGLE9BQU8sRUFBRSw4QkFBOEIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBRzNGLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixNQUFNLENBQUMsSUFBTSxxQkFBcUIsR0FBRyxVQUFDLElBQUksSUFBSyxPQUFBLENBQzdDLDZCQUNFLEtBQUssRUFBRTtRQUNMLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVztRQUM3QixRQUFRLEtBQUssTUFBTSxDQUFDLGNBQWMsSUFBSSxLQUFLLENBQUMsV0FBVyxDQUFDLGlCQUFpQjtLQUMxRSxFQUNELEdBQUcsRUFBRSxJQUFJO0lBQ1Qsb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBSTtJQUNsRSxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxHQUFJO0lBQ2pFLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLEdBQUk7SUFDakUsb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLFFBQVEsR0FBSSxDQUNqRSxDQUNQLEVBWjhDLENBWTlDLENBQUM7QUFFRixJQUFNLGtCQUFrQixHQUFHLFVBQUMsRUFBc0Q7UUFBcEQsNENBQW1CLEVBQUUsMEJBQVUsRUFBRSx3Q0FBaUI7SUFDOUUsSUFBTSxXQUFXLEdBQUc7UUFDbEIsUUFBUSxFQUFFO1lBQ1I7Z0JBQ0UsSUFBSSxFQUFFLG1CQUFtQjtnQkFDekIsSUFBSSxFQUFFLG9CQUFvQixDQUFDLEdBQUcsQ0FBQyxJQUFJO2dCQUNuQyxLQUFLLEVBQUUsWUFBWTthQUNwQjtZQUNEO2dCQUNFLElBQUksRUFBRSxVQUFVO2dCQUNoQixJQUFJLEVBQUUsb0JBQW9CLENBQUMsR0FBRyxDQUFDLElBQUk7Z0JBQ25DLEtBQUssRUFBRSxZQUFZO2FBQ3BCO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLGlCQUFpQjtnQkFDdkIsSUFBSSxFQUFFLG9CQUFvQixDQUFDLEdBQUcsQ0FBQyxJQUFJO2dCQUNuQyxLQUFLLEVBQUUsc0JBQXNCLENBQUMsY0FBYyxDQUFDLEtBQUs7YUFDbkQ7U0FDRjtLQUNGLENBQUM7SUFFRixNQUFNLENBQUMsb0JBQUMsZ0JBQWdCLGVBQUssV0FBVyxFQUFJLENBQUM7QUFDL0MsQ0FBQyxDQUFDO0FBRUYsSUFBTSxzQkFBc0IsR0FBRyxVQUFDLEVBQVE7UUFBTixjQUFJO0lBQ3BDLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztXQUNyQixJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBSSxFQUFFLEtBQUs7WUFDdEIsRUFBRSxDQUFDLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxLQUFLLElBQUksc0JBQXNCLENBQUMsY0FBYyxDQUFDLElBQUksS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUN4RyxJQUFNLHFCQUFxQixHQUFHO29CQUM1QixHQUFHLEVBQUUsdUJBQXFCLEtBQU87b0JBQ2pDLEtBQUssRUFBRSxJQUFJLENBQUMsYUFBYTtvQkFDekIsSUFBSSxFQUFFLElBQUksQ0FBQyxTQUFTO29CQUNwQixJQUFJLEVBQUUsQ0FBQztvQkFDUCxHQUFHLEVBQUssOEJBQThCLFNBQUksSUFBSSxDQUFDLGFBQWU7aUJBQy9ELENBQUM7Z0JBRUYsTUFBTSxDQUFDLG9CQUFDLGdCQUFnQixlQUFLLHFCQUFxQixFQUFJLENBQUM7WUFDekQsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFBO0FBQ04sQ0FBQyxDQUFBO0FBRUQsSUFBTSxtQkFBbUIsR0FBRyxVQUFDLEVBQWlDO1FBQS9CLDhCQUFZLEVBQUUsa0JBQWUsRUFBZixvQ0FBZTtJQUMxRCxJQUFNLGtCQUFrQixHQUFHO1FBQ3pCLElBQUksRUFBRSxVQUFVLElBQUksRUFBRTtRQUN0QixLQUFLLEVBQUUsRUFBRSxZQUFZLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxDQUFDLEVBQUU7UUFDekMsSUFBSSxFQUFFLDhCQUE4QjtLQUNyQyxDQUFBO0lBRUQsTUFBTSxDQUFDLENBQ0wsb0JBQUMsTUFBTTtRQUNMLG9CQUFDLG1CQUFtQixlQUFLLGtCQUFrQixFQUFJO1FBQzlDLHNCQUFzQixDQUFDLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxDQUFDLENBQ3hDLENBQ1YsQ0FBQztBQUVKLENBQUMsQ0FBQztBQUVGLElBQU0sVUFBVSxHQUFHLFVBQUMsRUFBMEM7UUFBeEMsZ0JBQUssRUFBRSxnQkFBSyxFQUFFLHNEQUF3QjtJQUNwRCxJQUFBLFVBSWEsRUFIakIsNENBQW1CLEVBQ25CLGdEQUFxQixFQUNyQixxQkFBa0QsRUFBakMsd0NBQWlCLEVBQUUsOEJBQVksQ0FDOUI7SUFFZCxJQUFBLFVBQTJELEVBQXpELG9DQUFlLEVBQUUsOENBQW9CLENBQXFCO0lBRWxFLElBQU0sVUFBVSxHQUFHLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQzdJLElBQU0saUJBQWlCLEdBQUcsQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFFN0osSUFBTSxvQkFBb0IsR0FBRyxZQUFZLENBQUMsbUJBQW1CLENBQUMsQ0FBQztJQUMvRCxJQUFNLFNBQVMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBRTdHLElBQU0sc0JBQXNCLEdBQUcsWUFBWSxDQUFDLHFCQUFxQixDQUFDLENBQUM7SUFDbkUsSUFBTSxtQkFBbUIsR0FBRyxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBRTNILElBQU0sZ0JBQWdCLEdBQUc7UUFDdkIsSUFBSSxFQUFFLE9BQU87UUFDYixZQUFZLEVBQUUsa0JBQWtCLENBQUMsRUFBRSxtQkFBbUIscUJBQUEsRUFBRSxVQUFVLFlBQUEsRUFBRSxpQkFBaUIsbUJBQUEsRUFBRSxDQUFDO1FBQ3hGLGFBQWEsRUFBRSxzQkFBc0IsQ0FBQyxFQUFFLElBQUksRUFBRSxpQkFBaUIsQ0FBQyxTQUFTLElBQUksRUFBRSxFQUFFLENBQUM7S0FDbkYsQ0FBQztJQUVGLElBQU0sb0JBQW9CLEdBQUcsVUFBQyxTQUFTO1FBQ3JDLENBQUMsQ0FBQyxTQUFTO2VBQ04sQ0FBQyxvQkFBb0I7ZUFDckIsQ0FBQyxDQUFDLHdCQUF3QjtlQUMxQix3QkFBd0IsRUFBRSxDQUFDO1FBRWhDLE1BQU0sQ0FBQyxvQkFBQyxnQkFBZ0IsSUFBQyxJQUFJLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFJLENBQUE7SUFDdkYsQ0FBQyxDQUFBO0lBRUQsSUFBTSxVQUFVLEdBQUc7UUFDakIsTUFBTSxFQUFFLGNBQU0sT0FBQSxDQUNaO1lBQ0Usb0JBQUMsY0FBYyxJQUFDLElBQUksRUFBRSxtQkFBbUIsR0FBSTtZQUM1QyxtQkFBbUIsQ0FBQyxFQUFFLFlBQVksRUFBRSxpQkFBaUIsQ0FBQyxTQUFTLElBQUksRUFBRSxFQUFFLFVBQVUsRUFBRSxpQkFBaUIsQ0FBQyxVQUFVLElBQUksRUFBRSxFQUFFLENBQUM7WUFFdkgsQ0FBQyxlQUFlO2dCQUNkLENBQUMsQ0FBQyxDQUNBLG9CQUFDLGVBQWUsSUFBQyxNQUFNLEVBQUUsR0FBRyxJQUN6QixVQUFDLEVBQWE7d0JBQVgsd0JBQVM7b0JBQU8sT0FBQSxvQkFBb0IsQ0FBQyxTQUFTLENBQUM7Z0JBQS9CLENBQStCLENBQ25DLENBQ25CLENBQUMsQ0FBQyxDQUFDLG9CQUFDLE9BQU8sSUFBQyxLQUFLLEVBQUUsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLEdBQUk7WUFFM0Msb0JBQUMsZ0JBQWdCLElBQUMsSUFBSSxFQUFFLGlCQUFpQixFQUFFLElBQUksRUFBRSxzQkFBc0IsQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsQ0FBQyxjQUFjLENBQUMsS0FBSyxHQUFJLENBRS9JLENBQ1AsRUFmYSxDQWViO1FBQ0QsT0FBTyxFQUFFLGNBQU0sT0FBQSxDQUNiLG9CQUFDLFVBQVUsSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFNBQVM7WUFDaEMsb0JBQUMsY0FBYyxJQUFDLElBQUksRUFBRSxtQkFBbUIsR0FBSTtZQUM3QyxvQkFBQyxXQUFXLGVBQUssZ0JBQWdCLEVBQUk7WUFFbkMsQ0FBQyxlQUFlO2dCQUNkLENBQUMsQ0FBQyxDQUNBLG9CQUFDLGVBQWUsSUFBQyxNQUFNLEVBQUUsR0FBRyxJQUN6QixVQUFDLEVBQWE7d0JBQVgsd0JBQVM7b0JBQU8sT0FBQSxvQkFBb0IsQ0FBQyxTQUFTLENBQUM7Z0JBQS9CLENBQStCLENBQ25DLENBQ25CLENBQUMsQ0FBQyxDQUFDLG9CQUFDLE9BQU8sSUFBQyxLQUFLLEVBQUUsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLEdBQUksQ0FFaEMsQ0FDZCxFQWJjLENBYWQ7S0FDRixDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsc0RBQ0csVUFBVSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUNYLENBQzVCLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// EXTERNAL MODULE: ./constants/application/magazine.ts
var application_magazine = __webpack_require__(170);

// CONCATENATED MODULE: ./container/app-shop/magazine/index/initialize.tsx

var index_initialize_DEFAULT_PROPS = {
    magazineDefaultParams: { page: 1, perPage: 4, type: application_magazine["a" /* MAGAZINE_LIST_TYPE */].DEFAULT },
    magazineVideoParams: { page: 1, perPage: 5, type: application_magazine["a" /* MAGAZINE_LIST_TYPE */].VIDEO }
};
var index_initialize_INITIAL_STATE = {
    isPriorityBlock: true,
    isFetchMagazineVideo: false
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBR2hGLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixxQkFBcUIsRUFBRSxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsa0JBQWtCLENBQUMsT0FBTyxFQUFFO0lBQ2hGLG1CQUFtQixFQUFFLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxrQkFBa0IsQ0FBQyxLQUFLLEVBQUU7Q0FDbkUsQ0FBQztBQUVaLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixlQUFlLEVBQUUsSUFBSTtJQUNyQixvQkFBb0IsRUFBRSxLQUFLO0NBQ2xCLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/magazine/index/container.tsx
var container_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var container_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var container_MagazineIndexContainer = /** @class */ (function (_super) {
    container_extends(MagazineIndexContainer, _super);
    function MagazineIndexContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.state = index_initialize_INITIAL_STATE;
        return _this;
    }
    MagazineIndexContainer.prototype.handleFetchMagazineDashboard = function () {
        var _a = this.props, magazineDashboard = _a.magazineStore.magazineDashboard, fetchMagazineDashboard = _a.fetchMagazineDashboard;
        Object(validate["j" /* isEmptyObject */])(magazineDashboard)
            ? fetchMagazineDashboard()
            : this.state.isPriorityBlock && this.setState({ isPriorityBlock: false });
    };
    MagazineIndexContainer.prototype.handleFetchMagazineVideo = function () {
        var _a = this.state, isPriorityBlock = _a.isPriorityBlock, isFetchMagazineVideo = _a.isFetchMagazineVideo;
        if (!!isPriorityBlock) {
            return;
        }
        if (!isFetchMagazineVideo) {
            var _b = this.props, fetchMagazineList = _b.fetchMagazineList, magazineVideoParams = _b.magazineVideoParams, magazineList = _b.magazineStore.magazineList;
            var keyHash = Object(encode["h" /* objectToHash */])(magazineVideoParams);
            this.setState({ isFetchMagazineVideo: true });
            Object(validate["l" /* isUndefined */])(magazineList[keyHash]) && fetchMagazineList(magazineVideoParams);
        }
    };
    MagazineIndexContainer.prototype.componentWillMount = function () {
        var _a = this.props, fetchMagazineList = _a.fetchMagazineList, magazineDefaultParams = _a.magazineDefaultParams;
        fetchMagazineList(magazineDefaultParams);
    };
    MagazineIndexContainer.prototype.componentDidMount = function () {
        this.props.updateMetaInfoAction({
            info: {
                url: "http://lxbtest.tk/magazine",
                type: "article",
                title: 'Lixibox Magazine',
                description: 'LixiBox Magazine | Chia sẻ kiến thức mỹ phẩm, làm đẹp cùng LixiBox',
                keyword: 'mỹ phẩm, dưỡng da, trị mụn, skincare, makeup, halio, lustre',
                image: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/meta/cover.jpeg'
            },
            structuredData: {
                breadcrumbList: [
                    {
                        position: 2,
                        name: 'Lixibox Magazine',
                        item: "http://lxbtest.tk/magazine"
                    }
                ]
            }
        });
    };
    MagazineIndexContainer.prototype.componentWillReceiveProps = function (nextProps) {
        var _a = this.props.magazineStore, isFetchMagazineListSuccess = _a.isFetchMagazineListSuccess, isFetchMagazineDashboardSuccess = _a.isFetchMagazineDashboardSuccess;
        var magazineStore = nextProps.magazineStore;
        !isFetchMagazineListSuccess
            && magazineStore
            && magazineStore.isFetchMagazineListSuccess
            && this.state.isPriorityBlock
            && this.handleFetchMagazineDashboard();
        !isFetchMagazineDashboardSuccess
            && magazineStore
            && magazineStore.isFetchMagazineDashboardSuccess
            && this.state.isPriorityBlock
            && this.setState({ isPriorityBlock: false });
    };
    MagazineIndexContainer.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state,
            handleFetchMagazineVideo: this.handleFetchMagazineVideo.bind(this)
        };
        return index_view(args);
    };
    ;
    MagazineIndexContainer.defaultProps = index_initialize_DEFAULT_PROPS;
    MagazineIndexContainer = container_decorate([
        radium
    ], MagazineIndexContainer);
    return MagazineIndexContainer;
}(react["PureComponent"]));
/* harmony default export */ var container = (container_MagazineIndexContainer);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLEVBQUUsYUFBYSxFQUFFLFdBQVcsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3hFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUUxRCxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFFaEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFHNUQ7SUFBcUMsMENBQTZCO0lBRWhFLGdDQUFZLEtBQWE7UUFBekIsWUFDRSxrQkFBTSxLQUFLLENBQUMsU0FFYjtRQURDLEtBQUksQ0FBQyxLQUFLLEdBQUcsYUFBdUIsQ0FBQzs7SUFDdkMsQ0FBQztJQUVELDZEQUE0QixHQUE1QjtRQUNRLElBQUEsZUFBdUYsRUFBcEUsc0RBQWlCLEVBQUksa0RBQXNCLENBQTBCO1FBQzlGLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQztZQUM5QixDQUFDLENBQUMsc0JBQXNCLEVBQUU7WUFDMUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUM5RSxDQUFDO0lBRUQseURBQXdCLEdBQXhCO1FBQ1EsSUFBQSxlQUFnRSxFQUE5RCxvQ0FBZSxFQUFFLDhDQUFvQixDQUEwQjtRQUV2RSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQztRQUFDLENBQUM7UUFFbEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUM7WUFDcEIsSUFBQSxlQUlrQixFQUh0Qix3Q0FBaUIsRUFDakIsNENBQW1CLEVBQ0YsNENBQVksQ0FDTjtZQUV6QixJQUFNLE9BQU8sR0FBRyxZQUFZLENBQUMsbUJBQW1CLENBQUMsQ0FBQztZQUNsRCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsb0JBQW9CLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztZQUM5QyxXQUFXLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksaUJBQWlCLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUMvRSxDQUFDO0lBQ0gsQ0FBQztJQUVELG1EQUFrQixHQUFsQjtRQUNRLElBQUEsZUFBbUUsRUFBakUsd0NBQWlCLEVBQUUsZ0RBQXFCLENBQTBCO1FBRTFFLGlCQUFpQixDQUFDLHFCQUFxQixDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVELGtEQUFpQixHQUFqQjtRQUNFLElBQUksQ0FBQyxLQUFLLENBQUMsb0JBQW9CLENBQUM7WUFDOUIsSUFBSSxFQUFFO2dCQUNKLEdBQUcsRUFBRSxrQ0FBa0M7Z0JBQ3ZDLElBQUksRUFBRSxTQUFTO2dCQUNmLEtBQUssRUFBRSxrQkFBa0I7Z0JBQ3pCLFdBQVcsRUFBRSxvRUFBb0U7Z0JBQ2pGLE9BQU8sRUFBRSw2REFBNkQ7Z0JBQ3RFLEtBQUssRUFBRSxpQkFBaUIsR0FBRyxnQ0FBZ0M7YUFDNUQ7WUFDRCxjQUFjLEVBQUU7Z0JBQ2QsY0FBYyxFQUFFO29CQUNkO3dCQUNFLFFBQVEsRUFBRSxDQUFDO3dCQUNYLElBQUksRUFBRSxrQkFBa0I7d0JBQ3hCLElBQUksRUFBRSxrQ0FBa0M7cUJBQ3pDO2lCQUNGO2FBQ0Y7U0FDRixDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsMERBQXlCLEdBQXpCLFVBQTBCLFNBQVM7UUFDekIsSUFBQSw2QkFBOEUsRUFBN0QsMERBQTBCLEVBQUUsb0VBQStCLENBQTRCO1FBQ3hHLElBQUEsdUNBQWEsQ0FBeUI7UUFFOUMsQ0FBQywwQkFBMEI7ZUFDdEIsYUFBYTtlQUNiLGFBQWEsQ0FBQywwQkFBMEI7ZUFDeEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlO2VBQzFCLElBQUksQ0FBQyw0QkFBNEIsRUFBRSxDQUFDO1FBRXpDLENBQUMsK0JBQStCO2VBQzNCLGFBQWE7ZUFDYixhQUFhLENBQUMsK0JBQStCO2VBQzdDLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZTtlQUMxQixJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELHVDQUFNLEdBQU47UUFDRSxJQUFNLElBQUksR0FBRztZQUNYLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDbkUsQ0FBQTtRQUVELE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDMUIsQ0FBQztJQUFBLENBQUM7SUFwRkssbUNBQVksR0FBVyxhQUFhLENBQUM7SUFEeEMsc0JBQXNCO1FBRDNCLE1BQU07T0FDRCxzQkFBc0IsQ0FzRjNCO0lBQUQsNkJBQUM7Q0FBQSxBQXRGRCxDQUFxQyxhQUFhLEdBc0ZqRDtBQUVELGVBQWUsc0JBQXNCLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/magazine/index/store.tsx
var connect = __webpack_require__(129).connect;



var mapStateToProps = function (state) { return ({
    magazineStore: state.magazine
}); };
var mapDispatchToProps = function (dispatch) { return ({
    fetchMagazineList: function (data) { return dispatch(Object(magazine["e" /* fetchMagazineListAction */])(data)); },
    fetchMagazineDashboard: function () { return dispatch(Object(magazine["d" /* fetchMagazineDashboardAction */])()); },
    updateMetaInfoAction: function (data) { return dispatch(Object(meta["a" /* updateMetaInfoAction */])(data)); }
}); };
/* harmony default export */ var store = __webpack_exports__["default"] = (connect(mapStateToProps, mapDispatchToProps)(container));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUMvRCxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUVwRyxPQUFPLHNCQUFzQixNQUFNLGFBQWEsQ0FBQztBQUVqRCxJQUFNLGVBQWUsR0FBRyxVQUFBLEtBQUssSUFBSSxPQUFBLENBQUM7SUFDaEMsYUFBYSxFQUFFLEtBQUssQ0FBQyxRQUFRO0NBQzlCLENBQUMsRUFGK0IsQ0FFL0IsQ0FBQztBQUVILElBQU0sa0JBQWtCLEdBQUcsVUFBQSxRQUFRLElBQUksT0FBQSxDQUFDO0lBQ3RDLGlCQUFpQixFQUFFLFVBQUMsSUFBSSxJQUFLLE9BQUEsUUFBUSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxDQUFDLEVBQXZDLENBQXVDO0lBQ3BFLHNCQUFzQixFQUFFLGNBQU0sT0FBQSxRQUFRLENBQUMsNEJBQTRCLEVBQUUsQ0FBQyxFQUF4QyxDQUF3QztJQUN0RSxvQkFBb0IsRUFBRSxVQUFDLElBQUksSUFBSyxPQUFBLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFwQyxDQUFvQztDQUNyRSxDQUFDLEVBSnFDLENBSXJDLENBQUM7QUFFSCxlQUFlLE9BQU8sQ0FDcEIsZUFBZSxFQUNmLGtCQUFrQixDQUNuQixDQUFDLHNCQUFzQixDQUFDLENBQUMifQ==

/***/ })

}]);