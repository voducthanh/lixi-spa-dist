(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[94],{

/***/ 756:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 96).then(__webpack_require__.bind(null, 779)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 814:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// CONCATENATED MODULE: ./container/layout/auth-block/initialize.tsx
var DEFAULT_PROPS = {
    type: 'left',
    isInviteSignup: false
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsTUFBTTtJQUNaLGNBQWMsRUFBRSxLQUFLO0NBQ1osQ0FBQyJ9
// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// EXTERNAL MODULE: ./utils/uri.ts
var uri = __webpack_require__(347);

// CONCATENATED MODULE: ./container/layout/auth-block/style.tsx




var backgroundImage = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/login/bg.jpg';
var mobileImage = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/login/mobile.jpg';
/* harmony default export */ var auth_block_style = ({
    container: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{ flexDirection: 'column' }],
        DESKTOP: [{ flexDirection: 'row', }],
        GENERAL: [{
                display: 'flex',
                justifyContent: 'space-between',
                maxWidth: 650,
                borderRadius: 2,
                boxShadow: variable["shadowBlurSort"],
                background: variable["colorWhite"],
                transition: variable["transitionNormal"],
            }]
    }),
    leftContent: {
        flex: 5,
        position: 'relative',
        overflow: 'hidden',
        background: variable["colorWhite"],
        zIndex: variable["zIndex9"],
        borderRight: "1px solid " + variable["colorE5"],
        backdrop: {
            backgroundImage: "url(" + mobileImage + ")",
            backgroundSize: 'cover',
            backgroundPosition: 'left center',
            position: variable["position"].absolute,
            width: '100%',
            height: '100%',
            top: 0,
            left: 0,
            opacity: .1
        },
        topInfo: {
            marginBottom: 20,
            heading: {
                color: variable["colorBlack"],
                fontFamily: variable["fontAvenirDemiBold"],
                fontWeight: 600,
                fontSize: 26,
                lineHeight: '28px',
                marginBottom: 20,
                position: 'relative',
                border: {
                    position: variable["position"].absolute,
                    width: 5,
                    height: 30,
                    background: variable["colorBlack"],
                    top: -3,
                    left: -30,
                    display: 'block'
                }
            },
            textDescription: {
                color: variable["color2E"],
                fontSize: 14,
                lineHeight: '20px',
                fontFamily: variable["fontAvenirMedium"]
            },
        },
        bottomInfo: {},
        facebookLogin: {
            width: '100%',
            textAlign: 'center',
            fontSize: 14,
            marginBottom: 20,
            icon: {
                display: 'inline-block',
                width: 36,
                height: 36,
                verticalAlign: 'middle',
            }
        },
        spacer: {
            width: 150,
            height: 1,
            background: variable["colorE5"],
            marginTop: 30,
            marginRight: 'auto',
            marginBottom: 30,
            marginLeft: 'auto',
        },
        promotion: {
            padding: '30px 20px 0',
            avatarWrap: {
                display: variable["display"].flex,
                alignItems: 'center',
                justifyContent: 'center',
            },
            avatar: function (url) {
                if (url === void 0) { url = ''; }
                return {
                    backgroundImage: "url(" + url + ")",
                    backgroundSize: 'contain',
                    backgroundPosition: 'center center',
                    backgroundColor: variable["colorE5"],
                    width: 70,
                    height: 70,
                    borderRadius: '50%',
                    marginBottom: 10,
                    boxShadow: variable["shadowBlurSort"]
                };
            },
            title: {
                fontSize: 18,
                fontFamily: variable["fontAvenirDemiBold"],
                marginBottom: 10,
                textAlign: 'center'
            },
            note: {
                fontSize: 14,
                textAlign: 'justify'
            }
        }
    },
    relatedLink: {
        width: '100%',
        textAlign: 'center',
        text: {
            color: variable["color2E"],
            fontSize: 14,
            lineHeight: '20px',
            fontFamily: variable["fontAvenirMedium"]
        },
        link: {
            fontFamily: variable["fontAvenirDemiBold"],
            textDecoretion: 'underline',
            color: variable["color4D"],
            fontSize: 14,
            lineHeight: '20px',
            cursor: 'pointer',
            marginLeft: 5,
            marginRight: 5,
        }
    },
    rightContent: {
        flex: 6,
        position: 'relative',
        orSpacer: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{ display: variable["display"].none }],
            DESKTOP: [{ display: variable["display"].block }],
            GENERAL: [{
                    position: variable["position"].absolute,
                    width: 34,
                    height: 34,
                    lineHeight: '34px',
                    textAlign: 'center',
                    borderRadius: '50%',
                    boxShadow: variable["shadowBlurSort"],
                    zIndex: variable["zIndex9"],
                    top: '50%',
                    left: 0,
                    transform: 'translate(-50%, -50%)',
                    background: variable["colorWhite"],
                    fontSize: 12,
                    color: variable["colorBlack"],
                }]
        }),
    },
    innerContent: [
        layout["a" /* flexContainer */].justify,
        layout["a" /* flexContainer */].verticalFlex,
        {
            position: 'relative',
            zIndex: variable["zIndex5"],
            paddingTop: 30,
            paddingRight: 30,
            paddingBottom: 30,
            paddingLeft: 30,
            height: '100%',
        }
    ],
    innerContentRight: [
        layout["a" /* flexContainer */].justify,
        layout["a" /* flexContainer */].verticalFlex,
        {
            position: 'relative',
            zIndex: variable["zIndex5"],
            paddingRight: 30,
            paddingBottom: 30,
            paddingLeft: 30,
            height: '100%',
            paddingTop: 20,
        }
    ],
    errorMessage: {
        color: variable["colorRed"],
        fontSize: 13,
        lineHeight: '18px',
        paddingTop: 5,
        paddingBottom: 5,
        textAlign: 'center',
        overflow: variable["visible"].hidden,
        transition: variable["transitionNormal"]
    },
    mobile: {
        container: {
            background: variable["colorWhite"],
            minWidth: '100%',
            display: variable["display"].flex,
            flexDirection: 'column',
            justifyContent: 'space-between',
        },
        top: {
            backgroundImage: "url(" + mobileImage + ")",
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            width: '100%',
            position: variable["position"].relative,
            zIndex: variable["zIndex5"],
            marginBottom: 30,
            overlay: {
                position: variable["position"].absolute,
                zIndex: variable["zIndex1"],
                background: variable["colorBlack"],
                opacity: 0.6,
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
            },
            iconList: {
                width: '100%',
                paddingTop: 5,
                paddingRight: 5,
                paddingBottom: 5,
                paddingLeft: 5,
                display: 'flex',
                justifyContent: 'space-between',
                marginBottom: 35
            },
            link: {
                height: 44,
                lineHeight: '44px',
                zIndex: variable["zIndex5"],
                display: 'flex',
                color: variable["colorWhite"],
                fontSize: 15,
                fontFamily: variable["fontAvenirMedium"],
            },
            icon: {
                width: 44,
                height: 44,
                color: variable["colorWhite"],
                position: variable["position"].relative,
                zIndex: variable["zIndex5"],
            },
            innerIcon: {
                height: 20
            },
            logo: {
                width: 80,
                height: 80,
                color: variable["colorBlack"],
                background: variable["colorWhite"],
                borderRadius: '50%',
                position: variable["position"].absolute,
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)'
            },
            innerLogo: {
                width: 70
            },
            gap: {
                position: variable["position"].relative,
                top: 20,
                zIndex: variable["zIndex5"],
                backgroundSize: '100% 100%',
                width: '100%',
                height: 40,
                borderTop: "50% solid " + variable["colorWhite"],
                borderLeft: "50% solid " + variable["colorWhite"],
                borderRight: "50% solid " + variable["colorTransparent"],
                borderBottom: "50% solid " + variable["colorTransparent"],
                filter: "drop-shadow(-4px -4px 2px rgba(0,0,0,.1))"
            },
            title: {
                fontSize: 16
            }
        },
        main: {
            padding: 20,
            background: variable["colorWhite"],
            maxWidth: 400,
            width: '100%',
            margin: '0 auto'
        },
        fbButton: {
            paddingLeft: 20,
            paddingRight: 20,
            maxWidth: 400,
            width: '100%',
            margin: '0 auto'
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLE1BQU0sTUFBTSx1QkFBdUIsQ0FBQztBQUNoRCxPQUFPLEtBQUssUUFBUSxNQUFNLHlCQUF5QixDQUFDO0FBQ3BELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUN2RCxJQUFNLGVBQWUsR0FBRyxpQkFBaUIsR0FBRyw2QkFBNkIsQ0FBQztBQUMxRSxJQUFNLFdBQVcsR0FBRyxpQkFBaUIsR0FBRyxpQ0FBaUMsQ0FBQztBQUUxRSxlQUFlO0lBQ2IsU0FBUyxFQUFFLFlBQVksQ0FBQztRQUN0QixNQUFNLEVBQUUsQ0FBQyxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsQ0FBQztRQUNyQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEdBQUcsQ0FBQztRQUVwQyxPQUFPLEVBQUUsQ0FBQztnQkFDUixPQUFPLEVBQUUsTUFBTTtnQkFDZixjQUFjLEVBQUUsZUFBZTtnQkFDL0IsUUFBUSxFQUFFLEdBQUc7Z0JBQ2IsWUFBWSxFQUFFLENBQUM7Z0JBQ2YsU0FBUyxFQUFFLFFBQVEsQ0FBQyxjQUFjO2dCQUNsQyxVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVU7Z0JBQy9CLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO2FBQ3RDLENBQUM7S0FDSCxDQUFDO0lBRUYsV0FBVyxFQUFFO1FBQ1gsSUFBSSxFQUFFLENBQUM7UUFDUCxRQUFRLEVBQUUsVUFBVTtRQUNwQixRQUFRLEVBQUUsUUFBUTtRQUNsQixVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVU7UUFDL0IsTUFBTSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1FBQ3hCLFdBQVcsRUFBRSxlQUFhLFFBQVEsQ0FBQyxPQUFTO1FBRTVDLFFBQVEsRUFBRTtZQUNSLGVBQWUsRUFBRSxTQUFPLFdBQVcsTUFBRztZQUN0QyxjQUFjLEVBQUUsT0FBTztZQUN2QixrQkFBa0IsRUFBRSxhQUFhO1lBQ2pDLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDcEMsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsTUFBTTtZQUNkLEdBQUcsRUFBRSxDQUFDO1lBQ04sSUFBSSxFQUFFLENBQUM7WUFDUCxPQUFPLEVBQUUsRUFBRTtTQUNaO1FBRUQsT0FBTyxFQUFFO1lBQ1AsWUFBWSxFQUFFLEVBQUU7WUFFaEIsT0FBTyxFQUFFO2dCQUNQLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxrQkFBa0I7Z0JBQ3ZDLFVBQVUsRUFBRSxHQUFHO2dCQUNmLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxNQUFNO2dCQUNsQixZQUFZLEVBQUUsRUFBRTtnQkFDaEIsUUFBUSxFQUFFLFVBQVU7Z0JBRXBCLE1BQU0sRUFBRTtvQkFDTixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUNwQyxLQUFLLEVBQUUsQ0FBQztvQkFDUixNQUFNLEVBQUUsRUFBRTtvQkFDVixVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVU7b0JBQy9CLEdBQUcsRUFBRSxDQUFDLENBQUM7b0JBQ1AsSUFBSSxFQUFFLENBQUMsRUFBRTtvQkFDVCxPQUFPLEVBQUUsT0FBTztpQkFDakI7YUFDRjtZQUVELGVBQWUsRUFBRTtnQkFDZixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87Z0JBQ3ZCLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxNQUFNO2dCQUNsQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjthQUN0QztTQUNGO1FBRUQsVUFBVSxFQUFFLEVBQUU7UUFFZCxhQUFhLEVBQUU7WUFDYixLQUFLLEVBQUUsTUFBTTtZQUNiLFNBQVMsRUFBRSxRQUFRO1lBQ25CLFFBQVEsRUFBRSxFQUFFO1lBQ1osWUFBWSxFQUFFLEVBQUU7WUFFaEIsSUFBSSxFQUFFO2dCQUNKLE9BQU8sRUFBRSxjQUFjO2dCQUN2QixLQUFLLEVBQUUsRUFBRTtnQkFDVCxNQUFNLEVBQUUsRUFBRTtnQkFDVixhQUFhLEVBQUUsUUFBUTthQUN4QjtTQUNGO1FBRUQsTUFBTSxFQUFFO1lBQ04sS0FBSyxFQUFFLEdBQUc7WUFDVixNQUFNLEVBQUUsQ0FBQztZQUNULFVBQVUsRUFBRSxRQUFRLENBQUMsT0FBTztZQUM1QixTQUFTLEVBQUUsRUFBRTtZQUNiLFdBQVcsRUFBRSxNQUFNO1lBQ25CLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFVBQVUsRUFBRSxNQUFNO1NBQ25CO1FBRUQsU0FBUyxFQUFFO1lBQ1QsT0FBTyxFQUFFLGFBQWE7WUFFdEIsVUFBVSxFQUFFO2dCQUNWLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLFVBQVUsRUFBRSxRQUFRO2dCQUNwQixjQUFjLEVBQUUsUUFBUTthQUN6QjtZQUVELE1BQU0sRUFBRSxVQUFDLEdBQVE7Z0JBQVIsb0JBQUEsRUFBQSxRQUFRO2dCQUNmLE1BQU0sQ0FBQztvQkFDTCxlQUFlLEVBQUUsU0FBTyxHQUFHLE1BQUc7b0JBQzlCLGNBQWMsRUFBRSxTQUFTO29CQUN6QixrQkFBa0IsRUFBRSxlQUFlO29CQUNuQyxlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87b0JBQ2pDLEtBQUssRUFBRSxFQUFFO29CQUNULE1BQU0sRUFBRSxFQUFFO29CQUNWLFlBQVksRUFBRSxLQUFLO29CQUNuQixZQUFZLEVBQUUsRUFBRTtvQkFDaEIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxjQUFjO2lCQUNuQyxDQUFBO1lBQ0gsQ0FBQztZQUVELEtBQUssRUFBRTtnQkFDTCxRQUFRLEVBQUUsRUFBRTtnQkFDWixVQUFVLEVBQUUsUUFBUSxDQUFDLGtCQUFrQjtnQkFDdkMsWUFBWSxFQUFFLEVBQUU7Z0JBQ2hCLFNBQVMsRUFBRSxRQUFRO2FBQ3BCO1lBRUQsSUFBSSxFQUFFO2dCQUNKLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFNBQVMsRUFBRSxTQUFTO2FBQ3JCO1NBQ0Y7S0FDRjtJQUVELFdBQVcsRUFBRTtRQUNYLEtBQUssRUFBRSxNQUFNO1FBQ2IsU0FBUyxFQUFFLFFBQVE7UUFFbkIsSUFBSSxFQUFFO1lBQ0osS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO1lBQ3ZCLFFBQVEsRUFBRSxFQUFFO1lBQ1osVUFBVSxFQUFFLE1BQU07WUFDbEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7U0FDdEM7UUFFRCxJQUFJLEVBQUU7WUFDSixVQUFVLEVBQUUsUUFBUSxDQUFDLGtCQUFrQjtZQUN2QyxjQUFjLEVBQUUsV0FBVztZQUMzQixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87WUFDdkIsUUFBUSxFQUFFLEVBQUU7WUFDWixVQUFVLEVBQUUsTUFBTTtZQUNsQixNQUFNLEVBQUUsU0FBUztZQUNqQixVQUFVLEVBQUUsQ0FBQztZQUNiLFdBQVcsRUFBRSxDQUFDO1NBQ2Y7S0FDRjtJQUVELFlBQVksRUFBRTtRQUNaLElBQUksRUFBRSxDQUFDO1FBQ1AsUUFBUSxFQUFFLFVBQVU7UUFFcEIsUUFBUSxFQUFFLFlBQVksQ0FBQztZQUNyQixNQUFNLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzVDLE9BQU8sRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7WUFFOUMsT0FBTyxFQUFFLENBQUM7b0JBQ1IsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtvQkFDcEMsS0FBSyxFQUFFLEVBQUU7b0JBQ1QsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsVUFBVSxFQUFFLE1BQU07b0JBQ2xCLFNBQVMsRUFBRSxRQUFRO29CQUNuQixZQUFZLEVBQUUsS0FBSztvQkFDbkIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxjQUFjO29CQUNsQyxNQUFNLEVBQUUsUUFBUSxDQUFDLE9BQU87b0JBQ3hCLEdBQUcsRUFBRSxLQUFLO29CQUNWLElBQUksRUFBRSxDQUFDO29CQUNQLFNBQVMsRUFBRSx1QkFBdUI7b0JBQ2xDLFVBQVUsRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDL0IsUUFBUSxFQUFFLEVBQUU7b0JBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2lCQUMzQixDQUFDO1NBQ0gsQ0FBQztLQUNIO0lBRUQsWUFBWSxFQUFFO1FBQ1osTUFBTSxDQUFDLGFBQWEsQ0FBQyxPQUFPO1FBQzVCLE1BQU0sQ0FBQyxhQUFhLENBQUMsWUFBWTtRQUNqQztZQUNFLFFBQVEsRUFBRSxVQUFVO1lBQ3BCLE1BQU0sRUFBRSxRQUFRLENBQUMsT0FBTztZQUN4QixVQUFVLEVBQUUsRUFBRTtZQUNkLFlBQVksRUFBRSxFQUFFO1lBQ2hCLGFBQWEsRUFBRSxFQUFFO1lBQ2pCLFdBQVcsRUFBRSxFQUFFO1lBQ2YsTUFBTSxFQUFFLE1BQU07U0FDZjtLQUNGO0lBRUQsaUJBQWlCLEVBQUU7UUFDakIsTUFBTSxDQUFDLGFBQWEsQ0FBQyxPQUFPO1FBQzVCLE1BQU0sQ0FBQyxhQUFhLENBQUMsWUFBWTtRQUNqQztZQUNFLFFBQVEsRUFBRSxVQUFVO1lBQ3BCLE1BQU0sRUFBRSxRQUFRLENBQUMsT0FBTztZQUN4QixZQUFZLEVBQUUsRUFBRTtZQUNoQixhQUFhLEVBQUUsRUFBRTtZQUNqQixXQUFXLEVBQUUsRUFBRTtZQUNmLE1BQU0sRUFBRSxNQUFNO1lBQ2QsVUFBVSxFQUFFLEVBQUU7U0FDZjtLQUNGO0lBRUQsWUFBWSxFQUFFO1FBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxRQUFRO1FBQ3hCLFFBQVEsRUFBRSxFQUFFO1FBQ1osVUFBVSxFQUFFLE1BQU07UUFDbEIsVUFBVSxFQUFFLENBQUM7UUFDYixhQUFhLEVBQUUsQ0FBQztRQUNoQixTQUFTLEVBQUUsUUFBUTtRQUNuQixRQUFRLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNO1FBQ2pDLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO0tBQ3RDO0lBRUQsTUFBTSxFQUFFO1FBQ04sU0FBUyxFQUFFO1lBQ1QsVUFBVSxFQUFFLFFBQVEsQ0FBQyxVQUFVO1lBQy9CLFFBQVEsRUFBRSxNQUFNO1lBQ2hCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsYUFBYSxFQUFFLFFBQVE7WUFDdkIsY0FBYyxFQUFFLGVBQWU7U0FDaEM7UUFFRCxHQUFHLEVBQUU7WUFDSCxlQUFlLEVBQUUsU0FBTyxXQUFXLE1BQUc7WUFDdEMsY0FBYyxFQUFFLE9BQU87WUFDdkIsa0JBQWtCLEVBQUUsUUFBUTtZQUM1QixLQUFLLEVBQUUsTUFBTTtZQUNiLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDcEMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1lBQ3hCLFlBQVksRUFBRSxFQUFFO1lBRWhCLE9BQU8sRUFBRTtnQkFDUCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxNQUFNLEVBQUUsUUFBUSxDQUFDLE9BQU87Z0JBQ3hCLFVBQVUsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDL0IsT0FBTyxFQUFFLEdBQUc7Z0JBQ1osR0FBRyxFQUFFLENBQUM7Z0JBQ04sSUFBSSxFQUFFLENBQUM7Z0JBQ1AsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsTUFBTSxFQUFFLE1BQU07YUFDZjtZQUVELFFBQVEsRUFBRTtnQkFDUixLQUFLLEVBQUUsTUFBTTtnQkFDYixVQUFVLEVBQUUsQ0FBQztnQkFDYixZQUFZLEVBQUUsQ0FBQztnQkFDZixhQUFhLEVBQUUsQ0FBQztnQkFDaEIsV0FBVyxFQUFFLENBQUM7Z0JBQ2QsT0FBTyxFQUFFLE1BQU07Z0JBQ2YsY0FBYyxFQUFFLGVBQWU7Z0JBQy9CLFlBQVksRUFBRSxFQUFFO2FBQ2pCO1lBRUQsSUFBSSxFQUFFO2dCQUNKLE1BQU0sRUFBRSxFQUFFO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2dCQUNsQixNQUFNLEVBQUUsUUFBUSxDQUFDLE9BQU87Z0JBQ3hCLE9BQU8sRUFBRSxNQUFNO2dCQUNmLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDMUIsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7YUFDdEM7WUFFRCxJQUFJLEVBQUU7Z0JBQ0osS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMxQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxNQUFNLEVBQUUsUUFBUSxDQUFDLE9BQU87YUFDekI7WUFFRCxTQUFTLEVBQUU7Z0JBQ1QsTUFBTSxFQUFFLEVBQUU7YUFDWDtZQUVELElBQUksRUFBRTtnQkFDSixLQUFLLEVBQUUsRUFBRTtnQkFDVCxNQUFNLEVBQUUsRUFBRTtnQkFDVixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7Z0JBQzFCLFVBQVUsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDL0IsWUFBWSxFQUFFLEtBQUs7Z0JBQ25CLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLEdBQUcsRUFBRSxLQUFLO2dCQUNWLElBQUksRUFBRSxLQUFLO2dCQUNYLFNBQVMsRUFBRSx1QkFBdUI7YUFDbkM7WUFFRCxTQUFTLEVBQUU7Z0JBQ1QsS0FBSyxFQUFFLEVBQUU7YUFDVjtZQUVELEdBQUcsRUFBRTtnQkFDSCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxHQUFHLEVBQUUsRUFBRTtnQkFDUCxNQUFNLEVBQUUsUUFBUSxDQUFDLE9BQU87Z0JBQ3hCLGNBQWMsRUFBRSxXQUFXO2dCQUMzQixLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsRUFBRTtnQkFDVixTQUFTLEVBQUUsZUFBYSxRQUFRLENBQUMsVUFBWTtnQkFDN0MsVUFBVSxFQUFFLGVBQWEsUUFBUSxDQUFDLFVBQVk7Z0JBQzlDLFdBQVcsRUFBRSxlQUFhLFFBQVEsQ0FBQyxnQkFBa0I7Z0JBQ3JELFlBQVksRUFBRSxlQUFhLFFBQVEsQ0FBQyxnQkFBa0I7Z0JBQ3RELE1BQU0sRUFBRSwyQ0FBMkM7YUFDcEQ7WUFFRCxLQUFLLEVBQUU7Z0JBQ0wsUUFBUSxFQUFFLEVBQUU7YUFDYjtTQUNGO1FBRUQsSUFBSSxFQUFFO1lBQ0osT0FBTyxFQUFFLEVBQUU7WUFDWCxVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDL0IsUUFBUSxFQUFFLEdBQUc7WUFDYixLQUFLLEVBQUUsTUFBTTtZQUNiLE1BQU0sRUFBRSxRQUFRO1NBQ2pCO1FBRUQsUUFBUSxFQUFFO1lBQ1IsV0FBVyxFQUFFLEVBQUU7WUFDZixZQUFZLEVBQUUsRUFBRTtZQUNoQixRQUFRLEVBQUUsR0FBRztZQUNiLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLFFBQVE7U0FDakI7S0FDRjtDQUNNLENBQUMifQ==
// CONCATENATED MODULE: ./container/layout/auth-block/view-desktop.tsx



var renderHeader = function (_a) {
    var title = _a.title, description = _a.description, _b = _a.userReferrerProfile, userReferrerProfile = _b === void 0 ? {} : _b;
    return (react["createElement"]("div", { style: auth_block_style.leftContent.topInfo },
        react["createElement"]("div", { style: auth_block_style.leftContent.topInfo.heading },
            title,
            react["createElement"]("span", { style: auth_block_style.leftContent.topInfo.heading.border })),
        !Object(validate["j" /* isEmptyObject */])(userReferrerProfile) ? renderPromotion(userReferrerProfile) : react["createElement"]("div", { style: auth_block_style.leftContent.topInfo.textDescription }, description)));
};
var renderPromotion = function (userReferrerProfile) {
    var avatarUrl = userReferrerProfile.avatar && userReferrerProfile.avatar.original_url;
    var userName = userReferrerProfile.first_name + " " + userReferrerProfile.last_name;
    return (react["createElement"]("div", null,
        react["createElement"]("div", { style: auth_block_style.leftContent.promotion.avatarWrap },
            react["createElement"]("div", { style: auth_block_style.leftContent.promotion.avatar(avatarUrl) })),
        react["createElement"]("div", { style: auth_block_style.leftContent.promotion.title },
            userName,
            " t\u1EB7ng b\u1EA1n m\u1ED9t v\u00E0i m\u00F3n qu\u00E0 khi mua s\u1EAFm t\u1EA1i Lixibox"),
        react["createElement"]("div", { style: auth_block_style.leftContent.promotion.note }, "\u0110\u0103ng k\u00FD ngay h\u00F4m nay \u0111\u1EC3 \u0111\u01B0\u1EE3c nh\u1EADn qu\u00E0 t\u1EB7ng cho \u0111\u01A1n h\u00E0ng \u0111\u1EA7u ti\u00EAn.")));
};
var renderSub = function (_a) {
    var subContainer = _a.subContainer;
    return (react["createElement"]("div", { style: auth_block_style.leftContent.bottomInfo }, subContainer));
};
var renderMain = function (_a) {
    var mainContainer = _a.mainContainer;
    return (react["createElement"]("div", { style: auth_block_style.rightContent },
        react["createElement"]("div", { style: auth_block_style.rightContent.orSpacer }, "OR"),
        react["createElement"]("div", { style: auth_block_style.innerContentRight }, mainContainer)));
};
var renderDesktop = function (_a) {
    var props = _a.props;
    var title = props.title, description = props.description, subContainer = props.subContainer, mainContainer = props.mainContainer, style = props.style, userReferrerProfile = props.userReferrerProfile;
    return (react["createElement"]("auth-block", { style: [auth_block_style.container, style] },
        react["createElement"]("div", { style: auth_block_style.leftContent },
            react["createElement"]("div", { style: auth_block_style.leftContent.backdrop }),
            react["createElement"]("div", { style: auth_block_style.innerContent },
                renderHeader({ title: title, description: description, userReferrerProfile: userReferrerProfile }),
                renderSub({ subContainer: subContainer }))),
        renderMain({ mainContainer: mainContainer })));
};
/* harmony default export */ var view_desktop = (renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1kZXNrdG9wLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1kZXNrdG9wLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUUvQixPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFFeEQsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRTVCLElBQU0sWUFBWSxHQUFHLFVBQUMsRUFBZ0Q7UUFBOUMsZ0JBQUssRUFBRSw0QkFBVyxFQUFFLDJCQUF3QixFQUF4Qiw2Q0FBd0I7SUFBTyxPQUFBLENBQ3pFLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU87UUFDbkMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLE9BQU87WUFDMUMsS0FBSztZQUNOLDhCQUFNLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFTLENBQzFEO1FBRUwsQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxlQUFlLElBQUcsV0FBVyxDQUFPLENBQ3BKLENBQ1A7QUFUMEUsQ0FTMUUsQ0FBQztBQUVGLElBQU0sZUFBZSxHQUFHLFVBQUMsbUJBQW1CO0lBQzFDLElBQU0sU0FBUyxHQUFHLG1CQUFtQixDQUFDLE1BQU0sSUFBSSxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDO0lBQ3hGLElBQU0sUUFBUSxHQUFNLG1CQUFtQixDQUFDLFVBQVUsU0FBSSxtQkFBbUIsQ0FBQyxTQUFXLENBQUM7SUFFdEYsTUFBTSxDQUFDLENBQ0w7UUFDRSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsVUFBVTtZQUFFLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQVEsQ0FBTTtRQUMzSCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsS0FBSztZQUFHLFFBQVE7d0dBQXdEO1FBQ2hILDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxJQUFJLGtLQUF5RSxDQUVqSCxDQUNQLENBQUE7QUFDSCxDQUFDLENBQUM7QUFFRixJQUFNLFNBQVMsR0FBRyxVQUFDLEVBQWdCO1FBQWQsOEJBQVk7SUFBTyxPQUFBLENBQ3RDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFVBQVUsSUFDckMsWUFBWSxDQUNULENBQ1A7QUFKdUMsQ0FJdkMsQ0FBQztBQUVGLElBQU0sVUFBVSxHQUFHLFVBQUMsRUFBaUI7UUFBZixnQ0FBYTtJQUFPLE9BQUEsQ0FDeEMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxZQUFZO1FBQzVCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWSxDQUFDLFFBQVEsU0FBVTtRQUVqRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGlCQUFpQixJQUNoQyxhQUFhLENBQ1YsQ0FDRixDQUNQO0FBUnlDLENBUXpDLENBQUM7QUFFRixJQUFNLGFBQWEsR0FBRyxVQUFDLEVBQVM7UUFBUCxnQkFBSztJQUNwQixJQUFBLG1CQUFLLEVBQUUsK0JBQVcsRUFBRSxpQ0FBWSxFQUFFLG1DQUFhLEVBQUUsbUJBQUssRUFBRSwrQ0FBbUIsQ0FBVztJQUU5RixNQUFNLENBQUMsQ0FDTCxvQ0FBWSxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQztRQUN6Qyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVc7WUFDM0IsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsUUFBUSxHQUFRO1lBRTlDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWTtnQkFDM0IsWUFBWSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsV0FBVyxhQUFBLEVBQUUsbUJBQW1CLHFCQUFBLEVBQUUsQ0FBQztnQkFDekQsU0FBUyxDQUFDLEVBQUUsWUFBWSxjQUFBLEVBQUUsQ0FBQyxDQUN4QixDQUNGO1FBRUwsVUFBVSxDQUFDLEVBQUUsYUFBYSxlQUFBLEVBQUUsQ0FBQyxDQUNuQixDQUNkLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLGFBQWEsQ0FBQyJ9
// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// EXTERNAL MODULE: ./container/layout/fade-in/index.tsx
var fade_in = __webpack_require__(756);

// CONCATENATED MODULE: ./container/layout/auth-block/view-mobile.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};







// const renderHeader = ({ title, description }) => (
//   <div style={STYLE.leftContent.topInfo}>
//     <div style={STYLE.leftContent.topInfo.heading}>
//       {title}
//       <span style={STYLE.leftContent.topInfo.heading.border}></span>
//     </div>
//     <div style={STYLE.leftContent.topInfo.textDescription}>{description}</div>
//   </div>
// );
// const renderSub = ({ subContainer }) => (
//   <div style={STYLE.leftContent.bottomInfo}>
//     {subContainer}
//   </div>
// );
var view_mobile_renderMain = function (_a) {
    var mainContainer = _a.mainContainer;
    return (react["createElement"]("div", { style: auth_block_style.mobile.main }, mainContainer));
};
var renderTop = function (_a) {
    var rightIcon = _a.rightIcon, history = _a.history;
    var iconBackProps = {
        name: 'angle-left',
        style: auth_block_style.mobile.top.icon,
        innerStyle: auth_block_style.mobile.top.innerIcon,
    };
    var iconLogoProps = {
        name: 'logo-line',
        style: auth_block_style.mobile.top.logo,
        innerStyle: auth_block_style.mobile.top.innerLogo
    };
    var rightIconProps = rightIcon && {
        name: rightIcon.name,
        style: auth_block_style.mobile.top.icon,
        innerStyle: auth_block_style.mobile.top.innerIcon,
    };
    return (react["createElement"]("div", { style: auth_block_style.mobile.top },
        react["createElement"]("div", { style: auth_block_style.mobile.top.overlay }),
        react["createElement"]("div", { style: auth_block_style.mobile.top.iconList },
            react["createElement"](react_router_dom["NavLink"], { to: routing["kb" /* ROUTING_SHOP_INDEX */], style: auth_block_style.mobile.top.link },
                react["createElement"](icon["a" /* default */], __assign({}, iconBackProps)),
                "V\u1EC1 Trang Ch\u1EE7"),
            rightIcon
                && (react["createElement"](react_router_dom["NavLink"], { to: rightIcon.link, style: auth_block_style.mobile.top.link },
                    rightIcon.title,
                    react["createElement"](icon["a" /* default */], __assign({}, rightIconProps))))),
        react["createElement"]("div", { style: auth_block_style.mobile.top.gap },
            react["createElement"](icon["a" /* default */], __assign({}, iconLogoProps)))));
};
var renderFbButton = function (_a) {
    var loginFbContent = _a.loginFbContent;
    return (react["createElement"]("div", { style: auth_block_style.mobile.fbButton }, loginFbContent));
};
var view_mobile_renderPromotion = function (userReferrerProfile) {
    var avatarUrl = userReferrerProfile.avatar && userReferrerProfile.avatar.original_url;
    var userName = userReferrerProfile.first_name + " " + userReferrerProfile.last_name;
    return (react["createElement"]("div", { style: auth_block_style.leftContent.promotion },
        react["createElement"]("div", { style: auth_block_style.leftContent.promotion.avatarWrap },
            react["createElement"]("div", { style: auth_block_style.leftContent.promotion.avatar(avatarUrl) })),
        react["createElement"]("div", { style: auth_block_style.leftContent.promotion.title },
            userName,
            " t\u1EB7ng b\u1EA1n m\u1ED9t v\u00E0i m\u00F3n qu\u00E0 khi mua s\u1EAFm t\u1EA1i Lixibox"),
        react["createElement"]("div", { style: auth_block_style.leftContent.promotion.note }, "\u0110\u0103ng k\u00FD ngay h\u00F4m nay \u0111\u1EC3 \u0111\u01B0\u1EE3c nh\u1EADn qu\u00E0 t\u1EB7ng cho \u0111\u01A1n h\u00E0ng \u0111\u1EA7u ti\u00EAn.")));
};
var renderMobile = function (_a) {
    var props = _a.props;
    var mainContainer = props.mainContainer, rightIcon = props.rightIcon, loginFbContent = props.loginFbContent, history = props.history, userReferrerProfile = props.userReferrerProfile;
    return (react["createElement"]("auth-block", { style: auth_block_style.mobile.container },
        renderTop({ rightIcon: rightIcon, history: history }),
        !Object(validate["j" /* isEmptyObject */])(userReferrerProfile) && view_mobile_renderPromotion(userReferrerProfile),
        react["createElement"](fade_in["a" /* default */], null, view_mobile_renderMain({ mainContainer: mainContainer })),
        react["createElement"](fade_in["a" /* default */], null, renderFbButton({ loginFbContent: loginFbContent }))));
};
/* harmony default export */ var view_mobile = (renderMobile);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1tb2JpbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LW1vYmlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUMvQixPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFFM0MsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDNUUsT0FBTyxJQUFJLE1BQU0sNkJBQTZCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBRXhELE9BQU8sTUFBTSxNQUFNLFlBQVksQ0FBQztBQUVoQyxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIscURBQXFEO0FBQ3JELDRDQUE0QztBQUM1QyxzREFBc0Q7QUFDdEQsZ0JBQWdCO0FBQ2hCLHVFQUF1RTtBQUN2RSxhQUFhO0FBRWIsaUZBQWlGO0FBQ2pGLFdBQVc7QUFDWCxLQUFLO0FBRUwsNENBQTRDO0FBQzVDLCtDQUErQztBQUMvQyxxQkFBcUI7QUFDckIsV0FBVztBQUNYLEtBQUs7QUFFTCxJQUFNLFVBQVUsR0FBRyxVQUFDLEVBQWlCO1FBQWYsZ0NBQWE7SUFBTyxPQUFBLENBQ3hDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksSUFDMUIsYUFBYSxDQUNWLENBQ1A7QUFKeUMsQ0FJekMsQ0FBQztBQUVGLElBQU0sU0FBUyxHQUFHLFVBQUMsRUFBc0I7UUFBcEIsd0JBQVMsRUFBRSxvQkFBTztJQUNyQyxJQUFNLGFBQWEsR0FBRztRQUNwQixJQUFJLEVBQUUsWUFBWTtRQUNsQixLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSTtRQUM1QixVQUFVLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsU0FBUztLQUN2QyxDQUFDO0lBRUYsSUFBTSxhQUFhLEdBQUc7UUFDcEIsSUFBSSxFQUFFLFdBQVc7UUFDakIsS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUk7UUFDNUIsVUFBVSxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFNBQVM7S0FDdkMsQ0FBQztJQUVGLElBQU0sY0FBYyxHQUFHLFNBQVMsSUFBSTtRQUNsQyxJQUFJLEVBQUUsU0FBUyxDQUFDLElBQUk7UUFDcEIsS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUk7UUFDNUIsVUFBVSxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFNBQVM7S0FDdkMsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLEdBQUc7UUFDMUIsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sR0FBUTtRQUM1Qyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsUUFBUTtZQUNuQyxvQkFBQyxPQUFPLElBQUMsRUFBRSxFQUFFLGtCQUFrQixFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJO2dCQUMzRCxvQkFBQyxJQUFJLGVBQUssYUFBYSxFQUFJO3lDQUVuQjtZQUVSLFNBQVM7bUJBQ04sQ0FDRCxvQkFBQyxPQUFPLElBQUMsRUFBRSxFQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUk7b0JBQ3RELFNBQVMsQ0FBQyxLQUFLO29CQUNoQixvQkFBQyxJQUFJLGVBQUssY0FBYyxFQUFJLENBQ3BCLENBQ1gsQ0FFQztRQUNOLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHO1lBQzlCLG9CQUFDLElBQUksZUFBSyxhQUFhLEVBQUksQ0FDdkIsQ0FDRixDQUNQLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixJQUFNLGNBQWMsR0FBRyxVQUFDLEVBQWtCO1FBQWhCLGtDQUFjO0lBQU8sT0FBQSxDQUM3Qyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLElBQzlCLGNBQWMsQ0FDWCxDQUNQO0FBSjhDLENBSTlDLENBQUM7QUFFRixJQUFNLGVBQWUsR0FBRyxVQUFDLG1CQUFtQjtJQUMxQyxJQUFNLFNBQVMsR0FBRyxtQkFBbUIsQ0FBQyxNQUFNLElBQUksbUJBQW1CLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQztJQUN4RixJQUFNLFFBQVEsR0FBTSxtQkFBbUIsQ0FBQyxVQUFVLFNBQUksbUJBQW1CLENBQUMsU0FBVyxDQUFDO0lBRXRGLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFNBQVM7UUFDckMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFVBQVU7WUFBRSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFRLENBQU07UUFDM0gsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLEtBQUs7WUFBRyxRQUFRO3dHQUF3RDtRQUNoSCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsSUFBSSxrS0FBeUUsQ0FFakgsQ0FDUCxDQUFBO0FBQ0gsQ0FBQyxDQUFDO0FBRUYsSUFBTSxZQUFZLEdBQUcsVUFBQyxFQUFTO1FBQVAsZ0JBQUs7SUFDbkIsSUFBQSxtQ0FBYSxFQUFFLDJCQUFTLEVBQUUscUNBQWMsRUFBRSx1QkFBTyxFQUFFLCtDQUFtQixDQUFXO0lBRXpGLE1BQU0sQ0FBQyxDQUNMLG9DQUFZLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVM7UUFDdEMsU0FBUyxDQUFDLEVBQUUsU0FBUyxXQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQztRQUNqQyxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQztRQUM1RSxvQkFBQyxNQUFNLFFBQUUsVUFBVSxDQUFDLEVBQUUsYUFBYSxlQUFBLEVBQUUsQ0FBQyxDQUFVO1FBQ2hELG9CQUFDLE1BQU0sUUFBRSxjQUFjLENBQUMsRUFBRSxjQUFjLGdCQUFBLEVBQUUsQ0FBQyxDQUFVLENBQzFDLENBQ2QsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsWUFBWSxDQUFDIn0=
// CONCATENATED MODULE: ./container/layout/auth-block/view.tsx


var renderView = function (_a) {
    var props = _a.props;
    var switchView = {
        MOBILE: function () { return view_mobile({ props: props }); },
        DESKTOP: function () { return view_desktop({ props: props }); }
    };
    return switchView[window.DEVICE_VERSION]();
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sYUFBYSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sWUFBWSxNQUFNLGVBQWUsQ0FBQztBQUV6QyxJQUFNLFVBQVUsR0FBRyxVQUFDLEVBQVM7UUFBUCxnQkFBSztJQUN6QixJQUFNLFVBQVUsR0FBRztRQUNqQixNQUFNLEVBQUUsY0FBTSxPQUFBLFlBQVksQ0FBQyxFQUFFLEtBQUssT0FBQSxFQUFFLENBQUMsRUFBdkIsQ0FBdUI7UUFDckMsT0FBTyxFQUFFLGNBQU0sT0FBQSxhQUFhLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxDQUFDLEVBQXhCLENBQXdCO0tBQ3hDLENBQUM7SUFFRixNQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDO0FBQzdDLENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./container/layout/auth-block/container.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var container_AuthBlock = /** @class */ (function (_super) {
    __extends(AuthBlock, _super);
    function AuthBlock(props) {
        return _super.call(this, props) || this;
    }
    AuthBlock.prototype.render = function () {
        return view({ props: this.props });
    };
    ;
    AuthBlock.defaultProps = DEFAULT_PROPS;
    AuthBlock = __decorate([
        radium
    ], AuthBlock);
    return AuthBlock;
}(react["PureComponent"]));
/* harmony default export */ var container = __webpack_exports__["default"] = (container_AuthBlock);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQzdDLE9BQU8sVUFBVSxNQUFNLFFBQVEsQ0FBQztBQUloQztJQUF3Qiw2QkFBNkI7SUFHbkQsbUJBQVksS0FBYTtlQUN2QixrQkFBTSxLQUFLLENBQUM7SUFDZCxDQUFDO0lBRUQsMEJBQU0sR0FBTjtRQUNFLE1BQU0sQ0FBQyxVQUFVLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUFBLENBQUM7SUFSSyxzQkFBWSxHQUFXLGFBQWEsQ0FBQztJQUR4QyxTQUFTO1FBRGQsTUFBTTtPQUNELFNBQVMsQ0FVZDtJQUFELGdCQUFDO0NBQUEsQUFWRCxDQUF3QixhQUFhLEdBVXBDO0FBRUQsZUFBZSxTQUFTLENBQUMifQ==

/***/ })

}]);