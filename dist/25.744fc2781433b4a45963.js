(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[25],{

/***/ 746:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 99).then(__webpack_require__.bind(null, 759)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 751:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/general/pagination/style.tsx

/* harmony default export */ var style = ({
    width: '100%',
    container: {
        paddingTop: 10,
        paddingRight: 20,
        paddingBottom: 10,
        paddingLeft: 20,
    },
    icon: {
        cursor: 'pointer',
        height: 36,
        width: 36,
        minWidth: 36,
        color: variable["colorBlack"],
        inner: {
            width: 14
        }
    },
    item: {
        height: 36,
        minWidth: 36,
        paddingTop: 0,
        paddingRight: 10,
        paddingBottom: 0,
        paddingLeft: 10,
        lineHeight: '36px',
        textAlign: 'center',
        backgroundColor: variable["colorWhite"],
        fontFamily: variable["fontAvenirMedium"],
        fontSize: 13,
        color: variable["color4D"],
        cursor: 'pointer',
        marginBottom: 10,
        verticalAlign: 'top',
        icon: {
            paddingRight: 0,
            paddingLeft: 0,
        },
        active: {
            backgroundColor: variable["colorWhite"],
            color: variable["colorBlack"],
            borderRadius: 3,
            pointerEvents: 'none',
            border: "1px solid " + variable["color75"],
            zIndex: variable["zIndex5"],
            fontFamily: variable["fontAvenirDemiBold"],
        },
        disabled: {
            pointerEvents: 'none',
            backgroundColor: variable["colorWhite"],
        },
        ':hover': {
            border: "1px solid " + variable["colorA2"],
        }
    },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxlQUFlO0lBQ2IsS0FBSyxFQUFFLE1BQU07SUFFYixTQUFTLEVBQUU7UUFDVCxVQUFVLEVBQUUsRUFBRTtRQUNkLFlBQVksRUFBRSxFQUFFO1FBQ2hCLGFBQWEsRUFBRSxFQUFFO1FBQ2pCLFdBQVcsRUFBRSxFQUFFO0tBQ2hCO0lBRUQsSUFBSSxFQUFFO1FBQ0osTUFBTSxFQUFFLFNBQVM7UUFDakIsTUFBTSxFQUFFLEVBQUU7UUFDVixLQUFLLEVBQUUsRUFBRTtRQUNULFFBQVEsRUFBRSxFQUFFO1FBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1FBRTFCLEtBQUssRUFBRTtZQUNMLEtBQUssRUFBRSxFQUFFO1NBQ1Y7S0FDRjtJQUVELElBQUksRUFBRTtRQUNKLE1BQU0sRUFBRSxFQUFFO1FBQ1YsUUFBUSxFQUFFLEVBQUU7UUFDWixVQUFVLEVBQUUsQ0FBQztRQUNiLFlBQVksRUFBRSxFQUFFO1FBQ2hCLGFBQWEsRUFBRSxDQUFDO1FBQ2hCLFdBQVcsRUFBRSxFQUFFO1FBQ2YsVUFBVSxFQUFFLE1BQU07UUFDbEIsU0FBUyxFQUFFLFFBQVE7UUFDbkIsZUFBZSxFQUFFLFFBQVEsQ0FBQyxVQUFVO1FBQ3BDLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO1FBQ3JDLFFBQVEsRUFBRSxFQUFFO1FBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO1FBQ3ZCLE1BQU0sRUFBRSxTQUFTO1FBQ2pCLFlBQVksRUFBRSxFQUFFO1FBQ2hCLGFBQWEsRUFBRSxLQUFLO1FBRXBCLElBQUksRUFBRTtZQUNKLFlBQVksRUFBRSxDQUFDO1lBQ2YsV0FBVyxFQUFFLENBQUM7U0FDZjtRQUdELE1BQU0sRUFBRTtZQUNOLGVBQWUsRUFBRSxRQUFRLENBQUMsVUFBVTtZQUNwQyxLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDMUIsWUFBWSxFQUFFLENBQUM7WUFDZixhQUFhLEVBQUUsTUFBTTtZQUNyQixNQUFNLEVBQUUsZUFBYSxRQUFRLENBQUMsT0FBUztZQUN2QyxNQUFNLEVBQUUsUUFBUSxDQUFDLE9BQU87WUFDeEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxrQkFBa0I7U0FDeEM7UUFFRCxRQUFRLEVBQUU7WUFDUixhQUFhLEVBQUUsTUFBTTtZQUNyQixlQUFlLEVBQUUsUUFBUSxDQUFDLFVBQVU7U0FDckM7UUFFRCxRQUFRLEVBQUU7WUFDUixNQUFNLEVBQUUsZUFBYSxRQUFRLENBQUMsT0FBUztTQUN4QztLQUNGO0NBQ0YsQ0FBQyJ9
// CONCATENATED MODULE: ./components/general/pagination/view.tsx






function renderComponent() {
    var _this = this;
    var _a = this.props, current = _a.current, per = _a.per, total = _a.total, urlList = _a.urlList, handleClick = _a.handleClick;
    var list = this.state.list;
    var numberList = [];
    var handleOnClick = function (val) { return 'function' === typeof handleClick && handleClick(val); };
    var isUrlListEmpty = null === urlList
        || Object(validate["l" /* isUndefined */])(urlList)
        || urlList.length === 0
        || total !== urlList.length;
    return isUrlListEmpty ? null : (react["createElement"]("div", { style: style },
        react["createElement"]("div", { style: [layout["a" /* flexContainer */].center, layout["a" /* flexContainer */].wrap, style.container] },
            current !== 1 &&
                react["createElement"](react_router_dom["NavLink"], { title: 'Trang ' + (current - 1), to: urlList[current - 2].link, onClick: function () { handleOnClick(current - 1), _this.handleScrollTop(); }, style: Object.assign({}, style.item, style.item.icon) },
                    react["createElement"](icon["a" /* default */], { name: 'arrow-left', style: style.icon, innerStyle: style.icon.inner })),
            total > 1
                && Array.isArray(list)
                && list.map(function (item) {
                    return react["createElement"](react_router_dom["NavLink"], { title: 'Trang ' + item.title, to: item.link, key: "pagi-item-" + item.number, onClick: function () { handleOnClick(item.title), _this.handleScrollTop(); }, style: Object.assign({}, style.item, (item.number === current) && style.item.active, item.disabled && style.item.disabled) }, item.title);
                }),
            current !== total &&
                react["createElement"](react_router_dom["NavLink"], { title: 'Trang ' + (current + 1), to: urlList[current].link, style: Object.assign({}, style.item, style.item.icon), onClick: function () { handleOnClick(current + 1), _this.handleScrollTop(); } },
                    react["createElement"](icon["a" /* default */], { name: 'arrow-right', style: style.icon, innerStyle: style.icon.inner })))));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUUzQyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDdEQsT0FBTyxLQUFLLE1BQU0sTUFBTSx1QkFBdUIsQ0FBQztBQUNoRCxPQUFPLElBQUksTUFBTSxlQUFlLENBQUM7QUFDakMsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRzVCLE1BQU07SUFBTixpQkErREM7SUE5RE8sSUFBQSxlQUEwRCxFQUF4RCxvQkFBTyxFQUFFLFlBQUcsRUFBRSxnQkFBSyxFQUFFLG9CQUFPLEVBQUUsNEJBQVcsQ0FBZ0I7SUFDekQsSUFBQSxzQkFBSSxDQUFnQjtJQUM1QixJQUFNLFVBQVUsR0FBa0IsRUFBRSxDQUFDO0lBRXJDLElBQU0sYUFBYSxHQUFHLFVBQUMsR0FBRyxJQUFLLE9BQUEsVUFBVSxLQUFLLE9BQU8sV0FBVyxJQUFJLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckQsQ0FBcUQsQ0FBQztJQUNyRixJQUFNLGNBQWMsR0FBRyxJQUFJLEtBQUssT0FBTztXQUNsQyxXQUFXLENBQUMsT0FBTyxDQUFDO1dBQ3BCLE9BQU8sQ0FBQyxNQUFNLEtBQUssQ0FBQztXQUNwQixLQUFLLEtBQUssT0FBTyxDQUFDLE1BQU0sQ0FBQztJQUU5QixNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQzdCLDZCQUFLLEtBQUssRUFBRSxLQUFLO1FBQ2YsNkJBQUssS0FBSyxFQUFFLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQztZQUVqRixPQUFPLEtBQUssQ0FBQztnQkFDYixvQkFBQyxPQUFPLElBQ04sS0FBSyxFQUFFLFFBQVEsR0FBRyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsRUFDL0IsRUFBRSxFQUFFLE9BQU8sQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUM3QixPQUFPLEVBQUUsY0FBUSxhQUFhLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxFQUFFLEtBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQSxDQUFDLENBQUMsRUFDckUsS0FBSyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7b0JBQ3JELG9CQUFDLElBQUksSUFDSCxJQUFJLEVBQUUsWUFBWSxFQUNsQixLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksRUFDakIsVUFBVSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFJLENBQzFCO1lBSVYsS0FBSyxHQUFHLENBQUM7bUJBQ04sS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7bUJBQ25CLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJO29CQUNkLE9BQUEsb0JBQUMsT0FBTyxJQUNOLEtBQUssRUFBRSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssRUFDNUIsRUFBRSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQ2IsR0FBRyxFQUFFLGVBQWEsSUFBSSxDQUFDLE1BQVEsRUFDL0IsT0FBTyxFQUFFLGNBQVEsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxLQUFJLENBQUMsZUFBZSxFQUFFLENBQUEsQ0FBQyxDQUFDLEVBQ3BFLEtBQUssRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFDckIsS0FBSyxDQUFDLElBQUksRUFDVixDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssT0FBTyxDQUFDLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQzlDLElBQUksQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQ3JDLElBQ0EsSUFBSSxDQUFDLEtBQUssQ0FDSDtnQkFYVixDQVdVLENBQ1g7WUFJRCxPQUFPLEtBQUssS0FBSztnQkFDakIsb0JBQUMsT0FBTyxJQUNOLEtBQUssRUFBRSxRQUFRLEdBQUcsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLEVBQy9CLEVBQUUsRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxFQUN6QixLQUFLLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsS0FBSyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUNyRCxPQUFPLEVBQUUsY0FBUSxhQUFhLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxFQUFFLEtBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQSxDQUFDLENBQUM7b0JBQ3JFLG9CQUFDLElBQUksSUFDSCxJQUFJLEVBQUUsYUFBYSxFQUNuQixLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksRUFDakIsVUFBVSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFJLENBQzFCLENBRVIsQ0FDRCxDQUNSLENBQUE7QUFDSCxDQUFDO0FBQUEsQ0FBQyJ9
// CONCATENATED MODULE: ./components/general/pagination/initialize.tsx
var DEFAULT_PROPS = {
    current: 0,
    per: 0,
    total: 0,
    urlList: [],
    canScrollToTop: true,
    elementId: '',
    scrollToElementNum: 0
};
var INITIAL_STATE = { list: [] };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixPQUFPLEVBQUUsQ0FBQztJQUNWLEdBQUcsRUFBRSxDQUFDO0lBQ04sS0FBSyxFQUFFLENBQUM7SUFDUixPQUFPLEVBQUUsRUFBRTtJQUNYLGNBQWMsRUFBRSxJQUFJO0lBQ3BCLFNBQVMsRUFBRSxFQUFFO0lBQ2Isa0JBQWtCLEVBQUUsQ0FBQztDQUNGLENBQUM7QUFFdEIsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBc0IsQ0FBQyJ9
// CONCATENATED MODULE: ./components/general/pagination/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var component_Pagination = /** @class */ (function (_super) {
    __extends(Pagination, _super);
    function Pagination(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    Pagination.prototype.componentDidMount = function () {
        this.initData();
    };
    Pagination.prototype.componentWillReceiveProps = function (nextProps) {
        this.initData(nextProps);
    };
    Pagination.prototype.handleScrollTop = function () {
        var _a = this.props, canScrollToTop = _a.canScrollToTop, elementId = _a.elementId, scrollToElementNum = _a.scrollToElementNum;
        canScrollToTop && window.scrollTo(0, 0);
        if (elementId && elementId.length > 0) {
            var element = document.getElementById(elementId);
            element && element.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
        }
        scrollToElementNum > 0 && window.scrollTo(0, scrollToElementNum);
    };
    Pagination.prototype.initData = function (props) {
        if (props === void 0) { props = this.props; }
        var urlList = props.urlList, current = props.current, per = props.per, total = props.total;
        if (null === urlList
            || true === Object(validate["l" /* isUndefined */])(urlList)
            || 0 === urlList.length
            || urlList.length !== total) {
            return;
        }
        var list = [], startInnerList, endInnerList;
        /** Generate head-list */
        if (current >= 5) {
            list.push({
                number: urlList[0].number,
                title: urlList[0].title,
                link: urlList[0].link,
                active: false,
                disabled: false
            });
            list.push({
                value: -1,
                number: -1,
                title: '...',
                link: '#',
                active: false,
                disabled: true
            });
        }
        /**
         * Generate inner list
         *
         * startInnerList : min 0
         * endInnerList : max pagingInfo.total
         *
         */
        startInnerList = current - 2;
        startInnerList = startInnerList <= 0
            /** Min value : 1 */
            ? 1
            : (startInnerList === 2
                /** If start value === 2 -> force to 1 */
                ? 1
                : startInnerList);
        endInnerList = startInnerList + 4;
        endInnerList = endInnerList > total
            /** Max value total */
            ? total
            : (
            /** If end value === total - 1 -> force to total */
            endInnerList === total - 1
                ? total
                : endInnerList);
        for (var i = startInnerList; i <= endInnerList; i++) {
            list.push({
                number: urlList[i - 1].number,
                title: urlList[i - 1].title,
                link: urlList[i - 1].link,
                active: i === current,
                disabled: false,
                endList: i === total,
            });
        }
        /** Generate tail-list */
        if (total > 5 && current <= total - 4) {
            list.push({
                value: -2,
                number: -2,
                title: '...',
                link: '#',
                active: false,
                disabled: true
            });
            list.push({
                number: urlList[total - 1].number,
                title: urlList[total - 1].title,
                link: urlList[total - 1].link,
                active: false,
                disabled: false,
                endList: true
            });
        }
        this.setState({ list: list });
    };
    /**
     * Go To Page
     *
     * @param newPage object : page clicked
     */
    Pagination.prototype.goToPage = function (newPage) {
        // const { onSelectPage } = this.props as IPaginationProps;
        // onSelectPage(newPage);
    };
    /**
     * Navigate page:
     *
     * @param direction string next/prev
     *
     * @return call to goToPage()
     */
    Pagination.prototype.navigatePage = function (direction) {
        // const { productByCategory } = this.props;
        // const pagingInfo = productByCategory.paging;
        if (direction === void 0) { direction = 'next'; }
        // /** Generate new page value */
        // const newPage = pagingInfo.current + ('next' === direction ? 1 : -1);
        // /** Check range value [1, total] */
        // if (newPage < 0 || newPage > pagingInfo.total) {
        //   return;
        // }
        // this.goToPage(newPage);
    };
    Pagination.prototype.render = function () { return renderComponent.bind(this)(); };
    Pagination.defaultProps = DEFAULT_PROPS;
    Pagination = __decorate([
        radium
    ], Pagination);
    return Pagination;
}(react["Component"]));
;
/* harmony default export */ var component = (component_Pagination);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFHakMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUN6QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDdEQsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFJNUQ7SUFBeUIsOEJBQW1EO0lBRTFFLG9CQUFZLEtBQUs7UUFBakIsWUFDRSxrQkFBTSxLQUFLLENBQUMsU0FFYjtRQURDLEtBQUksQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDOztJQUM3QixDQUFDO0lBRUQsc0NBQWlCLEdBQWpCO1FBQ0UsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xCLENBQUM7SUFFRCw4Q0FBeUIsR0FBekIsVUFBMEIsU0FBUztRQUNqQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFRCxvQ0FBZSxHQUFmO1FBQ1EsSUFBQSxlQUFrRixFQUFoRixrQ0FBYyxFQUFFLHdCQUFTLEVBQUUsMENBQWtCLENBQW9DO1FBQ3pGLGNBQWMsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUV4QyxFQUFFLENBQUMsQ0FBQyxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3RDLElBQU0sT0FBTyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDbkQsT0FBTyxJQUFJLE9BQU8sQ0FBQyxjQUFjLENBQUMsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUM7UUFDL0YsQ0FBQztRQUVELGtCQUFrQixHQUFHLENBQUMsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO0lBQ25FLENBQUM7SUFFRCw2QkFBUSxHQUFSLFVBQVMsS0FBa0I7UUFBbEIsc0JBQUEsRUFBQSxRQUFRLElBQUksQ0FBQyxLQUFLO1FBQ2pCLElBQUEsdUJBQU8sRUFBRSx1QkFBTyxFQUFFLGVBQUcsRUFBRSxtQkFBSyxDQUFXO1FBRS9DLEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxPQUFPO2VBQ2YsSUFBSSxLQUFLLFdBQVcsQ0FBQyxPQUFPLENBQUM7ZUFDN0IsQ0FBQyxLQUFLLE9BQU8sQ0FBQyxNQUFNO2VBQ3BCLE9BQU8sQ0FBQyxNQUFNLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQztZQUM5QixNQUFNLENBQUM7UUFDVCxDQUFDO1FBRUQsSUFBSSxJQUFJLEdBQWUsRUFBRSxFQUFFLGNBQWMsRUFBRSxZQUFZLENBQUM7UUFFeEQseUJBQXlCO1FBQ3pCLEVBQUUsQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2pCLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ1IsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNO2dCQUN6QixLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUs7Z0JBQ3ZCLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtnQkFDckIsTUFBTSxFQUFFLEtBQUs7Z0JBQ2IsUUFBUSxFQUFFLEtBQUs7YUFDaEIsQ0FBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDUixLQUFLLEVBQUUsQ0FBQyxDQUFDO2dCQUNULE1BQU0sRUFBRSxDQUFDLENBQUM7Z0JBQ1YsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osSUFBSSxFQUFFLEdBQUc7Z0JBQ1QsTUFBTSxFQUFFLEtBQUs7Z0JBQ2IsUUFBUSxFQUFFLElBQUk7YUFDZixDQUFDLENBQUM7UUFDTCxDQUFDO1FBRUQ7Ozs7OztXQU1HO1FBQ0gsY0FBYyxHQUFHLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFDN0IsY0FBYyxHQUFHLGNBQWMsSUFBSSxDQUFDO1lBQ2xDLG9CQUFvQjtZQUNwQixDQUFDLENBQUMsQ0FBQztZQUNILENBQUMsQ0FBQyxDQUNBLGNBQWMsS0FBSyxDQUFDO2dCQUNsQix5Q0FBeUM7Z0JBQ3pDLENBQUMsQ0FBQyxDQUFDO2dCQUNILENBQUMsQ0FBQyxjQUFjLENBQ25CLENBQUM7UUFFSixZQUFZLEdBQUcsY0FBYyxHQUFHLENBQUMsQ0FBQztRQUNsQyxZQUFZLEdBQUcsWUFBWSxHQUFHLEtBQUs7WUFDakMsc0JBQXNCO1lBQ3RCLENBQUMsQ0FBQyxLQUFLO1lBQ1AsQ0FBQyxDQUFDO1lBQ0EsbURBQW1EO1lBQ25ELFlBQVksS0FBSyxLQUFLLEdBQUcsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLEtBQUs7Z0JBQ1AsQ0FBQyxDQUFDLFlBQVksQ0FDakIsQ0FBQztRQUVKLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLGNBQWMsRUFBRSxDQUFDLElBQUksWUFBWSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDcEQsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDUixNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNO2dCQUM3QixLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLO2dCQUMzQixJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJO2dCQUN6QixNQUFNLEVBQUUsQ0FBQyxLQUFLLE9BQU87Z0JBQ3JCLFFBQVEsRUFBRSxLQUFLO2dCQUNmLE9BQU8sRUFBRSxDQUFDLEtBQUssS0FBSzthQUNyQixDQUFDLENBQUM7UUFDTCxDQUFDO1FBRUQseUJBQXlCO1FBQ3pCLEVBQUUsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLElBQUksT0FBTyxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ1IsS0FBSyxFQUFFLENBQUMsQ0FBQztnQkFDVCxNQUFNLEVBQUUsQ0FBQyxDQUFDO2dCQUNWLEtBQUssRUFBRSxLQUFLO2dCQUNaLElBQUksRUFBRSxHQUFHO2dCQUNULE1BQU0sRUFBRSxLQUFLO2dCQUNiLFFBQVEsRUFBRSxJQUFJO2FBQ2YsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDUixNQUFNLEVBQUUsT0FBTyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNO2dCQUNqQyxLQUFLLEVBQUUsT0FBTyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLO2dCQUMvQixJQUFJLEVBQUUsT0FBTyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJO2dCQUM3QixNQUFNLEVBQUUsS0FBSztnQkFDYixRQUFRLEVBQUUsS0FBSztnQkFDZixPQUFPLEVBQUUsSUFBSTthQUNkLENBQUMsQ0FBQztRQUNMLENBQUM7UUFFRCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCw2QkFBUSxHQUFSLFVBQVMsT0FBTztRQUNkLDJEQUEyRDtRQUMzRCx5QkFBeUI7SUFDM0IsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNILGlDQUFZLEdBQVosVUFBYSxTQUFrQjtRQUM3Qiw0Q0FBNEM7UUFDNUMsK0NBQStDO1FBRnBDLDBCQUFBLEVBQUEsa0JBQWtCO1FBSTdCLGlDQUFpQztRQUNqQyx3RUFBd0U7UUFFeEUsc0NBQXNDO1FBQ3RDLG1EQUFtRDtRQUNuRCxZQUFZO1FBQ1osSUFBSTtRQUVKLDBCQUEwQjtJQUM1QixDQUFDO0lBRUQsMkJBQU0sR0FBTixjQUFXLE1BQU0sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBekoxQyx1QkFBWSxHQUFxQixhQUFhLENBQUM7SUFEbEQsVUFBVTtRQURmLE1BQU07T0FDRCxVQUFVLENBMkpmO0lBQUQsaUJBQUM7Q0FBQSxBQTNKRCxDQUF5QixLQUFLLENBQUMsU0FBUyxHQTJKdkM7QUFBQSxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// CONCATENATED MODULE: ./components/general/pagination/index.tsx

/* harmony default export */ var pagination = __webpack_exports__["a"] = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxVQUFVLE1BQU0sYUFBYSxDQUFDO0FBQ3JDLGVBQWUsVUFBVSxDQUFDIn0=

/***/ }),

/***/ 911:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./action/meta.ts
var meta = __webpack_require__(754);

// EXTERNAL MODULE: ./action/magazine.ts + 1 modules
var magazine = __webpack_require__(761);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// EXTERNAL MODULE: ./utils/format.ts
var format = __webpack_require__(116);

// EXTERNAL MODULE: ./utils/encode.ts
var encode = __webpack_require__(7);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ../node_modules/react-on-screen/lib/index.js
var lib = __webpack_require__(758);
var lib_default = /*#__PURE__*/__webpack_require__.n(lib);

// EXTERNAL MODULE: ./components/ui/loading/index.tsx + 4 modules
var loading = __webpack_require__(747);

// EXTERNAL MODULE: ./constants/application/magazine-widget.ts
var magazine_widget = __webpack_require__(842);

// EXTERNAL MODULE: ./constants/application/magazine-category.ts
var magazine_category = __webpack_require__(790);

// EXTERNAL MODULE: ./components/general/pagination/index.tsx + 4 modules
var pagination = __webpack_require__(751);

// EXTERNAL MODULE: ./container/layout/wrap/index.tsx
var wrap = __webpack_require__(745);

// EXTERNAL MODULE: ./container/layout/split/index.tsx
var split = __webpack_require__(753);

// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./container/layout/main-block/index.tsx
var main_block = __webpack_require__(746);

// EXTERNAL MODULE: ./components/magazine/item-list/index.tsx + 9 modules
var item_list = __webpack_require__(835);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./components/magazine/category-detail/style.tsx


/* harmony default export */ var style = ({
    contentWrap: {
        paddingLeft: 10,
        paddingRight: 10,
        title: {
            fontSize: 16,
            lineHeight: '20px',
            color: variable["colorBlack09"],
            fontFamily: variable["fontAvenirMedium"],
            width: '100%',
            fontWeight: 900,
            display: variable["display"].block,
            marginBottom: 10,
            textTransform: 'capitalize',
            textAlign: 'justify',
        },
        description: {
            fontSize: 14,
            lineHeight: '20px',
            maxHeight: 60,
            overflow: 'hidden',
            color: variable["colorBlack06"],
            textAlign: 'justify',
            width: '100%',
            marginBottom: 10,
        }
    },
    imgText: {
        position: variable["position"].absolute,
        fontSize: 24,
        lineHeight: '32px',
        width: '70%',
        padding: '10px 20px',
        textAlign: 'left',
        left: 0,
        bottom: 20,
        color: variable["colorBlack07"],
        fontFamily: variable["fontAvenirDemiBold"]
    },
    headerMenuContainer: {
        display: variable["display"].block,
        position: variable["position"].relative,
        height: 50,
        maxHeight: 50,
        headerMenuWrap: {
            width: '100%',
            height: '100%',
            display: variable["display"].flex,
            position: variable["position"].relative,
            backgroundBlur: {
                backgroundColor: variable["colorWhite08"],
                position: variable["position"].absolute,
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
                zIndex: variable["zIndexNegative"]
            },
        }
    },
    headerMenu: {
        display: variable["display"].flex,
        alignItems: 'center',
        justifyContent: 'flex-start',
        fontFamily: variable["fontAvenirMedium"],
        borderBottom: "1px solid " + variable["colorE5"],
        height: '100%',
        width: '100%',
        position: variable["position"].absolute,
        top: 0,
        left: 0,
        zIndex: variable["zIndex9"],
        isTop: {
            position: variable["position"].fixed,
            top: 0,
            left: 0,
            width: '100%',
            height: 50,
        },
        textBreadCrumb: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    fontSize: 15,
                    lineHeight: '50px',
                    height: '100%',
                    width: '100%',
                    paddingLeft: 10,
                    fontFamily: variable["fontTrirong"],
                    color: variable["colorBlack"],
                    display: variable["display"].inlineBlock,
                    textTransform: 'capitalize',
                }],
            DESKTOP: [{
                    fontSize: 18,
                    height: 60,
                    lineHeight: '60px',
                    textTransform: 'capitalize',
                    fontFamily: variable["fontAvenirMedium"],
                }],
            GENERAL: [{
                    color: variable["colorBlack"],
                    cursor: 'pointer',
                }]
        }),
        icon: function (isOpening) { return ({
            width: 50,
            height: 50,
            color: variable["colorBlack08"],
            transition: variable["transitionNormal"],
            transform: isOpening ? 'rotate(-180deg)' : 'rotate(0deg)'
        }); },
        inner: {
            width: 10,
        }
    },
    categoryList: {
        position: variable["position"].absolute,
        paddingTop: 15,
        paddingBottom: 15,
        top: 50,
        left: 0,
        width: '100%',
        zIndex: variable["zIndex9"],
        backgroundColor: variable["colorWhite08"],
        paddingLeft: 10,
        display: variable["display"].block,
        borderBottom: "1px solid " + variable["colorE5"],
        visibility: variable["visible"].hidden,
        opacity: 0,
    },
    isShowing: {
        transition: variable["transitionNormal"],
        visibility: variable["visible"].visible,
        opacity: 1
    },
    category: {
        display: variable["display"].block,
        fontSize: 14,
        lineHeight: '44px',
        height: 44,
        maxHeight: 44,
        fontFamily: variable["fontAvenirMedium"],
        color: variable["colorBlack"],
    },
    categoryDetail: {
        marginBottom: 20,
        largeItem: {
            display: variable["display"].block,
            width: '100%',
            cursor: 'pointer',
            marginBottom: 20,
            itemImage: function (image) { return ({
                width: '100%',
                paddingTop: '65%',
                backgroundImage: "url(" + image + ")",
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                position: variable["position"].relative,
                transition: variable["transitionNormal"],
                marginBottom: 10,
            }); }
        },
        listSubItem: {
            width: '100%',
            display: variable["display"].flex,
            justifyContent: 'space-between',
            flexWrap: 'wrap',
            marginBottom: 20,
            subItem: {
                width: 'calc(50% - 10px)',
                display: variable["display"].block,
                marginBottom: 20,
                itemImage: function (image) { return ({
                    width: '100%',
                    paddingTop: '65%',
                    backgroundImage: "url(" + image + ")",
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    marginBottom: 10,
                    posicontion: variable["position"].relative,
                    transition: variable["transitionNormal"],
                }); }
            },
        },
    },
    customStyleLoading: {
        height: 80
    },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFekQsZUFBZTtJQUNiLFdBQVcsRUFBRTtRQUNYLFdBQVcsRUFBRSxFQUFFO1FBQ2YsWUFBWSxFQUFFLEVBQUU7UUFFaEIsS0FBSyxFQUFFO1lBQ0wsUUFBUSxFQUFFLEVBQUU7WUFDWixVQUFVLEVBQUUsTUFBTTtZQUNsQixLQUFLLEVBQUUsUUFBUSxDQUFDLFlBQVk7WUFDNUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7WUFDckMsS0FBSyxFQUFFLE1BQU07WUFDYixVQUFVLEVBQUUsR0FBRztZQUNmLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDL0IsWUFBWSxFQUFFLEVBQUU7WUFDaEIsYUFBYSxFQUFFLFlBQVk7WUFDM0IsU0FBUyxFQUFFLFNBQVM7U0FDckI7UUFFRCxXQUFXLEVBQUU7WUFDWCxRQUFRLEVBQUUsRUFBRTtZQUNaLFVBQVUsRUFBRSxNQUFNO1lBQ2xCLFNBQVMsRUFBRSxFQUFFO1lBQ2IsUUFBUSxFQUFFLFFBQVE7WUFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxZQUFZO1lBQzVCLFNBQVMsRUFBRSxTQUFTO1lBQ3BCLEtBQUssRUFBRSxNQUFNO1lBQ2IsWUFBWSxFQUFFLEVBQUU7U0FDakI7S0FDRjtJQUVELE9BQU8sRUFBRTtRQUNQLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7UUFDcEMsUUFBUSxFQUFFLEVBQUU7UUFDWixVQUFVLEVBQUUsTUFBTTtRQUNsQixLQUFLLEVBQUUsS0FBSztRQUNaLE9BQU8sRUFBRSxXQUFXO1FBQ3BCLFNBQVMsRUFBRSxNQUFNO1FBQ2pCLElBQUksRUFBRSxDQUFDO1FBQ1AsTUFBTSxFQUFFLEVBQUU7UUFDVixLQUFLLEVBQUUsUUFBUSxDQUFDLFlBQVk7UUFDNUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxrQkFBa0I7S0FDeEM7SUFFRCxtQkFBbUIsRUFBRTtRQUNuQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1FBQy9CLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7UUFDcEMsTUFBTSxFQUFFLEVBQUU7UUFDVixTQUFTLEVBQUUsRUFBRTtRQUViLGNBQWMsRUFBRTtZQUNkLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLE1BQU07WUFDZCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFFcEMsY0FBYyxFQUFFO2dCQUNkLGVBQWUsRUFBRSxRQUFRLENBQUMsWUFBWTtnQkFDdEMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtnQkFDcEMsR0FBRyxFQUFFLENBQUM7Z0JBQ04sSUFBSSxFQUFFLENBQUM7Z0JBQ1AsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsTUFBTSxFQUFFLE1BQU07Z0JBQ2QsTUFBTSxFQUFFLFFBQVEsQ0FBQyxjQUFjO2FBQ2hDO1NBQ0Y7S0FDRjtJQUVELFVBQVUsRUFBRTtRQUNWLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7UUFDOUIsVUFBVSxFQUFFLFFBQVE7UUFDcEIsY0FBYyxFQUFFLFlBQVk7UUFDNUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7UUFDckMsWUFBWSxFQUFFLGVBQWEsUUFBUSxDQUFDLE9BQVM7UUFDN0MsTUFBTSxFQUFFLE1BQU07UUFDZCxLQUFLLEVBQUUsTUFBTTtRQUNiLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7UUFDcEMsR0FBRyxFQUFFLENBQUM7UUFDTixJQUFJLEVBQUUsQ0FBQztRQUNQLE1BQU0sRUFBRSxRQUFRLENBQUMsT0FBTztRQUV4QixLQUFLLEVBQUU7WUFDTCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLO1lBQ2pDLEdBQUcsRUFBRSxDQUFDO1lBQ04sSUFBSSxFQUFFLENBQUM7WUFDUCxLQUFLLEVBQUUsTUFBTTtZQUNiLE1BQU0sRUFBRSxFQUFFO1NBQ1g7UUFFRCxjQUFjLEVBQUUsWUFBWSxDQUFDO1lBQzNCLE1BQU0sRUFBRSxDQUFDO29CQUNQLFFBQVEsRUFBRSxFQUFFO29CQUNaLFVBQVUsRUFBRSxNQUFNO29CQUNsQixNQUFNLEVBQUUsTUFBTTtvQkFDZCxLQUFLLEVBQUUsTUFBTTtvQkFDYixXQUFXLEVBQUUsRUFBRTtvQkFDZixVQUFVLEVBQUUsUUFBUSxDQUFDLFdBQVc7b0JBQ2hDLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDMUIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztvQkFDckMsYUFBYSxFQUFFLFlBQVk7aUJBQzVCLENBQUM7WUFFRixPQUFPLEVBQUUsQ0FBQztvQkFDUixRQUFRLEVBQUUsRUFBRTtvQkFDWixNQUFNLEVBQUUsRUFBRTtvQkFDVixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsYUFBYSxFQUFFLFlBQVk7b0JBQzNCLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO2lCQUN0QyxDQUFDO1lBRUYsT0FBTyxFQUFFLENBQUM7b0JBQ1IsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixNQUFNLEVBQUUsU0FBUztpQkFDbEIsQ0FBQztTQUNILENBQUM7UUFFRixJQUFJLEVBQUUsVUFBQyxTQUFTLElBQUssT0FBQSxDQUFDO1lBQ3BCLEtBQUssRUFBRSxFQUFFO1lBQ1QsTUFBTSxFQUFFLEVBQUU7WUFDVixLQUFLLEVBQUUsUUFBUSxDQUFDLFlBQVk7WUFDNUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7WUFDckMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLGNBQWM7U0FDMUQsQ0FBQyxFQU5tQixDQU1uQjtRQUVGLEtBQUssRUFBRTtZQUNMLEtBQUssRUFBRSxFQUFFO1NBQ1Y7S0FDRjtJQUVELFlBQVksRUFBRTtRQUNaLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7UUFDcEMsVUFBVSxFQUFFLEVBQUU7UUFDZCxhQUFhLEVBQUUsRUFBRTtRQUNqQixHQUFHLEVBQUUsRUFBRTtRQUNQLElBQUksRUFBRSxDQUFDO1FBQ1AsS0FBSyxFQUFFLE1BQU07UUFDYixNQUFNLEVBQUUsUUFBUSxDQUFDLE9BQU87UUFDeEIsZUFBZSxFQUFFLFFBQVEsQ0FBQyxZQUFZO1FBQ3RDLFdBQVcsRUFBRSxFQUFFO1FBQ2YsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztRQUMvQixZQUFZLEVBQUUsZUFBYSxRQUFRLENBQUMsT0FBUztRQUM3QyxVQUFVLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNO1FBQ25DLE9BQU8sRUFBRSxDQUFDO0tBQ1g7SUFFRCxTQUFTLEVBQUU7UUFDVCxVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtRQUNyQyxVQUFVLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxPQUFPO1FBQ3BDLE9BQU8sRUFBRSxDQUFDO0tBQ1g7SUFFRCxRQUFRLEVBQUU7UUFDUixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1FBQy9CLFFBQVEsRUFBRSxFQUFFO1FBQ1osVUFBVSxFQUFFLE1BQU07UUFDbEIsTUFBTSxFQUFFLEVBQUU7UUFDVixTQUFTLEVBQUUsRUFBRTtRQUNiLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO1FBQ3JDLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtLQUMzQjtJQUVELGNBQWMsRUFBRTtRQUNkLFlBQVksRUFBRSxFQUFFO1FBRWhCLFNBQVMsRUFBRTtZQUNULE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDL0IsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsU0FBUztZQUNqQixZQUFZLEVBQUUsRUFBRTtZQUVoQixTQUFTLEVBQUUsVUFBQyxLQUFhLElBQUssT0FBQSxDQUFDO2dCQUM3QixLQUFLLEVBQUUsTUFBTTtnQkFDYixVQUFVLEVBQUUsS0FBSztnQkFDakIsZUFBZSxFQUFFLFNBQU8sS0FBSyxNQUFHO2dCQUNoQyxjQUFjLEVBQUUsT0FBTztnQkFDdkIsa0JBQWtCLEVBQUUsUUFBUTtnQkFDNUIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtnQkFDcEMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLFlBQVksRUFBRSxFQUFFO2FBQ2pCLENBQUMsRUFUNEIsQ0FTNUI7U0FDSDtRQUVELFdBQVcsRUFBRTtZQUNYLEtBQUssRUFBRSxNQUFNO1lBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixjQUFjLEVBQUUsZUFBZTtZQUMvQixRQUFRLEVBQUUsTUFBTTtZQUNoQixZQUFZLEVBQUUsRUFBRTtZQUVoQixPQUFPLEVBQUU7Z0JBQ1AsS0FBSyxFQUFFLGtCQUFrQjtnQkFDekIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztnQkFDL0IsWUFBWSxFQUFFLEVBQUU7Z0JBRWhCLFNBQVMsRUFBRSxVQUFDLEtBQWEsSUFBSyxPQUFBLENBQUM7b0JBQzdCLEtBQUssRUFBRSxNQUFNO29CQUNiLFVBQVUsRUFBRSxLQUFLO29CQUNqQixlQUFlLEVBQUUsU0FBTyxLQUFLLE1BQUc7b0JBQ2hDLGtCQUFrQixFQUFFLFFBQVE7b0JBQzVCLGNBQWMsRUFBRSxPQUFPO29CQUN2QixZQUFZLEVBQUUsRUFBRTtvQkFDaEIsV0FBVyxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtvQkFDdkMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7aUJBQ3RDLENBQUMsRUFUNEIsQ0FTNUI7YUFDSDtTQUNGO0tBQ0Y7SUFFRCxrQkFBa0IsRUFBRTtRQUNsQixNQUFNLEVBQUUsRUFBRTtLQUNYO0NBQ00sQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/category-detail/view-desktop.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};






var mainBlockContent = function (_a) {
    var list = _a.list, slug = _a.slug, isTagUrl = _a.isTagUrl;
    var txtExtra = true === isTagUrl ? '#' : '';
    return (0 === list.length
        ? null
        :
            react["createElement"]("div", null,
                react["createElement"]("magazine-breadcrumb", null,
                    react["createElement"](react_router_dom["NavLink"], { to: "" + routing["Ia" /* ROUTING_MAGAZINE */], style: style.headerMenu.textBreadCrumb }, "Magazine "),
                    react["createElement"]("span", { style: style.headerMenu.textBreadCrumb },
                        "/ ",
                        txtExtra,
                        slug)),
                react["createElement"]("div", { style: style.categoryDetail },
                    react["createElement"](item_list["a" /* default */], { list: list }))));
};
var renderView = function (props) {
    var _a = props, list = _a.list, slug = _a.slug, isTagUrl = _a.isTagUrl;
    var mainBlockProps = {
        showHeader: false,
        showViewMore: false,
        content: mainBlockContent({ list: list, slug: slug, isTagUrl: isTagUrl }),
        style: {}
    };
    return (react["createElement"]("magazine-category", null,
        react["createElement"](main_block["a" /* default */], __assign({}, mainBlockProps))));
};
/* harmony default export */ var view_desktop = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1kZXNrdG9wLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1kZXNrdG9wLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUUzQyxPQUFPLFNBQVMsTUFBTSxzQ0FBc0MsQ0FBQztBQUM3RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUUxRSxPQUFPLFFBQVEsTUFBTSxjQUFjLENBQUM7QUFHcEMsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRTVCLElBQU0sZ0JBQWdCLEdBQUcsVUFBQyxFQUF3QjtRQUF0QixjQUFJLEVBQUUsY0FBSSxFQUFFLHNCQUFRO0lBQzlDLElBQU0sUUFBUSxHQUFHLElBQUksS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBRTlDLE1BQU0sQ0FBQyxDQUNMLENBQUMsS0FBSyxJQUFJLENBQUMsTUFBTTtRQUNmLENBQUMsQ0FBQyxJQUFJO1FBQ04sQ0FBQztZQUNEO2dCQUNFO29CQUNFLG9CQUFDLE9BQU8sSUFBQyxFQUFFLEVBQUUsS0FBRyxnQkFBa0IsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLFVBQVUsQ0FBQyxjQUFjLGdCQUFxQjtvQkFDL0YsOEJBQU0sS0FBSyxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsY0FBYzs7d0JBQUssUUFBUTt3QkFBRSxJQUFJLENBQVEsQ0FDbkQ7Z0JBQ3RCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsY0FBYztvQkFDOUIsb0JBQUMsUUFBUSxJQUFDLElBQUksRUFBRSxJQUFJLEdBQUksQ0FDcEIsQ0FDRixDQUNULENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixJQUFNLFVBQVUsR0FBRyxVQUFDLEtBQUs7SUFDakIsSUFBQSxVQUEwQyxFQUF4QyxjQUFJLEVBQUUsY0FBSSxFQUFFLHNCQUFRLENBQXFCO0lBQ2pELElBQU0sY0FBYyxHQUFHO1FBQ3JCLFVBQVUsRUFBRSxLQUFLO1FBQ2pCLFlBQVksRUFBRSxLQUFLO1FBQ25CLE9BQU8sRUFBRSxnQkFBZ0IsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLFFBQVEsVUFBQSxFQUFFLENBQUM7UUFDbkQsS0FBSyxFQUFFLEVBQUU7S0FDVixDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0w7UUFDRSxvQkFBQyxTQUFTLGVBQUssY0FBYyxFQUFJLENBQ2YsQ0FDckIsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// CONCATENATED MODULE: ./components/magazine/category-detail/view-mobile.tsx
var view_mobile_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};







var handleRenderCategory = function (item) { return react["createElement"](react_router_dom["NavLink"], { to: routing["Ka" /* ROUTING_MAGAZINE_CATEGORY_PATH */] + "/" + (item && item.slug || ''), key: "category-item-" + (item && item.name || ''), style: style.category }, item && item.name || ''); };
var renderCategoryList = function (_a) {
    var showSubCategory = _a.showSubCategory, isSubCategoryOnTop = _a.isSubCategoryOnTop, categories = _a.categories;
    return (react["createElement"]("div", { style: [style.categoryList, showSubCategory && style.isShowing], className: 'backdrop-blur' }, Array.isArray(categories) && categories.map(handleRenderCategory)));
};
var view_mobile_mainBlockContent = function (_a) {
    var list = _a.list, slug = _a.slug, state = _a.state, handleShowSubCategory = _a.handleShowSubCategory, categories = _a.categories;
    var _b = state, isSubCategoryOnTop = _b.isSubCategoryOnTop, showSubCategory = _b.showSubCategory;
    var iconProps = {
        name: 'angle-down',
        style: style.headerMenu.icon(showSubCategory),
        innerStyle: style.headerMenu.inner
    };
    return list && 0 === list.length
        ? null
        : (react["createElement"]("div", null,
            react["createElement"]("magazine-breadcrumb", { style: style.headerMenuContainer },
                react["createElement"]("div", { style: [style.headerMenu, isSubCategoryOnTop && style.headerMenu.isTop], id: 'category-detail-menu' },
                    react["createElement"]("div", { style: style.headerMenuContainer.headerMenuWrap, onClick: handleShowSubCategory },
                        react["createElement"]("div", { style: style.headerMenuContainer.headerMenuWrap.backgroundBlur, className: 'backdrop-blur' }),
                        react["createElement"]("div", { style: style.headerMenu.textBreadCrumb }, slug),
                        react["createElement"](icon["a" /* default */], view_mobile_assign({}, iconProps)),
                        renderCategoryList({ showSubCategory: showSubCategory, isSubCategoryOnTop: isSubCategoryOnTop, categories: categories })))),
            react["createElement"]("div", { style: style.categoryDetail },
                react["createElement"](item_list["a" /* default */], { list: list }))));
};
var view_mobile_renderView = function (_a) {
    var props = _a.props, state = _a.state, handleShowSubCategory = _a.handleShowSubCategory;
    var _b = props, list = _b.list, slug = _b.slug, categories = _b.categories;
    var mainBlockProps = {
        showHeader: false,
        showViewMore: false,
        content: view_mobile_mainBlockContent({ list: list, slug: slug, state: state, handleShowSubCategory: handleShowSubCategory, categories: categories }),
        style: {}
    };
    return (react["createElement"]("magazine-category", null,
        react["createElement"](main_block["a" /* default */], view_mobile_assign({}, mainBlockProps))));
};
/* harmony default export */ var view_mobile = (view_mobile_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1tb2JpbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LW1vYmlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUMvQixPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFFM0MsT0FBTyxFQUFFLDhCQUE4QixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDeEYsT0FBTyxTQUFTLE1BQU0sc0NBQXNDLENBQUM7QUFDN0QsT0FBTyxJQUFJLE1BQU0sNkJBQTZCLENBQUM7QUFFL0MsT0FBTyxRQUFRLE1BQU0sY0FBYyxDQUFDO0FBR3BDLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixJQUFNLG9CQUFvQixHQUFHLFVBQUMsSUFBSSxJQUFLLE9BQUEsb0JBQUMsT0FBTyxJQUFDLEVBQUUsRUFBSyw4QkFBOEIsVUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxFQUFFLENBQUUsRUFBRSxHQUFHLEVBQUUsb0JBQWlCLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBRSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsUUFBUSxJQUFHLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBVyxFQUF4TCxDQUF3TCxDQUFDO0FBRWhPLElBQU0sa0JBQWtCLEdBQUcsVUFBQyxFQUFtRDtRQUFqRCxvQ0FBZSxFQUFFLDBDQUFrQixFQUFFLDBCQUFVO0lBQzNFLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUUsZUFBZSxJQUFJLEtBQUssQ0FBQyxTQUFTLENBQUMsRUFBRSxTQUFTLEVBQUUsZUFBZSxJQUM3RixLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLFVBQVUsQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FDOUQsQ0FDUCxDQUFBO0FBQ0gsQ0FBQyxDQUFDO0FBRUYsSUFBTSxnQkFBZ0IsR0FBRyxVQUFDLEVBQXdEO1FBQXRELGNBQUksRUFBRSxjQUFJLEVBQUUsZ0JBQUssRUFBRSxnREFBcUIsRUFBRSwwQkFBVTtJQUN4RSxJQUFBLFVBQXlELEVBQXZELDBDQUFrQixFQUFFLG9DQUFlLENBQXFCO0lBRWhFLElBQU0sU0FBUyxHQUFHO1FBQ2hCLElBQUksRUFBRSxZQUFZO1FBQ2xCLEtBQUssRUFBRSxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUM7UUFDN0MsVUFBVSxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsS0FBSztLQUNuQyxDQUFDO0lBRUYsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLE1BQU07UUFDOUIsQ0FBQyxDQUFDLElBQUk7UUFDTixDQUFDLENBQUMsQ0FDQTtZQUNFLDZDQUFxQixLQUFLLEVBQUUsS0FBSyxDQUFDLG1CQUFtQjtnQkFDbkQsNkJBQUssS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxrQkFBa0IsSUFBSSxLQUFLLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsRUFBRSxzQkFBc0I7b0JBQ3RHLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsbUJBQW1CLENBQUMsY0FBYyxFQUFFLE9BQU8sRUFBRSxxQkFBcUI7d0JBQ2xGLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLGNBQWMsRUFBRSxTQUFTLEVBQUUsZUFBZSxHQUFRO3dCQUN2Ryw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFVBQVUsQ0FBQyxjQUFjLElBQUcsSUFBSSxDQUFPO3dCQUN6RCxvQkFBQyxJQUFJLGVBQUssU0FBUyxFQUFJO3dCQUN0QixrQkFBa0IsQ0FBQyxFQUFFLGVBQWUsaUJBQUEsRUFBRSxrQkFBa0Isb0JBQUEsRUFBRSxVQUFVLFlBQUEsRUFBRSxDQUFDLENBQ3BFLENBQ0YsQ0FDYztZQUN0Qiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGNBQWM7Z0JBQzlCLG9CQUFDLFFBQVEsSUFBQyxJQUFJLEVBQUUsSUFBSSxHQUFJLENBQ3BCLENBQ0YsQ0FDUCxDQUFBO0FBQ0wsQ0FBQyxDQUFDO0FBRUYsSUFBTSxVQUFVLEdBQUcsVUFBQyxFQUF1QztRQUFyQyxnQkFBSyxFQUFFLGdCQUFLLEVBQUUsZ0RBQXFCO0lBQ2pELElBQUEsVUFBNEMsRUFBMUMsY0FBSSxFQUFFLGNBQUksRUFBRSwwQkFBVSxDQUFxQjtJQUVuRCxJQUFNLGNBQWMsR0FBRztRQUNyQixVQUFVLEVBQUUsS0FBSztRQUNqQixZQUFZLEVBQUUsS0FBSztRQUNuQixPQUFPLEVBQUUsZ0JBQWdCLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxLQUFLLE9BQUEsRUFBRSxxQkFBcUIsdUJBQUEsRUFBRSxVQUFVLFlBQUEsRUFBRSxDQUFDO1FBQ25GLEtBQUssRUFBRSxFQUFFO0tBQ1YsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMO1FBQ0Usb0JBQUMsU0FBUyxlQUFLLGNBQWMsRUFBSSxDQUNmLENBQ3JCLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/category-detail/view.tsx


var view_renderView = function (_a) {
    var props = _a.props, state = _a.state, handleShowSubCategory = _a.handleShowSubCategory;
    var switchView = {
        MOBILE: function () { return view_mobile({ props: props, state: state, handleShowSubCategory: handleShowSubCategory }); },
        DESKTOP: function () { return view_desktop(props); }
    };
    return switchView[window.DEVICE_VERSION]();
};
/* harmony default export */ var view = (view_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sYUFBYSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sWUFBWSxNQUFNLGVBQWUsQ0FBQztBQUV6QyxJQUFNLFVBQVUsR0FBRyxVQUFDLEVBQXVDO1FBQXJDLGdCQUFLLEVBQUUsZ0JBQUssRUFBRSxnREFBcUI7SUFDdkQsSUFBTSxVQUFVLEdBQUc7UUFDakIsTUFBTSxFQUFFLGNBQU0sT0FBQSxZQUFZLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxLQUFLLE9BQUEsRUFBRSxxQkFBcUIsdUJBQUEsRUFBRSxDQUFDLEVBQXJELENBQXFEO1FBQ25FLE9BQU8sRUFBRSxjQUFNLE9BQUEsYUFBYSxDQUFDLEtBQUssQ0FBQyxFQUFwQixDQUFvQjtLQUNwQyxDQUFDO0lBRUYsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQztBQUM3QyxDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/category-detail/initialize.tsx
var DEFAULT_PROPS = {
    list: [],
    categories: [],
    slug: '',
    isTagUrl: false,
};
var INITIAL_STATE = {
    isSubCategoryOnTop: false,
    heightSubCategoryToTop: 0,
    showSubCategory: false
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLFVBQVUsRUFBRSxFQUFFO0lBQ2QsSUFBSSxFQUFFLEVBQUU7SUFDUixRQUFRLEVBQUUsS0FBSztDQUNOLENBQUM7QUFFWixNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUc7SUFDM0Isa0JBQWtCLEVBQUUsS0FBSztJQUN6QixzQkFBc0IsRUFBRSxDQUFDO0lBQ3pCLGVBQWUsRUFBRSxLQUFLO0NBQ2IsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/category-detail/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_MagazineCategory = /** @class */ (function (_super) {
    __extends(MagazineCategory, _super);
    function MagazineCategory(props) {
        return _super.call(this, props) || this;
    }
    MagazineCategory.prototype.componentDidMount = function () {
        window.addEventListener('scroll', this.handleScroll.bind(this));
    };
    MagazineCategory.prototype.handleScroll = function () {
        var _a = this.state, isSubCategoryOnTop = _a.isSubCategoryOnTop, heightSubCategoryToTop = _a.heightSubCategoryToTop;
        var eleInfo = this.getPositionElementById('category-detail-menu');
        eleInfo
            && eleInfo.top <= 0
            && !isSubCategoryOnTop
            && this.setState({ isSubCategoryOnTop: true, heightSubCategoryToTop: window.scrollY });
        heightSubCategoryToTop >= window.scrollY
            && isSubCategoryOnTop
            && this.setState({ isSubCategoryOnTop: false });
    };
    MagazineCategory.prototype.getPositionElementById = function (elementId) {
        var el = document.getElementById(elementId);
        return el && el.getBoundingClientRect();
    };
    MagazineCategory.prototype.componentWillUnmount = function () {
        window.addEventListener('scroll', function () { });
    };
    MagazineCategory.prototype.handleShowSubCategory = function () {
        this.setState({ showSubCategory: !this.state.showSubCategory });
    };
    MagazineCategory.prototype.componentWillReceiveProps = function (nextProps) {
        this.props.slug !== nextProps.slug
            && this.state.showSubCategory
            && this.setState({ showSubCategory: false });
    };
    MagazineCategory.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state,
            handleShowSubCategory: this.handleShowSubCategory.bind(this)
        };
        return view(args);
    };
    ;
    MagazineCategory.defaultProps = DEFAULT_PROPS;
    MagazineCategory = __decorate([
        radium
    ], MagazineCategory);
    return MagazineCategory;
}(react["PureComponent"]));
/* harmony default export */ var component = (component_MagazineCategory);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFFaEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUc3QztJQUErQixvQ0FBNkI7SUFFMUQsMEJBQVksS0FBYTtlQUN2QixrQkFBTSxLQUFLLENBQUM7SUFDZCxDQUFDO0lBRUQsNENBQWlCLEdBQWpCO1FBQ0UsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7SUFFRCx1Q0FBWSxHQUFaO1FBQ1EsSUFBQSxlQUFxRSxFQUFuRSwwQ0FBa0IsRUFBRSxrREFBc0IsQ0FBMEI7UUFFNUUsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFFbEUsT0FBTztlQUNGLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQztlQUNoQixDQUFDLGtCQUFrQjtlQUNuQixJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsa0JBQWtCLEVBQUUsSUFBSSxFQUFFLHNCQUFzQixFQUFFLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1FBRXpGLHNCQUFzQixJQUFJLE1BQU0sQ0FBQyxPQUFPO2VBQ25DLGtCQUFrQjtlQUNsQixJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsa0JBQWtCLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsaURBQXNCLEdBQXRCLFVBQXVCLFNBQVM7UUFDOUIsSUFBTSxFQUFFLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM5QyxNQUFNLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO0lBQzFDLENBQUM7SUFFRCwrQ0FBb0IsR0FBcEI7UUFDRSxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLGNBQVEsQ0FBQyxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVELGdEQUFxQixHQUFyQjtRQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxlQUFlLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7SUFDbEUsQ0FBQztJQUVELG9EQUF5QixHQUF6QixVQUEwQixTQUFTO1FBQ2pDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLFNBQVMsQ0FBQyxJQUFJO2VBQzdCLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZTtlQUMxQixJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELGlDQUFNLEdBQU47UUFDRSxJQUFNLElBQUksR0FBRztZQUNYLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIscUJBQXFCLEVBQUUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDN0QsQ0FBQTtRQUVELE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDMUIsQ0FBQztJQUFBLENBQUM7SUFuREssNkJBQVksR0FBRyxhQUFhLENBQUM7SUFEaEMsZ0JBQWdCO1FBRHJCLE1BQU07T0FDRCxnQkFBZ0IsQ0FxRHJCO0lBQUQsdUJBQUM7Q0FBQSxBQXJERCxDQUErQixhQUFhLEdBcUQzQztBQUVELGVBQWUsZ0JBQWdCLENBQUMifQ==
// CONCATENATED MODULE: ./components/magazine/category-detail/index.tsx

/* harmony default export */ var category_detail = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxnQkFBZ0IsTUFBTSxhQUFhLENBQUM7QUFDM0MsZUFBZSxnQkFBZ0IsQ0FBQyJ9
// EXTERNAL MODULE: ./components/magazine/category/index.tsx + 7 modules
var category = __webpack_require__(797);

// EXTERNAL MODULE: ./components/magazine/right-widget-group/index.tsx + 10 modules
var right_widget_group = __webpack_require__(845);

// EXTERNAL MODULE: ./components/ui/loading-placeholder/index.tsx + 4 modules
var loading_placeholder = __webpack_require__(750);

// CONCATENATED MODULE: ./container/app-shop/magazine/category/style.tsx

/* harmony default export */ var category_style = ({
    tempText: {
        width: '100%',
        textAlign: 'center',
        fontSize: 20,
        lineHeight: '150px'
    },
    placeholder: {
        width: '100%',
        paddingTop: 20,
        imgCover: {
            width: '100%',
            height: 510,
            backgroundColor: variable["colorE5"],
            position: variable["position"].relative,
            marginBottom: 30,
            item: {
                position: variable["position"].absolute,
                left: 0,
                bottom: 25,
                backgroundColor: variable["colorWhite08"],
                paddingTop: 20,
                paddingRight: 20,
                paddingBottom: 20,
                paddingLeft: 20,
                width: '50%',
                height: 125,
                display: variable["display"].flex,
                justifyContent: 'space-between',
                alignItems: 'center',
                flexDirection: 'column',
                text: {
                    width: '100%',
                    height: 20,
                }
            }
        },
        title: {
            background: variable["colorWhite"],
            display: variable["display"].flex,
            alignItems: 'center',
            width: '100%',
            height: 60,
            marginLeft: 10,
            text: {
                width: '40%',
                height: 30
            }
        },
        control: {
            width: '100%',
            display: variable["display"].flex,
            justifyContent: 'space-between',
            paddingLeft: 10,
            paddingRight: 10,
            marginBottom: 20,
        },
        controlItem: {
            width: 150,
            height: 30,
            background: variable["colorF7"]
        },
        productList: {
            display: variable["display"].flex,
            flexWrap: 'wrap'
        },
        productMobileItem: {
            minWidth: '50%',
            width: '50%',
        },
        productItem: {
            flex: 1,
            paddingLeft: 10,
            paddingRight: 10,
            marginBottom: 20,
            minWidth: '20%',
            width: '20%',
            image: {
                width: '100%',
                height: 'auto',
                paddingTop: '82%',
                marginBottom: 10,
            },
            text: {
                width: '94%',
                height: 25,
                marginBottom: 10,
            },
            lastText: {
                width: '65%',
                height: 25,
            }
        },
        mobile: {
            width: '100%',
            image: {
                width: '100%',
                height: 192,
                marginBottom: 10
            },
            text: {
                height: 48,
                paddingTop: 0,
                paddingRight: 10,
                paddingBottom: 10,
                paddingLeft: 0,
                marginBottom: 10,
                marginLeft: 10,
                marginRight: 10
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSw0QkFBNEIsQ0FBQztBQUV2RCxlQUFlO0lBQ2IsUUFBUSxFQUFFO1FBQ1IsS0FBSyxFQUFFLE1BQU07UUFDYixTQUFTLEVBQUUsUUFBUTtRQUNuQixRQUFRLEVBQUUsRUFBRTtRQUNaLFVBQVUsRUFBRSxPQUFPO0tBQ3BCO0lBRUQsV0FBVyxFQUFFO1FBQ1gsS0FBSyxFQUFFLE1BQU07UUFDYixVQUFVLEVBQUUsRUFBRTtRQUVkLFFBQVEsRUFBRTtZQUNSLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLEdBQUc7WUFDWCxlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87WUFDakMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxZQUFZLEVBQUUsRUFBRTtZQUVoQixJQUFJLEVBQUU7Z0JBQ0osUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtnQkFDcEMsSUFBSSxFQUFFLENBQUM7Z0JBQ1AsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsZUFBZSxFQUFFLFFBQVEsQ0FBQyxZQUFZO2dCQUN0QyxVQUFVLEVBQUUsRUFBRTtnQkFDZCxZQUFZLEVBQUUsRUFBRTtnQkFDaEIsYUFBYSxFQUFFLEVBQUU7Z0JBQ2pCLFdBQVcsRUFBRSxFQUFFO2dCQUNmLEtBQUssRUFBRSxLQUFLO2dCQUNaLE1BQU0sRUFBRSxHQUFHO2dCQUNYLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLGNBQWMsRUFBRSxlQUFlO2dCQUMvQixVQUFVLEVBQUUsUUFBUTtnQkFDcEIsYUFBYSxFQUFFLFFBQVE7Z0JBRXZCLElBQUksRUFBRTtvQkFDSixLQUFLLEVBQUUsTUFBTTtvQkFDYixNQUFNLEVBQUUsRUFBRTtpQkFDWDthQUNGO1NBQ0Y7UUFFRCxLQUFLLEVBQUU7WUFDTCxVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDL0IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixVQUFVLEVBQUUsUUFBUTtZQUNwQixLQUFLLEVBQUUsTUFBTTtZQUNiLE1BQU0sRUFBRSxFQUFFO1lBQ1YsVUFBVSxFQUFFLEVBQUU7WUFFZCxJQUFJLEVBQUU7Z0JBQ0osS0FBSyxFQUFFLEtBQUs7Z0JBQ1osTUFBTSxFQUFFLEVBQUU7YUFDWDtTQUNGO1FBRUQsT0FBTyxFQUFFO1lBQ1AsS0FBSyxFQUFFLE1BQU07WUFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLGNBQWMsRUFBRSxlQUFlO1lBQy9CLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLEVBQUU7WUFDaEIsWUFBWSxFQUFFLEVBQUU7U0FDakI7UUFFRCxXQUFXLEVBQUU7WUFDWCxLQUFLLEVBQUUsR0FBRztZQUNWLE1BQU0sRUFBRSxFQUFFO1lBQ1YsVUFBVSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1NBQzdCO1FBRUQsV0FBVyxFQUFFO1lBQ1gsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixRQUFRLEVBQUUsTUFBTTtTQUNqQjtRQUVELGlCQUFpQixFQUFFO1lBQ2pCLFFBQVEsRUFBRSxLQUFLO1lBQ2YsS0FBSyxFQUFFLEtBQUs7U0FDYjtRQUVELFdBQVcsRUFBRTtZQUNYLElBQUksRUFBRSxDQUFDO1lBQ1AsV0FBVyxFQUFFLEVBQUU7WUFDZixZQUFZLEVBQUUsRUFBRTtZQUNoQixZQUFZLEVBQUUsRUFBRTtZQUNoQixRQUFRLEVBQUUsS0FBSztZQUNmLEtBQUssRUFBRSxLQUFLO1lBRVosS0FBSyxFQUFFO2dCQUNMLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxNQUFNO2dCQUNkLFVBQVUsRUFBRSxLQUFLO2dCQUNqQixZQUFZLEVBQUUsRUFBRTthQUNqQjtZQUVELElBQUksRUFBRTtnQkFDSixLQUFLLEVBQUUsS0FBSztnQkFDWixNQUFNLEVBQUUsRUFBRTtnQkFDVixZQUFZLEVBQUUsRUFBRTthQUNqQjtZQUVELFFBQVEsRUFBRTtnQkFDUixLQUFLLEVBQUUsS0FBSztnQkFDWixNQUFNLEVBQUUsRUFBRTthQUNYO1NBQ0Y7UUFFRCxNQUFNLEVBQUU7WUFDTixLQUFLLEVBQUUsTUFBTTtZQUViLEtBQUssRUFBRTtnQkFDTCxLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsR0FBRztnQkFDWCxZQUFZLEVBQUUsRUFBRTthQUNqQjtZQUVELElBQUksRUFBRTtnQkFDSixNQUFNLEVBQUUsRUFBRTtnQkFDVixVQUFVLEVBQUUsQ0FBQztnQkFDYixZQUFZLEVBQUUsRUFBRTtnQkFDaEIsYUFBYSxFQUFFLEVBQUU7Z0JBQ2pCLFdBQVcsRUFBRSxDQUFDO2dCQUNkLFlBQVksRUFBRSxFQUFFO2dCQUNoQixVQUFVLEVBQUUsRUFBRTtnQkFDZCxXQUFXLEVBQUUsRUFBRTthQUNoQjtTQUNGO0tBQ0Y7Q0FDTSxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/magazine/category/view.tsx
var view_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};















var renderItemPlaceholder = function (item) { return (react["createElement"]("div", { style: [
        category_style.placeholder.productItem,
        'MOBILE' === window.DEVICE_VERSION && category_style.placeholder.productMobileItem
    ], key: item },
    react["createElement"](loading_placeholder["a" /* default */], { style: category_style.placeholder.productItem.image }),
    react["createElement"](loading_placeholder["a" /* default */], { style: category_style.placeholder.productItem.text }),
    react["createElement"](loading_placeholder["a" /* default */], { style: category_style.placeholder.productItem.text }),
    react["createElement"](loading_placeholder["a" /* default */], { style: category_style.placeholder.productItem.lastText }))); };
var switchRenderLoadingPlaceholder = {
    DESKTOP: function () { return (react["createElement"]("div", null,
        react["createElement"]("div", { style: category_style.placeholder.title },
            react["createElement"](loading_placeholder["a" /* default */], { style: category_style.placeholder.title.text })),
        react["createElement"]("div", { style: category_style.placeholder.imgCover },
            react["createElement"]("div", { style: category_style.placeholder.imgCover.item },
                react["createElement"](loading_placeholder["a" /* default */], { style: [category_style.placeholder.imgCover.item.text, { marginBottom: 10 }] }),
                react["createElement"](loading_placeholder["a" /* default */], { style: [category_style.placeholder.imgCover.item.text, { marginBottom: 10 }] }),
                react["createElement"](loading_placeholder["a" /* default */], { style: category_style.placeholder.imgCover.item.text }))),
        react["createElement"]("div", { style: category_style.placeholder.productList }, [1, 2].map(function (item) { return renderItemPlaceholder(item); })))); },
    MOBILE: function () { return (react["createElement"]("div", null,
        react["createElement"]("div", { style: category_style.placeholder.title },
            react["createElement"](loading_placeholder["a" /* default */], { style: category_style.placeholder.title.text })),
        react["createElement"](loading_placeholder["a" /* default */], { style: category_style.placeholder.mobile.image }),
        react["createElement"](loading_placeholder["a" /* default */], { style: category_style.placeholder.mobile.text }),
        react["createElement"](loading_placeholder["a" /* default */], { style: category_style.placeholder.mobile.text }),
        react["createElement"]("div", { style: category_style.placeholder.productList }, [1, 2].map(function (item) { return renderItemPlaceholder(item); })))); }
};
var renderSubContainer = function (_a) {
    var magazineDefaultList = _a.magazineDefaultList, widgetList = _a.widgetList, expertsReviewList = _a.expertsReviewList;
    var widgetProps = {
        dataList: [
            {
                list: magazineDefaultList,
                type: magazine_widget["a" /* MAGAZINE_WIDGET_TYPE */].ONE.type,
                title: 'Đừng bỏ lỡ'
            },
            {
                list: widgetList,
                type: magazine_widget["a" /* MAGAZINE_WIDGET_TYPE */].TWO.type,
                title: 'Ingredient'
            },
            {
                list: expertsReviewList,
                type: magazine_widget["a" /* MAGAZINE_WIDGET_TYPE */].TWO.type,
                title: magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].EXPERTS_REVIEW.title
            }
        ]
    };
    return react["createElement"](right_widget_group["a" /* default */], view_assign({}, widgetProps));
};
var renderMainContainer = function (_a) {
    var per = _a.per, slug = _a.slug, list = _a.list, total = _a.total, current = _a.current, urlList = _a.urlList, isTagUrl = _a.isTagUrl, categories = _a.categories;
    var categoryDetailProps = {
        list: list,
        slug: slug,
        isTagUrl: isTagUrl,
        categories: categories
    };
    var paginationProps = {
        per: per,
        total: total,
        urlList: urlList,
        current: current
    };
    return Array.isArray(list) && 0 === list.length
        ? switchRenderLoadingPlaceholder[window.DEVICE_VERSION]()
        : (react["createElement"]("div", null,
            react["createElement"](category_detail, view_assign({}, categoryDetailProps)),
            react["createElement"](pagination["a" /* default */], view_assign({}, paginationProps))));
};
var isTagOrCategory = function (slug) {
    return 'tag' === slug;
};
var category_view_renderView = function (_a) {
    var props = _a.props, state = _a.state, handleFetchMagazineVideo = _a.handleFetchMagazineVideo;
    var _b = props, location = _b.location, magazineVideoParams = _b.magazineVideoParams, magazineDefaultParams = _b.magazineDefaultParams, _c = _b.match.params, idCategory = _c.idCategory, idTag = _c.idTag, _d = _b.magazineStore, magazineCategory = _d.magazineCategory, magazineList = _d.magazineList, magazineDashboard = _d.magazineDashboard, magazineTagName = _d.magazineTagName, perPage = _b.perPage;
    var _e = state, page = _e.page, urlList = _e.urlList, isPriorityBlock = _e.isPriorityBlock, isFetchMagazineVideo = _e.isFetchMagazineVideo;
    // TODO
    var arrPath = location.pathname.split('/');
    var path = arrPath.length > 2 ? arrPath[2] : '';
    var slug = isTagOrCategory(path) ? idTag : idCategory;
    var data = isTagOrCategory(path) ? magazineTagName : magazineCategory;
    var keyHash = Object(encode["j" /* objectToHash */])({ slug: slug, page: page, perPage: perPage });
    var list = !Object(validate["l" /* isUndefined */])(data[keyHash]) ? data[keyHash].magazines : [];
    var keyHashMagazineVideo = Object(encode["j" /* objectToHash */])(magazineVideoParams);
    var videoList = !Object(validate["l" /* isUndefined */])(magazineList[keyHashMagazineVideo]) ? magazineList[keyHashMagazineVideo] : [];
    console.error('videoList', videoList);
    var widgetList = !Object(validate["l" /* isUndefined */])(magazineDashboard.magazines) ? magazineDashboard.magazines[magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].THREE.type].magazines : [];
    var expertsReviewList = !Object(validate["l" /* isUndefined */])(magazineDashboard.magazines) ? magazineDashboard.magazines[magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].EXPERTS_REVIEW.type].magazines : [];
    var keyHashMagazineDefault = Object(encode["j" /* objectToHash */])(magazineDefaultParams);
    var magazineDefaultList = !Object(validate["l" /* isUndefined */])(magazineList[keyHashMagazineDefault]) ? magazineList[keyHashMagazineDefault] : [];
    var _urlList = 0 !== list.length ? urlList : [];
    var _f = data[keyHash] && data[keyHash].paging || { current_page: 0, per_page: 0, total_pages: 0 }, current_page = _f.current_page, per_page = _f.per_page, total_pages = _f.total_pages;
    var _categories = magazineDashboard && magazineDashboard.categories || [];
    var categories = _categories.filter(function (item) { return item.slug !== slug; });
    var renderMagazineVideos = function (isVisible) {
        !!isVisible
            && !isFetchMagazineVideo
            && !!handleFetchMagazineVideo
            && handleFetchMagazineVideo();
        console.error('isVisible', isVisible, isFetchMagazineVideo);
        return react["createElement"](category["a" /* default */], { list: videoList, type: magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].VIDEO.type });
    };
    var splitLayoutProps = {
        type: 'right',
        subContainer: renderSubContainer({ magazineDefaultList: magazineDefaultList, widgetList: widgetList, expertsReviewList: expertsReviewList }),
        mainContainer: renderMainContainer({
            slug: slug,
            list: list,
            per: per_page,
            urlList: _urlList,
            total: total_pages,
            current: current_page,
            isTagUrl: isTagOrCategory(path),
            categories: categories
        })
    };
    var switchView = {
        MOBILE: function () { return (react["createElement"]("div", null,
            renderMainContainer({
                slug: slug,
                list: list,
                categories: categories,
                per: per_page,
                urlList: _urlList,
                total: total_pages,
                current: current_page,
                isTagUrl: isTagOrCategory(path)
            }),
            !isPriorityBlock
                ? (react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                    var isVisible = _a.isVisible;
                    return renderMagazineVideos(isVisible);
                })) : react["createElement"](loading["a" /* default */], { style: { height: 400 } }))); },
        DESKTOP: function () { return (react["createElement"](wrap["a" /* default */], null,
            react["createElement"](split["a" /* default */], view_assign({}, splitLayoutProps)),
            !isPriorityBlock
                ? (react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                    var isVisible = _a.isVisible;
                    return renderMagazineVideos(isVisible);
                })) : react["createElement"]("div", null, "HDHDHFDFHDFHF") //<Loading style={{ height: 400 }} />
        )); }
    };
    return (react["createElement"]("magazine-category-container", null, switchView[window.DEVICE_VERSION]()));
};
/* harmony default export */ var category_view = (category_view_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxlQUFlLE1BQU0saUJBQWlCLENBQUM7QUFFOUMsT0FBTyxPQUFPLE1BQU0sbUNBQW1DLENBQUM7QUFDeEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3pELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxtREFBbUQsQ0FBQztBQUN6RixPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSxxREFBcUQsQ0FBQztBQUU3RixPQUFPLFVBQVUsTUFBTSwyQ0FBMkMsQ0FBQztBQUNuRSxPQUFPLFVBQVUsTUFBTSxzQkFBc0IsQ0FBQztBQUM5QyxPQUFPLFdBQVcsTUFBTSx1QkFBdUIsQ0FBQztBQUNoRCxPQUFPLGNBQWMsTUFBTSxpREFBaUQsQ0FBQztBQUM3RSxPQUFPLGdCQUFnQixNQUFNLDBDQUEwQyxDQUFDO0FBQ3hFLE9BQU8sZ0JBQWdCLE1BQU0sb0RBQW9ELENBQUM7QUFDbEYsT0FBTyxrQkFBa0IsTUFBTSwrQ0FBK0MsQ0FBQztBQUcvRSxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQUcsVUFBQyxJQUFJLElBQUssT0FBQSxDQUM3Qyw2QkFDRSxLQUFLLEVBQUU7UUFDTCxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVc7UUFDN0IsUUFBUSxLQUFLLE1BQU0sQ0FBQyxjQUFjLElBQUksS0FBSyxDQUFDLFdBQVcsQ0FBQyxpQkFBaUI7S0FDMUUsRUFDRCxHQUFHLEVBQUUsSUFBSTtJQUNULG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEdBQUk7SUFDbEUsb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksR0FBSTtJQUNqRSxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxHQUFJO0lBQ2pFLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxRQUFRLEdBQUksQ0FDakUsQ0FDUCxFQVo4QyxDQVk5QyxDQUFDO0FBRUYsSUFBTSw4QkFBOEIsR0FBRztJQUNyQyxPQUFPLEVBQUUsY0FBTSxPQUFBLENBQ2I7UUFDRSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxLQUFLO1lBQ2pDLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUksQ0FDdkQ7UUFDTiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxRQUFRO1lBQ3BDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJO2dCQUN6QyxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxDQUFDLEdBQUk7Z0JBQzNGLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLENBQUMsR0FBSTtnQkFDM0Ysb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUksQ0FDL0QsQ0FDRjtRQUNOLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsSUFFckMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBSSxJQUFLLE9BQUEscUJBQXFCLENBQUMsSUFBSSxDQUFDLEVBQTNCLENBQTJCLENBQUMsQ0FFL0MsQ0FDRixDQUNQLEVBbEJjLENBa0JkO0lBQ0QsTUFBTSxFQUFFLGNBQU0sT0FBQSxDQUNaO1FBQ0UsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsS0FBSztZQUNqQyxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFJLENBQ3ZEO1FBQ04sb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBSTtRQUM3RCxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFJO1FBQzVELG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEdBQUk7UUFDNUQsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxJQUVyQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsRUFBM0IsQ0FBMkIsQ0FBQyxDQUUvQyxDQUNGLENBQ1AsRUFkYSxDQWNiO0NBQ0YsQ0FBQztBQUVGLElBQU0sa0JBQWtCLEdBQUcsVUFBQyxFQUFzRDtRQUFwRCw0Q0FBbUIsRUFBRSwwQkFBVSxFQUFFLHdDQUFpQjtJQUM5RSxJQUFNLFdBQVcsR0FBRztRQUNsQixRQUFRLEVBQUU7WUFDUjtnQkFDRSxJQUFJLEVBQUUsbUJBQW1CO2dCQUN6QixJQUFJLEVBQUUsb0JBQW9CLENBQUMsR0FBRyxDQUFDLElBQUk7Z0JBQ25DLEtBQUssRUFBRSxZQUFZO2FBQ3BCO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLElBQUksRUFBRSxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsSUFBSTtnQkFDbkMsS0FBSyxFQUFFLFlBQVk7YUFDcEI7WUFDRDtnQkFDRSxJQUFJLEVBQUUsaUJBQWlCO2dCQUN2QixJQUFJLEVBQUUsb0JBQW9CLENBQUMsR0FBRyxDQUFDLElBQUk7Z0JBQ25DLEtBQUssRUFBRSxzQkFBc0IsQ0FBQyxjQUFjLENBQUMsS0FBSzthQUNuRDtTQUNGO0tBQ0YsQ0FBQztJQUVGLE1BQU0sQ0FBQyxvQkFBQyxnQkFBZ0IsZUFBSyxXQUFXLEVBQUksQ0FBQztBQUMvQyxDQUFDLENBQUM7QUFFRixJQUFNLG1CQUFtQixHQUFHLFVBQUMsRUFTNUI7UUFSQyxZQUFHLEVBQ0gsY0FBSSxFQUNKLGNBQUksRUFDSixnQkFBSyxFQUNMLG9CQUFPLEVBQ1Asb0JBQU8sRUFDUCxzQkFBUSxFQUNSLDBCQUFVO0lBRVYsSUFBTSxtQkFBbUIsR0FBRztRQUMxQixJQUFJLE1BQUE7UUFDSixJQUFJLE1BQUE7UUFDSixRQUFRLFVBQUE7UUFDUixVQUFVLFlBQUE7S0FDWCxDQUFDO0lBRUYsSUFBTSxlQUFlLEdBQUc7UUFDdEIsR0FBRyxLQUFBO1FBQ0gsS0FBSyxPQUFBO1FBQ0wsT0FBTyxTQUFBO1FBQ1AsT0FBTyxTQUFBO0tBQ1IsQ0FBQztJQUVGLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsTUFBTTtRQUM3QyxDQUFDLENBQUMsOEJBQThCLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxFQUFFO1FBQ3pELENBQUMsQ0FBQyxDQUNBO1lBQ0Usb0JBQUMsY0FBYyxlQUFLLG1CQUFtQixFQUFJO1lBQzNDLG9CQUFDLFVBQVUsZUFBSyxlQUFlLEVBQUksQ0FDL0IsQ0FDUCxDQUFDO0FBQ04sQ0FBQyxDQUFDO0FBRUYsSUFBTSxlQUFlLEdBQUcsVUFBQyxJQUFJO0lBQzNCLE1BQU0sQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDO0FBQ3hCLENBQUMsQ0FBQTtBQUVELElBQU0sVUFBVSxHQUFHLFVBQUMsRUFBMEM7UUFBeEMsZ0JBQUssRUFBRSxnQkFBSyxFQUFFLHNEQUF3QjtJQUNwRCxJQUFBLFVBT2EsRUFOakIsc0JBQVEsRUFDUiw0Q0FBbUIsRUFDbkIsZ0RBQXFCLEVBQ1osb0JBQTZCLEVBQW5CLDBCQUFVLEVBQUUsZ0JBQUssRUFDcEMscUJBQXFGLEVBQXBFLHNDQUFnQixFQUFFLDhCQUFZLEVBQUUsd0NBQWlCLEVBQUUsb0NBQWUsRUFDbkYsb0JBQU8sQ0FDVztJQUVkLElBQUEsVUFLYSxFQUpqQixjQUFJLEVBQ0osb0JBQU8sRUFDUCxvQ0FBZSxFQUNmLDhDQUFvQixDQUNGO0lBRXBCLE9BQU87SUFDUCxJQUFNLE9BQU8sR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUM3QyxJQUFNLElBQUksR0FBRyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDbEQsSUFBTSxJQUFJLEdBQUcsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQztJQUN4RCxJQUFNLElBQUksR0FBRyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUM7SUFFeEUsSUFBTSxPQUFPLEdBQUcsWUFBWSxDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDO0lBQ3RELElBQU0sSUFBSSxHQUFHLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFFeEUsSUFBTSxvQkFBb0IsR0FBRyxZQUFZLENBQUMsbUJBQW1CLENBQUMsQ0FBQztJQUMvRCxJQUFNLFNBQVMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQzdHLE9BQU8sQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLFNBQVMsQ0FBQyxDQUFDO0lBRXRDLElBQU0sVUFBVSxHQUFHLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQzdJLElBQU0saUJBQWlCLEdBQUcsQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFFN0osSUFBTSxzQkFBc0IsR0FBRyxZQUFZLENBQUMscUJBQXFCLENBQUMsQ0FBQztJQUNuRSxJQUFNLG1CQUFtQixHQUFHLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFFM0gsSUFBTSxRQUFRLEdBQUcsQ0FBQyxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBRTVDLElBQUEsOEZBQ3FGLEVBRG5GLDhCQUFZLEVBQUUsc0JBQVEsRUFBRSw0QkFBVyxDQUNnRDtJQUUzRixJQUFNLFdBQVcsR0FBRyxpQkFBaUIsSUFBSSxpQkFBaUIsQ0FBQyxVQUFVLElBQUksRUFBRSxDQUFDO0lBQzVFLElBQU0sVUFBVSxHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsSUFBSSxLQUFLLElBQUksRUFBbEIsQ0FBa0IsQ0FBQyxDQUFDO0lBRWxFLElBQU0sb0JBQW9CLEdBQUcsVUFBQyxTQUFTO1FBQ3JDLENBQUMsQ0FBQyxTQUFTO2VBQ04sQ0FBQyxvQkFBb0I7ZUFDckIsQ0FBQyxDQUFDLHdCQUF3QjtlQUMxQix3QkFBd0IsRUFBRSxDQUFDO1FBRWhDLE9BQU8sQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLFNBQVMsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDO1FBRTVELE1BQU0sQ0FBQyxvQkFBQyxnQkFBZ0IsSUFBQyxJQUFJLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFJLENBQUE7SUFDdkYsQ0FBQyxDQUFBO0lBRUQsSUFBTSxnQkFBZ0IsR0FBRztRQUN2QixJQUFJLEVBQUUsT0FBTztRQUNiLFlBQVksRUFBRSxrQkFBa0IsQ0FBQyxFQUFFLG1CQUFtQixxQkFBQSxFQUFFLFVBQVUsWUFBQSxFQUFFLGlCQUFpQixtQkFBQSxFQUFFLENBQUM7UUFDeEYsYUFBYSxFQUFFLG1CQUFtQixDQUFDO1lBQ2pDLElBQUksTUFBQTtZQUNKLElBQUksTUFBQTtZQUNKLEdBQUcsRUFBRSxRQUFRO1lBQ2IsT0FBTyxFQUFFLFFBQVE7WUFDakIsS0FBSyxFQUFFLFdBQVc7WUFDbEIsT0FBTyxFQUFFLFlBQVk7WUFDckIsUUFBUSxFQUFFLGVBQWUsQ0FBQyxJQUFJLENBQUM7WUFDL0IsVUFBVSxZQUFBO1NBQ1gsQ0FBQztLQUNILENBQUM7SUFFRixJQUFNLFVBQVUsR0FBRztRQUNqQixNQUFNLEVBQUUsY0FBTSxPQUFBLENBQ1o7WUFFSSxtQkFBbUIsQ0FBQztnQkFDbEIsSUFBSSxNQUFBO2dCQUNKLElBQUksTUFBQTtnQkFDSixVQUFVLFlBQUE7Z0JBQ1YsR0FBRyxFQUFFLFFBQVE7Z0JBQ2IsT0FBTyxFQUFFLFFBQVE7Z0JBQ2pCLEtBQUssRUFBRSxXQUFXO2dCQUNsQixPQUFPLEVBQUUsWUFBWTtnQkFDckIsUUFBUSxFQUFFLGVBQWUsQ0FBQyxJQUFJLENBQUM7YUFDaEMsQ0FBQztZQUdGLENBQUMsZUFBZTtnQkFDZCxDQUFDLENBQUMsQ0FDQSxvQkFBQyxlQUFlLElBQUMsTUFBTSxFQUFFLEdBQUcsSUFDekIsVUFBQyxFQUFhO3dCQUFYLHdCQUFTO29CQUFPLE9BQUEsb0JBQW9CLENBQUMsU0FBUyxDQUFDO2dCQUEvQixDQUErQixDQUNuQyxDQUNuQixDQUFDLENBQUMsQ0FBQyxvQkFBQyxPQUFPLElBQUMsS0FBSyxFQUFFLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxHQUFJLENBRXZDLENBQ1AsRUF2QmEsQ0F1QmI7UUFDRCxPQUFPLEVBQUUsY0FBTSxPQUFBLENBQ2Isb0JBQUMsVUFBVTtZQUNULG9CQUFDLFdBQVcsZUFBSyxnQkFBZ0IsRUFBSTtZQUVuQyxDQUFDLGVBQWU7Z0JBQ2QsQ0FBQyxDQUFDLENBQ0Esb0JBQUMsZUFBZSxJQUFDLE1BQU0sRUFBRSxHQUFHLElBQ3pCLFVBQUMsRUFBYTt3QkFBWCx3QkFBUztvQkFBTyxPQUFBLG9CQUFvQixDQUFDLFNBQVMsQ0FBQztnQkFBL0IsQ0FBK0IsQ0FDbkMsQ0FDbkIsQ0FBQyxDQUFDLENBQUMsaURBQXdCLENBQUEscUNBQXFDO1NBRTFELENBQ2QsRUFaYyxDQVlkO0tBQ0YsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLHlEQUNHLFVBQVUsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FDUixDQUMvQixDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// EXTERNAL MODULE: ./constants/application/magazine.ts
var application_magazine = __webpack_require__(170);

// CONCATENATED MODULE: ./container/app-shop/magazine/category/initialize.tsx

var initialize_DEFAULT_PROPS = {
    perPage: 12,
    magazineDefaultParams: { page: 1, perPage: 4, type: application_magazine["a" /* MAGAZINE_LIST_TYPE */].DEFAULT },
    magazineVideoParams: { page: 1, perPage: 5, type: application_magazine["a" /* MAGAZINE_LIST_TYPE */].VIDEO }
};
var initialize_INITIAL_STATE = {
    page: 1,
    urlList: [],
    isPriorityBlock: true,
    isFetchMagazineVideo: false
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBR2hGLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixPQUFPLEVBQUUsRUFBRTtJQUNYLHFCQUFxQixFQUFFLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxrQkFBa0IsQ0FBQyxPQUFPLEVBQUU7SUFDaEYsbUJBQW1CLEVBQUUsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLGtCQUFrQixDQUFDLEtBQUssRUFBRTtDQUNuRSxDQUFDO0FBRVosTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHO0lBQzNCLElBQUksRUFBRSxDQUFDO0lBQ1AsT0FBTyxFQUFFLEVBQUU7SUFDWCxlQUFlLEVBQUUsSUFBSTtJQUNyQixvQkFBb0IsRUFBRSxLQUFLO0NBQ2xCLENBQUMifQ==
// EXTERNAL MODULE: ./utils/uri.ts
var uri = __webpack_require__(347);

// CONCATENATED MODULE: ./container/app-shop/magazine/category/container.tsx
var container_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var container_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var container_MagazineCategoryContainer = /** @class */ (function (_super) {
    container_extends(MagazineCategoryContainer, _super);
    function MagazineCategoryContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.STR_TAG = 'tag';
        _this.getSlug = function (_a) {
            var location = _a.location, params = _a.params;
            return _this.isTagNameCategory({ location: location }) ? params.idTag : params.idCategory;
        };
        _this.isTagNameCategory = function (_a) {
            var location = _a.location;
            // TODO
            var arrPath = location.pathname.split('/');
            if (arrPath.length < 3) {
                return false;
            }
            var tagOrCategoryUrl = location.pathname.split('/')[2];
            return _this.STR_TAG === tagOrCategoryUrl;
        };
        _this.state = initialize_INITIAL_STATE;
        return _this;
    }
    MagazineCategoryContainer.prototype.initPagination = function (props) {
        if (props === void 0) { props = this.props; }
        var params = props.match.params, _a = props.magazineStore, magazineCategory = _a.magazineCategory, magazineTagName = _a.magazineTagName, perPage = props.perPage, location = props.location;
        var slug = this.getSlug({ location: location, params: params });
        var page = this.getPage();
        var param = { slug: slug, page: page, perPage: perPage };
        var keyHash = Object(encode["j" /* objectToHash */])(param);
        var url = this.isTagNameCategory({ location: location }) ? routing["Oa" /* ROUTING_MAGAZINE_TAG_PATH */] : routing["Ka" /* ROUTING_MAGAZINE_CATEGORY_PATH */];
        var data = this.isTagNameCategory({ location: location }) ? magazineTagName : magazineCategory;
        var total_pages = (data[keyHash]
            && data[keyHash].paging || 0).total_pages;
        var urlList = [];
        for (var i = 1; i <= total_pages; i++) {
            urlList.push({
                number: i,
                title: i,
                link: url + "/" + slug + "?page=" + i
            });
        }
        this.setState({ urlList: urlList });
    };
    MagazineCategoryContainer.prototype.getPage = function () {
        var page = Object(format["e" /* getUrlParameter */])(location.search, 'page') || 1;
        this.setState({ page: page });
        return page;
    };
    MagazineCategoryContainer.prototype.handleFetchMagazineByTagNameOrCategory = function (props) {
        if (props === void 0) { props = this.props; }
        var _a = props, perPage = _a.perPage, location = _a.location, fetchMagazineCategory = _a.fetchMagazineCategory, fetchMagazineByTagName = _a.fetchMagazineByTagName, params = _a.match.params, _b = _a.magazineStore, magazineCategory = _b.magazineCategory, magazineTagName = _b.magazineTagName;
        var page = this.state.page;
        var slug = this.getSlug({ location: location, params: params });
        var param = { slug: slug, page: page, perPage: perPage };
        var keyHash = Object(encode["j" /* objectToHash */])(param);
        this.isTagNameCategory({ location: location })
            ? Object(validate["l" /* isUndefined */])(magazineTagName[keyHash]) ? fetchMagazineByTagName(param) : this.initPagination(this.props)
            : Object(validate["l" /* isUndefined */])(magazineCategory[keyHash]) ? fetchMagazineCategory(param) : this.initPagination(this.props);
    };
    MagazineCategoryContainer.prototype.handleFetchMagazineVideo = function () {
        var _a = this.state, isPriorityBlock = _a.isPriorityBlock, isFetchMagazineVideo = _a.isFetchMagazineVideo;
        if (!!isPriorityBlock) {
            return;
        }
        if (!isFetchMagazineVideo) {
            var _b = this.props, fetchMagazineList = _b.fetchMagazineList, magazineVideoParams = _b.magazineVideoParams, magazineList = _b.magazineStore.magazineList;
            var keyHash = Object(encode["j" /* objectToHash */])(magazineVideoParams);
            this.setState({ isFetchMagazineVideo: true });
            Object(validate["l" /* isUndefined */])(magazineList[keyHash]) && fetchMagazineList(magazineVideoParams);
        }
    };
    MagazineCategoryContainer.prototype.updatePriorityBlock = function (props) {
        if (props === void 0) { props = this.props; }
        var _a = props, perPage = _a.perPage, location = _a.location, magazineDefaultParams = _a.magazineDefaultParams, params = _a.match.params, _b = _a.magazineStore, magazineDashboard = _b.magazineDashboard, magazineList = _b.magazineList, magazineCategory = _b.magazineCategory, magazineTagName = _b.magazineTagName;
        var _c = this.state, page = _c.page, isPriorityBlock = _c.isPriorityBlock;
        var slug = this.getSlug({ location: location, params: params });
        var param = { slug: slug, page: page, perPage: perPage };
        var keyHash = Object(encode["j" /* objectToHash */])(param);
        var isFullData = this.isTagNameCategory({ location: location })
            ? !Object(validate["l" /* isUndefined */])(magazineTagName[keyHash])
            : !Object(validate["l" /* isUndefined */])(magazineCategory[keyHash]);
        console.error('isPriorityBlock', isPriorityBlock, isFullData);
        isPriorityBlock
            && isFullData
            && !Object(validate["j" /* isEmptyObject */])(magazineDashboard)
            && !Object(validate["l" /* isUndefined */])(magazineList[Object(encode["j" /* objectToHash */])(magazineDefaultParams)])
            && this.setState({ isPriorityBlock: false });
    };
    MagazineCategoryContainer.prototype.componentDidMount = function () {
        var _a = this.props, fetchMagazineList = _a.fetchMagazineList, magazineDefaultParams = _a.magazineDefaultParams, fetchMagazineDashboard = _a.fetchMagazineDashboard, _b = _a.magazineStore, magazineDashboard = _b.magazineDashboard, magazineList = _b.magazineList;
        this.handleFetchMagazineByTagNameOrCategory(this.props);
        var keyHashMagazineDefault = Object(encode["j" /* objectToHash */])(magazineDefaultParams);
        Object(validate["l" /* isUndefined */])(magazineList[keyHashMagazineDefault]) && fetchMagazineList(magazineDefaultParams);
        Object(validate["j" /* isEmptyObject */])(magazineDashboard) && fetchMagazineDashboard();
        this.state.isPriorityBlock
            && this.updatePriorityBlock(this.props);
    };
    MagazineCategoryContainer.prototype.componentWillReceiveProps = function (nextProps) {
        var _this = this;
        var _a = this.props, perPage = _a.perPage, updateMetaInfoAction = _a.updateMetaInfoAction, _b = _a.match.params, idCategory = _b.idCategory, idTag = _b.idTag, _c = _a.magazineStore, magazineCategory = _c.magazineCategory, magazineTagName = _c.magazineTagName, isFetchMagazineListSuccess = _c.isFetchMagazineListSuccess;
        this.state.isPriorityBlock
            && this.updatePriorityBlock(nextProps);
        (idCategory !== nextProps.match.params.idCategory
            ||
                idTag !== nextProps.match.params.idTag)
            && this.setState({ page: 1 }, function () { return _this.handleFetchMagazineByTagNameOrCategory(nextProps); });
        // Check param paging change
        var page = Object(format["e" /* getUrlParameter */])(location.search, 'page') || 1;
        page !== this.state.page
            && this.setState({ page: page }, function () { return _this.handleFetchMagazineByTagNameOrCategory(nextProps); });
        this.initPagination(nextProps);
        // Set meta for SEO
        var slug = this.getSlug({ location: nextProps.location, params: nextProps.match.params });
        var nextPage = this.getPage();
        var param = { slug: slug, page: nextPage, perPage: perPage };
        var keyHash = Object(encode["j" /* objectToHash */])(param);
        var url;
        var data;
        var nextData;
        if (this.isTagNameCategory({ location: nextProps.location })) {
            data = magazineTagName;
            url = routing["Oa" /* ROUTING_MAGAZINE_TAG_PATH */];
            nextData = nextProps.magazineStore.magazineTagName;
        }
        else {
            data = magazineCategory;
            url = routing["Ka" /* ROUTING_MAGAZINE_CATEGORY_PATH */];
            nextData = nextProps.magazineStore.magazineCategory;
        }
        Object(validate["l" /* isUndefined */])(data[keyHash])
            && !Object(validate["l" /* isUndefined */])(nextData[keyHash])
            && updateMetaInfoAction({
                info: {
                    url: "https://www.lixibox.com" + url + "/" + slug,
                    type: "article",
                    title: slug.toUpperCase() + " | LixiBox Magazine",
                    description: 'LixiBox Magazine | Chia sẻ kiến thức mỹ phẩm, làm đẹp cùng LixiBox',
                    keyword: nextData[keyHash].title + ", lixibox, magazine, beauty blog, m\u1EF9 ph\u1EA9m, l\u00E0m \u0111\u1EB9p, chia s\u1EBB ki\u1EBFn th\u1EE9c l\u00E0m \u0111\u1EB9p",
                    image: uri["a" /* CDN_ASSETS_PREFIX */] + "/assets/images/meta/cover.jpeg"
                },
                structuredData: {
                    breadcrumbList: [
                        {
                            position: 2,
                            name: slug.toUpperCase() + " | LixiBox Magazine",
                            item: "https://www.lixibox.com" + url + "/" + slug
                        }
                    ]
                }
            });
    };
    MagazineCategoryContainer.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state,
            handleFetchMagazineVideo: this.handleFetchMagazineVideo.bind(this)
        };
        return category_view(args);
    };
    ;
    MagazineCategoryContainer.defaultProps = initialize_DEFAULT_PROPS;
    MagazineCategoryContainer = container_decorate([
        radium
    ], MagazineCategoryContainer);
    return MagazineCategoryContainer;
}(react["PureComponent"]));
/* harmony default export */ var container = (container_MagazineCategoryContainer);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3hFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUMzRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFeEQsT0FBTyxFQUFFLDhCQUE4QixFQUFFLHlCQUF5QixFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFFdEgsT0FBTyxVQUFVLE1BQU0sUUFBUSxDQUFDO0FBRWhDLE9BQU8sRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBRTVELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBRzFEO0lBQXdDLDZDQUE2QjtJQUluRSxtQ0FBWSxLQUFhO1FBQXpCLFlBQ0Usa0JBQU0sS0FBSyxDQUFDLFNBRWI7UUFOTyxhQUFPLEdBQUcsS0FBSyxDQUFDO1FBMkN4QixhQUFPLEdBQUcsVUFBQyxFQUFvQjtnQkFBbEIsc0JBQVEsRUFBRSxrQkFBTTtZQUMzQixNQUFNLENBQUMsS0FBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUUsUUFBUSxVQUFBLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDO1FBQ2pGLENBQUMsQ0FBQTtRQUVELHVCQUFpQixHQUFHLFVBQUMsRUFBWTtnQkFBVixzQkFBUTtZQUM3QixPQUFPO1lBQ1AsSUFBTSxPQUFPLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDN0MsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN2QixNQUFNLENBQUMsS0FBSyxDQUFDO1lBQ2YsQ0FBQztZQUVELElBQU0sZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekQsTUFBTSxDQUFDLEtBQUksQ0FBQyxPQUFPLEtBQUssZ0JBQWdCLENBQUM7UUFDM0MsQ0FBQyxDQUFBO1FBbkRDLEtBQUksQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDOztJQUM3QixDQUFDO0lBRUQsa0RBQWMsR0FBZCxVQUFlLEtBQWtCO1FBQWxCLHNCQUFBLEVBQUEsUUFBUSxJQUFJLENBQUMsS0FBSztRQUNkLElBQUEsMkJBQU0sRUFBSSx3QkFBb0QsRUFBbkMsc0NBQWdCLEVBQUUsb0NBQWUsRUFBSSx1QkFBTyxFQUFFLHlCQUFRLENBQVc7UUFFN0csSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLFFBQVEsVUFBQSxFQUFFLE1BQU0sUUFBQSxFQUFFLENBQUMsQ0FBQztRQUVoRCxJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDNUIsSUFBTSxLQUFLLEdBQUcsRUFBRSxJQUFJLE1BQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDO1FBQ3RDLElBQU0sT0FBTyxHQUFHLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwQyxJQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsRUFBRSxRQUFRLFVBQUEsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQyw4QkFBOEIsQ0FBQztRQUM5RyxJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsRUFBRSxRQUFRLFVBQUEsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUM7UUFFL0UsSUFBQTtxREFBVyxDQUVZO1FBRS9CLElBQU0sT0FBTyxHQUFlLEVBQUUsQ0FBQztRQUUvQixHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLFdBQVcsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ3RDLE9BQU8sQ0FBQyxJQUFJLENBQUM7Z0JBQ1gsTUFBTSxFQUFFLENBQUM7Z0JBQ1QsS0FBSyxFQUFFLENBQUM7Z0JBQ1IsSUFBSSxFQUFLLEdBQUcsU0FBSSxJQUFJLGNBQVMsQ0FBRzthQUNqQyxDQUFDLENBQUM7UUFDTCxDQUFDO1FBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBRUQsMkNBQU8sR0FBUDtRQUNFLElBQU0sSUFBSSxHQUFHLGVBQWUsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDO1FBRXhCLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBaUJELDBFQUFzQyxHQUF0QyxVQUF1QyxLQUFrQjtRQUFsQixzQkFBQSxFQUFBLFFBQVEsSUFBSSxDQUFDLEtBQUs7UUFDakQsSUFBQSxVQU9hLEVBTmpCLG9CQUFPLEVBQ1Asc0JBQVEsRUFDUixnREFBcUIsRUFDckIsa0RBQXNCLEVBQ2Isd0JBQU0sRUFDZixxQkFBb0QsRUFBbkMsc0NBQWdCLEVBQUUsb0NBQWUsQ0FDaEM7UUFFWixJQUFBLHNCQUFJLENBQTBCO1FBRXRDLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxRQUFRLFVBQUEsRUFBRSxNQUFNLFFBQUEsRUFBRSxDQUFDLENBQUM7UUFDaEQsSUFBTSxLQUFLLEdBQUcsRUFBRSxJQUFJLE1BQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDO1FBQ3RDLElBQU0sT0FBTyxHQUFHLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUVwQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsRUFBRSxRQUFRLFVBQUEsRUFBRSxDQUFDO1lBQ2xDLENBQUMsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7WUFDekcsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDOUcsQ0FBQztJQUVELDREQUF3QixHQUF4QjtRQUNRLElBQUEsZUFBZ0UsRUFBOUQsb0NBQWUsRUFBRSw4Q0FBb0IsQ0FBMEI7UUFFdkUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUM7UUFBQyxDQUFDO1FBRWxDLEVBQUUsQ0FBQyxDQUFDLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLElBQUEsZUFJa0IsRUFIdEIsd0NBQWlCLEVBQ2pCLDRDQUFtQixFQUNGLDRDQUFZLENBQ047WUFFekIsSUFBTSxPQUFPLEdBQUcsWUFBWSxDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDbEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLG9CQUFvQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7WUFDOUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLGlCQUFpQixDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDL0UsQ0FBQztJQUNILENBQUM7SUFFRCx1REFBbUIsR0FBbkIsVUFBb0IsS0FBa0I7UUFBbEIsc0JBQUEsRUFBQSxRQUFRLElBQUksQ0FBQyxLQUFLO1FBQzlCLElBQUEsVUFNYSxFQUxqQixvQkFBTyxFQUNQLHNCQUFRLEVBQ1IsZ0RBQXFCLEVBQ1osd0JBQU0sRUFDZixxQkFBcUYsRUFBcEUsd0NBQWlCLEVBQUUsOEJBQVksRUFBRSxzQ0FBZ0IsRUFBRSxvQ0FBZSxDQUNqRTtRQUVkLElBQUEsZUFBZ0QsRUFBOUMsY0FBSSxFQUFFLG9DQUFlLENBQTBCO1FBRXZELElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxRQUFRLFVBQUEsRUFBRSxNQUFNLFFBQUEsRUFBRSxDQUFDLENBQUM7UUFDaEQsSUFBTSxLQUFLLEdBQUcsRUFBRSxJQUFJLE1BQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDO1FBQ3RDLElBQU0sT0FBTyxHQUFHLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUVwQyxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsRUFBRSxRQUFRLFVBQUEsRUFBRSxDQUFDO1lBQ3JELENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDeEMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7UUFFNUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsRUFBRSxlQUFlLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFFOUQsZUFBZTtlQUNWLFVBQVU7ZUFDVixDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQztlQUNqQyxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztlQUMvRCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELHFEQUFpQixHQUFqQjtRQUNRLElBQUEsZUFLa0IsRUFKdEIsd0NBQWlCLEVBQ2pCLGdEQUFxQixFQUNyQixrREFBc0IsRUFDdEIscUJBQWtELEVBQWpDLHdDQUFpQixFQUFFLDhCQUFZLENBQ3pCO1FBRXpCLElBQUksQ0FBQyxzQ0FBc0MsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFeEQsSUFBTSxzQkFBc0IsR0FBRyxZQUFZLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUNuRSxXQUFXLENBQUMsWUFBWSxDQUFDLHNCQUFzQixDQUFDLENBQUMsSUFBSSxpQkFBaUIsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBRTlGLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLHNCQUFzQixFQUFFLENBQUM7UUFFN0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlO2VBQ3JCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVELDZEQUF5QixHQUF6QixVQUEwQixTQUFTO1FBQW5DLGlCQWtFQztRQWpFTyxJQUFBLGVBS1EsRUFKWixvQkFBTyxFQUNQLDhDQUFvQixFQUNYLG9CQUE2QixFQUFuQiwwQkFBVSxFQUFFLGdCQUFLLEVBQ3BDLHFCQUFnRixFQUEvRCxzQ0FBZ0IsRUFBRSxvQ0FBZSxFQUFFLDBEQUEwQixDQUNqRTtRQUVmLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZTtlQUNyQixJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFekMsQ0FDRSxVQUFVLEtBQUssU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVTs7Z0JBRWhELEtBQUssS0FBSyxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQ3ZDO2VBQ0ksSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsRUFBRSxjQUFNLE9BQUEsS0FBSSxDQUFDLHNDQUFzQyxDQUFDLFNBQVMsQ0FBQyxFQUF0RCxDQUFzRCxDQUFDLENBQUM7UUFFOUYsNEJBQTRCO1FBQzVCLElBQU0sSUFBSSxHQUFHLGVBQWUsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzRCxJQUFJLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJO2VBQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxFQUFFLGNBQU0sT0FBQSxLQUFJLENBQUMsc0NBQXNDLENBQUMsU0FBUyxDQUFDLEVBQXRELENBQXNELENBQUMsQ0FBQztRQUUzRixJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRS9CLG1CQUFtQjtRQUNuQixJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsUUFBUSxFQUFFLFNBQVMsQ0FBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLFNBQVMsQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztRQUU1RixJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDaEMsSUFBTSxLQUFLLEdBQUcsRUFBRSxJQUFJLE1BQUEsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUM7UUFDaEQsSUFBTSxPQUFPLEdBQUcsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXBDLElBQUksR0FBRyxDQUFDO1FBQ1IsSUFBSSxJQUFJLENBQUM7UUFDVCxJQUFJLFFBQVEsQ0FBQztRQUNiLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLFFBQVEsRUFBRSxTQUFTLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0QsSUFBSSxHQUFHLGVBQWUsQ0FBQztZQUN2QixHQUFHLEdBQUcseUJBQXlCLENBQUM7WUFDaEMsUUFBUSxHQUFHLFNBQVMsQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDO1FBQ3JELENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLElBQUksR0FBRyxnQkFBZ0IsQ0FBQztZQUN4QixHQUFHLEdBQUcsOEJBQThCLENBQUM7WUFDckMsUUFBUSxHQUFHLFNBQVMsQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUM7UUFDdEQsQ0FBQztRQUVELFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7ZUFDckIsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2VBQy9CLG9CQUFvQixDQUFDO2dCQUN0QixJQUFJLEVBQUU7b0JBQ0osR0FBRyxFQUFFLDRCQUEwQixHQUFHLFNBQUksSUFBTTtvQkFDNUMsSUFBSSxFQUFFLFNBQVM7b0JBQ2YsS0FBSyxFQUFLLElBQUksQ0FBQyxXQUFXLEVBQUUsd0JBQXFCO29CQUNqRCxXQUFXLEVBQUUsb0VBQW9FO29CQUNqRixPQUFPLEVBQUssUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUsseUlBQStFO29CQUNsSCxLQUFLLEVBQUUsaUJBQWlCLEdBQUcsZ0NBQWdDO2lCQUM1RDtnQkFDRCxjQUFjLEVBQUU7b0JBQ2QsY0FBYyxFQUFFO3dCQUNkOzRCQUNFLFFBQVEsRUFBRSxDQUFDOzRCQUNYLElBQUksRUFBSyxJQUFJLENBQUMsV0FBVyxFQUFFLHdCQUFxQjs0QkFDaEQsSUFBSSxFQUFFLDRCQUEwQixHQUFHLFNBQUksSUFBTTt5QkFDOUM7cUJBQ0Y7aUJBQ0Y7YUFDRixDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsMENBQU0sR0FBTjtRQUNFLElBQU0sSUFBSSxHQUFHO1lBQ1gsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQix3QkFBd0IsRUFBRSxJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztTQUNuRSxDQUFDO1FBRUYsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBQUEsQ0FBQztJQTNOSyxzQ0FBWSxHQUFXLGFBQWEsQ0FBQztJQUZ4Qyx5QkFBeUI7UUFEOUIsTUFBTTtPQUNELHlCQUF5QixDQThOOUI7SUFBRCxnQ0FBQztDQUFBLEFBOU5ELENBQXdDLGFBQWEsR0E4TnBEO0FBRUQsZUFBZSx5QkFBeUIsQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/magazine/category/store.tsx
var connect = __webpack_require__(129).connect;



var mapStateToProps = function (state) { return ({
    magazineStore: state.magazine
}); };
var mapDispatchToProps = function (dispatch) { return ({
    fetchMagazineCategory: function (data) { return dispatch(Object(magazine["c" /* fetchMagazineCategoryAction */])(data)); },
    fetchMagazineList: function (data) { return dispatch(Object(magazine["e" /* fetchMagazineListAction */])(data)); },
    fetchMagazineDashboard: function () { return dispatch(Object(magazine["d" /* fetchMagazineDashboardAction */])()); },
    fetchMagazineByTagName: function (data) { return dispatch(Object(magazine["b" /* fetchMagazineByTagNameAction */])(data)); },
    updateMetaInfoAction: function (data) { return dispatch(Object(meta["a" /* updateMetaInfoAction */])(data)); }
}); };
/* harmony default export */ var store = __webpack_exports__["default"] = (connect(mapStateToProps, mapDispatchToProps)(container));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUMvRCxPQUFPLEVBQ0wsNEJBQTRCLEVBQzVCLDJCQUEyQixFQUMzQix1QkFBdUIsRUFDdkIsNEJBQTRCLEVBQzdCLE1BQU0sNkJBQTZCLENBQUM7QUFFckMsT0FBTyx5QkFBeUIsTUFBTSxhQUFhLENBQUM7QUFFcEQsSUFBTSxlQUFlLEdBQUcsVUFBQSxLQUFLLElBQUksT0FBQSxDQUFDO0lBQ2hDLGFBQWEsRUFBRSxLQUFLLENBQUMsUUFBUTtDQUM5QixDQUFDLEVBRitCLENBRS9CLENBQUM7QUFFSCxJQUFNLGtCQUFrQixHQUFHLFVBQUEsUUFBUSxJQUFJLE9BQUEsQ0FBQztJQUN0QyxxQkFBcUIsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUEzQyxDQUEyQztJQUNqRixpQkFBaUIsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUF2QyxDQUF1QztJQUN6RSxzQkFBc0IsRUFBRSxjQUFNLE9BQUEsUUFBUSxDQUFDLDRCQUE0QixFQUFFLENBQUMsRUFBeEMsQ0FBd0M7SUFDdEUsc0JBQXNCLEVBQUUsVUFBQyxJQUFTLElBQUssT0FBQSxRQUFRLENBQUMsNEJBQTRCLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBNUMsQ0FBNEM7SUFDbkYsb0JBQW9CLEVBQUUsVUFBQyxJQUFJLElBQUssT0FBQSxRQUFRLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBcEMsQ0FBb0M7Q0FDckUsQ0FBQyxFQU5xQyxDQU1yQyxDQUFDO0FBRUgsZUFBZSxPQUFPLENBQ3BCLGVBQWUsRUFDZixrQkFBa0IsQ0FDbkIsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDIn0=

/***/ })

}]);