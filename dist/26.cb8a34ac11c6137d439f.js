(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[26,6,21,100],{

/***/ 754:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return updateMetaInfoAction; });
/* harmony import */ var _constants_api_meta__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(352);

var updateMetaInfoAction = function (data) { return ({
    type: _constants_api_meta__WEBPACK_IMPORTED_MODULE_0__[/* UPDATE_META_INFO */ "a"],
    payload: data
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWV0YS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1ldGEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLGdCQUFnQixFQUNqQixNQUFNLHVCQUF1QixDQUFDO0FBRS9CLE1BQU0sQ0FBQyxJQUFNLG9CQUFvQixHQUFHLFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FBQztJQUM3QyxJQUFJLEVBQUUsZ0JBQWdCO0lBQ3RCLE9BQU8sRUFBRSxJQUFJO0NBQ2QsQ0FBQyxFQUg0QyxDQUc1QyxDQUFDIn0=

/***/ }),

/***/ 755:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// CONCATENATED MODULE: ./container/layout/wrap/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};


;
var renderView = function (_a) {
    var children = _a.children, style = _a.style, type = _a.type;
    var containerProps = {
        key: 'wrap-layout-container',
        style: [layout["c" /* wrapLayout */], style, 'larger' === type && layout["c" /* wrapLayout */].larger]
    };
    return (react["createElement"]("div", __assign({}, containerProps), children));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSx1QkFBdUIsQ0FBQztBQU0vQyxDQUFDO0FBRUYsSUFBTSxVQUFVLEdBQUcsVUFBQyxFQUFzQztRQUFwQyxzQkFBUSxFQUFFLGdCQUFLLEVBQUUsY0FBSTtJQUN6QyxJQUFNLGNBQWMsR0FBRztRQUNyQixHQUFHLEVBQUUsdUJBQXVCO1FBQzVCLEtBQUssRUFBRSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsS0FBSyxFQUFFLFFBQVEsS0FBSyxJQUFJLElBQUksTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUM7S0FDakYsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLHdDQUFTLGNBQWMsR0FDcEIsUUFBUSxDQUNMLENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQTtBQUVELGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./container/layout/wrap/container.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var container_WrapLayout = /** @class */ (function (_super) {
    __extends(WrapLayout, _super);
    function WrapLayout(props) {
        return _super.call(this, props) || this;
    }
    WrapLayout.prototype.render = function () {
        var _a = this.props, children = _a.children, style = _a.style, type = _a.type;
        return view({ children: children, style: style, type: type });
    };
    ;
    WrapLayout.defaultProps = { type: 'normal' };
    WrapLayout = __decorate([
        radium
    ], WrapLayout);
    return WrapLayout;
}(react["PureComponent"]));
/* harmony default export */ var container = __webpack_exports__["default"] = (container_WrapLayout);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFJaEM7SUFBeUIsOEJBQTZCO0lBR3BELG9CQUFZLEtBQWE7ZUFDdkIsa0JBQU0sS0FBSyxDQUFDO0lBQ2QsQ0FBQztJQUVELDJCQUFNLEdBQU47UUFDUSxJQUFBLGVBQXNDLEVBQXBDLHNCQUFRLEVBQUUsZ0JBQUssRUFBRSxjQUFJLENBQWdCO1FBQzdDLE1BQU0sQ0FBQyxVQUFVLENBQUMsRUFBRSxRQUFRLFVBQUEsRUFBRSxLQUFLLE9BQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUFBLENBQUM7SUFUSyx1QkFBWSxHQUFXLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxDQUFDO0lBRDdDLFVBQVU7UUFEZixNQUFNO09BQ0QsVUFBVSxDQVdmO0lBQUQsaUJBQUM7Q0FBQSxBQVhELENBQXlCLGFBQWEsR0FXckM7QUFFRCxlQUFlLFVBQVUsQ0FBQyJ9

/***/ }),

/***/ 757:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return renderHtmlContent; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var renderHtmlContent = function (message_html, style) {
    if (style === void 0) { style = {}; }
    return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { style: style, dangerouslySetInnerHTML: { __html: message_html } }));
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHRtbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImh0bWwudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRS9CLE1BQU0sQ0FBQyxJQUFNLGlCQUFpQixHQUFHLFVBQUMsWUFBb0IsRUFBRSxLQUFVO0lBQVYsc0JBQUEsRUFBQSxVQUFVO0lBQUssT0FBQSxDQUNyRSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxFQUFFLHVCQUF1QixFQUFFLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxHQUFJLENBQ3pFO0FBRnNFLENBRXRFLENBQUMifQ==

/***/ }),

/***/ 761:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// CONCATENATED MODULE: ./api/magazine.ts

;
var fetchMagazineList = function (_a) {
    var _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 10 : _c, _d = _a.type, type = _d === void 0 ? 'default' : _d;
    var query = "?page=" + page + "&per_page=" + perPage + "&filter[post_type]=" + type;
    return Object(restful_method["b" /* get */])({
        path: "/magazines" + query,
        description: 'Get magazine list',
        errorMesssage: "Can't get magazine list. Please try again",
    });
};
var fetchMagazineDashboard = function () { return Object(restful_method["b" /* get */])({
    path: "/magazines/dashboard",
    description: 'Get magazine dashboard list',
    errorMesssage: "Can't get magazine dashboard list. Please try again",
}); };
/**
 * Fetch magazine category by slug (id)
 */
var fetchMagazineCategory = function (_a) {
    var slug = _a.slug, page = _a.page, perPage = _a.perPage;
    var query = slug + "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/category/" + query,
        description: 'Get magazine category by slug (id)',
        errorMesssage: "Can't get magazine category by slug (id). Please try again",
    });
};
/**
* Fetch magazine by slug (id)
*/
var fetchMagazineBySlug = function (_a) {
    var slug = _a.slug;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/" + slug,
        description: 'Get magazine by slug (id)',
        errorMesssage: "Can't get magazine by slug (id). Please try again",
    });
};
/**
* Fetch magazine related blogs by slug (id)
*/
var fetchMagazineRelatedBlog = function (_a) {
    var slug = _a.slug;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/" + slug + "/related_blogs",
        description: 'Get magazine related blogs by slug (id)',
        errorMesssage: "Can't get magazine related blogs by slug (id). Please try again",
    });
};
/**
* Fetch magazine related boxes by slug (id)
*/
var fetchMagazineRelatedBox = function (_a) {
    var slug = _a.slug, limit = _a.limit;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/" + slug + "/related_boxes?limit=" + limit,
        description: 'Get magazine related boxes by slug (id)',
        errorMesssage: "Can't get magazine related boxes by slug (id). Please try again",
    });
};
/**
* Fetch magazine by tag name
*/
var fetchMagazineByTagName = function (_a) {
    var slug = _a.slug, page = _a.page, perPage = _a.perPage;
    var query = slug + "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/tag/" + query,
        description: 'Get magazine by tag name',
        errorMesssage: "Can't get magazine by tag name. Please try again",
    });
};
/**
* Fetch search magazine by keyword
*/
var fetchMagazineByKeyword = function (_a) {
    var keyword = _a.keyword, page = _a.page, perPage = _a.perPage;
    var query = keyword + "?page=" + page + "&per_page=" + perPage;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/search/" + query,
        description: 'Get search magazine by keyword',
        errorMesssage: "Can't get search magazine by keyword. Please try again",
    });
};
/**
* Fetch search suggestion magazine by keyword
*/
var fetchMagazineSuggestionByKeyword = function (_a) {
    var keyword = _a.keyword, limit = _a.limit;
    var query = keyword + "?limit=" + limit;
    return Object(restful_method["b" /* get */])({
        path: "/magazines/search_suggestion/" + query,
        description: 'Get search suggestion magazine by keyword',
        errorMesssage: "Can't get search suggestion magazine by keyword. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFnYXppbmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtYWdhemluZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFNOUMsQ0FBQztBQUVGLE1BQU0sQ0FBQyxJQUFNLGlCQUFpQixHQUM1QixVQUFDLEVBQXFFO1FBQW5FLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWSxFQUFFLFlBQWdCLEVBQWhCLHFDQUFnQjtJQUN6QyxJQUFNLEtBQUssR0FBRyxXQUFTLElBQUksa0JBQWEsT0FBTywyQkFBc0IsSUFBTSxDQUFDO0lBRTVFLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsZUFBYSxLQUFPO1FBQzFCLFdBQVcsRUFBRSxtQkFBbUI7UUFDaEMsYUFBYSxFQUFFLDJDQUEyQztLQUMzRCxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSixNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FBRyxjQUFNLE9BQUEsR0FBRyxDQUFDO0lBQzlDLElBQUksRUFBRSxzQkFBc0I7SUFDNUIsV0FBVyxFQUFFLDZCQUE2QjtJQUMxQyxhQUFhLEVBQUUscURBQXFEO0NBQ3JFLENBQUMsRUFKMEMsQ0FJMUMsQ0FBQztBQUVIOztHQUVHO0FBQ0gsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQ2hDLFVBQUMsRUFBdUI7UUFBckIsY0FBSSxFQUFFLGNBQUksRUFBRSxvQkFBTztJQUNwQixJQUFNLEtBQUssR0FBTSxJQUFJLGNBQVMsSUFBSSxrQkFBYSxPQUFTLENBQUM7SUFDekQsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSx5QkFBdUIsS0FBTztRQUNwQyxXQUFXLEVBQUUsb0NBQW9DO1FBQ2pELGFBQWEsRUFBRSw0REFBNEQ7S0FDNUUsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBRUo7O0VBRUU7QUFDRixNQUFNLENBQUMsSUFBTSxtQkFBbUIsR0FBRyxVQUFDLEVBQVE7UUFBTixjQUFJO0lBQU8sT0FBQSxHQUFHLENBQUM7UUFDbkQsSUFBSSxFQUFFLGdCQUFjLElBQU07UUFDMUIsV0FBVyxFQUFFLDJCQUEyQjtRQUN4QyxhQUFhLEVBQUUsbURBQW1EO0tBQ25FLENBQUM7QUFKK0MsQ0FJL0MsQ0FBQztBQUVIOztFQUVFO0FBQ0YsTUFBTSxDQUFDLElBQU0sd0JBQXdCLEdBQUcsVUFBQyxFQUFRO1FBQU4sY0FBSTtJQUFPLE9BQUEsR0FBRyxDQUFDO1FBQ3hELElBQUksRUFBRSxnQkFBYyxJQUFJLG1CQUFnQjtRQUN4QyxXQUFXLEVBQUUseUNBQXlDO1FBQ3RELGFBQWEsRUFBRSxpRUFBaUU7S0FDakYsQ0FBQztBQUpvRCxDQUlwRCxDQUFDO0FBRUg7O0VBRUU7QUFDRixNQUFNLENBQUMsSUFBTSx1QkFBdUIsR0FBRyxVQUFDLEVBQWU7UUFBYixjQUFJLEVBQUUsZ0JBQUs7SUFBTyxPQUFBLEdBQUcsQ0FBQztRQUM5RCxJQUFJLEVBQUUsZ0JBQWMsSUFBSSw2QkFBd0IsS0FBTztRQUN2RCxXQUFXLEVBQUUseUNBQXlDO1FBQ3RELGFBQWEsRUFBRSxpRUFBaUU7S0FDakYsQ0FBQztBQUowRCxDQUkxRCxDQUFDO0FBRUg7O0VBRUU7QUFDRixNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FDakMsVUFBQyxFQUF1QjtRQUFyQixjQUFJLEVBQUUsY0FBSSxFQUFFLG9CQUFPO0lBQ3BCLElBQU0sS0FBSyxHQUFNLElBQUksY0FBUyxJQUFJLGtCQUFhLE9BQVMsQ0FBQztJQUN6RCxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLG9CQUFrQixLQUFPO1FBQy9CLFdBQVcsRUFBRSwwQkFBMEI7UUFDdkMsYUFBYSxFQUFFLGtEQUFrRDtLQUNsRSxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSjs7RUFFRTtBQUNGLE1BQU0sQ0FBQyxJQUFNLHNCQUFzQixHQUNqQyxVQUFDLEVBQTBCO1FBQXhCLG9CQUFPLEVBQUUsY0FBSSxFQUFFLG9CQUFPO0lBQ3ZCLElBQU0sS0FBSyxHQUFNLE9BQU8sY0FBUyxJQUFJLGtCQUFhLE9BQVMsQ0FBQztJQUM1RCxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ1QsSUFBSSxFQUFFLHVCQUFxQixLQUFPO1FBQ2xDLFdBQVcsRUFBRSxnQ0FBZ0M7UUFDN0MsYUFBYSxFQUFFLHdEQUF3RDtLQUN4RSxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFFSjs7RUFFRTtBQUNGLE1BQU0sQ0FBQyxJQUFNLGdDQUFnQyxHQUMzQyxVQUFDLEVBQWtCO1FBQWhCLG9CQUFPLEVBQUUsZ0JBQUs7SUFDZixJQUFNLEtBQUssR0FBTSxPQUFPLGVBQVUsS0FBTyxDQUFDO0lBQzFDLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsa0NBQWdDLEtBQU87UUFDN0MsV0FBVyxFQUFFLDJDQUEyQztRQUN4RCxhQUFhLEVBQUUsbUVBQW1FO0tBQ25GLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQyJ9
// EXTERNAL MODULE: ./constants/api/magazine.ts
var magazine = __webpack_require__(22);

// CONCATENATED MODULE: ./action/magazine.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return fetchMagazineListAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return fetchMagazineDashboardAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return fetchMagazineCategoryAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fetchMagazineBySlugAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return fetchMagazineRelatedBlogAction; });
/* unused harmony export fetchMagazineRelatedBoxAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fetchMagazineByTagNameAction; });
/* unused harmony export showHideMagazineSearchAction */
/* unused harmony export fetchMagazineByKeywordAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return fetchMagazineSuggestionByKeywordAction; });


/**
 * Fetch love list by filter paramsx
 *
 * @param {number} page ex: 1, 2, 3, 4
 * @param {number} perPage ex: 10, 15, 20
 * @param {'default' | 'video-post' | 'quote-post'} type
 */
var fetchMagazineListAction = function (_a) {
    var page = _a.page, perPage = _a.perPage, type = _a.type;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["f" /* FETCH_MAGAZINE_LIST */],
            payload: { promise: fetchMagazineList({ page: page, perPage: perPage, type: type }).then(function (res) { return res; }) },
            meta: { page: page, perPage: perPage, type: type }
        });
    };
};
var fetchMagazineDashboardAction = function () {
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["e" /* FETCH_MAGAZINE_DASHBOARD */],
            payload: { promise: fetchMagazineDashboard().then(function (res) { return res; }) },
            meta: {}
        });
    };
};
/**
* Fetch magazine category by slug (id)
*
* @param {string} slug ex: makeup
* @param {number} page ex: 1, 2, 3, 4
* @param {number} perPage ex: 10, 15, 20
*/
var fetchMagazineCategoryAction = function (_a) {
    var slug = _a.slug, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["d" /* FETCH_MAGAZINE_CATEGORY */],
            payload: { promise: fetchMagazineCategory({ slug: slug, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { slug: slug, page: page, perPage: perPage }
        });
    };
};
/**
* Fetch magazine by slug (id)
*
* @param {string} slug ex: makeup
*/
var fetchMagazineBySlugAction = function (_a) {
    var slug = _a.slug;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["c" /* FETCH_MAGAZINE_BY_SLUG */],
            payload: { promise: fetchMagazineBySlug({ slug: slug }).then(function (res) { return res; }) },
            meta: { slug: slug }
        });
    };
};
/**
* Fetch magazine related blogs by slug (id)
*
* @param {string} slug ex: makeup
*/
var fetchMagazineRelatedBlogAction = function (_a) {
    var slug = _a.slug;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["g" /* FETCH_MAGAZINE_RELATED_BLOGS */],
            payload: { promise: fetchMagazineRelatedBlog({ slug: slug }).then(function (res) { return res; }) },
            meta: { slug: slug }
        });
    };
};
/**
* Fetch magazine related boxes by slug (id)
*
* @param {string} slug ex: makeup
* @param {number} limit ex: 1, 2, 3, 4
*/
var fetchMagazineRelatedBoxAction = function (_a) {
    var slug = _a.slug, _b = _a.limit, limit = _b === void 0 ? 6 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["h" /* FETCH_MAGAZINE_RELATED_BOXES */],
            payload: { promise: fetchMagazineRelatedBox({ slug: slug, limit: limit }).then(function (res) { return res; }) },
            meta: { slug: slug }
        });
    };
};
/**
* Fetch magazine by tag name
*
* @param {string} tagName ex: halio
* @param {number} page ex: 1, 2, 3, 4
* @param {number} perPage ex: 10, 15, 20
*/
var fetchMagazineByTagNameAction = function (_a) {
    var slug = _a.slug, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["j" /* FETCH_MAGAZINE_TAG */],
            payload: { promise: fetchMagazineByTagName({ slug: slug, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { slug: slug, page: page, perPage: perPage }
        });
    };
};
/**
* Show / Hide search suggest with state param
*
* @param {boolean} state
*/
var showHideMagazineSearchAction = function (state) {
    if (state === void 0) { state = false; }
    return ({
        type: magazine["a" /* DISPLAY_MAGAZINE_SEARCH_SUGGESTION */],
        payload: state
    });
};
/**
* Fetch magazine by keyword
*
* @param {string} keyword ex: halio
* @param {number} page ex: 1, 2, 3, 4
* @param {number} perPage ex: 10, 15, 20
*/
var fetchMagazineByKeywordAction = function (_a) {
    var keyword = _a.keyword, _b = _a.page, page = _b === void 0 ? 1 : _b, _c = _a.perPage, perPage = _c === void 0 ? 12 : _c;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["b" /* FETCH_MAGAZINE_BY_KEWORD */],
            payload: { promise: fetchMagazineByKeyword({ keyword: keyword, page: page, perPage: perPage }).then(function (res) { return res; }) },
            meta: { keyword: keyword }
        });
    };
};
/**
* Fetch magazine suggestion by keyword
*
* @param {string} keyword ex: halio
* @param {number} limit ex: 1, 2, 3, 4
*/
var fetchMagazineSuggestionByKeywordAction = function (_a) {
    var keyword = _a.keyword, _b = _a.limit, limit = _b === void 0 ? 10 : _b;
    return function (dispatch, getState) {
        return dispatch({
            type: magazine["i" /* FETCH_MAGAZINE_SUGGESTION_BY_KEWORD */],
            payload: { promise: fetchMagazineSuggestionByKeyword({ keyword: keyword, limit: limit }).then(function (res) { return res; }) },
            meta: { keyword: keyword }
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFnYXppbmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtYWdhemluZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBRUwsaUJBQWlCLEVBQ2pCLHNCQUFzQixFQUN0QixxQkFBcUIsRUFDckIsbUJBQW1CLEVBQ25CLHdCQUF3QixFQUN4Qix1QkFBdUIsRUFDdkIsc0JBQXNCLEVBQ3RCLHNCQUFzQixFQUN0QixnQ0FBZ0MsRUFDakMsTUFBTSxpQkFBaUIsQ0FBQztBQUV6QixPQUFPLEVBQ0wsbUJBQW1CLEVBQ25CLHdCQUF3QixFQUN4Qix1QkFBdUIsRUFDdkIsc0JBQXNCLEVBQ3RCLDRCQUE0QixFQUM1Qiw0QkFBNEIsRUFDNUIsa0JBQWtCLEVBQ2xCLGtDQUFrQyxFQUNsQyx3QkFBd0IsRUFDeEIsbUNBQW1DLEVBQ3BDLE1BQU0sMkJBQTJCLENBQUM7QUFFbkM7Ozs7OztHQU1HO0FBQ0gsTUFBTSxDQUFDLElBQU0sdUJBQXVCLEdBQ2xDLFVBQUMsRUFBZ0Q7UUFBOUMsY0FBSSxFQUFFLG9CQUFPLEVBQUUsY0FBSTtJQUNwQixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsbUJBQW1CO1lBQ3pCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDakYsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUU7U0FDOUIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVCxNQUFNLENBQUMsSUFBTSw0QkFBNEIsR0FDdkM7SUFDRSxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsd0JBQXdCO1lBQzlCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUMvRCxJQUFJLEVBQUUsRUFBRTtTQUNULENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7OztFQU1FO0FBQ0YsTUFBTSxDQUFDLElBQU0sMkJBQTJCLEdBQ3RDLFVBQUMsRUFBZ0M7UUFBOUIsY0FBSSxFQUFFLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUM3QixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsdUJBQXVCO1lBQzdCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxxQkFBcUIsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDckYsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDOUIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0seUJBQXlCLEdBQ3BDLFVBQUMsRUFBUTtRQUFOLGNBQUk7SUFDTCxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsc0JBQXNCO1lBQzVCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDcEUsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUU7U0FDZixDQUFDO0lBSkYsQ0FJRTtBQUxKLENBS0ksQ0FBQztBQUdUOzs7O0VBSUU7QUFDRixNQUFNLENBQUMsSUFBTSw4QkFBOEIsR0FDekMsVUFBQyxFQUFRO1FBQU4sY0FBSTtJQUNMLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSw0QkFBNEI7WUFDbEMsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLHdCQUF3QixDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUN6RSxJQUFJLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRTtTQUNmLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7O0VBS0U7QUFDRixNQUFNLENBQUMsSUFBTSw2QkFBNkIsR0FDeEMsVUFBQyxFQUFtQjtRQUFqQixjQUFJLEVBQUUsYUFBUyxFQUFULDhCQUFTO0lBQ2hCLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSw0QkFBNEI7WUFDbEMsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLHVCQUF1QixDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtZQUMvRSxJQUFJLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRTtTQUNmLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBR1Q7Ozs7OztFQU1FO0FBQ0YsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsRUFBZ0M7UUFBOUIsY0FBSSxFQUFFLFlBQVEsRUFBUiw2QkFBUSxFQUFFLGVBQVksRUFBWixpQ0FBWTtJQUM3QixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsa0JBQWtCO1lBQ3hCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxzQkFBc0IsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDdEYsSUFBSSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDOUIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUM7QUFFVDs7OztFQUlFO0FBQ0YsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsS0FBYTtJQUFiLHNCQUFBLEVBQUEsYUFBYTtJQUFLLE9BQUEsQ0FBQztRQUNsQixJQUFJLEVBQUUsa0NBQWtDO1FBQ3hDLE9BQU8sRUFBRSxLQUFLO0tBQ2YsQ0FBQztBQUhpQixDQUdqQixDQUFDO0FBRUw7Ozs7OztFQU1FO0FBQ0YsTUFBTSxDQUFDLElBQU0sNEJBQTRCLEdBQ3ZDLFVBQUMsRUFBbUM7UUFBakMsb0JBQU8sRUFBRSxZQUFRLEVBQVIsNkJBQVEsRUFBRSxlQUFZLEVBQVosaUNBQVk7SUFDaEMsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLHdCQUF3QjtZQUM5QixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsc0JBQXNCLENBQUMsRUFBRSxPQUFPLFNBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQ3pGLElBQUksRUFBRSxFQUFFLE9BQU8sU0FBQSxFQUFFO1NBQ2xCLENBQUM7SUFKRixDQUlFO0FBTEosQ0FLSSxDQUFDO0FBRVQ7Ozs7O0VBS0U7QUFDRixNQUFNLENBQUMsSUFBTSxzQ0FBc0MsR0FDakQsVUFBQyxFQUF1QjtRQUFyQixvQkFBTyxFQUFFLGFBQVUsRUFBViwrQkFBVTtJQUNwQixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUsbUNBQW1DO1lBQ3pDLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxnQ0FBZ0MsQ0FBQyxFQUFFLE9BQU8sU0FBQSxFQUFFLEtBQUssT0FBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDM0YsSUFBSSxFQUFFLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDbEIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUMifQ==

/***/ }),

/***/ 765:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/image.ts
var utils_image = __webpack_require__(348);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./components/magazine/image-slider/initialize.tsx
var DEFAULT_PROPS = {
    data: [],
    type: 'full',
    title: '',
    column: 3,
    showViewMore: false,
    showHeader: true,
    isCustomTitle: false,
    style: {},
    titleStyle: {}
};
var INITIAL_STATE = function (data) { return ({
    magazineList: data || [],
    magazineSlide: [],
    magazineSlideSelected: {},
    countChangeSlide: 0,
    firstInit: false
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLElBQUksRUFBRSxNQUFNO0lBQ1osS0FBSyxFQUFFLEVBQUU7SUFDVCxNQUFNLEVBQUUsQ0FBQztJQUNULFlBQVksRUFBRSxLQUFLO0lBQ25CLFVBQVUsRUFBRSxJQUFJO0lBQ2hCLGFBQWEsRUFBRSxLQUFLO0lBQ3BCLEtBQUssRUFBRSxFQUFFO0lBQ1QsVUFBVSxFQUFFLEVBQUU7Q0FDTCxDQUFDO0FBRVosTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHLFVBQUMsSUFBUyxJQUFLLE9BQUEsQ0FBQztJQUMzQyxZQUFZLEVBQUUsSUFBSSxJQUFJLEVBQUU7SUFDeEIsYUFBYSxFQUFFLEVBQUU7SUFDakIscUJBQXFCLEVBQUUsRUFBRTtJQUN6QixnQkFBZ0IsRUFBRSxDQUFDO0lBQ25CLFNBQVMsRUFBRSxLQUFLO0NBQ04sQ0FBQSxFQU5nQyxDQU1oQyxDQUFDIn0=
// EXTERNAL MODULE: ./container/layout/main-block/index.tsx
var main_block = __webpack_require__(746);

// EXTERNAL MODULE: ./components/ui/loading-placeholder/index.tsx + 4 modules
var loading_placeholder = __webpack_require__(750);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// EXTERNAL MODULE: ./style/component.ts
var component = __webpack_require__(744);

// CONCATENATED MODULE: ./components/magazine/image-slider-item/initialize.tsx
var initialize_DEFAULT_PROPS = {
    item: [],
    type: '',
    column: 4
};
var initialize_INITIAL_STATE = {
    isLoadedImage: false
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLElBQUksRUFBRSxFQUFFO0lBQ1IsTUFBTSxFQUFFLENBQUM7Q0FDQSxDQUFDO0FBRVosTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHO0lBQzNCLGFBQWEsRUFBRSxLQUFLO0NBQ1gsQ0FBQyJ9
// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ../node_modules/react-on-screen/lib/index.js
var lib = __webpack_require__(758);
var lib_default = /*#__PURE__*/__webpack_require__.n(lib);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./components/magazine/image-slider-item/style.tsx

var generateSwitchStyle = function (mobile, desktop) {
    var switchStyle = {
        MOBILE: { paddingRight: 10, width: mobile, minWidth: mobile },
        DESKTOP: { width: desktop }
    };
    return switchStyle[window.DEVICE_VERSION];
};
/* harmony default export */ var image_slider_item_style = ({
    mainWrap: {
        paddingLeft: 10,
        paddingRight: 10
    },
    heading: {
        paddingLeft: 10,
        display: variable["display"].inlineBlock,
        maxWidth: "100%",
        whiteSpace: "nowrap",
        overflow: "hidden",
        textOverflow: "ellipsis",
        fontFamily: variable["fontAvenirMedium"],
        color: variable["colorBlack"],
        fontSize: 20,
        lineHeight: "40px",
        height: 40,
        letterSpacing: -0.5,
        textTransform: "uppercase",
    },
    container: {
        width: '100%',
        overflowX: 'auto',
        whiteSpace: 'nowrap',
        paddingTop: 0,
        marginBottom: 0,
        display: variable["display"].flex,
        itemSlider: {
            width: "100%",
            height: "100%",
            display: variable["display"].inlineBlock,
            borderRadius: 5,
            overflow: 'hidden',
            boxShadow: variable["shadowBlur"],
            position: variable["position"].relative
        },
        itemSliderPanel: function (imgUrl) { return ({
            width: '100%',
            paddingTop: '62.5%',
            position: variable["position"].relative,
            backgroundImage: "url(" + imgUrl + ")",
            backgroundColor: variable["colorF7"],
            backgroundPosition: 'top center',
            transition: variable["transitionOpacity"],
            backgroundSize: 'cover',
        }); },
        videoIcon: {
            width: 70,
            height: 70,
            position: variable["position"].absolute,
            top: '50%',
            left: '55%',
            transform: 'translate(-50%, -50%)',
            borderTop: '35px solid transparent',
            boxSizing: 'border-box',
            borderLeft: '51px solid white',
            borderBottom: '35px solid transparent',
            opacity: .8
        },
        info: {
            width: '100%',
            height: 110,
            padding: 12,
            position: variable["position"].relative,
            background: variable["colorWhite"],
            display: variable["display"].flex,
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
            image: function (imgUrl) { return ({
                width: '100%',
                height: '100%',
                top: 0,
                left: 0,
                zIndex: -1,
                position: variable["position"].absolute,
                backgroundColor: variable["colorF7"],
                backgroundImage: "url(" + imgUrl + ")",
                backgroundPosition: 'bottom center',
                backgroundSize: 'cover',
                filter: 'blur(4px)',
                transform: "scaleY(-1) scale(1.1)",
                'WebkitBackfaceVisibility': 'hidden',
                'WebkitPerspective': 1000,
                'WebkitTransform': ['translate3d(0,0,0)', 'translateZ(0)'],
                'backfaceVisibility': 'hidden',
                perspective: 1000,
            }); },
            title: {
                color: variable["colorBlack"],
                whiteSpace: 'pre-wrap',
                fontFamily: variable["fontAvenirMedium"],
                fontSize: 14,
                lineHeight: '22px',
                maxHeight: '44px',
                overflow: 'hidden',
                marginBottom: 5
            },
            description: {
                fontSize: 12,
                color: variable["colorBlack"],
                fontFamily: variable["fontAvenirMedium"],
                opacity: .7,
                marginRight: 10,
                whiteSpace: 'pre-wrap',
                lineHeight: '18px',
                maxHeight: 36,
                overflow: 'hidden',
            },
            tagList: {
                width: '100%',
                display: variable["display"].flex,
                flexWrap: 'wrap',
                height: '36px',
                maxHeight: '36px',
                overflow: 'hidden'
            },
            tagItem: {
                fontSize: 12,
                lineHeight: '18px',
                color: variable["colorBlack"],
                fontFamily: variable["fontAvenirMedium"],
                opacity: .7,
                marginRight: 10,
                whiteSpace: 'pre-wrap'
            },
        }
    },
    column: {
        1: { width: '100%' },
        2: { width: '50%' },
        3: generateSwitchStyle('75%', '33.33%'),
        4: generateSwitchStyle('47%', '25%'),
        5: generateSwitchStyle('47%', '20%'),
        6: generateSwitchStyle('50%', '16.66%'),
    },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRCxJQUFNLG1CQUFtQixHQUFHLFVBQUMsTUFBTSxFQUFFLE9BQU87SUFDMUMsSUFBTSxXQUFXLEdBQUc7UUFDbEIsTUFBTSxFQUFFLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUU7UUFDN0QsT0FBTyxFQUFFLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRTtLQUM1QixDQUFDO0lBRUYsTUFBTSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUM7QUFDNUMsQ0FBQyxDQUFDO0FBRUYsZUFBZTtJQUNiLFFBQVEsRUFBRTtRQUNSLFdBQVcsRUFBRSxFQUFFO1FBQ2YsWUFBWSxFQUFFLEVBQUU7S0FDakI7SUFFRCxPQUFPLEVBQUU7UUFDUCxXQUFXLEVBQUUsRUFBRTtRQUNmLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLFdBQVc7UUFDckMsUUFBUSxFQUFFLE1BQU07UUFDaEIsVUFBVSxFQUFFLFFBQVE7UUFDcEIsUUFBUSxFQUFFLFFBQVE7UUFDbEIsWUFBWSxFQUFFLFVBQVU7UUFDeEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7UUFDckMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1FBQzFCLFFBQVEsRUFBRSxFQUFFO1FBQ1osVUFBVSxFQUFFLE1BQU07UUFDbEIsTUFBTSxFQUFFLEVBQUU7UUFDVixhQUFhLEVBQUUsQ0FBQyxHQUFHO1FBQ25CLGFBQWEsRUFBRSxXQUFXO0tBQzNCO0lBRUQsU0FBUyxFQUFFO1FBQ1QsS0FBSyxFQUFFLE1BQU07UUFDYixTQUFTLEVBQUUsTUFBTTtRQUNqQixVQUFVLEVBQUUsUUFBUTtRQUNwQixVQUFVLEVBQUUsQ0FBQztRQUNiLFlBQVksRUFBRSxDQUFDO1FBQ2YsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtRQUU5QixVQUFVLEVBQUU7WUFDVixLQUFLLEVBQUUsTUFBTTtZQUNiLE1BQU0sRUFBRSxNQUFNO1lBQ2QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztZQUNyQyxZQUFZLEVBQUUsQ0FBQztZQUNmLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTtZQUM5QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1NBQ3JDO1FBRUQsZUFBZSxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztZQUM1QixLQUFLLEVBQUUsTUFBTTtZQUNiLFVBQVUsRUFBRSxPQUFPO1lBQ25CLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDcEMsZUFBZSxFQUFFLFNBQU8sTUFBTSxNQUFHO1lBQ2pDLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztZQUNqQyxrQkFBa0IsRUFBRSxZQUFZO1lBQ2hDLFVBQVUsRUFBRSxRQUFRLENBQUMsaUJBQWlCO1lBQ3RDLGNBQWMsRUFBRSxPQUFPO1NBQ3hCLENBQUMsRUFUMkIsQ0FTM0I7UUFFRixTQUFTLEVBQUU7WUFDVCxLQUFLLEVBQUUsRUFBRTtZQUNULE1BQU0sRUFBRSxFQUFFO1lBQ1YsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxHQUFHLEVBQUUsS0FBSztZQUNWLElBQUksRUFBRSxLQUFLO1lBQ1gsU0FBUyxFQUFFLHVCQUF1QjtZQUNsQyxTQUFTLEVBQUUsd0JBQXdCO1lBQ25DLFNBQVMsRUFBRSxZQUFZO1lBQ3ZCLFVBQVUsRUFBRSxrQkFBa0I7WUFDOUIsWUFBWSxFQUFFLHdCQUF3QjtZQUN0QyxPQUFPLEVBQUUsRUFBRTtTQUNaO1FBRUQsSUFBSSxFQUFFO1lBQ0osS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsR0FBRztZQUNYLE9BQU8sRUFBRSxFQUFFO1lBQ1gsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDL0IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixhQUFhLEVBQUUsUUFBUTtZQUN2QixjQUFjLEVBQUUsWUFBWTtZQUM1QixVQUFVLEVBQUUsWUFBWTtZQUV4QixLQUFLLEVBQUUsVUFBQyxNQUFNLElBQUssT0FBQSxDQUFDO2dCQUNsQixLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsTUFBTTtnQkFDZCxHQUFHLEVBQUUsQ0FBQztnQkFDTixJQUFJLEVBQUUsQ0FBQztnQkFDUCxNQUFNLEVBQUUsQ0FBQyxDQUFDO2dCQUNWLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztnQkFDakMsZUFBZSxFQUFFLFNBQU8sTUFBTSxNQUFHO2dCQUNqQyxrQkFBa0IsRUFBRSxlQUFlO2dCQUNuQyxjQUFjLEVBQUUsT0FBTztnQkFDdkIsTUFBTSxFQUFFLFdBQVc7Z0JBQ25CLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLDBCQUEwQixFQUFFLFFBQVE7Z0JBQ3BDLG1CQUFtQixFQUFFLElBQUk7Z0JBQ3pCLGlCQUFpQixFQUFFLENBQUMsb0JBQW9CLEVBQUUsZUFBZSxDQUFDO2dCQUMxRCxvQkFBb0IsRUFBRSxRQUFRO2dCQUM5QixXQUFXLEVBQUUsSUFBSTthQUNsQixDQUFDLEVBbEJpQixDQWtCakI7WUFFRixLQUFLLEVBQUU7Z0JBQ0wsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMxQixVQUFVLEVBQUUsVUFBVTtnQkFDdEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxNQUFNO2dCQUNsQixTQUFTLEVBQUUsTUFBTTtnQkFDakIsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFlBQVksRUFBRSxDQUFDO2FBQ2hCO1lBRUQsV0FBVyxFQUFFO2dCQUNYLFFBQVEsRUFBRSxFQUFFO2dCQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLE9BQU8sRUFBRSxFQUFFO2dCQUNYLFdBQVcsRUFBRSxFQUFFO2dCQUNmLFVBQVUsRUFBRSxVQUFVO2dCQUN0QixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsU0FBUyxFQUFFLEVBQUU7Z0JBQ2IsUUFBUSxFQUFFLFFBQVE7YUFDbkI7WUFFRCxPQUFPLEVBQUU7Z0JBQ1AsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsUUFBUSxFQUFFLE1BQU07Z0JBQ2hCLE1BQU0sRUFBRSxNQUFNO2dCQUNkLFNBQVMsRUFBRSxNQUFNO2dCQUNqQixRQUFRLEVBQUUsUUFBUTthQUNuQjtZQUVELE9BQU8sRUFBRTtnQkFDUCxRQUFRLEVBQUUsRUFBRTtnQkFDWixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtnQkFDckMsT0FBTyxFQUFFLEVBQUU7Z0JBQ1gsV0FBVyxFQUFFLEVBQUU7Z0JBQ2YsVUFBVSxFQUFFLFVBQVU7YUFDdkI7U0FDRjtLQUNGO0lBRUQsTUFBTSxFQUFFO1FBQ04sQ0FBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRTtRQUNwQixDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFO1FBQ25CLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDO1FBQ3ZDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDO1FBQ3BDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDO1FBQ3BDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDO0tBQ3hDO0NBQ00sQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider-item/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};





function renderComponent(_a) {
    var state = _a.state, props = _a.props, handleLoadImage = _a.handleLoadImage;
    var _b = props, item = _b.item, type = _b.type, column = _b.column;
    var isLoadedImage = state.isLoadedImage;
    var linkProps = {
        to: routing["Ma" /* ROUTING_MAGAZINE_DETAIL_PATH */] + "/" + (item && item.slug || ''),
        style: image_slider_item_style.container.itemSlider
    };
    return (react["createElement"](lib_default.a, { style: Object.assign({}, image_slider_item_style.column[column || 4], image_slider_item_style.mainWrap), offset: 200 }, function (_a) {
        var isVisible = _a.isVisible;
        if (!!isVisible) {
            handleLoadImage();
        }
        return (react["createElement"](react_router_dom["NavLink"], __assign({}, linkProps),
            react["createElement"]("div", { style: [
                    image_slider_item_style.container.itemSliderPanel(isLoadedImage && item ? item.cover_image && item.cover_image.medium_url : ''),
                    { opacity: isLoadedImage ? 1 : 0 }
                ] }, 'video' === type && (react["createElement"]("div", { style: image_slider_item_style.container.videoIcon }))),
            react["createElement"]("div", { style: image_slider_item_style.container.info },
                react["createElement"]("div", { style: image_slider_item_style.container.info.title }, item && item.title || ''),
                react["createElement"]("div", { style: image_slider_item_style.container.info.description }, item && item.description || ''))));
    }));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQ3RGLE9BQU8sZUFBZSxNQUFNLGlCQUFpQixDQUFDO0FBRTlDLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixNQUFNLDBCQUEwQixFQUFpQztRQUEvQixnQkFBSyxFQUFFLGdCQUFLLEVBQUUsb0NBQWU7SUFDdkQsSUFBQSxVQUF3QyxFQUF0QyxjQUFJLEVBQUUsY0FBSSxFQUFFLGtCQUFNLENBQXFCO0lBQ3ZDLElBQUEsbUNBQWEsQ0FBVztJQUVoQyxJQUFNLFNBQVMsR0FBRztRQUNoQixFQUFFLEVBQUssNEJBQTRCLFVBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFFO1FBQ2hFLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLFVBQVU7S0FDbEMsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLG9CQUFDLGVBQWUsSUFBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxFQUFFLE1BQU0sRUFBRSxHQUFHLElBRTVGLFVBQUMsRUFBYTtZQUFYLHdCQUFTO1FBQ1YsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFBQyxlQUFlLEVBQUUsQ0FBQTtRQUFBLENBQUM7UUFFckMsTUFBTSxDQUFDLENBQ0wsb0JBQUMsT0FBTyxlQUFLLFNBQVM7WUFDcEIsNkJBQUssS0FBSyxFQUFFO29CQUNSLEtBQUssQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFFLGFBQWEsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDOUcsRUFBRSxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtpQkFDbkMsSUFFQyxPQUFPLEtBQUssSUFBSSxJQUFJLENBQ2xCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBUSxDQUM5QyxDQUVDO1lBQ04sNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSTtnQkFDOUIsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBRyxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQU87Z0JBQ3hFLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLElBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksRUFBRSxDQUFPLENBQ2hGLENBQ0UsQ0FDWCxDQUFBO0lBQ0gsQ0FBQyxDQUVhLENBQ25CLENBQUM7QUFDSixDQUFDO0FBQUEsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider-item/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_SlideItem = /** @class */ (function (_super) {
    __extends(SlideItem, _super);
    function SlideItem(props) {
        var _this = _super.call(this, props) || this;
        _this.state = initialize_INITIAL_STATE;
        return _this;
    }
    // shouldComponentUpdate(nextProps: IProps, nextState: IState) {
    //   // if (true === isEmptyObject(this.props.data) && false === isEmptyObject(nextProps.data)) { return true; }
    //   // if (this.props.listLikedId.length !== nextProps.listLikedId.length) { return true; }
    //   return true;
    // }
    SlideItem.prototype.handleLoadImage = function () {
        var isLoadedImage = this.state.isLoadedImage;
        if (!!isLoadedImage) {
            return;
        }
        this.setState({ isLoadedImage: true });
    };
    SlideItem.prototype.render = function () {
        var renderViewProps = {
            props: this.props,
            state: this.state,
            handleLoadImage: this.handleLoadImage.bind(this)
        };
        return renderComponent(renderViewProps);
    };
    SlideItem.defaultProps = initialize_DEFAULT_PROPS;
    SlideItem = __decorate([
        radium
    ], SlideItem);
    return SlideItem;
}(react["Component"]));
;
/* harmony default export */ var image_slider_item_component = (component_SlideItem);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDNUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUl6QztJQUF3Qiw2QkFBK0I7SUFHckQsbUJBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUM7O0lBQzdCLENBQUM7SUFFRCxnRUFBZ0U7SUFDaEUsZ0hBQWdIO0lBQ2hILDRGQUE0RjtJQUU1RixpQkFBaUI7SUFDakIsSUFBSTtJQUVKLG1DQUFlLEdBQWY7UUFDVSxJQUFBLHdDQUFhLENBQWdCO1FBQ3JDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sQ0FBQztRQUNULENBQUM7UUFFRCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUMsYUFBYSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELDBCQUFNLEdBQU47UUFDRSxJQUFNLGVBQWUsR0FBRztZQUN0QixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLGVBQWUsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDakQsQ0FBQztRQUVGLE1BQU0sQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQS9CTSxzQkFBWSxHQUFXLGFBQWEsQ0FBQztJQUR4QyxTQUFTO1FBRGQsTUFBTTtPQUNELFNBQVMsQ0FpQ2Q7SUFBRCxnQkFBQztDQUFBLEFBakNELENBQXdCLEtBQUssQ0FBQyxTQUFTLEdBaUN0QztBQUFBLENBQUM7QUFFRixlQUFlLFNBQVMsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider-item/index.tsx

/* harmony default export */ var image_slider_item = (image_slider_item_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxTQUFTLE1BQU0sYUFBYSxDQUFDO0FBQ3BDLGVBQWUsU0FBUyxDQUFDIn0=
// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// CONCATENATED MODULE: ./components/magazine/image-slider/style.tsx




var INLINE_STYLE = {
    '.magazine-slide-container .pagination': {
        opacity: 0,
        transform: 'translateY(20px)'
    },
    '.magazine-slide-container:hover .pagination': {
        opacity: 1,
        transform: 'translateY(0)'
    },
    '.magazine-slide-container .left-nav': {
        width: 40,
        height: 60,
        opacity: 0,
        transform: 'translate(-60px, -50%)',
        visibility: 'hidden',
    },
    '.magazine-slide-container:hover .left-nav': {
        opacity: 1,
        transform: 'translate(1px, -50%)',
        visibility: 'visible',
    },
    '.magazine-slide-container .right-nav': {
        width: 40,
        height: 60,
        opacity: 0,
        transform: 'translate(60px, -50%)',
        visibility: 'hidden',
    },
    '.magazine-slide-container:hover .right-nav': {
        opacity: 1,
        transform: 'translate(1px, -50%)',
        visibility: 'visible',
    }
};
/* harmony default export */ var image_slider_style = ({
    container: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{ marginBottom: 10 }],
        DESKTOP: [{ marginBottom: 30 }],
        GENERAL: [{ display: variable["display"].block, width: '100%' }]
    }),
    magazineSlide: {
        position: 'relative',
        overflow: 'hidden',
        container: Object.assign({}, layout["a" /* flexContainer */].justify, component["c" /* block */].content, {
            paddingTop: 10,
            transition: variable["transitionOpacity"],
        }),
        pagination: [
            layout["a" /* flexContainer */].center,
            component["f" /* slidePagination */],
            {
                transition: variable["transitionNormal"],
                bottom: 0
            },
        ],
        navigation: [
            layout["a" /* flexContainer */].center,
            layout["a" /* flexContainer */].verticalCenter,
            component["e" /* slideNavigation */]
        ],
    },
    customStyleLoading: {
        height: 300
    },
    mobileWrap: {
        width: '100%',
        overflowX: 'auto',
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        panel: {
            flexWrap: 'wrap'
        },
        item: {
            width: '100%',
            marginBottom: 5
        }
    },
    placeholder: {
        width: '100%',
        display: variable["display"].flex,
        marginBottom: 35,
        item: {
            flex: 1,
            paddingLeft: 10,
            paddingRight: 10,
        },
        image: {
            width: '100%',
            height: 282,
            marginBottom: 10,
        },
        text: {
            width: '94%',
            height: 25,
            marginBottom: 10,
        },
        lastText: {
            width: '65%',
            height: 25,
        }
    },
    desktop: {
        mainWrap: {
            display: variable["display"].block,
            marginLeft: -9,
            marginRight: -9
        },
        container: {
            width: '100%',
            overflowX: 'auto',
            whiteSpace: 'nowrap',
            paddingTop: 0,
            paddingBottom: 15,
            display: variable["display"].flex,
            overflow: 'hidden',
            itemSlider: {
                width: "100%",
                display: variable["display"].inlineBlock,
                borderRadius: 5,
                overflow: 'hidden',
                boxShadow: variable["shadowBlur"],
                position: variable["position"].relative,
            },
            itemSliderPanel: function (imgUrl) { return ({
                width: '100%',
                paddingTop: '62.5%',
                position: variable["position"].relative,
                backgroundImage: "url(" + imgUrl + ")",
                backgroundColor: variable["colorF7"],
                backgroundPosition: 'top center',
                backgroundSize: 'cover',
            }); },
            videoIcon: {
                width: 70,
                height: 70,
                position: variable["position"].absolute,
                top: '50%',
                left: '55%',
                transform: 'translate(-50%, -50%)',
                borderTop: '35px solid transparent',
                boxSizing: 'border-box',
                borderLeft: '51px solid white',
                borderBottom: '35px solid transparent',
                opacity: .8
            },
            info: {
                width: '100%',
                height: 110,
                padding: 12,
                position: variable["position"].relative,
                background: variable["colorWhite"],
                display: variable["display"].flex,
                flexDirection: 'column',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                image: function (imgUrl) { return ({
                    width: '100%',
                    height: '100%',
                    top: 0,
                    left: 0,
                    zIndex: -1,
                    position: variable["position"].absolute,
                    backgroundColor: variable["colorF7"],
                    backgroundImage: "url(" + imgUrl + ")",
                    backgroundPosition: 'bottom center',
                    backgroundSize: 'cover',
                    filter: 'blur(4px)',
                    transform: "scaleY(-1) scale(1.1)",
                    'WebkitBackfaceVisibility': 'hidden',
                    'WebkitPerspective': 1000,
                    'WebkitTransform': ['translate3d(0,0,0)', 'translateZ(0)'],
                    'backfaceVisibility': 'hidden',
                    perspective: 1000,
                }); },
                title: {
                    color: variable["colorBlack"],
                    whiteSpace: 'pre-wrap',
                    fontFamily: variable["fontAvenirMedium"],
                    fontSize: 14,
                    lineHeight: '22px',
                    maxHeight: '44px',
                    overflow: 'hidden',
                    marginBottom: 5
                },
                description: {
                    fontSize: 12,
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap',
                    lineHeight: '18px',
                    maxHeight: 36,
                    overflow: 'hidden',
                },
                tagList: {
                    width: '100%',
                    display: variable["display"].flex,
                    flexWrap: 'wrap',
                    height: '36px',
                    maxHeight: '36px',
                    overflow: 'hidden'
                },
                tagItem: {
                    fontSize: 12,
                    lineHeight: '18px',
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap'
                },
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFNBQVMsTUFBTSwwQkFBMEIsQ0FBQztBQUN0RCxPQUFPLEtBQUssTUFBTSxNQUFNLHVCQUF1QixDQUFDO0FBQ2hELE9BQU8sS0FBSyxRQUFRLE1BQU0seUJBQXlCLENBQUM7QUFDcEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRXpELE1BQU0sQ0FBQyxJQUFNLFlBQVksR0FBRztJQUMxQix1Q0FBdUMsRUFBRTtRQUN2QyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxrQkFBa0I7S0FDOUI7SUFFRCw2Q0FBNkMsRUFBRTtRQUM3QyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxlQUFlO0tBQzNCO0lBRUQscUNBQXFDLEVBQUU7UUFDckMsS0FBSyxFQUFFLEVBQUU7UUFDVCxNQUFNLEVBQUUsRUFBRTtRQUNWLE9BQU8sRUFBRSxDQUFDO1FBQ1YsU0FBUyxFQUFFLHdCQUF3QjtRQUNuQyxVQUFVLEVBQUUsUUFBUTtLQUNyQjtJQUVELDJDQUEyQyxFQUFFO1FBQzNDLE9BQU8sRUFBRSxDQUFDO1FBQ1YsU0FBUyxFQUFFLHNCQUFzQjtRQUNqQyxVQUFVLEVBQUUsU0FBUztLQUN0QjtJQUVELHNDQUFzQyxFQUFFO1FBQ3RDLEtBQUssRUFBRSxFQUFFO1FBQ1QsTUFBTSxFQUFFLEVBQUU7UUFDVixPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSx1QkFBdUI7UUFDbEMsVUFBVSxFQUFFLFFBQVE7S0FDckI7SUFFRCw0Q0FBNEMsRUFBRTtRQUM1QyxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxzQkFBc0I7UUFDakMsVUFBVSxFQUFFLFNBQVM7S0FDdEI7Q0FDRixDQUFDO0FBRUYsZUFBZTtJQUNiLFNBQVMsRUFBRSxZQUFZLENBQUM7UUFDdEIsTUFBTSxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLENBQUM7UUFDOUIsT0FBTyxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLENBQUM7UUFDL0IsT0FBTyxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxDQUFDO0tBQzlELENBQUM7SUFFRixhQUFhLEVBQUU7UUFDYixRQUFRLEVBQUUsVUFBVTtRQUNwQixRQUFRLEVBQUUsUUFBUTtRQUVsQixTQUFTLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQ3pCLE1BQU0sQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUM1QixTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFDdkI7WUFDRSxVQUFVLEVBQUUsRUFBRTtZQUNkLFVBQVUsRUFBRSxRQUFRLENBQUMsaUJBQWlCO1NBQ3ZDLENBQ0Y7UUFFRCxVQUFVLEVBQUU7WUFDVixNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU07WUFDM0IsU0FBUyxDQUFDLGVBQWU7WUFDekI7Z0JBQ0UsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7Z0JBQ3JDLE1BQU0sRUFBRSxDQUFDO2FBQ1Y7U0FDRjtRQUVELFVBQVUsRUFBRTtZQUNWLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTTtZQUMzQixNQUFNLENBQUMsYUFBYSxDQUFDLGNBQWM7WUFDbkMsU0FBUyxDQUFDLGVBQWU7U0FDMUI7S0FDRjtJQUVELGtCQUFrQixFQUFFO1FBQ2xCLE1BQU0sRUFBRSxHQUFHO0tBQ1o7SUFFRCxVQUFVLEVBQUU7UUFDVixLQUFLLEVBQUUsTUFBTTtRQUNiLFNBQVMsRUFBRSxNQUFNO1FBQ2pCLFVBQVUsRUFBRSxDQUFDO1FBQ2IsWUFBWSxFQUFFLENBQUM7UUFDZixhQUFhLEVBQUUsQ0FBQztRQUNoQixXQUFXLEVBQUUsQ0FBQztRQUVkLEtBQUssRUFBRTtZQUNMLFFBQVEsRUFBRSxNQUFNO1NBQ2pCO1FBRUQsSUFBSSxFQUFFO1lBQ0osS0FBSyxFQUFFLE1BQU07WUFDYixZQUFZLEVBQUUsQ0FBQztTQUNoQjtLQUNGO0lBRUQsV0FBVyxFQUFFO1FBQ1gsS0FBSyxFQUFFLE1BQU07UUFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1FBQzlCLFlBQVksRUFBRSxFQUFFO1FBRWhCLElBQUksRUFBRTtZQUNKLElBQUksRUFBRSxDQUFDO1lBQ1AsV0FBVyxFQUFFLEVBQUU7WUFDZixZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELEtBQUssRUFBRTtZQUNMLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLEdBQUc7WUFDWCxZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELElBQUksRUFBRTtZQUNKLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLEVBQUU7WUFDVixZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELFFBQVEsRUFBRTtZQUNSLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLEVBQUU7U0FDWDtLQUNGO0lBRUQsT0FBTyxFQUFFO1FBQ1AsUUFBUSxFQUFFO1lBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztZQUMvQixVQUFVLEVBQUUsQ0FBQyxDQUFDO1lBQ2QsV0FBVyxFQUFFLENBQUMsQ0FBQztTQUNoQjtRQUVELFNBQVMsRUFBRTtZQUNULEtBQUssRUFBRSxNQUFNO1lBQ2IsU0FBUyxFQUFFLE1BQU07WUFDakIsVUFBVSxFQUFFLFFBQVE7WUFDcEIsVUFBVSxFQUFFLENBQUM7WUFDYixhQUFhLEVBQUUsRUFBRTtZQUNqQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLFFBQVEsRUFBRSxRQUFRO1lBRWxCLFVBQVUsRUFBRTtnQkFDVixLQUFLLEVBQUUsTUFBTTtnQkFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxXQUFXO2dCQUNyQyxZQUFZLEVBQUUsQ0FBQztnQkFDZixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUM5QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2FBQ3JDO1lBRUQsZUFBZSxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztnQkFDNUIsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsVUFBVSxFQUFFLE9BQU87Z0JBQ25CLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztnQkFDakMsZUFBZSxFQUFFLFFBQVEsQ0FBQyxPQUFPO2dCQUNqQyxrQkFBa0IsRUFBRSxZQUFZO2dCQUNoQyxjQUFjLEVBQUUsT0FBTzthQUN4QixDQUFDLEVBUjJCLENBUTNCO1lBRUYsU0FBUyxFQUFFO2dCQUNULEtBQUssRUFBRSxFQUFFO2dCQUNULE1BQU0sRUFBRSxFQUFFO2dCQUNWLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLEdBQUcsRUFBRSxLQUFLO2dCQUNWLElBQUksRUFBRSxLQUFLO2dCQUNYLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLFNBQVMsRUFBRSx3QkFBd0I7Z0JBQ25DLFNBQVMsRUFBRSxZQUFZO2dCQUN2QixVQUFVLEVBQUUsa0JBQWtCO2dCQUM5QixZQUFZLEVBQUUsd0JBQXdCO2dCQUN0QyxPQUFPLEVBQUUsRUFBRTthQUNaO1lBRUQsSUFBSSxFQUFFO2dCQUNKLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxHQUFHO2dCQUNYLE9BQU8sRUFBRSxFQUFFO2dCQUNYLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLFVBQVUsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDL0IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsYUFBYSxFQUFFLFFBQVE7Z0JBQ3ZCLGNBQWMsRUFBRSxZQUFZO2dCQUM1QixVQUFVLEVBQUUsWUFBWTtnQkFFeEIsS0FBSyxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztvQkFDbEIsS0FBSyxFQUFFLE1BQU07b0JBQ2IsTUFBTSxFQUFFLE1BQU07b0JBQ2QsR0FBRyxFQUFFLENBQUM7b0JBQ04sSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLENBQUMsQ0FBQztvQkFDVixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUNwQyxlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87b0JBQ2pDLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztvQkFDakMsa0JBQWtCLEVBQUUsZUFBZTtvQkFDbkMsY0FBYyxFQUFFLE9BQU87b0JBQ3ZCLE1BQU0sRUFBRSxXQUFXO29CQUNuQixTQUFTLEVBQUUsdUJBQXVCO29CQUNsQywwQkFBMEIsRUFBRSxRQUFRO29CQUNwQyxtQkFBbUIsRUFBRSxJQUFJO29CQUN6QixpQkFBaUIsRUFBRSxDQUFDLG9CQUFvQixFQUFFLGVBQWUsQ0FBQztvQkFDMUQsb0JBQW9CLEVBQUUsUUFBUTtvQkFDOUIsV0FBVyxFQUFFLElBQUk7aUJBQ2xCLENBQUMsRUFsQmlCLENBa0JqQjtnQkFFRixLQUFLLEVBQUU7b0JBQ0wsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsVUFBVTtvQkFDdEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7b0JBQ3JDLFFBQVEsRUFBRSxFQUFFO29CQUNaLFVBQVUsRUFBRSxNQUFNO29CQUNsQixTQUFTLEVBQUUsTUFBTTtvQkFDakIsUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLFlBQVksRUFBRSxDQUFDO2lCQUNoQjtnQkFFRCxXQUFXLEVBQUU7b0JBQ1gsUUFBUSxFQUFFLEVBQUU7b0JBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsVUFBVSxFQUFFLFVBQVU7b0JBQ3RCLFVBQVUsRUFBRSxNQUFNO29CQUNsQixTQUFTLEVBQUUsRUFBRTtvQkFDYixRQUFRLEVBQUUsUUFBUTtpQkFDbkI7Z0JBRUQsT0FBTyxFQUFFO29CQUNQLEtBQUssRUFBRSxNQUFNO29CQUNiLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7b0JBQzlCLFFBQVEsRUFBRSxNQUFNO29CQUNoQixNQUFNLEVBQUUsTUFBTTtvQkFDZCxTQUFTLEVBQUUsTUFBTTtvQkFDakIsUUFBUSxFQUFFLFFBQVE7aUJBQ25CO2dCQUVELE9BQU8sRUFBRTtvQkFDUCxRQUFRLEVBQUUsRUFBRTtvQkFDWixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsVUFBVSxFQUFFLFVBQVU7aUJBQ3ZCO2FBQ0Y7U0FDRjtLQUNGO0NBQ0ssQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider/view-desktop.tsx
var view_desktop_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};






var renderSlider = function (_a) {
    var column = _a.column, type = _a.type, magazineSlideSelected = _a.magazineSlideSelected, style = _a.style;
    return (react["createElement"]("magazine-category", { style: image_slider_style.desktop.mainWrap },
        react["createElement"]("div", { style: [image_slider_style.desktop.container, style] }, magazineSlideSelected
            && Array.isArray(magazineSlideSelected.list)
            && magazineSlideSelected.list.map(function (item) {
                var slideProps = {
                    item: item,
                    type: type,
                    column: column
                };
                return react["createElement"](image_slider_item, view_desktop_assign({ key: "slider-item-" + item.id }, slideProps));
            }))));
};
var renderNavigation = function (_a) {
    var magazineSlide = _a.magazineSlide, navLeftSlide = _a.navLeftSlide, navRightSlide = _a.navRightSlide;
    var leftNavProps = {
        className: 'left-nav',
        onClick: navLeftSlide,
        style: [image_slider_style.magazineSlide.navigation, component["e" /* slideNavigation */]['left'], { top: "calc(50% - 65px)" }]
    };
    var rightNavProps = {
        className: 'right-nav',
        onClick: navRightSlide,
        style: [image_slider_style.magazineSlide.navigation, component["e" /* slideNavigation */]['right'], { top: "calc(50% - 65px)" }]
    };
    return magazineSlide.length <= 1
        ? null
        : (react["createElement"]("div", null,
            react["createElement"]("div", view_desktop_assign({}, leftNavProps),
                react["createElement"](icon["a" /* default */], { name: 'angle-left', style: component["e" /* slideNavigation */].icon })),
            react["createElement"]("div", view_desktop_assign({}, rightNavProps),
                react["createElement"](icon["a" /* default */], { name: 'angle-right', style: component["e" /* slideNavigation */].icon }))));
};
var renderPagination = function (_a) {
    var magazineSlide = _a.magazineSlide, selectSlide = _a.selectSlide, countChangeSlide = _a.countChangeSlide;
    var generateItemProps = function (item, index) { return ({
        key: "product-slider-" + item.id,
        onClick: function () { return selectSlide(index); },
        style: [
            component["f" /* slidePagination */].item,
            index === countChangeSlide && component["f" /* slidePagination */].itemActive
        ]
    }); };
    return magazineSlide.length <= 1
        ? null
        : (react["createElement"]("div", { style: image_slider_style.magazineSlide.pagination, className: 'pagination' }, Array.isArray(magazineSlide)
            && magazineSlide.map(function (item, $index) {
                var itemProps = generateItemProps(item, $index);
                return react["createElement"]("div", view_desktop_assign({}, itemProps));
            })));
};
var renderDesktop = function (_a) {
    var props = _a.props, state = _a.state, handleMouseHover = _a.handleMouseHover, selectSlide = _a.selectSlide, navLeftSlide = _a.navLeftSlide, navRightSlide = _a.navRightSlide;
    var _b = state, magazineSlide = _b.magazineSlide, magazineSlideSelected = _b.magazineSlideSelected, countChangeSlide = _b.countChangeSlide;
    var _c = props, column = _c.column, type = _c.type, style = _c.style;
    var containerProps = {
        style: image_slider_style.magazineSlide,
        onMouseEnter: handleMouseHover
    };
    return (react["createElement"]("div", view_desktop_assign({}, containerProps, { className: 'magazine-slide-container' }),
        renderSlider({ column: column, type: type, magazineSlideSelected: magazineSlideSelected, style: style }),
        renderPagination({ magazineSlide: magazineSlide, selectSlide: selectSlide, countChangeSlide: countChangeSlide }),
        renderNavigation({ magazineSlide: magazineSlide, navLeftSlide: navLeftSlide, navRightSlide: navRightSlide }),
        react["createElement"](radium["Style"], { rules: INLINE_STYLE })));
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1kZXNrdG9wLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1kZXNrdG9wLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFL0IsT0FBTyxJQUFJLE1BQU0sZUFBZSxDQUFDO0FBQ2pDLE9BQU8sS0FBSyxTQUFTLE1BQU0sMEJBQTBCLENBQUM7QUFFdEQsT0FBTyxlQUFlLE1BQU0sc0JBQXNCLENBQUM7QUFFbkQsT0FBTyxLQUFLLEVBQUUsRUFBRSxZQUFZLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFHOUMsSUFBTSxZQUFZLEdBQUcsVUFBQyxFQUE4QztRQUE1QyxrQkFBTSxFQUFFLGNBQUksRUFBRSxnREFBcUIsRUFBRSxnQkFBSztJQUFPLE9BQUEsQ0FDdkUsMkNBQW1CLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVE7UUFDOUMsNkJBQUssS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLElBRXhDLHFCQUFxQjtlQUNsQixLQUFLLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQztlQUN6QyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSTtnQkFDcEMsSUFBTSxVQUFVLEdBQUc7b0JBQ2pCLElBQUksTUFBQTtvQkFDSixJQUFJLE1BQUE7b0JBQ0osTUFBTSxRQUFBO2lCQUNQLENBQUE7Z0JBRUQsTUFBTSxDQUFDLG9CQUFDLGVBQWUsYUFBQyxHQUFHLEVBQUUsaUJBQWUsSUFBSSxDQUFDLEVBQUksSUFBTSxVQUFVLEVBQUksQ0FBQTtZQUMzRSxDQUFDLENBQUMsQ0FFQSxDQUNZLENBQ3JCO0FBbEJ3RSxDQWtCeEUsQ0FBQztBQUVGLElBQU0sZ0JBQWdCLEdBQUcsVUFBQyxFQUE4QztRQUE1QyxnQ0FBYSxFQUFFLDhCQUFZLEVBQUUsZ0NBQWE7SUFDcEUsSUFBTSxZQUFZLEdBQUc7UUFDbkIsU0FBUyxFQUFFLFVBQVU7UUFDckIsT0FBTyxFQUFFLFlBQVk7UUFDckIsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxrQkFBa0IsRUFBRSxDQUFDO0tBQ3hHLENBQUM7SUFFRixJQUFNLGFBQWEsR0FBRztRQUNwQixTQUFTLEVBQUUsV0FBVztRQUN0QixPQUFPLEVBQUUsYUFBYTtRQUN0QixLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxFQUFFLEVBQUUsR0FBRyxFQUFFLGtCQUFrQixFQUFFLENBQUM7S0FDekcsQ0FBQztJQUVGLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxJQUFJLENBQUM7UUFDOUIsQ0FBQyxDQUFDLElBQUk7UUFDTixDQUFDLENBQUMsQ0FDQTtZQUNFLHdDQUFTLFlBQVk7Z0JBQ25CLG9CQUFDLElBQUksSUFBQyxJQUFJLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsZUFBZSxDQUFDLElBQUksR0FBSSxDQUMvRDtZQUNOLHdDQUFTLGFBQWE7Z0JBQ3BCLG9CQUFDLElBQUksSUFBQyxJQUFJLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsZUFBZSxDQUFDLElBQUksR0FBSSxDQUNoRSxDQUNGLENBQ1AsQ0FBQztBQUNOLENBQUMsQ0FBQztBQUVGLElBQU0sZ0JBQWdCLEdBQUcsVUFBQyxFQUFnRDtRQUE5QyxnQ0FBYSxFQUFFLDRCQUFXLEVBQUUsc0NBQWdCO0lBQ3RFLElBQU0saUJBQWlCLEdBQUcsVUFBQyxJQUFJLEVBQUUsS0FBSyxJQUFLLE9BQUEsQ0FBQztRQUMxQyxHQUFHLEVBQUUsb0JBQWtCLElBQUksQ0FBQyxFQUFJO1FBQ2hDLE9BQU8sRUFBRSxjQUFNLE9BQUEsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFsQixDQUFrQjtRQUNqQyxLQUFLLEVBQUU7WUFDTCxTQUFTLENBQUMsZUFBZSxDQUFDLElBQUk7WUFDOUIsS0FBSyxLQUFLLGdCQUFnQixJQUFJLFNBQVMsQ0FBQyxlQUFlLENBQUMsVUFBVTtTQUNuRTtLQUNGLENBQUMsRUFQeUMsQ0FPekMsQ0FBQztJQUVILE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxJQUFJLENBQUM7UUFDOUIsQ0FBQyxDQUFDLElBQUk7UUFDTixDQUFDLENBQUMsQ0FDQSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsU0FBUyxFQUFFLFlBQVksSUFFL0QsS0FBSyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUM7ZUFDekIsYUFBYSxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxNQUFNO2dCQUNoQyxJQUFNLFNBQVMsR0FBRyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7Z0JBQ2xELE1BQU0sQ0FBQyx3Q0FBUyxTQUFTLEVBQVEsQ0FBQztZQUNwQyxDQUFDLENBQUMsQ0FFQSxDQUNQLENBQUM7QUFDTixDQUFDLENBQUM7QUFFRixNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUcsVUFBQyxFQUE0RTtRQUExRSxnQkFBSyxFQUFFLGdCQUFLLEVBQUUsc0NBQWdCLEVBQUUsNEJBQVcsRUFBRSw4QkFBWSxFQUFFLGdDQUFhO0lBQ2hHLElBQUEsVUFBNEUsRUFBMUUsZ0NBQWEsRUFBRSxnREFBcUIsRUFBRSxzQ0FBZ0IsQ0FBcUI7SUFDN0UsSUFBQSxVQUF5QyxFQUF2QyxrQkFBTSxFQUFFLGNBQUksRUFBRSxnQkFBSyxDQUFxQjtJQUVoRCxJQUFNLGNBQWMsR0FBRztRQUNyQixLQUFLLEVBQUUsS0FBSyxDQUFDLGFBQWE7UUFDMUIsWUFBWSxFQUFFLGdCQUFnQjtLQUMvQixDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsd0NBQVMsY0FBYyxJQUFFLFNBQVMsRUFBRSwwQkFBMEI7UUFDM0QsWUFBWSxDQUFDLEVBQUUsTUFBTSxRQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUscUJBQXFCLHVCQUFBLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQztRQUM1RCxnQkFBZ0IsQ0FBQyxFQUFFLGFBQWEsZUFBQSxFQUFFLFdBQVcsYUFBQSxFQUFFLGdCQUFnQixrQkFBQSxFQUFFLENBQUM7UUFDbEUsZ0JBQWdCLENBQUMsRUFBRSxhQUFhLGVBQUEsRUFBRSxZQUFZLGNBQUEsRUFBRSxhQUFhLGVBQUEsRUFBRSxDQUFDO1FBQ2pFLG9CQUFDLEtBQUssSUFBQyxLQUFLLEVBQUUsWUFBWSxHQUFJLENBQzFCLENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/image-slider/view-mobile.tsx





var renderMobile = function (_a) {
    var type = _a.type, magazineList = _a.magazineList, column = _a.column;
    return (react["createElement"]("div", { style: [component["c" /* block */].content, image_slider_style.mobileWrap] },
        react["createElement"]("div", { style: [layout["a" /* flexContainer */].noWrap, image_slider_style.mobileWrap.panel] }, Array.isArray(magazineList)
            && magazineList.map(function (magazine, index) { return react["createElement"]("div", { style: image_slider_style.mobileWrap.item },
                react["createElement"](image_slider_item, { key: "image-slide-item-" + index, item: magazine, type: type, column: column })); }))));
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1tb2JpbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LW1vYmlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxlQUFlLE1BQU0sc0JBQXNCLENBQUM7QUFFbkQsT0FBTyxLQUFLLFNBQVMsTUFBTSwwQkFBMEIsQ0FBQztBQUN0RCxPQUFPLEtBQUssTUFBTSxNQUFNLHVCQUF1QixDQUFDO0FBQ2hELE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixNQUFNLENBQUMsSUFBTSxZQUFZLEdBQUcsVUFBQyxFQUE4QjtRQUE1QixjQUFJLEVBQUUsOEJBQVksRUFBRSxrQkFBTTtJQUV2RCxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsVUFBVSxDQUFDO1FBQ3JELDZCQUFLLEtBQUssRUFBRSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLElBRTdELEtBQUssQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDO2VBQ3hCLFlBQVksQ0FBQyxHQUFHLENBQUMsVUFBQyxRQUFRLEVBQUUsS0FBSyxJQUFLLE9BQUEsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSTtnQkFBRSxvQkFBQyxlQUFlLElBQUMsR0FBRyxFQUFFLHNCQUFvQixLQUFPLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxNQUFNLEdBQUksQ0FBTSxFQUExSSxDQUEwSSxDQUFDLENBRWxMLENBQ0YsQ0FDUCxDQUFDO0FBQ0osQ0FBQyxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/image-slider/view.tsx
var view_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};







var renderItemPlaceholder = function (item) { return (react["createElement"]("div", { style: image_slider_style.placeholder.item, key: item },
    react["createElement"](loading_placeholder["a" /* default */], { style: image_slider_style.placeholder.image }))); };
var renderLoadingPlaceholder = function () { return (react["createElement"]("div", { style: image_slider_style.placeholder }, [1, 2, 3, 4].map(renderItemPlaceholder))); };
var renderCustomTitle = function (_a) {
    var title = _a.title, isCustomTitle = _a.isCustomTitle, _b = _a.titleStyle, titleStyle = _b === void 0 ? {} : _b;
    return false === isCustomTitle
        ? null
        : (react["createElement"]("div", null,
            react["createElement"]("div", { style: [component["c" /* block */].heading, component["c" /* block */].heading.multiLine, image_slider_style.desktopTitle, titleStyle] },
                react["createElement"]("div", { style: component["c" /* block */].heading.title.multiLine },
                    react["createElement"]("span", { style: [component["c" /* block */].heading.title.text, component["c" /* block */].heading.title.text.multiLine] }, title)))));
};
var renderView = function (_a) {
    var props = _a.props, state = _a.state, handleMouseHover = _a.handleMouseHover, selectSlide = _a.selectSlide, navLeftSlide = _a.navLeftSlide, navRightSlide = _a.navRightSlide;
    var magazineList = state.magazineList;
    var _b = props, type = _b.type, title = _b.title, showViewMore = _b.showViewMore, column = _b.column, showHeader = _b.showHeader, isCustomTitle = _b.isCustomTitle, titleStyle = _b.titleStyle;
    var renderMobileProps = { type: type, magazineList: magazineList, column: column };
    var renderDesktopProps = {
        props: props,
        state: state,
        navLeftSlide: navLeftSlide,
        navRightSlide: navRightSlide,
        selectSlide: selectSlide,
        handleMouseHover: handleMouseHover
    };
    var switchStyle = {
        MOBILE: function () { return renderMobile(renderMobileProps); },
        DESKTOP: function () { return renderDesktop(renderDesktopProps); }
    };
    var mainBlockProps = {
        title: isCustomTitle ? '' : title,
        showHeader: showHeader,
        showViewMore: showViewMore,
        content: 0 === magazineList.length
            ? renderLoadingPlaceholder()
            : switchStyle[window.DEVICE_VERSION](),
        style: {}
    };
    return (react["createElement"]("div", { style: image_slider_style.container },
        isCustomTitle && renderCustomTitle({ title: title, isCustomTitle: isCustomTitle, titleStyle: titleStyle }),
        react["createElement"](main_block["a" /* default */], view_assign({}, mainBlockProps))));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFHL0IsT0FBTyxTQUFTLE1BQU0sc0NBQXNDLENBQUM7QUFFN0QsT0FBTyxrQkFBa0IsTUFBTSw4QkFBOEIsQ0FBQztBQUU5RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDL0MsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUU3QyxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsT0FBTyxLQUFLLFNBQVMsTUFBTSwwQkFBMEIsQ0FBQztBQUV0RCxJQUFNLHFCQUFxQixHQUFHLFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FDdEMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJO0lBQzNDLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBSSxDQUNsRCxDQUNQLEVBSnVDLENBSXZDLENBQUM7QUFFRixJQUFNLHdCQUF3QixHQUFHLGNBQU0sT0FBQSxDQUNyQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsSUFDMUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FDcEMsQ0FDUCxFQUpzQyxDQUl0QyxDQUFDO0FBRUYsSUFBTSxpQkFBaUIsR0FBRyxVQUFDLEVBQXlDO1FBQXZDLGdCQUFLLEVBQUUsZ0NBQWEsRUFBRSxrQkFBZSxFQUFmLG9DQUFlO0lBQU8sT0FBQSxLQUFLLEtBQUssYUFBYTtRQUM5RixDQUFDLENBQUMsSUFBSTtRQUNOLENBQUMsQ0FBQyxDQUNBO1lBQ0UsNkJBQUssS0FBSyxFQUFFLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxZQUFZLEVBQUUsVUFBVSxDQUFDO2dCQUN0Ryw2QkFBSyxLQUFLLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFNBQVM7b0JBQ2pELDhCQUFNLEtBQUssRUFBRSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFDNUYsS0FBSyxDQUNELENBQ0gsQ0FDRixDQUNGLENBQ1A7QUFac0UsQ0FZdEUsQ0FBQztBQUVKLElBQU0sVUFBVSxHQUFHLFVBQUMsRUFBNEU7UUFBMUUsZ0JBQUssRUFBRSxnQkFBSyxFQUFFLHNDQUFnQixFQUFFLDRCQUFXLEVBQUUsOEJBQVksRUFBRSxnQ0FBYTtJQUNwRixJQUFBLGlDQUFZLENBQXFCO0lBQ25DLElBQUEsVUFBOEYsRUFBNUYsY0FBSSxFQUFFLGdCQUFLLEVBQUUsOEJBQVksRUFBRSxrQkFBTSxFQUFFLDBCQUFVLEVBQUUsZ0NBQWEsRUFBRSwwQkFBVSxDQUFxQjtJQUVyRyxJQUFNLGlCQUFpQixHQUFHLEVBQUUsSUFBSSxNQUFBLEVBQUUsWUFBWSxjQUFBLEVBQUUsTUFBTSxRQUFBLEVBQUUsQ0FBQztJQUN6RCxJQUFNLGtCQUFrQixHQUFHO1FBQ3pCLEtBQUssT0FBQTtRQUNMLEtBQUssT0FBQTtRQUNMLFlBQVksY0FBQTtRQUNaLGFBQWEsZUFBQTtRQUNiLFdBQVcsYUFBQTtRQUNYLGdCQUFnQixrQkFBQTtLQUNqQixDQUFDO0lBRUYsSUFBTSxXQUFXLEdBQUc7UUFDbEIsTUFBTSxFQUFFLGNBQU0sT0FBQSxZQUFZLENBQUMsaUJBQWlCLENBQUMsRUFBL0IsQ0FBK0I7UUFDN0MsT0FBTyxFQUFFLGNBQU0sT0FBQSxhQUFhLENBQUMsa0JBQWtCLENBQUMsRUFBakMsQ0FBaUM7S0FDakQsQ0FBQztJQUVGLElBQU0sY0FBYyxHQUFHO1FBQ3JCLEtBQUssRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSztRQUNqQyxVQUFVLFlBQUE7UUFDVixZQUFZLGNBQUE7UUFDWixPQUFPLEVBQUUsQ0FBQyxLQUFLLFlBQVksQ0FBQyxNQUFNO1lBQ2hDLENBQUMsQ0FBQyx3QkFBd0IsRUFBRTtZQUM1QixDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFBRTtRQUN4QyxLQUFLLEVBQUUsRUFBRTtLQUNWLENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFNBQVM7UUFDeEIsYUFBYSxJQUFJLGlCQUFpQixDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsYUFBYSxlQUFBLEVBQUUsVUFBVSxZQUFBLEVBQUUsQ0FBQztRQUN6RSxvQkFBQyxTQUFTLGVBQUssY0FBYyxFQUFJLENBQzdCLENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/image-slider/component.tsx
var component_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var component_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var component_ImageSlide = /** @class */ (function (_super) {
    component_extends(ImageSlide, _super);
    function ImageSlide(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE(props.data);
        return _this;
    }
    ImageSlide.prototype.componentDidMount = function () {
        this.initDataSlide();
    };
    /**
     * When component will receive new props -> init data for slide
     * @param nextProps prop from parent
     */
    ImageSlide.prototype.componentWillReceiveProps = function (nextProps) {
        this.initDataSlide(nextProps.data);
    };
    /**
     * Init data for slide
     * When : Component will mount OR Component will receive new props
     */
    ImageSlide.prototype.initDataSlide = function (_newMagazineList) {
        var _this = this;
        if (_newMagazineList === void 0) { _newMagazineList = this.state.magazineList; }
        if (true === Object(responsive["b" /* isDesktopVersion */])()) {
            if (false === this.state.firstInit) {
                this.setState({ firstInit: true });
            }
            /**
             * On DESKTOP
             * Init data for magazine slide & magazine slide selected
             */
            var _magazineSlide_1 = [];
            var groupMagazine_1 = {
                id: 0,
                list: []
            };
            /** Assign data into each slider */
            if (_newMagazineList.length > (this.props.column || 3)) {
                Array.isArray(_newMagazineList)
                    && _newMagazineList.map(function (magazine, $index) {
                        groupMagazine_1.id = _magazineSlide_1.length;
                        groupMagazine_1.list.push(magazine);
                        if (groupMagazine_1.list.length === _this.props.column) {
                            _magazineSlide_1.push(Object.assign({}, groupMagazine_1));
                            groupMagazine_1.list = [];
                        }
                    });
            }
            else {
                _magazineSlide_1 = [{ id: 0, list: _newMagazineList }];
            }
            this.setState({
                magazineList: _newMagazineList,
                magazineSlide: _magazineSlide_1,
                magazineSlideSelected: _magazineSlide_1[0] || {}
            });
        }
        else {
            /**
             * On Mobile
             * Only init data for list magazine, not apply slide animation
             */
            this.setState({ magazineList: _newMagazineList });
        }
    };
    /**
     * Navigate slide by button left or right
     * @param _direction `LEFT` or `RIGHT`
     * Will set new index value by @param _direction
     */
    ImageSlide.prototype.navSlide = function (_direction) {
        var _a = this.state, magazineSlide = _a.magazineSlide, countChangeSlide = _a.countChangeSlide;
        /**
         * If navigate to right: increase index value -> set +1 by countChangeSlide
         * If vavigate to left: decrease index value -> set -1 by countChangeSlide
         */
        var newIndexValue = 'left' === _direction ? -1 : 1;
        newIndexValue += countChangeSlide;
        /**
         * Validate new value in range [0, magazineSlide.length - 1]
         */
        newIndexValue = newIndexValue === magazineSlide.length
            ? 0 /** If over max value -> set 0 */
            : (newIndexValue === -1
                /** If under min value -> set magazineSlide.length - 1 */
                ? magazineSlide.length - 1
                : newIndexValue);
        /** Change to new index value */
        this.selectSlide(newIndexValue);
    };
    ImageSlide.prototype.navLeftSlide = function () {
        this.navSlide('left');
    };
    ImageSlide.prototype.navRightSlide = function () {
        this.navSlide('right');
    };
    /**
     * Change slide by set state and setTimeout for animation
     * @param _index new index value
     */
    ImageSlide.prototype.selectSlide = function (_index) {
        var _this = this;
        /** Change background */
        setTimeout(function () { return _this.setState(function (prevState, props) { return ({
            countChangeSlide: _index,
            magazineSlideSelected: prevState.magazineSlide[_index],
        }); }); }, 10);
    };
    ImageSlide.prototype.handleMouseHover = function () {
        var _a = this.props, _b = _a.data, data = _b === void 0 ? [] : _b, _c = _a.column, column = _c === void 0 ? 3 : _c;
        var preLoadImageList = Array.isArray(data)
            ? data
                .filter(function (item, $index) { return $index >= column; })
                .map(function (item) { return item.cover_image.original_url; })
            : [];
        Object(utils_image["b" /* preLoadImage */])(preLoadImageList);
    };
    // shouldComponentUpdate(nextProps: IProps, nextState: IState) {
    //   // if (this.props.data.length !== nextProps.data.length) { return true; }
    //   // if (this.state.countChangeSlide !== nextState.countChangeSlide) { return true; }
    //   // if (this.props.data.length > 0 && this.state.firstInit !== nextState.firstInit) { return true; }
    //   return true;
    // }
    ImageSlide.prototype.render = function () {
        var _this = this;
        var renderViewProps = {
            props: this.props,
            state: this.state,
            handleMouseHover: this.handleMouseHover.bind(this),
            selectSlide: function (index) { return _this.selectSlide(index); },
            navLeftSlide: this.navLeftSlide.bind(this),
            navRightSlide: this.navRightSlide.bind(this)
        };
        return view(renderViewProps);
    };
    ImageSlide.defaultProps = DEFAULT_PROPS;
    ImageSlide = component_decorate([
        radium
    ], ImageSlide);
    return ImageSlide;
}(react["Component"]));
;
/* harmony default export */ var image_slider_component = (component_ImageSlide);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRzdELE9BQU8sRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQzVELE9BQU8sVUFBVSxNQUFNLFFBQVEsQ0FBQztBQUdoQztJQUF5Qiw4QkFBK0I7SUFHdEQsb0JBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDOztJQUN6QyxDQUFDO0lBRUQsc0NBQWlCLEdBQWpCO1FBQ0UsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFFRDs7O09BR0c7SUFDSCw4Q0FBeUIsR0FBekIsVUFBMEIsU0FBUztRQUNqQyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsa0NBQWEsR0FBYixVQUFjLGdCQUFzRDtRQUFwRSxpQkEwQ0M7UUExQ2EsaUNBQUEsRUFBQSxtQkFBK0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZO1FBQ2xFLEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUVoQyxFQUFFLENBQUMsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFZLENBQUMsQ0FBQztZQUFDLENBQUM7WUFDckY7OztlQUdHO1lBQ0gsSUFBSSxnQkFBYyxHQUFlLEVBQUUsQ0FBQztZQUNwQyxJQUFJLGVBQWEsR0FBc0M7Z0JBQ3JELEVBQUUsRUFBRSxDQUFDO2dCQUNMLElBQUksRUFBRSxFQUFFO2FBQ1QsQ0FBQztZQUVGLG1DQUFtQztZQUNuQyxFQUFFLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZELEtBQUssQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUM7dUJBQzFCLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxVQUFDLFFBQVEsRUFBRSxNQUFNO3dCQUN2QyxlQUFhLENBQUMsRUFBRSxHQUFHLGdCQUFjLENBQUMsTUFBTSxDQUFDO3dCQUN6QyxlQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFFbEMsRUFBRSxDQUFDLENBQUMsZUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssS0FBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDOzRCQUNwRCxnQkFBYyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxlQUFhLENBQUMsQ0FBQyxDQUFDOzRCQUN0RCxlQUFhLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQzt3QkFDMUIsQ0FBQztvQkFDSCxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixnQkFBYyxHQUFHLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7WUFDdkQsQ0FBQztZQUVELElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ1osWUFBWSxFQUFFLGdCQUFnQjtnQkFDOUIsYUFBYSxFQUFFLGdCQUFjO2dCQUM3QixxQkFBcUIsRUFBRSxnQkFBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUU7YUFDckMsQ0FBQyxDQUFDO1FBQ2YsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ047OztlQUdHO1lBQ0gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLFlBQVksRUFBRSxnQkFBZ0IsRUFBWSxDQUFDLENBQUM7UUFDOUQsQ0FBQztJQUNILENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsNkJBQVEsR0FBUixVQUFTLFVBQVU7UUFDWCxJQUFBLGVBQWdELEVBQTlDLGdDQUFhLEVBQUUsc0NBQWdCLENBQWdCO1FBRXZEOzs7V0FHRztRQUNILElBQUksYUFBYSxHQUFHLE1BQU0sS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbkQsYUFBYSxJQUFJLGdCQUFnQixDQUFDO1FBRWxDOztXQUVHO1FBQ0gsYUFBYSxHQUFHLGFBQWEsS0FBSyxhQUFhLENBQUMsTUFBTTtZQUNwRCxDQUFDLENBQUMsQ0FBQyxDQUFDLGlDQUFpQztZQUNyQyxDQUFDLENBQUMsQ0FDQSxhQUFhLEtBQUssQ0FBQyxDQUFDO2dCQUNsQix5REFBeUQ7Z0JBQ3pELENBQUMsQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUM7Z0JBQzFCLENBQUMsQ0FBQyxhQUFhLENBQ2xCLENBQUM7UUFFSixnQ0FBZ0M7UUFDaEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRUQsaUNBQVksR0FBWjtRQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDeEIsQ0FBQztJQUVELGtDQUFhLEdBQWI7UUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxnQ0FBVyxHQUFYLFVBQVksTUFBTTtRQUFsQixpQkFPQztRQUxDLHdCQUF3QjtRQUN4QixVQUFVLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxRQUFRLENBQUMsVUFBQyxTQUFTLEVBQUUsS0FBSyxJQUFLLE9BQUEsQ0FBQztZQUNwRCxnQkFBZ0IsRUFBRSxNQUFNO1lBQ3hCLHFCQUFxQixFQUFFLFNBQVMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDO1NBQzVDLENBQUEsRUFIeUMsQ0FHekMsQ0FBQyxFQUhJLENBR0osRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNyQixDQUFDO0lBRUQscUNBQWdCLEdBQWhCO1FBQ1EsSUFBQSxlQUFzQyxFQUFwQyxZQUFTLEVBQVQsOEJBQVMsRUFBRSxjQUFVLEVBQVYsK0JBQVUsQ0FBZ0I7UUFFN0MsSUFBTSxnQkFBZ0IsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztZQUMxQyxDQUFDLENBQUMsSUFBSTtpQkFDSCxNQUFNLENBQUMsVUFBQyxJQUFJLEVBQUUsTUFBTSxJQUFLLE9BQUEsTUFBTSxJQUFJLE1BQU0sRUFBaEIsQ0FBZ0IsQ0FBQztpQkFDMUMsR0FBRyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQTdCLENBQTZCLENBQUM7WUFDN0MsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUVQLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFRCxnRUFBZ0U7SUFDaEUsOEVBQThFO0lBQzlFLHdGQUF3RjtJQUN4Rix3R0FBd0c7SUFFeEcsaUJBQWlCO0lBQ2pCLElBQUk7SUFFSiwyQkFBTSxHQUFOO1FBQUEsaUJBV0M7UUFWQyxJQUFNLGVBQWUsR0FBRztZQUN0QixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLGdCQUFnQixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2xELFdBQVcsRUFBRSxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQXZCLENBQXVCO1lBQzdDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUMsYUFBYSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztTQUM3QyxDQUFDO1FBRUYsTUFBTSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBdEpNLHVCQUFZLEdBQVcsYUFBYSxDQUFDO0lBRHhDLFVBQVU7UUFEZixNQUFNO09BQ0QsVUFBVSxDQXdKZjtJQUFELGlCQUFDO0NBQUEsQUF4SkQsQ0FBeUIsS0FBSyxDQUFDLFNBQVMsR0F3SnZDO0FBQUEsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/image-slider/index.tsx

/* harmony default export */ var image_slider = __webpack_exports__["a"] = (image_slider_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxVQUFVLE1BQU0sYUFBYSxDQUFDO0FBQ3JDLGVBQWUsVUFBVSxDQUFDIn0=

/***/ }),

/***/ 790:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MAGAZINE_CATEGORY_TYPE; });
var MAGAZINE_CATEGORY_TYPE = {
    ONE: {
        type: 1
    },
    TWO: {
        type: 2
    },
    THREE: {
        type: 3
    },
    FOUR: {
        type: 4
    },
    FIVE: {
        type: 5
    },
    EXPERTS_REVIEW: {
        type: 5,
        title: 'User\'s Review'
    },
    VIDEO: {
        type: 6,
        title: 'Video',
        url: ''
    },
    TRENDING: {
        type: 7,
        title: 'Trending',
        url: ''
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFnYXppbmUtY2F0ZWdvcnkuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtYWdhemluZS1jYXRlZ29yeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FBRztJQUNwQyxHQUFHLEVBQUU7UUFDSCxJQUFJLEVBQUUsQ0FBQztLQUNSO0lBRUQsR0FBRyxFQUFFO1FBQ0gsSUFBSSxFQUFFLENBQUM7S0FDUjtJQUVELEtBQUssRUFBRTtRQUNMLElBQUksRUFBRSxDQUFDO0tBQ1I7SUFFRCxJQUFJLEVBQUU7UUFDSixJQUFJLEVBQUUsQ0FBQztLQUNSO0lBRUQsSUFBSSxFQUFFO1FBQ0osSUFBSSxFQUFFLENBQUM7S0FDUjtJQUVELGNBQWMsRUFBRTtRQUNkLElBQUksRUFBRSxDQUFDO1FBQ1AsS0FBSyxFQUFFLGdCQUFnQjtLQUN4QjtJQUVELEtBQUssRUFBRTtRQUNMLElBQUksRUFBRSxDQUFDO1FBQ1AsS0FBSyxFQUFFLE9BQU87UUFDZCxHQUFHLEVBQUUsRUFBRTtLQUNSO0lBRUQsUUFBUSxFQUFFO1FBQ1IsSUFBSSxFQUFFLENBQUM7UUFDUCxLQUFLLEVBQUUsVUFBVTtRQUNqQixHQUFHLEVBQUUsRUFBRTtLQUNSO0NBQ0YsQ0FBQyJ9

/***/ }),

/***/ 797:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./constants/application/magazine-category.ts
var magazine_category = __webpack_require__(790);

// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./components/magazine/category/style.tsx


/* harmony default export */ var style = ({
    largeTitle: {
        fontSize: 22,
        lineHeight: '30px',
        width: '100%',
        fontWeight: 900,
        color: variable["colorBlack08"],
        fontFamily: variable["fontAvenirMedium"],
        display: variable["display"].block,
        marginBottom: 10,
        transition: variable["transitionNormal"],
        textTransform: 'capitalize',
    },
    title: {
        fontSize: 16,
        lineHeight: '20px',
        color: variable["colorBlack09"],
        fontFamily: variable["fontAvenirMedium"],
        width: '100%',
        fontWeight: 900,
        display: variable["display"].block,
        marginBottom: 10,
        textTransform: 'capitalize',
        textAlign: 'justify',
    },
    description: {
        fontSize: 14,
        lineHeight: '22px',
        maxHeight: 66,
        overflow: 'hidden',
        color: variable["colorBlack09"],
        textAlign: 'justify',
        width: '100%',
        marginBottom: 10,
    },
    mainDescription: {
        fontSize: Object(responsive["d" /* isMobileVersion */])() ? 14 : 15,
        lineHeight: '22px',
        color: variable["color2E"],
        width: '100%',
        fontWeight: 900,
        display: variable["display"].block,
        marginBottom: 10,
        maxHeight: 66,
        overflow: 'hidden',
    },
    imgText: {
        position: variable["position"].absolute,
        fontSize: 24,
        lineHeight: '32px',
        width: '50%',
        background: variable["colorWhite095"],
        padding: '20px 20px',
        textAlign: 'left',
        left: 0,
        bottom: 20,
        color: variable["colorBlack08"],
        textTransform: 'uppercase',
        fontFamily: variable["fontAvenirDemiBold"]
    },
    textAlignStyle: {
        left: {
            textAlign: 'left'
        },
        center: {
            textAlign: 'center'
        },
        right: {
            textAlign: 'right'
        }
    },
    magazineCategory: {
        categoryOneContent: {
            display: variable["display"].flex,
            flexDirection: Object(responsive["b" /* isDesktopVersion */])() ? 'row' : 'column',
            justifyContent: 'space-between',
            listSubItem: {
                width: Object(responsive["b" /* isDesktopVersion */])() ? 'calc(40% - 20px)' : '100%',
                marginBottom: 10,
                display: variable["display"].flex,
                justifyContent: 'space-between',
                flexDirection: Object(responsive["b" /* isDesktopVersion */])() ? 'column' : 'row',
                flexWrap: 'wrap',
                subItem: {
                    width: Object(responsive["b" /* isDesktopVersion */])() ? '100%' : '47.5%',
                    marginBottom: 10,
                    itemImage: function (image) { return ({
                        width: '100%',
                        paddingTop: '65%',
                        backgroundImage: "url(" + image + ")",
                        backgroundPosition: 'center',
                        backgroundSize: 'cover',
                        marginBottom: 10,
                        posicontion: variable["position"].relative,
                        transition: variable["transitionNormal"],
                    }); },
                },
            },
            largeItem: {
                width: Object(responsive["b" /* isDesktopVersion */])() ? '60%' : '100%',
                marginBottom: 20,
                cursor: 'pointer',
                itemImage: function (image) { return ({
                    width: '100%',
                    paddingTop: '65%',
                    backgroundImage: "url(" + image + ")",
                    backgroundSize: 'cover',
                    backgroundPosition: 'center',
                    marginBottom: 10,
                    position: variable["position"].relative,
                    transition: variable["transitionNormal"],
                }); },
                itemInfo: {
                    width: '100%',
                    textAlign: 'center',
                    height: 16,
                    marginBottom: 5,
                    marginTop: 10,
                }
            }
        },
        categoryTwoContent: {
            itemWrap: {
                display: variable["display"].flex,
                justifyContent: 'space-between',
                width: '100%',
                marginBottom: 20,
                itemImage: function (image) { return ({
                    width: 'calc(50% - 10px)',
                    paddingTop: '26%',
                    backgroundImage: "url(" + image + ")",
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    posicontion: variable["position"].relative,
                    transition: variable["transitionNormal"],
                }); },
                itemInfo: {
                    width: 'calc(50% - 10px)'
                }
            },
        },
        categoryThreeContent: {
            marginBottom: 20,
            largeItem: {
                display: variable["display"].block,
                width: '100%',
                cursor: 'pointer',
                marginBottom: 20,
                itemImage: function (imgUrl) { return ({
                    width: '100%',
                    paddingTop: '65%',
                    backgroundImage: "url(" + imgUrl + ")",
                    backgroundSize: 'cover',
                    backgroundPosition: 'center center',
                    position: variable["position"].relative,
                    transition: variable["transitionNormal"],
                    marginBottom: 10
                }); }
            }
        },
        categoryVideoContent: {
            container: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{}],
                DESKTOP: [{
                        display: variable["display"].flex,
                        justifyContent: 'space-between'
                    }],
                GENERAL: [{
                        marginBottom: 20
                    }],
            }),
            mainTitle: {
                fontSize: 30,
                lineHeight: '60px',
                color: variable["colorBlack"],
                fontFamily: variable["fontTrirong"]
            },
            largeVideoGroup: {
                container: Object(responsive["a" /* combineStyle */])({
                    MOBILE: [{
                            width: '100%'
                        }],
                    DESKTOP: [{
                            width: 'calc(50% - 20px)'
                        }],
                    GENERAL: [{
                            display: variable["display"].inlineBlock,
                            borderRadius: 5,
                            overflow: 'hidden',
                            boxShadow: variable["shadowBlur"],
                            position: variable["position"].relative,
                        }],
                }),
            },
            video: function (imgUrl) { return ({
                width: '100%',
                paddingTop: '62.5%',
                position: variable["position"].relative,
                backgroundImage: "url(" + imgUrl + ")",
                backgroundColor: variable["colorF7"],
                backgroundPosition: 'top center',
                backgroundSize: 'cover',
            }); },
            videoIcon: {
                width: 70,
                height: 70,
                position: variable["position"].absolute,
                top: '50%',
                left: '55%',
                transform: 'translate(-50%, -50%)',
                borderTop: "35px solid transparent",
                boxSizing: 'border-box',
                borderLeft: "51px solid " + variable["colorWhite"],
                borderBottom: "35px solid " + variable["colorTransparent"],
                opacity: 0.8,
            },
            contentWrap: {
                width: '100%',
                paddingTop: 12,
                paddingRight: 12,
                paddingBottom: 12,
                paddingLeft: 12,
                position: variable["position"].relative,
                backgroundColor: variable["colorWhite"],
                display: variable["display"].flex,
                flexDirection: 'column',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                title: {
                    color: variable["colorBlack"],
                    whiteSpace: 'pre-wrap',
                    fontFamily: variable["fontAvenirMedium"],
                    fontSize: 16,
                    lineHeight: '24px',
                    maxHeight: 48,
                    overflow: 'hidden',
                    marginBottom: 5,
                },
                description: {
                    fontSize: 12,
                    lineHeight: '18px',
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: 0.7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap',
                }
            },
            smallVideoGroup: {
                container: Object(responsive["a" /* combineStyle */])({
                    MOBILE: [{
                            width: '100%'
                        }],
                    DESKTOP: [{
                            width: '50%'
                        }],
                    GENERAL: [{
                            display: variable["display"].flex,
                            flexWrap: 'wrap',
                            justifyContent: 'space-between'
                        }],
                }),
                videoContainer: {
                    width: 'calc(50% - 10px)',
                    borderRadius: 5,
                    overflow: 'hidden',
                    boxShadow: variable["shadowBlur"],
                    position: variable["position"].relative,
                }
            }
        },
        categoryTrendingContent: {
            display: variable["display"].flex,
            flexWrap: 'wrap',
            justifyContent: 'space-between',
            marginBottom: 20,
            width: '100%',
            trendingWrap: {
                width: Object(responsive["d" /* isMobileVersion */])() ? 'calc(50% - 10px)' : 'calc(25% - 15px)',
                margin: '0 0 20px',
                itemImage: function (imgUrl) { return ({
                    width: '100%',
                    paddingTop: '65%',
                    backgroundImage: "url(" + imgUrl + ")",
                    backgroundPosition: 'center center',
                    backgroundSize: 'cover',
                    marginBottom: 10,
                    position: variable["position"].relative,
                    transition: variable["transitionNormal"],
                }); },
            }
        },
        boxContent: {
            display: variable["display"].flex,
            flexDirection: Object(responsive["b" /* isDesktopVersion */])() ? 'row' : 'column',
            largeItem: {
                flex: Object(responsive["b" /* isDesktopVersion */])() ? 10 : 1,
                marginBottom: 20,
                cursor: 'pointer',
                itemImage: function (image) { return ({
                    width: '100%',
                    paddingTop: '65%',
                    backgroundImage: "url(" + image + ")",
                    backgroundSize: 'cover',
                    backgroundPosition: 'center',
                    position: variable["position"].relative,
                    transition: variable["transitionNormal"],
                }); },
                itemInfo: {
                    width: '100%',
                    textAlign: 'center',
                    height: 16,
                    marginBottom: 5,
                    marginTop: 10,
                    infoTitle: {
                        fontSize: 18,
                        lineHeight: '24px',
                        color: variable["color4D"],
                        width: '100%',
                        fontWeight: 900,
                        padding: '0 10px',
                        textAlign: 'center',
                        display: variable["display"].block,
                        marginBottom: 10,
                        transition: variable["transitionNormal"],
                        textTransform: 'capitalize',
                        fontFamily: 'trirong-regular',
                    },
                    textImg: {
                        fontSize: 18,
                        lineHeight: '24px',
                        color: variable["color4D"],
                        width: '100%',
                        fontWeight: 900,
                        textAlign: 'center',
                        display: variable["display"].block,
                        marginBottom: 10,
                        transition: variable["transitionNormal"],
                        textTransform: 'capitalize',
                        fontFamily: 'trirong-regular',
                        position: variable["position"].absolute,
                        bottom: 10,
                        backgroundColor: variable["colorWhite"],
                        maxWidth: 200,
                    },
                    infoDescription: {
                        fontSize: 14,
                        lineHeight: '20px',
                        maxHeight: 60,
                        overflow: 'hidden',
                        color: variable["color75"],
                        textAlign: 'center',
                        width: '100%',
                        marginTop: Object(responsive["d" /* isMobileVersion */])() ? 10 : ''
                    }
                }
            },
            itemDescription: {
                fontSize: 14,
                lineHeight: '20px',
                maxHeight: 60,
                overflow: 'hidden',
                color: variable["color75"],
                textAlign: Object(responsive["b" /* isDesktopVersion */])() ? 'left' : 'center',
                width: '100%',
                padding: Object(responsive["b" /* isDesktopVersion */])() ? '0 10px 0 20px' : '0 10px',
            },
            listSubItem: {
                width: Object(responsive["b" /* isDesktopVersion */])() ? 312 : '100%',
                marginRight: Object(responsive["b" /* isDesktopVersion */])() ? 30 : 0,
                marginBottom: 10,
                display: variable["display"].flex,
                justifyContent: 'space-between',
                flexDirection: Object(responsive["b" /* isDesktopVersion */])() ? 'column' : 'row',
                flexWrap: 'wrap',
                subItem: {
                    width: Object(responsive["b" /* isDesktopVersion */])() ? '100%' : '47.5%',
                    itemImage: function (image) { return ({
                        width: '100%',
                        paddingTop: '65%',
                        backgroundImage: "url(" + image + ")",
                        backgroundPosition: 'center',
                        backgroundSize: 'cover',
                        marginBottom: 10,
                        posicontion: variable["position"].relative,
                        transition: variable["transitionNormal"],
                    }); }
                }
            }
        }
    },
    customStyleLoading: {
        height: 80
    },
    placeholder: {
        width: '100%',
        paddingTop: 20,
        title: {
            background: variable["colorF0"],
            display: variable["display"].block,
            width: 100,
            height: 40,
            margin: '0 auto 30px'
        },
        titleMobile: {
            margin: 0,
            textAlign: 'left'
        },
        control: {
            width: '100%',
            display: variable["display"].flex,
            justifyContent: 'space-between',
            paddingLeft: 10,
            paddingRight: 10,
            marginBottom: 20,
        },
        controlItem: {
            width: 150,
            height: 30,
            background: variable["colorF7"]
        },
        productList: {
            display: variable["display"].flex,
            flexWrap: 'wrap',
            paddingTop: 20,
        },
        productMobileItem: {
            minWidth: '50%',
            width: '50%',
        },
        productItem: {
            flex: 1,
            paddingLeft: 10,
            paddingRight: 10,
            marginBottom: 20,
            minWidth: '20%',
            width: '20%',
            image: {
                width: '100%',
                height: 'auto',
                paddingTop: '65%',
                marginBottom: 10,
            },
            text: {
                width: '100%',
                height: 65,
                marginBottom: 10,
            }
        },
    },
    mobile: {
        mainWrap: {
            display: variable["display"].block,
            paddingTop: 20,
        },
        heading: {
            paddingLeft: 10,
            display: variable["display"].inlineBlock,
            maxWidth: "100%",
            whiteSpace: "nowrap",
            overflow: "hidden",
            textOverflow: "ellipsis",
            fontFamily: variable["fontTrirong"],
            color: variable["colorBlack"],
            fontSize: 20,
            lineHeight: "40px",
            height: 40,
            letterSpacing: -0.5,
        },
        container: {
            width: '100%',
            overflowX: 'auto',
            whiteSpace: 'nowrap',
            paddingTop: 0,
            paddingLeft: 5,
            paddingRight: 5,
            marginBottom: 0,
            itemSlider: {
                margin: 5,
                width: '85%',
                maxWidth: 300,
                display: variable["display"].inlineBlock,
                borderRadius: 5,
                overflow: 'hidden',
                boxShadow: variable["shadowBlur"],
                position: variable["position"].relative,
            },
            itemSliderPanel: function (imgUrl) { return ({
                width: '100%',
                paddingTop: '62.5%',
                position: variable["position"].relative,
                backgroundImage: "url(" + imgUrl + ")",
                backgroundColor: variable["colorF7"],
                backgroundPosition: 'top center',
                backgroundSize: 'cover',
            }); },
            videoIcon: {
                width: 70,
                height: 70,
                position: variable["position"].absolute,
                top: '50%',
                left: '55%',
                transform: 'translate(-50%, -50%)',
                borderTop: '35px solid transparent',
                boxSizing: 'border-box',
                borderLeft: '51px solid white',
                borderBottom: '35px solid transparent',
                opacity: .8
            },
            info: {
                width: '100%',
                height: 110,
                padding: 12,
                position: variable["position"].relative,
                background: variable["colorWhite"],
                display: variable["display"].flex,
                flexDirection: 'column',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                image: function (imgUrl) { return ({
                    width: '100%',
                    height: '100%',
                    top: 0,
                    left: 0,
                    zIndex: -1,
                    position: variable["position"].absolute,
                    backgroundColor: variable["colorF7"],
                    backgroundImage: "url(" + imgUrl + ")",
                    backgroundPosition: 'bottom center',
                    backgroundSize: 'cover',
                    filter: 'blur(4px)',
                    transform: "scaleY(-1) scale(1.1)",
                    'WebkitBackfaceVisibility': 'hidden',
                    'WebkitPerspective': 1000,
                    'WebkitTransform': ['translate3d(0,0,0)', 'translateZ(0)'],
                    'backfaceVisibility': 'hidden',
                    perspective: 1000,
                }); },
                title: {
                    color: variable["colorBlack"],
                    whiteSpace: 'pre-wrap',
                    fontFamily: variable["fontAvenirMedium"],
                    fontSize: 14,
                    lineHeight: '22px',
                    maxHeight: '44px',
                    overflow: 'hidden',
                    marginBottom: 5
                },
                description: {
                    fontSize: 12,
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap',
                    lineHeight: '18px',
                    maxHeight: 36,
                    overflow: 'hidden',
                },
                tagList: {
                    width: '100%',
                    display: variable["display"].flex,
                    flexWrap: 'wrap',
                    height: '36px',
                    maxHeight: '36px',
                    overflow: 'hidden'
                },
                tagItem: {
                    fontSize: 12,
                    lineHeight: '18px',
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap'
                },
            }
        }
    },
    desktop: {
        heading: {
            fontSize: 30,
            lineHeight: '60px',
            color: variable["colorBlack"],
            fontFamily: variable["fontTrirong"]
        },
        container: {
            width: '100%',
            display: variable["display"].flex,
            flexWrap: 'wrap',
            justifyContent: 'space-between',
            paddingTop: 10,
            paddingLeft: 0,
            paddingRight: 0,
            marginBottom: 20,
            itemSlider: {
                marginBottom: 20,
                width: 'calc(50% - 10px)',
                display: variable["display"].inlineBlock,
                borderRadius: 5,
                overflow: 'hidden',
                boxShadow: variable["shadowBlur"],
                position: variable["position"].relative,
            },
            smallItemSlider: {
                marginBottom: 20,
                width: 'calc(25% - 15px)',
                display: variable["display"].inlineBlock,
                borderRadius: 5,
                overflow: 'hidden',
                boxShadow: variable["shadowBlur"],
                position: variable["position"].relative,
            },
            info: {
                width: '100%',
                padding: 12,
                position: variable["position"].relative,
                background: variable["colorWhite"],
                display: variable["display"].flex,
                flexDirection: 'column',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                description: {
                    fontSize: 13,
                    lineHeight: '18px',
                    maxHeight: 36,
                    overflow: 'hidden',
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap'
                },
                title: {
                    color: variable["colorBlack"],
                    whiteSpace: 'pre-wrap',
                    fontFamily: variable["fontAvenirMedium"],
                    fontSize: 16,
                    lineHeight: '24px',
                    maxHeight: '48px',
                    overflow: 'hidden',
                    marginBottom: 5
                },
                tagItem: {
                    fontSize: 12,
                    lineHeight: '18px',
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 12,
                    whiteSpace: 'pre-wrap'
                },
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsZUFBZSxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRTVGLGVBQWU7SUFFYixVQUFVLEVBQUU7UUFDVixRQUFRLEVBQUUsRUFBRTtRQUNaLFVBQVUsRUFBRSxNQUFNO1FBQ2xCLEtBQUssRUFBRSxNQUFNO1FBQ2IsVUFBVSxFQUFFLEdBQUc7UUFDZixLQUFLLEVBQUUsUUFBUSxDQUFDLFlBQVk7UUFDNUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7UUFDckMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztRQUMvQixZQUFZLEVBQUUsRUFBRTtRQUNoQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtRQUNyQyxhQUFhLEVBQUUsWUFBWTtLQUM1QjtJQUVELEtBQUssRUFBRTtRQUNMLFFBQVEsRUFBRSxFQUFFO1FBQ1osVUFBVSxFQUFFLE1BQU07UUFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxZQUFZO1FBQzVCLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO1FBQ3JDLEtBQUssRUFBRSxNQUFNO1FBQ2IsVUFBVSxFQUFFLEdBQUc7UUFDZixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1FBQy9CLFlBQVksRUFBRSxFQUFFO1FBQ2hCLGFBQWEsRUFBRSxZQUFZO1FBQzNCLFNBQVMsRUFBRSxTQUFTO0tBQ3JCO0lBRUQsV0FBVyxFQUFFO1FBQ1gsUUFBUSxFQUFFLEVBQUU7UUFDWixVQUFVLEVBQUUsTUFBTTtRQUNsQixTQUFTLEVBQUUsRUFBRTtRQUNiLFFBQVEsRUFBRSxRQUFRO1FBQ2xCLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTtRQUM1QixTQUFTLEVBQUUsU0FBUztRQUNwQixLQUFLLEVBQUUsTUFBTTtRQUNiLFlBQVksRUFBRSxFQUFFO0tBQ2pCO0lBRUQsZUFBZSxFQUFFO1FBQ2YsUUFBUSxFQUFFLGVBQWUsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUU7UUFDckMsVUFBVSxFQUFFLE1BQU07UUFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO1FBQ3ZCLEtBQUssRUFBRSxNQUFNO1FBQ2IsVUFBVSxFQUFFLEdBQUc7UUFDZixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1FBQy9CLFlBQVksRUFBRSxFQUFFO1FBQ2hCLFNBQVMsRUFBRSxFQUFFO1FBQ2IsUUFBUSxFQUFFLFFBQVE7S0FDbkI7SUFFRCxPQUFPLEVBQUU7UUFDUCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1FBQ3BDLFFBQVEsRUFBRSxFQUFFO1FBQ1osVUFBVSxFQUFFLE1BQU07UUFDbEIsS0FBSyxFQUFFLEtBQUs7UUFDWixVQUFVLEVBQUUsUUFBUSxDQUFDLGFBQWE7UUFDbEMsT0FBTyxFQUFFLFdBQVc7UUFDcEIsU0FBUyxFQUFFLE1BQU07UUFDakIsSUFBSSxFQUFFLENBQUM7UUFDUCxNQUFNLEVBQUUsRUFBRTtRQUNWLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTtRQUM1QixhQUFhLEVBQUUsV0FBVztRQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGtCQUFrQjtLQUN4QztJQUVELGNBQWMsRUFBRTtRQUNkLElBQUksRUFBRTtZQUNKLFNBQVMsRUFBRSxNQUFNO1NBQ2xCO1FBRUQsTUFBTSxFQUFFO1lBQ04sU0FBUyxFQUFFLFFBQVE7U0FDcEI7UUFFRCxLQUFLLEVBQUU7WUFDTCxTQUFTLEVBQUUsT0FBTztTQUNuQjtLQUNGO0lBRUQsZ0JBQWdCLEVBQUU7UUFFaEIsa0JBQWtCLEVBQUU7WUFDbEIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixhQUFhLEVBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxRQUFRO1lBQ3BELGNBQWMsRUFBRSxlQUFlO1lBRS9CLFdBQVcsRUFBRTtnQkFDWCxLQUFLLEVBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLE1BQU07Z0JBQ3ZELFlBQVksRUFBRSxFQUFFO2dCQUNoQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixjQUFjLEVBQUUsZUFBZTtnQkFDL0IsYUFBYSxFQUFFLGdCQUFnQixFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSztnQkFDcEQsUUFBUSxFQUFFLE1BQU07Z0JBRWhCLE9BQU8sRUFBRTtvQkFDUCxLQUFLLEVBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPO29CQUM1QyxZQUFZLEVBQUUsRUFBRTtvQkFFaEIsU0FBUyxFQUFFLFVBQUMsS0FBYSxJQUFLLE9BQUEsQ0FBQzt3QkFDN0IsS0FBSyxFQUFFLE1BQU07d0JBQ2IsVUFBVSxFQUFFLEtBQUs7d0JBQ2pCLGVBQWUsRUFBRSxTQUFPLEtBQUssTUFBRzt3QkFDaEMsa0JBQWtCLEVBQUUsUUFBUTt3QkFDNUIsY0FBYyxFQUFFLE9BQU87d0JBQ3ZCLFlBQVksRUFBRSxFQUFFO3dCQUNoQixXQUFXLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO3dCQUN2QyxVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtxQkFDdEMsQ0FBQyxFQVQ0QixDQVM1QjtpQkFDSDthQUNGO1lBRUQsU0FBUyxFQUFFO2dCQUNULEtBQUssRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLE1BQU07Z0JBQzFDLFlBQVksRUFBRSxFQUFFO2dCQUNoQixNQUFNLEVBQUUsU0FBUztnQkFFakIsU0FBUyxFQUFFLFVBQUMsS0FBYSxJQUFLLE9BQUEsQ0FBQztvQkFDN0IsS0FBSyxFQUFFLE1BQU07b0JBQ2IsVUFBVSxFQUFFLEtBQUs7b0JBQ2pCLGVBQWUsRUFBRSxTQUFPLEtBQUssTUFBRztvQkFDaEMsY0FBYyxFQUFFLE9BQU87b0JBQ3ZCLGtCQUFrQixFQUFFLFFBQVE7b0JBQzVCLFlBQVksRUFBRSxFQUFFO29CQUNoQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUNwQyxVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtpQkFDdEMsQ0FBQyxFQVQ0QixDQVM1QjtnQkFFRixRQUFRLEVBQUU7b0JBQ1IsS0FBSyxFQUFFLE1BQU07b0JBQ2IsU0FBUyxFQUFFLFFBQVE7b0JBQ25CLE1BQU0sRUFBRSxFQUFFO29CQUNWLFlBQVksRUFBRSxDQUFDO29CQUNmLFNBQVMsRUFBRSxFQUFFO2lCQUNkO2FBQ0Y7U0FDRjtRQUVELGtCQUFrQixFQUFFO1lBRWxCLFFBQVEsRUFBRTtnQkFDUixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixjQUFjLEVBQUUsZUFBZTtnQkFDL0IsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsWUFBWSxFQUFFLEVBQUU7Z0JBRWhCLFNBQVMsRUFBRSxVQUFDLEtBQWEsSUFBSyxPQUFBLENBQUM7b0JBQzdCLEtBQUssRUFBRSxrQkFBa0I7b0JBQ3pCLFVBQVUsRUFBRSxLQUFLO29CQUNqQixlQUFlLEVBQUUsU0FBTyxLQUFLLE1BQUc7b0JBQ2hDLGtCQUFrQixFQUFFLFFBQVE7b0JBQzVCLGNBQWMsRUFBRSxPQUFPO29CQUN2QixXQUFXLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUN2QyxVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtpQkFDdEMsQ0FBQyxFQVI0QixDQVE1QjtnQkFFRixRQUFRLEVBQUU7b0JBQ1IsS0FBSyxFQUFFLGtCQUFrQjtpQkFDMUI7YUFDRjtTQUNGO1FBRUQsb0JBQW9CLEVBQUU7WUFDcEIsWUFBWSxFQUFFLEVBQUU7WUFFaEIsU0FBUyxFQUFFO2dCQUNULE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7Z0JBQy9CLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxTQUFTO2dCQUNqQixZQUFZLEVBQUUsRUFBRTtnQkFFaEIsU0FBUyxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztvQkFDdEIsS0FBSyxFQUFFLE1BQU07b0JBQ2IsVUFBVSxFQUFFLEtBQUs7b0JBQ2pCLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztvQkFDakMsY0FBYyxFQUFFLE9BQU87b0JBQ3ZCLGtCQUFrQixFQUFFLGVBQWU7b0JBQ25DLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7b0JBQ3BDLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO29CQUNyQyxZQUFZLEVBQUUsRUFBRTtpQkFDakIsQ0FBQyxFQVRxQixDQVNyQjthQUNIO1NBQ0Y7UUFFRCxvQkFBb0IsRUFBRTtZQUNwQixTQUFTLEVBQUUsWUFBWSxDQUFDO2dCQUN0QixNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUM7Z0JBRVosT0FBTyxFQUFFLENBQUM7d0JBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTt3QkFDOUIsY0FBYyxFQUFFLGVBQWU7cUJBQ2hDLENBQUM7Z0JBRUYsT0FBTyxFQUFFLENBQUM7d0JBQ1IsWUFBWSxFQUFFLEVBQUU7cUJBQ2pCLENBQUM7YUFDSCxDQUFDO1lBRUYsU0FBUyxFQUFFO2dCQUNULFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxNQUFNO2dCQUNsQixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7Z0JBQzFCLFVBQVUsRUFBRSxRQUFRLENBQUMsV0FBVzthQUNqQztZQUVELGVBQWUsRUFBRTtnQkFDZixTQUFTLEVBQUUsWUFBWSxDQUFDO29CQUN0QixNQUFNLEVBQUUsQ0FBQzs0QkFDUCxLQUFLLEVBQUUsTUFBTTt5QkFDZCxDQUFDO29CQUVGLE9BQU8sRUFBRSxDQUFDOzRCQUNSLEtBQUssRUFBRSxrQkFBa0I7eUJBQzFCLENBQUM7b0JBRUYsT0FBTyxFQUFFLENBQUM7NEJBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVzs0QkFDckMsWUFBWSxFQUFFLENBQUM7NEJBQ2YsUUFBUSxFQUFFLFFBQVE7NEJBQ2xCLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTs0QkFDOUIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTt5QkFDckMsQ0FBQztpQkFDSCxDQUFDO2FBQ0g7WUFFRCxLQUFLLEVBQUUsVUFBQyxNQUFNLElBQUssT0FBQSxDQUFDO2dCQUNsQixLQUFLLEVBQUUsTUFBTTtnQkFDYixVQUFVLEVBQUUsT0FBTztnQkFDbkIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtnQkFDcEMsZUFBZSxFQUFFLFNBQU8sTUFBTSxNQUFHO2dCQUNqQyxlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87Z0JBQ2pDLGtCQUFrQixFQUFFLFlBQVk7Z0JBQ2hDLGNBQWMsRUFBRSxPQUFPO2FBQ3hCLENBQUMsRUFSaUIsQ0FRakI7WUFFRixTQUFTLEVBQUU7Z0JBQ1QsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtnQkFDcEMsR0FBRyxFQUFFLEtBQUs7Z0JBQ1YsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsU0FBUyxFQUFFLHVCQUF1QjtnQkFDbEMsU0FBUyxFQUFFLHdCQUF3QjtnQkFDbkMsU0FBUyxFQUFFLFlBQVk7Z0JBQ3ZCLFVBQVUsRUFBRSxnQkFBYyxRQUFRLENBQUMsVUFBWTtnQkFDL0MsWUFBWSxFQUFFLGdCQUFjLFFBQVEsQ0FBQyxnQkFBa0I7Z0JBQ3ZELE9BQU8sRUFBRSxHQUFHO2FBQ2I7WUFFRCxXQUFXLEVBQUU7Z0JBQ1gsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsVUFBVSxFQUFFLEVBQUU7Z0JBQ2QsWUFBWSxFQUFFLEVBQUU7Z0JBQ2hCLGFBQWEsRUFBRSxFQUFFO2dCQUNqQixXQUFXLEVBQUUsRUFBRTtnQkFDZixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxlQUFlLEVBQUUsUUFBUSxDQUFDLFVBQVU7Z0JBQ3BDLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLGFBQWEsRUFBRSxRQUFRO2dCQUN2QixjQUFjLEVBQUUsWUFBWTtnQkFDNUIsVUFBVSxFQUFFLFlBQVk7Z0JBRXhCLEtBQUssRUFBRTtvQkFDTCxLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7b0JBQzFCLFVBQVUsRUFBRSxVQUFVO29CQUN0QixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsUUFBUSxFQUFFLEVBQUU7b0JBQ1osVUFBVSxFQUFFLE1BQU07b0JBQ2xCLFNBQVMsRUFBRSxFQUFFO29CQUNiLFFBQVEsRUFBRSxRQUFRO29CQUNsQixZQUFZLEVBQUUsQ0FBQztpQkFDaEI7Z0JBRUQsV0FBVyxFQUFFO29CQUNYLFFBQVEsRUFBRSxFQUFFO29CQUNaLFVBQVUsRUFBRSxNQUFNO29CQUNsQixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7b0JBQzFCLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO29CQUNyQyxPQUFPLEVBQUUsR0FBRztvQkFDWixXQUFXLEVBQUUsRUFBRTtvQkFDZixVQUFVLEVBQUUsVUFBVTtpQkFDdkI7YUFDRjtZQUVELGVBQWUsRUFBRTtnQkFDZixTQUFTLEVBQUUsWUFBWSxDQUFDO29CQUN0QixNQUFNLEVBQUUsQ0FBQzs0QkFDUCxLQUFLLEVBQUUsTUFBTTt5QkFDZCxDQUFDO29CQUVGLE9BQU8sRUFBRSxDQUFDOzRCQUNSLEtBQUssRUFBRSxLQUFLO3lCQUNiLENBQUM7b0JBRUYsT0FBTyxFQUFFLENBQUM7NEJBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTs0QkFDOUIsUUFBUSxFQUFFLE1BQU07NEJBQ2hCLGNBQWMsRUFBRSxlQUFlO3lCQUNoQyxDQUFDO2lCQUNILENBQUM7Z0JBRUYsY0FBYyxFQUFFO29CQUNkLEtBQUssRUFBRSxrQkFBa0I7b0JBQ3pCLFlBQVksRUFBRSxDQUFDO29CQUNmLFFBQVEsRUFBRSxRQUFRO29CQUNsQixTQUFTLEVBQUUsUUFBUSxDQUFDLFVBQVU7b0JBQzlCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7aUJBQ3JDO2FBQ0Y7U0FDRjtRQUVELHVCQUF1QixFQUFFO1lBQ3ZCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsUUFBUSxFQUFFLE1BQU07WUFDaEIsY0FBYyxFQUFFLGVBQWU7WUFDL0IsWUFBWSxFQUFFLEVBQUU7WUFDaEIsS0FBSyxFQUFFLE1BQU07WUFFYixZQUFZLEVBQUU7Z0JBQ1osS0FBSyxFQUFFLGVBQWUsRUFBRSxDQUFDLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsa0JBQWtCO2dCQUNsRSxNQUFNLEVBQUUsVUFBVTtnQkFFbEIsU0FBUyxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztvQkFDdEIsS0FBSyxFQUFFLE1BQU07b0JBQ2IsVUFBVSxFQUFFLEtBQUs7b0JBQ2pCLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztvQkFDakMsa0JBQWtCLEVBQUUsZUFBZTtvQkFDbkMsY0FBYyxFQUFFLE9BQU87b0JBQ3ZCLFlBQVksRUFBRSxFQUFFO29CQUNoQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUNwQyxVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtpQkFDdEMsQ0FBQyxFQVRxQixDQVNyQjthQUNIO1NBQ0Y7UUFFRCxVQUFVLEVBQUU7WUFDVixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLGFBQWEsRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLFFBQVE7WUFFcEQsU0FBUyxFQUFFO2dCQUNULElBQUksRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pDLFlBQVksRUFBRSxFQUFFO2dCQUNoQixNQUFNLEVBQUUsU0FBUztnQkFFakIsU0FBUyxFQUFFLFVBQUMsS0FBYSxJQUFLLE9BQUEsQ0FBQztvQkFDN0IsS0FBSyxFQUFFLE1BQU07b0JBQ2IsVUFBVSxFQUFFLEtBQUs7b0JBQ2pCLGVBQWUsRUFBRSxTQUFPLEtBQUssTUFBRztvQkFDaEMsY0FBYyxFQUFFLE9BQU87b0JBQ3ZCLGtCQUFrQixFQUFFLFFBQVE7b0JBQzVCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7b0JBQ3BDLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO2lCQUN0QyxDQUFDLEVBUjRCLENBUTVCO2dCQUVGLFFBQVEsRUFBRTtvQkFDUixLQUFLLEVBQUUsTUFBTTtvQkFDYixTQUFTLEVBQUUsUUFBUTtvQkFDbkIsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsWUFBWSxFQUFFLENBQUM7b0JBQ2YsU0FBUyxFQUFFLEVBQUU7b0JBRWIsU0FBUyxFQUFFO3dCQUNULFFBQVEsRUFBRSxFQUFFO3dCQUNaLFVBQVUsRUFBRSxNQUFNO3dCQUNsQixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87d0JBQ3ZCLEtBQUssRUFBRSxNQUFNO3dCQUNiLFVBQVUsRUFBRSxHQUFHO3dCQUNmLE9BQU8sRUFBRSxRQUFRO3dCQUNqQixTQUFTLEVBQUUsUUFBUTt3QkFDbkIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSzt3QkFDL0IsWUFBWSxFQUFFLEVBQUU7d0JBQ2hCLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO3dCQUNyQyxhQUFhLEVBQUUsWUFBWTt3QkFDM0IsVUFBVSxFQUFFLGlCQUFpQjtxQkFDOUI7b0JBRUQsT0FBTyxFQUFFO3dCQUNQLFFBQVEsRUFBRSxFQUFFO3dCQUNaLFVBQVUsRUFBRSxNQUFNO3dCQUNsQixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87d0JBQ3ZCLEtBQUssRUFBRSxNQUFNO3dCQUNiLFVBQVUsRUFBRSxHQUFHO3dCQUNmLFNBQVMsRUFBRSxRQUFRO3dCQUNuQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO3dCQUMvQixZQUFZLEVBQUUsRUFBRTt3QkFDaEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7d0JBQ3JDLGFBQWEsRUFBRSxZQUFZO3dCQUMzQixVQUFVLEVBQUUsaUJBQWlCO3dCQUM3QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO3dCQUNwQyxNQUFNLEVBQUUsRUFBRTt3QkFDVixlQUFlLEVBQUUsUUFBUSxDQUFDLFVBQVU7d0JBQ3BDLFFBQVEsRUFBRSxHQUFHO3FCQUNkO29CQUVELGVBQWUsRUFBRTt3QkFDZixRQUFRLEVBQUUsRUFBRTt3QkFDWixVQUFVLEVBQUUsTUFBTTt3QkFDbEIsU0FBUyxFQUFFLEVBQUU7d0JBQ2IsUUFBUSxFQUFFLFFBQVE7d0JBQ2xCLEtBQUssRUFBRSxRQUFRLENBQUMsT0FBTzt3QkFDdkIsU0FBUyxFQUFFLFFBQVE7d0JBQ25CLEtBQUssRUFBRSxNQUFNO3dCQUNiLFNBQVMsRUFBRSxlQUFlLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFO3FCQUN2QztpQkFDRjthQUNGO1lBRUQsZUFBZSxFQUFFO2dCQUNmLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxNQUFNO2dCQUNsQixTQUFTLEVBQUUsRUFBRTtnQkFDYixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO2dCQUN2QixTQUFTLEVBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxRQUFRO2dCQUNqRCxLQUFLLEVBQUUsTUFBTTtnQkFDYixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxRQUFRO2FBQ3pEO1lBRUQsV0FBVyxFQUFFO2dCQUNYLEtBQUssRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLE1BQU07Z0JBQ3hDLFdBQVcsRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLFlBQVksRUFBRSxFQUFFO2dCQUNoQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixjQUFjLEVBQUUsZUFBZTtnQkFDL0IsYUFBYSxFQUFFLGdCQUFnQixFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSztnQkFDcEQsUUFBUSxFQUFFLE1BQU07Z0JBRWhCLE9BQU8sRUFBRTtvQkFDUCxLQUFLLEVBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPO29CQUU1QyxTQUFTLEVBQUUsVUFBQyxLQUFhLElBQUssT0FBQSxDQUFDO3dCQUM3QixLQUFLLEVBQUUsTUFBTTt3QkFDYixVQUFVLEVBQUUsS0FBSzt3QkFDakIsZUFBZSxFQUFFLFNBQU8sS0FBSyxNQUFHO3dCQUNoQyxrQkFBa0IsRUFBRSxRQUFRO3dCQUM1QixjQUFjLEVBQUUsT0FBTzt3QkFDdkIsWUFBWSxFQUFFLEVBQUU7d0JBQ2hCLFdBQVcsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7d0JBQ3ZDLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO3FCQUN0QyxDQUFDLEVBVDRCLENBUzVCO2lCQUNIO2FBQ0Y7U0FDRjtLQUNGO0lBRUQsa0JBQWtCLEVBQUU7UUFDbEIsTUFBTSxFQUFFLEVBQUU7S0FDWDtJQUVELFdBQVcsRUFBRTtRQUNYLEtBQUssRUFBRSxNQUFNO1FBQ2IsVUFBVSxFQUFFLEVBQUU7UUFFZCxLQUFLLEVBQUU7WUFDTCxVQUFVLEVBQUUsUUFBUSxDQUFDLE9BQU87WUFDNUIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztZQUMvQixLQUFLLEVBQUUsR0FBRztZQUNWLE1BQU0sRUFBRSxFQUFFO1lBQ1YsTUFBTSxFQUFFLGFBQWE7U0FDdEI7UUFFRCxXQUFXLEVBQUU7WUFDWCxNQUFNLEVBQUUsQ0FBQztZQUNULFNBQVMsRUFBRSxNQUFNO1NBQ2xCO1FBRUQsT0FBTyxFQUFFO1lBQ1AsS0FBSyxFQUFFLE1BQU07WUFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLGNBQWMsRUFBRSxlQUFlO1lBQy9CLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLEVBQUU7WUFDaEIsWUFBWSxFQUFFLEVBQUU7U0FDakI7UUFFRCxXQUFXLEVBQUU7WUFDWCxLQUFLLEVBQUUsR0FBRztZQUNWLE1BQU0sRUFBRSxFQUFFO1lBQ1YsVUFBVSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1NBQzdCO1FBRUQsV0FBVyxFQUFFO1lBQ1gsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixRQUFRLEVBQUUsTUFBTTtZQUNoQixVQUFVLEVBQUUsRUFBRTtTQUNmO1FBRUQsaUJBQWlCLEVBQUU7WUFDakIsUUFBUSxFQUFFLEtBQUs7WUFDZixLQUFLLEVBQUUsS0FBSztTQUNiO1FBRUQsV0FBVyxFQUFFO1lBQ1gsSUFBSSxFQUFFLENBQUM7WUFDUCxXQUFXLEVBQUUsRUFBRTtZQUNmLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFFBQVEsRUFBRSxLQUFLO1lBQ2YsS0FBSyxFQUFFLEtBQUs7WUFFWixLQUFLLEVBQUU7Z0JBQ0wsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsTUFBTSxFQUFFLE1BQU07Z0JBQ2QsVUFBVSxFQUFFLEtBQUs7Z0JBQ2pCLFlBQVksRUFBRSxFQUFFO2FBQ2pCO1lBRUQsSUFBSSxFQUFFO2dCQUNKLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxFQUFFO2dCQUNWLFlBQVksRUFBRSxFQUFFO2FBQ2pCO1NBQ0Y7S0FDRjtJQUVELE1BQU0sRUFBRTtRQUNOLFFBQVEsRUFBRTtZQUNSLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDL0IsVUFBVSxFQUFFLEVBQUU7U0FDZjtRQUVELE9BQU8sRUFBRTtZQUNQLFdBQVcsRUFBRSxFQUFFO1lBQ2YsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztZQUNyQyxRQUFRLEVBQUUsTUFBTTtZQUNoQixVQUFVLEVBQUUsUUFBUTtZQUNwQixRQUFRLEVBQUUsUUFBUTtZQUNsQixZQUFZLEVBQUUsVUFBVTtZQUN4QixVQUFVLEVBQUUsUUFBUSxDQUFDLFdBQVc7WUFDaEMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1lBQzFCLFFBQVEsRUFBRSxFQUFFO1lBQ1osVUFBVSxFQUFFLE1BQU07WUFDbEIsTUFBTSxFQUFFLEVBQUU7WUFDVixhQUFhLEVBQUUsQ0FBQyxHQUFHO1NBQ3BCO1FBRUQsU0FBUyxFQUFFO1lBQ1QsS0FBSyxFQUFFLE1BQU07WUFDYixTQUFTLEVBQUUsTUFBTTtZQUNqQixVQUFVLEVBQUUsUUFBUTtZQUNwQixVQUFVLEVBQUUsQ0FBQztZQUNiLFdBQVcsRUFBRSxDQUFDO1lBQ2QsWUFBWSxFQUFFLENBQUM7WUFDZixZQUFZLEVBQUUsQ0FBQztZQUVmLFVBQVUsRUFBRTtnQkFDVixNQUFNLEVBQUUsQ0FBQztnQkFDVCxLQUFLLEVBQUUsS0FBSztnQkFDWixRQUFRLEVBQUUsR0FBRztnQkFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxXQUFXO2dCQUNyQyxZQUFZLEVBQUUsQ0FBQztnQkFDZixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUM5QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2FBQ3JDO1lBRUQsZUFBZSxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztnQkFDNUIsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsVUFBVSxFQUFFLE9BQU87Z0JBQ25CLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztnQkFDakMsZUFBZSxFQUFFLFFBQVEsQ0FBQyxPQUFPO2dCQUNqQyxrQkFBa0IsRUFBRSxZQUFZO2dCQUNoQyxjQUFjLEVBQUUsT0FBTzthQUN4QixDQUFDLEVBUjJCLENBUTNCO1lBRUYsU0FBUyxFQUFFO2dCQUNULEtBQUssRUFBRSxFQUFFO2dCQUNULE1BQU0sRUFBRSxFQUFFO2dCQUNWLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLEdBQUcsRUFBRSxLQUFLO2dCQUNWLElBQUksRUFBRSxLQUFLO2dCQUNYLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLFNBQVMsRUFBRSx3QkFBd0I7Z0JBQ25DLFNBQVMsRUFBRSxZQUFZO2dCQUN2QixVQUFVLEVBQUUsa0JBQWtCO2dCQUM5QixZQUFZLEVBQUUsd0JBQXdCO2dCQUN0QyxPQUFPLEVBQUUsRUFBRTthQUNaO1lBRUQsSUFBSSxFQUFFO2dCQUNKLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxHQUFHO2dCQUNYLE9BQU8sRUFBRSxFQUFFO2dCQUNYLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLFVBQVUsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDL0IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsYUFBYSxFQUFFLFFBQVE7Z0JBQ3ZCLGNBQWMsRUFBRSxZQUFZO2dCQUM1QixVQUFVLEVBQUUsWUFBWTtnQkFFeEIsS0FBSyxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztvQkFDbEIsS0FBSyxFQUFFLE1BQU07b0JBQ2IsTUFBTSxFQUFFLE1BQU07b0JBQ2QsR0FBRyxFQUFFLENBQUM7b0JBQ04sSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLENBQUMsQ0FBQztvQkFDVixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUNwQyxlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87b0JBQ2pDLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztvQkFDakMsa0JBQWtCLEVBQUUsZUFBZTtvQkFDbkMsY0FBYyxFQUFFLE9BQU87b0JBQ3ZCLE1BQU0sRUFBRSxXQUFXO29CQUNuQixTQUFTLEVBQUUsdUJBQXVCO29CQUNsQywwQkFBMEIsRUFBRSxRQUFRO29CQUNwQyxtQkFBbUIsRUFBRSxJQUFJO29CQUN6QixpQkFBaUIsRUFBRSxDQUFDLG9CQUFvQixFQUFFLGVBQWUsQ0FBQztvQkFDMUQsb0JBQW9CLEVBQUUsUUFBUTtvQkFDOUIsV0FBVyxFQUFFLElBQUk7aUJBQ2xCLENBQUMsRUFsQmlCLENBa0JqQjtnQkFFRixLQUFLLEVBQUU7b0JBQ0wsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsVUFBVTtvQkFDdEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7b0JBQ3JDLFFBQVEsRUFBRSxFQUFFO29CQUNaLFVBQVUsRUFBRSxNQUFNO29CQUNsQixTQUFTLEVBQUUsTUFBTTtvQkFDakIsUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLFlBQVksRUFBRSxDQUFDO2lCQUNoQjtnQkFFRCxXQUFXLEVBQUU7b0JBQ1gsUUFBUSxFQUFFLEVBQUU7b0JBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsVUFBVSxFQUFFLFVBQVU7b0JBQ3RCLFVBQVUsRUFBRSxNQUFNO29CQUNsQixTQUFTLEVBQUUsRUFBRTtvQkFDYixRQUFRLEVBQUUsUUFBUTtpQkFDbkI7Z0JBRUQsT0FBTyxFQUFFO29CQUNQLEtBQUssRUFBRSxNQUFNO29CQUNiLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7b0JBQzlCLFFBQVEsRUFBRSxNQUFNO29CQUNoQixNQUFNLEVBQUUsTUFBTTtvQkFDZCxTQUFTLEVBQUUsTUFBTTtvQkFDakIsUUFBUSxFQUFFLFFBQVE7aUJBQ25CO2dCQUVELE9BQU8sRUFBRTtvQkFDUCxRQUFRLEVBQUUsRUFBRTtvQkFDWixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsVUFBVSxFQUFFLFVBQVU7aUJBQ3ZCO2FBQ0Y7U0FDRjtLQUNGO0lBRUQsT0FBTyxFQUFFO1FBQ1AsT0FBTyxFQUFFO1lBQ1AsUUFBUSxFQUFFLEVBQUU7WUFDWixVQUFVLEVBQUUsTUFBTTtZQUNsQixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxXQUFXO1NBQ2pDO1FBRUQsU0FBUyxFQUFFO1lBQ1QsS0FBSyxFQUFFLE1BQU07WUFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLFFBQVEsRUFBRSxNQUFNO1lBQ2hCLGNBQWMsRUFBRSxlQUFlO1lBQy9CLFVBQVUsRUFBRSxFQUFFO1lBQ2QsV0FBVyxFQUFFLENBQUM7WUFDZCxZQUFZLEVBQUUsQ0FBQztZQUNmLFlBQVksRUFBRSxFQUFFO1lBRWhCLFVBQVUsRUFBRTtnQkFDVixZQUFZLEVBQUUsRUFBRTtnQkFDaEIsS0FBSyxFQUFFLGtCQUFrQjtnQkFDekIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztnQkFDckMsWUFBWSxFQUFFLENBQUM7Z0JBQ2YsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDOUIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTthQUNyQztZQUVELGVBQWUsRUFBRTtnQkFDZixZQUFZLEVBQUUsRUFBRTtnQkFDaEIsS0FBSyxFQUFFLGtCQUFrQjtnQkFDekIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztnQkFDckMsWUFBWSxFQUFFLENBQUM7Z0JBQ2YsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDOUIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTthQUNyQztZQUVELElBQUksRUFBRTtnQkFDSixLQUFLLEVBQUUsTUFBTTtnQkFDYixPQUFPLEVBQUUsRUFBRTtnQkFDWCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVU7Z0JBQy9CLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLGFBQWEsRUFBRSxRQUFRO2dCQUN2QixjQUFjLEVBQUUsWUFBWTtnQkFDNUIsVUFBVSxFQUFFLFlBQVk7Z0JBRXhCLFdBQVcsRUFBRTtvQkFDWCxRQUFRLEVBQUUsRUFBRTtvQkFDWixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsU0FBUyxFQUFFLEVBQUU7b0JBQ2IsUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7b0JBQ3JDLE9BQU8sRUFBRSxFQUFFO29CQUNYLFdBQVcsRUFBRSxFQUFFO29CQUNmLFVBQVUsRUFBRSxVQUFVO2lCQUN2QjtnQkFFRCxLQUFLLEVBQUU7b0JBQ0wsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsVUFBVTtvQkFDdEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7b0JBQ3JDLFFBQVEsRUFBRSxFQUFFO29CQUNaLFVBQVUsRUFBRSxNQUFNO29CQUNsQixTQUFTLEVBQUUsTUFBTTtvQkFDakIsUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLFlBQVksRUFBRSxDQUFDO2lCQUNoQjtnQkFFRCxPQUFPLEVBQUU7b0JBQ1AsUUFBUSxFQUFFLEVBQUU7b0JBQ1osVUFBVSxFQUFFLE1BQU07b0JBQ2xCLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7b0JBQ3JDLE9BQU8sRUFBRSxFQUFFO29CQUNYLFdBQVcsRUFBRSxFQUFFO29CQUNmLFVBQVUsRUFBRSxVQUFVO2lCQUN2QjthQUNGO1NBQ0Y7S0FDRjtDQUNNLENBQUMifQ==
// CONCATENATED MODULE: ./components/magazine/category/view-desktop.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};




var renderView = function (_a) {
    var title = _a.title, url = _a.url, list = _a.list, titleStyle = _a.titleStyle, _b = _a.type, type = _b === void 0 ? 'normal' : _b, _c = _a.size, size = _c === void 0 ? 'normal' : _c;
    var renderItem = function (item, index) {
        var linkProps = {
            key: "img-" + index,
            to: routing["Ma" /* ROUTING_MAGAZINE_DETAIL_PATH */] + "/" + item.slug,
            style: 'small' === size ? style.desktop.container.smallItemSlider : style.desktop.container.itemSlider
        };
        return (react["createElement"](react_router_dom["NavLink"], __assign({}, linkProps),
            react["createElement"]("div", { style: style.mobile.container.itemSliderPanel(item.cover_image.large_url) }, 'video' === type && react["createElement"]("div", { style: style.mobile.container.videoIcon })),
            react["createElement"]("div", { style: style.desktop.container.info },
                react["createElement"]("div", { style: style.desktop.container.info.title }, item.title),
                react["createElement"]("div", { style: style.desktop.container.info.description }, item.description))));
    };
    return list && Array.isArray(list) && list.length > 0
        ?
            react["createElement"]("div", { className: 'user-select-all' },
                react["createElement"](react_router_dom["NavLink"], { to: url, style: Object.assign({}, style.desktop.heading, titleStyle) }, title),
                react["createElement"]("div", { style: style.desktop },
                    react["createElement"]("div", { style: style.desktop.container }, list.map(renderItem))))
        : null;
};
/* harmony default export */ var view_desktop = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1kZXNrdG9wLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1kZXNrdG9wLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUUzQyxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUV0RixPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsSUFBTSxVQUFVLEdBQUcsVUFBQyxFQUFrRTtRQUFoRSxnQkFBSyxFQUFFLFlBQUcsRUFBRSxjQUFJLEVBQUUsMEJBQVUsRUFBRSxZQUFlLEVBQWYsb0NBQWUsRUFBRSxZQUFlLEVBQWYsb0NBQWU7SUFDbEYsSUFBTSxVQUFVLEdBQUcsVUFBQyxJQUFJLEVBQUUsS0FBSztRQUM3QixJQUFNLFNBQVMsR0FBRztZQUNoQixHQUFHLEVBQUUsU0FBTyxLQUFPO1lBQ25CLEVBQUUsRUFBSyw0QkFBNEIsU0FBSSxJQUFJLENBQUMsSUFBTTtZQUNsRCxLQUFLLEVBQUUsT0FBTyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxVQUFVO1NBQ3ZHLENBQUM7UUFFRixNQUFNLENBQUMsQ0FDTCxvQkFBQyxPQUFPLGVBQUssU0FBUztZQUNwQiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLElBQzNFLE9BQU8sS0FBSyxJQUFJLElBQUksNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBUSxDQUNyRTtZQUNOLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJO2dCQUN0Qyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBRyxJQUFJLENBQUMsS0FBSyxDQUFPO2dCQUNsRSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBRyxJQUFJLENBQUMsV0FBVyxDQUFPLENBQzFFLENBQ0UsQ0FDWCxDQUFDO0lBQ0osQ0FBQyxDQUFDO0lBRUYsTUFBTSxDQUFDLElBQUksSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQztRQUNuRCxDQUFDO1lBQ0QsNkJBQUssU0FBUyxFQUFFLGlCQUFpQjtnQkFDL0Isb0JBQUMsT0FBTyxJQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxJQUFHLEtBQUssQ0FBVztnQkFDaEcsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxPQUFPO29CQUN2Qiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLElBQ2hDLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQ2pCLENBQ0YsQ0FDRjtRQUNOLENBQUMsQ0FBQyxJQUFJLENBQUM7QUFDWCxDQUFDLENBQUE7QUFFRCxlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/category/view-mobile.tsx
var view_mobile_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};




var view_mobile_renderView = function (_a) {
    var title = _a.title, url = _a.url, list = _a.list, _b = _a.type, type = _b === void 0 ? 'normal' : _b;
    var route = routing["Ma" /* ROUTING_MAGAZINE_DETAIL_PATH */];
    return (react["createElement"]("magazine-category", { style: style.mobile.mainWrap },
        react["createElement"](react_router_dom["NavLink"], { to: url, style: style.mobile.heading }, title),
        react["createElement"]("div", { style: style.mobile },
            react["createElement"]("div", { style: style.mobile.container }, Array.isArray(list)
                && list.map(function (item, index) {
                    var linkProps = {
                        key: "img-" + index,
                        to: route + "/" + item.slug,
                        style: style.mobile.container.itemSlider
                    };
                    var tags = 0 === item.tags.length ? [item.description] : item.tags;
                    tags = tags.map(function (item) {
                        if ('#' === item.charAt(0)) {
                            item = item.slice(1).toLowerCase();
                        }
                        return item;
                    }).filter(function (index) { return index < 10; });
                    return (react["createElement"](react_router_dom["NavLink"], view_mobile_assign({}, linkProps),
                        react["createElement"]("div", { style: style.mobile.container.itemSliderPanel(item.cover_image.original_url) }, 'video' === type && (react["createElement"]("div", { style: style.mobile.container.videoIcon }))),
                        react["createElement"]("div", { style: style.mobile.container.info },
                            react["createElement"]("div", { style: style.mobile.container.info.title }, item.title),
                            react["createElement"]("div", { style: style.mobile.container.info.description }, item.description))));
                })))));
};
/* harmony default export */ var view_mobile = (view_mobile_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1tb2JpbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LW1vYmlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUMvQixPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFFM0MsT0FBTyxFQUFFLDRCQUE0QixFQUErQixNQUFNLHdDQUF3QyxDQUFDO0FBRW5ILE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixJQUFNLFVBQVUsR0FBRyxVQUFDLEVBQXFDO1FBQW5DLGdCQUFLLEVBQUUsWUFBRyxFQUFFLGNBQUksRUFBRSxZQUFlLEVBQWYsb0NBQWU7SUFDckQsSUFBTSxLQUFLLEdBQUcsNEJBQTRCLENBQUM7SUFFM0MsTUFBTSxDQUFDLENBQ0wsMkNBQW1CLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVE7UUFDN0Msb0JBQUMsT0FBTyxJQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxJQUFHLEtBQUssQ0FBVztRQUNoRSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU07WUFDdEIsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxJQUU5QixLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQzttQkFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLO29CQUN0QixJQUFNLFNBQVMsR0FBRzt3QkFDaEIsR0FBRyxFQUFFLFNBQU8sS0FBTzt3QkFDbkIsRUFBRSxFQUFLLEtBQUssU0FBSSxJQUFJLENBQUMsSUFBTTt3QkFDM0IsS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFVBQVU7cUJBQ3pDLENBQUM7b0JBRUYsSUFBSSxJQUFJLEdBQUcsQ0FBQyxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztvQkFFbkUsSUFBSSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFJO3dCQUNuQixFQUFFLENBQUMsQ0FBQyxHQUFHLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUE7d0JBQUMsQ0FBQzt3QkFDbEUsTUFBTSxDQUFDLElBQUksQ0FBQztvQkFDZCxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBQyxLQUFLLElBQUssT0FBQSxLQUFLLEdBQUcsRUFBRSxFQUFWLENBQVUsQ0FBQyxDQUFDO29CQUVqQyxNQUFNLENBQUMsQ0FDTCxvQkFBQyxPQUFPLGVBQUssU0FBUzt3QkFDcEIsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxJQUU3RSxPQUFPLEtBQUssSUFBSSxJQUFJLENBQ2xCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQVEsQ0FDckQsQ0FFQzt3QkFDTiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsSUFBSTs0QkFFckMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUcsSUFBSSxDQUFDLEtBQUssQ0FBTzs0QkFDakUsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLElBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBTyxDQUN6RSxDQUNFLENBQ1gsQ0FBQztnQkFDSixDQUFDLENBQUMsQ0FFQSxDQUNGLENBQ1ksQ0FDckIsQ0FBQTtBQUNILENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// EXTERNAL MODULE: ./components/magazine/video-item/index.tsx + 4 modules
var video_item = __webpack_require__(805);

// CONCATENATED MODULE: ./components/magazine/category/view-video.tsx





var mainBlockContent = function (_a) {
    var list = _a.list;
    var categoryVideoStyle = style.magazineCategory.categoryVideoContent;
    var mainVideo = Array.isArray(list) ? list[0] : null;
    var videoList = Array.isArray(list) ? list.slice(1, 5) : [];
    return (react["createElement"]("magazine-video-category", null, 0 !== list.length &&
        react["createElement"]("div", null,
            react["createElement"]("div", { style: categoryVideoStyle.mainTitle }, "Video"),
            react["createElement"]("div", { style: categoryVideoStyle.container },
                react["createElement"](react_router_dom["NavLink"], { to: routing["Ma" /* ROUTING_MAGAZINE_DETAIL_PATH */] + "/" + mainVideo.slug, style: categoryVideoStyle.largeVideoGroup.container },
                    react["createElement"]("div", { style: categoryVideoStyle.video(mainVideo && mainVideo.cover_image && mainVideo.cover_image.original_url || '') },
                        react["createElement"]("div", { style: categoryVideoStyle.videoIcon })),
                    react["createElement"]("div", { style: categoryVideoStyle.contentWrap },
                        react["createElement"]("div", { style: categoryVideoStyle.contentWrap.title }, mainVideo && mainVideo.title || ''),
                        react["createElement"]("div", { style: categoryVideoStyle.contentWrap.description }, mainVideo && '#' + mainVideo.description || ''))),
                react["createElement"]("div", { style: categoryVideoStyle.smallVideoGroup.container }, Array.isArray(videoList) && videoList.map(function (item, index) { return react["createElement"](video_item["a" /* default */], { item: item, key: "video-item-" + item.id, lastChild: index > 1 }); }))))));
};
var view_video_renderView = function (_a) {
    var title = _a.title, list = _a.list;
    return list && 0 !== list.length ? mainBlockContent({ list: list }) : null;
};
/* harmony default export */ var view_video = (view_video_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy12aWRlby5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXctdmlkZW8udHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUUzQyxPQUFPLFNBQVMsTUFBTSxlQUFlLENBQUM7QUFDdEMsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFFdEYsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRTVCLElBQU0sZ0JBQWdCLEdBQUcsVUFBQyxFQUFRO1FBQU4sY0FBSTtJQUM5QixJQUFNLGtCQUFrQixHQUFHLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQztJQUN2RSxJQUFNLFNBQVMsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUN2RCxJQUFNLFNBQVMsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBRTlELE1BQU0sQ0FBQyxDQUNMLHFEQUVJLENBQUMsS0FBSyxJQUFJLENBQUMsTUFBTTtRQUNqQjtZQUNFLDZCQUFLLEtBQUssRUFBRSxrQkFBa0IsQ0FBQyxTQUFTLFlBQWE7WUFDckQsNkJBQUssS0FBSyxFQUFFLGtCQUFrQixDQUFDLFNBQVM7Z0JBQ3RDLG9CQUFDLE9BQU8sSUFBQyxFQUFFLEVBQUssNEJBQTRCLFNBQUksU0FBUyxDQUFDLElBQU0sRUFBRSxLQUFLLEVBQUUsa0JBQWtCLENBQUMsZUFBZSxDQUFDLFNBQVM7b0JBQ25ILDZCQUFLLEtBQUssRUFBRSxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsU0FBUyxJQUFJLFNBQVMsQ0FBQyxXQUFXLElBQUksU0FBUyxDQUFDLFdBQVcsQ0FBQyxZQUFZLElBQUksRUFBRSxDQUFDO3dCQUNsSCw2QkFBSyxLQUFLLEVBQUUsa0JBQWtCLENBQUMsU0FBUyxHQUFRLENBQzVDO29CQUNOLDZCQUFLLEtBQUssRUFBRSxrQkFBa0IsQ0FBQyxXQUFXO3dCQUN4Qyw2QkFBSyxLQUFLLEVBQUUsa0JBQWtCLENBQUMsV0FBVyxDQUFDLEtBQUssSUFBRyxTQUFTLElBQUksU0FBUyxDQUFDLEtBQUssSUFBSSxFQUFFLENBQU87d0JBQzVGLDZCQUFLLEtBQUssRUFBRSxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsV0FBVyxJQUFHLFNBQVMsSUFBSSxHQUFHLEdBQUcsU0FBUyxDQUFDLFdBQVcsSUFBSSxFQUFFLENBQU8sQ0FDMUcsQ0FDRTtnQkFDViw2QkFBSyxLQUFLLEVBQUUsa0JBQWtCLENBQUMsZUFBZSxDQUFDLFNBQVMsSUFDckQsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBSSxFQUFFLEtBQUssSUFBSyxPQUFBLG9CQUFDLFNBQVMsSUFBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxnQkFBYyxJQUFJLENBQUMsRUFBSSxFQUFFLFNBQVMsRUFBRSxLQUFLLEdBQUcsQ0FBQyxHQUFJLEVBQTdFLENBQTZFLENBQUMsQ0FDdEksQ0FDRixDQUNGLENBRWdCLENBQzNCLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixJQUFNLFVBQVUsR0FBRyxVQUFDLEVBQWU7UUFBYixnQkFBSyxFQUFFLGNBQUk7SUFDL0IsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztBQUN2RSxDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/category/view.tsx




var view_renderView = function (props) {
    var _a = props, type = _a.type, list = _a.list, title = _a.title, url = _a.url, titleStyle = _a.titleStyle;
    var switchView = {};
    switch (type) {
        case 1:
            switchView = {
                MOBILE: function () { return view_mobile({ title: title, url: url, list: list }); },
                DESKTOP: function () { return view_desktop({ title: title, url: url, list: list, titleStyle: titleStyle }); }
            };
            return switchView[window.DEVICE_VERSION]();
        case 2:
            switchView = {
                MOBILE: function () { return view_mobile({ title: title, url: url, list: list }); },
                DESKTOP: function () { return view_desktop({ title: title, url: url, list: list, titleStyle: titleStyle }); }
            };
            return switchView[window.DEVICE_VERSION]();
        case 3:
            switchView = {
                MOBILE: function () { return view_mobile({ title: title, url: url, list: list }); },
                DESKTOP: function () { return view_desktop({ title: title, url: url, list: list, titleStyle: titleStyle }); }
            };
            return switchView[window.DEVICE_VERSION]();
        case 4:
            switchView = {
                MOBILE: function () { return view_mobile({ title: title, url: url, list: list }); },
                DESKTOP: function () { return view_desktop({ title: title, url: url, list: list, titleStyle: titleStyle }); }
            };
            return switchView[window.DEVICE_VERSION]();
        case 5:
            switchView = {
                MOBILE: function () { return view_mobile({ title: title, url: url, list: list }); },
                DESKTOP: function () { return view_desktop({ title: title, url: url, list: list, titleStyle: titleStyle }); }
            };
            return switchView[window.DEVICE_VERSION]();
        case magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].VIDEO.type:
            switchView = {
                MOBILE: function () { return view_mobile({ title: magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].VIDEO.title, url: '', list: list, type: 'video' }); },
                DESKTOP: function () { return view_video({ title: magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].VIDEO.title, list: list }); }
            };
            return switchView[window.DEVICE_VERSION]();
        case magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].TRENDING.type:
            return view_desktop({ title: magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].TRENDING.title, url: '', list: list, size: 'small', titleStyle: titleStyle });
        default:
            switchView = {
                MOBILE: function () { return view_mobile({ title: title, url: url, list: list }); },
                DESKTOP: function () { return view_desktop({ title: title, url: url, list: list, titleStyle: titleStyle }); }
            };
            return switchView[window.DEVICE_VERSION]();
    }
};
/* harmony default export */ var view = (view_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBRzFGLE9BQU8sYUFBYSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sWUFBWSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLG1CQUFtQixNQUFNLGNBQWMsQ0FBQztBQUUvQyxJQUFNLFVBQVUsR0FBRyxVQUFDLEtBQWE7SUFDekIsSUFBQSxVQUF3RCxFQUF0RCxjQUFJLEVBQUUsY0FBSSxFQUFFLGdCQUFLLEVBQUUsWUFBRyxFQUFFLDBCQUFVLENBQXFCO0lBQy9ELElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQztJQUVwQixNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ2IsS0FBSyxDQUFDO1lBQ0osVUFBVSxHQUFHO2dCQUNYLE1BQU0sRUFBRSxjQUFNLE9BQUEsWUFBWSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsR0FBRyxLQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxFQUFsQyxDQUFrQztnQkFDaEQsT0FBTyxFQUFFLGNBQU0sT0FBQSxhQUFhLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxHQUFHLEtBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxVQUFVLFlBQUEsRUFBRSxDQUFDLEVBQS9DLENBQStDO2FBQy9ELENBQUM7WUFDRixNQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDO1FBRTdDLEtBQUssQ0FBQztZQUNKLFVBQVUsR0FBRztnQkFDWCxNQUFNLEVBQUUsY0FBTSxPQUFBLFlBQVksQ0FBQyxFQUFFLEtBQUssT0FBQSxFQUFFLEdBQUcsS0FBQSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsRUFBbEMsQ0FBa0M7Z0JBQ2hELE9BQU8sRUFBRSxjQUFNLE9BQUEsYUFBYSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsR0FBRyxLQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsVUFBVSxZQUFBLEVBQUUsQ0FBQyxFQUEvQyxDQUErQzthQUMvRCxDQUFDO1lBQ0YsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQztRQUU3QyxLQUFLLENBQUM7WUFDSixVQUFVLEdBQUc7Z0JBQ1gsTUFBTSxFQUFFLGNBQU0sT0FBQSxZQUFZLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxHQUFHLEtBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDLEVBQWxDLENBQWtDO2dCQUNoRCxPQUFPLEVBQUUsY0FBTSxPQUFBLGFBQWEsQ0FBQyxFQUFFLEtBQUssT0FBQSxFQUFFLEdBQUcsS0FBQSxFQUFFLElBQUksTUFBQSxFQUFFLFVBQVUsWUFBQSxFQUFFLENBQUMsRUFBL0MsQ0FBK0M7YUFDL0QsQ0FBQztZQUNGLE1BQU0sQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUM7UUFFN0MsS0FBSyxDQUFDO1lBQ0osVUFBVSxHQUFHO2dCQUNYLE1BQU0sRUFBRSxjQUFNLE9BQUEsWUFBWSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsR0FBRyxLQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxFQUFsQyxDQUFrQztnQkFDaEQsT0FBTyxFQUFFLGNBQU0sT0FBQSxhQUFhLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxHQUFHLEtBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxVQUFVLFlBQUEsRUFBRSxDQUFDLEVBQS9DLENBQStDO2FBQy9ELENBQUM7WUFDRixNQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDO1FBRTdDLEtBQUssQ0FBQztZQUNKLFVBQVUsR0FBRztnQkFDWCxNQUFNLEVBQUUsY0FBTSxPQUFBLFlBQVksQ0FBQyxFQUFFLEtBQUssT0FBQSxFQUFFLEdBQUcsS0FBQSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsRUFBbEMsQ0FBa0M7Z0JBQ2hELE9BQU8sRUFBRSxjQUFNLE9BQUEsYUFBYSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsR0FBRyxLQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsVUFBVSxZQUFBLEVBQUUsQ0FBQyxFQUEvQyxDQUErQzthQUMvRCxDQUFDO1lBQ0YsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQztRQUU3QyxLQUFLLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxJQUFJO1lBQ3BDLFVBQVUsR0FBRztnQkFDWCxNQUFNLEVBQUUsY0FBTSxPQUFBLFlBQVksQ0FBQyxFQUFFLEtBQUssRUFBRSxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQXpGLENBQXlGO2dCQUN2RyxPQUFPLEVBQUUsY0FBTSxPQUFBLG1CQUFtQixDQUFDLEVBQUUsS0FBSyxFQUFFLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxFQUF4RSxDQUF3RTthQUN4RixDQUFDO1lBQ0YsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQztRQUU3QyxLQUFLLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxJQUFJO1lBQ3ZDLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLElBQUksTUFBQSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsVUFBVSxZQUFBLEVBQUUsQ0FBQyxDQUFDO1FBRW5IO1lBQ0UsVUFBVSxHQUFHO2dCQUNYLE1BQU0sRUFBRSxjQUFNLE9BQUEsWUFBWSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsR0FBRyxLQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxFQUFsQyxDQUFrQztnQkFDaEQsT0FBTyxFQUFFLGNBQU0sT0FBQSxhQUFhLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxHQUFHLEtBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxVQUFVLFlBQUEsRUFBRSxDQUFDLEVBQS9DLENBQStDO2FBQy9ELENBQUM7WUFFRixNQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDO0lBQy9DLENBQUM7QUFDSCxDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/category/initialize.tsx
var DEFAULT_PROPS = {
    type: 0,
    list: [],
    title: '',
    url: '',
    titleStyle: {}
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsQ0FBQztJQUNQLElBQUksRUFBRSxFQUFFO0lBQ1IsS0FBSyxFQUFFLEVBQUU7SUFDVCxHQUFHLEVBQUUsRUFBRTtJQUNQLFVBQVUsRUFBRSxFQUFFO0NBQ0wsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/category/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_MagazineCategory = /** @class */ (function (_super) {
    __extends(MagazineCategory, _super);
    function MagazineCategory(props) {
        return _super.call(this, props) || this;
    }
    MagazineCategory.prototype.render = function () {
        return view(this.props);
    };
    ;
    MagazineCategory.defaultProps = DEFAULT_PROPS;
    MagazineCategory = __decorate([
        radium
    ], MagazineCategory);
    return MagazineCategory;
}(react["PureComponent"]));
/* harmony default export */ var component = (component_MagazineCategory);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFDaEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUk3QztJQUErQixvQ0FBNkI7SUFFMUQsMEJBQVksS0FBYTtlQUN2QixrQkFBTSxLQUFLLENBQUM7SUFDZCxDQUFDO0lBRUQsaUNBQU0sR0FBTjtRQUNFLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFBQSxDQUFDO0lBUEssNkJBQVksR0FBVyxhQUFhLENBQUM7SUFEeEMsZ0JBQWdCO1FBRHJCLE1BQU07T0FDRCxnQkFBZ0IsQ0FTckI7SUFBRCx1QkFBQztDQUFBLEFBVEQsQ0FBK0IsYUFBYSxHQVMzQztBQUVELGVBQWUsZ0JBQWdCLENBQUMifQ==
// CONCATENATED MODULE: ./components/magazine/category/index.tsx

/* harmony default export */ var category = __webpack_exports__["a"] = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxnQkFBZ0IsTUFBTSxhQUFhLENBQUM7QUFDM0MsZUFBZSxnQkFBZ0IsQ0FBQyJ9

/***/ }),

/***/ 805:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./components/magazine/video-item/style.tsx


var generateWidthByColumn = function (column) {
    switch (column) {
        case 2: return 'calc(50% - 10px)';
        case 3: return 'calc(33.3% - 10px)';
        case 4: return 'calc(25% - 15px)';
        case 5: return 'calc(20% - 10px)';
    }
};
/* harmony default export */ var video_item_style = ({
    video: function (imgUrl) { return ({
        width: '100%',
        paddingTop: '62.5%',
        position: variable["position"].relative,
        backgroundImage: "url(" + imgUrl + ")",
        backgroundColor: variable["colorF7"],
        backgroundPosition: 'top center',
        backgroundSize: 'cover',
    }); },
    videoIcon: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                width: 40,
                left: '60%',
                transform: 'translate(-50%, -50%)',
                borderTop: "15px solid " + variable["colorTransparent"],
                borderLeft: "20px solid " + variable["colorWhite"],
                borderBottom: "15px solid " + variable["colorTransparent"],
                borderRight: "15px solid " + variable["colorTransparent"],
            }],
        DESKTOP: [{
                width: 70,
                height: 70,
                left: '55%',
                transform: 'translate(-50%, -50%)',
                borderTop: "35px solid " + variable["colorTransparent"],
                borderLeft: "51px solid " + variable["colorWhite"],
                borderBottom: "35px solid " + variable["colorTransparent"],
            }],
        GENERAL: [{
                position: variable["position"].absolute,
                boxSizing: 'border-box',
                opacity: 0.8,
                top: '50%'
            }]
    }),
    contentWrap: {
        width: '100%',
        paddingTop: 12,
        paddingRight: 12,
        paddingBottom: 12,
        paddingLeft: 12,
        position: variable["position"].relative,
        backgroundColor: variable["colorWhite"],
        display: variable["display"].flex,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        title: {
            color: variable["colorBlack"],
            whiteSpace: 'pre-wrap',
            fontFamily: variable["fontAvenirMedium"],
            fontSize: 16,
            lineHeight: '24px',
            maxHeight: 48,
            minHeight: 48,
            overflow: 'hidden',
            marginBottom: 5,
        },
        description: {
            fontSize: 12,
            lineHeight: '18px',
            color: variable["colorBlack"],
            fontFamily: variable["fontAvenirMedium"],
            opacity: 0.7,
            marginRight: 10,
            whiteSpace: 'pre-wrap',
        }
    },
    videoContainer: function (column) { return Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                marginBottom: 10
            }],
        DESKTOP: [{
                marginBottom: 20
            }],
        GENERAL: [{
                borderRadius: 5,
                overflow: 'hidden',
                boxShadow: variable["shadowBlur"],
                display: variable["display"].block,
                position: variable["position"].relative,
                width: generateWidthByColumn(column)
            }]
    }); },
    lastChild: {
        marginBottom: 0
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFekQsSUFBTSxxQkFBcUIsR0FBRyxVQUFDLE1BQU07SUFDbkMsTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUNmLEtBQUssQ0FBQyxFQUFFLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQztRQUNsQyxLQUFLLENBQUMsRUFBRSxNQUFNLENBQUMsb0JBQW9CLENBQUM7UUFDcEMsS0FBSyxDQUFDLEVBQUUsTUFBTSxDQUFDLGtCQUFrQixDQUFDO1FBQ2xDLEtBQUssQ0FBQyxFQUFFLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQztJQUNwQyxDQUFDO0FBQ0gsQ0FBQyxDQUFDO0FBRUYsZUFBZTtJQUViLEtBQUssRUFBRSxVQUFDLE1BQU0sSUFBSyxPQUFBLENBQUM7UUFDbEIsS0FBSyxFQUFFLE1BQU07UUFDYixVQUFVLEVBQUUsT0FBTztRQUNuQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1FBQ3BDLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztRQUNqQyxlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87UUFDakMsa0JBQWtCLEVBQUUsWUFBWTtRQUNoQyxjQUFjLEVBQUUsT0FBTztLQUN4QixDQUFDLEVBUmlCLENBUWpCO0lBRUYsU0FBUyxFQUFFLFlBQVksQ0FBQztRQUN0QixNQUFNLEVBQUUsQ0FBQztnQkFDUCxLQUFLLEVBQUUsRUFBRTtnQkFDVCxJQUFJLEVBQUUsS0FBSztnQkFDWCxTQUFTLEVBQUUsdUJBQXVCO2dCQUNsQyxTQUFTLEVBQUUsZ0JBQWMsUUFBUSxDQUFDLGdCQUFrQjtnQkFDcEQsVUFBVSxFQUFFLGdCQUFjLFFBQVEsQ0FBQyxVQUFZO2dCQUMvQyxZQUFZLEVBQUUsZ0JBQWMsUUFBUSxDQUFDLGdCQUFrQjtnQkFDdkQsV0FBVyxFQUFFLGdCQUFjLFFBQVEsQ0FBQyxnQkFBa0I7YUFDdkQsQ0FBQztRQUVGLE9BQU8sRUFBRSxDQUFDO2dCQUNSLEtBQUssRUFBRSxFQUFFO2dCQUNULE1BQU0sRUFBRSxFQUFFO2dCQUNWLElBQUksRUFBRSxLQUFLO2dCQUNYLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLFNBQVMsRUFBRSxnQkFBYyxRQUFRLENBQUMsZ0JBQWtCO2dCQUNwRCxVQUFVLEVBQUUsZ0JBQWMsUUFBUSxDQUFDLFVBQVk7Z0JBQy9DLFlBQVksRUFBRSxnQkFBYyxRQUFRLENBQUMsZ0JBQWtCO2FBQ3hELENBQUM7UUFFRixPQUFPLEVBQUUsQ0FBQztnQkFDUixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxTQUFTLEVBQUUsWUFBWTtnQkFDdkIsT0FBTyxFQUFFLEdBQUc7Z0JBQ1osR0FBRyxFQUFFLEtBQUs7YUFDWCxDQUFDO0tBQ0gsQ0FBQztJQUVGLFdBQVcsRUFBRTtRQUNYLEtBQUssRUFBRSxNQUFNO1FBQ2IsVUFBVSxFQUFFLEVBQUU7UUFDZCxZQUFZLEVBQUUsRUFBRTtRQUNoQixhQUFhLEVBQUUsRUFBRTtRQUNqQixXQUFXLEVBQUUsRUFBRTtRQUNmLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7UUFDcEMsZUFBZSxFQUFFLFFBQVEsQ0FBQyxVQUFVO1FBQ3BDLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7UUFDOUIsYUFBYSxFQUFFLFFBQVE7UUFDdkIsY0FBYyxFQUFFLFlBQVk7UUFDNUIsVUFBVSxFQUFFLFlBQVk7UUFFeEIsS0FBSyxFQUFFO1lBQ0wsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1lBQzFCLFVBQVUsRUFBRSxVQUFVO1lBQ3RCLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO1lBQ3JDLFFBQVEsRUFBRSxFQUFFO1lBQ1osVUFBVSxFQUFFLE1BQU07WUFDbEIsU0FBUyxFQUFFLEVBQUU7WUFDYixTQUFTLEVBQUUsRUFBRTtZQUNiLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLFlBQVksRUFBRSxDQUFDO1NBQ2hCO1FBRUQsV0FBVyxFQUFFO1lBQ1gsUUFBUSxFQUFFLEVBQUU7WUFDWixVQUFVLEVBQUUsTUFBTTtZQUNsQixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7WUFDckMsT0FBTyxFQUFFLEdBQUc7WUFDWixXQUFXLEVBQUUsRUFBRTtZQUNmLFVBQVUsRUFBRSxVQUFVO1NBQ3ZCO0tBQ0Y7SUFFRCxjQUFjLEVBQUUsVUFBQyxNQUFNLElBQUssT0FBQSxZQUFZLENBQUM7UUFDdkMsTUFBTSxFQUFFLENBQUM7Z0JBQ1AsWUFBWSxFQUFFLEVBQUU7YUFDakIsQ0FBQztRQUVGLE9BQU8sRUFBRSxDQUFDO2dCQUNSLFlBQVksRUFBRSxFQUFFO2FBQ2pCLENBQUM7UUFFRixPQUFPLEVBQUUsQ0FBQztnQkFDUixZQUFZLEVBQUUsQ0FBQztnQkFDZixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUM5QixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO2dCQUMvQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxLQUFLLEVBQUUscUJBQXFCLENBQUMsTUFBTSxDQUFDO2FBQ3JDLENBQUM7S0FDSCxDQUFDLEVBakIwQixDQWlCMUI7SUFFRixTQUFTLEVBQUU7UUFDVCxZQUFZLEVBQUUsQ0FBQztLQUNoQjtDQUNNLENBQUMifQ==
// CONCATENATED MODULE: ./components/magazine/video-item/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};




var renderView = function (props) {
    var _a = props, item = _a.item, column = _a.column, lastChild = _a.lastChild, style = _a.style;
    var linkProps = {
        key: "small-video-" + item.id,
        to: routing["Ma" /* ROUTING_MAGAZINE_DETAIL_PATH */] + "/" + item.slug,
        style: Object.assign({}, video_item_style.videoContainer(column), style, lastChild && video_item_style.lastChild)
    };
    return (react["createElement"](react_router_dom["NavLink"], __assign({}, linkProps),
        react["createElement"]("div", { style: video_item_style.video(item.cover_image.original_url) },
            react["createElement"]("div", { style: video_item_style.videoIcon })),
        react["createElement"]("div", { style: video_item_style.contentWrap },
            react["createElement"]("div", { style: video_item_style.contentWrap.title }, item.title || ''))));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBR3RGLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixJQUFNLFVBQVUsR0FBRyxVQUFDLEtBQUs7SUFDakIsSUFBQSxVQUFvRCxFQUFsRCxjQUFJLEVBQUUsa0JBQU0sRUFBRSx3QkFBUyxFQUFFLGdCQUFLLENBQXFCO0lBRTNELElBQU0sU0FBUyxHQUFHO1FBQ2hCLEdBQUcsRUFBRSxpQkFBZSxJQUFJLENBQUMsRUFBSTtRQUM3QixFQUFFLEVBQUssNEJBQTRCLFNBQUksSUFBSSxDQUFDLElBQU07UUFDbEQsS0FBSyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEVBQUUsS0FBSyxFQUFFLFNBQVMsSUFBSSxLQUFLLENBQUMsU0FBUyxDQUFDO0tBQzVGLENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCxvQkFBQyxPQUFPLGVBQUssU0FBUztRQUNwQiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQztZQUNwRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFNBQVMsR0FBUSxDQUMvQjtRQUNOLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVztZQUMzQiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxLQUFLLElBQUcsSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQU8sQ0FDekQsQ0FDRSxDQUNYLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/video-item/initialize.tsx
var DEFAULT_PROPS = {
    item: {},
    style: {},
    column: 2,
    lastChild: false
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLEtBQUssRUFBRSxFQUFFO0lBQ1QsTUFBTSxFQUFFLENBQUM7SUFDVCxTQUFTLEVBQUUsS0FBSztDQUNqQixDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/video-item/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_VideoItem = /** @class */ (function (_super) {
    __extends(VideoItem, _super);
    function VideoItem(props) {
        return _super.call(this, props) || this;
    }
    VideoItem.prototype.render = function () {
        return view(this.props);
    };
    ;
    VideoItem.defaultProps = DEFAULT_PROPS;
    VideoItem = __decorate([
        radium
    ], VideoItem);
    return VideoItem;
}(react["PureComponent"]));
/* harmony default export */ var component = (component_VideoItem);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFFaEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUc3QztJQUF3Qiw2QkFBNkI7SUFFbkQsbUJBQVksS0FBYTtlQUN2QixrQkFBTSxLQUFLLENBQUM7SUFDZCxDQUFDO0lBRUQsMEJBQU0sR0FBTjtRQUNFLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFBQSxDQUFDO0lBUEssc0JBQVksR0FBRyxhQUFhLENBQUM7SUFEaEMsU0FBUztRQURkLE1BQU07T0FDRCxTQUFTLENBU2Q7SUFBRCxnQkFBQztDQUFBLEFBVEQsQ0FBd0IsYUFBYSxHQVNwQztBQUVELGVBQWUsU0FBUyxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/video-item/index.tsx

/* harmony default export */ var video_item = __webpack_exports__["a"] = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxTQUFTLE1BQU0sYUFBYSxDQUFDO0FBQ3BDLGVBQWUsU0FBUyxDQUFDIn0=

/***/ }),

/***/ 918:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./action/magazine.ts + 1 modules
var action_magazine = __webpack_require__(761);

// EXTERNAL MODULE: ./action/meta.ts
var meta = __webpack_require__(754);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// EXTERNAL MODULE: ./utils/encode.ts
var encode = __webpack_require__(7);

// EXTERNAL MODULE: ../node_modules/react-on-screen/lib/index.js
var lib = __webpack_require__(758);
var lib_default = /*#__PURE__*/__webpack_require__.n(lib);

// EXTERNAL MODULE: ./components/ui/loading/index.tsx + 4 modules
var loading = __webpack_require__(747);

// EXTERNAL MODULE: ./constants/application/magazine.ts
var application_magazine = __webpack_require__(170);

// EXTERNAL MODULE: ./constants/application/magazine-category.ts
var magazine_category = __webpack_require__(790);

// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./components/product/slider/index.tsx + 6 modules
var slider = __webpack_require__(770);

// EXTERNAL MODULE: ./components/ui/loading-placeholder/index.tsx + 4 modules
var loading_placeholder = __webpack_require__(750);

// EXTERNAL MODULE: ./components/magazine/image-slider/index.tsx + 11 modules
var image_slider = __webpack_require__(765);

// EXTERNAL MODULE: ./utils/html.tsx
var html = __webpack_require__(757);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./components/magazine/blog-detail/style.tsx


var INLINE_STYLE = {
    '.magazine-content-detail': {
        paddingLeft: 0,
        paddingRight: 0,
        maxWidth: 800,
        margin: '0 auto',
    },
    '.magazine-content-detail p, .magazine-content-detail img, .magazine-content-detail div': {
        marginBottom: "15px !important"
    },
    '.magazine-content-detail, .magazine-content-detail *': {
        fontSize: '18px',
        lineHeight: '28px !important',
        color: "" + variable["colorBlack09"],
        fontFamily: variable["fontTrirong"] + " !important",
    },
    '.magazine-content-detail b, .magazine-content-detail strong': {
        fontWeight: 600,
        color: variable["colorBlack"]
    },
    '.magazine-content-detail span': {
        display: variable["display"].inlineBlock,
        fontSize: 18,
        lineHeight: '28px !important',
        color: "" + variable["colorBlack09"],
        textAlign: 'justify !important',
        textIndent: 0
    },
    '.magazine-content-detail a': {
        fontStyle: 'italic',
        textDecoration: 'underline',
    },
    '.magazine-content-detail p, .magazine-content-detail em, .magazine-content-detail li': {
        maxWidth: '100%',
        display: variable["display"].inlineBlock,
        fontSize: '18px',
        lineHeight: '28px !important',
        color: "" + variable["colorBlack09"],
        textAlign: 'justify !important',
        textIndent: 0
    },
    '.magazine-content-detail img': {
        marginBottom: "0 !important",
        display: variable["display"].block,
        width: '100% !important',
        marginLeft: 0,
        height: 'auto !important',
        padding: '20px 0'
    },
    '.magazine-content-detail img.full-width': {
        width: '100vw !important',
        position: variable["position"].relative,
        marginLeft: 'calc((-50vw) + 455px - 30px)',
    },
    '.magazine-content-detail.magazine-content-detail-mobile img': {
        width: '100vw !important',
        marginLeft: -20
    },
    '.magazine-content-detail a,.magazine-content-detail a > strong': {
        textAlign: 'justify !important',
    },
    '.magazine-content-detail h1, .magazine-content-detail h2, .magazine-content-detail h3, .magazine-content-detail h4, .magazine-content-detail h5, .magazine-content-detail h6': {
        fontSize: '24px !important',
        textTransform: 'uppercase !important',
        margin: '10px 0',
    },
    '.magazine-content-detail iframe': {
        width: '100%',
        height: 560,
        marginBottom: 10
    },
};
/* harmony default export */ var blog_detail_style = ({
    headerWrap: {
        marginBottom: 20,
        imgWrap: {
            display: variable["display"].block,
            height: 'auto',
            marginBottom: 20,
            width: 'calc(100%)',
            position: variable["position"].relative,
            //left: '50%',
            marginLeft: 0
        },
        viewGroup: {
            container: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{ paddingLeft: 10, paddingRight: 10 }],
                DESKTOP: [{}],
                GENERAL: [{
                        display: variable["display"].flex,
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        marginBottom: 10,
                    }]
            }),
            iconWrap: {
                display: variable["display"].flex,
                icon: {
                    marginLeft: 10,
                    width: 20,
                    height: 20,
                    color: variable["colorBlack"]
                },
                iconEmail: {
                    marginLeft: 10,
                    width: 30,
                    height: 20,
                    color: variable["colorBlack"]
                }
            }
        },
        blogTitle: {
            display: variable["display"].block,
            width: '100%',
            maxWidth: 800,
            margin: '0 auto',
            textAlign: 'center',
            fontSize: 40,
            lineHeight: '42px',
            padding: '10px 40px 40px',
            fontFamily: variable["fontPlayFairRegular"],
            fontWeight: 600,
        },
        desc: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{ paddingLeft: 10, paddingRight: 10, fontSize: 18 }],
            DESKTOP: [{ fontSize: 20, maxWidth: 800, margin: '0 auto 30px', }],
            GENERAL: [{
                    fontStyle: 'italic',
                    color: variable["colorBlack"],
                    textAlign: 'justify',
                    fontFamily: variable["fontTrirong"],
                }]
        }),
    },
    contentWrap: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{ padding: '0 20px' }],
        DESKTOP: [],
        GENERAL: [{ marginBottom: 20 }]
    }),
    tagWrap: {
        container: {
            paddingLeft: 0,
            paddingRight: 0,
            maxWidth: 800,
            margin: '0 auto 20px',
            display: variable["display"].flex,
            flexWrap: 'wrap'
        },
        header: {
            height: 30,
            lineHeight: '30px',
            fontFamily: variable["fontTrirong"],
            display: variable["display"].block,
            fontSize: 20,
            color: variable["colorBlack08"],
            marginRight: 20,
        },
        tag: {
            display: variable["display"].block,
            marginBottom: 10,
            marginRight: 10,
            borderRadius: 4,
            background: variable["colorF0"],
            color: variable["colorBlack"],
            fontSize: 14,
            lineHeight: '30px',
            paddingLeft: 10,
            paddingRight: 10
        }
    },
    customStyleLoading: {
        height: 80
    },
    placeholder: {
        width: '100%',
        paddingTop: 20,
        mainImg: {
            width: 1130,
            height: 635,
            marginBottom: 20
        },
        mainImgMobile: {
            width: '100%',
            height: 240,
            marginBottom: 20
        },
        iconGroup: {
            display: variable["display"].flex,
            justifyContent: 'space-between',
            alignItems: 'center',
            marginBottom: 10,
            dateGroup: {
                height: 20,
                width: '20%'
            },
            socialGroup: {
                height: 20,
                width: '20%'
            }
        },
        title: {
            width: '100%',
            height: 70,
            marginBottom: 20,
        },
        content: {
            height: 30,
            width: '100%',
            marginBottom: 10
        }
    },
    mobile: {
        coverImage: {
            width: '100%',
            height: 'auto',
            display: variable["display"].block
        },
        coverInfo: {
            position: variable["position"].relative,
            background: variable["colorWhite"],
            blurImage: {
                position: variable["position"].absolute,
                width: '100%',
                height: '100%',
                backgroundSize: 'cover',
                backgroundPosition: 'bottom center',
                opacity: .5,
                transform: "scaleY(-1)"
            },
            info: {
                position: variable["position"].relative,
                textShadow: "0 1px 2px " + variable["colorWhite05"],
            },
            title: {
                padding: '15px 20px 5px',
                fontSize: 22,
                lineHeight: '30px',
                fontFamily: variable["fontPlayFairRegular"],
                fontWeight: 600,
            },
            description: {
                fontSize: 16,
                lineHeight: '22px',
                padding: '10px 0 0',
                margin: '0 20px 20px',
                textAlign: 'justify',
                fontFamily: variable["fontTrirong"],
                fontStyle: 'italic'
            },
        },
        tagWrap: {
            container: {
                paddingLeft: 20,
                paddingRight: 20,
                marginBottom: 20,
                display: variable["display"].flex,
                flexWrap: 'wrap'
            },
            header: {
                height: 30,
                lineHeight: '30px',
                fontFamily: variable["fontTrirong"],
                display: variable["display"].block,
                fontSize: 20,
                color: variable["colorBlack08"],
                marginRight: 20,
            },
            tag: {
                display: variable["display"].block,
                marginBottom: 10,
                marginRight: 10,
                borderRadius: 4,
                background: variable["colorF0"],
                color: variable["colorBlack"],
                fontSize: 14,
                lineHeight: '30px',
                paddingLeft: 10,
                paddingRight: 10
            }
        },
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFekQsTUFBTSxDQUFDLElBQU0sWUFBWSxHQUFHO0lBQzFCLDBCQUEwQixFQUFFO1FBQzFCLFdBQVcsRUFBRSxDQUFDO1FBQ2QsWUFBWSxFQUFFLENBQUM7UUFDZixRQUFRLEVBQUUsR0FBRztRQUNiLE1BQU0sRUFBRSxRQUFRO0tBQ2pCO0lBQ0Qsd0ZBQXdGLEVBQUU7UUFDeEYsWUFBWSxFQUFFLGlCQUFpQjtLQUNoQztJQUVELHNEQUFzRCxFQUFFO1FBQ3RELFFBQVEsRUFBRSxNQUFNO1FBQ2hCLFVBQVUsRUFBRSxpQkFBaUI7UUFDN0IsS0FBSyxFQUFFLEtBQUcsUUFBUSxDQUFDLFlBQWM7UUFDakMsVUFBVSxFQUFLLFFBQVEsQ0FBQyxXQUFXLGdCQUFhO0tBQ2pEO0lBRUQsNkRBQTZELEVBQUU7UUFDN0QsVUFBVSxFQUFFLEdBQUc7UUFDZixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7S0FDM0I7SUFFRCwrQkFBK0IsRUFBRTtRQUMvQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxXQUFXO1FBQ3JDLFFBQVEsRUFBRSxFQUFFO1FBQ1osVUFBVSxFQUFFLGlCQUFpQjtRQUM3QixLQUFLLEVBQUUsS0FBRyxRQUFRLENBQUMsWUFBYztRQUNqQyxTQUFTLEVBQUUsb0JBQW9CO1FBQy9CLFVBQVUsRUFBRSxDQUFDO0tBQ2Q7SUFDRCw0QkFBNEIsRUFBRTtRQUM1QixTQUFTLEVBQUUsUUFBUTtRQUNuQixjQUFjLEVBQUUsV0FBVztLQUM1QjtJQUNELHNGQUFzRixFQUFFO1FBQ3RGLFFBQVEsRUFBRSxNQUFNO1FBQ2hCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLFdBQVc7UUFDckMsUUFBUSxFQUFFLE1BQU07UUFDaEIsVUFBVSxFQUFFLGlCQUFpQjtRQUM3QixLQUFLLEVBQUUsS0FBRyxRQUFRLENBQUMsWUFBYztRQUNqQyxTQUFTLEVBQUUsb0JBQW9CO1FBQy9CLFVBQVUsRUFBRSxDQUFDO0tBQ2Q7SUFFRCw4QkFBOEIsRUFBRTtRQUM5QixZQUFZLEVBQUUsY0FBYztRQUM1QixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1FBQy9CLEtBQUssRUFBRSxpQkFBaUI7UUFDeEIsVUFBVSxFQUFFLENBQUM7UUFDYixNQUFNLEVBQUUsaUJBQWlCO1FBQ3pCLE9BQU8sRUFBRSxRQUFRO0tBQ2xCO0lBRUQseUNBQXlDLEVBQUU7UUFDekMsS0FBSyxFQUFFLGtCQUFrQjtRQUN6QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1FBQ3BDLFVBQVUsRUFBRSw4QkFBOEI7S0FDM0M7SUFFRCw2REFBNkQsRUFBRTtRQUM3RCxLQUFLLEVBQUUsa0JBQWtCO1FBQ3pCLFVBQVUsRUFBRSxDQUFDLEVBQUU7S0FDaEI7SUFFRCxnRUFBZ0UsRUFBRTtRQUNoRSxTQUFTLEVBQUUsb0JBQW9CO0tBQ2hDO0lBRUQsOEtBQThLLEVBQUU7UUFDOUssUUFBUSxFQUFFLGlCQUFpQjtRQUMzQixhQUFhLEVBQUUsc0JBQXNCO1FBQ3JDLE1BQU0sRUFBRSxRQUFRO0tBQ2pCO0lBRUQsaUNBQWlDLEVBQUU7UUFDakMsS0FBSyxFQUFFLE1BQU07UUFDYixNQUFNLEVBQUUsR0FBRztRQUNYLFlBQVksRUFBRSxFQUFFO0tBQ2pCO0NBQ0YsQ0FBQztBQUVGLGVBQWU7SUFDYixVQUFVLEVBQUU7UUFDVixZQUFZLEVBQUUsRUFBRTtRQUVoQixPQUFPLEVBQUU7WUFDUCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1lBQy9CLE1BQU0sRUFBRSxNQUFNO1lBQ2QsWUFBWSxFQUFFLEVBQUU7WUFDaEIsS0FBSyxFQUFFLFlBQVk7WUFDbkIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUNwQyxjQUFjO1lBQ2QsVUFBVSxFQUFFLENBQUM7U0FDZDtRQUVELFNBQVMsRUFBRTtZQUNULFNBQVMsRUFBRSxZQUFZLENBQUM7Z0JBQ3RCLE1BQU0sRUFBRSxDQUFDLEVBQUUsV0FBVyxFQUFFLEVBQUUsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLENBQUM7Z0JBQy9DLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQztnQkFFYixPQUFPLEVBQUUsQ0FBQzt3QkFDUixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO3dCQUM5QixjQUFjLEVBQUUsZUFBZTt3QkFDL0IsVUFBVSxFQUFFLFFBQVE7d0JBQ3BCLFlBQVksRUFBRSxFQUFFO3FCQUNqQixDQUFDO2FBQ0gsQ0FBQztZQUdGLFFBQVEsRUFBRTtnQkFDUixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUU5QixJQUFJLEVBQUU7b0JBQ0osVUFBVSxFQUFFLEVBQUU7b0JBQ2QsS0FBSyxFQUFFLEVBQUU7b0JBQ1QsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2lCQUMzQjtnQkFFRCxTQUFTLEVBQUU7b0JBQ1QsVUFBVSxFQUFFLEVBQUU7b0JBQ2QsS0FBSyxFQUFFLEVBQUU7b0JBQ1QsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2lCQUMzQjthQUNGO1NBQ0Y7UUFFRCxTQUFTLEVBQUU7WUFDVCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1lBQy9CLEtBQUssRUFBRSxNQUFNO1lBQ2IsUUFBUSxFQUFFLEdBQUc7WUFDYixNQUFNLEVBQUUsUUFBUTtZQUNoQixTQUFTLEVBQUUsUUFBUTtZQUNuQixRQUFRLEVBQUUsRUFBRTtZQUNaLFVBQVUsRUFBRSxNQUFNO1lBQ2xCLE9BQU8sRUFBRSxnQkFBZ0I7WUFDekIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxtQkFBbUI7WUFDeEMsVUFBVSxFQUFFLEdBQUc7U0FDaEI7UUFFRCxJQUFJLEVBQUUsWUFBWSxDQUFDO1lBQ2pCLE1BQU0sRUFBRSxDQUFDLEVBQUUsV0FBVyxFQUFFLEVBQUUsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUUsQ0FBQztZQUM3RCxPQUFPLEVBQUUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsYUFBYSxHQUFHLENBQUM7WUFFbEUsT0FBTyxFQUFFLENBQUM7b0JBQ1IsU0FBUyxFQUFFLFFBQVE7b0JBQ25CLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDMUIsU0FBUyxFQUFFLFNBQVM7b0JBQ3BCLFVBQVUsRUFBRSxRQUFRLENBQUMsV0FBVztpQkFDakMsQ0FBQztTQUNILENBQUM7S0FFSDtJQUVELFdBQVcsRUFBRSxZQUFZLENBQUM7UUFDeEIsTUFBTSxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLENBQUM7UUFDL0IsT0FBTyxFQUFFLEVBQUU7UUFDWCxPQUFPLEVBQUUsQ0FBQyxFQUFFLFlBQVksRUFBRSxFQUFFLEVBQUUsQ0FBQztLQUNoQyxDQUFDO0lBRUYsT0FBTyxFQUFFO1FBQ1AsU0FBUyxFQUFFO1lBQ1QsV0FBVyxFQUFFLENBQUM7WUFDZCxZQUFZLEVBQUUsQ0FBQztZQUNmLFFBQVEsRUFBRSxHQUFHO1lBQ2IsTUFBTSxFQUFFLGFBQWE7WUFDckIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixRQUFRLEVBQUUsTUFBTTtTQUNqQjtRQUVELE1BQU0sRUFBRTtZQUNOLE1BQU0sRUFBRSxFQUFFO1lBQ1YsVUFBVSxFQUFFLE1BQU07WUFDbEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxXQUFXO1lBQ2hDLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDL0IsUUFBUSxFQUFFLEVBQUU7WUFDWixLQUFLLEVBQUUsUUFBUSxDQUFDLFlBQVk7WUFDNUIsV0FBVyxFQUFFLEVBQUU7U0FDaEI7UUFFRCxHQUFHLEVBQUU7WUFDSCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1lBQy9CLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLENBQUM7WUFDZixVQUFVLEVBQUUsUUFBUSxDQUFDLE9BQU87WUFDNUIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1lBQzFCLFFBQVEsRUFBRSxFQUFFO1lBQ1osVUFBVSxFQUFFLE1BQU07WUFDbEIsV0FBVyxFQUFFLEVBQUU7WUFDZixZQUFZLEVBQUUsRUFBRTtTQUNqQjtLQUNGO0lBRUQsa0JBQWtCLEVBQUU7UUFDbEIsTUFBTSxFQUFFLEVBQUU7S0FDWDtJQUVELFdBQVcsRUFBRTtRQUNYLEtBQUssRUFBRSxNQUFNO1FBQ2IsVUFBVSxFQUFFLEVBQUU7UUFFZCxPQUFPLEVBQUU7WUFDUCxLQUFLLEVBQUUsSUFBSTtZQUNYLE1BQU0sRUFBRSxHQUFHO1lBQ1gsWUFBWSxFQUFFLEVBQUU7U0FDakI7UUFFRCxhQUFhLEVBQUU7WUFDYixLQUFLLEVBQUUsTUFBTTtZQUNiLE1BQU0sRUFBRSxHQUFHO1lBQ1gsWUFBWSxFQUFFLEVBQUU7U0FDakI7UUFFRCxTQUFTLEVBQUU7WUFDVCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLGNBQWMsRUFBRSxlQUFlO1lBQy9CLFVBQVUsRUFBRSxRQUFRO1lBQ3BCLFlBQVksRUFBRSxFQUFFO1lBRWhCLFNBQVMsRUFBRTtnQkFDVCxNQUFNLEVBQUUsRUFBRTtnQkFDVixLQUFLLEVBQUUsS0FBSzthQUNiO1lBRUQsV0FBVyxFQUFFO2dCQUNYLE1BQU0sRUFBRSxFQUFFO2dCQUNWLEtBQUssRUFBRSxLQUFLO2FBQ2I7U0FDRjtRQUVELEtBQUssRUFBRTtZQUNMLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLEVBQUU7WUFDVixZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELE9BQU8sRUFBRTtZQUNQLE1BQU0sRUFBRSxFQUFFO1lBQ1YsS0FBSyxFQUFFLE1BQU07WUFDYixZQUFZLEVBQUUsRUFBRTtTQUNqQjtLQUNGO0lBRUQsTUFBTSxFQUFFO1FBQ04sVUFBVSxFQUFFO1lBQ1YsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsTUFBTTtZQUNkLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7U0FDaEM7UUFFRCxTQUFTLEVBQUU7WUFDVCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1lBQ3BDLFVBQVUsRUFBRSxRQUFRLENBQUMsVUFBVTtZQUUvQixTQUFTLEVBQUU7Z0JBQ1QsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtnQkFDcEMsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsTUFBTSxFQUFFLE1BQU07Z0JBQ2QsY0FBYyxFQUFFLE9BQU87Z0JBQ3ZCLGtCQUFrQixFQUFFLGVBQWU7Z0JBQ25DLE9BQU8sRUFBRSxFQUFFO2dCQUNYLFNBQVMsRUFBRSxZQUFZO2FBQ3hCO1lBRUQsSUFBSSxFQUFFO2dCQUNKLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLFVBQVUsRUFBRSxlQUFhLFFBQVEsQ0FBQyxZQUFjO2FBQ2pEO1lBRUQsS0FBSyxFQUFFO2dCQUNMLE9BQU8sRUFBRSxlQUFlO2dCQUN4QixRQUFRLEVBQUUsRUFBRTtnQkFDWixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxtQkFBbUI7Z0JBQ3hDLFVBQVUsRUFBRSxHQUFHO2FBQ2hCO1lBRUQsV0FBVyxFQUFFO2dCQUNYLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxNQUFNO2dCQUNsQixPQUFPLEVBQUUsVUFBVTtnQkFDbkIsTUFBTSxFQUFFLGFBQWE7Z0JBQ3JCLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixVQUFVLEVBQUUsUUFBUSxDQUFDLFdBQVc7Z0JBQ2hDLFNBQVMsRUFBRSxRQUFRO2FBQ3BCO1NBQ0Y7UUFFRCxPQUFPLEVBQUU7WUFDUCxTQUFTLEVBQUU7Z0JBQ1QsV0FBVyxFQUFFLEVBQUU7Z0JBQ2YsWUFBWSxFQUFFLEVBQUU7Z0JBQ2hCLFlBQVksRUFBRSxFQUFFO2dCQUNoQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixRQUFRLEVBQUUsTUFBTTthQUNqQjtZQUVELE1BQU0sRUFBRTtnQkFDTixNQUFNLEVBQUUsRUFBRTtnQkFDVixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxXQUFXO2dCQUNoQyxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO2dCQUMvQixRQUFRLEVBQUUsRUFBRTtnQkFDWixLQUFLLEVBQUUsUUFBUSxDQUFDLFlBQVk7Z0JBQzVCLFdBQVcsRUFBRSxFQUFFO2FBQ2hCO1lBRUQsR0FBRyxFQUFFO2dCQUNILE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7Z0JBQy9CLFlBQVksRUFBRSxFQUFFO2dCQUNoQixXQUFXLEVBQUUsRUFBRTtnQkFDZixZQUFZLEVBQUUsQ0FBQztnQkFDZixVQUFVLEVBQUUsUUFBUSxDQUFDLE9BQU87Z0JBQzVCLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDMUIsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osVUFBVSxFQUFFLE1BQU07Z0JBQ2xCLFdBQVcsRUFBRSxFQUFFO2dCQUNmLFlBQVksRUFBRSxFQUFFO2FBQ2pCO1NBQ0Y7S0FDRjtDQUNLLENBQUMifQ==
// CONCATENATED MODULE: ./components/magazine/blog-detail/view-desktop.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};










var renderLoadingPlaceholder = function () { return (react["createElement"]("div", null,
    react["createElement"](loading_placeholder["a" /* default */], { style: [blog_detail_style.placeholder.mainImg, 'MOBILE' === window.DEVICE_VERSION && blog_detail_style.placeholder.mainImgMobile] }),
    react["createElement"]("div", { style: blog_detail_style.placeholder.iconGroup },
        react["createElement"](loading_placeholder["a" /* default */], { style: blog_detail_style.placeholder.iconGroup.dateGroup }),
        react["createElement"](loading_placeholder["a" /* default */], { style: blog_detail_style.placeholder.iconGroup.socialGroup })),
    react["createElement"](loading_placeholder["a" /* default */], { style: blog_detail_style.placeholder.title }),
    react["createElement"](loading_placeholder["a" /* default */], { style: [blog_detail_style.placeholder.content, { width: '80%' }] }),
    react["createElement"](loading_placeholder["a" /* default */], { style: [blog_detail_style.placeholder.content, { width: '60%' }] }),
    react["createElement"](loading_placeholder["a" /* default */], { style: [blog_detail_style.placeholder.content, { width: '40%' }] }))); };
var renderDesktop = function (props) {
    var _a = props, magazine = _a.magazine, magazineRelatedBlogList = _a.magazineRelatedBlogList;
    var productProps = {
        title: 'Sản phẩm trong bài viết',
        column: 4,
        data: magazine.related_boxes,
        showViewMore: false,
        showQuickView: false,
        showLike: false,
    };
    var magazineImageProps = {
        title: 'Bài viết liên quan',
        column: 4,
        showViewMore: false,
        data: magazineRelatedBlogList || []
    };
    return (true === Object(validate["j" /* isEmptyObject */])(magazine)
        ? renderLoadingPlaceholder()
        :
            react["createElement"]("magazine-blog-detail", null,
                react["createElement"]("div", { style: blog_detail_style.headerWrap },
                    react["createElement"]("img", { style: blog_detail_style.headerWrap.imgWrap, src: magazine.cover_image.original_url, alt: magazine.title }),
                    react["createElement"]("div", { style: blog_detail_style.headerWrap.blogTitle }, magazine.title),
                    react["createElement"]("div", { style: blog_detail_style.headerWrap.desc }, magazine.description)),
                react["createElement"]("div", { className: 'magazine-content-detail', style: blog_detail_style.contentWrap },
                    Object(html["a" /* renderHtmlContent */])(magazine.content),
                    react["createElement"](radium["Style"], { rules: INLINE_STYLE })),
                0 !== magazine.tags.length &&
                    react["createElement"]("div", { style: blog_detail_style.tagWrap.container },
                        react["createElement"]("div", { style: blog_detail_style.tagWrap.header }, "Tags:"),
                        magazine
                            && Array.isArray(magazine.tags)
                            && magazine.tags.map(function (item, index) {
                                var linkProps = {
                                    to: routing["Ia" /* ROUTING_MAGAZINE */] + "/tag/" + item.replace('#', ''),
                                    key: "link-tag-" + index
                                };
                                return react["createElement"](react_router_dom["NavLink"], __assign({}, linkProps),
                                    react["createElement"]("span", { key: "tag-" + index, style: blog_detail_style.tagWrap.tag }, item));
                            })),
                react["createElement"](image_slider["a" /* default */], __assign({}, magazineImageProps)),
                magazine && magazine.related_boxes && !!magazine.related_boxes.length && react["createElement"]("div", { style: blog_detail_style.productSlide },
                    react["createElement"](slider["a" /* default */], __assign({}, productProps)))));
};
/* harmony default export */ var view_desktop = (renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1kZXNrdG9wLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1kZXNrdG9wLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUMzQyxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sUUFBUSxDQUFDO0FBRS9CLE9BQU8sYUFBYSxNQUFNLHNCQUFzQixDQUFDO0FBQ2pELE9BQU8sa0JBQWtCLE1BQU0sOEJBQThCLENBQUM7QUFDOUQsT0FBTyxtQkFBbUIsTUFBTSwyQ0FBMkMsQ0FBQztBQUU1RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFHMUUsT0FBTyxLQUFLLEVBQUUsRUFBRSxZQUFZLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFFOUMsTUFBTSxDQUFDLElBQU0sd0JBQXdCLEdBQUcsY0FBTSxPQUFBLENBQzVDO0lBQ0Usb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsUUFBUSxLQUFLLE1BQU0sQ0FBQyxjQUFjLElBQUksS0FBSyxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsR0FBSTtJQUNqSSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxTQUFTO1FBQ3JDLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQUk7UUFDcEUsb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBSSxDQUNsRTtJQUNOLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBSTtJQUN0RCxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxHQUFJO0lBQzVFLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDLEdBQUk7SUFDNUUsb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUMsR0FBSSxDQUN4RSxDQUNQLEVBWjZDLENBWTdDLENBQUM7QUFFRixJQUFNLGFBQWEsR0FBRyxVQUFDLEtBQWE7SUFDNUIsSUFBQSxVQUF1RCxFQUFyRCxzQkFBUSxFQUFFLG9EQUF1QixDQUFxQjtJQUU5RCxJQUFNLFlBQVksR0FBRztRQUNuQixLQUFLLEVBQUUseUJBQXlCO1FBQ2hDLE1BQU0sRUFBRSxDQUFDO1FBQ1QsSUFBSSxFQUFFLFFBQVEsQ0FBQyxhQUFhO1FBQzVCLFlBQVksRUFBRSxLQUFLO1FBQ25CLGFBQWEsRUFBRSxLQUFLO1FBQ3BCLFFBQVEsRUFBRSxLQUFLO0tBQ2hCLENBQUM7SUFFRixJQUFNLGtCQUFrQixHQUFHO1FBQ3pCLEtBQUssRUFBRSxvQkFBb0I7UUFDM0IsTUFBTSxFQUFFLENBQUM7UUFDVCxZQUFZLEVBQUUsS0FBSztRQUNuQixJQUFJLEVBQUUsdUJBQXVCLElBQUksRUFBRTtLQUNwQyxDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsSUFBSSxLQUFLLGFBQWEsQ0FBQyxRQUFRLENBQUM7UUFDOUIsQ0FBQyxDQUFDLHdCQUF3QixFQUFFO1FBQzVCLENBQUM7WUFDRDtnQkFFRSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFVBQVU7b0JBQzFCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxHQUFHLEVBQUUsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsR0FBRyxFQUFFLFFBQVEsQ0FBQyxLQUFLLEdBQUk7b0JBQ3JHLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsVUFBVSxDQUFDLFNBQVMsSUFBRyxRQUFRLENBQUMsS0FBSyxDQUFPO29CQUM5RCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLElBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBTyxDQUMzRDtnQkFFTiw2QkFBSyxTQUFTLEVBQUUseUJBQXlCLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXO29CQUNoRSxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDO29CQUNwQyxvQkFBQyxLQUFLLElBQUMsS0FBSyxFQUFFLFlBQVksR0FBSSxDQUMxQjtnQkFHSixDQUFDLEtBQUssUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNO29CQUMxQiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTO3dCQUNqQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLFlBQWE7d0JBRTNDLFFBQVE7K0JBQ0wsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDOytCQUM1QixRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLO2dDQUMvQixJQUFNLFNBQVMsR0FBRztvQ0FDaEIsRUFBRSxFQUFLLGdCQUFnQixhQUFRLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBRztvQ0FDdEQsR0FBRyxFQUFFLGNBQVksS0FBTztpQ0FDekIsQ0FBQztnQ0FDRixNQUFNLENBQUMsb0JBQUMsT0FBTyxlQUFLLFNBQVM7b0NBQUUsOEJBQU0sR0FBRyxFQUFFLFNBQU8sS0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsSUFBRyxJQUFJLENBQVEsQ0FBVSxDQUFDOzRCQUM5RyxDQUFDLENBQUMsQ0FFQTtnQkFJUixvQkFBQyxtQkFBbUIsZUFBSyxrQkFBa0IsRUFBSTtnQkFHOUMsUUFBUSxJQUFJLFFBQVEsQ0FBQyxhQUFhLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsTUFBTSxJQUFJLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWTtvQkFBRSxvQkFBQyxhQUFhLGVBQUssWUFBWSxFQUFJLENBQU0sQ0FDOUgsQ0FDMUIsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsYUFBYSxDQUFDIn0=
// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// CONCATENATED MODULE: ./components/magazine/blog-detail/view-mobile.tsx
var view_mobile_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};











var view_mobile_renderLoadingPlaceholder = function () { return (react["createElement"]("div", null,
    react["createElement"](loading_placeholder["a" /* default */], { style: [blog_detail_style.placeholder.mainImg, 'MOBILE' === window.DEVICE_VERSION && blog_detail_style.placeholder.mainImgMobile] }),
    react["createElement"]("div", { style: blog_detail_style.placeholder.iconGroup },
        react["createElement"](loading_placeholder["a" /* default */], { style: blog_detail_style.placeholder.iconGroup.dateGroup }),
        react["createElement"](loading_placeholder["a" /* default */], { style: blog_detail_style.placeholder.iconGroup.socialGroup })),
    react["createElement"](loading_placeholder["a" /* default */], { style: blog_detail_style.placeholder.title }),
    react["createElement"](loading_placeholder["a" /* default */], { style: [blog_detail_style.placeholder.content, { width: '80%' }] }),
    react["createElement"](loading_placeholder["a" /* default */], { style: [blog_detail_style.placeholder.content, { width: '60%' }] }),
    react["createElement"](loading_placeholder["a" /* default */], { style: [blog_detail_style.placeholder.content, { width: '40%' }] }))); };
var renderIcon = function (_a) {
    var name = _a.name, style = _a.style;
    var iconProps = {
        name: name,
        style: style
    };
    return react["createElement"](icon["a" /* default */], view_mobile_assign({}, iconProps));
};
var renderIconContact = function () {
    var iconStyle = blog_detail_style.headerWrap.viewGroup.iconWrap;
    return (react["createElement"]("div", { style: iconStyle },
        renderIcon({ name: 'pinterest', style: iconStyle.icon }),
        renderIcon({ name: 'facebook', style: iconStyle.icon }),
        renderIcon({ name: 'instagram', style: iconStyle.icon }),
        renderIcon({ name: 'email', style: iconStyle.iconEmail })));
};
var renderCoverImage = function (magazine) { return (react["createElement"]("img", { style: blog_detail_style.mobile.coverImage, src: magazine.cover_image.original_url, alt: magazine.title })); };
var renderCoverInfo = function (magazine) {
    var blurImage = magazine.cover_image.blur_url;
    var blurImageStyle = [
        { backgroundImage: "url(" + blurImage + ")" },
        blog_detail_style.mobile.coverInfo.blurImage
    ];
    return (react["createElement"]("div", { style: blog_detail_style.mobile.coverInfo },
        react["createElement"]("div", { style: blurImageStyle }),
        react["createElement"]("div", { style: blog_detail_style.mobile.coverInfo.info, className: 'magazine-mobile-cover' },
            react["createElement"]("div", { style: blog_detail_style.mobile.coverInfo.title }, magazine.title),
            react["createElement"]("div", { style: blog_detail_style.mobile.coverInfo.description }, magazine.description))));
};
var renderMobile = function (props) {
    var _a = props, magazine = _a.magazine, magazineRelatedBlogList = _a.magazineRelatedBlogList;
    var productProps = {
        title: 'Sản phẩm trong bài viết',
        column: 4,
        data: magazine.related_boxes,
        showViewMore: false,
        showQuickView: false,
        showLike: false,
    };
    var magazineImageProps = {
        title: 'Bài viết liên quan',
        column: 1,
        showViewMore: false,
        data: magazineRelatedBlogList && !!magazineRelatedBlogList.length && magazineRelatedBlogList.slice(0, 4) || []
    };
    var renderItem = function (item, index) {
        var linkProps = {
            to: routing["Ia" /* ROUTING_MAGAZINE */] + "/tag/" + item.replace('#', ''),
            key: "link-tag-" + index,
            style: blog_detail_style.mobile.tagWrap.tag
        };
        return react["createElement"](react_router_dom["NavLink"], view_mobile_assign({}, linkProps), item);
    };
    return (true === Object(validate["j" /* isEmptyObject */])(magazine)
        ? view_mobile_renderLoadingPlaceholder()
        : (react["createElement"]("magazine-blog-detail", null,
            renderCoverImage(magazine),
            renderCoverInfo(magazine),
            react["createElement"]("div", { className: 'magazine-content-detail magazine-content-detail-mobile', style: blog_detail_style.contentWrap },
                Object(html["a" /* renderHtmlContent */])(magazine.content),
                react["createElement"](radium["Style"], { rules: INLINE_STYLE })),
            0 !== magazine.tags.length &&
                react["createElement"]("div", { style: blog_detail_style.mobile.tagWrap.container },
                    react["createElement"]("div", { style: blog_detail_style.mobile.tagWrap.header }, "Tags:"),
                    magazine && Array.isArray(magazine.tags) && magazine.tags.map(renderItem)),
            react["createElement"](image_slider["a" /* default */], view_mobile_assign({}, magazineImageProps)),
            0 !== magazine.related_boxes.length && react["createElement"](slider["a" /* default */], view_mobile_assign({}, productProps)))));
};
/* harmony default export */ var view_mobile = (renderMobile);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1tb2JpbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LW1vYmlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUMvQixPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDM0MsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUUvQixPQUFPLElBQUksTUFBTSxlQUFlLENBQUM7QUFDakMsT0FBTyxhQUFhLE1BQU0sc0JBQXNCLENBQUM7QUFDakQsT0FBTyxrQkFBa0IsTUFBTSw4QkFBOEIsQ0FBQztBQUM5RCxPQUFPLG1CQUFtQixNQUFNLDJDQUEyQyxDQUFDO0FBQzVFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUcxRSxPQUFPLEtBQUssRUFBRSxFQUFFLFlBQVksRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUU5QyxNQUFNLENBQUMsSUFBTSx3QkFBd0IsR0FBRyxjQUFNLE9BQUEsQ0FDNUM7SUFDRSxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxRQUFRLEtBQUssTUFBTSxDQUFDLGNBQWMsSUFBSSxLQUFLLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxHQUFJO0lBQ2pJLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFNBQVM7UUFDckMsb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBSTtRQUNwRSxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFJLENBQ2xFO0lBQ04sb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsS0FBSyxHQUFJO0lBQ3RELG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDLEdBQUk7SUFDNUUsb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUMsR0FBSTtJQUM1RSxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxHQUFJLENBQ3hFLENBQ1AsRUFaNkMsQ0FZN0MsQ0FBQztBQUVGLElBQU0sVUFBVSxHQUFHLFVBQUMsRUFBZTtRQUFiLGNBQUksRUFBRSxnQkFBSztJQUMvQixJQUFNLFNBQVMsR0FBRztRQUNoQixJQUFJLE1BQUE7UUFDSixLQUFLLE9BQUE7S0FDTixDQUFDO0lBQ0YsTUFBTSxDQUFDLG9CQUFDLElBQUksZUFBSyxTQUFTLEVBQUksQ0FBQztBQUNqQyxDQUFDLENBQUM7QUFFRixJQUFNLGlCQUFpQixHQUFHO0lBQ3hCLElBQU0sU0FBUyxHQUFHLEtBQUssQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztJQUN0RCxNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsU0FBUztRQUNsQixVQUFVLENBQUMsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDeEQsVUFBVSxDQUFDLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3ZELFVBQVUsQ0FBQyxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUN4RCxVQUFVLENBQUMsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FDdEQsQ0FDUCxDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsSUFBTSxnQkFBZ0IsR0FBRyxVQUFDLFFBQVEsSUFBSyxPQUFBLENBQ3JDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxHQUFHLEVBQUUsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsR0FBRyxFQUFFLFFBQVEsQ0FBQyxLQUFLLEdBQUksQ0FDckcsRUFGc0MsQ0FFdEMsQ0FBQztBQUVGLElBQU0sZUFBZSxHQUFHLFVBQUMsUUFBUTtJQUMvQixJQUFNLFNBQVMsR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQztJQUNoRCxJQUFNLGNBQWMsR0FBRztRQUNyQixFQUFFLGVBQWUsRUFBRSxTQUFPLFNBQVMsTUFBRyxFQUFFO1FBQ3hDLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVM7S0FDakMsQ0FBQTtJQUVELE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVM7UUFDaEMsNkJBQUssS0FBSyxFQUFFLGNBQWMsR0FBUTtRQUNsQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLFNBQVMsRUFBRSx1QkFBdUI7WUFDekUsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssSUFBRyxRQUFRLENBQUMsS0FBSyxDQUFPO1lBQ2hFLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxXQUFXLElBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBTyxDQUN4RSxDQUNGLENBQ1AsQ0FBQTtBQUNILENBQUMsQ0FBQTtBQUVELElBQU0sWUFBWSxHQUFHLFVBQUMsS0FBYTtJQUMzQixJQUFBLFVBQXVELEVBQXJELHNCQUFRLEVBQUUsb0RBQXVCLENBQXFCO0lBRTlELElBQU0sWUFBWSxHQUFHO1FBQ25CLEtBQUssRUFBRSx5QkFBeUI7UUFDaEMsTUFBTSxFQUFFLENBQUM7UUFDVCxJQUFJLEVBQUUsUUFBUSxDQUFDLGFBQWE7UUFDNUIsWUFBWSxFQUFFLEtBQUs7UUFDbkIsYUFBYSxFQUFFLEtBQUs7UUFDcEIsUUFBUSxFQUFFLEtBQUs7S0FDaEIsQ0FBQztJQUVGLElBQU0sa0JBQWtCLEdBQUc7UUFDekIsS0FBSyxFQUFFLG9CQUFvQjtRQUMzQixNQUFNLEVBQUUsQ0FBQztRQUNULFlBQVksRUFBRSxLQUFLO1FBQ25CLElBQUksRUFBRSx1QkFBdUIsSUFBSSxDQUFDLENBQUMsdUJBQXVCLENBQUMsTUFBTSxJQUFJLHVCQUF1QixDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRTtLQUMvRyxDQUFDO0lBRUYsSUFBTSxVQUFVLEdBQUcsVUFBQyxJQUFJLEVBQUUsS0FBSztRQUM3QixJQUFNLFNBQVMsR0FBRztZQUNoQixFQUFFLEVBQUssZ0JBQWdCLGFBQVEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFHO1lBQ3RELEdBQUcsRUFBRSxjQUFZLEtBQU87WUFDeEIsS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEdBQUc7U0FDaEMsQ0FBQztRQUNGLE1BQU0sQ0FBQyxvQkFBQyxPQUFPLGVBQUssU0FBUyxHQUFHLElBQUksQ0FBVyxDQUFDO0lBQ2xELENBQUMsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLElBQUksS0FBSyxhQUFhLENBQUMsUUFBUSxDQUFDO1FBQzlCLENBQUMsQ0FBQyx3QkFBd0IsRUFBRTtRQUM1QixDQUFDLENBQUMsQ0FDQTtZQUNHLGdCQUFnQixDQUFDLFFBQVEsQ0FBQztZQUMxQixlQUFlLENBQUMsUUFBUSxDQUFDO1lBRzFCLDZCQUFLLFNBQVMsRUFBRSx3REFBd0QsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVc7Z0JBQy9GLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUM7Z0JBQ3BDLG9CQUFDLEtBQUssSUFBQyxLQUFLLEVBQUUsWUFBWSxHQUFJLENBQzFCO1lBSUosQ0FBQyxLQUFLLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTTtnQkFDMUIsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVM7b0JBQ3hDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLFlBQWE7b0JBQ25ELFFBQVEsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FDdEU7WUFJUixvQkFBQyxtQkFBbUIsZUFBSyxrQkFBa0IsRUFBSTtZQUc5QyxDQUFDLEtBQUssUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLElBQUksb0JBQUMsYUFBYSxlQUFLLFlBQVksRUFBSSxDQUN0RCxDQUN4QixDQUNKLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLFlBQVksQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/blog-detail/view.tsx


function renderView(props) {
    var switchView = {
        MOBILE: function () { return view_mobile(props); },
        DESKTOP: function () { return view_desktop(props); }
    };
    return switchView[window.DEVICE_VERSION]();
}
;
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sYUFBYSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sWUFBWSxNQUFNLGVBQWUsQ0FBQztBQUV6QyxNQUFNLHFCQUFxQixLQUFLO0lBQzlCLElBQU0sVUFBVSxHQUFHO1FBQ2pCLE1BQU0sRUFBRSxjQUFNLE9BQUEsWUFBWSxDQUFDLEtBQUssQ0FBQyxFQUFuQixDQUFtQjtRQUNqQyxPQUFPLEVBQUUsY0FBTSxPQUFBLGFBQWEsQ0FBQyxLQUFLLENBQUMsRUFBcEIsQ0FBb0I7S0FDcEMsQ0FBQztJQUVGLE1BQU0sQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUM7QUFDN0MsQ0FBQztBQUFBLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/blog-detail/initialize.tsx
var DEFAULT_PROPS = {
    magazine: {},
    magazineRelatedBlogList: []
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixRQUFRLEVBQUUsRUFBRTtJQUNaLHVCQUF1QixFQUFFLEVBQUU7Q0FDbEIsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/blog-detail/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_BlogDetail = /** @class */ (function (_super) {
    __extends(BlogDetail, _super);
    function BlogDetail(props) {
        return _super.call(this, props) || this;
    }
    BlogDetail.prototype.render = function () {
        return view(this.props);
    };
    ;
    BlogDetail.defaultProps = DEFAULT_PROPS;
    BlogDetail = __decorate([
        radium
    ], BlogDetail);
    return BlogDetail;
}(react["PureComponent"]));
/* harmony default export */ var component = (component_BlogDetail);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFFaEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUc3QztJQUF5Qiw4QkFBNkI7SUFFcEQsb0JBQVksS0FBYTtlQUN2QixrQkFBTSxLQUFLLENBQUM7SUFDZCxDQUFDO0lBRUQsMkJBQU0sR0FBTjtRQUNFLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFBQSxDQUFDO0lBUEssdUJBQVksR0FBVyxhQUFhLENBQUM7SUFEeEMsVUFBVTtRQURmLE1BQU07T0FDRCxVQUFVLENBU2Y7SUFBRCxpQkFBQztDQUFBLEFBVEQsQ0FBeUIsYUFBYSxHQVNyQztBQUVELGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/blog-detail/index.tsx

/* harmony default export */ var blog_detail = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxVQUFVLE1BQU0sYUFBYSxDQUFDO0FBQ3JDLGVBQWUsVUFBVSxDQUFDIn0=
// EXTERNAL MODULE: ./components/magazine/category/index.tsx + 7 modules
var category = __webpack_require__(797);

// EXTERNAL MODULE: ./components/magazine/video-item/index.tsx + 4 modules
var video_item = __webpack_require__(805);

// EXTERNAL MODULE: ./container/layout/wrap/container.tsx + 1 modules
var container = __webpack_require__(755);

// EXTERNAL MODULE: ./utils/format.ts
var format = __webpack_require__(116);

// CONCATENATED MODULE: ./components/magazine/blog-video-detail/style.tsx


/* harmony default export */ var blog_video_detail_style = ({
    headerWrap: {
        marginBottom: 20,
        blogTitle: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    fontSize: 30,
                    paddingLeft: 10,
                    paddingRight: 10
                }],
            DESKTOP: [{
                    fontSize: 40
                }],
            GENERAL: [{
                    display: variable["display"].block,
                    width: '100%',
                    margin: '0 auto',
                    lineHeight: '42px',
                    fontWeight: 600,
                    paddingTop: 10,
                    paddingBottom: 40,
                    fontFamily: variable["fontPlayFairRegular"],
                }]
        }),
        desc: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{ paddingLeft: 10, paddingRight: 10, fontSize: 18 }],
            DESKTOP: [{ fontSize: 20, margin: '0 auto 30px', }],
            GENERAL: [{
                    fontStyle: 'italic',
                    color: variable["colorBlack"],
                    textAlign: 'justify',
                    fontFamily: variable["fontTrirong"],
                }]
        })
    },
    placeholder: {
        width: '100%',
        paddingTop: 20,
        mainImg: {
            width: '100%',
            height: 680,
            marginBottom: 20
        },
        mainImgMobile: {
            width: '100%',
            height: 240,
            marginBottom: 20
        },
        iconGroup: {
            display: variable["display"].flex,
            justifyContent: 'space-between',
            alignItems: 'center',
            marginBottom: 10,
            dateGroup: {
                height: 20,
                width: '20%'
            },
            socialGroup: {
                height: 20,
                width: '20%'
            }
        },
        title: {
            width: '100%',
            height: 70,
            marginBottom: 20,
        },
        content: {
            height: 30,
            width: '100%',
            marginBottom: 10
        }
    },
    videoContainer: {
        container: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    height: 250
                }],
            DESKTOP: [{
                    height: 680
                }],
            GENERAL: [{
                    marginBottom: 20,
                    backgroundColor: variable["colorF7"]
                }]
        }),
        video: {
            width: '100%',
            height: '100%'
        }
    },
    title: {
        container: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    marginBottom: 10
                }],
            DESKTOP: [{
                    marginBottom: 20
                }],
            GENERAL: [{
                    top: -1,
                    height: 60,
                    fontSize: 20,
                    letterSpacing: -0.5,
                    maxWidth: "100%",
                    overflow: "hidden",
                    lineHeight: "60px",
                    whiteSpace: "nowrap",
                    textOverflow: "ellipsis",
                    backdropFilter: 'blur(10px)',
                    WebkitBackdropFilter: 'blur(10px)',
                    color: variable["colorBlack"],
                    fontFamily: variable["fontTrirong"],
                    background: variable["colorWhite08"],
                    position: variable["position"].relative
                }]
        }),
        borderLine: {
            display: variable["display"].block,
            position: variable["position"].absolute,
            height: 4,
            width: 25,
            background: variable["colorBlack"],
            bottom: 1,
        },
        line: {
            display: variable["display"].block,
            position: variable["position"].absolute,
            height: 1,
            width: '100%',
            background: variable["colorBlack05"],
            bottom: 1,
        },
    },
    videoRelated: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                paddingLeft: 10,
                paddingRight: 10
            }],
        DESKTOP: [{}],
        GENERAL: [{
                marginBottom: 20
            }]
    })
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFekQsZUFBZTtJQUNiLFVBQVUsRUFBRTtRQUNWLFlBQVksRUFBRSxFQUFFO1FBRWhCLFNBQVMsRUFBRSxZQUFZLENBQUM7WUFDdEIsTUFBTSxFQUFFLENBQUM7b0JBQ1AsUUFBUSxFQUFFLEVBQUU7b0JBQ1osV0FBVyxFQUFFLEVBQUU7b0JBQ2YsWUFBWSxFQUFFLEVBQUU7aUJBQ2pCLENBQUM7WUFFRixPQUFPLEVBQUUsQ0FBQztvQkFDUixRQUFRLEVBQUUsRUFBRTtpQkFDYixDQUFDO1lBRUYsT0FBTyxFQUFFLENBQUM7b0JBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztvQkFDL0IsS0FBSyxFQUFFLE1BQU07b0JBQ2IsTUFBTSxFQUFFLFFBQVE7b0JBQ2hCLFVBQVUsRUFBRSxNQUFNO29CQUNsQixVQUFVLEVBQUUsR0FBRztvQkFDZixVQUFVLEVBQUUsRUFBRTtvQkFDZCxhQUFhLEVBQUUsRUFBRTtvQkFDakIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxtQkFBbUI7aUJBQ3pDLENBQUM7U0FDSCxDQUFDO1FBRUYsSUFBSSxFQUFFLFlBQVksQ0FBQztZQUNqQixNQUFNLEVBQUUsQ0FBQyxFQUFFLFdBQVcsRUFBRSxFQUFFLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsRUFBRSxFQUFFLENBQUM7WUFDN0QsT0FBTyxFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxhQUFhLEdBQUcsQ0FBQztZQUVuRCxPQUFPLEVBQUUsQ0FBQztvQkFDUixTQUFTLEVBQUUsUUFBUTtvQkFDbkIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixTQUFTLEVBQUUsU0FBUztvQkFDcEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxXQUFXO2lCQUNqQyxDQUFDO1NBQ0gsQ0FBQztLQUNIO0lBRUQsV0FBVyxFQUFFO1FBQ1gsS0FBSyxFQUFFLE1BQU07UUFDYixVQUFVLEVBQUUsRUFBRTtRQUVkLE9BQU8sRUFBRTtZQUNQLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLEdBQUc7WUFDWCxZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELGFBQWEsRUFBRTtZQUNiLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLEdBQUc7WUFDWCxZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELFNBQVMsRUFBRTtZQUNULE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsY0FBYyxFQUFFLGVBQWU7WUFDL0IsVUFBVSxFQUFFLFFBQVE7WUFDcEIsWUFBWSxFQUFFLEVBQUU7WUFFaEIsU0FBUyxFQUFFO2dCQUNULE1BQU0sRUFBRSxFQUFFO2dCQUNWLEtBQUssRUFBRSxLQUFLO2FBQ2I7WUFFRCxXQUFXLEVBQUU7Z0JBQ1gsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsS0FBSyxFQUFFLEtBQUs7YUFDYjtTQUNGO1FBRUQsS0FBSyxFQUFFO1lBQ0wsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsRUFBRTtZQUNWLFlBQVksRUFBRSxFQUFFO1NBQ2pCO1FBRUQsT0FBTyxFQUFFO1lBQ1AsTUFBTSxFQUFFLEVBQUU7WUFDVixLQUFLLEVBQUUsTUFBTTtZQUNiLFlBQVksRUFBRSxFQUFFO1NBQ2pCO0tBQ0Y7SUFFRCxjQUFjLEVBQUU7UUFDZCxTQUFTLEVBQUUsWUFBWSxDQUFDO1lBQ3RCLE1BQU0sRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxHQUFHO2lCQUNaLENBQUM7WUFFRixPQUFPLEVBQUUsQ0FBQztvQkFDUixNQUFNLEVBQUUsR0FBRztpQkFDWixDQUFDO1lBRUYsT0FBTyxFQUFFLENBQUM7b0JBQ1IsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLGVBQWUsRUFBRSxRQUFRLENBQUMsT0FBTztpQkFDbEMsQ0FBQztTQUNILENBQUM7UUFFRixLQUFLLEVBQUU7WUFDTCxLQUFLLEVBQUUsTUFBTTtZQUNiLE1BQU0sRUFBRSxNQUFNO1NBQ2Y7S0FDRjtJQUVELEtBQUssRUFBRTtRQUNMLFNBQVMsRUFBRSxZQUFZLENBQUM7WUFDdEIsTUFBTSxFQUFFLENBQUM7b0JBQ1AsWUFBWSxFQUFFLEVBQUU7aUJBQ2pCLENBQUM7WUFFRixPQUFPLEVBQUUsQ0FBQztvQkFDUixZQUFZLEVBQUUsRUFBRTtpQkFDakIsQ0FBQztZQUVGLE9BQU8sRUFBRSxDQUFDO29CQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7b0JBQ1AsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsUUFBUSxFQUFFLEVBQUU7b0JBQ1osYUFBYSxFQUFFLENBQUMsR0FBRztvQkFDbkIsUUFBUSxFQUFFLE1BQU07b0JBQ2hCLFFBQVEsRUFBRSxRQUFRO29CQUNsQixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsVUFBVSxFQUFFLFFBQVE7b0JBQ3BCLFlBQVksRUFBRSxVQUFVO29CQUN4QixjQUFjLEVBQUUsWUFBWTtvQkFDNUIsb0JBQW9CLEVBQUUsWUFBWTtvQkFDbEMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLFdBQVc7b0JBQ2hDLFVBQVUsRUFBRSxRQUFRLENBQUMsWUFBWTtvQkFDakMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtpQkFDckMsQ0FBQztTQUNILENBQUM7UUFFRixVQUFVLEVBQUU7WUFDVixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1lBQy9CLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDcEMsTUFBTSxFQUFFLENBQUM7WUFDVCxLQUFLLEVBQUUsRUFBRTtZQUNULFVBQVUsRUFBRSxRQUFRLENBQUMsVUFBVTtZQUMvQixNQUFNLEVBQUUsQ0FBQztTQUNWO1FBRUQsSUFBSSxFQUFFO1lBQ0osT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztZQUMvQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1lBQ3BDLE1BQU0sRUFBRSxDQUFDO1lBQ1QsS0FBSyxFQUFFLE1BQU07WUFDYixVQUFVLEVBQUUsUUFBUSxDQUFDLFlBQVk7WUFDakMsTUFBTSxFQUFFLENBQUM7U0FDVjtLQUNGO0lBRUQsWUFBWSxFQUFFLFlBQVksQ0FBQztRQUN6QixNQUFNLEVBQUUsQ0FBQztnQkFDUCxXQUFXLEVBQUUsRUFBRTtnQkFDZixZQUFZLEVBQUUsRUFBRTthQUNqQixDQUFDO1FBQ0YsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDO1FBQ2IsT0FBTyxFQUFFLENBQUM7Z0JBQ1IsWUFBWSxFQUFFLEVBQUU7YUFDakIsQ0FBQztLQUNILENBQUM7Q0FDSyxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/blog-video-detail/view.tsx
var view_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};






var view_renderLoadingPlaceholder = function () { return (react["createElement"]("div", null,
    react["createElement"](loading_placeholder["a" /* default */], { style: [blog_video_detail_style.placeholder.mainImg, 'MOBILE' === window.DEVICE_VERSION && blog_video_detail_style.placeholder.mainImgMobile] }),
    react["createElement"]("div", { style: blog_video_detail_style.placeholder.iconGroup },
        react["createElement"](loading_placeholder["a" /* default */], { style: blog_video_detail_style.placeholder.iconGroup.dateGroup }),
        react["createElement"](loading_placeholder["a" /* default */], { style: blog_video_detail_style.placeholder.iconGroup.socialGroup })),
    react["createElement"](loading_placeholder["a" /* default */], { style: blog_video_detail_style.placeholder.title }),
    react["createElement"](loading_placeholder["a" /* default */], { style: [blog_video_detail_style.placeholder.content, { width: '80%' }] }),
    react["createElement"](loading_placeholder["a" /* default */], { style: [blog_video_detail_style.placeholder.content, { width: '60%' }] }),
    react["createElement"](loading_placeholder["a" /* default */], { style: [blog_video_detail_style.placeholder.content, { width: '40%' }] }))); };
var renderContent = function (_a) {
    var title = _a.title, description = _a.description, content = _a.content;
    var videoProps = {
        style: blog_video_detail_style.videoContainer.video,
        src: content,
        frameBorder: '0',
        allowFullScreen: true
    };
    return (react["createElement"]("div", { style: blog_video_detail_style.headerWrap },
        react["createElement"]("div", { style: blog_video_detail_style.videoContainer.container },
            react["createElement"]("iframe", view_assign({}, videoProps))),
        react["createElement"]("div", { style: blog_video_detail_style.headerWrap.blogTitle }, title),
        Object(html["a" /* renderHtmlContent */])(Object(format["b" /* createBreakDownLine */])(description), blog_video_detail_style.headerWrap.desc)));
};
var view_renderView = function (props) {
    var mainVideo = props.mainVideo;
    return (Object(validate["j" /* isEmptyObject */])(mainVideo)
        ? view_renderLoadingPlaceholder()
        : (react["createElement"]("magazine-blog-detail", null, renderContent({
            title: mainVideo && mainVideo.title || '',
            content: mainVideo && mainVideo.content || '',
            description: mainVideo && mainVideo.description || ''
        }))));
};
/* harmony default export */ var blog_video_detail_view = (view_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxrQkFBa0IsTUFBTSw4QkFBOEIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDeEQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFHNUQsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRTVCLE1BQU0sQ0FBQyxJQUFNLHdCQUF3QixHQUFHLGNBQU0sT0FBQSxDQUM1QztJQUNFLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLFFBQVEsS0FBSyxNQUFNLENBQUMsY0FBYyxJQUFJLEtBQUssQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLEdBQUk7SUFDakksNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsU0FBUztRQUNyQyxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsU0FBUyxHQUFJO1FBQ3BFLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUksQ0FDbEU7SUFDTixvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEdBQUk7SUFDdEQsb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUMsR0FBSTtJQUM1RSxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxHQUFJO0lBQzVFLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDLEdBQUksQ0FDeEUsQ0FDUCxFQVo2QyxDQVk3QyxDQUFDO0FBRUYsSUFBTSxhQUFhLEdBQUcsVUFBQyxFQUErQjtRQUE3QixnQkFBSyxFQUFFLDRCQUFXLEVBQUUsb0JBQU87SUFDbEQsSUFBTSxVQUFVLEdBQUc7UUFDakIsS0FBSyxFQUFFLEtBQUssQ0FBQyxjQUFjLENBQUMsS0FBSztRQUNqQyxHQUFHLEVBQUUsT0FBTztRQUNaLFdBQVcsRUFBRSxHQUFHO1FBQ2hCLGVBQWUsRUFBRSxJQUFJO0tBQ3RCLENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFVBQVU7UUFDMUIsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxjQUFjLENBQUMsU0FBUztZQUN4QywyQ0FBWSxVQUFVLEVBQVcsQ0FDN0I7UUFDTiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFVBQVUsQ0FBQyxTQUFTLElBQUcsS0FBSyxDQUFPO1FBQ3BELGlCQUFpQixDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQ3ZFLENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLElBQU0sVUFBVSxHQUFHLFVBQUMsS0FBYTtJQUN2QixJQUFBLDJCQUFTLENBQXFCO0lBRXRDLE1BQU0sQ0FBQyxDQUNMLGFBQWEsQ0FBQyxTQUFTLENBQUM7UUFDdEIsQ0FBQyxDQUFDLHdCQUF3QixFQUFFO1FBQzVCLENBQUMsQ0FBQyxDQUNBLGtEQUVJLGFBQWEsQ0FBQztZQUNaLEtBQUssRUFBRSxTQUFTLElBQUksU0FBUyxDQUFDLEtBQUssSUFBSSxFQUFFO1lBQ3pDLE9BQU8sRUFBRSxTQUFTLElBQUksU0FBUyxDQUFDLE9BQU8sSUFBSSxFQUFFO1lBQzdDLFdBQVcsRUFBRSxTQUFTLElBQUksU0FBUyxDQUFDLFdBQVcsSUFBSSxFQUFFO1NBQ3RELENBQUMsQ0FFaUIsQ0FDeEIsQ0FDSixDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// CONCATENATED MODULE: ./components/magazine/blog-video-detail/initialize.tsx
var initialize_DEFAULT_PROPS = {
    mainVideo: {}
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixTQUFTLEVBQUUsRUFBRTtDQUNKLENBQUMifQ==
// CONCATENATED MODULE: ./components/magazine/blog-video-detail/component.tsx
var component_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var component_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_BlogVideoDetail = /** @class */ (function (_super) {
    component_extends(BlogVideoDetail, _super);
    function BlogVideoDetail(props) {
        return _super.call(this, props) || this;
    }
    BlogVideoDetail.prototype.render = function () {
        return blog_video_detail_view(this.props);
    };
    ;
    BlogVideoDetail.defaultProps = initialize_DEFAULT_PROPS;
    BlogVideoDetail = component_decorate([
        radium
    ], BlogVideoDetail);
    return BlogVideoDetail;
}(react["PureComponent"]));
/* harmony default export */ var blog_video_detail_component = (component_BlogVideoDetail);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFFaEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUc3QztJQUE4QixtQ0FBNkI7SUFFekQseUJBQVksS0FBYTtlQUN2QixrQkFBTSxLQUFLLENBQUM7SUFDZCxDQUFDO0lBRUQsZ0NBQU0sR0FBTjtRQUNFLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFBQSxDQUFDO0lBUEssNEJBQVksR0FBVyxhQUFhLENBQUM7SUFEeEMsZUFBZTtRQURwQixNQUFNO09BQ0QsZUFBZSxDQVNwQjtJQUFELHNCQUFDO0NBQUEsQUFURCxDQUE4QixhQUFhLEdBUzFDO0FBRUQsZUFBZSxlQUFlLENBQUMifQ==
// CONCATENATED MODULE: ./components/magazine/blog-video-detail/index.tsx

/* harmony default export */ var blog_video_detail = (blog_video_detail_component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxlQUFlLE1BQU0sYUFBYSxDQUFDO0FBQzFDLGVBQWUsZUFBZSxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/magazine/video/style.tsx


/* harmony default export */ var video_style = ({
    container: {
        backgroundSize: 'cover',
        display: variable["display"].block,
        background: variable["colorWhite"],
        heading: {
            width: '100%',
            marginLeft: 0,
            height: 60,
            background: variable["colorWhite05"],
            display: variable["display"].flex,
            justifyContent: 'space-between',
            position: variable["position"].fixed,
            zIndex: variable["zIndex9"],
            backdropFilter: "blur(20px)",
            WebkitBackdropFilter: "blur(20px)",
            borderBottom: "1px solid " + variable["colorWhite"],
            top: 0,
            left: 0,
            logo: {
                display: variable["display"].flex,
                back: {
                    width: 60,
                    height: 60,
                    background: variable["colorBlack04"],
                    color: variable["colorWhite"],
                },
                innerBack: { width: 20 },
                iconLogo: {
                    width: 'auto',
                    height: 60,
                    color: variable["colorBlack"],
                    paddingLeft: 15,
                },
                innerIconLogo: { height: 15 },
            },
            nav: {
                display: variable["display"].flex,
                justifyContent: 'flex-end',
                paddingLeft: 15,
                paddingRight: 75,
                link: {
                    color: variable["colorBlack08"],
                    lineHeight: '60px',
                    paddingLeft: 15,
                    paddingRight: 15,
                    fontSize: 16,
                    fontFamily: variable["fontAvenirMedium"],
                    textTransform: 'uppercase'
                },
            }
        },
        wrap: {
            margin: '0 auto',
            width: '100%',
            background: variable["colorWhite"],
            paddingBottom: 10,
        },
        blogTitle: {
            display: variable["display"].block,
            width: '100%',
            textAlign: 'center',
            fontSize: 40,
            lineHeight: '42px',
            padding: '30px 160px',
            fontFamily: variable["fontPlayFairRegular"],
            fontWeight: 600,
        },
        magazineTitle: {
            fontSize: 30,
            width: '100%',
            textAlign: 'center',
            display: variable["display"].block,
            fontFamily: variable["fontTrirong"]
        },
        videoRelatedGroup: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    paddingLeft: 10,
                    paddingRight: 10
                }],
            DESKTOP: [{}],
            GENERAL: [{}]
        }),
        btnViewMore: {
            container: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        marginBottom: 10
                    }],
                DESKTOP: [{
                        marginBottom: 20
                    }],
                GENERAL: [{
                        display: variable["display"].flex,
                        justifyContent: 'center',
                        paddingTop: 10,
                    }]
            }),
            btn: {
                paddingTop: 10,
                paddingRight: 10,
                paddingBottom: 10,
                paddingLeft: 10,
                width: 150,
                borderRadius: 3,
                fontSize: 18,
                color: variable["colorBlack09"],
                textAlign: 'center',
                border: "1px solid " + variable["colorBlack06"],
                cursor: 'pointer'
            }
        },
        videoWrap: {
            container: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        marginLeft: -5,
                        marginRight: -5
                    }],
                DESKTOP: [{
                        marginLeft: -10,
                        marginRight: -10
                    }],
                GENERAL: [{
                        flexWrap: 'wrap',
                        display: variable["display"].flex
                    }]
            }),
            video: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        width: '50%',
                        paddingLeft: 5,
                        paddingRight: 5
                    }],
                DESKTOP: [{
                        width: '25%',
                        paddingLeft: 10,
                        paddingRight: 10
                    }],
                GENERAL: [{}]
            })
        },
        mobileHeader: {
            top: -1,
            height: 60,
            fontSize: 20,
            marginBottom: 20,
            letterSpacing: -0.5,
            maxWidth: "100%",
            overflow: "hidden",
            lineHeight: "60px",
            textAlign: 'center',
            whiteSpace: "nowrap",
            textOverflow: "ellipsis",
            backdropFilter: 'blur(10px)',
            WebkitBackdropFilter: 'blur(10px)',
            color: variable["colorBlack"],
            fontFamily: variable["fontTrirong"],
            background: variable["colorWhite08"],
            position: variable["position"].relative
        },
        title: {
            container: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        marginBottom: 10
                    }],
                DESKTOP: [{
                        marginBottom: 20
                    }],
                GENERAL: [{
                        top: -1,
                        height: 60,
                        fontSize: 20,
                        letterSpacing: -0.5,
                        maxWidth: "100%",
                        overflow: "hidden",
                        lineHeight: "60px",
                        whiteSpace: "nowrap",
                        textOverflow: "ellipsis",
                        backdropFilter: 'blur(10px)',
                        WebkitBackdropFilter: 'blur(10px)',
                        color: variable["colorBlack"],
                        fontFamily: variable["fontTrirong"],
                        background: variable["colorWhite08"],
                        position: variable["position"].relative
                    }]
            }),
            borderLine: {
                display: variable["display"].block,
                position: variable["position"].absolute,
                height: 4,
                width: 25,
                background: variable["colorBlack"],
                bottom: 1,
            },
            line: {
                display: variable["display"].block,
                position: variable["position"].absolute,
                height: 1,
                width: '100%',
                background: variable["colorBlack05"],
                bottom: 1,
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSw0QkFBNEIsQ0FBQztBQUN2RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFFNUQsZUFBZTtJQUNiLFNBQVMsRUFBRTtRQUNULGNBQWMsRUFBRSxPQUFPO1FBQ3ZCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7UUFDL0IsVUFBVSxFQUFFLFFBQVEsQ0FBQyxVQUFVO1FBRS9CLE9BQU8sRUFBRTtZQUNQLEtBQUssRUFBRSxNQUFNO1lBQ2IsVUFBVSxFQUFFLENBQUM7WUFDYixNQUFNLEVBQUUsRUFBRTtZQUNWLFVBQVUsRUFBRSxRQUFRLENBQUMsWUFBWTtZQUNqQyxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLGNBQWMsRUFBRSxlQUFlO1lBQy9CLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUs7WUFDakMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1lBQ3hCLGNBQWMsRUFBRSxZQUFZO1lBQzVCLG9CQUFvQixFQUFFLFlBQVk7WUFDbEMsWUFBWSxFQUFFLGVBQWEsUUFBUSxDQUFDLFVBQVk7WUFDaEQsR0FBRyxFQUFFLENBQUM7WUFDTixJQUFJLEVBQUUsQ0FBQztZQUVQLElBQUksRUFBRTtnQkFDSixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUU5QixJQUFJLEVBQUU7b0JBQ0osS0FBSyxFQUFFLEVBQUU7b0JBQ1QsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsVUFBVSxFQUFFLFFBQVEsQ0FBQyxZQUFZO29CQUNqQyxLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7aUJBQzNCO2dCQUVELFNBQVMsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUU7Z0JBRXhCLFFBQVEsRUFBRTtvQkFDUixLQUFLLEVBQUUsTUFBTTtvQkFDYixNQUFNLEVBQUUsRUFBRTtvQkFDVixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7b0JBQzFCLFdBQVcsRUFBRSxFQUFFO2lCQUNoQjtnQkFFRCxhQUFhLEVBQUUsRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFO2FBQzlCO1lBQ0QsR0FBRyxFQUFFO2dCQUNILE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLGNBQWMsRUFBRSxVQUFVO2dCQUMxQixXQUFXLEVBQUUsRUFBRTtnQkFDZixZQUFZLEVBQUUsRUFBRTtnQkFFaEIsSUFBSSxFQUFFO29CQUNKLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTtvQkFDNUIsVUFBVSxFQUFFLE1BQU07b0JBQ2xCLFdBQVcsRUFBRSxFQUFFO29CQUNmLFlBQVksRUFBRSxFQUFFO29CQUNoQixRQUFRLEVBQUUsRUFBRTtvQkFDWixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsYUFBYSxFQUFFLFdBQVc7aUJBQzNCO2FBQ0Y7U0FDRjtRQUVELElBQUksRUFBRTtZQUNKLE1BQU0sRUFBRSxRQUFRO1lBQ2hCLEtBQUssRUFBRSxNQUFNO1lBQ2IsVUFBVSxFQUFFLFFBQVEsQ0FBQyxVQUFVO1lBQy9CLGFBQWEsRUFBRSxFQUFFO1NBQ2xCO1FBRUQsU0FBUyxFQUFFO1lBQ1QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztZQUMvQixLQUFLLEVBQUUsTUFBTTtZQUNiLFNBQVMsRUFBRSxRQUFRO1lBQ25CLFFBQVEsRUFBRSxFQUFFO1lBQ1osVUFBVSxFQUFFLE1BQU07WUFDbEIsT0FBTyxFQUFFLFlBQVk7WUFDckIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxtQkFBbUI7WUFDeEMsVUFBVSxFQUFFLEdBQUc7U0FDaEI7UUFFRCxhQUFhLEVBQUU7WUFDYixRQUFRLEVBQUUsRUFBRTtZQUNaLEtBQUssRUFBRSxNQUFNO1lBQ2IsU0FBUyxFQUFFLFFBQVE7WUFDbkIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztZQUMvQixVQUFVLEVBQUUsUUFBUSxDQUFDLFdBQVc7U0FDakM7UUFFRCxpQkFBaUIsRUFBRSxZQUFZLENBQUM7WUFDOUIsTUFBTSxFQUFFLENBQUM7b0JBQ1AsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsWUFBWSxFQUFFLEVBQUU7aUJBQ2pCLENBQUM7WUFDRixPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUM7WUFDYixPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUM7U0FDZCxDQUFDO1FBRUYsV0FBVyxFQUFFO1lBQ1gsU0FBUyxFQUFFLFlBQVksQ0FBQztnQkFDdEIsTUFBTSxFQUFFLENBQUM7d0JBQ1AsWUFBWSxFQUFFLEVBQUU7cUJBQ2pCLENBQUM7Z0JBRUYsT0FBTyxFQUFFLENBQUM7d0JBQ1IsWUFBWSxFQUFFLEVBQUU7cUJBQ2pCLENBQUM7Z0JBRUYsT0FBTyxFQUFFLENBQUM7d0JBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTt3QkFDOUIsY0FBYyxFQUFFLFFBQVE7d0JBQ3hCLFVBQVUsRUFBRSxFQUFFO3FCQUNmLENBQUM7YUFDSCxDQUFDO1lBRUYsR0FBRyxFQUFFO2dCQUNILFVBQVUsRUFBRSxFQUFFO2dCQUNkLFlBQVksRUFBRSxFQUFFO2dCQUNoQixhQUFhLEVBQUUsRUFBRTtnQkFDakIsV0FBVyxFQUFFLEVBQUU7Z0JBQ2YsS0FBSyxFQUFFLEdBQUc7Z0JBQ1YsWUFBWSxFQUFFLENBQUM7Z0JBQ2YsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxZQUFZO2dCQUM1QixTQUFTLEVBQUUsUUFBUTtnQkFDbkIsTUFBTSxFQUFFLGVBQWEsUUFBUSxDQUFDLFlBQWM7Z0JBQzVDLE1BQU0sRUFBRSxTQUFTO2FBQ2xCO1NBQ0Y7UUFFRCxTQUFTLEVBQUU7WUFDVCxTQUFTLEVBQUUsWUFBWSxDQUFDO2dCQUN0QixNQUFNLEVBQUUsQ0FBQzt3QkFDUCxVQUFVLEVBQUUsQ0FBQyxDQUFDO3dCQUNkLFdBQVcsRUFBRSxDQUFDLENBQUM7cUJBQ2hCLENBQUM7Z0JBRUYsT0FBTyxFQUFFLENBQUM7d0JBQ1IsVUFBVSxFQUFFLENBQUMsRUFBRTt3QkFDZixXQUFXLEVBQUUsQ0FBQyxFQUFFO3FCQUNqQixDQUFDO2dCQUVGLE9BQU8sRUFBRSxDQUFDO3dCQUNSLFFBQVEsRUFBRSxNQUFNO3dCQUNoQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO3FCQUMvQixDQUFDO2FBQ0gsQ0FBQztZQUVGLEtBQUssRUFBRSxZQUFZLENBQUM7Z0JBQ2xCLE1BQU0sRUFBRSxDQUFDO3dCQUNQLEtBQUssRUFBRSxLQUFLO3dCQUNaLFdBQVcsRUFBRSxDQUFDO3dCQUNkLFlBQVksRUFBRSxDQUFDO3FCQUNoQixDQUFDO2dCQUNGLE9BQU8sRUFBRSxDQUFDO3dCQUNSLEtBQUssRUFBRSxLQUFLO3dCQUNaLFdBQVcsRUFBRSxFQUFFO3dCQUNmLFlBQVksRUFBRSxFQUFFO3FCQUNqQixDQUFDO2dCQUNGLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQzthQUNkLENBQUM7U0FDSDtRQUVELFlBQVksRUFBRTtZQUNaLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFDUCxNQUFNLEVBQUUsRUFBRTtZQUNWLFFBQVEsRUFBRSxFQUFFO1lBQ1osWUFBWSxFQUFFLEVBQUU7WUFDaEIsYUFBYSxFQUFFLENBQUMsR0FBRztZQUNuQixRQUFRLEVBQUUsTUFBTTtZQUNoQixRQUFRLEVBQUUsUUFBUTtZQUNsQixVQUFVLEVBQUUsTUFBTTtZQUNsQixTQUFTLEVBQUUsUUFBUTtZQUNuQixVQUFVLEVBQUUsUUFBUTtZQUNwQixZQUFZLEVBQUUsVUFBVTtZQUN4QixjQUFjLEVBQUUsWUFBWTtZQUM1QixvQkFBb0IsRUFBRSxZQUFZO1lBQ2xDLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtZQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLFdBQVc7WUFDaEMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxZQUFZO1lBQ2pDLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7U0FDckM7UUFFRCxLQUFLLEVBQUU7WUFDTCxTQUFTLEVBQUUsWUFBWSxDQUFDO2dCQUN0QixNQUFNLEVBQUUsQ0FBQzt3QkFDUCxZQUFZLEVBQUUsRUFBRTtxQkFDakIsQ0FBQztnQkFFRixPQUFPLEVBQUUsQ0FBQzt3QkFDUixZQUFZLEVBQUUsRUFBRTtxQkFDakIsQ0FBQztnQkFFRixPQUFPLEVBQUUsQ0FBQzt3QkFDUixHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLE1BQU0sRUFBRSxFQUFFO3dCQUNWLFFBQVEsRUFBRSxFQUFFO3dCQUNaLGFBQWEsRUFBRSxDQUFDLEdBQUc7d0JBQ25CLFFBQVEsRUFBRSxNQUFNO3dCQUNoQixRQUFRLEVBQUUsUUFBUTt3QkFDbEIsVUFBVSxFQUFFLE1BQU07d0JBQ2xCLFVBQVUsRUFBRSxRQUFRO3dCQUNwQixZQUFZLEVBQUUsVUFBVTt3QkFDeEIsY0FBYyxFQUFFLFlBQVk7d0JBQzVCLG9CQUFvQixFQUFFLFlBQVk7d0JBQ2xDLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTt3QkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxXQUFXO3dCQUNoQyxVQUFVLEVBQUUsUUFBUSxDQUFDLFlBQVk7d0JBQ2pDLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7cUJBQ3JDLENBQUM7YUFDSCxDQUFDO1lBRUYsVUFBVSxFQUFFO2dCQUNWLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7Z0JBQy9CLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLE1BQU0sRUFBRSxDQUFDO2dCQUNULEtBQUssRUFBRSxFQUFFO2dCQUNULFVBQVUsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDL0IsTUFBTSxFQUFFLENBQUM7YUFDVjtZQUVELElBQUksRUFBRTtnQkFDSixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO2dCQUMvQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxNQUFNLEVBQUUsQ0FBQztnQkFDVCxLQUFLLEVBQUUsTUFBTTtnQkFDYixVQUFVLEVBQUUsUUFBUSxDQUFDLFlBQVk7Z0JBQ2pDLE1BQU0sRUFBRSxDQUFDO2FBQ1Y7U0FDRjtLQUNGO0NBQ00sQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/magazine/video/view.tsx
var video_view_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};








var renderMainContainer = function (_a) {
    var mainVideo = _a.mainVideo;
    return react["createElement"](blog_video_detail, { mainVideo: mainVideo });
};
var renderViewMoreBtn = function (_a) {
    var isLoading = _a.isLoading, handleViewMore = _a.handleViewMore;
    return (react["createElement"]("div", null, isLoading
        ? react["createElement"](loading["a" /* default */], null)
        : (react["createElement"]("div", { style: video_style.container.btnViewMore.container },
            react["createElement"]("div", { style: video_style.container.btnViewMore.btn, onClick: handleViewMore }, "Xem th\u00EAm")))));
};
var handleRenderVideoItem = function (item) {
    var videoProps = {
        item: item,
        column: 1
    };
    return (react["createElement"]("div", { key: "video-item-" + item.id, style: video_style.container.videoWrap.video },
        react["createElement"](video_item["a" /* default */], video_view_assign({}, videoProps))));
};
var renderVideoRelated = function (_a) {
    var videoList = _a.videoList, isFullyLoading = _a.isFullyLoading, isLoading = _a.isLoading, handleViewMore = _a.handleViewMore;
    var mobileHeader = function () { return react["createElement"]("div", { style: video_style.container.mobileHeader }, "Video li\u00EAn quan"); };
    var desktopHeader = function () { return (react["createElement"]("div", { style: video_style.container.title.container },
        "Video li\u00EAn quan",
        react["createElement"]("span", { style: video_style.container.title.borderLine }),
        react["createElement"]("span", { style: video_style.container.title.line }))); };
    var switchHeader = {
        MOBILE: function () { return mobileHeader(); },
        DESKTOP: function () { return desktopHeader(); }
    };
    return (react["createElement"]("div", { style: video_style.container.videoRelatedGroup },
        switchHeader[window.DEVICE_VERSION](),
        react["createElement"]("div", { style: video_style.container.videoWrap.container }, Array.isArray(videoList) && videoList.map(handleRenderVideoItem)),
        !isFullyLoading && renderViewMoreBtn({ isLoading: isLoading, handleViewMore: handleViewMore })));
};
var renderProductRelated = function (_a) {
    var productList = _a.productList;
    var productProps = {
        column: 5,
        showLike: false,
        showHeader: false,
        showViewMore: false,
        isCustomTitle: true,
        showQuickView: false,
        data: productList,
        title: 'Sản phẩm trong bài viết'
    };
    return (productList
        && 0 !== productList.length
        ? react["createElement"]("div", { style: video_style.productSlide },
            react["createElement"](slider["a" /* default */], video_view_assign({}, productProps)))
        : null);
};
var video_view_renderView = function (_a) {
    var props = _a.props, state = _a.state, handleFetchVideo = _a.handleFetchVideo, handleViewMore = _a.handleViewMore;
    var _b = props, content = _b.content, videoList = _b.magazineStore.videoList;
    var _c = state, isFullyLoading = _c.isFullyLoading, isLoading = _c.isLoading, isFetchVideo = _c.isFetchVideo;
    return (react["createElement"]("div", { style: video_style.desktop, className: 'user-select-all' },
        react["createElement"](container["default"], { style: video_style.container.wrap },
            renderMainContainer({ mainVideo: content }),
            renderProductRelated({ productList: content && content.related_boxes || [] }),
            react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                var isVisible = _a.isVisible;
                !!isVisible
                    && !isFetchVideo
                    && handleFetchVideo();
                return renderVideoRelated({ videoList: videoList, isFullyLoading: isFullyLoading, isLoading: isLoading, handleViewMore: handleViewMore });
            }))));
};
/* harmony default export */ var video_view = (video_view_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxlQUFlLE1BQU0saUJBQWlCLENBQUM7QUFFOUMsT0FBTyxPQUFPLE1BQU0sbUNBQW1DLENBQUM7QUFDeEQsT0FBTyxTQUFTLE1BQU0sNENBQTRDLENBQUM7QUFDbkUsT0FBTyxVQUFVLE1BQU0sZ0NBQWdDLENBQUM7QUFDeEQsT0FBTyxhQUFhLE1BQU0sdUNBQXVDLENBQUM7QUFDbEUsT0FBTyxlQUFlLE1BQU0sbURBQW1ELENBQUM7QUFHaEYsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRTVCLElBQU0sbUJBQW1CLEdBQUcsVUFBQyxFQUFhO1FBQVgsd0JBQVM7SUFBTyxPQUFBLG9CQUFDLGVBQWUsSUFBQyxTQUFTLEVBQUUsU0FBUyxHQUFJO0FBQXpDLENBQXlDLENBQUM7QUFFekYsSUFBTSxpQkFBaUIsR0FBRyxVQUFDLEVBQTZCO1FBQTNCLHdCQUFTLEVBQUUsa0NBQWM7SUFDcEQsTUFBTSxDQUFDLENBQ0wsaUNBRUksU0FBUztRQUNQLENBQUMsQ0FBQyxvQkFBQyxPQUFPLE9BQUc7UUFDYixDQUFDLENBQUMsQ0FDQSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsU0FBUztZQUMvQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLE9BQU8sRUFBRSxjQUFjLG9CQUFnQixDQUNoRixDQUNQLENBRUQsQ0FDUCxDQUFBO0FBQ0gsQ0FBQyxDQUFDO0FBRUYsSUFBTSxxQkFBcUIsR0FBRyxVQUFDLElBQUk7SUFDakMsSUFBTSxVQUFVLEdBQUc7UUFDakIsSUFBSSxNQUFBO1FBQ0osTUFBTSxFQUFFLENBQUM7S0FDVixDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsNkJBQUssR0FBRyxFQUFFLGdCQUFjLElBQUksQ0FBQyxFQUFJLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEtBQUs7UUFDdkUsb0JBQUMsU0FBUyxlQUFLLFVBQVUsRUFBSSxDQUN6QixDQUNQLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixJQUFNLGtCQUFrQixHQUFHLFVBQUMsRUFBd0Q7UUFBdEQsd0JBQVMsRUFBRSxrQ0FBYyxFQUFFLHdCQUFTLEVBQUUsa0NBQWM7SUFDaEYsSUFBTSxZQUFZLEdBQUcsY0FBTSxPQUFBLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLFlBQVksMkJBQXVCLEVBQS9ELENBQStELENBQUM7SUFFM0YsSUFBTSxhQUFhLEdBQUcsY0FBTSxPQUFBLENBQzFCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxTQUFTOztRQUV2Qyw4QkFBTSxLQUFLLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFTO1FBQ3hELDhCQUFNLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQVMsQ0FDNUMsQ0FDUCxFQU4yQixDQU0zQixDQUFDO0lBRUYsSUFBTSxZQUFZLEdBQUc7UUFDbkIsTUFBTSxFQUFFLGNBQU0sT0FBQSxZQUFZLEVBQUUsRUFBZCxDQUFjO1FBQzVCLE9BQU8sRUFBRSxjQUFNLE9BQUEsYUFBYSxFQUFFLEVBQWYsQ0FBZTtLQUMvQixDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsaUJBQWlCO1FBQzFDLFlBQVksQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUU7UUFDdEMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLFNBQVMsSUFDNUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxTQUFTLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQzdEO1FBQ0wsQ0FBQyxjQUFjLElBQUksaUJBQWlCLENBQUMsRUFBRSxTQUFTLFdBQUEsRUFBRSxjQUFjLGdCQUFBLEVBQUUsQ0FBQyxDQUNoRSxDQUVQLENBQUM7QUFDSixDQUFDLENBQUE7QUFFRCxJQUFNLG9CQUFvQixHQUFHLFVBQUMsRUFBZTtRQUFiLDRCQUFXO0lBQ3pDLElBQU0sWUFBWSxHQUFHO1FBQ25CLE1BQU0sRUFBRSxDQUFDO1FBQ1QsUUFBUSxFQUFFLEtBQUs7UUFDZixVQUFVLEVBQUUsS0FBSztRQUNqQixZQUFZLEVBQUUsS0FBSztRQUNuQixhQUFhLEVBQUUsSUFBSTtRQUNuQixhQUFhLEVBQUUsS0FBSztRQUNwQixJQUFJLEVBQUUsV0FBVztRQUNqQixLQUFLLEVBQUUseUJBQXlCO0tBQ2pDLENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCxXQUFXO1dBQ04sQ0FBQyxLQUFLLFdBQVcsQ0FBQyxNQUFNO1FBQzNCLENBQUMsQ0FBQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFlBQVk7WUFBRSxvQkFBQyxhQUFhLGVBQUssWUFBWSxFQUFJLENBQU07UUFDM0UsQ0FBQyxDQUFDLElBQUksQ0FDVCxDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsSUFBTSxVQUFVLEdBQUcsVUFBQyxFQUFrRDtRQUFoRCxnQkFBSyxFQUFFLGdCQUFLLEVBQUUsc0NBQWdCLEVBQUUsa0NBQWM7SUFDNUQsSUFBQSxVQUdhLEVBRmpCLG9CQUFPLEVBQ1Usc0NBQVMsQ0FDUjtJQUVkLElBQUEsVUFBNkQsRUFBM0Qsa0NBQWMsRUFBRSx3QkFBUyxFQUFFLDhCQUFZLENBQXFCO0lBRXBFLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTyxFQUFFLFNBQVMsRUFBRSxpQkFBaUI7UUFDckQsb0JBQUMsVUFBVSxJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUk7WUFDcEMsbUJBQW1CLENBQUMsRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLENBQUM7WUFDM0Msb0JBQW9CLENBQUMsRUFBRSxXQUFXLEVBQUUsT0FBTyxJQUFJLE9BQU8sQ0FBQyxhQUFhLElBQUksRUFBRSxFQUFFLENBQUM7WUFHNUUsb0JBQUMsZUFBZSxJQUFDLE1BQU0sRUFBRSxHQUFHLElBRXhCLFVBQUMsRUFBYTtvQkFBWCx3QkFBUztnQkFDVixDQUFDLENBQUMsU0FBUzt1QkFDTixDQUFDLFlBQVk7dUJBQ2IsZ0JBQWdCLEVBQUUsQ0FBQztnQkFFeEIsTUFBTSxDQUFDLGtCQUFrQixDQUFDLEVBQUUsU0FBUyxXQUFBLEVBQUUsY0FBYyxnQkFBQSxFQUFFLFNBQVMsV0FBQSxFQUFFLGNBQWMsZ0JBQUEsRUFBRSxDQUFDLENBQUE7WUFDckYsQ0FBQyxDQUVhLENBRVQsQ0FDVCxDQUNQLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/magazine/video/initialize.tsx
var video_initialize_DEFAULT_PROPS = {
    match: {
        params: {
            idVideo: ''
        }
    },
    perPage: 8
};
var INITIAL_STATE = {
    page: 1,
    isLoading: false,
    isFetchVideo: false,
    isFullyLoading: false,
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixLQUFLLEVBQUU7UUFDTCxNQUFNLEVBQUU7WUFDTixPQUFPLEVBQUUsRUFBRTtTQUNaO0tBQ0Y7SUFFRCxPQUFPLEVBQUUsQ0FBQztDQUNYLENBQUM7QUFFRixNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUc7SUFDM0IsSUFBSSxFQUFFLENBQUM7SUFDUCxTQUFTLEVBQUUsS0FBSztJQUNoQixZQUFZLEVBQUUsS0FBSztJQUNuQixjQUFjLEVBQUUsS0FBSztDQUN0QixDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/magazine/video/container.tsx
var container_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var container_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var container_MagazineVideoContainer = /** @class */ (function (_super) {
    container_extends(MagazineVideoContainer, _super);
    function MagazineVideoContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    MagazineVideoContainer.prototype.handleViewMore = function () {
        this.setState({ page: this.state.page + 1, isLoading: true }, this.fetchVideo);
    };
    MagazineVideoContainer.prototype.handleFetchVideo = function () {
        var isFetchVideo = this.state.isFetchVideo;
        !isFetchVideo
            && this.setState({ isFetchVideo: true }, this.fetchVideo);
    };
    MagazineVideoContainer.prototype.fetchVideo = function () {
        var _a = this.props, perPage = _a.perPage, fetchMagazineList = _a.fetchMagazineList;
        var page = this.state.page;
        var paramMagazineVideo = { page: page, perPage: perPage, type: application_magazine["a" /* MAGAZINE_LIST_TYPE */].VIDEO };
        this.setState({ isLoading: true }, fetchMagazineList(paramMagazineVideo));
    };
    MagazineVideoContainer.prototype.componentWillReceiveProps = function (nextProps) {
        var _a = nextProps.magazineStore, videoPaging = _a.videoPaging, isFetchMagazineListSuccess = _a.isFetchMagazineListSuccess;
        if (!this.props.magazineStore.isFetchMagazineListSuccess && isFetchMagazineListSuccess) {
            this.setState({ isLoading: false });
            !Object(validate["j" /* isEmptyObject */])(videoPaging)
                && videoPaging.current_page === videoPaging.total_pages
                && this.setState({ isFullyLoading: true });
        }
        // if (this.props.match.params.idVideo !== idVideo) {
        //   fetchMagazineBySlug({ slug: idVideo });
        //   // Load new video page
        //   this.setState({ page: 1, isFullyLoading: false });
        //   fetchMagazineList({ page: 1, perPage, type: MAGAZINE_LIST_TYPE.VIDEO });
        // }
        // const keyHash = objectToHash({ slug: idVideo });
        // const magazineBySlugResult = isUndefined(magazineBySlug[keyHash]) ? null : magazineBySlug[keyHash].magazine;
        // if (!!magazineBySlugResult) {
        //   const currentMagazineBySlugResult = !!isUndefined(this.props.magazineStore.magazineBySlug[keyHash])
        //     ? null
        //     : this.props.magazineStore.magazineBySlug[keyHash].magazine;
        //   if (!currentMagazineBySlugResult || (currentMagazineBySlugResult.slug !== magazineBySlugResult.slug)) {
        //     this.props.updateMetaInfoAction({
        //       info: {
        //         url: `http://lxbtest.tk/magazine/video/${magazineBySlugResult.slug}`,
        //         type: "article",
        //         title: magazineBySlugResult.title,
        //         description: magazineBySlugResult.description,
        //         keyword: magazineBySlugResult.tags.join(', '),
        //         image: magazineBySlugResult.cover_image.large_url
        //       }
        //     })
        //   }
        // }
    };
    MagazineVideoContainer.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state,
            handleViewMore: this.handleViewMore.bind(this),
            handleFetchVideo: this.handleFetchVideo.bind(this)
        };
        return video_view(args);
    };
    ;
    MagazineVideoContainer.defaultProps = video_initialize_DEFAULT_PROPS;
    MagazineVideoContainer = container_decorate([
        radium
    ], MagazineVideoContainer);
    return MagazineVideoContainer;
}(react["PureComponent"]));
/* harmony default export */ var video_container = (container_MagazineVideoContainer);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDM0QsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFFaEYsT0FBTyxVQUFVLE1BQU0sUUFBUSxDQUFDO0FBRWhDLE9BQU8sRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBRzVEO0lBQXFDLDBDQUE2QjtJQUVoRSxnQ0FBWSxLQUFhO1FBQXpCLFlBQ0Usa0JBQU0sS0FBSyxDQUFDLFNBR2I7UUFEQyxLQUFJLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQzs7SUFDN0IsQ0FBQztJQUVELCtDQUFjLEdBQWQ7UUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLENBQUMsRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ2pGLENBQUM7SUFFRCxpREFBZ0IsR0FBaEI7UUFDVSxJQUFBLHNDQUFZLENBQTBCO1FBRTlDLENBQUMsWUFBWTtlQUNSLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCwyQ0FBVSxHQUFWO1FBQ1EsSUFBQSxlQUdrQixFQUZ0QixvQkFBTyxFQUNQLHdDQUFpQixDQUNNO1FBRWpCLElBQUEsc0JBQUksQ0FBZ0I7UUFFNUIsSUFBTSxrQkFBa0IsR0FBRyxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLElBQUksRUFBRSxrQkFBa0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM3RSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxFQUFFLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQztJQUM1RSxDQUFDO0lBRUQsMERBQXlCLEdBQXpCLFVBQTBCLFNBQVM7UUFDekIsSUFBQSw0QkFBMEQsRUFBekMsNEJBQVcsRUFBRSwwREFBMEIsQ0FBaUI7UUFFakYsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQywwQkFBMEIsSUFBSSwwQkFBMEIsQ0FBQyxDQUFDLENBQUM7WUFDdkYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO1lBRXBDLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQzttQkFDdEIsV0FBVyxDQUFDLFlBQVksS0FBSyxXQUFXLENBQUMsV0FBVzttQkFDcEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLGNBQWMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBQy9DLENBQUM7UUFFRCxxREFBcUQ7UUFDckQsNENBQTRDO1FBQzVDLDJCQUEyQjtRQUMzQix1REFBdUQ7UUFDdkQsNkVBQTZFO1FBQzdFLElBQUk7UUFFSixtREFBbUQ7UUFDbkQsK0dBQStHO1FBQy9HLGdDQUFnQztRQUNoQyx3R0FBd0c7UUFDeEcsYUFBYTtRQUNiLG1FQUFtRTtRQUVuRSw0R0FBNEc7UUFDNUcsd0NBQXdDO1FBQ3hDLGdCQUFnQjtRQUNoQixzRkFBc0Y7UUFDdEYsMkJBQTJCO1FBQzNCLDZDQUE2QztRQUM3Qyx5REFBeUQ7UUFDekQseURBQXlEO1FBQ3pELDREQUE0RDtRQUM1RCxVQUFVO1FBQ1YsU0FBUztRQUNULE1BQU07UUFDTixJQUFJO0lBQ04sQ0FBQztJQUVELHVDQUFNLEdBQU47UUFDRSxJQUFNLElBQUksR0FBRztZQUNYLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsY0FBYyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUM5QyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztTQUNuRCxDQUFDO1FBRUYsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBQUEsQ0FBQztJQS9FSyxtQ0FBWSxHQUFXLGFBQWEsQ0FBQztJQUR4QyxzQkFBc0I7UUFEM0IsTUFBTTtPQUNELHNCQUFzQixDQWlGM0I7SUFBRCw2QkFBQztDQUFBLEFBakZELENBQXFDLGFBQWEsR0FpRmpEO0FBRUQsZUFBZSxzQkFBc0IsQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/magazine/video/store.tsx
var connect = __webpack_require__(129).connect;



var mapStateToProps = function (state) { return ({
    magazineStore: state.magazine
}); };
var mapDispatchToProps = function (dispatch) { return ({
    fetchMagazineList: function (data) { return dispatch(Object(action_magazine["e" /* fetchMagazineListAction */])(data)); },
    updateMetaInfoAction: function (data) { return dispatch(Object(meta["a" /* updateMetaInfoAction */])(data)); },
}); };
/* harmony default export */ var store = (connect(mapStateToProps, mapDispatchToProps)(video_container));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUMvRCxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUV0RSxPQUFPLHVCQUF1QixNQUFNLGFBQWEsQ0FBQztBQUVsRCxJQUFNLGVBQWUsR0FBRyxVQUFBLEtBQUssSUFBSSxPQUFBLENBQUM7SUFDaEMsYUFBYSxFQUFFLEtBQUssQ0FBQyxRQUFRO0NBQzlCLENBQUMsRUFGK0IsQ0FFL0IsQ0FBQztBQUVILElBQU0sa0JBQWtCLEdBQUcsVUFBQSxRQUFRLElBQUksT0FBQSxDQUFDO0lBQ3RDLGlCQUFpQixFQUFFLFVBQUMsSUFBSSxJQUFLLE9BQUEsUUFBUSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxDQUFDLEVBQXZDLENBQXVDO0lBQ3BFLG9CQUFvQixFQUFFLFVBQUMsSUFBSSxJQUFLLE9BQUEsUUFBUSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDLEVBQXBDLENBQW9DO0NBQ3JFLENBQUMsRUFIcUMsQ0FHckMsQ0FBQztBQUVILGVBQWUsT0FBTyxDQUNwQixlQUFlLEVBQ2Ysa0JBQWtCLENBQ25CLENBQUMsdUJBQXVCLENBQUMsQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/magazine/video/index.tsx

/* harmony default export */ var video = (store);
// import * as React from 'react';
// import * as Loadable from 'react-loadable';
// import LazyLoading from '../../../../components/ui/lazy-loading';
// export default Loadable({
//   loader: () => import('./store'),
//   loading: () => <LazyLoading />
// });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxzQkFBc0IsTUFBTSxTQUFTLENBQUM7QUFDN0MsZUFBZSxzQkFBc0IsQ0FBQztBQUV0QyxrQ0FBa0M7QUFDbEMsOENBQThDO0FBQzlDLG9FQUFvRTtBQUVwRSw0QkFBNEI7QUFDNUIscUNBQXFDO0FBQ3JDLG1DQUFtQztBQUNuQyxNQUFNIn0=
// CONCATENATED MODULE: ./container/app-shop/magazine/detail/style.tsx

/* harmony default export */ var detail_style = ({
    mainTitle: {
        display: variable["display"].block,
        width: '100%',
        textAlign: 'center',
        fontSize: 32,
        lineHeight: '42px',
        padding: '30px 160px',
        fontFamily: variable["fontPlayFairRegular"],
        borderBottom: "1px solid " + variable["colorD2"],
        fontWeight: 600,
        marginBottom: 30,
    },
    desktop: {
        backgroundSize: 'cover',
        display: variable["display"].block,
        background: variable["colorWhite"],
        heading: {
            width: '100%',
            marginLeft: 0,
            height: 60,
            background: variable["colorWhite05"],
            display: variable["display"].flex,
            justifyContent: 'space-between',
            position: variable["position"].fixed,
            zIndex: variable["zIndex9"],
            backdropFilter: "blur(20px)",
            WebkitBackdropFilter: "blur(20px)",
            borderBottom: "1px solid " + variable["colorWhite"],
            top: 0,
            left: 0,
            logo: {
                display: variable["display"].flex,
                back: {
                    width: 60,
                    height: 60,
                    background: variable["colorBlack04"],
                    color: variable["colorWhite"],
                },
                innerBack: { width: 20 },
                iconLogo: {
                    width: 'auto',
                    height: 60,
                    color: variable["colorBlack"],
                    paddingLeft: 15,
                },
                innerIconLogo: { height: 15 },
            },
            nav: {
                display: variable["display"].flex,
                justifyContent: 'flex-end',
                paddingLeft: 15,
                paddingRight: 75,
                link: {
                    color: variable["colorBlack08"],
                    lineHeight: '60px',
                    paddingLeft: 15,
                    paddingRight: 15,
                    fontSize: 16,
                    fontFamily: variable["fontAvenirMedium"],
                    textTransform: 'uppercase'
                },
            }
        },
        wrap: {
            margin: '0 auto',
            width: '100%',
            background: variable["colorWhite"],
            paddingBottom: 10,
        },
        blogTitle: {
            display: variable["display"].block,
            width: '100%',
            textAlign: 'center',
            fontSize: 40,
            lineHeight: '42px',
            padding: '30px 160px',
            fontFamily: variable["fontPlayFairRegular"],
            fontWeight: 600,
        },
    },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSw0QkFBNEIsQ0FBQztBQUV2RCxlQUFlO0lBQ2IsU0FBUyxFQUFFO1FBQ1QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztRQUMvQixLQUFLLEVBQUUsTUFBTTtRQUNiLFNBQVMsRUFBRSxRQUFRO1FBQ25CLFFBQVEsRUFBRSxFQUFFO1FBQ1osVUFBVSxFQUFFLE1BQU07UUFDbEIsT0FBTyxFQUFFLFlBQVk7UUFDckIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxtQkFBbUI7UUFDeEMsWUFBWSxFQUFFLGVBQWEsUUFBUSxDQUFDLE9BQVM7UUFDN0MsVUFBVSxFQUFFLEdBQUc7UUFDZixZQUFZLEVBQUUsRUFBRTtLQUNqQjtJQUVELE9BQU8sRUFBRTtRQUNQLGNBQWMsRUFBRSxPQUFPO1FBQ3ZCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7UUFDL0IsVUFBVSxFQUFFLFFBQVEsQ0FBQyxVQUFVO1FBRS9CLE9BQU8sRUFBRTtZQUNQLEtBQUssRUFBRSxNQUFNO1lBQ2IsVUFBVSxFQUFFLENBQUM7WUFDYixNQUFNLEVBQUUsRUFBRTtZQUNWLFVBQVUsRUFBRSxRQUFRLENBQUMsWUFBWTtZQUNqQyxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLGNBQWMsRUFBRSxlQUFlO1lBQy9CLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUs7WUFDakMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1lBQ3hCLGNBQWMsRUFBRSxZQUFZO1lBQzVCLG9CQUFvQixFQUFFLFlBQVk7WUFDbEMsWUFBWSxFQUFFLGVBQWEsUUFBUSxDQUFDLFVBQVk7WUFDaEQsR0FBRyxFQUFFLENBQUM7WUFDTixJQUFJLEVBQUUsQ0FBQztZQUVQLElBQUksRUFBRTtnQkFDSixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUU5QixJQUFJLEVBQUU7b0JBQ0osS0FBSyxFQUFFLEVBQUU7b0JBQ1QsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsVUFBVSxFQUFFLFFBQVEsQ0FBQyxZQUFZO29CQUNqQyxLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7aUJBQzNCO2dCQUVELFNBQVMsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUU7Z0JBRXhCLFFBQVEsRUFBRTtvQkFDUixLQUFLLEVBQUUsTUFBTTtvQkFDYixNQUFNLEVBQUUsRUFBRTtvQkFDVixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7b0JBQzFCLFdBQVcsRUFBRSxFQUFFO2lCQUNoQjtnQkFFRCxhQUFhLEVBQUUsRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFO2FBQzlCO1lBQ0QsR0FBRyxFQUFFO2dCQUNILE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLGNBQWMsRUFBRSxVQUFVO2dCQUMxQixXQUFXLEVBQUUsRUFBRTtnQkFDZixZQUFZLEVBQUUsRUFBRTtnQkFFaEIsSUFBSSxFQUFFO29CQUNKLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTtvQkFDNUIsVUFBVSxFQUFFLE1BQU07b0JBQ2xCLFdBQVcsRUFBRSxFQUFFO29CQUNmLFlBQVksRUFBRSxFQUFFO29CQUNoQixRQUFRLEVBQUUsRUFBRTtvQkFDWixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsYUFBYSxFQUFFLFdBQVc7aUJBQzNCO2FBQ0Y7U0FDRjtRQUVELElBQUksRUFBRTtZQUNKLE1BQU0sRUFBRSxRQUFRO1lBQ2hCLEtBQUssRUFBRSxNQUFNO1lBQ2IsVUFBVSxFQUFFLFFBQVEsQ0FBQyxVQUFVO1lBQy9CLGFBQWEsRUFBRSxFQUFFO1NBQ2xCO1FBRUQsU0FBUyxFQUFFO1lBQ1QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztZQUMvQixLQUFLLEVBQUUsTUFBTTtZQUNiLFNBQVMsRUFBRSxRQUFRO1lBQ25CLFFBQVEsRUFBRSxFQUFFO1lBQ1osVUFBVSxFQUFFLE1BQU07WUFDbEIsT0FBTyxFQUFFLFlBQVk7WUFDckIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxtQkFBbUI7WUFDeEMsVUFBVSxFQUFFLEdBQUc7U0FDaEI7S0FDRjtDQUNNLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/magazine/detail/view-desktop.tsx
var view_desktop_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};












var view_desktop_renderDesktop = function (_a) {
    var props = _a.props, state = _a.state, handleFetchMagazineList = _a.handleFetchMagazineList;
    var _b = props, idPost = _b.match.params.idPost, magazineDefaultParams = _b.magazineDefaultParams, _c = _b.magazineStore, magazineBySlug = _c.magazineBySlug, magazineList = _c.magazineList, magazineRelatedBlog = _c.magazineRelatedBlog;
    var _d = state, isPriorityBlock = _d.isPriorityBlock, isFetchMagazineList = _d.isFetchMagazineList;
    console.error('isPriorityBlock', isPriorityBlock);
    var keyHash = Object(encode["j" /* objectToHash */])({ slug: idPost });
    var magazineBySlugList = !Object(validate["l" /* isUndefined */])(magazineBySlug[keyHash]) ? magazineBySlug[keyHash].magazine : [];
    var magazineRelatedBlogList = magazineRelatedBlog && !Object(validate["l" /* isUndefined */])(magazineRelatedBlog[keyHash]) ? magazineRelatedBlog[keyHash] : [];
    var keyHashMagazineDefault = Object(encode["j" /* objectToHash */])(magazineDefaultParams);
    var magazineDefaultList = !Object(validate["l" /* isUndefined */])(magazineList[keyHashMagazineDefault]) ? magazineList[keyHashMagazineDefault] : [];
    var blogDetailProps = {
        magazine: magazineBySlugList,
        magazineRelatedBlogList: magazineRelatedBlogList
    };
    return magazineBySlugList.post_type === application_magazine["a" /* MAGAZINE_LIST_TYPE */].VIDEO
        ? react["createElement"](video, { content: magazineBySlugList })
        : (react["createElement"]("div", { style: detail_style.desktop, className: 'user-select-all' },
            react["createElement"](container["default"], { style: detail_style.desktop.wrap },
                react["createElement"](blog_detail, view_desktop_assign({}, blogDetailProps)),
                !isPriorityBlock
                    ? (react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                        var isVisible = _a.isVisible;
                        !!isVisible
                            && !isFetchMagazineList
                            && !!handleFetchMagazineList
                            && handleFetchMagazineList();
                        var magazineCategoryProps = {
                            type: magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].TRENDING.type,
                            list: Array.isArray(magazineDefaultList) ? magazineDefaultList.slice(0, 4) : [],
                            titleStyle: { textAlign: 'center', width: '100%', display: 'block', fontFamily: 'trirong-regular, serif', fontSize: 30 }
                        };
                        return react["createElement"](category["a" /* default */], view_desktop_assign({}, magazineCategoryProps));
                    })) : react["createElement"](loading["a" /* default */], { style: { height: 400 } }))));
};
/* harmony default export */ var detail_view_desktop = (view_desktop_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1kZXNrdG9wLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1kZXNrdG9wLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sZUFBZSxNQUFNLGlCQUFpQixDQUFDO0FBRTlDLE9BQU8sT0FBTyxNQUFNLG1DQUFtQyxDQUFDO0FBQ3hELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDaEYsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFFN0YsT0FBTyxVQUFVLE1BQU0sNkNBQTZDLENBQUM7QUFDckUsT0FBTyxnQkFBZ0IsTUFBTSwwQ0FBMEMsQ0FBQztBQUV4RSxPQUFPLGFBQWEsTUFBTSxVQUFVLENBQUM7QUFFckMsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRTVCLE9BQU8sVUFBVSxNQUFNLGdDQUFnQyxDQUFDO0FBRXhELElBQU0sYUFBYSxHQUFHLFVBQUMsRUFBeUM7UUFBdkMsZ0JBQUssRUFBRSxnQkFBSyxFQUFFLG9EQUF1QjtJQUN0RCxJQUFBLFVBSWEsRUFIRSwrQkFBTSxFQUN6QixnREFBcUIsRUFDckIscUJBQW9FLEVBQW5ELGtDQUFjLEVBQUUsOEJBQVksRUFBRSw0Q0FBbUIsQ0FDaEQ7SUFFZCxJQUFBLFVBQTBELEVBQXhELG9DQUFlLEVBQUUsNENBQW1CLENBQXFCO0lBQ2pFLE9BQU8sQ0FBQyxLQUFLLENBQUMsaUJBQWlCLEVBQUUsZUFBZSxDQUFDLENBQUM7SUFFbEQsSUFBTSxPQUFPLEdBQUcsWUFBWSxDQUFDLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFDL0MsSUFBTSxrQkFBa0IsR0FBRyxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQ3pHLElBQU0sdUJBQXVCLEdBQUcsbUJBQW1CLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUV0SSxJQUFNLHNCQUFzQixHQUFHLFlBQVksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO0lBQ25FLElBQU0sbUJBQW1CLEdBQUcsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUUzSCxJQUFNLGVBQWUsR0FBRztRQUN0QixRQUFRLEVBQUUsa0JBQWtCO1FBQzVCLHVCQUF1QixFQUFFLHVCQUF1QjtLQUNqRCxDQUFDO0lBRUYsTUFBTSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsS0FBSyxrQkFBa0IsQ0FBQyxLQUFLO1FBQzlELENBQUMsQ0FBQyxvQkFBQyxhQUFhLElBQUMsT0FBTyxFQUFFLGtCQUFrQixHQUFJO1FBQ2hELENBQUMsQ0FBQyxDQUNBLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTyxFQUFFLFNBQVMsRUFBRSxpQkFBaUI7WUFDckQsb0JBQUMsVUFBVSxJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQ25DLG9CQUFDLFVBQVUsZUFBSyxlQUFlLEVBQUk7Z0JBRWpDLENBQUMsZUFBZTtvQkFDZCxDQUFDLENBQUMsQ0FDQSxvQkFBQyxlQUFlLElBQUMsTUFBTSxFQUFFLEdBQUcsSUFFeEIsVUFBQyxFQUFhOzRCQUFYLHdCQUFTO3dCQUNWLENBQUMsQ0FBQyxTQUFTOytCQUNOLENBQUMsbUJBQW1COytCQUNwQixDQUFDLENBQUMsdUJBQXVCOytCQUN6Qix1QkFBdUIsRUFBRSxDQUFDO3dCQUUvQixJQUFNLHFCQUFxQixHQUFHOzRCQUM1QixJQUFJLEVBQUUsc0JBQXNCLENBQUMsUUFBUSxDQUFDLElBQUk7NEJBQzFDLElBQUksRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7NEJBQy9FLFVBQVUsRUFBRSxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSx3QkFBd0IsRUFBRSxRQUFRLEVBQUUsRUFBRSxFQUFFO3lCQUN6SCxDQUFBO3dCQUVELE1BQU0sQ0FBQyxvQkFBQyxnQkFBZ0IsZUFBSyxxQkFBcUIsRUFBSSxDQUFBO29CQUN4RCxDQUFDLENBRWEsQ0FDbkIsQ0FBQyxDQUFDLENBQUMsb0JBQUMsT0FBTyxJQUFDLEtBQUssRUFBRSxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsR0FBSSxDQUVoQyxDQUNULENBQ1AsQ0FBQztBQUNOLENBQUMsQ0FBQztBQUVGLGVBQWUsYUFBYSxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/magazine/detail/view-mobile.tsx
var detail_view_mobile_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};










var view_mobile_renderMobile = function (_a) {
    var props = _a.props, state = _a.state, handleFetchMagazineList = _a.handleFetchMagazineList;
    var _b = props, magazineDefaultParams = _b.magazineDefaultParams, idPost = _b.match.params.idPost, _c = _b.magazineStore, magazineBySlug = _c.magazineBySlug, magazineList = _c.magazineList, magazineRelatedBlog = _c.magazineRelatedBlog;
    var _d = state, isPriorityBlock = _d.isPriorityBlock, isFetchMagazineList = _d.isFetchMagazineList;
    var keyHash = Object(encode["j" /* objectToHash */])({ slug: idPost });
    var magazineBySlugList = Object(validate["l" /* isUndefined */])(magazineBySlug[keyHash]) ? [] : magazineBySlug[keyHash].magazine;
    var magazineRelatedBlogList = magazineRelatedBlog && !Object(validate["l" /* isUndefined */])(magazineRelatedBlog[keyHash]) ? magazineRelatedBlog[keyHash] : [];
    var keyHashMagazineDefault = Object(encode["j" /* objectToHash */])(magazineDefaultParams);
    var magazineDefaultList = Object(validate["l" /* isUndefined */])(magazineList[keyHashMagazineDefault]) ? [] : magazineList[keyHashMagazineDefault];
    return magazineBySlugList.post_type === application_magazine["a" /* MAGAZINE_LIST_TYPE */].VIDEO
        ? react["createElement"](video, { content: magazineBySlugList })
        : (react["createElement"]("magazine-detail-container", null,
            react["createElement"](blog_detail, { magazine: magazineBySlugList, magazineRelatedBlogList: magazineRelatedBlogList }),
            !isPriorityBlock
                ? (react["createElement"](lib_default.a, { offset: 200 }, function (_a) {
                    var isVisible = _a.isVisible;
                    !!isVisible
                        && !isFetchMagazineList
                        && !!handleFetchMagazineList
                        && handleFetchMagazineList();
                    var magazineCategoryProps = {
                        title: 'Xem thêm',
                        list: Array.isArray(magazineDefaultList) ? magazineDefaultList.slice(0, 5) : [],
                        type: magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].ONE.type
                    };
                    return react["createElement"](category["a" /* default */], detail_view_mobile_assign({}, magazineCategoryProps));
                })) : react["createElement"](loading["a" /* default */], { style: { height: 400 } }),
            react["createElement"](category["a" /* default */], null)));
};
/* harmony default export */ var detail_view_mobile = (view_mobile_renderMobile);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1tb2JpbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LW1vYmlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUUvQixPQUFPLGVBQWUsTUFBTSxpQkFBaUIsQ0FBQztBQUU5QyxPQUFPLE9BQU8sTUFBTSxtQ0FBbUMsQ0FBQztBQUN4RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDekQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHFEQUFxRCxDQUFDO0FBRTdGLE9BQU8sVUFBVSxNQUFNLDZDQUE2QyxDQUFDO0FBQ3JFLE9BQU8sZ0JBQWdCLE1BQU0sMENBQTBDLENBQUM7QUFFeEUsT0FBTyxhQUFhLE1BQU0sVUFBVSxDQUFDO0FBSXJDLElBQU0sWUFBWSxHQUFHLFVBQUMsRUFBeUM7UUFBdkMsZ0JBQUssRUFBRSxnQkFBSyxFQUFFLG9EQUF1QjtJQUNyRCxJQUFBLFVBSWEsRUFIakIsZ0RBQXFCLEVBQ0YsK0JBQU0sRUFDekIscUJBQW9FLEVBQW5ELGtDQUFjLEVBQUUsOEJBQVksRUFBRSw0Q0FBbUIsQ0FDaEQ7SUFFZCxJQUFBLFVBQTBELEVBQXhELG9DQUFlLEVBQUUsNENBQW1CLENBQXFCO0lBRWpFLElBQU0sT0FBTyxHQUFHLFlBQVksQ0FBQyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO0lBQy9DLElBQU0sa0JBQWtCLEdBQUcsV0FBVyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUM7SUFDeEcsSUFBTSx1QkFBdUIsR0FBRyxtQkFBbUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBRXRJLElBQU0sc0JBQXNCLEdBQUcsWUFBWSxDQUFDLHFCQUFxQixDQUFDLENBQUM7SUFDbkUsSUFBTSxtQkFBbUIsR0FBRyxXQUFXLENBQUMsWUFBWSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsc0JBQXNCLENBQUMsQ0FBQztJQUUxSCxNQUFNLENBQUMsa0JBQWtCLENBQUMsU0FBUyxLQUFLLGtCQUFrQixDQUFDLEtBQUs7UUFDOUQsQ0FBQyxDQUFDLG9CQUFDLGFBQWEsSUFBQyxPQUFPLEVBQUUsa0JBQWtCLEdBQUk7UUFDaEQsQ0FBQyxDQUFDLENBQ0E7WUFDRSxvQkFBQyxVQUFVLElBQUMsUUFBUSxFQUFFLGtCQUFrQixFQUFFLHVCQUF1QixFQUFFLHVCQUF1QixHQUFJO1lBRTVGLENBQUMsZUFBZTtnQkFDZCxDQUFDLENBQUMsQ0FDQSxvQkFBQyxlQUFlLElBQUMsTUFBTSxFQUFFLEdBQUcsSUFFeEIsVUFBQyxFQUFhO3dCQUFYLHdCQUFTO29CQUNWLENBQUMsQ0FBQyxTQUFTOzJCQUNOLENBQUMsbUJBQW1COzJCQUNwQixDQUFDLENBQUMsdUJBQXVCOzJCQUN6Qix1QkFBdUIsRUFBRSxDQUFDO29CQUUvQixJQUFNLHFCQUFxQixHQUFHO3dCQUM1QixLQUFLLEVBQUUsVUFBVTt3QkFDakIsSUFBSSxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTt3QkFDL0UsSUFBSSxFQUFFLHNCQUFzQixDQUFDLEdBQUcsQ0FBQyxJQUFJO3FCQUN0QyxDQUFBO29CQUVELE1BQU0sQ0FBQyxvQkFBQyxnQkFBZ0IsZUFBSyxxQkFBcUIsRUFBSSxDQUFBO2dCQUN4RCxDQUFDLENBRWEsQ0FDbkIsQ0FBQyxDQUFDLENBQUMsb0JBQUMsT0FBTyxJQUFDLEtBQUssRUFBRSxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsR0FBSTtZQUUzQyxvQkFBQyxnQkFBZ0IsT0FBRyxDQUNNLENBQzdCLENBQUM7QUFDTixDQUFDLENBQUM7QUFFRixlQUFlLFlBQVksQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/magazine/detail/view.tsx


function detail_view_renderView(data) {
    var switchView = {
        MOBILE: function () { return detail_view_mobile(data); },
        DESKTOP: function () { return detail_view_desktop(data); }
    };
    return switchView[window.DEVICE_VERSION]();
}
;
/* harmony default export */ var detail_view = (detail_view_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sYUFBYSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sWUFBWSxNQUFNLGVBQWUsQ0FBQztBQUV6QyxNQUFNLHFCQUFxQixJQUFJO0lBQzdCLElBQU0sVUFBVSxHQUFHO1FBQ2pCLE1BQU0sRUFBRSxjQUFNLE9BQUEsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFsQixDQUFrQjtRQUNoQyxPQUFPLEVBQUUsY0FBTSxPQUFBLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBbkIsQ0FBbUI7S0FDbkMsQ0FBQztJQUVGLE1BQU0sQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUM7QUFDN0MsQ0FBQztBQUFBLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/magazine/detail/initialize.tsx

var detail_initialize_DEFAULT_PROPS = {
    magazineDefaultParams: { page: 1, perPage: 4, type: application_magazine["a" /* MAGAZINE_LIST_TYPE */].DEFAULT }
};
var initialize_INITIAL_STATE = {
    isPriorityBlock: true,
    isFetchMagazineList: false
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBR2hGLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixxQkFBcUIsRUFBRSxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsa0JBQWtCLENBQUMsT0FBTyxFQUFFO0NBQ3ZFLENBQUM7QUFFWixNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUc7SUFDM0IsZUFBZSxFQUFFLElBQUk7SUFDckIsbUJBQW1CLEVBQUUsS0FBSztDQUNqQixDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/magazine/detail/container.tsx
var detail_container_extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var detail_container_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var container_MagazineDetailContainer = /** @class */ (function (_super) {
    detail_container_extends(MagazineDetailContainer, _super);
    function MagazineDetailContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.state = initialize_INITIAL_STATE;
        return _this;
    }
    MagazineDetailContainer.prototype.handleFetchMagazineList = function () {
        var _a = this.state, isPriorityBlock = _a.isPriorityBlock, isFetchMagazineList = _a.isFetchMagazineList;
        if (!!isPriorityBlock) {
            return;
        }
        if (!isFetchMagazineList) {
            var _b = this.props, fetchMagazineList = _b.fetchMagazineList, magazineDefaultParams = _b.magazineDefaultParams, magazineList = _b.magazineStore.magazineList;
            Object(validate["l" /* isUndefined */])(magazineList[Object(encode["j" /* objectToHash */])(magazineDefaultParams)])
                ? fetchMagazineList(magazineDefaultParams)
                : this.state.isPriorityBlock && this.setState({ isPriorityBlock: false });
        }
    };
    MagazineDetailContainer.prototype.handleFetchMagazineBySlug = function (slug) {
        var _a = this.props, fetchMagazineBySlug = _a.fetchMagazineBySlug, magazineBySlug = _a.magazineStore.magazineBySlug;
        Object(validate["l" /* isUndefined */])(magazineBySlug[Object(encode["j" /* objectToHash */])({ slug: slug })])
            ? fetchMagazineBySlug({ slug: slug })
            : this.handleFetchMagazineRelatedBlog(slug);
    };
    MagazineDetailContainer.prototype.handleFetchMagazineRelatedBlog = function (slug) {
        var _a = this.props, fetchMagazineRelatedBlog = _a.fetchMagazineRelatedBlog, magazineRelatedBlog = _a.magazineStore.magazineRelatedBlog;
        var keyHash = Object(encode["j" /* objectToHash */])({ slug: slug });
        Object(validate["l" /* isUndefined */])(magazineRelatedBlog[keyHash])
            ? fetchMagazineRelatedBlog({ slug: slug })
            : this.state.isPriorityBlock && this.setState({ isPriorityBlock: false });
    };
    MagazineDetailContainer.prototype.componentWillMount = function () {
        var idPost = this.props.match.params.idPost;
        this.handleFetchMagazineBySlug(idPost);
    };
    MagazineDetailContainer.prototype.componentWillReceiveProps = function (nextProps) {
        var magazineStore = this.props.magazineStore;
        var idPost = nextProps.match.params.idPost, _a = nextProps.magazineStore, magazineBySlug = _a.magazineBySlug, isFetchMagazineBySlugSuccess = _a.isFetchMagazineBySlugSuccess, isFetchMagazineRelatedBlogSuccess = _a.isFetchMagazineRelatedBlogSuccess;
        magazineStore
            && !magazineStore.isFetchMagazineBySlugSuccess
            && isFetchMagazineBySlugSuccess
            && this.handleFetchMagazineRelatedBlog(idPost);
        magazineStore
            && !magazineStore.isFetchMagazineRelatedBlogSuccess
            && isFetchMagazineRelatedBlogSuccess
            && this.state.isPriorityBlock
            && this.setState({ isPriorityBlock: false });
        this.props.match.params.idPost !== idPost
            && this.handleFetchMagazineBySlug(idPost);
        var keyHash = Object(encode["j" /* objectToHash */])({ slug: idPost });
        var magazineBySlugResult = true === Object(validate["l" /* isUndefined */])(magazineBySlug[keyHash]) ? null : magazineBySlug[keyHash].magazine;
        if (!!magazineBySlugResult) {
            var currentMagazineBySlugResult = !!Object(validate["l" /* isUndefined */])(this.props.magazineStore.magazineBySlug[keyHash])
                ? null
                : this.props.magazineStore.magazineBySlug[keyHash].magazine;
            if (!currentMagazineBySlugResult || (currentMagazineBySlugResult.slug !== magazineBySlugResult.slug)) {
                this.props.updateMetaInfoAction({
                    info: {
                        url: "http://lxbtest.tk/magazine/" + magazineBySlugResult.slug,
                        type: "article",
                        title: magazineBySlugResult.title,
                        description: magazineBySlugResult.description,
                        keyword: magazineBySlugResult.tags.join(', '),
                        image: magazineBySlugResult.cover_image.large_url
                    },
                    structuredData: {
                        breadcrumbList: [
                            {
                                position: 2,
                                name: magazineBySlugResult.title,
                                item: "http://lxbtest.tk/magazine/" + magazineBySlugResult.slug
                            }
                        ]
                    }
                });
            }
        }
    };
    MagazineDetailContainer.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state,
            handleFetchMagazineList: this.handleFetchMagazineList.bind(this)
        };
        return detail_view(args);
    };
    ;
    MagazineDetailContainer.defaultProps = detail_initialize_DEFAULT_PROPS;
    MagazineDetailContainer = detail_container_decorate([
        radium
    ], MagazineDetailContainer);
    return MagazineDetailContainer;
}(react["PureComponent"]));
/* harmony default export */ var detail_container = (container_MagazineDetailContainer);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDekQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBRXhELE9BQU8sVUFBVSxNQUFNLFFBQVEsQ0FBQztBQUVoQyxPQUFPLEVBQUUsYUFBYSxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUc1RDtJQUFzQywyQ0FBNkI7SUFFakUsaUNBQVksS0FBYTtRQUF6QixZQUNFLGtCQUFNLEtBQUssQ0FBQyxTQUViO1FBREMsS0FBSSxDQUFDLEtBQUssR0FBRyxhQUF1QixDQUFDOztJQUN2QyxDQUFDO0lBRUQseURBQXVCLEdBQXZCO1FBQ1EsSUFBQSxlQUErRCxFQUE3RCxvQ0FBZSxFQUFFLDRDQUFtQixDQUEwQjtRQUV0RSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQztRQUFDLENBQUM7UUFFbEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7WUFDbkIsSUFBQSxlQUlrQixFQUh0Qix3Q0FBaUIsRUFDakIsZ0RBQXFCLEVBQ0osNENBQVksQ0FDTjtZQUV6QixXQUFXLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUM7Z0JBQzVELENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxxQkFBcUIsQ0FBQztnQkFDMUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztRQUM5RSxDQUFDO0lBQ0gsQ0FBQztJQUVELDJEQUF5QixHQUF6QixVQUEwQixJQUFJO1FBQ3RCLElBQUEsZUFHa0IsRUFGdEIsNENBQW1CLEVBQ0YsZ0RBQWMsQ0FDUjtRQUV6QixXQUFXLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ2pELENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLENBQUM7WUFDL0IsQ0FBQyxDQUFDLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQsZ0VBQThCLEdBQTlCLFVBQStCLElBQUk7UUFDM0IsSUFBQSxlQUdrQixFQUZ0QixzREFBd0IsRUFDUCwwREFBbUIsQ0FDYjtRQUV6QixJQUFNLE9BQU8sR0FBRyxZQUFZLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDLENBQUM7UUFFdkMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZDLENBQUMsQ0FBQyx3QkFBd0IsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLENBQUM7WUFDcEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUM5RSxDQUFDO0lBRUQsb0RBQWtCLEdBQWxCO1FBRXVCLElBQUEsdUNBQU0sQ0FDWjtRQUVmLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRUQsMkRBQXlCLEdBQXpCLFVBQTBCLFNBQVM7UUFDekIsSUFBQSx3Q0FBYSxDQUFnQjtRQUdoQixJQUFBLHNDQUFNLEVBQ3pCLDRCQUFrRyxFQUFqRixrQ0FBYyxFQUFFLDhEQUE0QixFQUFFLHdFQUFpQyxDQUNwRjtRQUVkLGFBQWE7ZUFDUixDQUFDLGFBQWEsQ0FBQyw0QkFBNEI7ZUFDM0MsNEJBQTRCO2VBQzVCLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUVqRCxhQUFhO2VBQ1IsQ0FBQyxhQUFhLENBQUMsaUNBQWlDO2VBQ2hELGlDQUFpQztlQUNqQyxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWU7ZUFDMUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLGVBQWUsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO1FBRS9DLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEtBQUssTUFBTTtlQUNwQyxJQUFJLENBQUMseUJBQXlCLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFNUMsSUFBTSxPQUFPLEdBQUcsWUFBWSxDQUFDLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7UUFDL0MsSUFBTSxvQkFBb0IsR0FBRyxJQUFJLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUM7UUFDckgsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQztZQUMzQixJQUFNLDJCQUEyQixHQUFHLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNqRyxDQUFDLENBQUMsSUFBSTtnQkFDTixDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQVEsQ0FBQztZQUU5RCxFQUFFLENBQUMsQ0FBQyxDQUFDLDJCQUEyQixJQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxLQUFLLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDckcsSUFBSSxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQztvQkFDOUIsSUFBSSxFQUFFO3dCQUNKLEdBQUcsRUFBRSxzQ0FBb0Msb0JBQW9CLENBQUMsSUFBTTt3QkFDcEUsSUFBSSxFQUFFLFNBQVM7d0JBQ2YsS0FBSyxFQUFFLG9CQUFvQixDQUFDLEtBQUs7d0JBQ2pDLFdBQVcsRUFBRSxvQkFBb0IsQ0FBQyxXQUFXO3dCQUM3QyxPQUFPLEVBQUUsb0JBQW9CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7d0JBQzdDLEtBQUssRUFBRSxvQkFBb0IsQ0FBQyxXQUFXLENBQUMsU0FBUztxQkFDbEQ7b0JBQ0QsY0FBYyxFQUFFO3dCQUNkLGNBQWMsRUFBRTs0QkFDZDtnQ0FDRSxRQUFRLEVBQUUsQ0FBQztnQ0FDWCxJQUFJLEVBQUUsb0JBQW9CLENBQUMsS0FBSztnQ0FDaEMsSUFBSSxFQUFFLHNDQUFvQyxvQkFBb0IsQ0FBQyxJQUFNOzZCQUN0RTt5QkFDRjtxQkFDRjtpQkFDRixDQUFDLENBQUE7WUFDSixDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUFFRCx3Q0FBTSxHQUFOO1FBQ0UsSUFBTSxJQUFJLEdBQUc7WUFDWCxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLHVCQUF1QixFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ2pFLENBQUE7UUFFRCxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFBQSxDQUFDO0lBckhLLG9DQUFZLEdBQVcsYUFBYSxDQUFDO0lBRHhDLHVCQUF1QjtRQUQ1QixNQUFNO09BQ0QsdUJBQXVCLENBdUg1QjtJQUFELDhCQUFDO0NBQUEsQUF2SEQsQ0FBc0MsYUFBYSxHQXVIbEQ7QUFFRCxlQUFlLHVCQUF1QixDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/magazine/detail/store.tsx
var store_connect = __webpack_require__(129).connect;



var store_mapStateToProps = function (state) { return ({
    magazineStore: state.magazine
}); };
var store_mapDispatchToProps = function (dispatch) { return ({
    fetchMagazineBySlug: function (_a) {
        var slug = _a.slug;
        return dispatch(Object(action_magazine["a" /* fetchMagazineBySlugAction */])({ slug: slug }));
    },
    fetchMagazineList: function (_a) {
        var page = _a.page, perPage = _a.perPage, type = _a.type;
        return dispatch(Object(action_magazine["e" /* fetchMagazineListAction */])({ page: page, perPage: perPage, type: type }));
    },
    fetchMagazineRelatedBlog: function (_a) {
        var slug = _a.slug;
        return dispatch(Object(action_magazine["f" /* fetchMagazineRelatedBlogAction */])({ slug: slug }));
    },
    fetchMagazineDashboard: function () { return dispatch(Object(action_magazine["d" /* fetchMagazineDashboardAction */])()); },
    updateMetaInfoAction: function (data) { return dispatch(Object(meta["a" /* updateMetaInfoAction */])(data)); },
}); };
/* harmony default export */ var detail_store = __webpack_exports__["default"] = (store_connect(store_mapStateToProps, store_mapDispatchToProps)(detail_container));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLEVBQ0wsdUJBQXVCLEVBQ3ZCLHlCQUF5QixFQUN6Qiw0QkFBNEIsRUFDNUIsOEJBQThCLEVBQy9CLE1BQU0sNkJBQTZCLENBQUM7QUFDckMsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFFL0QsT0FBTyx1QkFBdUIsTUFBTSxhQUFhLENBQUM7QUFFbEQsSUFBTSxlQUFlLEdBQUcsVUFBQSxLQUFLLElBQUksT0FBQSxDQUFDO0lBQ2hDLGFBQWEsRUFBRSxLQUFLLENBQUMsUUFBUTtDQUM5QixDQUFDLEVBRitCLENBRS9CLENBQUM7QUFFSCxJQUFNLGtCQUFrQixHQUFHLFVBQUEsUUFBUSxJQUFJLE9BQUEsQ0FBQztJQUN0QyxtQkFBbUIsRUFBRSxVQUFDLEVBQVE7WUFBTixjQUFJO1FBQU8sT0FBQSxRQUFRLENBQUMseUJBQXlCLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDLENBQUM7SUFBN0MsQ0FBNkM7SUFDaEYsaUJBQWlCLEVBQUUsVUFBQyxFQUF1QjtZQUFyQixjQUFJLEVBQUUsb0JBQU8sRUFBRSxjQUFJO1FBQU8sT0FBQSxRQUFRLENBQUMsdUJBQXVCLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDLENBQUM7SUFBMUQsQ0FBMEQ7SUFDMUcsd0JBQXdCLEVBQUUsVUFBQyxFQUFRO1lBQU4sY0FBSTtRQUFPLE9BQUEsUUFBUSxDQUFDLDhCQUE4QixDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDO0lBQWxELENBQWtEO0lBQzFGLHNCQUFzQixFQUFFLGNBQU0sT0FBQSxRQUFRLENBQUMsNEJBQTRCLEVBQUUsQ0FBQyxFQUF4QyxDQUF3QztJQUN0RSxvQkFBb0IsRUFBRSxVQUFDLElBQUksSUFBSyxPQUFBLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFwQyxDQUFvQztDQUNyRSxDQUFDLEVBTnFDLENBTXJDLENBQUM7QUFFSCxlQUFlLE9BQU8sQ0FDcEIsZUFBZSxFQUNmLGtCQUFrQixDQUNuQixDQUFDLHVCQUF1QixDQUFDLENBQUMifQ==

/***/ })

}]);