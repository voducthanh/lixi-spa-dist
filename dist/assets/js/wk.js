self.addEventListener('message', function (e) {
  var w = e && e.data ? e.data.worker : null;
  if (!w) { return }

  var data = e && e.data ? e.data.data : null;
  if (!data) { return }

  switch (w) {
    case 'AJAX': return xhrCreator(data);
    case 'PREFETCH-DNS-API':  return prefetchDnsApi();

    default: return;
  }
}, false);

function sendDataToMainThread(data) { self.postMessage(data); }

function xhrOnReadyStateChange(p) {
  var
    xhr = p.xhr,
    id = p.id;

  if (4 === xhr.readyState) {
    var jsonResponse;

    try {
      jsonResponse = JSON.parse(xhr.response) || {};
    } catch (e) {
      jsonResponse = {};
    }


    sendDataToMainThread({
      worker: 'AJAX',
      status: xhr.status,
      data: jsonResponse,
      id: id,
    });
  }
};

function xhrCreator(p) {
  var
    m = p.method,
    s = p.server,
    d = p.detail,
    v = p.version || '',
    t = p.t || '',
    c = p.c || '',
    e = p.e || '',
    f = p.f || '',
    id = p.id;

  var data = d.data ? JSON.stringify(d.data) : null;
  var xhr = new XMLHttpRequest();
  var p = s + v + d.path;
  xhr.open(m, p, true);
  xhr.setRequestHeader('Accept', 'application/json');

  if ('' !== t) { xhr.setRequestHeader('browser-info', t); }
  if ('' !== t) { xhr.setRequestHeader('hash-key', '+UU4q3C2RdRJQm1uHToLZldiui1mwOXKuE9X3Xid/ZM='); }
  if ('' !== c) { xhr.setRequestHeader('metadata', c); }
  if ('' !== e) { xhr.setRequestHeader('i18n-key', e); }
  if ('' !== f) { xhr.setRequestHeader('sessions-data', f); }

  xhr.setRequestHeader('Content-type', 'application/json');
  xhr.withCredentials = true;
  xhr.onreadystatechange = function () { xhrOnReadyStateChange({ xhr: xhr, id: id }); }
  xhr.send(data);
};

function prefetchDnsApi() {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', 'http://lxbtest.tk/web/settings/constants', true);
  xhr.setRequestHeader('Accept', 'application/json');
  xhr.setRequestHeader('Content-type', 'application/json');
  xhr.withCredentials = true;
  xhr.send();
}