importScripts('/cache-polyfill.js');

self.addEventListener('install', function (e) {
  e.waitUntil(
    caches.open('lixibox').then(function (cache) {
      let jsList = [];
      for (let index = 0; index < 84; index++) {
        jsList.push(`/${index}.js`);
      }

      return cache.addAll([
        '/',
        '/index.html',
        '/app.js',
        '/vendor.js',
        ...jsList,
      ]);
    })
  );
});
self.addEventListener('activate', event => {
  event.waitUntil(self.clients.claim());
});

self.addEventListener('fetch', event => {
  event.respondWith(
    caches.open(cacheName)
      .then(cache => cache.match(event.request, { ignoreSearch: true }))
      .then(response => {
        return response || fetch(event.request);
      })
  );
});
