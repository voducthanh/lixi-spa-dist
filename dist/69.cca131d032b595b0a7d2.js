(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[69],{

/***/ 798:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// CONCATENATED MODULE: ./api/brand.ts

var fetchBrandList = function () { return Object(restful_method["b" /* get */])({
    path: "/brands",
    description: 'Get Brand list',
    errorMesssage: "Can't get list brands. Please try again",
}); };
var fetchProductByBrandId = function (_a) {
    var id = _a.id, _b = _a.pl, pl = _b === void 0 ? '' : _b, _c = _a.ph, ph = _c === void 0 ? '' : _c, _d = _a.sort, sort = _d === void 0 ? 'newest' : _d, _e = _a.page, page = _e === void 0 ? 1 : _e, _f = _a.perPage, perPage = _f === void 0 ? 12 : _f;
    var query = "?page=" + page + "&per_page=" + perPage + "&pl=" + pl + "&ph=" + ph + "&sort=" + sort;
    return Object(restful_method["b" /* get */])({
        path: "/brands/" + id + query,
        description: 'Fetch product by brand id',
        errorMesssage: "Can't fetch product by brand id. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnJhbmQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJicmFuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFL0MsTUFBTSxDQUFDLElBQU0sY0FBYyxHQUFHLGNBQU0sT0FBQSxHQUFHLENBQUM7SUFDdEMsSUFBSSxFQUFFLFNBQVM7SUFDZixXQUFXLEVBQUUsZ0JBQWdCO0lBQzdCLGFBQWEsRUFBRSx5Q0FBeUM7Q0FDekQsQ0FBQyxFQUprQyxDQUlsQyxDQUFDO0FBR0gsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQUcsVUFBQyxFQU10QjtRQUxkLFVBQUUsRUFDRixVQUFPLEVBQVAsNEJBQU8sRUFDUCxVQUFPLEVBQVAsNEJBQU8sRUFDUCxZQUFlLEVBQWYsb0NBQWUsRUFDZixZQUFRLEVBQVIsNkJBQVEsRUFDUixlQUFZLEVBQVosaUNBQVk7SUFFWixJQUFNLEtBQUssR0FBRyxXQUFTLElBQUksa0JBQWEsT0FBTyxZQUFPLEVBQUUsWUFBTyxFQUFFLGNBQVMsSUFBTSxDQUFDO0lBRWpGLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsYUFBVyxFQUFFLEdBQUcsS0FBTztRQUM3QixXQUFXLEVBQUUsMkJBQTJCO1FBQ3hDLGFBQWEsRUFBRSxtREFBbUQ7S0FDbkUsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFBIn0=
// EXTERNAL MODULE: ./constants/api/brand.ts
var brand = __webpack_require__(113);

// CONCATENATED MODULE: ./action/brand.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fetchBrandListAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fetchProductByBrandIdAction; });


/** Fetch Brand List */
var fetchBrandListAction = function () { return function (dispatch, getState) {
    return dispatch({
        type: brand["a" /* FETCH_BRAND_LIST */],
        payload: { promise: fetchBrandList().then(function (res) { return res; }) },
    });
}; };
/** Fecth list all product by brand id */
var fetchProductByBrandIdAction = function (_a) {
    var id = _a.id, _b = _a.pl, pl = _b === void 0 ? '' : _b, _c = _a.ph, ph = _c === void 0 ? '' : _c, _d = _a.sort, sort = _d === void 0 ? '' : _d, _e = _a.page, page = _e === void 0 ? 1 : _e, _f = _a.perPage, perPage = _f === void 0 ? 12 : _f;
    return function (dispatch, getState) {
        return dispatch({
            type: brand["b" /* FETCH_PRODUCT_BY_BRAND_ID */],
            payload: { promise: fetchProductByBrandId({ id: id, page: page, perPage: perPage, pl: pl, ph: ph, sort: sort }).then(function (res) { return res; }) },
            meta: { id: id, page: page, perPage: perPage }
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnJhbmQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJicmFuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsY0FBYyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSx5QkFBeUIsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBRXJGLHVCQUF1QjtBQUN2QixNQUFNLENBQUMsSUFBTSxvQkFBb0IsR0FDL0IsY0FBTSxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7SUFDdkIsT0FBQSxRQUFRLENBQUM7UUFDUCxJQUFJLEVBQUUsZ0JBQWdCO1FBQ3RCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxjQUFjLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7S0FDeEQsQ0FBQztBQUhGLENBR0UsRUFKRSxDQUlGLENBQUM7QUFFUCx5Q0FBeUM7QUFDekMsTUFBTSxDQUFDLElBQU0sMkJBQTJCLEdBQUcsVUFBQyxFQU01QjtRQUxkLFVBQUUsRUFDRixVQUFPLEVBQVAsNEJBQU8sRUFDUCxVQUFPLEVBQVAsNEJBQU8sRUFDUCxZQUFTLEVBQVQsOEJBQVMsRUFDVCxZQUFRLEVBQVIsNkJBQVEsRUFDUixlQUFZLEVBQVosaUNBQVk7SUFDWixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUseUJBQXlCO1lBQy9CLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxxQkFBcUIsQ0FBQyxFQUFFLEVBQUUsSUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLEVBQUUsSUFBQSxFQUFFLEVBQUUsSUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDakcsSUFBSSxFQUFFLEVBQUUsRUFBRSxJQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDNUIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUMifQ==

/***/ }),

/***/ 878:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/format.ts
var format = __webpack_require__(116);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./action/brand.ts + 1 modules
var brand = __webpack_require__(798);

// CONCATENATED MODULE: ./container/app-shop/mobile-index-tab/brand/store.tsx

var mapStateToProps = function (state) { return ({
    brandStore: state.brand
}); };
var mapDispatchToProps = function (dispatch) { return ({
    fetchBrandListAction: function () { return dispatch(Object(brand["a" /* fetchBrandListAction */])()); },
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFaEUsTUFBTSxDQUFDLElBQU0sZUFBZSxHQUFHLFVBQUMsS0FBSyxJQUFLLE9BQUEsQ0FBQztJQUN6QyxVQUFVLEVBQUUsS0FBSyxDQUFDLEtBQUs7Q0FDeEIsQ0FBQyxFQUZ3QyxDQUV4QyxDQUFDO0FBRUgsTUFBTSxDQUFDLElBQU0sa0JBQWtCLEdBQUcsVUFBQyxRQUFRLElBQUssT0FBQSxDQUFDO0lBQy9DLG9CQUFvQixFQUFFLGNBQVksT0FBQSxRQUFRLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxFQUFoQyxDQUFnQztDQUNuRSxDQUFDLEVBRjhDLENBRTlDLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/mobile-index-tab/brand/initialize.tsx
var DEFAULT_PROPS = {};
var INITIAL_STATE = {
    filteredBrandList: []
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRyxFQUFZLENBQUM7QUFFMUMsTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHO0lBQzNCLGlCQUFpQixFQUFFLEVBQUU7Q0FDWixDQUFDIn0=
// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./container/app-shop/mobile-index-tab/brand/style.tsx

/* harmony default export */ var style = ({
    display: variable["display"].block,
    position: variable["position"].relative,
    header: {
        height: 50,
        maxHeight: 50,
        display: variable["display"].flex,
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 20,
        backgroundColor: variable["colorWhite"],
        position: variable["position"].fixed,
        width: '100%',
        top: 0,
        left: 0,
        zIndex: variable["zIndex9"],
        icon: {
            width: 50,
            height: 50,
            color: variable["colorBlack"]
        },
        innerIcon: {
            width: 16,
            height: 16
        },
        input: {
            flex: 10,
            height: 34,
            maxHeight: 34,
            marginLeft: 10,
            paddingLeft: 10,
            borderRadius: 17,
            background: variable["colorF7"],
            boxShadow: 'none',
            border: "1px solid " + variable["colorF0"],
            outline: 'none'
        }
    },
    brandContainer: {
        background: variable["colorWhite"],
        boxShadow: variable["shadow3"],
        width: '100%',
        height: 350,
        paddingTop: 20,
        paddingRight: 20,
        paddingBottom: 20,
        paddingLeft: 20,
        list: {
            paddingTop: 10,
            paddingRight: 10,
            paddingBottom: 10,
            paddingLeft: 10,
            marginTop: 50,
            width: '100%',
            height: '100%',
            group: {
                marginBottom: 20
            },
            heading: {
                borderBottom: "1px solid " + variable["color75"],
                fontSize: 18,
                fontFamily: variable["fontAvenirDemiBold"],
                height: 25,
                lineHeight: '27px',
                marginBottom: 10,
                backgroundColor: variable["colorWhite"],
                top: 0
            },
            item: {
                fontSize: 13,
                lineHeight: '40px',
                cursor: 'pointer',
                textDecoration: 'none',
                color: variable["color75"],
            }
        }
    },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSw0QkFBNEIsQ0FBQztBQUV2RCxlQUFlO0lBQ2IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztJQUMvQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO0lBRXBDLE1BQU0sRUFBRTtRQUNOLE1BQU0sRUFBRSxFQUFFO1FBQ1YsU0FBUyxFQUFFLEVBQUU7UUFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1FBQzlCLFVBQVUsRUFBRSxRQUFRO1FBQ3BCLGNBQWMsRUFBRSxlQUFlO1FBQy9CLFlBQVksRUFBRSxFQUFFO1FBQ2hCLGVBQWUsRUFBRSxRQUFRLENBQUMsVUFBVTtRQUNwQyxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLO1FBQ2pDLEtBQUssRUFBRSxNQUFNO1FBQ2IsR0FBRyxFQUFFLENBQUM7UUFDTixJQUFJLEVBQUUsQ0FBQztRQUNQLE1BQU0sRUFBRSxRQUFRLENBQUMsT0FBTztRQUV4QixJQUFJLEVBQUU7WUFDSixLQUFLLEVBQUUsRUFBRTtZQUNULE1BQU0sRUFBRSxFQUFFO1lBQ1YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1NBQzNCO1FBRUQsU0FBUyxFQUFFO1lBQ1QsS0FBSyxFQUFFLEVBQUU7WUFDVCxNQUFNLEVBQUUsRUFBRTtTQUNYO1FBRUQsS0FBSyxFQUFFO1lBQ0wsSUFBSSxFQUFFLEVBQUU7WUFDUixNQUFNLEVBQUUsRUFBRTtZQUNWLFNBQVMsRUFBRSxFQUFFO1lBQ2IsVUFBVSxFQUFFLEVBQUU7WUFDZCxXQUFXLEVBQUUsRUFBRTtZQUNmLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFVBQVUsRUFBRSxRQUFRLENBQUMsT0FBTztZQUM1QixTQUFTLEVBQUUsTUFBTTtZQUNqQixNQUFNLEVBQUUsZUFBYSxRQUFRLENBQUMsT0FBUztZQUN2QyxPQUFPLEVBQUUsTUFBTTtTQUNoQjtLQUNGO0lBRUQsY0FBYyxFQUFFO1FBQ2QsVUFBVSxFQUFFLFFBQVEsQ0FBQyxVQUFVO1FBQy9CLFNBQVMsRUFBRSxRQUFRLENBQUMsT0FBTztRQUMzQixLQUFLLEVBQUUsTUFBTTtRQUNiLE1BQU0sRUFBRSxHQUFHO1FBQ1gsVUFBVSxFQUFFLEVBQUU7UUFDZCxZQUFZLEVBQUUsRUFBRTtRQUNoQixhQUFhLEVBQUUsRUFBRTtRQUNqQixXQUFXLEVBQUUsRUFBRTtRQUVmLElBQUksRUFBRTtZQUNKLFVBQVUsRUFBRSxFQUFFO1lBQ2QsWUFBWSxFQUFFLEVBQUU7WUFDaEIsYUFBYSxFQUFFLEVBQUU7WUFDakIsV0FBVyxFQUFFLEVBQUU7WUFDZixTQUFTLEVBQUUsRUFBRTtZQUNiLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLE1BQU07WUFFZCxLQUFLLEVBQUU7Z0JBQ0wsWUFBWSxFQUFFLEVBQUU7YUFDakI7WUFFRCxPQUFPLEVBQUU7Z0JBQ1AsWUFBWSxFQUFFLGVBQWEsUUFBUSxDQUFDLE9BQVM7Z0JBQzdDLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxRQUFRLENBQUMsa0JBQWtCO2dCQUN2QyxNQUFNLEVBQUUsRUFBRTtnQkFDVixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsWUFBWSxFQUFFLEVBQUU7Z0JBQ2hCLGVBQWUsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDcEMsR0FBRyxFQUFFLENBQUM7YUFDUDtZQUVELElBQUksRUFBRTtnQkFDSixRQUFRLEVBQUUsRUFBRTtnQkFDWixVQUFVLEVBQUUsTUFBTTtnQkFDbEIsTUFBTSxFQUFFLFNBQVM7Z0JBQ2pCLGNBQWMsRUFBRSxNQUFNO2dCQUN0QixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87YUFDeEI7U0FDRjtLQUNGO0NBQ00sQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/mobile-index-tab/brand/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};





var renderHeader = function (_a) {
    var handleGoBack = _a.handleGoBack, searchBrandFilter = _a.searchBrandFilter;
    var backIconProps = {
        name: 'close',
        style: style.header.icon,
        innerStyle: style.header.innerIcon,
        onClick: handleGoBack
    };
    var filterInputProps = {
        style: style.header.input,
        placeholder: 'Nhập thương hiệu cần tìm kiếm',
        onChange: function (e) { return searchBrandFilter(e); }
    };
    return (react["createElement"]("div", { style: style.header },
        react["createElement"]("input", __assign({}, filterInputProps)),
        react["createElement"](icon["a" /* default */], __assign({}, backIconProps))));
};
function renderComponent(_a) {
    var state = _a.state, props = _a.props, handleGoBack = _a.handleGoBack, searchBrandFilter = _a.searchBrandFilter;
    var list = props.brandStore.list;
    var filteredBrandList = state.filteredBrandList;
    var generateItemBrandProps = function (brandItem, indexItem) { return ({
        to: routing["g" /* ROUTING_BRAND_DETAIL_PATH */] + "/" + brandItem.slug,
        key: "mobile-brand-item-" + indexItem,
        style: style.brandContainer.list.item
    }); };
    var brandList = filteredBrandList
        && filteredBrandList.length > 0
        ? filteredBrandList
        : list;
    return (react["createElement"]("div", null,
        renderHeader({ handleGoBack: handleGoBack, searchBrandFilter: searchBrandFilter }),
        react["createElement"]("div", { style: style.brandContainer.list }, Array.isArray(brandList)
            && brandList.map(function (brandGroup, index) {
                var groupIndex = Object.keys(brandGroup)[0];
                var brands = brandGroup[groupIndex];
                var containerItemProps = {
                    key: "mobile-brand-group-" + index,
                    style: style.brandContainer.list.group,
                };
                return brands
                    && Array.isArray(brands)
                    && brands.length > 0
                    && (react["createElement"]("div", __assign({}, containerItemProps),
                        react["createElement"]("div", { className: 'sticky', style: style.brandContainer.list.heading }, groupIndex),
                        brands.map(function (brandItem, indexItem) {
                            var brandItemProps = generateItemBrandProps(brandItem, indexItem);
                            return (react["createElement"](react_router_dom["NavLink"], __assign({}, brandItemProps),
                                react["createElement"]("div", null, brandItem.name)));
                        })));
            }))));
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQ3RGLE9BQU8sSUFBSSxNQUFNLGdDQUFnQyxDQUFDO0FBRWxELE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUc1QixJQUFNLFlBQVksR0FBRyxVQUFDLEVBQW1DO1FBQWpDLDhCQUFZLEVBQUUsd0NBQWlCO0lBQ3JELElBQU0sYUFBYSxHQUFHO1FBQ3BCLElBQUksRUFBRSxPQUFPO1FBQ2IsS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSTtRQUN4QixVQUFVLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTO1FBQ2xDLE9BQU8sRUFBRSxZQUFZO0tBQ3RCLENBQUM7SUFFRixJQUFNLGdCQUFnQixHQUFHO1FBQ3ZCLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUs7UUFDekIsV0FBVyxFQUFFLCtCQUErQjtRQUM1QyxRQUFRLEVBQUUsVUFBQyxDQUFDLElBQUssT0FBQSxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsRUFBcEIsQ0FBb0I7S0FDdEMsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTTtRQUN0QiwwQ0FBVyxnQkFBZ0IsRUFBSTtRQUMvQixvQkFBQyxJQUFJLGVBQUssYUFBYSxFQUFJLENBQ3ZCLENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLE1BQU0sMEJBQTBCLEVBQWlEO1FBQS9DLGdCQUFLLEVBQUUsZ0JBQUssRUFBRSw4QkFBWSxFQUFFLHdDQUFpQjtJQUN2RCxJQUFBLDRCQUFJLENBQXVCO0lBQ3pDLElBQUEsMkNBQWlCLENBQXFCO0lBRTlDLElBQU0sc0JBQXNCLEdBQUcsVUFBQyxTQUFTLEVBQUUsU0FBUyxJQUFLLE9BQUEsQ0FBQztRQUN4RCxFQUFFLEVBQUsseUJBQXlCLFNBQUksU0FBUyxDQUFDLElBQU07UUFDcEQsR0FBRyxFQUFFLHVCQUFxQixTQUFXO1FBQ3JDLEtBQUssRUFBRSxLQUFLLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJO0tBQ3RDLENBQUMsRUFKdUQsQ0FJdkQsQ0FBQztJQUVILElBQU0sU0FBUyxHQUFHLGlCQUFpQjtXQUM5QixpQkFBaUIsQ0FBQyxNQUFNLEdBQUcsQ0FBQztRQUMvQixDQUFDLENBQUMsaUJBQWlCO1FBQ25CLENBQUMsQ0FBQyxJQUFJLENBQUM7SUFFVCxNQUFNLENBQUMsQ0FDTDtRQUNHLFlBQVksQ0FBQyxFQUFFLFlBQVksY0FBQSxFQUFFLGlCQUFpQixtQkFBQSxFQUFFLENBQUM7UUFDbEQsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxjQUFjLENBQUMsSUFBSSxJQUVqQyxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQztlQUNyQixTQUFTLENBQUMsR0FBRyxDQUFDLFVBQUMsVUFBVSxFQUFFLEtBQUs7Z0JBQ2pDLElBQU0sVUFBVSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzlDLElBQU0sTUFBTSxHQUFHLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFFdEMsSUFBTSxrQkFBa0IsR0FBRztvQkFDekIsR0FBRyxFQUFFLHdCQUFzQixLQUFPO29CQUNsQyxLQUFLLEVBQUUsS0FBSyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSztpQkFDdkMsQ0FBQztnQkFFRixNQUFNLENBQUMsTUFBTTt1QkFDUixLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQzt1QkFDckIsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDO3VCQUNqQixDQUNELHdDQUFTLGtCQUFrQjt3QkFDekIsNkJBQUssU0FBUyxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFHLFVBQVUsQ0FBTzt3QkFFcEYsTUFBTSxDQUFDLEdBQUcsQ0FBQyxVQUFDLFNBQVMsRUFBRSxTQUFTOzRCQUM5QixJQUFNLGNBQWMsR0FBRyxzQkFBc0IsQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUM7NEJBRXBFLE1BQU0sQ0FBQyxDQUNMLG9CQUFDLE9BQU8sZUFBSyxjQUFjO2dDQUN6QixpQ0FBTSxTQUFTLENBQUMsSUFBSSxDQUFPLENBQ25CLENBQ1gsQ0FBQzt3QkFDSixDQUFDLENBQUMsQ0FFQSxDQUNQLENBQUM7WUFDTixDQUFDLENBQUMsQ0FFQSxDQUNGLENBQ1AsQ0FBQztBQUNKLENBQUM7QUFBQSxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/mobile-index-tab/brand/container.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var connect = __webpack_require__(129).connect;






var container_BrandMobileContainer = /** @class */ (function (_super) {
    __extends(BrandMobileContainer, _super);
    function BrandMobileContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    BrandMobileContainer.prototype.componentWillMount = function () {
        Object(responsive["b" /* isDesktopVersion */])() && this.props.history.push("" + routing["kb" /* ROUTING_SHOP_INDEX */]);
    };
    BrandMobileContainer.prototype.componentDidMount = function () {
        var _a = this.props, list = _a.brandStore.list, fetchBrandListAction = _a.fetchBrandListAction;
        if (!list || list && 0 === list.length) {
            fetchBrandListAction();
        }
    };
    BrandMobileContainer.prototype.handleGoBack = function () {
        var history = this.props.history;
        history && history.goBack();
    };
    /**
      * Search filter in list
      * @param {*} event : event from search input
      * get value from seacrh input to fitler list select
    */
    BrandMobileContainer.prototype.searchBrandFilter = function (event) {
        var valueSearch = event.target.value;
        var list = this.props.brandStore.list;
        if (valueSearch.length === 0) {
            this.setState({ filteredBrandList: [] });
            return;
        }
        var filteredBrandList = Array.isArray(list) &&
            list.map(function (alphaGroup) {
                var groupIndex = Object.keys(alphaGroup)[0];
                return _a = {}, _a[groupIndex] = alphaGroup[groupIndex].filter(function (item) { return Object(format["a" /* changeAlias */])(item.name).indexOf(Object(format["a" /* changeAlias */])(valueSearch)) >= 0; }), _a;
                var _a;
            });
        this.setState({ filteredBrandList: filteredBrandList });
    };
    BrandMobileContainer.prototype.render = function () {
        var args = {
            state: this.state,
            props: this.props,
            handleGoBack: this.handleGoBack.bind(this),
            searchBrandFilter: this.searchBrandFilter.bind(this)
        };
        return renderComponent(args);
    };
    BrandMobileContainer.defaultProps = DEFAULT_PROPS;
    BrandMobileContainer = __decorate([
        radium
    ], BrandMobileContainer);
    return BrandMobileContainer;
}(react["Component"]));
;
/* harmony default export */ var container = __webpack_exports__["default"] = (connect(mapStateToProps, mapDispatchToProps)(container_BrandMobileContainer));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFDakMsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDdkQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDaEUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFFL0UsT0FBTyxFQUFFLGVBQWUsRUFBRSxrQkFBa0IsRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUU5RCxPQUFPLEVBQUUsYUFBYSxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUM1RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sUUFBUSxDQUFDO0FBR3pDO0lBQW1DLHdDQUErQjtJQUdoRSw4QkFBWSxLQUFhO1FBQXpCLFlBQ0Usa0JBQU0sS0FBSyxDQUFDLFNBRWI7UUFEQyxLQUFJLENBQUMsS0FBSyxHQUFHLGFBQXVCLENBQUM7O0lBQ3ZDLENBQUM7SUFFRCxpREFBa0IsR0FBbEI7UUFDRSxnQkFBZ0IsRUFBRSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFHLGtCQUFvQixDQUFDLENBQUM7SUFDekUsQ0FBQztJQUVELGdEQUFpQixHQUFqQjtRQUNRLElBQUEsZUFBcUUsRUFBckQseUJBQUksRUFBSSw4Q0FBb0IsQ0FBMEI7UUFFNUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUN2QyxvQkFBb0IsRUFBRSxDQUFDO1FBQ3pCLENBQUM7SUFDSCxDQUFDO0lBRUQsMkNBQVksR0FBWjtRQUNVLElBQUEsNEJBQU8sQ0FBZ0I7UUFDL0IsT0FBTyxJQUFJLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUM5QixDQUFDO0lBRUQ7Ozs7TUFJRTtJQUNGLGdEQUFpQixHQUFqQixVQUFrQixLQUFLO1FBQ3JCLElBQU0sV0FBVyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2pCLElBQUEsaUNBQUksQ0FBa0I7UUFFNUMsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxpQkFBaUIsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ3pDLE1BQU0sQ0FBQztRQUNULENBQUM7UUFFRCxJQUFNLGlCQUFpQixHQUNyQixLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztZQUNuQixJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUEsVUFBVTtnQkFDakIsSUFBTSxVQUFVLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDOUMsTUFBTSxVQUFHLEdBQUMsVUFBVSxJQUFHLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQTdELENBQTZELENBQUMsS0FBRTs7WUFDL0gsQ0FBQyxDQUFDLENBQUM7UUFFTCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsaUJBQWlCLG1CQUFBLEVBQVksQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxxQ0FBTSxHQUFOO1FBQ0UsSUFBTSxJQUFJLEdBQUc7WUFDWCxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDckQsQ0FBQztRQUVGLE1BQU0sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQXpETSxpQ0FBWSxHQUFXLGFBQWEsQ0FBQztJQUR4QyxvQkFBb0I7UUFEekIsTUFBTTtPQUNELG9CQUFvQixDQTJEekI7SUFBRCwyQkFBQztDQUFBLEFBM0RELENBQW1DLEtBQUssQ0FBQyxTQUFTLEdBMkRqRDtBQUFBLENBQUM7QUFFRixlQUFlLE9BQU8sQ0FDcEIsZUFBZSxFQUNmLGtCQUFrQixDQUNuQixDQUFDLG9CQUFvQixDQUFDLENBQUMifQ==

/***/ })

}]);