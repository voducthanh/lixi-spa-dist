(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[6],{

/***/ 754:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return updateMetaInfoAction; });
/* harmony import */ var _constants_api_meta__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(352);

var updateMetaInfoAction = function (data) { return ({
    type: _constants_api_meta__WEBPACK_IMPORTED_MODULE_0__[/* UPDATE_META_INFO */ "a"],
    payload: data
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWV0YS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1ldGEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLGdCQUFnQixFQUNqQixNQUFNLHVCQUF1QixDQUFDO0FBRS9CLE1BQU0sQ0FBQyxJQUFNLG9CQUFvQixHQUFHLFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FBQztJQUM3QyxJQUFJLEVBQUUsZ0JBQWdCO0lBQ3RCLE9BQU8sRUFBRSxJQUFJO0NBQ2QsQ0FBQyxFQUg0QyxDQUc1QyxDQUFDIn0=

/***/ }),

/***/ 790:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MAGAZINE_CATEGORY_TYPE; });
var MAGAZINE_CATEGORY_TYPE = {
    ONE: {
        type: 1
    },
    TWO: {
        type: 2
    },
    THREE: {
        type: 3
    },
    FOUR: {
        type: 4
    },
    FIVE: {
        type: 5
    },
    EXPERTS_REVIEW: {
        type: 5,
        title: 'User\'s Review'
    },
    VIDEO: {
        type: 6,
        title: 'Video',
        url: ''
    },
    TRENDING: {
        type: 7,
        title: 'Trending',
        url: ''
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFnYXppbmUtY2F0ZWdvcnkuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtYWdhemluZS1jYXRlZ29yeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxNQUFNLENBQUMsSUFBTSxzQkFBc0IsR0FBRztJQUNwQyxHQUFHLEVBQUU7UUFDSCxJQUFJLEVBQUUsQ0FBQztLQUNSO0lBRUQsR0FBRyxFQUFFO1FBQ0gsSUFBSSxFQUFFLENBQUM7S0FDUjtJQUVELEtBQUssRUFBRTtRQUNMLElBQUksRUFBRSxDQUFDO0tBQ1I7SUFFRCxJQUFJLEVBQUU7UUFDSixJQUFJLEVBQUUsQ0FBQztLQUNSO0lBRUQsSUFBSSxFQUFFO1FBQ0osSUFBSSxFQUFFLENBQUM7S0FDUjtJQUVELGNBQWMsRUFBRTtRQUNkLElBQUksRUFBRSxDQUFDO1FBQ1AsS0FBSyxFQUFFLGdCQUFnQjtLQUN4QjtJQUVELEtBQUssRUFBRTtRQUNMLElBQUksRUFBRSxDQUFDO1FBQ1AsS0FBSyxFQUFFLE9BQU87UUFDZCxHQUFHLEVBQUUsRUFBRTtLQUNSO0lBRUQsUUFBUSxFQUFFO1FBQ1IsSUFBSSxFQUFFLENBQUM7UUFDUCxLQUFLLEVBQUUsVUFBVTtRQUNqQixHQUFHLEVBQUUsRUFBRTtLQUNSO0NBQ0YsQ0FBQyJ9

/***/ }),

/***/ 797:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./constants/application/magazine-category.ts
var magazine_category = __webpack_require__(790);

// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./components/magazine/category/style.tsx


/* harmony default export */ var style = ({
    largeTitle: {
        fontSize: 22,
        lineHeight: '30px',
        width: '100%',
        fontWeight: 900,
        color: variable["colorBlack08"],
        fontFamily: variable["fontAvenirMedium"],
        display: variable["display"].block,
        marginBottom: 10,
        transition: variable["transitionNormal"],
        textTransform: 'capitalize',
    },
    title: {
        fontSize: 16,
        lineHeight: '20px',
        color: variable["colorBlack09"],
        fontFamily: variable["fontAvenirMedium"],
        width: '100%',
        fontWeight: 900,
        display: variable["display"].block,
        marginBottom: 10,
        textTransform: 'capitalize',
        textAlign: 'justify',
    },
    description: {
        fontSize: 14,
        lineHeight: '22px',
        maxHeight: 66,
        overflow: 'hidden',
        color: variable["colorBlack09"],
        textAlign: 'justify',
        width: '100%',
        marginBottom: 10,
    },
    mainDescription: {
        fontSize: Object(responsive["d" /* isMobileVersion */])() ? 14 : 15,
        lineHeight: '22px',
        color: variable["color2E"],
        width: '100%',
        fontWeight: 900,
        display: variable["display"].block,
        marginBottom: 10,
        maxHeight: 66,
        overflow: 'hidden',
    },
    imgText: {
        position: variable["position"].absolute,
        fontSize: 24,
        lineHeight: '32px',
        width: '50%',
        background: variable["colorWhite095"],
        padding: '20px 20px',
        textAlign: 'left',
        left: 0,
        bottom: 20,
        color: variable["colorBlack08"],
        textTransform: 'uppercase',
        fontFamily: variable["fontAvenirDemiBold"]
    },
    textAlignStyle: {
        left: {
            textAlign: 'left'
        },
        center: {
            textAlign: 'center'
        },
        right: {
            textAlign: 'right'
        }
    },
    magazineCategory: {
        categoryOneContent: {
            display: variable["display"].flex,
            flexDirection: Object(responsive["b" /* isDesktopVersion */])() ? 'row' : 'column',
            justifyContent: 'space-between',
            listSubItem: {
                width: Object(responsive["b" /* isDesktopVersion */])() ? 'calc(40% - 20px)' : '100%',
                marginBottom: 10,
                display: variable["display"].flex,
                justifyContent: 'space-between',
                flexDirection: Object(responsive["b" /* isDesktopVersion */])() ? 'column' : 'row',
                flexWrap: 'wrap',
                subItem: {
                    width: Object(responsive["b" /* isDesktopVersion */])() ? '100%' : '47.5%',
                    marginBottom: 10,
                    itemImage: function (image) { return ({
                        width: '100%',
                        paddingTop: '65%',
                        backgroundImage: "url(" + image + ")",
                        backgroundPosition: 'center',
                        backgroundSize: 'cover',
                        marginBottom: 10,
                        posicontion: variable["position"].relative,
                        transition: variable["transitionNormal"],
                    }); },
                },
            },
            largeItem: {
                width: Object(responsive["b" /* isDesktopVersion */])() ? '60%' : '100%',
                marginBottom: 20,
                cursor: 'pointer',
                itemImage: function (image) { return ({
                    width: '100%',
                    paddingTop: '65%',
                    backgroundImage: "url(" + image + ")",
                    backgroundSize: 'cover',
                    backgroundPosition: 'center',
                    marginBottom: 10,
                    position: variable["position"].relative,
                    transition: variable["transitionNormal"],
                }); },
                itemInfo: {
                    width: '100%',
                    textAlign: 'center',
                    height: 16,
                    marginBottom: 5,
                    marginTop: 10,
                }
            }
        },
        categoryTwoContent: {
            itemWrap: {
                display: variable["display"].flex,
                justifyContent: 'space-between',
                width: '100%',
                marginBottom: 20,
                itemImage: function (image) { return ({
                    width: 'calc(50% - 10px)',
                    paddingTop: '26%',
                    backgroundImage: "url(" + image + ")",
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    posicontion: variable["position"].relative,
                    transition: variable["transitionNormal"],
                }); },
                itemInfo: {
                    width: 'calc(50% - 10px)'
                }
            },
        },
        categoryThreeContent: {
            marginBottom: 20,
            largeItem: {
                display: variable["display"].block,
                width: '100%',
                cursor: 'pointer',
                marginBottom: 20,
                itemImage: function (imgUrl) { return ({
                    width: '100%',
                    paddingTop: '65%',
                    backgroundImage: "url(" + imgUrl + ")",
                    backgroundSize: 'cover',
                    backgroundPosition: 'center center',
                    position: variable["position"].relative,
                    transition: variable["transitionNormal"],
                    marginBottom: 10
                }); }
            }
        },
        categoryVideoContent: {
            container: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{}],
                DESKTOP: [{
                        display: variable["display"].flex,
                        justifyContent: 'space-between'
                    }],
                GENERAL: [{
                        marginBottom: 20
                    }],
            }),
            mainTitle: {
                fontSize: 30,
                lineHeight: '60px',
                color: variable["colorBlack"],
                fontFamily: variable["fontTrirong"]
            },
            largeVideoGroup: {
                container: Object(responsive["a" /* combineStyle */])({
                    MOBILE: [{
                            width: '100%'
                        }],
                    DESKTOP: [{
                            width: 'calc(50% - 20px)'
                        }],
                    GENERAL: [{
                            display: variable["display"].inlineBlock,
                            borderRadius: 5,
                            overflow: 'hidden',
                            boxShadow: variable["shadowBlur"],
                            position: variable["position"].relative,
                        }],
                }),
            },
            video: function (imgUrl) { return ({
                width: '100%',
                paddingTop: '62.5%',
                position: variable["position"].relative,
                backgroundImage: "url(" + imgUrl + ")",
                backgroundColor: variable["colorF7"],
                backgroundPosition: 'top center',
                backgroundSize: 'cover',
            }); },
            videoIcon: {
                width: 70,
                height: 70,
                position: variable["position"].absolute,
                top: '50%',
                left: '55%',
                transform: 'translate(-50%, -50%)',
                borderTop: "35px solid transparent",
                boxSizing: 'border-box',
                borderLeft: "51px solid " + variable["colorWhite"],
                borderBottom: "35px solid " + variable["colorTransparent"],
                opacity: 0.8,
            },
            contentWrap: {
                width: '100%',
                paddingTop: 12,
                paddingRight: 12,
                paddingBottom: 12,
                paddingLeft: 12,
                position: variable["position"].relative,
                backgroundColor: variable["colorWhite"],
                display: variable["display"].flex,
                flexDirection: 'column',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                title: {
                    color: variable["colorBlack"],
                    whiteSpace: 'pre-wrap',
                    fontFamily: variable["fontAvenirMedium"],
                    fontSize: 16,
                    lineHeight: '24px',
                    maxHeight: 48,
                    overflow: 'hidden',
                    marginBottom: 5,
                },
                description: {
                    fontSize: 12,
                    lineHeight: '18px',
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: 0.7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap',
                }
            },
            smallVideoGroup: {
                container: Object(responsive["a" /* combineStyle */])({
                    MOBILE: [{
                            width: '100%'
                        }],
                    DESKTOP: [{
                            width: '50%'
                        }],
                    GENERAL: [{
                            display: variable["display"].flex,
                            flexWrap: 'wrap',
                            justifyContent: 'space-between'
                        }],
                }),
                videoContainer: {
                    width: 'calc(50% - 10px)',
                    borderRadius: 5,
                    overflow: 'hidden',
                    boxShadow: variable["shadowBlur"],
                    position: variable["position"].relative,
                }
            }
        },
        categoryTrendingContent: {
            display: variable["display"].flex,
            flexWrap: 'wrap',
            justifyContent: 'space-between',
            marginBottom: 20,
            width: '100%',
            trendingWrap: {
                width: Object(responsive["d" /* isMobileVersion */])() ? 'calc(50% - 10px)' : 'calc(25% - 15px)',
                margin: '0 0 20px',
                itemImage: function (imgUrl) { return ({
                    width: '100%',
                    paddingTop: '65%',
                    backgroundImage: "url(" + imgUrl + ")",
                    backgroundPosition: 'center center',
                    backgroundSize: 'cover',
                    marginBottom: 10,
                    position: variable["position"].relative,
                    transition: variable["transitionNormal"],
                }); },
            }
        },
        boxContent: {
            display: variable["display"].flex,
            flexDirection: Object(responsive["b" /* isDesktopVersion */])() ? 'row' : 'column',
            largeItem: {
                flex: Object(responsive["b" /* isDesktopVersion */])() ? 10 : 1,
                marginBottom: 20,
                cursor: 'pointer',
                itemImage: function (image) { return ({
                    width: '100%',
                    paddingTop: '65%',
                    backgroundImage: "url(" + image + ")",
                    backgroundSize: 'cover',
                    backgroundPosition: 'center',
                    position: variable["position"].relative,
                    transition: variable["transitionNormal"],
                }); },
                itemInfo: {
                    width: '100%',
                    textAlign: 'center',
                    height: 16,
                    marginBottom: 5,
                    marginTop: 10,
                    infoTitle: {
                        fontSize: 18,
                        lineHeight: '24px',
                        color: variable["color4D"],
                        width: '100%',
                        fontWeight: 900,
                        padding: '0 10px',
                        textAlign: 'center',
                        display: variable["display"].block,
                        marginBottom: 10,
                        transition: variable["transitionNormal"],
                        textTransform: 'capitalize',
                        fontFamily: 'trirong-regular',
                    },
                    textImg: {
                        fontSize: 18,
                        lineHeight: '24px',
                        color: variable["color4D"],
                        width: '100%',
                        fontWeight: 900,
                        textAlign: 'center',
                        display: variable["display"].block,
                        marginBottom: 10,
                        transition: variable["transitionNormal"],
                        textTransform: 'capitalize',
                        fontFamily: 'trirong-regular',
                        position: variable["position"].absolute,
                        bottom: 10,
                        backgroundColor: variable["colorWhite"],
                        maxWidth: 200,
                    },
                    infoDescription: {
                        fontSize: 14,
                        lineHeight: '20px',
                        maxHeight: 60,
                        overflow: 'hidden',
                        color: variable["color75"],
                        textAlign: 'center',
                        width: '100%',
                        marginTop: Object(responsive["d" /* isMobileVersion */])() ? 10 : ''
                    }
                }
            },
            itemDescription: {
                fontSize: 14,
                lineHeight: '20px',
                maxHeight: 60,
                overflow: 'hidden',
                color: variable["color75"],
                textAlign: Object(responsive["b" /* isDesktopVersion */])() ? 'left' : 'center',
                width: '100%',
                padding: Object(responsive["b" /* isDesktopVersion */])() ? '0 10px 0 20px' : '0 10px',
            },
            listSubItem: {
                width: Object(responsive["b" /* isDesktopVersion */])() ? 312 : '100%',
                marginRight: Object(responsive["b" /* isDesktopVersion */])() ? 30 : 0,
                marginBottom: 10,
                display: variable["display"].flex,
                justifyContent: 'space-between',
                flexDirection: Object(responsive["b" /* isDesktopVersion */])() ? 'column' : 'row',
                flexWrap: 'wrap',
                subItem: {
                    width: Object(responsive["b" /* isDesktopVersion */])() ? '100%' : '47.5%',
                    itemImage: function (image) { return ({
                        width: '100%',
                        paddingTop: '65%',
                        backgroundImage: "url(" + image + ")",
                        backgroundPosition: 'center',
                        backgroundSize: 'cover',
                        marginBottom: 10,
                        posicontion: variable["position"].relative,
                        transition: variable["transitionNormal"],
                    }); }
                }
            }
        }
    },
    customStyleLoading: {
        height: 80
    },
    placeholder: {
        width: '100%',
        paddingTop: 20,
        title: {
            background: variable["colorF0"],
            display: variable["display"].block,
            width: 100,
            height: 40,
            margin: '0 auto 30px'
        },
        titleMobile: {
            margin: 0,
            textAlign: 'left'
        },
        control: {
            width: '100%',
            display: variable["display"].flex,
            justifyContent: 'space-between',
            paddingLeft: 10,
            paddingRight: 10,
            marginBottom: 20,
        },
        controlItem: {
            width: 150,
            height: 30,
            background: variable["colorF7"]
        },
        productList: {
            display: variable["display"].flex,
            flexWrap: 'wrap',
            paddingTop: 20,
        },
        productMobileItem: {
            minWidth: '50%',
            width: '50%',
        },
        productItem: {
            flex: 1,
            paddingLeft: 10,
            paddingRight: 10,
            marginBottom: 20,
            minWidth: '20%',
            width: '20%',
            image: {
                width: '100%',
                height: 'auto',
                paddingTop: '65%',
                marginBottom: 10,
            },
            text: {
                width: '100%',
                height: 65,
                marginBottom: 10,
            }
        },
    },
    mobile: {
        mainWrap: {
            display: variable["display"].block,
            paddingTop: 20,
        },
        heading: {
            paddingLeft: 10,
            display: variable["display"].inlineBlock,
            maxWidth: "100%",
            whiteSpace: "nowrap",
            overflow: "hidden",
            textOverflow: "ellipsis",
            fontFamily: variable["fontTrirong"],
            color: variable["colorBlack"],
            fontSize: 20,
            lineHeight: "40px",
            height: 40,
            letterSpacing: -0.5,
        },
        container: {
            width: '100%',
            overflowX: 'auto',
            whiteSpace: 'nowrap',
            paddingTop: 0,
            paddingLeft: 5,
            paddingRight: 5,
            marginBottom: 0,
            itemSlider: {
                margin: 5,
                width: '85%',
                maxWidth: 300,
                display: variable["display"].inlineBlock,
                borderRadius: 5,
                overflow: 'hidden',
                boxShadow: variable["shadowBlur"],
                position: variable["position"].relative,
            },
            itemSliderPanel: function (imgUrl) { return ({
                width: '100%',
                paddingTop: '62.5%',
                position: variable["position"].relative,
                backgroundImage: "url(" + imgUrl + ")",
                backgroundColor: variable["colorF7"],
                backgroundPosition: 'top center',
                backgroundSize: 'cover',
            }); },
            videoIcon: {
                width: 70,
                height: 70,
                position: variable["position"].absolute,
                top: '50%',
                left: '55%',
                transform: 'translate(-50%, -50%)',
                borderTop: '35px solid transparent',
                boxSizing: 'border-box',
                borderLeft: '51px solid white',
                borderBottom: '35px solid transparent',
                opacity: .8
            },
            info: {
                width: '100%',
                height: 110,
                padding: 12,
                position: variable["position"].relative,
                background: variable["colorWhite"],
                display: variable["display"].flex,
                flexDirection: 'column',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                image: function (imgUrl) { return ({
                    width: '100%',
                    height: '100%',
                    top: 0,
                    left: 0,
                    zIndex: -1,
                    position: variable["position"].absolute,
                    backgroundColor: variable["colorF7"],
                    backgroundImage: "url(" + imgUrl + ")",
                    backgroundPosition: 'bottom center',
                    backgroundSize: 'cover',
                    filter: 'blur(4px)',
                    transform: "scaleY(-1) scale(1.1)",
                    'WebkitBackfaceVisibility': 'hidden',
                    'WebkitPerspective': 1000,
                    'WebkitTransform': ['translate3d(0,0,0)', 'translateZ(0)'],
                    'backfaceVisibility': 'hidden',
                    perspective: 1000,
                }); },
                title: {
                    color: variable["colorBlack"],
                    whiteSpace: 'pre-wrap',
                    fontFamily: variable["fontAvenirMedium"],
                    fontSize: 14,
                    lineHeight: '22px',
                    maxHeight: '44px',
                    overflow: 'hidden',
                    marginBottom: 5
                },
                description: {
                    fontSize: 12,
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap',
                    lineHeight: '18px',
                    maxHeight: 36,
                    overflow: 'hidden',
                },
                tagList: {
                    width: '100%',
                    display: variable["display"].flex,
                    flexWrap: 'wrap',
                    height: '36px',
                    maxHeight: '36px',
                    overflow: 'hidden'
                },
                tagItem: {
                    fontSize: 12,
                    lineHeight: '18px',
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap'
                },
            }
        }
    },
    desktop: {
        heading: {
            fontSize: 30,
            lineHeight: '60px',
            color: variable["colorBlack"],
            fontFamily: variable["fontTrirong"]
        },
        container: {
            width: '100%',
            display: variable["display"].flex,
            flexWrap: 'wrap',
            justifyContent: 'space-between',
            paddingTop: 10,
            paddingLeft: 0,
            paddingRight: 0,
            marginBottom: 20,
            itemSlider: {
                marginBottom: 20,
                width: 'calc(50% - 10px)',
                display: variable["display"].inlineBlock,
                borderRadius: 5,
                overflow: 'hidden',
                boxShadow: variable["shadowBlur"],
                position: variable["position"].relative,
            },
            smallItemSlider: {
                marginBottom: 20,
                width: 'calc(25% - 15px)',
                display: variable["display"].inlineBlock,
                borderRadius: 5,
                overflow: 'hidden',
                boxShadow: variable["shadowBlur"],
                position: variable["position"].relative,
            },
            info: {
                width: '100%',
                padding: 12,
                position: variable["position"].relative,
                background: variable["colorWhite"],
                display: variable["display"].flex,
                flexDirection: 'column',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                description: {
                    fontSize: 13,
                    lineHeight: '18px',
                    maxHeight: 36,
                    overflow: 'hidden',
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 10,
                    whiteSpace: 'pre-wrap'
                },
                title: {
                    color: variable["colorBlack"],
                    whiteSpace: 'pre-wrap',
                    fontFamily: variable["fontAvenirMedium"],
                    fontSize: 16,
                    lineHeight: '24px',
                    maxHeight: '48px',
                    overflow: 'hidden',
                    marginBottom: 5
                },
                tagItem: {
                    fontSize: 12,
                    lineHeight: '18px',
                    color: variable["colorBlack"],
                    fontFamily: variable["fontAvenirMedium"],
                    opacity: .7,
                    marginRight: 12,
                    whiteSpace: 'pre-wrap'
                },
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsZUFBZSxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRTVGLGVBQWU7SUFFYixVQUFVLEVBQUU7UUFDVixRQUFRLEVBQUUsRUFBRTtRQUNaLFVBQVUsRUFBRSxNQUFNO1FBQ2xCLEtBQUssRUFBRSxNQUFNO1FBQ2IsVUFBVSxFQUFFLEdBQUc7UUFDZixLQUFLLEVBQUUsUUFBUSxDQUFDLFlBQVk7UUFDNUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7UUFDckMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztRQUMvQixZQUFZLEVBQUUsRUFBRTtRQUNoQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtRQUNyQyxhQUFhLEVBQUUsWUFBWTtLQUM1QjtJQUVELEtBQUssRUFBRTtRQUNMLFFBQVEsRUFBRSxFQUFFO1FBQ1osVUFBVSxFQUFFLE1BQU07UUFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxZQUFZO1FBQzVCLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO1FBQ3JDLEtBQUssRUFBRSxNQUFNO1FBQ2IsVUFBVSxFQUFFLEdBQUc7UUFDZixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1FBQy9CLFlBQVksRUFBRSxFQUFFO1FBQ2hCLGFBQWEsRUFBRSxZQUFZO1FBQzNCLFNBQVMsRUFBRSxTQUFTO0tBQ3JCO0lBRUQsV0FBVyxFQUFFO1FBQ1gsUUFBUSxFQUFFLEVBQUU7UUFDWixVQUFVLEVBQUUsTUFBTTtRQUNsQixTQUFTLEVBQUUsRUFBRTtRQUNiLFFBQVEsRUFBRSxRQUFRO1FBQ2xCLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTtRQUM1QixTQUFTLEVBQUUsU0FBUztRQUNwQixLQUFLLEVBQUUsTUFBTTtRQUNiLFlBQVksRUFBRSxFQUFFO0tBQ2pCO0lBRUQsZUFBZSxFQUFFO1FBQ2YsUUFBUSxFQUFFLGVBQWUsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUU7UUFDckMsVUFBVSxFQUFFLE1BQU07UUFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO1FBQ3ZCLEtBQUssRUFBRSxNQUFNO1FBQ2IsVUFBVSxFQUFFLEdBQUc7UUFDZixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1FBQy9CLFlBQVksRUFBRSxFQUFFO1FBQ2hCLFNBQVMsRUFBRSxFQUFFO1FBQ2IsUUFBUSxFQUFFLFFBQVE7S0FDbkI7SUFFRCxPQUFPLEVBQUU7UUFDUCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1FBQ3BDLFFBQVEsRUFBRSxFQUFFO1FBQ1osVUFBVSxFQUFFLE1BQU07UUFDbEIsS0FBSyxFQUFFLEtBQUs7UUFDWixVQUFVLEVBQUUsUUFBUSxDQUFDLGFBQWE7UUFDbEMsT0FBTyxFQUFFLFdBQVc7UUFDcEIsU0FBUyxFQUFFLE1BQU07UUFDakIsSUFBSSxFQUFFLENBQUM7UUFDUCxNQUFNLEVBQUUsRUFBRTtRQUNWLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTtRQUM1QixhQUFhLEVBQUUsV0FBVztRQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGtCQUFrQjtLQUN4QztJQUVELGNBQWMsRUFBRTtRQUNkLElBQUksRUFBRTtZQUNKLFNBQVMsRUFBRSxNQUFNO1NBQ2xCO1FBRUQsTUFBTSxFQUFFO1lBQ04sU0FBUyxFQUFFLFFBQVE7U0FDcEI7UUFFRCxLQUFLLEVBQUU7WUFDTCxTQUFTLEVBQUUsT0FBTztTQUNuQjtLQUNGO0lBRUQsZ0JBQWdCLEVBQUU7UUFFaEIsa0JBQWtCLEVBQUU7WUFDbEIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixhQUFhLEVBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxRQUFRO1lBQ3BELGNBQWMsRUFBRSxlQUFlO1lBRS9CLFdBQVcsRUFBRTtnQkFDWCxLQUFLLEVBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLE1BQU07Z0JBQ3ZELFlBQVksRUFBRSxFQUFFO2dCQUNoQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixjQUFjLEVBQUUsZUFBZTtnQkFDL0IsYUFBYSxFQUFFLGdCQUFnQixFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSztnQkFDcEQsUUFBUSxFQUFFLE1BQU07Z0JBRWhCLE9BQU8sRUFBRTtvQkFDUCxLQUFLLEVBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPO29CQUM1QyxZQUFZLEVBQUUsRUFBRTtvQkFFaEIsU0FBUyxFQUFFLFVBQUMsS0FBYSxJQUFLLE9BQUEsQ0FBQzt3QkFDN0IsS0FBSyxFQUFFLE1BQU07d0JBQ2IsVUFBVSxFQUFFLEtBQUs7d0JBQ2pCLGVBQWUsRUFBRSxTQUFPLEtBQUssTUFBRzt3QkFDaEMsa0JBQWtCLEVBQUUsUUFBUTt3QkFDNUIsY0FBYyxFQUFFLE9BQU87d0JBQ3ZCLFlBQVksRUFBRSxFQUFFO3dCQUNoQixXQUFXLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO3dCQUN2QyxVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtxQkFDdEMsQ0FBQyxFQVQ0QixDQVM1QjtpQkFDSDthQUNGO1lBRUQsU0FBUyxFQUFFO2dCQUNULEtBQUssRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLE1BQU07Z0JBQzFDLFlBQVksRUFBRSxFQUFFO2dCQUNoQixNQUFNLEVBQUUsU0FBUztnQkFFakIsU0FBUyxFQUFFLFVBQUMsS0FBYSxJQUFLLE9BQUEsQ0FBQztvQkFDN0IsS0FBSyxFQUFFLE1BQU07b0JBQ2IsVUFBVSxFQUFFLEtBQUs7b0JBQ2pCLGVBQWUsRUFBRSxTQUFPLEtBQUssTUFBRztvQkFDaEMsY0FBYyxFQUFFLE9BQU87b0JBQ3ZCLGtCQUFrQixFQUFFLFFBQVE7b0JBQzVCLFlBQVksRUFBRSxFQUFFO29CQUNoQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUNwQyxVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtpQkFDdEMsQ0FBQyxFQVQ0QixDQVM1QjtnQkFFRixRQUFRLEVBQUU7b0JBQ1IsS0FBSyxFQUFFLE1BQU07b0JBQ2IsU0FBUyxFQUFFLFFBQVE7b0JBQ25CLE1BQU0sRUFBRSxFQUFFO29CQUNWLFlBQVksRUFBRSxDQUFDO29CQUNmLFNBQVMsRUFBRSxFQUFFO2lCQUNkO2FBQ0Y7U0FDRjtRQUVELGtCQUFrQixFQUFFO1lBRWxCLFFBQVEsRUFBRTtnQkFDUixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixjQUFjLEVBQUUsZUFBZTtnQkFDL0IsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsWUFBWSxFQUFFLEVBQUU7Z0JBRWhCLFNBQVMsRUFBRSxVQUFDLEtBQWEsSUFBSyxPQUFBLENBQUM7b0JBQzdCLEtBQUssRUFBRSxrQkFBa0I7b0JBQ3pCLFVBQVUsRUFBRSxLQUFLO29CQUNqQixlQUFlLEVBQUUsU0FBTyxLQUFLLE1BQUc7b0JBQ2hDLGtCQUFrQixFQUFFLFFBQVE7b0JBQzVCLGNBQWMsRUFBRSxPQUFPO29CQUN2QixXQUFXLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUN2QyxVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtpQkFDdEMsQ0FBQyxFQVI0QixDQVE1QjtnQkFFRixRQUFRLEVBQUU7b0JBQ1IsS0FBSyxFQUFFLGtCQUFrQjtpQkFDMUI7YUFDRjtTQUNGO1FBRUQsb0JBQW9CLEVBQUU7WUFDcEIsWUFBWSxFQUFFLEVBQUU7WUFFaEIsU0FBUyxFQUFFO2dCQUNULE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7Z0JBQy9CLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxTQUFTO2dCQUNqQixZQUFZLEVBQUUsRUFBRTtnQkFFaEIsU0FBUyxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztvQkFDdEIsS0FBSyxFQUFFLE1BQU07b0JBQ2IsVUFBVSxFQUFFLEtBQUs7b0JBQ2pCLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztvQkFDakMsY0FBYyxFQUFFLE9BQU87b0JBQ3ZCLGtCQUFrQixFQUFFLGVBQWU7b0JBQ25DLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7b0JBQ3BDLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO29CQUNyQyxZQUFZLEVBQUUsRUFBRTtpQkFDakIsQ0FBQyxFQVRxQixDQVNyQjthQUNIO1NBQ0Y7UUFFRCxvQkFBb0IsRUFBRTtZQUNwQixTQUFTLEVBQUUsWUFBWSxDQUFDO2dCQUN0QixNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUM7Z0JBRVosT0FBTyxFQUFFLENBQUM7d0JBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTt3QkFDOUIsY0FBYyxFQUFFLGVBQWU7cUJBQ2hDLENBQUM7Z0JBRUYsT0FBTyxFQUFFLENBQUM7d0JBQ1IsWUFBWSxFQUFFLEVBQUU7cUJBQ2pCLENBQUM7YUFDSCxDQUFDO1lBRUYsU0FBUyxFQUFFO2dCQUNULFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxNQUFNO2dCQUNsQixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7Z0JBQzFCLFVBQVUsRUFBRSxRQUFRLENBQUMsV0FBVzthQUNqQztZQUVELGVBQWUsRUFBRTtnQkFDZixTQUFTLEVBQUUsWUFBWSxDQUFDO29CQUN0QixNQUFNLEVBQUUsQ0FBQzs0QkFDUCxLQUFLLEVBQUUsTUFBTTt5QkFDZCxDQUFDO29CQUVGLE9BQU8sRUFBRSxDQUFDOzRCQUNSLEtBQUssRUFBRSxrQkFBa0I7eUJBQzFCLENBQUM7b0JBRUYsT0FBTyxFQUFFLENBQUM7NEJBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVzs0QkFDckMsWUFBWSxFQUFFLENBQUM7NEJBQ2YsUUFBUSxFQUFFLFFBQVE7NEJBQ2xCLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTs0QkFDOUIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTt5QkFDckMsQ0FBQztpQkFDSCxDQUFDO2FBQ0g7WUFFRCxLQUFLLEVBQUUsVUFBQyxNQUFNLElBQUssT0FBQSxDQUFDO2dCQUNsQixLQUFLLEVBQUUsTUFBTTtnQkFDYixVQUFVLEVBQUUsT0FBTztnQkFDbkIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtnQkFDcEMsZUFBZSxFQUFFLFNBQU8sTUFBTSxNQUFHO2dCQUNqQyxlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87Z0JBQ2pDLGtCQUFrQixFQUFFLFlBQVk7Z0JBQ2hDLGNBQWMsRUFBRSxPQUFPO2FBQ3hCLENBQUMsRUFSaUIsQ0FRakI7WUFFRixTQUFTLEVBQUU7Z0JBQ1QsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtnQkFDcEMsR0FBRyxFQUFFLEtBQUs7Z0JBQ1YsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsU0FBUyxFQUFFLHVCQUF1QjtnQkFDbEMsU0FBUyxFQUFFLHdCQUF3QjtnQkFDbkMsU0FBUyxFQUFFLFlBQVk7Z0JBQ3ZCLFVBQVUsRUFBRSxnQkFBYyxRQUFRLENBQUMsVUFBWTtnQkFDL0MsWUFBWSxFQUFFLGdCQUFjLFFBQVEsQ0FBQyxnQkFBa0I7Z0JBQ3ZELE9BQU8sRUFBRSxHQUFHO2FBQ2I7WUFFRCxXQUFXLEVBQUU7Z0JBQ1gsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsVUFBVSxFQUFFLEVBQUU7Z0JBQ2QsWUFBWSxFQUFFLEVBQUU7Z0JBQ2hCLGFBQWEsRUFBRSxFQUFFO2dCQUNqQixXQUFXLEVBQUUsRUFBRTtnQkFDZixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxlQUFlLEVBQUUsUUFBUSxDQUFDLFVBQVU7Z0JBQ3BDLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLGFBQWEsRUFBRSxRQUFRO2dCQUN2QixjQUFjLEVBQUUsWUFBWTtnQkFDNUIsVUFBVSxFQUFFLFlBQVk7Z0JBRXhCLEtBQUssRUFBRTtvQkFDTCxLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7b0JBQzFCLFVBQVUsRUFBRSxVQUFVO29CQUN0QixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsUUFBUSxFQUFFLEVBQUU7b0JBQ1osVUFBVSxFQUFFLE1BQU07b0JBQ2xCLFNBQVMsRUFBRSxFQUFFO29CQUNiLFFBQVEsRUFBRSxRQUFRO29CQUNsQixZQUFZLEVBQUUsQ0FBQztpQkFDaEI7Z0JBRUQsV0FBVyxFQUFFO29CQUNYLFFBQVEsRUFBRSxFQUFFO29CQUNaLFVBQVUsRUFBRSxNQUFNO29CQUNsQixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7b0JBQzFCLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO29CQUNyQyxPQUFPLEVBQUUsR0FBRztvQkFDWixXQUFXLEVBQUUsRUFBRTtvQkFDZixVQUFVLEVBQUUsVUFBVTtpQkFDdkI7YUFDRjtZQUVELGVBQWUsRUFBRTtnQkFDZixTQUFTLEVBQUUsWUFBWSxDQUFDO29CQUN0QixNQUFNLEVBQUUsQ0FBQzs0QkFDUCxLQUFLLEVBQUUsTUFBTTt5QkFDZCxDQUFDO29CQUVGLE9BQU8sRUFBRSxDQUFDOzRCQUNSLEtBQUssRUFBRSxLQUFLO3lCQUNiLENBQUM7b0JBRUYsT0FBTyxFQUFFLENBQUM7NEJBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTs0QkFDOUIsUUFBUSxFQUFFLE1BQU07NEJBQ2hCLGNBQWMsRUFBRSxlQUFlO3lCQUNoQyxDQUFDO2lCQUNILENBQUM7Z0JBRUYsY0FBYyxFQUFFO29CQUNkLEtBQUssRUFBRSxrQkFBa0I7b0JBQ3pCLFlBQVksRUFBRSxDQUFDO29CQUNmLFFBQVEsRUFBRSxRQUFRO29CQUNsQixTQUFTLEVBQUUsUUFBUSxDQUFDLFVBQVU7b0JBQzlCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7aUJBQ3JDO2FBQ0Y7U0FDRjtRQUVELHVCQUF1QixFQUFFO1lBQ3ZCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsUUFBUSxFQUFFLE1BQU07WUFDaEIsY0FBYyxFQUFFLGVBQWU7WUFDL0IsWUFBWSxFQUFFLEVBQUU7WUFDaEIsS0FBSyxFQUFFLE1BQU07WUFFYixZQUFZLEVBQUU7Z0JBQ1osS0FBSyxFQUFFLGVBQWUsRUFBRSxDQUFDLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsa0JBQWtCO2dCQUNsRSxNQUFNLEVBQUUsVUFBVTtnQkFFbEIsU0FBUyxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztvQkFDdEIsS0FBSyxFQUFFLE1BQU07b0JBQ2IsVUFBVSxFQUFFLEtBQUs7b0JBQ2pCLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztvQkFDakMsa0JBQWtCLEVBQUUsZUFBZTtvQkFDbkMsY0FBYyxFQUFFLE9BQU87b0JBQ3ZCLFlBQVksRUFBRSxFQUFFO29CQUNoQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUNwQyxVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtpQkFDdEMsQ0FBQyxFQVRxQixDQVNyQjthQUNIO1NBQ0Y7UUFFRCxVQUFVLEVBQUU7WUFDVixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLGFBQWEsRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLFFBQVE7WUFFcEQsU0FBUyxFQUFFO2dCQUNULElBQUksRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pDLFlBQVksRUFBRSxFQUFFO2dCQUNoQixNQUFNLEVBQUUsU0FBUztnQkFFakIsU0FBUyxFQUFFLFVBQUMsS0FBYSxJQUFLLE9BQUEsQ0FBQztvQkFDN0IsS0FBSyxFQUFFLE1BQU07b0JBQ2IsVUFBVSxFQUFFLEtBQUs7b0JBQ2pCLGVBQWUsRUFBRSxTQUFPLEtBQUssTUFBRztvQkFDaEMsY0FBYyxFQUFFLE9BQU87b0JBQ3ZCLGtCQUFrQixFQUFFLFFBQVE7b0JBQzVCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7b0JBQ3BDLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO2lCQUN0QyxDQUFDLEVBUjRCLENBUTVCO2dCQUVGLFFBQVEsRUFBRTtvQkFDUixLQUFLLEVBQUUsTUFBTTtvQkFDYixTQUFTLEVBQUUsUUFBUTtvQkFDbkIsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsWUFBWSxFQUFFLENBQUM7b0JBQ2YsU0FBUyxFQUFFLEVBQUU7b0JBRWIsU0FBUyxFQUFFO3dCQUNULFFBQVEsRUFBRSxFQUFFO3dCQUNaLFVBQVUsRUFBRSxNQUFNO3dCQUNsQixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87d0JBQ3ZCLEtBQUssRUFBRSxNQUFNO3dCQUNiLFVBQVUsRUFBRSxHQUFHO3dCQUNmLE9BQU8sRUFBRSxRQUFRO3dCQUNqQixTQUFTLEVBQUUsUUFBUTt3QkFDbkIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSzt3QkFDL0IsWUFBWSxFQUFFLEVBQUU7d0JBQ2hCLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO3dCQUNyQyxhQUFhLEVBQUUsWUFBWTt3QkFDM0IsVUFBVSxFQUFFLGlCQUFpQjtxQkFDOUI7b0JBRUQsT0FBTyxFQUFFO3dCQUNQLFFBQVEsRUFBRSxFQUFFO3dCQUNaLFVBQVUsRUFBRSxNQUFNO3dCQUNsQixLQUFLLEVBQUUsUUFBUSxDQUFDLE9BQU87d0JBQ3ZCLEtBQUssRUFBRSxNQUFNO3dCQUNiLFVBQVUsRUFBRSxHQUFHO3dCQUNmLFNBQVMsRUFBRSxRQUFRO3dCQUNuQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO3dCQUMvQixZQUFZLEVBQUUsRUFBRTt3QkFDaEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7d0JBQ3JDLGFBQWEsRUFBRSxZQUFZO3dCQUMzQixVQUFVLEVBQUUsaUJBQWlCO3dCQUM3QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO3dCQUNwQyxNQUFNLEVBQUUsRUFBRTt3QkFDVixlQUFlLEVBQUUsUUFBUSxDQUFDLFVBQVU7d0JBQ3BDLFFBQVEsRUFBRSxHQUFHO3FCQUNkO29CQUVELGVBQWUsRUFBRTt3QkFDZixRQUFRLEVBQUUsRUFBRTt3QkFDWixVQUFVLEVBQUUsTUFBTTt3QkFDbEIsU0FBUyxFQUFFLEVBQUU7d0JBQ2IsUUFBUSxFQUFFLFFBQVE7d0JBQ2xCLEtBQUssRUFBRSxRQUFRLENBQUMsT0FBTzt3QkFDdkIsU0FBUyxFQUFFLFFBQVE7d0JBQ25CLEtBQUssRUFBRSxNQUFNO3dCQUNiLFNBQVMsRUFBRSxlQUFlLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFO3FCQUN2QztpQkFDRjthQUNGO1lBRUQsZUFBZSxFQUFFO2dCQUNmLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxNQUFNO2dCQUNsQixTQUFTLEVBQUUsRUFBRTtnQkFDYixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxPQUFPO2dCQUN2QixTQUFTLEVBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxRQUFRO2dCQUNqRCxLQUFLLEVBQUUsTUFBTTtnQkFDYixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxRQUFRO2FBQ3pEO1lBRUQsV0FBVyxFQUFFO2dCQUNYLEtBQUssRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLE1BQU07Z0JBQ3hDLFdBQVcsRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLFlBQVksRUFBRSxFQUFFO2dCQUNoQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixjQUFjLEVBQUUsZUFBZTtnQkFDL0IsYUFBYSxFQUFFLGdCQUFnQixFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSztnQkFDcEQsUUFBUSxFQUFFLE1BQU07Z0JBRWhCLE9BQU8sRUFBRTtvQkFDUCxLQUFLLEVBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPO29CQUU1QyxTQUFTLEVBQUUsVUFBQyxLQUFhLElBQUssT0FBQSxDQUFDO3dCQUM3QixLQUFLLEVBQUUsTUFBTTt3QkFDYixVQUFVLEVBQUUsS0FBSzt3QkFDakIsZUFBZSxFQUFFLFNBQU8sS0FBSyxNQUFHO3dCQUNoQyxrQkFBa0IsRUFBRSxRQUFRO3dCQUM1QixjQUFjLEVBQUUsT0FBTzt3QkFDdkIsWUFBWSxFQUFFLEVBQUU7d0JBQ2hCLFdBQVcsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7d0JBQ3ZDLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO3FCQUN0QyxDQUFDLEVBVDRCLENBUzVCO2lCQUNIO2FBQ0Y7U0FDRjtLQUNGO0lBRUQsa0JBQWtCLEVBQUU7UUFDbEIsTUFBTSxFQUFFLEVBQUU7S0FDWDtJQUVELFdBQVcsRUFBRTtRQUNYLEtBQUssRUFBRSxNQUFNO1FBQ2IsVUFBVSxFQUFFLEVBQUU7UUFFZCxLQUFLLEVBQUU7WUFDTCxVQUFVLEVBQUUsUUFBUSxDQUFDLE9BQU87WUFDNUIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztZQUMvQixLQUFLLEVBQUUsR0FBRztZQUNWLE1BQU0sRUFBRSxFQUFFO1lBQ1YsTUFBTSxFQUFFLGFBQWE7U0FDdEI7UUFFRCxXQUFXLEVBQUU7WUFDWCxNQUFNLEVBQUUsQ0FBQztZQUNULFNBQVMsRUFBRSxNQUFNO1NBQ2xCO1FBRUQsT0FBTyxFQUFFO1lBQ1AsS0FBSyxFQUFFLE1BQU07WUFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLGNBQWMsRUFBRSxlQUFlO1lBQy9CLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLEVBQUU7WUFDaEIsWUFBWSxFQUFFLEVBQUU7U0FDakI7UUFFRCxXQUFXLEVBQUU7WUFDWCxLQUFLLEVBQUUsR0FBRztZQUNWLE1BQU0sRUFBRSxFQUFFO1lBQ1YsVUFBVSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1NBQzdCO1FBRUQsV0FBVyxFQUFFO1lBQ1gsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixRQUFRLEVBQUUsTUFBTTtZQUNoQixVQUFVLEVBQUUsRUFBRTtTQUNmO1FBRUQsaUJBQWlCLEVBQUU7WUFDakIsUUFBUSxFQUFFLEtBQUs7WUFDZixLQUFLLEVBQUUsS0FBSztTQUNiO1FBRUQsV0FBVyxFQUFFO1lBQ1gsSUFBSSxFQUFFLENBQUM7WUFDUCxXQUFXLEVBQUUsRUFBRTtZQUNmLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFFBQVEsRUFBRSxLQUFLO1lBQ2YsS0FBSyxFQUFFLEtBQUs7WUFFWixLQUFLLEVBQUU7Z0JBQ0wsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsTUFBTSxFQUFFLE1BQU07Z0JBQ2QsVUFBVSxFQUFFLEtBQUs7Z0JBQ2pCLFlBQVksRUFBRSxFQUFFO2FBQ2pCO1lBRUQsSUFBSSxFQUFFO2dCQUNKLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxFQUFFO2dCQUNWLFlBQVksRUFBRSxFQUFFO2FBQ2pCO1NBQ0Y7S0FDRjtJQUVELE1BQU0sRUFBRTtRQUNOLFFBQVEsRUFBRTtZQUNSLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDL0IsVUFBVSxFQUFFLEVBQUU7U0FDZjtRQUVELE9BQU8sRUFBRTtZQUNQLFdBQVcsRUFBRSxFQUFFO1lBQ2YsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztZQUNyQyxRQUFRLEVBQUUsTUFBTTtZQUNoQixVQUFVLEVBQUUsUUFBUTtZQUNwQixRQUFRLEVBQUUsUUFBUTtZQUNsQixZQUFZLEVBQUUsVUFBVTtZQUN4QixVQUFVLEVBQUUsUUFBUSxDQUFDLFdBQVc7WUFDaEMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1lBQzFCLFFBQVEsRUFBRSxFQUFFO1lBQ1osVUFBVSxFQUFFLE1BQU07WUFDbEIsTUFBTSxFQUFFLEVBQUU7WUFDVixhQUFhLEVBQUUsQ0FBQyxHQUFHO1NBQ3BCO1FBRUQsU0FBUyxFQUFFO1lBQ1QsS0FBSyxFQUFFLE1BQU07WUFDYixTQUFTLEVBQUUsTUFBTTtZQUNqQixVQUFVLEVBQUUsUUFBUTtZQUNwQixVQUFVLEVBQUUsQ0FBQztZQUNiLFdBQVcsRUFBRSxDQUFDO1lBQ2QsWUFBWSxFQUFFLENBQUM7WUFDZixZQUFZLEVBQUUsQ0FBQztZQUVmLFVBQVUsRUFBRTtnQkFDVixNQUFNLEVBQUUsQ0FBQztnQkFDVCxLQUFLLEVBQUUsS0FBSztnQkFDWixRQUFRLEVBQUUsR0FBRztnQkFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxXQUFXO2dCQUNyQyxZQUFZLEVBQUUsQ0FBQztnQkFDZixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUM5QixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2FBQ3JDO1lBRUQsZUFBZSxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztnQkFDNUIsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsVUFBVSxFQUFFLE9BQU87Z0JBQ25CLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztnQkFDakMsZUFBZSxFQUFFLFFBQVEsQ0FBQyxPQUFPO2dCQUNqQyxrQkFBa0IsRUFBRSxZQUFZO2dCQUNoQyxjQUFjLEVBQUUsT0FBTzthQUN4QixDQUFDLEVBUjJCLENBUTNCO1lBRUYsU0FBUyxFQUFFO2dCQUNULEtBQUssRUFBRSxFQUFFO2dCQUNULE1BQU0sRUFBRSxFQUFFO2dCQUNWLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLEdBQUcsRUFBRSxLQUFLO2dCQUNWLElBQUksRUFBRSxLQUFLO2dCQUNYLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLFNBQVMsRUFBRSx3QkFBd0I7Z0JBQ25DLFNBQVMsRUFBRSxZQUFZO2dCQUN2QixVQUFVLEVBQUUsa0JBQWtCO2dCQUM5QixZQUFZLEVBQUUsd0JBQXdCO2dCQUN0QyxPQUFPLEVBQUUsRUFBRTthQUNaO1lBRUQsSUFBSSxFQUFFO2dCQUNKLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxHQUFHO2dCQUNYLE9BQU8sRUFBRSxFQUFFO2dCQUNYLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLFVBQVUsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDL0IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtnQkFDOUIsYUFBYSxFQUFFLFFBQVE7Z0JBQ3ZCLGNBQWMsRUFBRSxZQUFZO2dCQUM1QixVQUFVLEVBQUUsWUFBWTtnQkFFeEIsS0FBSyxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztvQkFDbEIsS0FBSyxFQUFFLE1BQU07b0JBQ2IsTUFBTSxFQUFFLE1BQU07b0JBQ2QsR0FBRyxFQUFFLENBQUM7b0JBQ04sSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLENBQUMsQ0FBQztvQkFDVixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUNwQyxlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87b0JBQ2pDLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztvQkFDakMsa0JBQWtCLEVBQUUsZUFBZTtvQkFDbkMsY0FBYyxFQUFFLE9BQU87b0JBQ3ZCLE1BQU0sRUFBRSxXQUFXO29CQUNuQixTQUFTLEVBQUUsdUJBQXVCO29CQUNsQywwQkFBMEIsRUFBRSxRQUFRO29CQUNwQyxtQkFBbUIsRUFBRSxJQUFJO29CQUN6QixpQkFBaUIsRUFBRSxDQUFDLG9CQUFvQixFQUFFLGVBQWUsQ0FBQztvQkFDMUQsb0JBQW9CLEVBQUUsUUFBUTtvQkFDOUIsV0FBVyxFQUFFLElBQUk7aUJBQ2xCLENBQUMsRUFsQmlCLENBa0JqQjtnQkFFRixLQUFLLEVBQUU7b0JBQ0wsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsVUFBVTtvQkFDdEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7b0JBQ3JDLFFBQVEsRUFBRSxFQUFFO29CQUNaLFVBQVUsRUFBRSxNQUFNO29CQUNsQixTQUFTLEVBQUUsTUFBTTtvQkFDakIsUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLFlBQVksRUFBRSxDQUFDO2lCQUNoQjtnQkFFRCxXQUFXLEVBQUU7b0JBQ1gsUUFBUSxFQUFFLEVBQUU7b0JBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsVUFBVSxFQUFFLFVBQVU7b0JBQ3RCLFVBQVUsRUFBRSxNQUFNO29CQUNsQixTQUFTLEVBQUUsRUFBRTtvQkFDYixRQUFRLEVBQUUsUUFBUTtpQkFDbkI7Z0JBRUQsT0FBTyxFQUFFO29CQUNQLEtBQUssRUFBRSxNQUFNO29CQUNiLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7b0JBQzlCLFFBQVEsRUFBRSxNQUFNO29CQUNoQixNQUFNLEVBQUUsTUFBTTtvQkFDZCxTQUFTLEVBQUUsTUFBTTtvQkFDakIsUUFBUSxFQUFFLFFBQVE7aUJBQ25CO2dCQUVELE9BQU8sRUFBRTtvQkFDUCxRQUFRLEVBQUUsRUFBRTtvQkFDWixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtvQkFDckMsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsVUFBVSxFQUFFLFVBQVU7aUJBQ3ZCO2FBQ0Y7U0FDRjtLQUNGO0lBRUQsT0FBTyxFQUFFO1FBQ1AsT0FBTyxFQUFFO1lBQ1AsUUFBUSxFQUFFLEVBQUU7WUFDWixVQUFVLEVBQUUsTUFBTTtZQUNsQixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxXQUFXO1NBQ2pDO1FBRUQsU0FBUyxFQUFFO1lBQ1QsS0FBSyxFQUFFLE1BQU07WUFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLFFBQVEsRUFBRSxNQUFNO1lBQ2hCLGNBQWMsRUFBRSxlQUFlO1lBQy9CLFVBQVUsRUFBRSxFQUFFO1lBQ2QsV0FBVyxFQUFFLENBQUM7WUFDZCxZQUFZLEVBQUUsQ0FBQztZQUNmLFlBQVksRUFBRSxFQUFFO1lBRWhCLFVBQVUsRUFBRTtnQkFDVixZQUFZLEVBQUUsRUFBRTtnQkFDaEIsS0FBSyxFQUFFLGtCQUFrQjtnQkFDekIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztnQkFDckMsWUFBWSxFQUFFLENBQUM7Z0JBQ2YsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDOUIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTthQUNyQztZQUVELGVBQWUsRUFBRTtnQkFDZixZQUFZLEVBQUUsRUFBRTtnQkFDaEIsS0FBSyxFQUFFLGtCQUFrQjtnQkFDekIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVztnQkFDckMsWUFBWSxFQUFFLENBQUM7Z0JBQ2YsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFNBQVMsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDOUIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTthQUNyQztZQUVELElBQUksRUFBRTtnQkFDSixLQUFLLEVBQUUsTUFBTTtnQkFDYixPQUFPLEVBQUUsRUFBRTtnQkFDWCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxVQUFVLEVBQUUsUUFBUSxDQUFDLFVBQVU7Z0JBQy9CLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLGFBQWEsRUFBRSxRQUFRO2dCQUN2QixjQUFjLEVBQUUsWUFBWTtnQkFDNUIsVUFBVSxFQUFFLFlBQVk7Z0JBRXhCLFdBQVcsRUFBRTtvQkFDWCxRQUFRLEVBQUUsRUFBRTtvQkFDWixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsU0FBUyxFQUFFLEVBQUU7b0JBQ2IsUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7b0JBQ3JDLE9BQU8sRUFBRSxFQUFFO29CQUNYLFdBQVcsRUFBRSxFQUFFO29CQUNmLFVBQVUsRUFBRSxVQUFVO2lCQUN2QjtnQkFFRCxLQUFLLEVBQUU7b0JBQ0wsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUMxQixVQUFVLEVBQUUsVUFBVTtvQkFDdEIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7b0JBQ3JDLFFBQVEsRUFBRSxFQUFFO29CQUNaLFVBQVUsRUFBRSxNQUFNO29CQUNsQixTQUFTLEVBQUUsTUFBTTtvQkFDakIsUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLFlBQVksRUFBRSxDQUFDO2lCQUNoQjtnQkFFRCxPQUFPLEVBQUU7b0JBQ1AsUUFBUSxFQUFFLEVBQUU7b0JBQ1osVUFBVSxFQUFFLE1BQU07b0JBQ2xCLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7b0JBQ3JDLE9BQU8sRUFBRSxFQUFFO29CQUNYLFdBQVcsRUFBRSxFQUFFO29CQUNmLFVBQVUsRUFBRSxVQUFVO2lCQUN2QjthQUNGO1NBQ0Y7S0FDRjtDQUNNLENBQUMifQ==
// CONCATENATED MODULE: ./components/magazine/category/view-desktop.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};




var renderView = function (_a) {
    var title = _a.title, url = _a.url, list = _a.list, titleStyle = _a.titleStyle, _b = _a.type, type = _b === void 0 ? 'normal' : _b, _c = _a.size, size = _c === void 0 ? 'normal' : _c;
    var renderItem = function (item, index) {
        var linkProps = {
            key: "img-" + index,
            to: routing["Ma" /* ROUTING_MAGAZINE_DETAIL_PATH */] + "/" + item.slug,
            style: 'small' === size ? style.desktop.container.smallItemSlider : style.desktop.container.itemSlider
        };
        return (react["createElement"](react_router_dom["NavLink"], __assign({}, linkProps),
            react["createElement"]("div", { style: style.mobile.container.itemSliderPanel(item.cover_image.large_url) }, 'video' === type && react["createElement"]("div", { style: style.mobile.container.videoIcon })),
            react["createElement"]("div", { style: style.desktop.container.info },
                react["createElement"]("div", { style: style.desktop.container.info.title }, item.title),
                react["createElement"]("div", { style: style.desktop.container.info.description }, item.description))));
    };
    return list && Array.isArray(list) && list.length > 0
        ?
            react["createElement"]("div", { className: 'user-select-all' },
                react["createElement"](react_router_dom["NavLink"], { to: url, style: Object.assign({}, style.desktop.heading, titleStyle) }, title),
                react["createElement"]("div", { style: style.desktop },
                    react["createElement"]("div", { style: style.desktop.container }, list.map(renderItem))))
        : null;
};
/* harmony default export */ var view_desktop = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1kZXNrdG9wLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1kZXNrdG9wLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUUzQyxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUV0RixPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFFNUIsSUFBTSxVQUFVLEdBQUcsVUFBQyxFQUFrRTtRQUFoRSxnQkFBSyxFQUFFLFlBQUcsRUFBRSxjQUFJLEVBQUUsMEJBQVUsRUFBRSxZQUFlLEVBQWYsb0NBQWUsRUFBRSxZQUFlLEVBQWYsb0NBQWU7SUFDbEYsSUFBTSxVQUFVLEdBQUcsVUFBQyxJQUFJLEVBQUUsS0FBSztRQUM3QixJQUFNLFNBQVMsR0FBRztZQUNoQixHQUFHLEVBQUUsU0FBTyxLQUFPO1lBQ25CLEVBQUUsRUFBSyw0QkFBNEIsU0FBSSxJQUFJLENBQUMsSUFBTTtZQUNsRCxLQUFLLEVBQUUsT0FBTyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxVQUFVO1NBQ3ZHLENBQUM7UUFFRixNQUFNLENBQUMsQ0FDTCxvQkFBQyxPQUFPLGVBQUssU0FBUztZQUNwQiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLElBQzNFLE9BQU8sS0FBSyxJQUFJLElBQUksNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBUSxDQUNyRTtZQUNOLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJO2dCQUN0Qyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBRyxJQUFJLENBQUMsS0FBSyxDQUFPO2dCQUNsRSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBRyxJQUFJLENBQUMsV0FBVyxDQUFPLENBQzFFLENBQ0UsQ0FDWCxDQUFDO0lBQ0osQ0FBQyxDQUFDO0lBRUYsTUFBTSxDQUFDLElBQUksSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQztRQUNuRCxDQUFDO1lBQ0QsNkJBQUssU0FBUyxFQUFFLGlCQUFpQjtnQkFDL0Isb0JBQUMsT0FBTyxJQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxJQUFHLEtBQUssQ0FBVztnQkFDaEcsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxPQUFPO29CQUN2Qiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLElBQ2hDLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQ2pCLENBQ0YsQ0FDRjtRQUNOLENBQUMsQ0FBQyxJQUFJLENBQUM7QUFDWCxDQUFDLENBQUE7QUFFRCxlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/category/view-mobile.tsx
var view_mobile_assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};




var view_mobile_renderView = function (_a) {
    var title = _a.title, url = _a.url, list = _a.list, _b = _a.type, type = _b === void 0 ? 'normal' : _b;
    var route = routing["Ma" /* ROUTING_MAGAZINE_DETAIL_PATH */];
    return (react["createElement"]("magazine-category", { style: style.mobile.mainWrap },
        react["createElement"](react_router_dom["NavLink"], { to: url, style: style.mobile.heading }, title),
        react["createElement"]("div", { style: style.mobile },
            react["createElement"]("div", { style: style.mobile.container }, Array.isArray(list)
                && list.map(function (item, index) {
                    var linkProps = {
                        key: "img-" + index,
                        to: route + "/" + item.slug,
                        style: style.mobile.container.itemSlider
                    };
                    var tags = 0 === item.tags.length ? [item.description] : item.tags;
                    tags = tags.map(function (item) {
                        if ('#' === item.charAt(0)) {
                            item = item.slice(1).toLowerCase();
                        }
                        return item;
                    }).filter(function (index) { return index < 10; });
                    return (react["createElement"](react_router_dom["NavLink"], view_mobile_assign({}, linkProps),
                        react["createElement"]("div", { style: style.mobile.container.itemSliderPanel(item.cover_image.original_url) }, 'video' === type && (react["createElement"]("div", { style: style.mobile.container.videoIcon }))),
                        react["createElement"]("div", { style: style.mobile.container.info },
                            react["createElement"]("div", { style: style.mobile.container.info.title }, item.title),
                            react["createElement"]("div", { style: style.mobile.container.info.description }, item.description))));
                })))));
};
/* harmony default export */ var view_mobile = (view_mobile_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1tb2JpbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LW1vYmlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUMvQixPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFFM0MsT0FBTyxFQUFFLDRCQUE0QixFQUErQixNQUFNLHdDQUF3QyxDQUFDO0FBRW5ILE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixJQUFNLFVBQVUsR0FBRyxVQUFDLEVBQXFDO1FBQW5DLGdCQUFLLEVBQUUsWUFBRyxFQUFFLGNBQUksRUFBRSxZQUFlLEVBQWYsb0NBQWU7SUFDckQsSUFBTSxLQUFLLEdBQUcsNEJBQTRCLENBQUM7SUFFM0MsTUFBTSxDQUFDLENBQ0wsMkNBQW1CLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVE7UUFDN0Msb0JBQUMsT0FBTyxJQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxJQUFHLEtBQUssQ0FBVztRQUNoRSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU07WUFDdEIsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxJQUU5QixLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQzttQkFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLO29CQUN0QixJQUFNLFNBQVMsR0FBRzt3QkFDaEIsR0FBRyxFQUFFLFNBQU8sS0FBTzt3QkFDbkIsRUFBRSxFQUFLLEtBQUssU0FBSSxJQUFJLENBQUMsSUFBTTt3QkFDM0IsS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFVBQVU7cUJBQ3pDLENBQUM7b0JBRUYsSUFBSSxJQUFJLEdBQUcsQ0FBQyxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztvQkFFbkUsSUFBSSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFJO3dCQUNuQixFQUFFLENBQUMsQ0FBQyxHQUFHLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUE7d0JBQUMsQ0FBQzt3QkFDbEUsTUFBTSxDQUFDLElBQUksQ0FBQztvQkFDZCxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBQyxLQUFLLElBQUssT0FBQSxLQUFLLEdBQUcsRUFBRSxFQUFWLENBQVUsQ0FBQyxDQUFDO29CQUVqQyxNQUFNLENBQUMsQ0FDTCxvQkFBQyxPQUFPLGVBQUssU0FBUzt3QkFDcEIsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxJQUU3RSxPQUFPLEtBQUssSUFBSSxJQUFJLENBQ2xCLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQVEsQ0FDckQsQ0FFQzt3QkFDTiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsSUFBSTs0QkFFckMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUcsSUFBSSxDQUFDLEtBQUssQ0FBTzs0QkFDakUsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLElBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBTyxDQUN6RSxDQUNFLENBQ1gsQ0FBQztnQkFDSixDQUFDLENBQUMsQ0FFQSxDQUNGLENBQ1ksQ0FDckIsQ0FBQTtBQUNILENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// EXTERNAL MODULE: ./components/magazine/video-item/index.tsx + 4 modules
var video_item = __webpack_require__(805);

// CONCATENATED MODULE: ./components/magazine/category/view-video.tsx





var mainBlockContent = function (_a) {
    var list = _a.list;
    var categoryVideoStyle = style.magazineCategory.categoryVideoContent;
    var mainVideo = Array.isArray(list) ? list[0] : null;
    var videoList = Array.isArray(list) ? list.slice(1, 5) : [];
    return (react["createElement"]("magazine-video-category", null, 0 !== list.length &&
        react["createElement"]("div", null,
            react["createElement"]("div", { style: categoryVideoStyle.mainTitle }, "Video"),
            react["createElement"]("div", { style: categoryVideoStyle.container },
                react["createElement"](react_router_dom["NavLink"], { to: routing["Ma" /* ROUTING_MAGAZINE_DETAIL_PATH */] + "/" + mainVideo.slug, style: categoryVideoStyle.largeVideoGroup.container },
                    react["createElement"]("div", { style: categoryVideoStyle.video(mainVideo && mainVideo.cover_image && mainVideo.cover_image.original_url || '') },
                        react["createElement"]("div", { style: categoryVideoStyle.videoIcon })),
                    react["createElement"]("div", { style: categoryVideoStyle.contentWrap },
                        react["createElement"]("div", { style: categoryVideoStyle.contentWrap.title }, mainVideo && mainVideo.title || ''),
                        react["createElement"]("div", { style: categoryVideoStyle.contentWrap.description }, mainVideo && '#' + mainVideo.description || ''))),
                react["createElement"]("div", { style: categoryVideoStyle.smallVideoGroup.container }, Array.isArray(videoList) && videoList.map(function (item, index) { return react["createElement"](video_item["a" /* default */], { item: item, key: "video-item-" + item.id, lastChild: index > 1 }); }))))));
};
var view_video_renderView = function (_a) {
    var title = _a.title, list = _a.list;
    return list && 0 !== list.length ? mainBlockContent({ list: list }) : null;
};
/* harmony default export */ var view_video = (view_video_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy12aWRlby5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXctdmlkZW8udHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUUzQyxPQUFPLFNBQVMsTUFBTSxlQUFlLENBQUM7QUFDdEMsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFFdEYsT0FBTyxLQUFLLE1BQU0sU0FBUyxDQUFDO0FBRTVCLElBQU0sZ0JBQWdCLEdBQUcsVUFBQyxFQUFRO1FBQU4sY0FBSTtJQUM5QixJQUFNLGtCQUFrQixHQUFHLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQztJQUN2RSxJQUFNLFNBQVMsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUN2RCxJQUFNLFNBQVMsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBRTlELE1BQU0sQ0FBQyxDQUNMLHFEQUVJLENBQUMsS0FBSyxJQUFJLENBQUMsTUFBTTtRQUNqQjtZQUNFLDZCQUFLLEtBQUssRUFBRSxrQkFBa0IsQ0FBQyxTQUFTLFlBQWE7WUFDckQsNkJBQUssS0FBSyxFQUFFLGtCQUFrQixDQUFDLFNBQVM7Z0JBQ3RDLG9CQUFDLE9BQU8sSUFBQyxFQUFFLEVBQUssNEJBQTRCLFNBQUksU0FBUyxDQUFDLElBQU0sRUFBRSxLQUFLLEVBQUUsa0JBQWtCLENBQUMsZUFBZSxDQUFDLFNBQVM7b0JBQ25ILDZCQUFLLEtBQUssRUFBRSxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsU0FBUyxJQUFJLFNBQVMsQ0FBQyxXQUFXLElBQUksU0FBUyxDQUFDLFdBQVcsQ0FBQyxZQUFZLElBQUksRUFBRSxDQUFDO3dCQUNsSCw2QkFBSyxLQUFLLEVBQUUsa0JBQWtCLENBQUMsU0FBUyxHQUFRLENBQzVDO29CQUNOLDZCQUFLLEtBQUssRUFBRSxrQkFBa0IsQ0FBQyxXQUFXO3dCQUN4Qyw2QkFBSyxLQUFLLEVBQUUsa0JBQWtCLENBQUMsV0FBVyxDQUFDLEtBQUssSUFBRyxTQUFTLElBQUksU0FBUyxDQUFDLEtBQUssSUFBSSxFQUFFLENBQU87d0JBQzVGLDZCQUFLLEtBQUssRUFBRSxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsV0FBVyxJQUFHLFNBQVMsSUFBSSxHQUFHLEdBQUcsU0FBUyxDQUFDLFdBQVcsSUFBSSxFQUFFLENBQU8sQ0FDMUcsQ0FDRTtnQkFDViw2QkFBSyxLQUFLLEVBQUUsa0JBQWtCLENBQUMsZUFBZSxDQUFDLFNBQVMsSUFDckQsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBSSxFQUFFLEtBQUssSUFBSyxPQUFBLG9CQUFDLFNBQVMsSUFBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxnQkFBYyxJQUFJLENBQUMsRUFBSSxFQUFFLFNBQVMsRUFBRSxLQUFLLEdBQUcsQ0FBQyxHQUFJLEVBQTdFLENBQTZFLENBQUMsQ0FDdEksQ0FDRixDQUNGLENBRWdCLENBQzNCLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixJQUFNLFVBQVUsR0FBRyxVQUFDLEVBQWU7UUFBYixnQkFBSyxFQUFFLGNBQUk7SUFDL0IsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztBQUN2RSxDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/category/view.tsx




var view_renderView = function (props) {
    var _a = props, type = _a.type, list = _a.list, title = _a.title, url = _a.url, titleStyle = _a.titleStyle;
    var switchView = {};
    switch (type) {
        case 1:
            switchView = {
                MOBILE: function () { return view_mobile({ title: title, url: url, list: list }); },
                DESKTOP: function () { return view_desktop({ title: title, url: url, list: list, titleStyle: titleStyle }); }
            };
            return switchView[window.DEVICE_VERSION]();
        case 2:
            switchView = {
                MOBILE: function () { return view_mobile({ title: title, url: url, list: list }); },
                DESKTOP: function () { return view_desktop({ title: title, url: url, list: list, titleStyle: titleStyle }); }
            };
            return switchView[window.DEVICE_VERSION]();
        case 3:
            switchView = {
                MOBILE: function () { return view_mobile({ title: title, url: url, list: list }); },
                DESKTOP: function () { return view_desktop({ title: title, url: url, list: list, titleStyle: titleStyle }); }
            };
            return switchView[window.DEVICE_VERSION]();
        case 4:
            switchView = {
                MOBILE: function () { return view_mobile({ title: title, url: url, list: list }); },
                DESKTOP: function () { return view_desktop({ title: title, url: url, list: list, titleStyle: titleStyle }); }
            };
            return switchView[window.DEVICE_VERSION]();
        case 5:
            switchView = {
                MOBILE: function () { return view_mobile({ title: title, url: url, list: list }); },
                DESKTOP: function () { return view_desktop({ title: title, url: url, list: list, titleStyle: titleStyle }); }
            };
            return switchView[window.DEVICE_VERSION]();
        case magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].VIDEO.type:
            switchView = {
                MOBILE: function () { return view_mobile({ title: magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].VIDEO.title, url: '', list: list, type: 'video' }); },
                DESKTOP: function () { return view_video({ title: magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].VIDEO.title, list: list }); }
            };
            return switchView[window.DEVICE_VERSION]();
        case magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].TRENDING.type:
            return view_desktop({ title: magazine_category["a" /* MAGAZINE_CATEGORY_TYPE */].TRENDING.title, url: '', list: list, size: 'small', titleStyle: titleStyle });
        default:
            switchView = {
                MOBILE: function () { return view_mobile({ title: title, url: url, list: list }); },
                DESKTOP: function () { return view_desktop({ title: title, url: url, list: list, titleStyle: titleStyle }); }
            };
            return switchView[window.DEVICE_VERSION]();
    }
};
/* harmony default export */ var view = (view_renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBRzFGLE9BQU8sYUFBYSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sWUFBWSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLG1CQUFtQixNQUFNLGNBQWMsQ0FBQztBQUUvQyxJQUFNLFVBQVUsR0FBRyxVQUFDLEtBQWE7SUFDekIsSUFBQSxVQUF3RCxFQUF0RCxjQUFJLEVBQUUsY0FBSSxFQUFFLGdCQUFLLEVBQUUsWUFBRyxFQUFFLDBCQUFVLENBQXFCO0lBQy9ELElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQztJQUVwQixNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ2IsS0FBSyxDQUFDO1lBQ0osVUFBVSxHQUFHO2dCQUNYLE1BQU0sRUFBRSxjQUFNLE9BQUEsWUFBWSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsR0FBRyxLQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxFQUFsQyxDQUFrQztnQkFDaEQsT0FBTyxFQUFFLGNBQU0sT0FBQSxhQUFhLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxHQUFHLEtBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxVQUFVLFlBQUEsRUFBRSxDQUFDLEVBQS9DLENBQStDO2FBQy9ELENBQUM7WUFDRixNQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDO1FBRTdDLEtBQUssQ0FBQztZQUNKLFVBQVUsR0FBRztnQkFDWCxNQUFNLEVBQUUsY0FBTSxPQUFBLFlBQVksQ0FBQyxFQUFFLEtBQUssT0FBQSxFQUFFLEdBQUcsS0FBQSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsRUFBbEMsQ0FBa0M7Z0JBQ2hELE9BQU8sRUFBRSxjQUFNLE9BQUEsYUFBYSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsR0FBRyxLQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsVUFBVSxZQUFBLEVBQUUsQ0FBQyxFQUEvQyxDQUErQzthQUMvRCxDQUFDO1lBQ0YsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQztRQUU3QyxLQUFLLENBQUM7WUFDSixVQUFVLEdBQUc7Z0JBQ1gsTUFBTSxFQUFFLGNBQU0sT0FBQSxZQUFZLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxHQUFHLEtBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDLEVBQWxDLENBQWtDO2dCQUNoRCxPQUFPLEVBQUUsY0FBTSxPQUFBLGFBQWEsQ0FBQyxFQUFFLEtBQUssT0FBQSxFQUFFLEdBQUcsS0FBQSxFQUFFLElBQUksTUFBQSxFQUFFLFVBQVUsWUFBQSxFQUFFLENBQUMsRUFBL0MsQ0FBK0M7YUFDL0QsQ0FBQztZQUNGLE1BQU0sQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUM7UUFFN0MsS0FBSyxDQUFDO1lBQ0osVUFBVSxHQUFHO2dCQUNYLE1BQU0sRUFBRSxjQUFNLE9BQUEsWUFBWSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsR0FBRyxLQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxFQUFsQyxDQUFrQztnQkFDaEQsT0FBTyxFQUFFLGNBQU0sT0FBQSxhQUFhLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxHQUFHLEtBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxVQUFVLFlBQUEsRUFBRSxDQUFDLEVBQS9DLENBQStDO2FBQy9ELENBQUM7WUFDRixNQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDO1FBRTdDLEtBQUssQ0FBQztZQUNKLFVBQVUsR0FBRztnQkFDWCxNQUFNLEVBQUUsY0FBTSxPQUFBLFlBQVksQ0FBQyxFQUFFLEtBQUssT0FBQSxFQUFFLEdBQUcsS0FBQSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsRUFBbEMsQ0FBa0M7Z0JBQ2hELE9BQU8sRUFBRSxjQUFNLE9BQUEsYUFBYSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsR0FBRyxLQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsVUFBVSxZQUFBLEVBQUUsQ0FBQyxFQUEvQyxDQUErQzthQUMvRCxDQUFDO1lBQ0YsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQztRQUU3QyxLQUFLLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxJQUFJO1lBQ3BDLFVBQVUsR0FBRztnQkFDWCxNQUFNLEVBQUUsY0FBTSxPQUFBLFlBQVksQ0FBQyxFQUFFLEtBQUssRUFBRSxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQXpGLENBQXlGO2dCQUN2RyxPQUFPLEVBQUUsY0FBTSxPQUFBLG1CQUFtQixDQUFDLEVBQUUsS0FBSyxFQUFFLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxFQUF4RSxDQUF3RTthQUN4RixDQUFDO1lBQ0YsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQztRQUU3QyxLQUFLLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxJQUFJO1lBQ3ZDLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLElBQUksTUFBQSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsVUFBVSxZQUFBLEVBQUUsQ0FBQyxDQUFDO1FBRW5IO1lBQ0UsVUFBVSxHQUFHO2dCQUNYLE1BQU0sRUFBRSxjQUFNLE9BQUEsWUFBWSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsR0FBRyxLQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxFQUFsQyxDQUFrQztnQkFDaEQsT0FBTyxFQUFFLGNBQU0sT0FBQSxhQUFhLENBQUMsRUFBRSxLQUFLLE9BQUEsRUFBRSxHQUFHLEtBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxVQUFVLFlBQUEsRUFBRSxDQUFDLEVBQS9DLENBQStDO2FBQy9ELENBQUM7WUFFRixNQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDO0lBQy9DLENBQUM7QUFDSCxDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/category/initialize.tsx
var DEFAULT_PROPS = {
    type: 0,
    list: [],
    title: '',
    url: '',
    titleStyle: {}
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsQ0FBQztJQUNQLElBQUksRUFBRSxFQUFFO0lBQ1IsS0FBSyxFQUFFLEVBQUU7SUFDVCxHQUFHLEVBQUUsRUFBRTtJQUNQLFVBQVUsRUFBRSxFQUFFO0NBQ0wsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/category/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_MagazineCategory = /** @class */ (function (_super) {
    __extends(MagazineCategory, _super);
    function MagazineCategory(props) {
        return _super.call(this, props) || this;
    }
    MagazineCategory.prototype.render = function () {
        return view(this.props);
    };
    ;
    MagazineCategory.defaultProps = DEFAULT_PROPS;
    MagazineCategory = __decorate([
        radium
    ], MagazineCategory);
    return MagazineCategory;
}(react["PureComponent"]));
/* harmony default export */ var component = (component_MagazineCategory);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFDaEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUk3QztJQUErQixvQ0FBNkI7SUFFMUQsMEJBQVksS0FBYTtlQUN2QixrQkFBTSxLQUFLLENBQUM7SUFDZCxDQUFDO0lBRUQsaUNBQU0sR0FBTjtRQUNFLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFBQSxDQUFDO0lBUEssNkJBQVksR0FBVyxhQUFhLENBQUM7SUFEeEMsZ0JBQWdCO1FBRHJCLE1BQU07T0FDRCxnQkFBZ0IsQ0FTckI7SUFBRCx1QkFBQztDQUFBLEFBVEQsQ0FBK0IsYUFBYSxHQVMzQztBQUVELGVBQWUsZ0JBQWdCLENBQUMifQ==
// CONCATENATED MODULE: ./components/magazine/category/index.tsx

/* harmony default export */ var category = __webpack_exports__["a"] = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxnQkFBZ0IsTUFBTSxhQUFhLENBQUM7QUFDM0MsZUFBZSxnQkFBZ0IsQ0FBQyJ9

/***/ }),

/***/ 805:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./components/magazine/video-item/style.tsx


var generateWidthByColumn = function (column) {
    switch (column) {
        case 2: return 'calc(50% - 10px)';
        case 3: return 'calc(33.3% - 10px)';
        case 4: return 'calc(25% - 15px)';
        case 5: return 'calc(20% - 10px)';
    }
};
/* harmony default export */ var video_item_style = ({
    video: function (imgUrl) { return ({
        width: '100%',
        paddingTop: '62.5%',
        position: variable["position"].relative,
        backgroundImage: "url(" + imgUrl + ")",
        backgroundColor: variable["colorF7"],
        backgroundPosition: 'top center',
        backgroundSize: 'cover',
    }); },
    videoIcon: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                width: 40,
                left: '60%',
                transform: 'translate(-50%, -50%)',
                borderTop: "15px solid " + variable["colorTransparent"],
                borderLeft: "20px solid " + variable["colorWhite"],
                borderBottom: "15px solid " + variable["colorTransparent"],
                borderRight: "15px solid " + variable["colorTransparent"],
            }],
        DESKTOP: [{
                width: 70,
                height: 70,
                left: '55%',
                transform: 'translate(-50%, -50%)',
                borderTop: "35px solid " + variable["colorTransparent"],
                borderLeft: "51px solid " + variable["colorWhite"],
                borderBottom: "35px solid " + variable["colorTransparent"],
            }],
        GENERAL: [{
                position: variable["position"].absolute,
                boxSizing: 'border-box',
                opacity: 0.8,
                top: '50%'
            }]
    }),
    contentWrap: {
        width: '100%',
        paddingTop: 12,
        paddingRight: 12,
        paddingBottom: 12,
        paddingLeft: 12,
        position: variable["position"].relative,
        backgroundColor: variable["colorWhite"],
        display: variable["display"].flex,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        title: {
            color: variable["colorBlack"],
            whiteSpace: 'pre-wrap',
            fontFamily: variable["fontAvenirMedium"],
            fontSize: 16,
            lineHeight: '24px',
            maxHeight: 48,
            minHeight: 48,
            overflow: 'hidden',
            marginBottom: 5,
        },
        description: {
            fontSize: 12,
            lineHeight: '18px',
            color: variable["colorBlack"],
            fontFamily: variable["fontAvenirMedium"],
            opacity: 0.7,
            marginRight: 10,
            whiteSpace: 'pre-wrap',
        }
    },
    videoContainer: function (column) { return Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                marginBottom: 10
            }],
        DESKTOP: [{
                marginBottom: 20
            }],
        GENERAL: [{
                borderRadius: 5,
                overflow: 'hidden',
                boxShadow: variable["shadowBlur"],
                display: variable["display"].block,
                position: variable["position"].relative,
                width: generateWidthByColumn(column)
            }]
    }); },
    lastChild: {
        marginBottom: 0
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFekQsSUFBTSxxQkFBcUIsR0FBRyxVQUFDLE1BQU07SUFDbkMsTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUNmLEtBQUssQ0FBQyxFQUFFLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQztRQUNsQyxLQUFLLENBQUMsRUFBRSxNQUFNLENBQUMsb0JBQW9CLENBQUM7UUFDcEMsS0FBSyxDQUFDLEVBQUUsTUFBTSxDQUFDLGtCQUFrQixDQUFDO1FBQ2xDLEtBQUssQ0FBQyxFQUFFLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQztJQUNwQyxDQUFDO0FBQ0gsQ0FBQyxDQUFDO0FBRUYsZUFBZTtJQUViLEtBQUssRUFBRSxVQUFDLE1BQU0sSUFBSyxPQUFBLENBQUM7UUFDbEIsS0FBSyxFQUFFLE1BQU07UUFDYixVQUFVLEVBQUUsT0FBTztRQUNuQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1FBQ3BDLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztRQUNqQyxlQUFlLEVBQUUsUUFBUSxDQUFDLE9BQU87UUFDakMsa0JBQWtCLEVBQUUsWUFBWTtRQUNoQyxjQUFjLEVBQUUsT0FBTztLQUN4QixDQUFDLEVBUmlCLENBUWpCO0lBRUYsU0FBUyxFQUFFLFlBQVksQ0FBQztRQUN0QixNQUFNLEVBQUUsQ0FBQztnQkFDUCxLQUFLLEVBQUUsRUFBRTtnQkFDVCxJQUFJLEVBQUUsS0FBSztnQkFDWCxTQUFTLEVBQUUsdUJBQXVCO2dCQUNsQyxTQUFTLEVBQUUsZ0JBQWMsUUFBUSxDQUFDLGdCQUFrQjtnQkFDcEQsVUFBVSxFQUFFLGdCQUFjLFFBQVEsQ0FBQyxVQUFZO2dCQUMvQyxZQUFZLEVBQUUsZ0JBQWMsUUFBUSxDQUFDLGdCQUFrQjtnQkFDdkQsV0FBVyxFQUFFLGdCQUFjLFFBQVEsQ0FBQyxnQkFBa0I7YUFDdkQsQ0FBQztRQUVGLE9BQU8sRUFBRSxDQUFDO2dCQUNSLEtBQUssRUFBRSxFQUFFO2dCQUNULE1BQU0sRUFBRSxFQUFFO2dCQUNWLElBQUksRUFBRSxLQUFLO2dCQUNYLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLFNBQVMsRUFBRSxnQkFBYyxRQUFRLENBQUMsZ0JBQWtCO2dCQUNwRCxVQUFVLEVBQUUsZ0JBQWMsUUFBUSxDQUFDLFVBQVk7Z0JBQy9DLFlBQVksRUFBRSxnQkFBYyxRQUFRLENBQUMsZ0JBQWtCO2FBQ3hELENBQUM7UUFFRixPQUFPLEVBQUUsQ0FBQztnQkFDUixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxTQUFTLEVBQUUsWUFBWTtnQkFDdkIsT0FBTyxFQUFFLEdBQUc7Z0JBQ1osR0FBRyxFQUFFLEtBQUs7YUFDWCxDQUFDO0tBQ0gsQ0FBQztJQUVGLFdBQVcsRUFBRTtRQUNYLEtBQUssRUFBRSxNQUFNO1FBQ2IsVUFBVSxFQUFFLEVBQUU7UUFDZCxZQUFZLEVBQUUsRUFBRTtRQUNoQixhQUFhLEVBQUUsRUFBRTtRQUNqQixXQUFXLEVBQUUsRUFBRTtRQUNmLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7UUFDcEMsZUFBZSxFQUFFLFFBQVEsQ0FBQyxVQUFVO1FBQ3BDLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7UUFDOUIsYUFBYSxFQUFFLFFBQVE7UUFDdkIsY0FBYyxFQUFFLFlBQVk7UUFDNUIsVUFBVSxFQUFFLFlBQVk7UUFFeEIsS0FBSyxFQUFFO1lBQ0wsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO1lBQzFCLFVBQVUsRUFBRSxVQUFVO1lBQ3RCLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO1lBQ3JDLFFBQVEsRUFBRSxFQUFFO1lBQ1osVUFBVSxFQUFFLE1BQU07WUFDbEIsU0FBUyxFQUFFLEVBQUU7WUFDYixTQUFTLEVBQUUsRUFBRTtZQUNiLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLFlBQVksRUFBRSxDQUFDO1NBQ2hCO1FBRUQsV0FBVyxFQUFFO1lBQ1gsUUFBUSxFQUFFLEVBQUU7WUFDWixVQUFVLEVBQUUsTUFBTTtZQUNsQixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7WUFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7WUFDckMsT0FBTyxFQUFFLEdBQUc7WUFDWixXQUFXLEVBQUUsRUFBRTtZQUNmLFVBQVUsRUFBRSxVQUFVO1NBQ3ZCO0tBQ0Y7SUFFRCxjQUFjLEVBQUUsVUFBQyxNQUFNLElBQUssT0FBQSxZQUFZLENBQUM7UUFDdkMsTUFBTSxFQUFFLENBQUM7Z0JBQ1AsWUFBWSxFQUFFLEVBQUU7YUFDakIsQ0FBQztRQUVGLE9BQU8sRUFBRSxDQUFDO2dCQUNSLFlBQVksRUFBRSxFQUFFO2FBQ2pCLENBQUM7UUFFRixPQUFPLEVBQUUsQ0FBQztnQkFDUixZQUFZLEVBQUUsQ0FBQztnQkFDZixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2dCQUM5QixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO2dCQUMvQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUNwQyxLQUFLLEVBQUUscUJBQXFCLENBQUMsTUFBTSxDQUFDO2FBQ3JDLENBQUM7S0FDSCxDQUFDLEVBakIwQixDQWlCMUI7SUFFRixTQUFTLEVBQUU7UUFDVCxZQUFZLEVBQUUsQ0FBQztLQUNoQjtDQUNNLENBQUMifQ==
// CONCATENATED MODULE: ./components/magazine/video-item/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};




var renderView = function (props) {
    var _a = props, item = _a.item, column = _a.column, lastChild = _a.lastChild, style = _a.style;
    var linkProps = {
        key: "small-video-" + item.id,
        to: routing["Ma" /* ROUTING_MAGAZINE_DETAIL_PATH */] + "/" + item.slug,
        style: Object.assign({}, video_item_style.videoContainer(column), style, lastChild && video_item_style.lastChild)
    };
    return (react["createElement"](react_router_dom["NavLink"], __assign({}, linkProps),
        react["createElement"]("div", { style: video_item_style.video(item.cover_image.original_url) },
            react["createElement"]("div", { style: video_item_style.videoIcon })),
        react["createElement"]("div", { style: video_item_style.contentWrap },
            react["createElement"]("div", { style: video_item_style.contentWrap.title }, item.title || ''))));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBR3RGLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixJQUFNLFVBQVUsR0FBRyxVQUFDLEtBQUs7SUFDakIsSUFBQSxVQUFvRCxFQUFsRCxjQUFJLEVBQUUsa0JBQU0sRUFBRSx3QkFBUyxFQUFFLGdCQUFLLENBQXFCO0lBRTNELElBQU0sU0FBUyxHQUFHO1FBQ2hCLEdBQUcsRUFBRSxpQkFBZSxJQUFJLENBQUMsRUFBSTtRQUM3QixFQUFFLEVBQUssNEJBQTRCLFNBQUksSUFBSSxDQUFDLElBQU07UUFDbEQsS0FBSyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEVBQUUsS0FBSyxFQUFFLFNBQVMsSUFBSSxLQUFLLENBQUMsU0FBUyxDQUFDO0tBQzVGLENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCxvQkFBQyxPQUFPLGVBQUssU0FBUztRQUNwQiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQztZQUNwRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFNBQVMsR0FBUSxDQUMvQjtRQUNOLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVztZQUMzQiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxLQUFLLElBQUcsSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQU8sQ0FDekQsQ0FDRSxDQUNYLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./components/magazine/video-item/initialize.tsx
var DEFAULT_PROPS = {
    item: {},
    style: {},
    column: 2,
    lastChild: false
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLEtBQUssRUFBRSxFQUFFO0lBQ1QsTUFBTSxFQUFFLENBQUM7SUFDVCxTQUFTLEVBQUUsS0FBSztDQUNqQixDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/video-item/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_VideoItem = /** @class */ (function (_super) {
    __extends(VideoItem, _super);
    function VideoItem(props) {
        return _super.call(this, props) || this;
    }
    VideoItem.prototype.render = function () {
        return view(this.props);
    };
    ;
    VideoItem.defaultProps = DEFAULT_PROPS;
    VideoItem = __decorate([
        radium
    ], VideoItem);
    return VideoItem;
}(react["PureComponent"]));
/* harmony default export */ var component = (component_VideoItem);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFFaEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUc3QztJQUF3Qiw2QkFBNkI7SUFFbkQsbUJBQVksS0FBYTtlQUN2QixrQkFBTSxLQUFLLENBQUM7SUFDZCxDQUFDO0lBRUQsMEJBQU0sR0FBTjtRQUNFLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFBQSxDQUFDO0lBUEssc0JBQVksR0FBRyxhQUFhLENBQUM7SUFEaEMsU0FBUztRQURkLE1BQU07T0FDRCxTQUFTLENBU2Q7SUFBRCxnQkFBQztDQUFBLEFBVEQsQ0FBd0IsYUFBYSxHQVNwQztBQUVELGVBQWUsU0FBUyxDQUFDIn0=
// CONCATENATED MODULE: ./components/magazine/video-item/index.tsx

/* harmony default export */ var video_item = __webpack_exports__["a"] = (component);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxTQUFTLE1BQU0sYUFBYSxDQUFDO0FBQ3BDLGVBQWUsU0FBUyxDQUFDIn0=

/***/ })

}]);