(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[99],{

/***/ 756:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./style/layout.ts
var layout = __webpack_require__(93);

// CONCATENATED MODULE: ./container/layout/wrap/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};


;
var renderView = function (_a) {
    var children = _a.children, style = _a.style, type = _a.type;
    var containerProps = {
        key: 'wrap-layout-container',
        style: [layout["c" /* wrapLayout */], style, 'larger' === type && layout["c" /* wrapLayout */].larger]
    };
    return (react["createElement"]("div", __assign({}, containerProps), children));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFFL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSx1QkFBdUIsQ0FBQztBQU0vQyxDQUFDO0FBRUYsSUFBTSxVQUFVLEdBQUcsVUFBQyxFQUFzQztRQUFwQyxzQkFBUSxFQUFFLGdCQUFLLEVBQUUsY0FBSTtJQUN6QyxJQUFNLGNBQWMsR0FBRztRQUNyQixHQUFHLEVBQUUsdUJBQXVCO1FBQzVCLEtBQUssRUFBRSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsS0FBSyxFQUFFLFFBQVEsS0FBSyxJQUFJLElBQUksTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUM7S0FDakYsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMLHdDQUFTLGNBQWMsR0FDcEIsUUFBUSxDQUNMLENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQTtBQUVELGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./container/layout/wrap/container.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var container_WrapLayout = /** @class */ (function (_super) {
    __extends(WrapLayout, _super);
    function WrapLayout(props) {
        return _super.call(this, props) || this;
    }
    WrapLayout.prototype.render = function () {
        var _a = this.props, children = _a.children, style = _a.style, type = _a.type;
        return view({ children: children, style: style, type: type });
    };
    ;
    WrapLayout.defaultProps = { type: 'normal' };
    WrapLayout = __decorate([
        radium
    ], WrapLayout);
    return WrapLayout;
}(react["PureComponent"]));
/* harmony default export */ var container = __webpack_exports__["default"] = (container_WrapLayout);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFJaEM7SUFBeUIsOEJBQTZCO0lBR3BELG9CQUFZLEtBQWE7ZUFDdkIsa0JBQU0sS0FBSyxDQUFDO0lBQ2QsQ0FBQztJQUVELDJCQUFNLEdBQU47UUFDUSxJQUFBLGVBQXNDLEVBQXBDLHNCQUFRLEVBQUUsZ0JBQUssRUFBRSxjQUFJLENBQWdCO1FBQzdDLE1BQU0sQ0FBQyxVQUFVLENBQUMsRUFBRSxRQUFRLFVBQUEsRUFBRSxLQUFLLE9BQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUFBLENBQUM7SUFUSyx1QkFBWSxHQUFXLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxDQUFDO0lBRDdDLFVBQVU7UUFEZixNQUFNO09BQ0QsVUFBVSxDQVdmO0lBQUQsaUJBQUM7Q0FBQSxBQVhELENBQXlCLGFBQWEsR0FXckM7QUFFRCxlQUFlLFVBQVUsQ0FBQyJ9

/***/ })

}]);