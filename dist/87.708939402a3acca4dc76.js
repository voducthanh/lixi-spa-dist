(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[87],{

/***/ 745:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 100).then(__webpack_require__.bind(null, 755)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 746:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 99).then(__webpack_require__.bind(null, 759)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 754:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return updateMetaInfoAction; });
/* harmony import */ var _constants_api_meta__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(352);

var updateMetaInfoAction = function (data) { return ({
    type: _constants_api_meta__WEBPACK_IMPORTED_MODULE_0__[/* UPDATE_META_INFO */ "a"],
    payload: data
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWV0YS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1ldGEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLGdCQUFnQixFQUNqQixNQUFNLHVCQUF1QixDQUFDO0FBRS9CLE1BQU0sQ0FBQyxJQUFNLG9CQUFvQixHQUFHLFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FBQztJQUM3QyxJQUFJLEVBQUUsZ0JBQWdCO0lBQ3RCLE9BQU8sRUFBRSxJQUFJO0NBQ2QsQ0FBQyxFQUg0QyxDQUc1QyxDQUFDIn0=

/***/ }),

/***/ 893:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/encode.ts
var encode = __webpack_require__(7);

// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// EXTERNAL MODULE: ./action/meta.ts
var meta = __webpack_require__(754);

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// CONCATENATED MODULE: ./api/discount-code.ts

var fetchDiscountCodesByCode = function (_a) {
    var code = _a.code;
    return Object(restful_method["b" /* get */])({
        path: "/discount_codes/" + code,
        description: 'Get product list discount code by code',
        errorMesssage: "Can't get product list discount code by code. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlzY291bnQtY29kZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRpc2NvdW50LWNvZGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBRS9DLE1BQU0sQ0FBQyxJQUFNLHdCQUF3QixHQUNuQyxVQUFDLEVBQVE7UUFBTixjQUFJO0lBRUwsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSxxQkFBbUIsSUFBTTtRQUMvQixXQUFXLEVBQUUsd0NBQXdDO1FBQ3JELGFBQWEsRUFBRSxnRUFBZ0U7S0FDaEYsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDIn0=
// EXTERNAL MODULE: ./constants/api/discount-code.ts
var discount_code = __webpack_require__(209);

// CONCATENATED MODULE: ./action/discount-code.ts


/**
* Fetch discount code by code
*
* @param {string} code ex: EA9F8521
*/
var fetchDiscountCodesByCodeAction = function (_a) {
    var code = _a.code;
    return function (dispatch, getState) {
        return dispatch({
            type: discount_code["a" /* FETCH_DISCOUNT_CODE */],
            payload: { promise: fetchDiscountCodesByCode({ code: code }).then(function (res) { return res; }) },
            meta: { code: code }
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlzY291bnQtY29kZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRpc2NvdW50LWNvZGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFFaEUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFFckU7Ozs7RUFJRTtBQUNGLE1BQU0sQ0FBQyxJQUFNLDhCQUE4QixHQUN6QyxVQUFDLEVBQVE7UUFBTixjQUFJO0lBQ0wsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLG1CQUFtQjtZQUN6QixPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsd0JBQXdCLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsQ0FBQyxFQUFFO1lBQ3pFLElBQUksRUFBRSxFQUFFLElBQUksTUFBQSxFQUFFO1NBQ2YsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/discount-code/store.tsx


var mapStateToProps = function (state) { return ({
    discountCodeStore: state.discountCode,
}); };
var mapDispatchToProps = function (dispatch) { return ({
    fetchDiscountCodesByCodeAction: function (data) { return dispatch(fetchDiscountCodesByCodeAction(data)); },
    updateMetaInfoAction: function (data) { return dispatch(Object(meta["a" /* updateMetaInfoAction */])(data)); }
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDNUQsT0FBTyxFQUFFLDhCQUE4QixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFJL0UsTUFBTSxDQUFDLElBQU0sZUFBZSxHQUFHLFVBQUMsS0FBSyxJQUFLLE9BQUEsQ0FBQztJQUN6QyxpQkFBaUIsRUFBRSxLQUFLLENBQUMsWUFBWTtDQUMzQixDQUFBLEVBRjhCLENBRTlCLENBQUM7QUFFYixNQUFNLENBQUMsSUFBTSxrQkFBa0IsR0FBRyxVQUFDLFFBQVEsSUFBSyxPQUFBLENBQUM7SUFDL0MsOEJBQThCLEVBQUUsVUFBQyxJQUFTLElBQUssT0FBQSxRQUFRLENBQUMsOEJBQThCLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBOUMsQ0FBOEM7SUFDN0Ysb0JBQW9CLEVBQUUsVUFBQyxJQUFJLElBQUssT0FBQSxRQUFRLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBcEMsQ0FBb0M7Q0FDMUQsQ0FBQSxFQUhvQyxDQUdwQyxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/discount-code/initialize.tsx
var DEFAULT_PROPS = {
    type: 'full',
    column: 4,
};
var INITIAL_STATE = {
    isLoading: false,
    isSubCategoryOnTop: false,
    heightSubCategoryToTop: 0
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsTUFBTTtJQUNaLE1BQU0sRUFBRSxDQUFDO0NBQ0EsQ0FBQztBQUVaLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixTQUFTLEVBQUUsS0FBSztJQUNoQixrQkFBa0IsRUFBRSxLQUFLO0lBQ3pCLHNCQUFzQixFQUFFLENBQUM7Q0FDaEIsQ0FBQyJ9
// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./container/layout/main-block/index.tsx
var main_block = __webpack_require__(746);

// EXTERNAL MODULE: ./container/layout/wrap/index.tsx
var wrap = __webpack_require__(745);

// EXTERNAL MODULE: ./components/product/detail-item/index.tsx + 5 modules
var detail_item = __webpack_require__(780);

// EXTERNAL MODULE: ./components/ui/loading-placeholder/index.tsx + 4 modules
var loading_placeholder = __webpack_require__(750);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./style/media-queries.ts
var media_queries = __webpack_require__(76);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./container/app-shop/discount-code/style.tsx



/* harmony default export */ var style = ({
    container: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                display: variable["display"].block,
                position: variable["position"].relative
            }],
        DESKTOP: [{
                paddingTop: 30,
                marginBottom: 30,
                minHeight: 400,
                display: variable["display"].flex,
                alignItems: 'center'
            }],
        GENERAL: [{
                zIndex: variable["zIndex5"]
            }]
    }),
    row: {
        display: variable["display"].flex,
        flexWrap: 'wrap',
        paddingTop: 5,
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 5,
    },
    itemWrap: {
        padding: 5
    },
    column4: (style_a = {
            width: '50%'
        },
        style_a[media_queries["a" /* default */].tablet960] = {
            width: '25%',
        },
        style_a),
    column5: (style_b = {
            width: '50%'
        },
        style_b[media_queries["a" /* default */].tablet960] = {
            width: '20%',
        },
        style_b),
    customStyleLoading: {
        height: 400
    },
    placeholder: {
        width: '100%',
        paddingTop: 10,
        title: {
            background: variable["colorF0"],
            display: variable["display"].block,
            width: '50%',
            height: 40,
            margin: '0 auto 30px'
        },
        titleMobile: {
            margin: 0,
            textAlign: 'left'
        },
        control: {
            width: '100%',
            display: variable["display"].flex,
            justifyContent: 'space-between',
            paddingLeft: 10,
            paddingRight: 10,
            marginBottom: 20,
        },
        controlItem: {
            width: 150,
            height: 30,
            background: variable["colorF7"]
        },
        productList: {
            display: variable["display"].flex,
            flexWrap: 'wrap',
            paddingTop: 20,
        },
        productMobileItem: {
            minWidth: '50%',
            width: '50%',
        },
        productItem: {
            flex: 1,
            paddingLeft: 10,
            paddingRight: 10,
            marginBottom: 20,
            minWidth: '25%',
            width: '25%',
            image: {
                width: '100%',
                height: 'auto',
                paddingTop: '82%',
                marginBottom: 10,
            },
            text: {
                width: '94%',
                height: 25,
                marginBottom: 10,
            },
            lastText: {
                width: '65%',
                height: 25,
            }
        },
    },
    desc: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                fontSize: 16,
                paddingTop: 10,
                marginBottom: 10
            }],
        DESKTOP: [{
                fontSize: 20,
                marginBottom: 20
            }],
        GENERAL: [{
                textAlign: 'center',
                fontFamily: variable["fontAvenirMedium"]
            }]
    }),
    headerMenuContainer: {
        display: variable["display"].block,
        position: variable["position"].relative,
        height: 50,
        maxHeight: 50,
        background: variable["colorWhite08"],
        headerMenuWrap: {
            width: '100%',
            height: '100%',
            display: variable["display"].flex,
            position: variable["position"].relative,
        },
        headerMenu: {
            display: variable["display"].flex,
            alignItems: 'center',
            justifyContent: 'flex-start',
            fontFamily: variable["fontAvenirMedium"],
            borderBottom: "1px solid " + variable["colorBlack005"],
            background: variable["colorWhite08"],
            backdropFilter: "blur(10px)",
            WebkitBackdropFilter: "blur(10px)",
            height: '100%',
            width: '100%',
            position: variable["position"].absolute,
            top: 0,
            left: 0,
            isTop: {
                position: variable["position"].fixed,
                top: 0,
                left: 0,
                width: '100%',
                height: 50,
                zIndex: variable["zIndexMax"]
            },
            textBreadCrumb: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        fontSize: 15,
                        lineHeight: '50px',
                        height: '100%',
                        width: '100%',
                        paddingLeft: 10,
                        color: variable["colorBlack"],
                        fontFamily: variable["fontTrirong"],
                        display: variable["display"].inlineBlock
                    }],
                DESKTOP: [{
                        fontSize: 18,
                        height: 60,
                        lineHeight: '60px',
                        textTransform: 'capitalize'
                    }],
                GENERAL: [{
                        color: variable["colorBlack"],
                        cursor: 'pointer',
                    }]
            }),
            icon: function (isOpening) { return ({
                width: 50,
                height: 50,
                color: variable["colorBlack08"],
                transition: variable["transitionNormal"],
                transform: isOpening ? 'rotate(-180deg)' : 'rotate(0deg)'
            }); },
            inner: {
                width: 10,
            }
        }
    },
    subContent: {
        container: function (existedImage) { return Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    marginTop: 0,
                    marginBottom: 10,
                    paddingLeft: 10,
                    paddingRight: 10,
                    paddingTop: 10,
                    paddingBottom: 10,
                }],
            DESKTOP: [{ marginTop: 10, marginBottom: 30, }],
            GENERAL: [{
                    position: variable["position"].sticky,
                    top: 20,
                    width: '100%',
                    maxWidth: '100%'
                }]
        }); },
        wrap: {
            container: function (existedImage) { return ({
                position: variable["position"].relative,
                paddingTop: existedImage ? 20 : 10,
                boxShadow: variable["shadowBlurSort"],
                borderRadius: 5,
            }); },
            imgContainer: function (imgUrl) { return ({
                width: 200,
                margin: '0 auto',
                height: 'auto',
                borderRadius: '50%',
                zIndex: variable["zIndex9"],
                backgroundImage: "url(" + imgUrl + ")",
                backgroundSize: 'contain',
                backgroundPosition: 'center center',
                backgroundRepeat: 'no-repeat',
                backgroundColor: variable["colorWhite"],
                display: variable["display"].block,
                marginBottom: 20
            }); },
            content: {
                paddingLeft: 20,
                paddingRight: 20,
                paddingBottom: 20,
                brandName: {
                    fontSize: 22,
                    fontFamily: variable["fontTrirong"],
                    textAlign: 'center',
                    marginBottom: 10,
                    textTransform: 'uppercase',
                    wordBreak: 'break-all'
                },
                description: {
                    textAlign: 'justify',
                    fontSize: 16,
                    color: variable["colorBlack06"]
                }
            }
        }
    }
});
var style_a, style_b;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLGFBQWEsTUFBTSw4QkFBOEIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFekQsZUFBZTtJQUNiLFNBQVMsRUFBRSxZQUFZLENBQUM7UUFDdEIsTUFBTSxFQUFFLENBQUM7Z0JBQ1AsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztnQkFDL0IsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTthQUNyQyxDQUFDO1FBRUYsT0FBTyxFQUFFLENBQUM7Z0JBQ1IsVUFBVSxFQUFFLEVBQUU7Z0JBQ2QsWUFBWSxFQUFFLEVBQUU7Z0JBQ2hCLFNBQVMsRUFBRSxHQUFHO2dCQUNkLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzlCLFVBQVUsRUFBRSxRQUFRO2FBQ3JCLENBQUM7UUFFRixPQUFPLEVBQUUsQ0FBQztnQkFDUixNQUFNLEVBQUUsUUFBUSxDQUFDLE9BQU87YUFDekIsQ0FBQztLQUNILENBQUM7SUFFRixHQUFHLEVBQUU7UUFDSCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1FBQzlCLFFBQVEsRUFBRSxNQUFNO1FBQ2hCLFVBQVUsRUFBRSxDQUFDO1FBQ2IsV0FBVyxFQUFFLENBQUM7UUFDZCxZQUFZLEVBQUUsQ0FBQztRQUNmLGFBQWEsRUFBRSxDQUFDO0tBQ2pCO0lBRUQsUUFBUSxFQUFFO1FBQ1IsT0FBTyxFQUFFLENBQUM7S0FDWDtJQUVELE9BQU87WUFDTCxLQUFLLEVBQUUsS0FBSzs7UUFFWixHQUFDLGFBQWEsQ0FBQyxTQUFTLElBQUc7WUFDekIsS0FBSyxFQUFFLEtBQUs7U0FDYjtXQUNGO0lBRUQsT0FBTztZQUNMLEtBQUssRUFBRSxLQUFLOztRQUVaLEdBQUMsYUFBYSxDQUFDLFNBQVMsSUFBRztZQUN6QixLQUFLLEVBQUUsS0FBSztTQUNiO1dBQ0Y7SUFFRCxrQkFBa0IsRUFBRTtRQUNsQixNQUFNLEVBQUUsR0FBRztLQUNaO0lBRUQsV0FBVyxFQUFFO1FBQ1gsS0FBSyxFQUFFLE1BQU07UUFDYixVQUFVLEVBQUUsRUFBRTtRQUVkLEtBQUssRUFBRTtZQUNMLFVBQVUsRUFBRSxRQUFRLENBQUMsT0FBTztZQUM1QixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1lBQy9CLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLEVBQUU7WUFDVixNQUFNLEVBQUUsYUFBYTtTQUN0QjtRQUVELFdBQVcsRUFBRTtZQUNYLE1BQU0sRUFBRSxDQUFDO1lBQ1QsU0FBUyxFQUFFLE1BQU07U0FDbEI7UUFFRCxPQUFPLEVBQUU7WUFDUCxLQUFLLEVBQUUsTUFBTTtZQUNiLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsY0FBYyxFQUFFLGVBQWU7WUFDL0IsV0FBVyxFQUFFLEVBQUU7WUFDZixZQUFZLEVBQUUsRUFBRTtZQUNoQixZQUFZLEVBQUUsRUFBRTtTQUNqQjtRQUVELFdBQVcsRUFBRTtZQUNYLEtBQUssRUFBRSxHQUFHO1lBQ1YsTUFBTSxFQUFFLEVBQUU7WUFDVixVQUFVLEVBQUUsUUFBUSxDQUFDLE9BQU87U0FDN0I7UUFFRCxXQUFXLEVBQUU7WUFDWCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLFFBQVEsRUFBRSxNQUFNO1lBQ2hCLFVBQVUsRUFBRSxFQUFFO1NBQ2Y7UUFFRCxpQkFBaUIsRUFBRTtZQUNqQixRQUFRLEVBQUUsS0FBSztZQUNmLEtBQUssRUFBRSxLQUFLO1NBQ2I7UUFFRCxXQUFXLEVBQUU7WUFDWCxJQUFJLEVBQUUsQ0FBQztZQUNQLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLEVBQUU7WUFDaEIsWUFBWSxFQUFFLEVBQUU7WUFDaEIsUUFBUSxFQUFFLEtBQUs7WUFDZixLQUFLLEVBQUUsS0FBSztZQUVaLEtBQUssRUFBRTtnQkFDTCxLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsTUFBTTtnQkFDZCxVQUFVLEVBQUUsS0FBSztnQkFDakIsWUFBWSxFQUFFLEVBQUU7YUFDakI7WUFFRCxJQUFJLEVBQUU7Z0JBQ0osS0FBSyxFQUFFLEtBQUs7Z0JBQ1osTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsWUFBWSxFQUFFLEVBQUU7YUFDakI7WUFFRCxRQUFRLEVBQUU7Z0JBQ1IsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osTUFBTSxFQUFFLEVBQUU7YUFDWDtTQUNGO0tBQ0Y7SUFFRCxJQUFJLEVBQUUsWUFBWSxDQUFDO1FBQ2pCLE1BQU0sRUFBRSxDQUFDO2dCQUNQLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxFQUFFO2dCQUNkLFlBQVksRUFBRSxFQUFFO2FBQ2pCLENBQUM7UUFFRixPQUFPLEVBQUUsQ0FBQztnQkFDUixRQUFRLEVBQUUsRUFBRTtnQkFDWixZQUFZLEVBQUUsRUFBRTthQUNqQixDQUFDO1FBRUYsT0FBTyxFQUFFLENBQUM7Z0JBQ1IsU0FBUyxFQUFFLFFBQVE7Z0JBQ25CLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO2FBQ3RDLENBQUM7S0FDSCxDQUFDO0lBRUYsbUJBQW1CLEVBQUU7UUFDbkIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztRQUMvQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1FBQ3BDLE1BQU0sRUFBRSxFQUFFO1FBQ1YsU0FBUyxFQUFFLEVBQUU7UUFDYixVQUFVLEVBQUUsUUFBUSxDQUFDLFlBQVk7UUFFakMsY0FBYyxFQUFFO1lBQ2QsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsTUFBTTtZQUNkLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtTQUNyQztRQUVELFVBQVUsRUFBRTtZQUNWLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsVUFBVSxFQUFFLFFBQVE7WUFDcEIsY0FBYyxFQUFFLFlBQVk7WUFDNUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7WUFDckMsWUFBWSxFQUFFLGVBQWEsUUFBUSxDQUFDLGFBQWU7WUFDbkQsVUFBVSxFQUFFLFFBQVEsQ0FBQyxZQUFZO1lBQ2pDLGNBQWMsRUFBRSxZQUFZO1lBQzVCLG9CQUFvQixFQUFFLFlBQVk7WUFDbEMsTUFBTSxFQUFFLE1BQU07WUFDZCxLQUFLLEVBQUUsTUFBTTtZQUNiLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDcEMsR0FBRyxFQUFFLENBQUM7WUFDTixJQUFJLEVBQUUsQ0FBQztZQUVQLEtBQUssRUFBRTtnQkFDTCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLO2dCQUNqQyxHQUFHLEVBQUUsQ0FBQztnQkFDTixJQUFJLEVBQUUsQ0FBQztnQkFDUCxLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsRUFBRTtnQkFDVixNQUFNLEVBQUUsUUFBUSxDQUFDLFNBQVM7YUFDM0I7WUFFRCxjQUFjLEVBQUUsWUFBWSxDQUFDO2dCQUMzQixNQUFNLEVBQUUsQ0FBQzt3QkFDUCxRQUFRLEVBQUUsRUFBRTt3QkFDWixVQUFVLEVBQUUsTUFBTTt3QkFDbEIsTUFBTSxFQUFFLE1BQU07d0JBQ2QsS0FBSyxFQUFFLE1BQU07d0JBQ2IsV0FBVyxFQUFFLEVBQUU7d0JBQ2YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO3dCQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLFdBQVc7d0JBQ2hDLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLFdBQVc7cUJBQ3RDLENBQUM7Z0JBRUYsT0FBTyxFQUFFLENBQUM7d0JBQ1IsUUFBUSxFQUFFLEVBQUU7d0JBQ1osTUFBTSxFQUFFLEVBQUU7d0JBQ1YsVUFBVSxFQUFFLE1BQU07d0JBQ2xCLGFBQWEsRUFBRSxZQUFZO3FCQUM1QixDQUFDO2dCQUVGLE9BQU8sRUFBRSxDQUFDO3dCQUNSLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTt3QkFDMUIsTUFBTSxFQUFFLFNBQVM7cUJBQ2xCLENBQUM7YUFDSCxDQUFDO1lBRUYsSUFBSSxFQUFFLFVBQUMsU0FBUyxJQUFLLE9BQUEsQ0FBQztnQkFDcEIsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxZQUFZO2dCQUM1QixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtnQkFDckMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLGNBQWM7YUFDMUQsQ0FBQyxFQU5tQixDQU1uQjtZQUVGLEtBQUssRUFBRTtnQkFDTCxLQUFLLEVBQUUsRUFBRTthQUNWO1NBQ0Y7S0FDRjtJQUVELFVBQVUsRUFBRTtRQUNWLFNBQVMsRUFBRSxVQUFDLFlBQVksSUFBSyxPQUFBLFlBQVksQ0FBQztZQUN4QyxNQUFNLEVBQUUsQ0FBQztvQkFDUCxTQUFTLEVBQUUsQ0FBQztvQkFDWixZQUFZLEVBQUUsRUFBRTtvQkFDaEIsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLFVBQVUsRUFBRSxFQUFFO29CQUNkLGFBQWEsRUFBRSxFQUFFO2lCQUNsQixDQUFDO1lBQ0YsT0FBTyxFQUFFLENBQUMsRUFBRSxTQUFTLEVBQUUsRUFBRSxFQUFFLFlBQVksRUFBRSxFQUFFLEdBQUcsQ0FBQztZQUMvQyxPQUFPLEVBQUUsQ0FBQztvQkFDUixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxNQUFNO29CQUNsQyxHQUFHLEVBQUUsRUFBRTtvQkFDUCxLQUFLLEVBQUUsTUFBTTtvQkFDYixRQUFRLEVBQUUsTUFBTTtpQkFDakIsQ0FBQztTQUNILENBQUMsRUFoQjJCLENBZ0IzQjtRQUVGLElBQUksRUFBRTtZQUNKLFNBQVMsRUFBRSxVQUFDLFlBQVksSUFBSyxPQUFBLENBQUM7Z0JBQzVCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLFVBQVUsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDbEMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxjQUFjO2dCQUNsQyxZQUFZLEVBQUUsQ0FBQzthQUNoQixDQUFDLEVBTDJCLENBSzNCO1lBRUYsWUFBWSxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztnQkFDekIsS0FBSyxFQUFFLEdBQUc7Z0JBQ1YsTUFBTSxFQUFFLFFBQVE7Z0JBQ2hCLE1BQU0sRUFBRSxNQUFNO2dCQUNkLFlBQVksRUFBRSxLQUFLO2dCQUNuQixNQUFNLEVBQUUsUUFBUSxDQUFDLE9BQU87Z0JBQ3hCLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztnQkFDakMsY0FBYyxFQUFFLFNBQVM7Z0JBQ3pCLGtCQUFrQixFQUFFLGVBQWU7Z0JBQ25DLGdCQUFnQixFQUFFLFdBQVc7Z0JBQzdCLGVBQWUsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDcEMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztnQkFDL0IsWUFBWSxFQUFFLEVBQUU7YUFDakIsQ0FBQyxFQWJ3QixDQWF4QjtZQUVGLE9BQU8sRUFBRTtnQkFDUCxXQUFXLEVBQUUsRUFBRTtnQkFDZixZQUFZLEVBQUUsRUFBRTtnQkFDaEIsYUFBYSxFQUFFLEVBQUU7Z0JBRWpCLFNBQVMsRUFBRTtvQkFDVCxRQUFRLEVBQUUsRUFBRTtvQkFDWixVQUFVLEVBQUUsUUFBUSxDQUFDLFdBQVc7b0JBQ2hDLFNBQVMsRUFBRSxRQUFRO29CQUNuQixZQUFZLEVBQUUsRUFBRTtvQkFDaEIsYUFBYSxFQUFFLFdBQVc7b0JBQzFCLFNBQVMsRUFBRSxXQUFXO2lCQUN2QjtnQkFFRCxXQUFXLEVBQUU7b0JBQ1gsU0FBUyxFQUFFLFNBQVM7b0JBQ3BCLFFBQVEsRUFBRSxFQUFFO29CQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTtpQkFDN0I7YUFDRjtTQUNGO0tBRUY7Q0FDSyxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/discount-code/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};










var renderItemPlaceholder = function (item) { return (react["createElement"]("div", { style: [
        style.placeholder.productItem,
        'MOBILE' === window.DEVICE_VERSION && style.placeholder.productMobileItem
    ], key: item },
    react["createElement"](loading_placeholder["a" /* default */], { style: style.placeholder.productItem.image }),
    react["createElement"](loading_placeholder["a" /* default */], { style: style.placeholder.productItem.text }),
    react["createElement"](loading_placeholder["a" /* default */], { style: style.placeholder.productItem.text }),
    react["createElement"](loading_placeholder["a" /* default */], { style: style.placeholder.productItem.lastText }))); };
var renderLoadingPlaceholder = function () {
    var list = 'MOBILE' === window.DEVICE_VERSION ? [1, 2, 3, 4] : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
    return (react["createElement"]("div", { style: style.placeholder },
        react["createElement"]("div", { style: style.placeholder.productList }, Array.isArray(list) && list.map(renderItemPlaceholder))));
};
var renderHeader = function (_a) {
    var title = _a.title;
    var headerStyle = style.headerMenuContainer;
    return (react["createElement"](react_router_dom["NavLink"], { to: routing["Qa" /* ROUTING_MOBILE_BRAND_PATH */], style: headerStyle },
        react["createElement"]("div", { style: headerStyle.headerMenu, id: 'discount-code-detail-menu', className: 'backdrop-blur' },
            react["createElement"]("div", { style: headerStyle.headerMenuWrap },
                react["createElement"]("div", { style: headerStyle.headerMenu.textBreadCrumb }, title)))));
};
var renderContent = function (_a) {
    var discountCodeInfo = _a.discountCodeInfo, type = _a.type, column = _a.column, _b = _a.isLoading, isLoading = _b === void 0 ? false : _b;
    return isLoading
        ? renderLoadingPlaceholder()
        : (react["createElement"]("div", null,
            react["createElement"]("div", { style: style.desc }, !Object(validate["j" /* isEmptyObject */])(discountCodeInfo)
                && !Object(validate["l" /* isUndefined */])(discountCodeInfo.description)
                && discountCodeInfo.description || ''),
            react["createElement"]("div", { style: style.row }, !Object(validate["j" /* isEmptyObject */])(discountCodeInfo)
                && Array.isArray(discountCodeInfo.boxes)
                && discountCodeInfo.boxes.map(function (product) {
                    var productProps = { type: type, data: Object.assign({}, product), showQuickView: true };
                    return (react["createElement"]("div", { key: "brand-product-" + product.id, style: [style.itemWrap, style["column" + column]] },
                        react["createElement"](detail_item["a" /* default */], __assign({}, productProps))));
                }))));
};
var renderView = function (_a) {
    var props = _a.props, state = _a.state;
    var _b = props, type = _b.type, column = _b.column, discountCode = _b.match.params.discountCode, discountCodes = _b.discountCodeStore.discountCodes;
    var isLoading = state.isLoading;
    var keyHash = Object(encode["j" /* objectToHash */])({ code: discountCode });
    var discountCodeInfo = !Object(validate["j" /* isEmptyObject */])(discountCodes)
        && !Object(validate["l" /* isUndefined */])(discountCodes[keyHash])
        && !Object(validate["j" /* isEmptyObject */])(discountCodes[keyHash])
        && discountCodes[keyHash] || {};
    var title = discountCode
        ? "Discount code: #" + discountCode.toUpperCase() : '';
    var mainBlockMobileProps = {
        showHeader: false,
        showViewMore: false,
        content: react["createElement"]("div", null,
            renderHeader({ title: title }),
            renderContent({ discountCodeInfo: discountCodeInfo, type: type, column: column, isLoading: isLoading })),
        style: {}
    };
    var mainBlockDesktopProps = {
        title: title,
        style: {},
        showHeader: true,
        showViewMore: false,
        content: renderContent({ discountCodeInfo: discountCodeInfo, type: type, column: column, isLoading: isLoading })
    };
    var switchVersion = {
        MOBILE: mainBlockMobileProps,
        DESKTOP: mainBlockDesktopProps
    };
    var mainBlockProps = switchVersion[window.DEVICE_VERSION];
    return (react["createElement"]("discount-code", { style: style.container },
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"](main_block["a" /* default */], __assign({}, mainBlockProps)))));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sU0FBUyxNQUFNLHlCQUF5QixDQUFDO0FBQ2hELE9BQU8sVUFBVSxNQUFNLG1CQUFtQixDQUFDO0FBQzNDLE9BQU8saUJBQWlCLE1BQU0seUNBQXlDLENBQUM7QUFDeEUsT0FBTyxrQkFBa0IsTUFBTSw0Q0FBNEMsQ0FBQztBQUU1RSxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUNuRixPQUFPLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ3JFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUVyRCxPQUFPLEtBQUssTUFBTSxTQUFTLENBQUM7QUFHNUIsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQUcsVUFBQyxJQUFJLElBQUssT0FBQSxDQUM3Qyw2QkFDRSxLQUFLLEVBQUU7UUFDTCxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVc7UUFDN0IsUUFBUSxLQUFLLE1BQU0sQ0FBQyxjQUFjLElBQUksS0FBSyxDQUFDLFdBQVcsQ0FBQyxpQkFBaUI7S0FDMUUsRUFDRCxHQUFHLEVBQUUsSUFBSTtJQUNULG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEdBQUk7SUFDbEUsb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksR0FBSTtJQUNqRSxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxHQUFJO0lBQ2pFLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxRQUFRLEdBQUksQ0FDakUsQ0FDUCxFQVo4QyxDQVk5QyxDQUFDO0FBRUYsSUFBTSx3QkFBd0IsR0FBRztJQUMvQixJQUFNLElBQUksR0FBRyxRQUFRLEtBQUssTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ3JILE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVztRQUMzQiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLElBQ3RDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUNuRCxDQUNGLENBQ1AsQ0FBQTtBQUNILENBQUMsQ0FBQztBQUVGLElBQU0sWUFBWSxHQUFHLFVBQUMsRUFBUztRQUFQLGdCQUFLO0lBQzNCLElBQU0sV0FBVyxHQUFHLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQztJQUU5QyxNQUFNLENBQUMsQ0FDTCxvQkFBQyxPQUFPLElBQUMsRUFBRSxFQUFFLHlCQUF5QixFQUFFLEtBQUssRUFBRSxXQUFXO1FBQ3hELDZCQUFLLEtBQUssRUFBRSxXQUFXLENBQUMsVUFBVSxFQUFFLEVBQUUsRUFBRSwyQkFBMkIsRUFBRSxTQUFTLEVBQUUsZUFBZTtZQUM3Riw2QkFBSyxLQUFLLEVBQUUsV0FBVyxDQUFDLGNBQWM7Z0JBQ3BDLDZCQUFLLEtBQUssRUFBRSxXQUFXLENBQUMsVUFBVSxDQUFDLGNBQWMsSUFBRyxLQUFLLENBQU8sQ0FDNUQsQ0FDRixDQUNFLENBQ1gsQ0FBQTtBQUNILENBQUMsQ0FBQztBQUVGLElBQU0sYUFBYSxHQUFHLFVBQUMsRUFBcUQ7UUFBbkQsc0NBQWdCLEVBQUUsY0FBSSxFQUFFLGtCQUFNLEVBQUUsaUJBQWlCLEVBQWpCLHNDQUFpQjtJQUN4RSxNQUFNLENBQUMsU0FBUztRQUNkLENBQUMsQ0FBQyx3QkFBd0IsRUFBRTtRQUM1QixDQUFDLENBQUMsQ0FDQTtZQUNFLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxJQUVsQixDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQzttQkFDN0IsQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDO21CQUMxQyxnQkFBZ0IsQ0FBQyxXQUFXLElBQUksRUFBRSxDQUVuQztZQUNOLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsR0FBRyxJQUVqQixDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQzttQkFDN0IsS0FBSyxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUM7bUJBQ3JDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsVUFBQyxPQUFPO29CQUNwQyxJQUFNLFlBQVksR0FBRyxFQUFFLElBQUksTUFBQSxFQUFFLElBQUksRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxPQUFPLENBQUMsRUFBRSxhQUFhLEVBQUUsSUFBSSxFQUFFLENBQUE7b0JBRXBGLE1BQU0sQ0FBQyxDQUNMLDZCQUNFLEdBQUcsRUFBRSxtQkFBaUIsT0FBTyxDQUFDLEVBQUksRUFDbEMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsV0FBUyxNQUFRLENBQUMsQ0FBQzt3QkFDakQsb0JBQUMsaUJBQWlCLGVBQUssWUFBWSxFQUFJLENBQ25DLENBQ1AsQ0FBQTtnQkFDSCxDQUFDLENBQUMsQ0FFQSxDQUNGLENBQ1AsQ0FBQTtBQUNMLENBQUMsQ0FBQztBQUVGLElBQU0sVUFBVSxHQUFHLFVBQUMsRUFBZ0I7UUFBZCxnQkFBSyxFQUFFLGdCQUFLO0lBQzFCLElBQUEsVUFLYSxFQUpqQixjQUFJLEVBQ0osa0JBQU0sRUFDYSwyQ0FBWSxFQUNWLGtEQUFhLENBQ2hCO0lBRVosSUFBQSwyQkFBUyxDQUFxQjtJQUV0QyxJQUFNLE9BQU8sR0FBRyxZQUFZLENBQUMsRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLENBQUMsQ0FBQztJQUVyRCxJQUFNLGdCQUFnQixHQUFHLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQztXQUNqRCxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7V0FDcEMsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1dBQ3RDLGFBQWEsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7SUFFbEMsSUFBTSxLQUFLLEdBQUcsWUFBWTtRQUN4QixDQUFDLENBQUMscUJBQW1CLFlBQVksQ0FBQyxXQUFXLEVBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBRXpELElBQU0sb0JBQW9CLEdBQUc7UUFDM0IsVUFBVSxFQUFFLEtBQUs7UUFDakIsWUFBWSxFQUFFLEtBQUs7UUFDbkIsT0FBTyxFQUNMO1lBQ0csWUFBWSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsQ0FBQztZQUN2QixhQUFhLENBQUMsRUFBRSxnQkFBZ0Isa0JBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxNQUFNLFFBQUEsRUFBRSxTQUFTLFdBQUEsRUFBRSxDQUFDLENBQ3pEO1FBQ1IsS0FBSyxFQUFFLEVBQUU7S0FDVixDQUFDO0lBRUYsSUFBTSxxQkFBcUIsR0FBRztRQUM1QixLQUFLLE9BQUE7UUFDTCxLQUFLLEVBQUUsRUFBRTtRQUNULFVBQVUsRUFBRSxJQUFJO1FBQ2hCLFlBQVksRUFBRSxLQUFLO1FBQ25CLE9BQU8sRUFBRSxhQUFhLENBQUMsRUFBRSxnQkFBZ0Isa0JBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxNQUFNLFFBQUEsRUFBRSxTQUFTLFdBQUEsRUFBRSxDQUFDO0tBQ3RFLENBQUM7SUFFRixJQUFNLGFBQWEsR0FBRztRQUNwQixNQUFNLEVBQUUsb0JBQW9CO1FBQzVCLE9BQU8sRUFBRSxxQkFBcUI7S0FDL0IsQ0FBQztJQUVGLElBQU0sY0FBYyxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUM7SUFFNUQsTUFBTSxDQUFDLENBQ0wsdUNBQWUsS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTO1FBQ25DLG9CQUFDLFVBQVU7WUFDVCxvQkFBQyxTQUFTLGVBQUssY0FBYyxFQUFJLENBQ3RCLENBQ0MsQ0FDakIsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/discount-code/container.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var connect = __webpack_require__(129).connect;





var container_DiscountCodeContainer = /** @class */ (function (_super) {
    __extends(DiscountCodeContainer, _super);
    function DiscountCodeContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    DiscountCodeContainer.prototype.init = function (props) {
        if (props === void 0) { props = this.props; }
        var _a = props, fetchDiscountCodesByCodeAction = _a.fetchDiscountCodesByCodeAction, discountCode = _a.match.params.discountCode;
        var params = { code: discountCode };
        this.setState({ isLoading: true }, fetchDiscountCodesByCodeAction(params));
    };
    DiscountCodeContainer.prototype.componentDidMount = function () {
        this.init(this.props);
    };
    DiscountCodeContainer.prototype.componentWillReceiveProps = function (nextProps) {
        var _a = this.props, updateMetaInfoAction = _a.updateMetaInfoAction, discountCode = _a.match.params.discountCode, isFetchDiscountCodeSuccess = _a.discountCodeStore.isFetchDiscountCodeSuccess;
        discountCode !== nextProps.match.params.discountCode
            && this.init(nextProps);
        !isFetchDiscountCodeSuccess
            && nextProps.discountCodeStore.isFetchDiscountCodeSuccess
            && this.setState({ isLoading: false });
        // Set meta for SEO
        var params = { code: discountCode };
        var keyHash = Object(encode["j" /* objectToHash */])(params);
        !isFetchDiscountCodeSuccess
            && nextProps.discountCodeStore.isFetchDiscountCodeSuccess
            && !Object(validate["j" /* isEmptyObject */])(nextProps.discountCodeStore.discountCodes[keyHash])
            && updateMetaInfoAction({
                info: {
                    url: "http://lxbtest.tk/discount-code/" + nextProps.match.params.discountCode,
                    type: "article",
                    title: "Discount code " + nextProps.discountCodeStore.discountCodes[keyHash].code,
                    description: nextProps.discountCodeStore.discountCodes[keyHash].description,
                    keyword: "lixibox, makeup, skincare, halio, okame, discount code"
                }
            });
    };
    DiscountCodeContainer.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state
        };
        return view(args);
    };
    DiscountCodeContainer.defaultProps = DEFAULT_PROPS;
    DiscountCodeContainer = __decorate([
        radium
    ], DiscountCodeContainer);
    return DiscountCodeContainer;
}(react["Component"]));
;
/* harmony default export */ var container = __webpack_exports__["default"] = (connect(mapStateToProps, mapDispatchToProps)(container_DiscountCodeContainer));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFDakMsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDckQsT0FBTyxFQUFlLGFBQWEsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBR3JFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxlQUFlLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFDOUQsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDNUQsT0FBTyxVQUFVLE1BQU0sUUFBUSxDQUFDO0FBR2hDO0lBQW9DLHlDQUErQjtJQUVqRSwrQkFBWSxLQUFhO1FBQXpCLFlBQ0Usa0JBQU0sS0FBSyxDQUFDLFNBRWI7UUFEQyxLQUFJLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQzs7SUFDN0IsQ0FBQztJQUVELG9DQUFJLEdBQUosVUFBSyxLQUFrQjtRQUFsQixzQkFBQSxFQUFBLFFBQVEsSUFBSSxDQUFDLEtBQUs7UUFDZixJQUFBLFVBR2EsRUFGakIsa0VBQThCLEVBQ1gsMkNBQVksQ0FDYjtRQUVwQixJQUFNLE1BQU0sR0FBRyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsQ0FBQztRQUV0QyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxFQUFFLDhCQUE4QixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDN0UsQ0FBQztJQUVELGlEQUFpQixHQUFqQjtRQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3hCLENBQUM7SUFFRCx5REFBeUIsR0FBekIsVUFBMEIsU0FBUztRQUMzQixJQUFBLGVBSVEsRUFIWiw4Q0FBb0IsRUFDRCwyQ0FBWSxFQUNWLDRFQUEwQixDQUNsQztRQUVmLFlBQVksS0FBSyxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZO2VBQy9DLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFMUIsQ0FBQywwQkFBMEI7ZUFDdEIsU0FBUyxDQUFDLGlCQUFpQixDQUFDLDBCQUEwQjtlQUN0RCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7UUFFekMsbUJBQW1CO1FBQ25CLElBQU0sTUFBTSxHQUFHLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxDQUFDO1FBQ3RDLElBQU0sT0FBTyxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUVyQyxDQUFDLDBCQUEwQjtlQUN0QixTQUFTLENBQUMsaUJBQWlCLENBQUMsMEJBQTBCO2VBQ3RELENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7ZUFDbEUsb0JBQW9CLENBQUM7Z0JBQ3RCLElBQUksRUFBRTtvQkFDSixHQUFHLEVBQUUsMkNBQXlDLFNBQVMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQWM7b0JBQ25GLElBQUksRUFBRSxTQUFTO29CQUNmLEtBQUssRUFBRSxtQkFBaUIsU0FBUyxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFNO29CQUNqRixXQUFXLEVBQUUsU0FBUyxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxXQUFXO29CQUMzRSxPQUFPLEVBQUUsd0RBQXdEO2lCQUNsRTthQUNGLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxzQ0FBTSxHQUFOO1FBQ0UsSUFBTSxJQUFJLEdBQUc7WUFDWCxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1NBQ2xCLENBQUM7UUFFRixNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUE1RE0sa0NBQVksR0FBVyxhQUFhLENBQUM7SUFEeEMscUJBQXFCO1FBRDFCLE1BQU07T0FDRCxxQkFBcUIsQ0E4RDFCO0lBQUQsNEJBQUM7Q0FBQSxBQTlERCxDQUFvQyxLQUFLLENBQUMsU0FBUyxHQThEbEQ7QUFBQSxDQUFDO0FBRUYsZUFBZSxPQUFPLENBQ3BCLGVBQWUsRUFDZixrQkFBa0IsQ0FDbkIsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDIn0=

/***/ })

}]);