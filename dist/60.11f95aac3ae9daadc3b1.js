(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[60],{

/***/ 916:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./container/app-shop/info/about/view-about-us.tsx
var view_about_us = __webpack_require__(847);

// EXTERNAL MODULE: ./container/app-shop/info/about/view-term.tsx
var view_term = __webpack_require__(846);

// EXTERNAL MODULE: ./container/layout/wrap/index.tsx
var wrap = __webpack_require__(746);

// EXTERNAL MODULE: ./container/app-shop/info/about/style.tsx
var style = __webpack_require__(767);

// CONCATENATED MODULE: ./container/app-shop/info/about/view-privacy.tsx




var renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("h2", null, "Thu th\u1EADp th\u00F4ng tin c\u00E1 nh\u00E2n"),
                "lixibox.com kh\u00F4ng b\u00E1n, chia s\u1EBB hay trao \u0111\u1ED5i th\u00F4ng tin c\u00E1 nh\u00E2n c\u1EE7a kh\u00E1ch h\u00E0ng thu th\u1EADp tr\u00EAn trang web cho m\u1ED9t b\u00EAn th\u1EE9 ba n\u00E0o kh\u00E1c.",
                react["createElement"]("p", null, "Th\u00F4ng tin c\u00E1 nh\u00E2n thu th\u1EADp \u0111\u01B0\u1EE3c s\u1EBD ch\u1EC9 \u0111\u01B0\u1EE3c s\u1EED d\u1EE5ng trong n\u1ED9i b\u1ED9 c\u00F4ng ty."),
                react["createElement"]("p", null, "Khi qu\u00FD kh\u00E1ch \u0111\u0103ng k\u00FD t\u00E0i kho\u1EA3n lixibox.com, th\u00F4ng tin c\u00E1 nh\u00E2n m\u00E0 ch\u00FAng t\u00F4i thu th\u1EADp bao g\u1ED3m:"),
                react["createElement"]("p", null, "- H\u1ECD v\u00E0 t\u00EAn"),
                react["createElement"]("p", null, "- S\u1ED1 \u0111i\u1EC7n tho\u1EA1i"),
                react["createElement"]("p", null, "- \u0110\u1ECBa ch\u1EC9 Email"),
                react["createElement"]("p", null, "- 4 s\u1ED1 cu\u1ED1i v\u00E0 ng\u00E0y h\u1EBFt h\u1EA1n tr\u00EAn th\u1EBB t\u00EDn d\u1EE5ng"),
                react["createElement"]("p", null, "Nh\u1EEFng th\u00F4ng tin tr\u00EAn s\u1EBD \u0111\u01B0\u1EE3c s\u1EED d\u1EE5ng cho m\u1ED9t ho\u1EB7c t\u1EA5t c\u1EA3 c\u00E1c m\u1EE5c \u0111\u00EDch sau \u0111\u00E2y:"),
                react["createElement"]("p", null, "- Giao h\u00E0ng qu\u00FD kh\u00E1ch \u0111\u00E3 mua t\u1EA1i lixibox.com"),
                react["createElement"]("p", null, "- Th\u00F4ng b\u00E1o v\u1EC1 vi\u1EC7c giao h\u00E0ng v\u00E0 h\u1ED7 tr\u1EE3 kh\u00E1ch h\u00E0ng"),
                react["createElement"]("p", null, "- Cung c\u1EA5p th\u00F4ng tin li\u00EAn quan \u0111\u1EBFn s\u1EA3n ph\u1EA9m"),
                react["createElement"]("p", null, "- X\u1EED l\u00FD \u0111\u01A1n \u0111\u1EB7t h\u00E0ng v\u00E0 cung c\u1EA5p d\u1ECBch v\u1EE5 v\u00E0 th\u00F4ng tin qua trang web c\u1EE7a ch\u00FAng t\u00F4i theo y\u00EAu c\u1EA7u c\u1EE7a qu\u00FD kh\u00E1ch"),
                react["createElement"]("p", null, "- Ngo\u00E0i ra, ch\u00FAng t\u00F4i s\u1EBD s\u1EED d\u1EE5ng th\u00F4ng tin qu\u00FD kh\u00E1ch cung c\u1EA5p \u0111\u1EC3 h\u1ED7 tr\u1EE3 qu\u1EA3n l\u00FD t\u00E0i kho\u1EA3n kh\u00E1ch h\u00E0ng; x\u00E1c nh\u1EADn v\u00E0 th\u1EF1c hi\u1EC7n c\u00E1c giao d\u1ECBch t\u00E0i ch\u00EDnh li\u00EAn quan \u0111\u1EBFn c\u00E1c kho\u1EA3n thanh to\u00E1n tr\u1EF1c tuy\u1EBFn c\u1EE7a qu\u00FD kh\u00E1ch; ki\u1EC3m tra d\u1EEF li\u1EC7u t\u1EA3i t\u1EEB trang web c\u1EE7a ch\u00FAng t\u00F4i; c\u1EA3i thi\u1EC7n giao di\u1EC7n v\u00E0/ho\u1EB7c n\u1ED9i dung c\u1EE7a c\u00E1c trang m\u1EE5c tr\u00EAn trang web v\u00E0 t\u00F9y ch\u1EC9nh \u0111\u1EC3 d\u1EC5 d\u00E0ng h\u01A1n khi s\u1EED d\u1EE5ng; nh\u1EADn di\u1EC7n kh\u00E1ch \u0111\u1EBFn th\u0103m trang web; nghi\u00EAn c\u1EE9u v\u1EC1 nh\u00E2n kh\u1EA9u h\u1ECDc c\u1EE7a ng\u01B0\u1EDDi s\u1EED d\u1EE5ng; g\u1EEDi \u0111\u1EBFn qu\u00FD kh\u00E1ch th\u00F4ng tin m\u00E0 ch\u00FAng t\u00F4i ngh\u0129 s\u1EBD c\u00F3 \u00EDch ho\u1EB7c do qu\u00FD kh\u00E1ch y\u00EAu c\u1EA7u, bao g\u1ED3m th\u00F4ng tin v\u1EC1 s\u1EA3n ph\u1EA9m v\u00E0 d\u1ECBch v\u1EE5, v\u1EDBi \u0111i\u1EC1u ki\u1EC7n qu\u00FD kh\u00E1ch \u0111\u1ED3ng \u00FD kh\u00F4ng ph\u1EA3n \u0111\u1ED1i vi\u1EC7c \u0111\u01B0\u1EE3c li\u00EAn l\u1EA1c cho c\u00E1c m\u1EE5c \u0111\u00EDch tr\u00EAn."),
                react["createElement"]("p", null, "Ch\u00FAng t\u00F4i c\u00F3 th\u1EC3 chia s\u1EBB t\u00EAn v\u00E0 \u0111\u1ECBa ch\u1EC9 c\u1EE7a qu\u00FD kh\u00E1ch cho d\u1ECBch v\u1EE5 chuy\u1EC3n ph\u00E1t nhanh ho\u1EB7c nh\u00E0 cung c\u1EA5p c\u1EE7a ch\u00FAng t\u00F4i \u0111\u1EC3 c\u00F3 th\u1EC3 giao h\u00E0ng cho qu\u00FD kh\u00E1ch."),
                react["createElement"]("p", null, "Khi qu\u00FD kh\u00E1ch \u0111\u0103ng k\u00FD l\u00E0m th\u00E0nh vi\u00EAn tr\u00EAn trang web lixibox.com, ch\u00FAng t\u00F4i c\u0169ng s\u1EBD s\u1EED d\u1EE5ng th\u00F4ng tin c\u00E1 nh\u00E2n c\u1EE7a qu\u00FD kh\u00E1ch \u0111\u1EC3 g\u1EEDi c\u00E1c th\u00F4ng tin khuy\u1EBFn m\u00E3i/ti\u1EBFp th\u1ECB. Qu\u00FD kh\u00E1ch c\u00F3 th\u1EC3 h\u1EE7y nh\u1EADn c\u00E1c th\u00F4ng tin \u0111\u00F3 b\u1EA5t k\u1EF3 l\u00FAc n\u00E0o b\u1EB1ng c\u00E1ch s\u1EED d\u1EE5ng ch\u1EE9c n\u0103ng h\u1EE7y \u0111\u0103ng k\u00FD trong c\u00E1c th\u00F4ng b\u00E1o qu\u1EA3ng c\u00E1o."),
                react["createElement"]("p", null, "C\u00E1c kho\u1EA3n thanh to\u00E1n m\u00E0 qu\u00FD kh\u00E1ch th\u1EF1c hi\u1EC7n qua trang web s\u1EBD \u0111\u01B0\u1EE3c x\u1EED l\u00FD b\u1EDFi c\u00F4ng ty c\u1EE7a ch\u00FAng t\u00F4i, C\u00F4ng Ty C\u1ED5 Ph\u1EA7n \u0110\u1EA7u T\u01B0 Th\u01B0\u01A1ng M\u1EA1i Lixibox. Qu\u00FD kh\u00E1ch ph\u1EA3i cung c\u1EA5p cho ch\u00FAng t\u00F4i ho\u1EB7c \u0111\u1EA1i l\u00FD c\u1EE7a ch\u00FAng t\u00F4i ho\u1EB7c trang web nh\u1EEFng th\u00F4ng tin ch\u00EDnh x\u00E1c v\u00E0 ph\u1EA3i lu\u00F4n c\u1EADp nh\u1EADt th\u00F4ng tin v\u00E0 b\u00E1o cho ch\u00FAng t\u00F4i bi\u1EBFt n\u1EBFu c\u00F3 thay \u0111\u1ED5i."),
                react["createElement"]("p", null, "Chi ti\u1EBFt \u0111\u01A1n h\u00E0ng c\u1EE7a qu\u00FD kh\u00E1ch s\u1EBD \u0111\u01B0\u1EE3c ch\u00FAng t\u00F4i l\u01B0u tr\u1EEF nh\u01B0ng v\u00EC l\u00FD do b\u1EA3o m\u1EADt, qu\u00FD kh\u00E1ch kh\u00F4ng th\u1EC3 y\u00EAu c\u1EA7u th\u00F4ng tin \u0111\u00F3 t\u1EEB ch\u00FAng t\u00F4i. Tuy nhi\u00EAn, qu\u00FD kh\u00E1ch c\u00F3 th\u1EC3 ki\u1EC3m tra th\u00F4ng tin \u0111\u00F3 b\u1EB1ng c\u00E1ch \u0111\u0103ng nh\u1EADp v\u00E0o t\u00E0i kho\u1EA3n ri\u00EAng c\u1EE7a m\u00ECnh tr\u00EAn trang web. T\u1EA1i \u0111\u00F3, qu\u00FD kh\u00E1ch c\u00F3 th\u1EC3 theo d\u00F5i \u0111\u1EA7y \u0111\u1EE7 chi ti\u1EBFt c\u1EE7a c\u00E1c \u0111\u01A1n h\u00E0ng \u0111\u00E3 ho\u00E0n t\u1EA5t, nh\u1EEFng \u0111\u01A1n h\u00E0ng m\u1EDF v\u00E0 nh\u1EEFng \u0111\u01A1n h\u00E0ng s\u1EAFp \u0111\u01B0\u1EE3c giao c\u0169ng nh\u01B0 qu\u1EA3n l\u00FD th\u00F4ng tin v\u1EC1 \u0111\u1ECBa ch\u1EC9, th\u00F4ng tin v\u1EC1 ng\u00E2n h\u00E0ng v\u00E0 nh\u1EEFng b\u1EA3n tin m\u00E0 qu\u00FD kh\u00E1ch \u0111\u00E3 \u0111\u0103ng k\u00FD nh\u1EADn. Qu\u00FD kh\u00E1ch c\u1EA7n b\u1EA3o \u0111\u1EA3m l\u00E0 th\u00F4ng tin \u0111\u01B0\u1EE3c truy c\u1EADp m\u1ED9t c\u00E1ch b\u00ED m\u1EADt v\u00E0 kh\u00F4ng l\u00E0m l\u1ED9 cho m\u1ED9t b\u00EAn th\u1EE9 ba kh\u00F4ng c\u00F3 quy\u1EC1n. Ch\u00FAng t\u00F4i s\u1EBD kh\u00F4ng ch\u1ECBu tr\u00E1ch nhi\u1EC7m \u0111\u1ED1i v\u1EDBi vi\u1EC7c s\u1EED d\u1EE5ng sai m\u1EADt kh\u1EA9u tr\u1EEB khi \u0111\u00F3 l\u00E0 l\u1ED7i c\u1EE7a ch\u00FAng t\u00F4i."),
                react["createElement"]("h2", null, "C\u1EADp nh\u1EADt th\u00F4ng tin c\u00E1 nh\u00E2n"),
                "Qu\u00FD kh\u00E1ch c\u00F3 th\u1EC3 c\u1EADp nh\u1EADt th\u00F4ng tin c\u00E1 nh\u00E2n c\u1EE7a m\u00ECnh b\u1EA5t k\u1EF3 l\u00FAc n\u00E0o b\u1EB1ng c\u00E1ch \u0111\u0103ng nh\u1EADp v\u00E0o trang web lixibox.com.",
                react["createElement"]("h2", null, "B\u1EA3o m\u1EADt th\u00F4ng tin c\u00E1 nh\u00E2n"),
                "lixibox.com \u0111\u1EA3m b\u1EA3o r\u1EB1ng m\u1ECDi th\u00F4ng tin thu th\u1EADp \u0111\u01B0\u1EE3c s\u1EBD \u0111\u01B0\u1EE3c l\u01B0u gi\u1EEF an to\u00E0n. Ch\u00FAng t\u00F4i b\u1EA3o v\u1EC7 th\u00F4ng tin c\u00E1 nh\u00E2n c\u1EE7a qu\u00FD kh\u00E1ch b\u1EB1ng c\u00E1ch:",
                react["createElement"]("p", null, "- Gi\u1EDBi h\u1EA1n truy c\u1EADp th\u00F4ng tin c\u00E1 nh\u00E2n"),
                react["createElement"]("p", null, "- S\u1EED d\u1EE5ng s\u1EA3n ph\u1EA9m c\u00F4ng ngh\u1EC7 \u0111\u1EC3 ng\u0103n ch\u1EB7n truy c\u1EADp m\u00E1y t\u00EDnh tr\u00E1i ph\u00E9p"),
                react["createElement"]("p", null, "- X\u00F3a th\u00F4ng tin c\u00E1 nh\u00E2n c\u1EE7a qu\u00FD kh\u00E1ch khi n\u00F3 kh\u00F4ng c\u00F2n c\u1EA7n thi\u1EBFt cho m\u1EE5c \u0111\u00EDch l\u01B0u tr\u1EEF h\u1ED3 s\u01A1 c\u1EE7a ch\u00FAng t\u00F4i"),
                react["createElement"]("p", null, "lixibox.com s\u1EED d\u1EE5ng c\u00F4ng ngh\u1EC7 m\u00E3 h\u00F3a theo giao th\u1EE9c 256-bit SSL (secure sockets layer) khi x\u1EED l\u00FD th\u00F4ng tin t\u00E0i ch\u00EDnh c\u1EE7a qu\u00FD kh\u00E1ch. M\u00E3 h\u00F3a 256-bit SSL ph\u1EA3i m\u1EA5t x\u1EA5p x\u1EC9 m\u1ED9t ngh\u00ECn t\u1EC9 t\u1EC9 n\u0103m m\u1EDBi c\u00F3 th\u1EC3 ph\u00E1 v\u1EE1 \u0111\u01B0\u1EE3c v\u00E0 l\u00E0 giao th\u1EE9c ti\u00EAu chu\u1EA9n c\u1EE7a m\u00E3 h\u00F3a."),
                react["createElement"]("h2", null, "Ti\u1EBFt l\u1ED9 th\u00F4ng tin c\u00E1 nh\u00E2n"),
                "Ch\u00FAng t\u00F4i s\u1EBD kh\u00F4ng chia s\u1EBB th\u00F4ng tin c\u1EE7a qu\u00FD kh\u00E1ch cho b\u1EA5t k\u1EF3 m\u1ED9t c\u00F4ng ty n\u00E0o kh\u00E1c ngo\u1EA1i tr\u1EEB nh\u1EEFng c\u00F4ng ty v\u00E0 c\u00E1c b\u00EAn th\u1EE9 ba c\u00F3 li\u00EAn quan tr\u1EF1c ti\u1EBFp \u0111\u1EBFn vi\u1EC7c giao h\u00E0ng m\u00E0 qu\u00FD kh\u00E1ch \u0111\u00E3 mua t\u1EA1i lixibox.com. Trong m\u1ED9t v\u00E0i tr\u01B0\u1EDDng h\u1EE3p \u0111\u1EB7c bi\u1EC7t, lixibox.com c\u00F3 th\u1EC3 b\u1ECB y\u00EAu c\u1EA7u ph\u1EA3i ti\u1EBFt l\u1ED9 th\u00F4ng tin c\u00E1 nh\u00E2n, v\u00ED d\u1EE5 nh\u01B0 khi c\u00F3 c\u0103n c\u1EE9 cho vi\u1EC7c ti\u1EBFt l\u1ED9 th\u00F4ng tin l\u00E0 c\u1EA7n thi\u1EBFt \u0111\u1EC3 ng\u0103n ch\u1EB7n c\u00E1c m\u1ED1i \u0111e d\u1ECDa v\u1EC1 t\u00EDnh m\u1EA1ng v\u00E0 s\u1EE9c kh\u1ECFe, hay cho m\u1EE5c \u0111\u00EDch th\u1EF1c thi ph\u00E1p lu\u1EADt. lixibox.com cam k\u1EBFt tu\u00E2n th\u1EE7 \u0110\u1EA1o lu\u1EADt B\u1EA3o M\u1EADt v\u00E0 c\u00E1c Nguy\u00EAn t\u1EAFc B\u1EA3o m\u1EADt Qu\u1ED1c gia.",
                react["createElement"]("p", null, "N\u1EBFu qu\u00FD kh\u00E1ch tin r\u1EB1ng b\u1EA3o m\u1EADt c\u1EE7a qu\u00FD kh\u00E1ch b\u1ECB lixibox.com x\u00E2m ph\u1EA1m, xin vui l\u00F2ng li\u00EAn h\u1EC7 v\u1EDBi ch\u00FAng t\u00F4i t\u1EA1i \u0111\u1ECBa ch\u1EC9 info@lixibox.com \u0111\u1EC3 \u0111\u01B0\u1EE3c gi\u1EA3i quy\u1EBFt v\u1EA5n \u0111\u1EC1."),
                react["createElement"]("h2", null, "Thu th\u1EADp d\u1EEF li\u1EC7u m\u00E1y t\u00EDnh"),
                "Khi qu\u00FD kh\u00E1ch \u0111\u1EBFn th\u0103m lixibox.com, m\u00E1y ch\u1EE7 c\u1EE7a c\u00F4ng ty ch\u00FAng t\u00F4i s\u1EBD t\u1EF1 \u0111\u1ED9ng l\u01B0u tr\u1EEF th\u00F4ng tin m\u00E0 tr\u00ECnh duy\u1EC7t c\u1EE7a qu\u00FD kh\u00E1ch g\u1EEDi \u0111\u1EBFn. Nh\u1EEFng th\u00F4ng tin n\u00E0y bao g\u1ED3m:",
                react["createElement"]("p", null, "- \u0110\u1ECBa ch\u1EC9 IP c\u1EE7a qu\u00FD kh\u00E1ch"),
                react["createElement"]("p", null, "- Lo\u1EA1i tr\u00ECnh duy\u1EC7t"),
                react["createElement"]("p", null, "- Nh\u1EEFng trang m\u1EE5c trong lixibox.com m\u00E0 qu\u00FD kh\u00E1ch gh\u00E9 th\u0103m"),
                react["createElement"]("p", null, "- Kho\u1EA3ng th\u1EDDi gian qu\u00FD kh\u00E1ch gi\u00E0nh ra \u0111\u00E3 xem nh\u1EEFng trang m\u1EE5c \u0111\u00F3, s\u1EA3n ph\u1EA9m, t\u00ECm ki\u1EBFm th\u00F4ng tin tr\u00EAn trang web, th\u1EDDi gian v\u00E0 ng\u00E0y th\u00E1ng truy c\u1EADp, v\u00E0 c\u00E1c s\u1ED1 li\u1EC7u th\u1ED1ng k\u00EA kh\u00E1c."),
                react["createElement"]("p", null, "C\u00E1c th\u00F4ng tin \u0111\u00F3 \u0111\u01B0\u1EE3c thu th\u1EADp cho m\u1EE5c \u0111\u00EDch ph\u00E2n t\u00EDch v\u00E0 \u0111\u00E1nh gi\u00E1 \u0111\u1EC3 g\u00F3p ph\u1EA7n c\u1EA3i thi\u1EC7n trang web, d\u1ECBch v\u1EE5, v\u00E0 s\u1EA3n ph\u1EA9m m\u00E0 ch\u00FAng t\u00F4i cung c\u1EA5p. Nh\u1EEFng d\u1EEF li\u1EC7u \u0111\u00F3 s\u1EBD kh\u00F4ng c\u00F3 li\u00EAn quan g\u00EC \u0111\u1EBFn c\u00E1c th\u00F4ng tin c\u00E1 nh\u00E2n kh\u00E1c."),
                react["createElement"]("p", null, "Nh\u01B0 \u0111\u00E3 \u0111\u1EC1 c\u1EADp \u1EDF ph\u1EA7n tr\u00EAn, lixibox s\u1EBD s\u1EED d\u1EE5ng t\u00EDnh n\u0103ng c\u1EE7a Google Analytics d\u1EF1a tr\u00EAn nh\u1EEFng d\u1EEF li\u1EC7u v\u1EC1 qu\u1EA3ng c\u00E1o b\u1EB1ng h\u00ECnh \u1EA3nh (Display advertising) bao g\u1ED3m nh\u1EEFng ph\u1EA7n sau: b\u00E1o c\u00E1o v\u1EC1 Re-marketing, b\u00E1o c\u00E1o v\u1EC1 s\u1ED1 l\u01B0\u1EE3t hi\u1EC7n qu\u1EA3ng c\u00E1o h\u00ECnh \u1EA3nh tr\u00EAn m\u1EA1ng l\u01B0\u1EDBi trang web c\u1EE7a Google, b\u00E1o c\u00E1o v\u1EC1 Double Click, th\u1ED1ng k\u00EA h\u00E0nh vi & s\u1EDF th\u00EDch kh\u00E1ch h\u00E0ng tr\u00EAn Google Analytics. H\u00E3y ch\u1ECDn m\u1EE5c thi\u1EBFt l\u1EADp qu\u1EA3ng c\u00E1o c\u1EE7a Google (https:// www.google.com/settings/ads ), \u0111\u1EC3 b\u1EA1n c\u00F3 th\u1EC3 t\u1EAFt ph\u1EA7n hi\u1EC3n th\u1ECB qu\u1EA3ng c\u00E1o t\u1EEB Google Analytics v\u00E0 t\u00F9y ch\u1EC9nh qu\u1EA3ng c\u00E1o tr\u00EAn h\u1EC7 th\u1ED1ng Google Display Network."),
                react["createElement"]("p", null, "Ngo\u00E0i ra, lixibox c\u00F2n s\u1EED d\u1EE5ng ch\u1EE9c n\u0103ng Re-marketing c\u1EE7a Google Analytic \u0111\u1EC3 qu\u1EA3ng c\u00E1o tr\u1EF1c tuy\u1EBFn; c\u00E1c b\u00EAn th\u1EE9 ba (bao g\u1ED3m Google) s\u1EBD c\u00F3 th\u1EC3 hi\u1EC3n th\u1ECB qu\u1EA3ng c\u00E1o c\u1EE7a lixibox tr\u00EAn c\u00E1c website li\u00EAn k\u1EBFt. lixibox v\u00E0 b\u00EAn cung c\u1EA5p th\u1EE9 3, bao g\u1ED3m c\u1EA3 Google, s\u1EBD s\u1EED d\u1EE5ng cookies c\u1EE7a b\u00EAn th\u1EE9 nh\u1EA5t (nh\u01B0 Google Analytics cookies v\u00E0 cookie) c\u1EE7a b\u00EAn th\u1EE9 3 (nh\u01B0 DoubleClick) \u0111\u1EC3 th\u00F4ng b\u00E1o, t\u1ED1i \u01B0u h\u00F3a v\u00E0 tr\u00ECnh chi\u1EBFu c\u00E1c m\u1EABu qu\u1EA3ng c\u00E1o d\u1EF1a tr\u00EAn nh\u1EEFng l\u1EA7n kh\u00E1ch h\u00E0ng truy c\u1EADp website tr\u01B0\u1EDBc \u0111\u00F3, \u0111\u1ED3ng th\u1EDDi cho bi\u1EBFt k\u1EBFt qu\u1EA3 ph\u1EA3n \u1EE9ng c\u1EE7a kh\u00E1ch h\u00E0ng \u0111\u1ED1i v\u1EDBi m\u1EABu qu\u1EA3ng c\u00E1o, nh\u1EEFng c\u00E1ch s\u1EED d\u1EE5ng kh\u00E1c c\u1EE7a qu\u1EA3ng c\u00E1o v\u00E0 \u0111\u1ED9 t\u01B0\u01A1ng t\u00E1c c\u1EE7a nh\u1EEFng m\u1EABu qu\u1EA3ng c\u00E1o n\u00E0y v\u00E0 d\u1ECBch v\u1EE5 qu\u1EA3ng c\u00E1o \u0111\u1EBFn s\u1ED1 l\u01B0\u1EE3ng truy c\u1EADp v\u00E0o trang lixibox."),
                react["createElement"]("h2", null, "Thay \u0111\u1ED5i c\u1EE7a Ch\u00EDnh s\u00E1ch B\u1EA3o m\u1EADt"),
                react["createElement"]("p", null, "lixibox.com c\u00F3 quy\u1EC1n thay \u0111\u1ED5i v\u00E0 ch\u1EC9nh s\u1EEDa Quy \u0111\u1ECBnh B\u1EA3o m\u1EADt v\u00E0o b\u1EA5t k\u1EF3 l\u00FAc n\u00E0o. B\u1EA5t c\u1EE9 thay \u0111\u1ED5i n\u00E0o v\u1EC1 ch\u00EDnh s\u00E1ch n\u00E0y \u0111\u1EC1u \u0111\u01B0\u1EE3c \u0111\u0103ng tr\u00EAn trang web c\u1EE7a ch\u00FAng t\u00F4i."),
                "N\u1EBFu qu\u00FD kh\u00E1ch kh\u00F4ng h\u00E0i l\u00F2ng v\u1EDBi vi\u1EC7c ch\u00FAng t\u00F4i x\u1EED l\u00FD th\u1EAFc m\u1EAFc hay khi\u1EBFu n\u1EA1i c\u1EE7a qu\u00FD kh\u00E1ch, xin vui l\u00F2ng li\u00EAn h\u1EC7 v\u1EDBi ch\u00FAng t\u00F4i t\u1EA1i info@lixibox.com."),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_privacy = (renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1wcml2YWN5LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1wcml2YWN5LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUMvQixPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sUUFBUSxDQUFDO0FBRS9CLE9BQU8sVUFBVSxNQUFNLHNCQUFzQixDQUFDO0FBRTlDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFFdkMsSUFBTSxhQUFhLEdBQUc7SUFFcEIsTUFBTSxDQUFDLENBQ0w7UUFDRSxvQkFBQyxVQUFVO1lBQ1QsNkJBQUssU0FBUyxFQUFFLGNBQWM7Z0JBQzVCLGlGQUFtQzs7Z0JBRWpDLGdNQUFnRjtnQkFDbEYsME1BQW9HO2dCQUNwRyw0REFBa0I7Z0JBQ2xCLHFFQUFzQjtnQkFDdEIsZ0VBQXNCO2dCQUN0QixpSUFBb0Q7Z0JBQ3BELCtNQUFxRjtnQkFDckYsNEdBQW1EO2dCQUNuRCxzSUFBeUQ7Z0JBQ3pELGdIQUFrRDtnQkFDbEQsdVBBQW1IO2dCQUNuSCw2MUNBQStwQjtnQkFDL3BCLDhVQUE2SjtnQkFDN0osOG1CQUE0UztnQkFDNVMsb3BCQUF3VTtnQkFDeFUsc2hEQUE0c0I7Z0JBQzVzQixzRkFBbUM7O2dCQUVqQyxxRkFBa0M7O2dCQUVsQyxxR0FBNEM7Z0JBQzlDLGtMQUE0RTtnQkFDNUUseVBBQWdIO2dCQUNoSCw0ZUFBK1A7Z0JBQy9QLHFGQUFrQzs7Z0JBRWhDLGtXQUF1SztnQkFDeksscUZBQWtDOztnQkFFaEMsMEZBQWlDO2dCQUNuQyxtRUFBeUI7Z0JBQ3pCLDhIQUFnRTtnQkFDaEUsZ1dBQW9MO2dCQUNwTCwrZUFBK047Z0JBQy9OLGloQ0FBdWtCO2dCQUN2a0IscXpDQUF3ckI7Z0JBQ3hyQixxR0FBd0M7Z0JBQ3hDLHVYQUF3Szt5U0FFbEs7WUFDUixvQkFBQyxLQUFLLElBQUMsS0FBSyxFQUFFLFlBQVksQ0FBQyxnQkFBZ0IsR0FBSSxDQUNwQyxDQUNTLENBQ3pCLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLGFBQWEsQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/info/about/view-give-gift-card.tsx




var view_give_gift_card_renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("h2", null, "N\u1ED9i dung"),
                react["createElement"]("p", null, "V\u1EDBi \u0111\u01A1n h\u00E0ng t\u1EEB 400.000\u0110 tr\u1EDF l\u00EAn, qu\u00FD kh\u00E1ch nh\u1EADp m\u00E3 Lixi100 \u0111\u1EC3 \u0111\u01B0\u1EE3c t\u1EB7ng 01 Giftcard tr\u1ECB gi\u00E1 100.000\u0110 v\u00E0o gi\u1ECF h\u00E0ng."),
                react["createElement"]("p", null, "V\u1EDBi \u0111\u01A1n h\u00E0ng t\u1EEB 700.000\u0110 tr\u1EDF l\u00EAn, qu\u00FD kh\u00E1ch nh\u1EADp m\u00E3 Lixi200 \u0111\u1EC3 \u0111\u01B0\u1EE3c t\u1EB7ng 01 Giftcard tr\u1ECB gi\u00E1 200.000\u0110 v\u00E0o gi\u1ECF h\u00E0ng."),
                react["createElement"]("p", null, "V\u1EDBi \u0111\u01A1n h\u00E0ng t\u1EEB 1.000.000\u0110 tr\u1EDF l\u00EAn, qu\u00FD kh\u00E1ch nh\u1EADp m\u00E3 Lixi400 \u0111\u1EC3 \u0111\u01B0\u1EE3c t\u1EB7ng 01 Giftcard tr\u1ECB gi\u00E1 400.000\u0110 v\u00E0o gi\u1ECF h\u00E0ng."),
                react["createElement"]("h2", null, "Th\u1EDDi gian t\u1EB7ng gift card"),
                react["createElement"]("p", null, "T\u1EEB ng\u00E0y 25/12/2017 \u0111\u1EBFn ng\u00E0y 31/01/2018. S\u1ED1 l\u01B0\u1EE3ng giftcard c\u00F3 h\u1EA1n. Ch\u01B0\u01A1ng tr\u00ECnh c\u00F3 th\u1EC3 k\u1EBFt th\u00FAc tr\u01B0\u1EDBc th\u1EDDi h\u1EA1n m\u00E0 kh\u00F4ng c\u1EA7n b\u00E1o tr\u01B0\u1EDBc."),
                react["createElement"]("h2", null, "C\u00E1ch s\u1EED d\u1EE5ng giftcard"),
                react["createElement"]("p", null, "Th\u1EDDi gian s\u1EED d\u1EE5ng giftcard: t\u1EEB ng\u00E0y 10/02/2018 cho \u0111\u1EBFn ng\u00E0y 20/02/2018."),
                react["createElement"]("p", null, "Qu\u00FD kh\u00E1ch nh\u1EADp m\u00E3 \u0111\u01B0\u1EE3c in tr\u00EAn m\u1ED7i giftcard v\u00E0o b\u01B0\u1EDBc thanh to\u00E1n \u0111\u1EC3 \u0111\u01B0\u1EE3c gi\u1EA3m ti\u1EC1n cho \u0111\u01A1n h\u00E0ng k\u1EBF ti\u1EBFp."),
                react["createElement"]("p", null, "M\u1ED7i m\u1ED9t giftcard ch\u1EC9 \u0111\u01B0\u1EE3c s\u1EED d\u1EE5ng 1 l\u1EA7n duy nh\u1EA5t."),
                react["createElement"]("p", null, "Giftcard kh\u00F4ng c\u00F3 gi\u00E1 tr\u1ECB quy \u0111\u1ED5i th\u00E0nh ti\u1EC1n m\u1EB7t v\u00E0 kh\u00F4ng ho\u00E0n tr\u1EA3 ti\u1EC1n d\u01B0 khi mua h\u00E0ng ho\u1EB7c \u0111\u1EC3 l\u1EA1i ti\u1EC1n d\u01B0 cho \u0111\u01A1n h\u00E0ng k\u1EBF ti\u1EBFp."),
                react["createElement"]("p", null, "M\u1ECDi v\u1EA5n \u0111\u1EC1 li\u00EAn quan, m\u1EDDi Qu\u00FD kh\u00E1ch chat v\u1EDBi T\u01B0 v\u1EA5n vi\u00EAn / ho\u1EB7c hotline 1800 2040")),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_give_gift_card = (view_give_gift_card_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1naXZlLWdpZnQtY2FyZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXctZ2l2ZS1naWZ0LWNhcmQudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFL0IsT0FBTyxVQUFVLE1BQU0sc0JBQXNCLENBQUM7QUFFOUMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUV2QyxJQUFNLGFBQWEsR0FBRztJQUVwQixNQUFNLENBQUMsQ0FDTDtRQUNFLG9CQUFDLFVBQVU7WUFDVCw2QkFBSyxTQUFTLEVBQUUsY0FBYztnQkFDNUIsZ0RBQWlCO2dCQUNqQiw2UUFBMEg7Z0JBQzFILDZRQUEwSDtnQkFDMUgsK1FBQTRIO2dCQUM1SCxxRUFBaUM7Z0JBQ2pDLDhTQUE0STtnQkFDNUksdUVBQThCO2dCQUM5QixpSkFBOEU7Z0JBQzlFLHNRQUE4RztnQkFDOUcscUlBQXdEO2dCQUN4RCwwU0FBd0k7Z0JBQ3hJLG9MQUF3RixDQUNwRjtZQUNOLG9CQUFDLEtBQUssSUFBQyxLQUFLLEVBQUUsWUFBWSxDQUFDLGdCQUFnQixHQUFJLENBQ3BDLENBQ1MsQ0FDekIsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsYUFBYSxDQUFDIn0=
// EXTERNAL MODULE: ./utils/uri.ts
var uri = __webpack_require__(348);

// CONCATENATED MODULE: ./container/app-shop/info/about/view-buy-on-app.tsx





var view_buy_on_app_renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("h2", null, "Tr\u1EA3i nghi\u1EC7m mua s\u1EAFm th\u00F4ng minh v\u1EDBi \u1EE8ng d\u1EE5ng LIXIBOX!"),
                react["createElement"]("div", { className: 'sub-title' }, "B\u01B0\u1EDBc 1: T\u1EA3i \u1EE9ng d\u1EE5ng LIXIBOX"),
                react["createElement"]("p", null, "\u1EE8ng d\u1EE5ng Lixibox v\u1EDBi giao di\u1EC7n th\u00E2n thi\u1EC7n v\u00E0 c\u00E1c b\u01B0\u1EDBc mua s\u1EAFm \u0111\u01A1n gi\u1EA3n nh\u1EA5t \u0111\u1ECBnh s\u1EBD l\u00E0m h\u00E0i l\u00F2ng b\u1EA1n t\u1EEB l\u1EA7n \u201Cy\u00EAu\" \u0111\u1EA7u ti\u00EAn. Quan tr\u1ECDng h\u01A1n \u0111\u00E2y s\u1EBD l\u00E0 ng\u01B0\u1EDDi b\u1EA1n th\u00E2n th\u01B0\u1EDDng xuy\u00EAn nh\u1EAFc b\u1EA1n v\u1EC1 c\u00E1c ch\u01B0\u01A1ng tr\u00ECnh \u01B0u \u0111\u00E3i m\u1EDBi nh\u1EA5t c\u1EE7a Lixibox n\u1EEFa \u0111\u1EA5y."),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'img-border imgMaxWidth300', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-app-001-download.jpg', alt: 'Download app Lixibox' })),
                react["createElement"]("div", { className: 'sub-title' }, "B\u01B0\u1EDBc 2: \u0110\u0103ng k\u00FD t\u00E0i kho\u1EA3n/ \u0110\u0103ng nh\u1EADp"),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'img-border imgMaxWidth300', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-app-002-signup.png', alt: 'Đăng ký tài khoản Lixibox' })),
                react["createElement"]("p", null, "\u0110\u1EC3 b\u1EAFt \u0111\u1EA7u mua h\u00E0ng tr\u00EAn Lixibox v\u00E0 \u0111\u01B0\u1EE3c h\u01B0\u1EDFng nh\u1EEFng \u01B0u \u0111\u00E3i h\u1EA5p d\u1EABn nh\u1EA5t t\u1EEB ch\u00FAng t\u00F4i, h\u00E3y d\u00E0nh ra 1 ph\u00FAt \u0111\u1EC3 t\u1EA1o t\u00E0i kho\u1EA3n b\u1EB1ng Email c\u00E1 nh\u00E2n ho\u1EB7c T\u00E0i kho\u1EA3n Facebook. "),
                react["createElement"]("p", null, "L\u01B0u \u00FD: Khi quay l\u1EA1i Lixibox t\u1EEB l\u1EA7n th\u1EE9 2, b\u1EA1n ch\u1EC9 c\u1EA7n \u0111\u0103ng nh\u1EADp b\u1EB1ng t\u00E0i kho\u1EA3n \u0111\u00E3 \u0111\u0103ng k\u00FD m\u00E0 kh\u00F4ng c\u1EA7n t\u1EA1o m\u1ED9t T\u00E0i kho\u1EA3n m\u1EDBi. T\u00E0i kho\u1EA3n \u0111\u00E3 t\u1EA1o tr\u00EAn Website v\u00E0 \u1EE8ng d\u1EE5ng kh\u00F4ng thay \u0111\u1ED5i."),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'img-border imgMaxWidth300', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-app-003-signin.png', alt: 'Đăng nhập tài khoản Lixibox' })),
                react["createElement"]("div", { className: 'sub-title' }, "B\u01B0\u1EDBc 3: T\u00ECm ki\u1EBFm s\u1EA3n ph\u1EA9m m\u00E0 b\u1EA1n quan t\u00E2m "),
                react["createElement"]("p", null, "\u0110\u1EC3 t\u00ECm ki\u1EBFm nh\u1EEFng s\u1EA3n ph\u1EA9m ph\u00F9 h\u1EE3p v\u1EDBi b\u1EA1n trong h\u01A1n 100.000 s\u1EA3n ph\u1EA9m c\u1EE7a Lixibox, b\u1EA1n c\u00F3 th\u1EC3 tham kh\u1EA3o hai b\u01B0\u1EDBc sau:"),
                react["createElement"]("p", null, "C\u00E1ch 1: Nh\u1EADp t\u00EAn s\u1EA3n ph\u1EA9m v\u00E0 t\u00EAn th\u01B0\u01A1ng hi\u1EC7u v\u00E0o ph\u1EA7n \u201CT\u00ECm ki\u1EBFm\""),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'img-border imgMaxWidth300', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-app-004-search.jpg', alt: 'Tìm kiếm sản phẩm Lixibox' })),
                react["createElement"]("p", null, "C\u00E1ch 2: Tham kh\u1EA3o c\u00E1c s\u1EA3n ph\u1EA9m v\u00E0 th\u01B0\u01A1ng hi\u1EC7u tr\u00EAn thanh m\u1EE5c l\u1EE5c c\u1EE7a trang ch\u1EE7"),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'img-border imgMaxWidth300', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-app-005-menu.png', alt: 'Tìm kiếm danh mục sản phẩm Lixibox' })),
                react["createElement"]("div", { className: 'sub-title' }, "B\u01B0\u1EDBc 4: T\u00ECm hi\u1EC3u th\u00F4ng tin s\u1EA3n ph\u1EA9m "),
                react["createElement"]("p", null, "Sau khi ch\u1ECDn \u0111\u01B0\u1EE3c s\u1EA3n ph\u1EA9m \u01B0ng \u00FD, h\u00E3y tham kh\u1EA3o c\u00E1c th\u00F4ng tin chi ti\u1EBFt \u0111\u01B0\u1EE3c Lixibox b\u1EADt m\u00ED trong ph\u1EA7n S\u1EA2N PH\u1EA8M v\u00E0 nh\u1EEFng \u0110\u00C1NH GI\u00C1 t\u1EEB tr\u1EA3i nghi\u1EC7m th\u1EF1c t\u1EBF c\u1EE7a c\u00E1c kh\u00E1ch h\u00E0ng \u0111\u00E3 s\u1EED d\u1EE5ng s\u1EA3n ph\u1EA9m n\u00E0y! "),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'img-border imgMaxWidth300', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-app-006-product-detail.jpg', alt: 'Chi tiết sản phẩm' })),
                react["createElement"]("p", null, "Ch\u1ECDn TH\u00CAM V\u00C0O GI\u1ECE \u0111\u1EC3 n\u1EBFu b\u1EA1n \u0111\u00E3 quy\u1EBFt \u0111\u1ECBnh rinh em \u1EA5y v\u1EC1 nh\u00E0. V\u1EADy l\u00E0 b\u1EA1n \u0111\u00E3 c\u00F3 th\u1EC3 y\u00EAn t\u00E2m ti\u1EBFp t\u1EE5c l\u1EF1a ch\u1ECDn c\u00E1c s\u1EA3n ph\u1EA9m kh\u00E1c r\u1ED3i!"),
                react["createElement"]("div", { className: 'sub-title' }, "B\u01B0\u1EDBc 5: Ti\u1EBFn h\u00E0nh \u0111\u1EB7t h\u00E0ng"),
                react["createElement"]("p", null, "Ki\u1EC3m tra c\u00E1c s\u1EA3n ph\u1EA9m \u0111\u00E3 c\u00F3 trong gi\u1ECF h\u00E0ng c\u1EE7a b\u1EA1n v\u00E0 ti\u1EBFn h\u00E0nh \u0111\u1EB7t h\u00E0ng. \u0110\u1EEBng qu\u00EAn \u0111i\u1EC1n c\u00E1c M\u00C3 GI\u1EA2M GI\u00C1 n\u1EBFu c\u00F3 nh\u00E9!"),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'img-border imgMaxWidth300', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-app-007-checkout-cart.jpg', alt: 'Checkout đơn hàng' })),
                react["createElement"]("div", { className: 'sub-title' }, "B\u01B0\u1EDBc 6: Mua s\u1EA3n ph\u1EA9m v\u1EDBi gi\u00E1 \u01B0u \u0111\u00E3i?"),
                react["createElement"]("p", null, "Lixibox lu\u00F4n c\u00F3 nh\u1EEFng s\u1EA3n ph\u1EA9m ADD ON (mua k\u00E8m) v\u1EDBi gi\u00E1 c\u1EF1c k\u1EF3 \u01B0u \u0111\u00E3i cho b\u1EA1n. B\u1EA1n ch\u1EC9 vi\u1EC7c ch\u1ECDn s\u1EA3n ph\u1EA9m \u01B0u \u0111\u00E3i v\u00E0 ho\u1EB7c ti\u1EBFn h\u00E0nh \u0111\u1EB7t h\u00E0ng ngay"),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'img-border imgMaxWidth300', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-app-008-checkout-addon.jpg', alt: 'Mua thêm sản phẩm' })),
                react["createElement"]("div", { className: 'sub-title' }, "B\u01B0\u1EDBc 7: \u0110i\u1EC1n th\u00F4ng tin chi ti\u1EBFt ng\u01B0\u1EDDi nh\u1EADn. Ch\u1ECDn h\u00ECnh th\u1EE9c thanh to\u00E1n"),
                react["createElement"]("p", null, "Nh\u1EADp \u0111\u1ECBa ch\u1EC9 giao h\u00E0ng m\u1ED9t c\u00E1ch ch\u00EDnh x\u00E1c \u0111\u1EC3 ch\u00FAng t\u00F4i c\u00F3 th\u1EC3 g\u1EEDi s\u1EA3n ph\u1EA9m \u0111\u1EBFn v\u1EDBi b\u1EA1n trong th\u1EDDi gian s\u1EDBm nh\u1EA5t! \u0110\u1EEBng qu\u00EAn ki\u1EC3m tra s\u1ED1 l\u01B0\u1EE3ng s\u1EA3n ph\u1EA9m, h\u1ECD t\u00EAn/ s\u1ED1 \u0111i\u1EC7n tho\u1EA1i ng\u01B0\u1EDDi nh\u1EADn m\u1ED9t m\u1ED9t l\u1EA7n tr\u01B0\u1EDBc khi sang b\u01B0\u1EDBc ti\u1EBFp theo"),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'img-border imgMaxWidth300', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-app-009-address-method.jpg', alt: 'Chọn địa chỉ giao hàng' })),
                react["createElement"]("p", null, "Sau khi \u0111i\u1EC1n \u0111\u1EE7 th\u00F4ng tin nh\u1EADn h\u00E0ng, Lixibox hi\u1EC7n c\u00F3 3 h\u00ECnh th\u1EE9c thanh to\u00E1n cho b\u1EA1n l\u1EF1a ch\u1ECDn l\u00E0 COD - Thanh to\u00E1n sau khi nh\u1EADn h\u00E0ng/ Thanh to\u00E1n online qua Th\u1EBB ng\u00E2n h\u00E0ng v\u00E0 cu\u1ED1i c\u00F9ng l\u00E0 Chuy\u1EC3n kho\u1EA3n \u0111\u1EBFn T\u00E0i kho\u1EA3n c\u1EE7a Lixibox. "),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'img-border imgMaxWidth300', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-app-010-payment-method.jpg', alt: 'Chọn phương thức giao hàng' })),
                react["createElement"]("div", { className: 'sub-title' }, "B\u01B0\u1EDBc 8: B\u1EA1n \u0111\u00E3 \u0111\u1EB7t h\u00E0ng th\u00E0nh c\u00F4ng!"),
                react["createElement"]("p", null, "Sau khi x\u00E1c nh\u1EADn \u0111\u1EB7t h\u00E0ng, b\u1EA1n s\u1EBD nh\u1EADn \u0111\u01B0\u1EE3c m\u00E3 \u0111\u01A1n h\u00E0ng v\u00E0 tin nh\u1EAFn/ email t\u1EEB Lixibox. H\u00E3y l\u01B0u l\u1EA1i m\u00E3 \u0111\u01A1n h\u00E0ng \u0111\u1EC3 b\u1ED9 ph\u1EADn CSKH c\u1EE7a ch\u00FAng t\u00F4i c\u00F3 th\u1EC3 gi\u1EA3i quy\u1EBFt c\u00E1c th\u1EAFc m\u1EAFc cho b\u1EA1n trong tr\u01B0\u1EDDng h\u1EE3p c\u1EA7n thi\u1EBFt nh\u00E9!"),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'img-border imgMaxWidth300', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-app-011-checkout-success.jpg', alt: 'Checkout thành công' })),
                react["createElement"]("p", null, "R\u1EA5t c\u1EA3m \u01A1n v\u00E0 hy v\u1ECDng b\u1EA1n \u0111\u00E3 tr\u1EA3i nghi\u1EC7m mua s\u1EAFm tuy\u1EC7t v\u1EDDi t\u1EA1i Lixibox.com!")),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_buy_on_app = (view_buy_on_app_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1idXktb24tYXBwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1idXktb24tYXBwLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUMvQixPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sUUFBUSxDQUFDO0FBRS9CLE9BQU8sVUFBVSxNQUFNLHNCQUFzQixDQUFDO0FBRTlDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzFELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFFdkMsSUFBTSxhQUFhLEdBQUc7SUFFcEIsTUFBTSxDQUFDLENBQ0w7UUFDRSxvQkFBQyxVQUFVO1lBQ1QsNkJBQUssU0FBUyxFQUFFLGNBQWM7Z0JBQzVCLDBIQUE2RDtnQkFDN0QsNkJBQUssU0FBUyxFQUFFLFdBQVcsNERBQW9DO2dCQUMvRCx1akJBQXlQO2dCQUN6UDtvQkFBRyw2QkFBSyxTQUFTLEVBQUUsMkJBQTJCLEVBQUUsR0FBRyxFQUFFLGlCQUFpQixHQUFHLGlEQUFpRCxFQUFFLEdBQUcsRUFBRSxzQkFBc0IsR0FBSSxDQUFJO2dCQUMvSiw2QkFBSyxTQUFTLEVBQUUsV0FBVyw2RkFBNEM7Z0JBQ3ZFO29CQUFHLDZCQUFLLFNBQVMsRUFBRSwyQkFBMkIsRUFBRSxHQUFHLEVBQUUsaUJBQWlCLEdBQUcsK0NBQStDLEVBQUUsR0FBRyxFQUFFLDJCQUEyQixHQUFJLENBQUk7Z0JBQ2xLLGtZQUE4SztnQkFDOUssaWFBQThMO2dCQUM5TDtvQkFBRyw2QkFBSyxTQUFTLEVBQUUsMkJBQTJCLEVBQUUsR0FBRyxFQUFFLGlCQUFpQixHQUFHLCtDQUErQyxFQUFFLEdBQUcsRUFBRSw2QkFBNkIsR0FBSSxDQUFJO2dCQUNwSyw2QkFBSyxTQUFTLEVBQUUsV0FBVyw4RkFBa0Q7Z0JBQzdFLGdRQUE0SDtnQkFDNUgsOEtBQXVFO2dCQUN2RTtvQkFBRyw2QkFBSyxTQUFTLEVBQUUsMkJBQTJCLEVBQUUsR0FBRyxFQUFFLGlCQUFpQixHQUFHLCtDQUErQyxFQUFFLEdBQUcsRUFBRSwyQkFBMkIsR0FBSSxDQUFJO2dCQUNsSyxzTEFBcUY7Z0JBQ3JGO29CQUFHLDZCQUFLLFNBQVMsRUFBRSwyQkFBMkIsRUFBRSxHQUFHLEVBQUUsaUJBQWlCLEdBQUcsNkNBQTZDLEVBQUUsR0FBRyxFQUFFLG9DQUFvQyxHQUFJLENBQUk7Z0JBQ3pLLDZCQUFLLFNBQVMsRUFBRSxXQUFXLDhFQUE0QztnQkFDdkUsd2JBQTJNO2dCQUMzTTtvQkFBRyw2QkFBSyxTQUFTLEVBQUUsMkJBQTJCLEVBQUUsR0FBRyxFQUFFLGlCQUFpQixHQUFHLHVEQUF1RCxFQUFFLEdBQUcsRUFBRSxtQkFBbUIsR0FBSSxDQUFJO2dCQUNsSywrVUFBMEk7Z0JBQzFJLDZCQUFLLFNBQVMsRUFBRSxXQUFXLG9FQUFrQztnQkFDN0QsdVNBQTJIO2dCQUMzSDtvQkFBRyw2QkFBSyxTQUFTLEVBQUUsMkJBQTJCLEVBQUUsR0FBRyxFQUFFLGlCQUFpQixHQUFHLHNEQUFzRCxFQUFFLEdBQUcsRUFBRSxtQkFBbUIsR0FBSSxDQUFJO2dCQUNqSyw2QkFBSyxTQUFTLEVBQUUsV0FBVyx3RkFBNEM7Z0JBQ3ZFLHdVQUF1SjtnQkFDdko7b0JBQUcsNkJBQUssU0FBUyxFQUFFLDJCQUEyQixFQUFFLEdBQUcsRUFBRSxpQkFBaUIsR0FBRyx1REFBdUQsRUFBRSxHQUFHLEVBQUUsbUJBQW1CLEdBQUksQ0FBSTtnQkFDbEssNkJBQUssU0FBUyxFQUFFLFdBQVcsNklBQTZFO2dCQUN4RyxrZ0JBQXdPO2dCQUN4TztvQkFBRyw2QkFBSyxTQUFTLEVBQUUsMkJBQTJCLEVBQUUsR0FBRyxFQUFFLGlCQUFpQixHQUFHLHVEQUF1RCxFQUFFLEdBQUcsRUFBRSx3QkFBd0IsR0FBSSxDQUFJO2dCQUN2Syw0YUFBdU87Z0JBQ3ZPO29CQUFHLDZCQUFLLFNBQVMsRUFBRSwyQkFBMkIsRUFBRSxHQUFHLEVBQUUsaUJBQWlCLEdBQUcsdURBQXVELEVBQUUsR0FBRyxFQUFFLDRCQUE0QixHQUFJLENBQUk7Z0JBQzNLLDZCQUFLLFNBQVMsRUFBRSxXQUFXLDRGQUEyQztnQkFDdEUsMmRBQTBOO2dCQUMxTjtvQkFBRyw2QkFBSyxTQUFTLEVBQUUsMkJBQTJCLEVBQUUsR0FBRyxFQUFFLGlCQUFpQixHQUFHLHlEQUF5RCxFQUFFLEdBQUcsRUFBRSxxQkFBcUIsR0FBSSxDQUFJO2dCQUN0SyxtTEFBa0YsQ0FDOUU7WUFDTixvQkFBQyxLQUFLLElBQUMsS0FBSyxFQUFFLFlBQVksQ0FBQyxnQkFBZ0IsR0FBSSxDQUNwQyxDQUNTLENBQ3pCLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLGFBQWEsQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/info/about/view-buy-on-web.tsx





var view_buy_on_web_renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("h2", null, "Mua h\u00E0ng t\u1EA1i Lixibox ch\u1EC9 trong 3 n\u1ED1t nh\u1EA1c?"),
                react["createElement"]("div", { className: 'sub-title' }, "B\u01B0\u1EDBc 1: \u0110\u0103ng k\u00FD t\u00E0i kho\u1EA3n"),
                react["createElement"]("p", null, "\u0110\u1EC3 b\u1EAFt \u0111\u1EA7u mua h\u00E0ng tr\u00EAn Lixibox v\u00E0 \u0111\u01B0\u1EE3c h\u01B0\u1EDFng nh\u1EEFng \u01B0u \u0111\u00E3i h\u1EA5p d\u1EABn nh\u1EA5t t\u1EEB ch\u00FAng t\u00F4i, h\u00E3y d\u00E0nh 1\u2019 \u0111\u1EC3 t\u1EA1o t\u00E0i kho\u1EA3n b\u1EB1ng Email c\u00E1 nh\u00E2n ho\u1EB7c T\u00E0i kho\u1EA3n Facebook."),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'imgMaxWidth600', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-web-001-signup.png', alt: 'Đăng ký Lixibox' })),
                react["createElement"]("p", null, "L\u01B0u \u00FD: Khi quay l\u1EA1i Lixibox t\u1EEB l\u1EA7n th\u1EE9 2, b\u1EA1n ch\u1EC9 c\u1EA7n \u0111\u0103ng nh\u1EADp b\u1EB1ng t\u00E0i kho\u1EA3n \u0111\u00E3 \u0111\u0103ng k\u00FD m\u00E0 kh\u00F4ng c\u1EA7n t\u1EA1o m\u1ED9t T\u00E0i kho\u1EA3n m\u1EDBi."),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'imgMaxWidth600', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-web-003-login.png', alt: 'Đăng ký Lixibox' })),
                react["createElement"]("div", { className: 'sub-title' }, "B\u01B0\u1EDBc 2: T\u00ECm ki\u1EBFm s\u1EA3n ph\u1EA9m m\u00E0 b\u1EA1n mu\u1ED1n "),
                react["createElement"]("p", null, "\u0110\u1EC3 t\u00ECm ki\u1EBFm nh\u1EEFng s\u1EA3n ph\u1EA9m ph\u00F9 h\u1EE3p v\u1EDBi b\u1EA1n trong h\u01A1n 100.000 s\u1EA3n ph\u1EA9m c\u1EE7a Lixibox, b\u1EA1n c\u00F3 th\u1EC3 tham kh\u1EA3o hai b\u01B0\u1EDBc sau:"),
                react["createElement"]("p", null, "C\u00E1ch 1: Nh\u1EADp t\u00EAn s\u1EA3n ph\u1EA9m v\u00E0 t\u00EAn th\u01B0\u01A1ng hi\u1EC7u v\u00E0o ph\u1EA7n \u201CT\u00ECm ki\u1EBFm\""),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'img-border', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-web-004-search.png', alt: 'Tìm kiếm sản phẩm trên Lixibox' })),
                react["createElement"]("p", null, "C\u00E1ch 2: Tham kh\u1EA3o c\u00E1c s\u1EA3n ph\u1EA9m v\u00E0 th\u01B0\u01A1ng hi\u1EC7u tr\u00EAn thanh m\u1EE5c l\u1EE5c c\u1EE7a trang ch\u1EE7"),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'img-border', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-web-005-product-menu.png', alt: 'Tìm kiếm sản phẩm trên menu' })),
                react["createElement"]("div", { className: 'sub-title' }, "B\u01B0\u1EDBc 3: T\u00ECm hi\u1EC3u th\u00F4ng tin s\u1EA3n ph\u1EA9m "),
                react["createElement"]("p", null, "Sau khi ch\u1ECDn \u0111\u01B0\u1EE3c s\u1EA3n ph\u1EA9m \u01B0ng \u00FD, h\u00E3y tham kh\u1EA3o c\u00E1c th\u00F4ng tin chi ti\u1EBFt \u0111\u01B0\u1EE3c Lixibox b\u1EADt m\u00ED trong ph\u1EA7n S\u1EA2N PH\u1EA8M v\u00E0 nh\u1EEFng \u0110\u00C1NH GI\u00C1 t\u1EEB tr\u1EA3i nghi\u1EC7m th\u1EF1c t\u1EBF c\u1EE7a c\u00E1c kh\u00E1ch h\u00E0ng \u0111\u00E3 s\u1EED d\u1EE5ng s\u1EA3n ph\u1EA9m n\u00E0y!"),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'img-border', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-web-006-product-review.png', alt: 'Tìm kiếm sản phẩm trên menu' })),
                react["createElement"]("p", null, "Ch\u1ECDn TH\u00CAM V\u00C0O GI\u1ECE \u0111\u1EC3 \u0111\u01B0a s\u1EA3n ph\u1EA9m v\u00E0o gi\u1ECF h\u00E0ng. V\u1EADy l\u00E0 b\u1EA1n \u0111\u00E3 c\u00F3 th\u1EC3 y\u00EAn t\u00E2m ti\u1EBFp t\u1EE5c l\u1EF1a ch\u1ECDn c\u00E1c s\u1EA3n ph\u1EA9m kh\u00E1c r\u1ED3i!"),
                react["createElement"]("div", { className: 'sub-title' }, "B\u01B0\u1EDBc 4: Ti\u1EBFn h\u00E0nh \u0111\u1EB7t h\u00E0ng "),
                react["createElement"]("p", null, "Ki\u1EC3m tra c\u00E1c s\u1EA3n ph\u1EA9m \u0111\u00E3 c\u00F3 trong gi\u1ECF h\u00E0ng c\u1EE7a b\u1EA1n v\u00E0 ti\u1EBFn h\u00E0nh \u0111\u1EB7t h\u00E0ng. \u0110\u1EEBng qu\u00EAn \u0111i\u1EC1n c\u00E1c M\u00C3 GI\u1EA2M GI\u00C1 n\u1EBFu c\u00F3 nh\u00E9!"),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'img-border', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-web-007-checkout-cart.png', alt: 'Checkout' })),
                react["createElement"]("div", { className: 'sub-title' }, "B\u01B0\u1EDBc 5: Mua s\u1EA3n ph\u1EA9m v\u1EDBi gi\u00E1 \u01B0u \u0111\u00E3i?"),
                react["createElement"]("p", null, "Lixibox lu\u00F4n c\u00F3 nh\u1EEFng s\u1EA3n ph\u1EA9m ADD ON (mua k\u00E8m) v\u1EDBi gi\u00E1 c\u1EF1c k\u1EF3 \u01B0u \u0111\u00E3i cho b\u1EA1n. B\u1EA1n ch\u1EC9 vi\u1EC7c ch\u1ECDn s\u1EA3n ph\u1EA9m \u01B0u \u0111\u00E3i v\u00E0 ho\u1EB7c ti\u1EBFn h\u00E0nh \u0111\u1EB7t h\u00E0ng ngay"),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'img-border', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-web-008-checkout-addon.png', alt: 'Addon sản phẩm' })),
                react["createElement"]("div", { className: 'sub-title' }, "B\u01B0\u1EDBc 6: Ch\u1ECDn ph\u01B0\u01A1ng th\u1EE9c thanh to\u00E1n. \u0110i\u1EC1n th\u00F4ng tin chi ti\u1EBFt ng\u01B0\u1EDDi nh\u1EADn. Ch\u1ECDn h\u00ECnh th\u1EE9c thanh to\u00E1n "),
                react["createElement"]("p", null, "Ch\u1ECDn ph\u01B0\u01A1ng th\u1EE9c giao h\u00E0ng. G\u1ED3m 3 ph\u01B0\u01A1ng th\u1EE9c giao h\u00E0ng: Ti\u00EAu chu\u1EA9n - Trong ng\u00E0y - T\u1EA1i c\u1EEDa h\u00E0ng"),
                react["createElement"]("p", null,
                    react["createElement"]("img", { src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-web-009-delivery-method.png', alt: 'Phương thức giao hàng' })),
                react["createElement"]("p", null, "Nh\u1EADp \u0111\u1ECBa ch\u1EC9 giao h\u00E0ng m\u1ED9t c\u00E1ch ch\u00EDnh x\u00E1c \u0111\u1EC3 ch\u00FAng t\u00F4i c\u00F3 th\u1EC3 g\u1EEDi s\u1EA3n ph\u1EA9m \u0111\u1EBFn v\u1EDBi b\u1EA1n trong th\u1EDDi gian s\u1EDBm nh\u1EA5t! \u0110\u1EEBng qu\u00EAn ki\u1EC3m tra s\u1ED1 l\u01B0\u1EE3ng s\u1EA3n ph\u1EA9m, h\u1ECD t\u00EAn/ s\u1ED1 \u0111i\u1EC7n tho\u1EA1i ng\u01B0\u1EDDi nh\u1EADn m\u1ED9t m\u1ED9t l\u1EA7n tr\u01B0\u1EDBc khi sang b\u01B0\u1EDBc ti\u1EBFp theo"),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'img-border', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-web-010-address-method.png', alt: 'Địa chỉ giao hàng' })),
                react["createElement"]("p", null, "Sau khi \u0111i\u1EC1n \u0111\u1EE7 th\u00F4ng tin nh\u1EADn h\u00E0ng, Lixibox hi\u1EC7n c\u00F3 3 h\u00ECnh th\u1EE9c thanh to\u00E1n cho b\u1EA1n l\u1EF1a ch\u1ECDn l\u00E0 COD - Thanh to\u00E1n sau khi nh\u1EADn h\u00E0ng/ Thanh to\u00E1n online qua Th\u1EBB ng\u00E2n h\u00E0ng v\u00E0 cu\u1ED1i c\u00F9ng l\u00E0 Chuy\u1EC3n kho\u1EA3n \u0111\u1EBFn T\u00E0i kho\u1EA3n c\u1EE7a Lixibox. "),
                react["createElement"]("p", null,
                    react["createElement"]("img", { src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-web-011-payment-method.png', alt: 'Phương thức thanh toán' })),
                react["createElement"]("div", { className: 'sub-title' }, "B\u01B0\u1EDBc 9: B\u1EA1n \u0111\u00E3 \u0111\u1EB7t h\u00E0ng th\u00E0nh c\u00F4ng!"),
                react["createElement"]("p", null, "Sau khi x\u00E1c nh\u1EADn \u0111\u1EB7t h\u00E0ng, b\u1EA1n s\u1EBD nh\u1EADn \u0111\u01B0\u1EE3c m\u00E3 \u0111\u01A1n h\u00E0ng v\u00E0 tin nh\u1EAFn/ email t\u1EEB Lixibox. H\u00E3y l\u01B0u l\u1EA1i m\u00E3 \u0111\u01A1n h\u00E0ng \u0111\u1EC3 b\u1ED9 ph\u1EADn CSKH c\u1EE7a ch\u00FAng t\u00F4i c\u00F3 th\u1EC3 gi\u1EA3i quy\u1EBFt c\u00E1c th\u1EAFc m\u1EAFc cho b\u1EA1n trong tr\u01B0\u1EDDng h\u1EE3p c\u1EA7n thi\u1EBFt."),
                react["createElement"]("p", null,
                    react["createElement"]("img", { className: 'img-border', src: uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/info/buy-on-web-012-checkout-success.png', alt: 'Checkout thành công' })),
                react["createElement"]("p", null,
                    "R\u1EA5t c\u1EA3m \u01A1n v\u00E0 hy v\u1ECDng b\u1EA1n \u0111\u00E3 tr\u1EA3i nghi\u1EC7m mua s\u1EAFm tuy\u1EC7t v\u1EDDi t\u1EA1i ",
                    react["createElement"]("a", { href: 'http://lxbtest.tk/', target: '_blank' }, "Lixibox.com"),
                    "!")),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_buy_on_web = (view_buy_on_web_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1idXktb24td2ViLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1idXktb24td2ViLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUMvQixPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sUUFBUSxDQUFDO0FBRS9CLE9BQU8sVUFBVSxNQUFNLHNCQUFzQixDQUFDO0FBRTlDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFFdkMsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFFMUQsSUFBTSxhQUFhLEdBQUc7SUFFcEIsTUFBTSxDQUFDLENBQ0w7UUFDRSxvQkFBQyxVQUFVO1lBQ1QsNkJBQUssU0FBUyxFQUFFLGNBQWM7Z0JBQzVCLHNHQUFtRDtnQkFDbkQsNkJBQUssU0FBUyxFQUFFLFdBQVcsbUVBQWlDO2dCQUM1RCwwWEFBc0s7Z0JBQ3RLO29CQUFHLDZCQUFLLFNBQVMsRUFBRSxnQkFBZ0IsRUFBRSxHQUFHLEVBQUUsaUJBQWlCLEdBQUcsK0NBQStDLEVBQUUsR0FBRyxFQUFFLGlCQUFpQixHQUFJLENBQUk7Z0JBQzdJLDJTQUFxSTtnQkFDckk7b0JBQUcsNkJBQUssU0FBUyxFQUFFLGdCQUFnQixFQUFFLEdBQUcsRUFBRSxpQkFBaUIsR0FBRyw4Q0FBOEMsRUFBRSxHQUFHLEVBQUUsaUJBQWlCLEdBQUksQ0FBSTtnQkFDNUksNkJBQUssU0FBUyxFQUFFLFdBQVcsMEZBQThDO2dCQUN6RSxnUUFBNkg7Z0JBQzdILDhLQUF3RTtnQkFDeEU7b0JBQUcsNkJBQUssU0FBUyxFQUFFLFlBQVksRUFBRSxHQUFHLEVBQUUsaUJBQWlCLEdBQUcsK0NBQStDLEVBQUUsR0FBRyxFQUFFLGdDQUFnQyxHQUFJLENBQUk7Z0JBQ3hKLHNMQUFzRjtnQkFDdEY7b0JBQUcsNkJBQUssU0FBUyxFQUFFLFlBQVksRUFBRSxHQUFHLEVBQUUsaUJBQWlCLEdBQUcscURBQXFELEVBQUUsR0FBRyxFQUFFLDZCQUE2QixHQUFJLENBQUk7Z0JBQzNKLDZCQUFLLFNBQVMsRUFBRSxXQUFXLDhFQUE0QztnQkFDdkUsdWJBQTBNO2dCQUMxTTtvQkFBRyw2QkFBSyxTQUFTLEVBQUUsWUFBWSxFQUFFLEdBQUcsRUFBRSxpQkFBaUIsR0FBRyx1REFBdUQsRUFBRSxHQUFHLEVBQUUsNkJBQTZCLEdBQUksQ0FBSTtnQkFDN0osa1RBQTZIO2dCQUM3SCw2QkFBSyxTQUFTLEVBQUUsV0FBVyxxRUFBbUM7Z0JBQzlELHVTQUE0SDtnQkFDNUg7b0JBQUcsNkJBQUssU0FBUyxFQUFFLFlBQVksRUFBRSxHQUFHLEVBQUUsaUJBQWlCLEdBQUcsc0RBQXNELEVBQUUsR0FBRyxFQUFFLFVBQVUsR0FBSSxDQUFJO2dCQUN6SSw2QkFBSyxTQUFTLEVBQUUsV0FBVyx3RkFBNEM7Z0JBQ3ZFLHdVQUF3SjtnQkFDeEo7b0JBQUcsNkJBQUssU0FBUyxFQUFFLFlBQVksRUFBRSxHQUFHLEVBQUUsaUJBQWlCLEdBQUcsdURBQXVELEVBQUUsR0FBRyxFQUFFLGdCQUFnQixHQUFJLENBQUk7Z0JBQ2hKLDZCQUFLLFNBQVMsRUFBRSxXQUFXLG9NQUEyRztnQkFDdEksaU5BQXVHO2dCQUN2RztvQkFBRyw2QkFBSyxHQUFHLEVBQUUsaUJBQWlCLEdBQUcsd0RBQXdELEVBQUUsR0FBRyxFQUFFLHVCQUF1QixHQUFJLENBQUk7Z0JBQy9ILGtnQkFBeU87Z0JBQ3pPO29CQUFHLDZCQUFLLFNBQVMsRUFBRSxZQUFZLEVBQUUsR0FBRyxFQUFFLGlCQUFpQixHQUFHLHVEQUF1RCxFQUFFLEdBQUcsRUFBRSxtQkFBbUIsR0FBSSxDQUFJO2dCQUNuSiw0YUFBd087Z0JBQ3hPO29CQUFHLDZCQUFLLEdBQUcsRUFBRSxpQkFBaUIsR0FBRyx1REFBdUQsRUFBRSxHQUFHLEVBQUUsd0JBQXdCLEdBQUksQ0FBSTtnQkFDL0gsNkJBQUssU0FBUyxFQUFFLFdBQVcsNEZBQTJDO2dCQUN0RSxrZEFBdU47Z0JBQ3ZOO29CQUFHLDZCQUFLLFNBQVMsRUFBRSxZQUFZLEVBQUUsR0FBRyxFQUFFLGlCQUFpQixHQUFHLHlEQUF5RCxFQUFFLEdBQUcsRUFBRSxxQkFBcUIsR0FBSSxDQUFJO2dCQUN2Sjs7b0JBQWtFLDJCQUFHLElBQUksRUFBRSwwQkFBMEIsRUFBRSxNQUFNLEVBQUUsUUFBUSxrQkFBaUI7d0JBQUssQ0FDekk7WUFDTixvQkFBQyxLQUFLLElBQUMsS0FBSyxFQUFFLFlBQVksQ0FBQyxnQkFBZ0IsR0FBSSxDQUNwQyxDQUNTLENBQ3pCLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLGFBQWEsQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/info/about/view-shipping-fee.tsx




var view_shipping_fee_renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("p", null, "Ph\u00ED v\u1EADn chuy\u1EC3n \u0111\u01B0\u1EE3c t\u00EDnh l\u00FAc \u0111\u1EB7t h\u00E0ng, tu\u1EF3 theo \u0111\u1ECBa ch\u1EC9 nh\u1EADn h\u00E0ng c\u1EE7a qu\u00FD kh\u00E1ch. Lixibox mi\u1EC5n ph\u00ED v\u1EADn chuy\u1EC3n to\u00E0n qu\u1ED1c cho \u0111\u01A1n h\u00E0ng t\u1EEB 800,000\u0111 tr\u1EDF l\u00EAn. Qu\u00FD kh\u00E1ch l\u01B0u \u00FD ki\u1EC3m tra k\u0129 \u0111\u1ECBa ch\u1EC9 nh\u1EADn h\u00E0ng \u0111\u1EC3 vi\u1EC7c t\u00EDnh ph\u00ED v\u1EADn chuy\u1EC3n \u0111\u01B0\u1EE3c ch\u00EDnh x\u00E1c nh\u1EA5t.")),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_shipping_fee = (view_shipping_fee_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1zaGlwcGluZy1mZWUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LXNoaXBwaW5nLWZlZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUUvQixPQUFPLFVBQVUsTUFBTSxzQkFBc0IsQ0FBQztBQUU5QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBRXZDLElBQU0sYUFBYSxHQUFHO0lBRXBCLE1BQU0sQ0FBQyxDQUNMO1FBQ0Usb0JBQUMsVUFBVTtZQUNULDZCQUFLLFNBQVMsRUFBRSxjQUFjO2dCQUM1QixzakJBQThQLENBQzFQO1lBQ04sb0JBQUMsS0FBSyxJQUFDLEtBQUssRUFBRSxZQUFZLENBQUMsZ0JBQWdCLEdBQUksQ0FDcEMsQ0FDUyxDQUN6QixDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsZUFBZSxhQUFhLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/info/about/view-delivery-and-payment.tsx




var view_delivery_and_payment_renderDesktop = function () { return (react["createElement"]("info-detail-container", null,
    react["createElement"](wrap["a" /* default */], null,
        react["createElement"]("div", { className: 'info-content' },
            react["createElement"]("h2", null, "Hi\u1EC7n Lixibox c\u00F3 3 h\u00ECnh th\u1EE9c thanh to\u00E1n cho kh\u00E1ch h\u00E0ng l\u1EF1a ch\u1ECDn:"),
            react["createElement"]("div", { className: 'sub-title' }, "1. C\u1ED5ng thanh to\u00E1n Onepay (\u00E1p d\u1EE5ng cho m\u1ECDi \u0111\u01A1n h\u00E0ng):"),
            react["createElement"]("p", null, "Sau khi kh\u00E1ch h\u00E0ng thanh to\u00E1n qua c\u1ED5ng Onepay, h\u1EC7 th\u1ED1ng s\u1EBD t\u1EF1 \u0111\u1ED9ng g\u1EDFi SMS x\u00E1c nh\u1EADn cho kh\u00E1ch, \u0111\u01A1n h\u00E0ng s\u1EBD \u0111\u01B0\u1EE3c x\u1EED l\u00FD v\u00E0 ship \u0111i trong v\u00F2ng 24h."),
            react["createElement"]("div", { className: 'sub-title' }, "2. Chuy\u1EC3n kho\u1EA3n (\u00E1p d\u1EE5ng cho m\u1ECDi \u0111\u01A1n h\u00E0ng):"),
            react["createElement"]("p", null, "Sau khi kh\u00E1ch h\u00E0ng \u0111\u1EB7t h\u00E0ng th\u00E0nh c\u00F4ng, h\u1EC7 th\u1ED1ng s\u1EBD g\u1EEDi SMS v\u00E0 email x\u00E1c nh\u1EADn \u0111\u1EB7t h\u00E0ng g\u1ED3m c\u00F3 m\u00E3 \u0111\u01A1n h\u00E0ng v\u00E0 th\u00F4ng tin chuy\u1EC3n kho\u1EA3n. Kh\u00E1ch h\u00E0ng c\u00F3 th\u1EC3 chuy\u1EC3n qua iBanking, chuy\u1EC3n ti\u1EC1n m\u1EB7t \u1EDF ng\u00E2n h\u00E0ng, chuy\u1EC3n qua ATM... Khi chuy\u1EC3n qu\u00FD kh\u00E1ch l\u01B0u \u00FD ghi r\u00F5 m\u00E3 \u0111\u01A1n h\u00E0ng \u0111\u1EC3 ti\u1EC7n trong vi\u1EC7c ki\u1EC3m tra v\u00E0 c\u1EADp nh\u1EADp thanh to\u00E1n. Tr\u01B0\u1EDDng h\u1EE3p qu\u00FD kh\u00E1ch chuy\u1EC3n kho\u1EA3n qua c\u00E2y ATM kh\u00F4ng ghi \u0111\u01B0\u1EE3c n\u1ED9i dung, vui l\u00F2ng gi\u1EEF l\u1EA1i bi\u00EAn nh\u1EADn v\u00E0 g\u1ECDi v\u00E0o hotline"),
            react["createElement"]("p", null,
                "1800 2040 ho\u1EB7c g\u1EDFi tin nh\u1EAFn v\u00E0o fanpage: ",
                react["createElement"]("a", { href: 'https://www.facebook.com/lixiboxvn', target: '_blank' }, "https://www.facebook.com/lixiboxvn"),
                " \u0111\u1EC3 \u0111\u01B0\u1EE3c h\u1ED7 tr\u1EE3 x\u00E1c nh\u1EADn thanh to\u00E1n. Sau khi \u0111\u01A1n h\u00E0ng \u0111\u01B0\u1EE3c c\u1EADp nh\u1EADt thanh to\u00E1n th\u00E0nh c\u00F4ng s\u1EBD \u0111\u01B0\u1EE3c x\u1EED l\u00FD v\u00E0 ship \u0111i trong v\u00F2ng 24h."),
            react["createElement"]("div", { className: 'sub-title' }, "3. COD (\u00E1p d\u1EE5ng cho m\u1ECDi \u0111\u01A1n h\u00E0ng c\u00F3 \u0111\u1ECBa ch\u1EC9 nh\u1EADn h\u00E0ng t\u1EA1i th\u00E0nh ph\u1ED1 H\u1ED3 Ch\u00ED Minh, H\u00E0 N\u1ED9i v\u00E0 c\u00E1c \u0111\u01A1n h\u00E0ng t\u1EEB 400,000\u0111 tr\u1EDF l\u00EAn c\u00F3 \u0111\u1ECBa ch\u1EC9 nh\u1EADn h\u00E0ng t\u1EA1i c\u00E1c t\u1EC9nh th\u00E0nh kh\u00E1c):"),
            react["createElement"]("p", null, "Sau khi qu\u00FD kh\u00E1ch ho\u00E0n t\u1EA5t \u0111\u1EB7t h\u00E0ng, b\u1ED9 ph\u1EADn Ch\u0103m s\u00F3c kh\u00E1ch h\u00E0ng c\u1EE7a Lixibox s\u1EBD g\u1ECDi \u0111i\u1EC7n cho qu\u00FD kh\u00E1ch \u0111\u1EC3 x\u00E1c nh\u1EADn. Sau khi x\u00E1c nh\u1EADn th\u00E0nh c\u00F4ng, \u0111\u01A1n h\u00E0ng s\u1EBD \u0111\u01B0\u1EE3c x\u1EED l\u00FD v\u00E0 ship \u0111i trong v\u00F2ng 24h.")),
        react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup })))); };
/* harmony default export */ var view_delivery_and_payment = (view_delivery_and_payment_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1kZWxpdmVyeS1hbmQtcGF5bWVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXctZGVsaXZlcnktYW5kLXBheW1lbnQudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFL0IsT0FBTyxVQUFVLE1BQU0sc0JBQXNCLENBQUM7QUFDOUMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUV2QyxJQUFNLGFBQWEsR0FBRyxjQUFNLE9BQUEsQ0FDMUI7SUFDRSxvQkFBQyxVQUFVO1FBQ1QsNkJBQUssU0FBUyxFQUFFLGNBQWM7WUFDNUIsK0lBQXdFO1lBQ3hFLDZCQUFLLFNBQVMsRUFBRSxXQUFXLG9HQUE2RDtZQUN4RixvVEFBdUo7WUFDdkosNkJBQUssU0FBUyxFQUFFLFdBQVcsMEZBQW1EO1lBQzlFLDgxQkFBd2I7WUFDeGI7O2dCQUE0QywyQkFBRyxJQUFJLEVBQUUsb0NBQW9DLEVBQUUsTUFBTSxFQUFFLFFBQVEseUNBQXdDOzJTQUFzSTtZQUN6Uiw2QkFBSyxTQUFTLEVBQUUsV0FBVyxvWEFBa0w7WUFDN00sNGFBQXlNLENBQ3JNO1FBQ04sb0JBQUMsS0FBSyxJQUFDLEtBQUssRUFBRSxZQUFZLENBQUMsZ0JBQWdCLEdBQUksQ0FDcEMsQ0FDUyxDQUN6QixFQWhCMkIsQ0FnQjNCLENBQUM7QUFFRixlQUFlLGFBQWEsQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/info/about/view-receive-time.tsx




var view_receive_time_renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("div", { className: 'sub-title' }, "1. \u0110\u1ED1i v\u1EDBi \u0111\u01A1n h\u00E0ng ti\u00EAu chu\u1EA9n:"),
                react["createElement"]("p", null,
                    "\u0110\u01A1n h\u00E0ng \u1EDF H\u1ED3 Ch\u00ED Minh, H\u00E0 N\u1ED9i: 1 - 2 ng\u00E0y l\u00E0m vi\u1EC7c.",
                    react["createElement"]("br", null),
                    "\u0110\u01A1n h\u00E0ng \u1EDF c\u00E1c t\u1EC9nh th\u00E0nh kh\u00E1c: 2 - 5 ng\u00E0y l\u00E0m vi\u1EC7c."),
                react["createElement"]("div", { className: 'sub-title' }, "2. \u0110\u1ED1i v\u1EDBi \u0111\u01A1n h\u00E0ng \u0111\u1EB7t tr\u01B0\u1EDBc (Pre-order):"),
                react["createElement"]("p", null, "Th\u1EDDi gian nh\u1EADn h\u00E0ng d\u1EF1 ki\u1EBFn s\u1EBD hi\u1EC3n th\u1ECB tr\u00EAn website, ph\u00EDa b\u00EAn d\u01B0\u1EDBi gi\u00E1 c\u1EE7a s\u1EA3n ph\u1EA9m t\u1EA1i giao di\u1EC7n \u0111\u1EB7t h\u00E0ng c\u1EE7a qu\u00FD kh\u00E1ch. Qu\u00FD kh\u00E1ch c\u00F3 th\u1EC3 ki\u1EC3m tra th\u00F4ng tin ng\u00E0y nh\u1EADn h\u00E0ng d\u1EF1 ki\u1EBFn trong email x\u00E1c nh\u1EADn \u0111\u01A1n h\u00E0ng do h\u1EC7 th\u1ED1ng t\u1EF1 \u0111\u1ED9ng g\u1EEDi v\u1EC1."),
                react["createElement"]("p", null, "M\u1ECDi thay \u0111\u1ED5i li\u00EAn quan t\u1EDBi th\u1EDDi gian nh\u1EADn h\u00E0ng b\u1ED9 ph\u1EADn Ch\u0103m s\u00F3c kh\u00E1ch h\u00E0ng c\u1EE7a Lixibox s\u1EBD li\u00EAn l\u1EA1c tr\u1EF1c ti\u1EBFp v\u1EDBi qu\u00FD kh\u00E1ch.")),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_receive_time = (view_receive_time_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1yZWNlaXZlLXRpbWUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LXJlY2VpdmUtdGltZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUUvQixPQUFPLFVBQVUsTUFBTSxzQkFBc0IsQ0FBQztBQUU5QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBRXZDLElBQU0sYUFBYSxHQUFHO0lBRXBCLE1BQU0sQ0FBQyxDQUNMO1FBQ0Usb0JBQUMsVUFBVTtZQUNULDZCQUFLLFNBQVMsRUFBRSxjQUFjO2dCQUM1Qiw2QkFBSyxTQUFTLEVBQUUsV0FBVyw4RUFBdUM7Z0JBQ2xFOztvQkFBdUQsK0JBQU07a0lBQXdEO2dCQUNySCw2QkFBSyxTQUFTLEVBQUUsV0FBVyxtR0FBa0Q7Z0JBQzdFLGlnQkFBc1A7Z0JBQ3RQLGdSQUFrSSxDQUM5SDtZQUNOLG9CQUFDLEtBQUssSUFBQyxLQUFLLEVBQUUsWUFBWSxDQUFDLGdCQUFnQixHQUFJLENBQ3BDLENBQ1MsQ0FDekIsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsYUFBYSxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/info/about/view-receive-and-redeem.tsx




var view_receive_and_redeem_renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("p", null, "- Lixibox giao h\u00E0ng trong gi\u1EDD h\u00E0nh ch\u00EDnh (8h - 17h t\u1EEB th\u1EE9 2 - th\u1EE9 7), tr\u01B0\u1EDDng h\u1EE3p qu\u00FD kh\u00E1ch c\u00F3 nhu c\u1EA7u nh\u1EADn h\u00E0ng ngo\u00E0i gi\u1EDD h\u00E0nh ch\u00EDnh vui l\u00F2ng g\u1ECDi \u0111i\u1EC7n v\u00E0o hotline 1800 2040 \u0111\u1EC3 \u0111\u01B0\u1EE3c h\u1ED7 tr\u1EE3."),
                react["createElement"]("p", null, "- Tr\u01B0\u1EDBc khi k\u00ED bi\u00EAn b\u1EA3n nh\u1EADn h\u00E0ng, qu\u00FD kh\u00E1ch vui l\u00F2ng m\u1EDF h\u1ED9p v\u00E0 ki\u1EC3m tra k\u0129 t\u1EA5t c\u1EA3 c\u00E1c s\u1EA3n ph\u1EA9m tr\u01B0\u1EDBc m\u1EB7t \u0111\u01A1n v\u1ECB v\u1EADn chuy\u1EC3n. N\u1EBFu h\u00E0ng ho\u00E1 thi\u1EBFu ho\u1EB7c h\u01B0 h\u1ECFng, vui l\u00F2ng t\u1EEB ch\u1ED1i nh\u1EADn h\u00E0ng v\u00E0 g\u1ECDi ngay v\u00E0o hotline 1800 2040 \u0111\u1EC3 \u0111\u01B0\u1EE3c ch\u00FAng t\u00F4i h\u1ED7 tr\u1EE3 nhanh nh\u1EA5t. Ch\u00FAng t\u00F4i s\u1EBD kh\u00F4ng ch\u1ECBu tr\u00E1ch nhi\u1EC7m trong tr\u01B0\u1EDDng h\u1EE3p qu\u00FD kh\u00E1ch b\u00E1o m\u1EA5t m\u00E1t, h\u01B0 h\u1ECFng sau khi \u0111\u00E3 k\u00FD bi\u00EAn b\u1EA3n nh\u1EADn h\u00E0ng v\u00E0 \u0111\u01A1n v\u1ECB v\u1EADn chuy\u1EC3n \u0111\u00E3 r\u1EDDi \u0111i."),
                react["createElement"]("p", null, "- Lixibox ch\u1EA5p nh\u1EADn \u0111\u1ED5i tr\u1EA3 h\u00E0ng do l\u1ED7i c\u1EE7a nh\u00E0 s\u1EA3n xu\u1EA5t trong v\u00F2ng 7 ng\u00E0y k\u1EC3 t\u1EEB khi nh\u1EADn h\u00E0ng. Sau khi Lixibox nh\u1EADn \u0111\u01B0\u1EE3c h\u00E0ng tr\u1EA3 s\u1EBD ti\u1EBFn h\u00E0nh ki\u1EC3m tra v\u00E0 li\u00EAn l\u1EA1c l\u1EA1i v\u1EDBi qu\u00FD kh\u00E1ch \u0111\u1EC3 th\u1ED1ng nh\u1EA5t c\u00E1ch gi\u1EA3i quy\u1EBFt. Sau 7 ng\u00E0y k\u1EC3 t\u1EEB khi nh\u1EADn h\u00E0ng, Lixibox s\u1EBD ng\u01B0ng gi\u1EA3i quy\u1EBFt c\u00E1c y\u00EAu c\u1EA7u \u0111\u1ED5i/tr\u1EA3 h\u00E0ng.")),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_receive_and_redeem = (view_receive_and_redeem_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1yZWNlaXZlLWFuZC1yZWRlZW0uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LXJlY2VpdmUtYW5kLXJlZGVlbS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUUvQixPQUFPLFVBQVUsTUFBTSxzQkFBc0IsQ0FBQztBQUU5QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBRXZDLElBQU0sYUFBYSxHQUFHO0lBRXBCLE1BQU0sQ0FBQyxDQUNMO1FBQ0Usb0JBQUMsVUFBVTtZQUNULDZCQUFLLFNBQVMsRUFBRSxjQUFjO2dCQUM1Qiw4WEFBbU07Z0JBQ25NLHkyQkFDNkw7Z0JBQzdMLDBtQkFBa1QsQ0FDOVM7WUFDTixvQkFBQyxLQUFLLElBQUMsS0FBSyxFQUFFLFlBQVksQ0FBQyxnQkFBZ0IsR0FBSSxDQUNwQyxDQUNVLENBQzFCLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLGFBQWEsQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/info/about/view-guarantee.tsx




var view_guarantee_renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("p", null, "Lixibox th\u1EF1c hi\u1EC7n qu\u1EA3n l\u00FD t\u1EF1 \u0111\u1ED9ng tr\u00EAn h\u1EC7 th\u1ED1ng b\u1EA3o h\u00E0nh v\u00E0 \u0111\u1ED5i tr\u1EA3 s\u1EA3n ph\u1EA9m v\u1EDBi c\u00E1c s\u1EA3n ph\u1EA9m c\u00F3 ch\u00EDnh s\u00E1ch b\u1EA3o h\u00E0nh. Ch\u00FAng t\u00F4i kh\u00F4ng ph\u00E1t h\u00E0nh b\u1EA5t k\u1EF3 lo\u1EA1i gi\u1EA5y t\u1EDD b\u1EA3o h\u00E0nh n\u00E0o. Qu\u00FD kh\u00E1ch c\u1EA7n th\u1EF1c hi\u1EC7n b\u1EA3o h\u00E0nh s\u1EA3n ph\u1EA9m, vui l\u00F2ng thao t\u00E1c theo h\u01B0\u1EDBng d\u1EABn:"),
                react["createElement"]("div", { className: 'sub-title' },
                    "1. Qu\u00FD kh\u00E1ch li\u00EAn l\u1EA1c tr\u1EF1c ti\u1EBFp v\u1EDBi Lixibox qua hotline 1800 2040 ho\u1EB7c fanpage ",
                    react["createElement"]("a", { href: 'https://www.facebook.com/lixiboxvn' }, "https:// www.facebook.com/lixiboxvn")),
                react["createElement"]("div", { className: 'sub-title' }, "2. Qu\u00FD kh\u00E1ch cung c\u1EA5p m\u00E3 \u0111\u01A1n h\u00E0ng ho\u1EB7c s\u1ED1 \u0111i\u1EC7n tho\u1EA1i \u0111\u1EB7t h\u00E0ng \u0111\u1EC3 nh\u00E2n vi\u00EAn ch\u0103m s\u00F3c kh\u00E1ch h\u00E0ng ki\u1EC3m tra tr\u00EAn h\u1EC7 th\u1ED1ng qu\u1EA3n l\u00FD."),
                react["createElement"]("div", { className: 'sub-title' }, "3. Qu\u00FD kh\u00E1ch g\u1EEDi h\u00E0ng theo h\u01B0\u1EDBng d\u1EABn c\u1EE7a nh\u00E2n vi\u00EAn ch\u0103m s\u00F3c kh\u00E1ch h\u00E0ng. Qu\u00FD kh\u00E1ch l\u01B0u \u00FD, qu\u00FD kh\u00E1ch s\u1EBD nh\u1EADn \u0111\u01B0\u1EE3c h\u1ED7 tr\u1EE3 50% chi ph\u00ED v\u1EADn chuy\u1EC3n ph\u00E1t sinh trong qu\u00E1 tr\u00ECnh b\u1EA3o h\u00E0nh."),
                react["createElement"]("p", null, "L\u01B0u \u00FD: khi g\u1EEDi h\u00E0nh v\u1EC1 b\u1EA3o h\u00E0nh qu\u00FD kh\u00E1ch vui l\u00F2ng g\u1EEDi k\u00E8m t\u1EA5t c\u1EA3 ph\u1EE5 ki\u1EC7n (n\u1EBFu c\u00F3), n\u1EBFu s\u1EA3n ph\u1EA9m b\u1ECB thi\u1EBFu ph\u1EE5 ki\u1EC7n Lixibox s\u1EBD kh\u00F4ng nh\u1EADn b\u1EA3o h\u00E0nh."),
                react["createElement"]("p", null, "Lixibox xin ch\u00E2n th\u00E0nh c\u1EA3m \u01A1n!")),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_guarantee = (view_guarantee_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1ndWFyYW50ZWUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LWd1YXJhbnRlZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUUvQixPQUFPLFVBQVUsTUFBTSxzQkFBc0IsQ0FBQztBQUU5QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBRXZDLElBQU0sYUFBYSxHQUFHO0lBRXBCLE1BQU0sQ0FBQyxDQUNMO1FBQ0Usb0JBQUMsVUFBVTtZQUNULDZCQUFLLFNBQVMsRUFBRSxjQUFjO2dCQUM1Qiw4aUJBQXFRO2dCQUNyUSw2QkFBSyxTQUFTLEVBQUUsV0FBVzs7b0JBQWlGLDJCQUFHLElBQUksRUFBRSxvQ0FBb0MsMENBQXlDLENBQU07Z0JBQ3hNLDZCQUFLLFNBQVMsRUFBRSxXQUFXLHNSQUFzSTtnQkFDakssNkJBQUssU0FBUyxFQUFFLFdBQVcsdVdBQXlMO2dCQUNwTiwyVUFBMEo7Z0JBQzFKLG9GQUFxQyxDQUNqQztZQUNOLG9CQUFDLEtBQUssSUFBQyxLQUFLLEVBQUUsWUFBWSxDQUFDLGdCQUFnQixHQUFJLENBQ3BDLENBQ1MsQ0FDekIsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsYUFBYSxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/info/about/view-content.tsx




var view_content_renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("p", null, "- V\u1EDBi \u0111\u01A1n h\u00E0ng t\u1EEB 400.000\u0110 tr\u1EDF l\u00EAn, qu\u00FD kh\u00E1ch nh\u1EADp m\u00E3 Lixi100 \u0111\u1EC3 \u0111\u01B0\u1EE3c t\u1EB7ng 01 Giftcard tr\u1ECB gi\u00E1 100.000\u0110 v\u00E0o gi\u1ECF h\u00E0ng."),
                react["createElement"]("p", null, "- V\u1EDBi \u0111\u01A1n h\u00E0ng t\u1EEB 700.000\u0110 tr\u1EDF l\u00EAn, qu\u00FD kh\u00E1ch nh\u1EADp m\u00E3 Lixi200 \u0111\u1EC3 \u0111\u01B0\u1EE3c t\u1EB7ng 01 Giftcard tr\u1ECB gi\u00E1 200.000\u0110 v\u00E0o gi\u1ECF h\u00E0ng."),
                react["createElement"]("p", null, "- V\u1EDBi \u0111\u01A1n h\u00E0ng t\u1EEB 1.000.000\u0110 tr\u1EDF l\u00EAn, qu\u00FD kh\u00E1ch nh\u1EADp m\u00E3 Lixi400 \u0111\u1EC3 \u0111\u01B0\u1EE3c t\u1EB7ng 01 Giftcard tr\u1ECB gi\u00E1 400.000\u0110 v\u00E0o gi\u1ECF h\u00E0ng.")),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_content = (view_content_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1jb250ZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1jb250ZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUMvQixPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sUUFBUSxDQUFDO0FBRS9CLE9BQU8sVUFBVSxNQUFNLHNCQUFzQixDQUFDO0FBRTlDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFFdkMsSUFBTSxhQUFhLEdBQUc7SUFFcEIsTUFBTSxDQUFDLENBQ0w7UUFDRSxvQkFBQyxVQUFVO1lBQ1QsNkJBQUssU0FBUyxFQUFFLGNBQWM7Z0JBQzVCLCtRQUE0SDtnQkFDNUgsK1FBQTRIO2dCQUM1SCxpUkFBOEgsQ0FDMUg7WUFDTixvQkFBQyxLQUFLLElBQUMsS0FBSyxFQUFFLFlBQVksQ0FBQyxnQkFBZ0IsR0FBSSxDQUNwQyxDQUNTLENBQ3pCLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLGFBQWEsQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/info/about/view-give-gift-card-time.tsx




var view_give_gift_card_time_renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("p", null, "T\u1EEB ng\u00E0y 25/12/2017 \u0111\u1EBFn ng\u00E0y 31/01/2018. S\u1ED1 l\u01B0\u1EE3ng giftcard c\u00F3 h\u1EA1n. Ch\u01B0\u01A1ng tr\u00ECnh c\u00F3 th\u1EC3 k\u1EBFt th\u00FAc tr\u01B0\u1EDBc th\u1EDDi h\u1EA1n m\u00E0 kh\u00F4ng c\u1EA7n b\u00E1o tr\u01B0\u1EDBc.")),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_give_gift_card_time = (view_give_gift_card_time_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1naXZlLWdpZnQtY2FyZC10aW1lLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1naXZlLWdpZnQtY2FyZC10aW1lLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUMvQixPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sUUFBUSxDQUFDO0FBRS9CLE9BQU8sVUFBVSxNQUFNLHNCQUFzQixDQUFDO0FBRTlDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFFdkMsSUFBTSxhQUFhLEdBQUc7SUFFcEIsTUFBTSxDQUFDLENBQ0w7UUFDRSxvQkFBQyxVQUFVO1lBQ1QsNkJBQUssU0FBUyxFQUFFLGNBQWM7Z0JBQzVCLDhTQUE0SSxDQUN4STtZQUNOLG9CQUFDLEtBQUssSUFBQyxLQUFLLEVBQUUsWUFBWSxDQUFDLGdCQUFnQixHQUFJLENBQ3BDLENBQ1MsQ0FDekIsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsYUFBYSxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/info/about/view-use-card-time.tsx




var view_use_card_time_renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("p", null, "Th\u1EDDi gian s\u1EED d\u1EE5ng giftcard: t\u1EEB ng\u00E0y 10/02/2018 cho \u0111\u1EBFn ng\u00E0y 20/02/2018."),
                react["createElement"]("p", null, "Qu\u00FD kh\u00E1ch nh\u1EADp m\u00E3 \u0111\u01B0\u1EE3c in tr\u00EAn m\u1ED7i gifcard v\u00E0o b\u01B0\u1EDBc thanh to\u00E1n \u0111\u1EC3 \u0111\u01B0\u1EE3c gi\u1EA3m ti\u1EC1n cho \u0111\u01A1n h\u00E0ng k\u1EBF ti\u1EBFp."),
                react["createElement"]("p", null, "M\u1ED7i m\u1ED9t giftcard ch\u1EC9 \u0111\u01B0\u1EE3c s\u1EED d\u1EE5ng 1 l\u1EA7n duy nh\u1EA5t v\u00E0 \u0111\u01B0\u1EE3c c\u1ED9ng d\u1ED3n nhi\u1EC1u giftcard trong m\u1ED9t \u0111\u01A1n h\u00E0ng."),
                react["createElement"]("p", null, "Giftcard kh\u00F4ng c\u00F3 gi\u00E1 tr\u1ECB quy \u0111\u1ED5i th\u00E0nh ti\u1EC1n m\u1EB7t v\u00E0 kh\u00F4ng ho\u00E0n tr\u1EA3 ti\u1EC1n d\u01B0 khi mua h\u00E0ng ho\u1EB7c \u0111\u1EC3 l\u1EA1i ti\u1EC1n d\u01B0 cho \u0111\u01A1n h\u00E0ng k\u1EBF ti\u1EBFp.")),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_use_card_time = (view_use_card_time_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy11c2UtY2FyZC10aW1lLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy11c2UtY2FyZC10aW1lLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEtBQUssS0FBSyxNQUFNLE9BQU8sQ0FBQztBQUMvQixPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sUUFBUSxDQUFDO0FBRS9CLE9BQU8sVUFBVSxNQUFNLHNCQUFzQixDQUFDO0FBRTlDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFFdkMsSUFBTSxhQUFhLEdBQUc7SUFFcEIsTUFBTSxDQUFDLENBQ0w7UUFDRSxvQkFBQyxVQUFVO1lBQ1QsNkJBQUssU0FBUyxFQUFFLGNBQWM7Z0JBQzVCLGlKQUE4RTtnQkFDOUUscVFBQTZHO2dCQUM3RywrT0FBMkc7Z0JBQzNHLDBTQUF3SSxDQUNwSTtZQUNOLG9CQUFDLEtBQUssSUFBQyxLQUFLLEVBQUUsWUFBWSxDQUFDLGdCQUFnQixHQUFJLENBQ3BDLENBQ1MsQ0FDekIsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsYUFBYSxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/info/about/view-question-about-us.tsx




var view_question_about_us_renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("h2", null, "Lixicoin l\u00E0 g\u00EC?"),
                react["createElement"]("div", null,
                    react["createElement"]("p", null, "Lixicoin l\u00E0 s\u1ED1 \u0111i\u1EC3m t\u00EDch l\u0169y t\u1EEB m\u1ED7i l\u1EA7n mua h\u00E0ng tr\u00EAn website c\u1EE7a Lixibox. Gi\u00E1 tr\u1ECB c\u1EE7a Lixicoin: 1 Lixicoin = 1.000 VND. S\u1ED1 Lixicoin s\u1EBD \u0111\u01B0\u1EE3c c\u1ED9ng v\u00E0o t\u00E0i kho\u1EA3n Lixibox c\u1EE7a b\u1EA1n m\u1ED7i khi \u0111\u01A1n h\u00E0ng c\u1EE7a b\u1EA1n \u0111\u01B0\u1EE3c giao th\u00E0nh c\u00F4ng.")),
                react["createElement"]("h2", null, "L\u00E0m sao \u0111\u1EC3 c\u00F3 Lixicoin?"),
                react["createElement"]("div", null,
                    react["createElement"]("p", null,
                        "C\u00E1ch 1: Mua h\u00E0ng t\u1EEB Lixibox.",
                        react["createElement"]("br", null),
                        "C\u00E1ch 2: Gi\u1EDBi thi\u1EC7u b\u1EA1n b\u00E8 \u0111\u1EC3 \u0111\u01B0\u1EE3c nh\u1EADn 200 lixicoin.",
                        react["createElement"]("br", null),
                        "C\u00E1ch 3: Vi\u1EBFt feedback v\u1EC1 s\u1EA3n ph\u1EA9m \u0111\u00E3 mua \u0111\u1EC3 nh\u1EADn 10 lixicoin cho m\u1ED7i feedback tr\u00EAn web.",
                        react["createElement"]("br", null),
                        "C\u00E1ch 4: Ch\u1EE5p h\u00ECnh v\u00E0 chia s\u1EBB c\u1EA3m nh\u1EADn v\u1EC1 tr\u1EA3i nghi\u1EC7m c\u1EE7a b\u1EA1n v\u1EDBi Lixibox l\u00EAn Facebook, Instagram \u0111\u1EC3 nh\u1EADn 100 Lixicoin, l\u01B0u \u00FD: m\u1ED7i order ch\u1EC9 chia s\u1EBB \u0111\u01B0\u1EE3c 1 l\u1EA7n.")),
                react["createElement"]("h2", null, "Lixicoin d\u00F9ng \u0111\u1EC3 l\u00E0m g\u00EC?"),
                react["createElement"]("div", null,
                    react["createElement"]("p", null, "Lixicoin \u0111\u01B0\u1EE3c s\u1EED d\u1EE5ng \u0111\u1EC3 \u0111\u1ED5i mi\u1EC5n ph\u00ED c\u00E1c s\u1EA3n ph\u1EA9m trong ch\u01B0\u01A1ng tr\u00ECnh \u0110\u1ED5i Lixicoin c\u1EE7a Lixibox, v\u00E0 trong c\u00E1c ch\u01B0\u01A1ng tr\u00ECnh \u01B0u \u0111\u00E3i s\u1EAFp t\u1EDBi c\u1EE7a Lixibox.")),
                react["createElement"]("h2", null, "Ch\u01B0\u01A1ng tr\u00ECnh \u0110\u1ED5i Lixicoin l\u00E0 g\u00EC? (Khi n\u00E0o th\u00EC \u0111\u01B0\u1EE3c \u0111\u1ED5i?)"),
                react["createElement"]("div", null,
                    react["createElement"]("p", null, "\u0110\u00E2y l\u00E0 ch\u01B0\u01A1ng tr\u00ECnh \u0111\u1ED5i Lixicoin \u0111\u00E3 t\u00EDch l\u0169y l\u1EA5y s\u1EA3n ph\u1EA9m v\u1EDBi gi\u00E1 tr\u1ECB t\u01B0\u01A1ng \u1EE9ng (gi\u00E1 tr\u1ECB quy \u0111\u1ED5i do Lixibox quy \u0111\u1ECBnh). Ch\u01B0\u01A1ng tr\u00ECnh di\u1EC5n ra su\u1ED1t n\u0103m n\u00EAn b\u1EA1n c\u00F3 th\u1EC3 \u0111\u1ED5i qu\u00E0 b\u1EAFt c\u1EE9 l\u00FAc n\u00E0o.")),
                react["createElement"]("h2", null, "Bao nhi\u00EAu Lixicoin th\u00EC c\u00F3 th\u1EC3 tham gia ch\u01B0\u01A1ng tr\u00ECnh?"),
                react["createElement"]("div", null,
                    react["createElement"]("p", null, "Ngay sau khi ho\u00E0n th\u00E0nh \u0111\u01A1n h\u00E0ng \u0111\u1EA7u ti\u00EAn, Lixicoin s\u1EBD \u0111\u01B0\u1EE3c c\u1ED9ng v\u00E0o t\u00E0i kho\u1EA3n c\u1EE7a b\u1EA1n \u0111\u1EC3 b\u1EA1n c\u00F3 th\u1EC3 \u0111\u1ED5i qu\u00E0. Lixibox kh\u00F4ng gi\u1EDBi h\u1EA1n s\u1ED1 Lixicoin t\u1ED1i thi\u1EC3u.")),
                react["createElement"]("h2", null, "L\u00E0m th\u1EBF n\u00E0o \u0111\u1EC3 c\u00F3 th\u1EC3 \u0111\u1ED5i Lixicoin?"),
                react["createElement"]("div", null,
                    react["createElement"]("p", null, "B\u01B0\u1EDBc 1: T\u00EDch l\u0169y \u0111\u1EE7 s\u1ED1 Lixicoin t\u1ED1i thi\u1EC3u \u0111\u1EC3 tham gia ch\u01B0\u01A1ng tr\u00ECnh. B\u01B0\u1EDBc 2: Ch\u1ECDn c\u00E1c s\u1EA3n ph\u1EA9m t\u01B0\u01A1ng \u1EE9ng v\u1EDBi s\u1ED1 Lixicoin c\u00F3 \u0111\u01B0\u1EE3c. B\u01B0\u1EDBc 3: L\u00E0m c\u00E1c thao t\u00E1c nh\u01B0 khi mua s\u1EA3n ph\u1EA9m (b\u1ECF v\u00E0o gi\u1ECF h\u00E0ng, thanh to\u00E1n s\u1EED d\u1EE5ng Lixicoin) Sau khi \u0111\u1ED5i, s\u1ED1 l\u01B0\u1EE3ng Lixicoin c\u1EE7a b\u1EA1n s\u1EBD gi\u1EA3m \u0111i theo s\u1ED1 l\u01B0\u1EE3ng coin t\u01B0\u01A1ng \u1EE9ng v\u1EDBi s\u1EA3n ph\u1EA9m. C\u1EA5p \u0111\u1ED9 th\u00E0nh vi\u00EAn v\u1EABn \u0111\u01B0\u1EE3c gi\u1EEF nguy\u00EAn nh\u01B0 tr\u01B0\u1EDBc khi \u0111\u1ED5i s\u1EA3n ph\u1EA9m.")),
                react["createElement"]("h2", null, "S\u1EA3n ph\u1EA9m \u0111\u1ED5i Lixicoin l\u00E0 g\u00EC?"),
                react["createElement"]("div", null,
                    react["createElement"]("p", null, "S\u1EA3n ph\u1EA9m \u0111\u1EC3 \u0111\u1ED5i Lixicoin l\u00E0 s\u1EA3n ph\u1EA9m d\u01B0\u1EE1ng da, makeup full size v\u00E0 mini size m\u1EDBi, thu\u1ED9c c\u00E1c nh\u00E3n h\u00E0ng cao c\u1EA5p \u0111\u01B0\u1EE3c Lixibox tuy\u1EC3n ch\u1ECDn.")),
                react["createElement"]("h2", null, "S\u1ED1 l\u01B0\u1EE3ng s\u1EA3n ph\u1EA9m c\u00F3 th\u1EC3 \u0111\u1ED5i l\u00E0 bao nhi\u00EAu?"),
                react["createElement"]("div", null,
                    react["createElement"]("p", null, "M\u1ED7i t\u00E0i kho\u1EA3n c\u00F3 th\u1EC3 \u0111\u1ED5i \u0111\u01B0\u1EE3c nhi\u1EC1u m\u00F3n s\u1EA3n ph\u1EA9m kh\u00E1c nhau tu\u1EF3 v\u00E0o s\u1ED1 Lixicoin t\u00EDch lu\u1EF9 \u0111\u01B0\u1EE3c. Tuy nhi\u00EAn m\u1ED9t s\u1EA3n ph\u1EA9m ch\u1EC9 \u0111\u1ED5i \u0111\u01B0\u1EE3c m\u1ED9t l\u1EA7n do s\u1ED1 l\u01B0\u1EE3ng c\u00F3 h\u1EA1n. S\u1ED1 l\u01B0\u1EE3ng s\u1EA3n ph\u1EA9m c\u00F3 gi\u1EDBi h\u1EA1n, n\u00EAn h\u1EC7 th\u1ED1ng s\u1EBD \u01B0u ti\u00EAn cho c\u00E1c kh\u00E1ch h\u00E0ng \u0111\u1ED5i s\u1EA3n ph\u1EA9m s\u1EDBm.")),
                react["createElement"]("h2", null, "M\u00ECnh c\u00F3 th\u1EC3 chuy\u1EC3n coin gi\u1EEFa c\u00E1c t\u00E0i kho\u1EA3n c\u00F9ng ch\u1EE7 th\u1EC3 v\u1EDBi nhau kh\u00F4ng?"),
                react["createElement"]("div", null,
                    react["createElement"]("p", null, "Hi\u1EC7n t\u1EA1i Lixibox ch\u01B0a cung c\u1EA5p t\u00EDnh n\u0103ng trao \u0111\u1ED5i coin gi\u1EEFa c\u00E1c t\u00E0i kho\u1EA3n.")),
                react["createElement"]("h2", null, "Sau khi \u0111\u1ED5i coin th\u00EC c\u1EA5p \u0111\u1ED9 th\u00E0nh vi\u00EAn c\u00F2n \u0111\u01B0\u1EE3c gi\u1EEF nguy\u00EAn kh\u00F4ng?"),
                react["createElement"]("div", null,
                    react["createElement"]("p", null, "Sau khi \u0111\u1ED5i th\u00EC c\u1EA5p \u0111\u1ED9 th\u00E0nh vi\u00EAn c\u1EE7a b\u1EA1n s\u1EBD v\u1EABn \u0111\u01B0\u1EE3c gi\u1EEF nguy\u00EAn.")),
                react["createElement"]("h2", null, "S\u1EA3n ph\u1EA9m sau khi \u0111\u1ED5i t\u1EEB Lixicoin c\u00F3 th\u1EC3 tr\u1EA3 l\u1EA1i kh\u00F4ng?"),
                react["createElement"]("div", null,
                    react["createElement"]("p", null, "S\u1EA3n ph\u1EA9m sau khi \u0111\u1ED5i s\u1EBD kh\u00F4ng th\u1EC3 ho\u00E0n tr\u1EA3."))),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_question_about_us = (view_question_about_us_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1xdWVzdGlvbi1hYm91dC11cy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXctcXVlc3Rpb24tYWJvdXQtdXMudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFL0IsT0FBTyxVQUFVLE1BQU0sc0JBQXNCLENBQUM7QUFFOUMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUV2QyxJQUFNLGFBQWEsR0FBRztJQUVwQixNQUFNLENBQUMsQ0FDTDtRQUNFLG9CQUFDLFVBQVU7WUFDVCw2QkFBSyxTQUFTLEVBQUUsY0FBYztnQkFDNUIsNERBQXdCO2dCQUN4QjtvQkFDRSx5YkFJSSxDQUNBO2dCQUNOLDhFQUFnQztnQkFDaEM7b0JBQ0U7O3dCQUM4QiwrQkFBTTs7d0JBQ2tCLCtCQUFNOzt3QkFDNkIsK0JBQU07NFRBRTNGLENBQ0E7Z0JBQ04sb0ZBQWlDO2dCQUNqQztvQkFDRSxrVkFFSSxDQUNBO2dCQUNOLGlLQUFpRTtnQkFDakU7b0JBQ0UseWJBR0ksQ0FDQTtnQkFDTiwwSEFBNkQ7Z0JBQzdEO29CQUNFLDZWQUVJLENBQ0E7Z0JBQ04sbUhBQTRDO2dCQUM1QztvQkFDRSxtekJBTUksQ0FDQTtnQkFDTiw2RkFBcUM7Z0JBQ3JDO29CQUNFLDJSQUVJLENBQ0E7Z0JBQ04sb0lBQW1EO2dCQUNuRDtvQkFDRSxpbEJBR0ksQ0FDQTtnQkFDTiwyS0FBZ0Y7Z0JBQ2hGO29CQUNFLHdLQUVJLENBQ0E7Z0JBQ04sK0tBQTBFO2dCQUMxRTtvQkFDRSx3TEFFSSxDQUNBO2dCQUNOLDJJQUErRDtnQkFDL0Q7b0JBQ0UsMEhBRUksQ0FDQSxDQUNGO1lBQ04sb0JBQUMsS0FBSyxJQUFDLEtBQUssRUFBRSxZQUFZLENBQUMsZ0JBQWdCLEdBQUksQ0FDcEMsQ0FDUyxDQUN6QixDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsZUFBZSxhQUFhLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/info/about/view-question-receive-gift.tsx




var view_question_receive_gift_renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("h2", null, "Chia s\u1EBB nh\u01B0 th\u1EBF n\u00E0o?"),
                react["createElement"]("div", { className: 'answer' },
                    "Ch\u1EE5p h\u00ECnh, vi\u1EBFt c\u1EA3m nh\u1EADn v\u1EC1 s\u1EA3n ph\u1EA9m v\u00E0 chia s\u1EBB l\u00EAn Facebook, Instagram \u1EDF ch\u1EBF \u0111\u1ED9 public. Copy \u0111\u01B0\u1EDDng d\u1EABn v\u1EEBa chia s\u1EBB v\u00E0 truy c\u1EADp ",
                    react["createElement"]("a", { href: 'http://lxbtest.tk/loves/' }, "https:// www.lixibox.com/loves/"),
                    " \u0111\u1EC3 d\u00E1n v\u00E0o, sau \u0111\u00F3 b\u1EA5m  X\u00E1c nh\u1EADn.",
                    react["createElement"]("p", null, " L\u01B0u \u00FD:"),
                    "Lixibox ch\u1EC9 x\u00E1c nh\u1EADn nh\u1EEFng chia s\u1EBB \u1EDF ch\u1EBF \u0111\u1ED9 public Ch\u1EC9 ch\u1EA5p nh\u1EADn nh\u1EEFng li\u00EAn k\u1EBFt chia s\u1EBB c\u00F3 c\u1EA3 h\u00ECnh \u1EA3nh v\u00E0 c\u1EA3m nh\u1EADn c\u1EE7a b\u1EA1n. S\u1ED1 l\u1EA7n t\u1ED1i \u0111a chia s\u1EBB d\u1EF1a tr\u00EAn s\u1ED1 \u0111\u01A1n h\u00E0ng b\u1EA1n \u0111\u00E3 thanh to\u00E1n."),
                react["createElement"]("h2", null, "Chia s\u1EBB c\u1EA3m nh\u1EADn s\u1EBD \u0111\u01B0\u1EE3c \u01B0u \u0111\u00E3i g\u00EC?"),
                react["createElement"]("div", { className: 'answer' },
                    "V\u1EDBi m\u1ED7i h\u00ECnh ch\u1EE5p s\u1EA3n ph\u1EA9m k\u00E8m #Lixibox, nh\u1EADn ngay 100 lixicoins. Ch\u1EE5p \u1EA3nh selfie v\u1EDBi s\u1EA3n ph\u1EA9m ho\u1EB7c box k\u00E8m #Lixibox v\u00E0 #LixiSelfie, nh\u1EADn ngay 200 lixicoins. Quay clip \u0111\u1EADp h\u1ED9p k\u00E8m #Lixibox nh\u1EADn ngay 200 lixicoins.",
                    react["createElement"]("p", null,
                        "Lixicoins c\u00F3 th\u1EC3 \u0111\u01B0\u1EE3c s\u1EED d\u1EE5ng \u0111\u1EC3 \u0111\u1ED5i qu\u00E0 t\u1EA1i ",
                        react["createElement"]("a", { href: 'http://lxbtest.tk/redeem' }, "\u0110\u00E2y"),
                        ". T\u00ECm hi\u1EC3u th\u00EAm v\u1EC1 ",
                        react["createElement"]("a", { href: 'http://lxbtest.tk/lixicoin' }, " Lixicoins"),
                        "."))),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_question_receive_gift = (view_question_receive_gift_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1xdWVzdGlvbi1yZWNlaXZlLWdpZnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LXF1ZXN0aW9uLXJlY2VpdmUtZ2lmdC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUUvQixPQUFPLFVBQVUsTUFBTSxzQkFBc0IsQ0FBQztBQUU5QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBRXZDLElBQU0sYUFBYSxHQUFHO0lBRXBCLE1BQU0sQ0FBQyxDQUNMO1FBQ0Usb0JBQUMsVUFBVTtZQUNULDZCQUFLLFNBQVMsRUFBRSxjQUFjO2dCQUM1QiwyRUFBNkI7Z0JBQzdCLDZCQUFLLFNBQVMsRUFBRSxRQUFROztvQkFFaUIsMkJBQUcsSUFBSSxFQUFFLGdDQUFnQyxzQ0FBcUM7O29CQUNySCxtREFBYzt3WkFJVjtnQkFDTiw2SEFBNEM7Z0JBQzVDLDZCQUFLLFNBQVMsRUFBRSxRQUFROztvQkFJdEI7O3dCQUFnRCwyQkFBRyxJQUFJLEVBQUUsZ0NBQWdDLG9CQUFTOzt3QkFBbUIsMkJBQUcsSUFBSSxFQUFFLGtDQUFrQyxpQkFBZ0I7NEJBQUssQ0FBTSxDQUN6TDtZQUNOLG9CQUFDLEtBQUssSUFBQyxLQUFLLEVBQUUsWUFBWSxDQUFDLGdCQUFnQixHQUFJLENBQ3BDLENBQ1MsQ0FDekIsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsYUFBYSxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/info/about/view-question-invite-friends-get-rewards.tsx




var view_question_invite_friends_get_rewards_renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("h2", null, "C\u00E1ch m\u1EDDi b\u1EA1n b\u00E8 nh\u01B0 th\u1EBF n\u00E0o?"),
                react["createElement"]("div", { className: 'answer' },
                    react["createElement"]("p", null, "\u0110\u00E2y l\u00E0 ch\u01B0\u01A1ng tr\u00ECnh m\u1EDDi b\u1EA1n b\u00E8 mua h\u00E0ng tr\u00EAn Lixibox, c\u1EA3 hai b\u1EA1n s\u1EBD nh\u1EADn \u0111\u01B0\u1EE3c \u01B0u \u0111\u00E3i \u0111\u1EB7c bi\u1EC7t."),
                    react["createElement"]("p", null,
                        "1. \u0110\u0103ng nh\u1EADp v\u00E0o ",
                        react["createElement"]("a", { href: 'http://lxbtest.tk/' }, "Lixibox"),
                        ", sau \u0111\u00F3 truy c\u1EADp ",
                        react["createElement"]("a", { href: 'http://lxbtest.tk/invite' }, "https:// www.lixibox.com/invite"),
                        ", trong \u0111\u00F3 s\u1EBD c\u00F3 m\u1ED9t \u0111\u01B0\u1EDDng link c\u1EE7a b\u1EA1n. M\u1ED7i user c\u00F3 m\u1ED9t \u0111\u01B0\u1EDDng link ri\u00EAng."),
                    react["createElement"]("p", null, "2. G\u1EDFi link \u0111\u00F3 cho b\u1EA1n b\u00E8 v\u00E0 m\u1EDDi h\u1ECD nh\u1EA5p v\u00E0o link \u0111\u1EC3 \u0111\u0103ng k\u00ED th\u00E0nh vi\u00EAn, mua h\u00E0ng \u1EDF Lixibox.")),
                react["createElement"]("h2", null, "M\u1EDDi b\u1EA1n b\u00E8 th\u00EC \u0111\u01B0\u1EE3c \u01B0u \u0111\u00E3i g\u00EC?"),
                react["createElement"]("div", { className: 'answer' },
                    react["createElement"]("p", null, "V\u1EDBi m\u1ED7i h\u00ECnh ch\u1EE5p s\u1EA3n ph\u1EA9m k\u00E8m #Lixibox, nh\u1EADn ngay 100 lixicoins Ch\u1EE5p \u1EA3nh selfie v\u1EDBi s\u1EA3n ph\u1EA9m ho\u1EB7c box k\u00E8m #Lixibox v\u00E0 #LixiSelfie, nh\u1EADn ngay 200 lixicoins Quay clip \u0111\u1EADp h\u1ED9p k\u00E8m #Lixibox nh\u1EADn ngay 200 lixicoins"),
                    react["createElement"]("p", null,
                        "Lixicoins c\u00F3 th\u1EC3 \u0111\u01B0\u1EE3c s\u1EED d\u1EE5ng \u0111\u1EC3 \u0111\u1ED5i qu\u00E0 t\u1EA1i ",
                        react["createElement"]("a", { href: 'http://lxbtest.tk/redeem' }, "\u0110\u00E2y"),
                        ". T\u00ECm hi\u1EC3u th\u00EAm v\u1EC1 ",
                        react["createElement"]("a", { href: 'http://lxbtest.tk/lixicoin' }, "Lixicoins"),
                        "."))),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_question_invite_friends_get_rewards = (view_question_invite_friends_get_rewards_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1xdWVzdGlvbi1pbnZpdGUtZnJpZW5kcy1nZXQtcmV3YXJkcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXctcXVlc3Rpb24taW52aXRlLWZyaWVuZHMtZ2V0LXJld2FyZHMudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRS9CLE9BQU8sVUFBVSxNQUFNLHNCQUFzQixDQUFDO0FBQzlDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFL0IsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUV2QyxJQUFNLGFBQWEsR0FBRztJQUVwQixNQUFNLENBQUMsQ0FDTDtRQUNFLG9CQUFDLFVBQVU7WUFDVCw2QkFBSyxTQUFTLEVBQUUsY0FBYztnQkFDNUIsa0dBQXFDO2dCQUNyQyw2QkFBSyxTQUFTLEVBQUUsUUFBUTtvQkFDdEIsd1BBQXFHO29CQUNyRzs7d0JBQW9CLDJCQUFHLElBQUksRUFBRSwwQkFBMEIsY0FBYTs7d0JBQWtCLDJCQUFHLElBQUksRUFBRSxnQ0FBZ0Msc0NBQXFDOzBMQUE4RTtvQkFDbFAsNk5BQW1HLENBQy9GO2dCQUNOLHdIQUF1QztnQkFDdkMsNkJBQUssU0FBUyxFQUFFLFFBQVE7b0JBQ3RCLGtXQUUwRDtvQkFDMUQ7O3dCQUFnRCwyQkFBRyxJQUFJLEVBQUUsZ0NBQWdDLG9CQUFTOzt3QkFBbUIsMkJBQUcsSUFBSSxFQUFFLGtDQUFrQyxnQkFBZTs0QkFBSyxDQUFNLENBQ3hMO1lBQ04sb0JBQUMsS0FBSyxJQUFDLEtBQUssRUFBRSxZQUFZLENBQUMsZ0JBQWdCLEdBQUksQ0FDcEMsQ0FDUyxDQUN6QixDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsZUFBZSxhQUFhLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/info/about/view-question-gift-card-2019.tsx




var view_question_gift_card_2019_renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("h2", null, "TH\u1EC2 L\u1EC6 CH\u01AF\u01A0NG TR\u00CCNH T\u1EB6NG GIFTCARD CH\u00C0O XU\u00C2N 2019 "),
                react["createElement"]("h2", null, "1/ N\u1ED9i dung: "),
                react["createElement"]("div", { className: 'answer' },
                    react["createElement"]("p", null, "- V\u1EDBi \u0111\u01A1n h\u00E0ng t\u1EEB 500.000\u0110 tr\u1EDF l\u00EAn, qu\u00FD kh\u00E1ch nh\u1EADp m\u00E3 LIXITET100 \u0111\u1EC3 \u0111\u01B0\u1EE3c t\u1EB7ng 01 Giftcard tr\u1ECB gi\u00E1 100.000\u0110 v\u00E0o gi\u1ECF h\u00E0ng. "),
                    react["createElement"]("p", null, "- V\u1EDBi \u0111\u01A1n h\u00E0ng t\u1EEB 1.000.000\u0110 tr\u1EDF l\u00EAn, qu\u00FD kh\u00E1ch nh\u1EADp m\u00E3 LIXITET250 \u0111\u1EC3 \u0111\u01B0\u1EE3c t\u1EB7ng 01 Giftcard tr\u1ECB gi\u00E1 250.000\u0110 v\u00E0o gi\u1ECF h\u00E0ng "),
                    react["createElement"]("p", null, "- V\u1EDBi \u0111\u01A1n h\u00E0ng t\u1EEB 1.500.000\u0110 tr\u1EDF l\u00EAn, qu\u00FD kh\u00E1ch nh\u1EADp m\u00E3 LIXITET400 \u0111\u1EC3 \u0111\u01B0\u1EE3c t\u1EB7ng 01 Giftcard tr\u1ECB gi\u00E1 400.000\u0110 v\u00E0o gi\u1ECF h\u00E0ng "),
                    react["createElement"]("p", null, "M\u00E3 t\u1EB7ng Giftcard \u00E1p d\u1EE5ng cho c\u1EA3 c\u00E1c s\u1EA3n ph\u1EA9m \u0111ang c\u00F3 gi\u00E1 khuy\u1EBFn m\u00E3i nh\u01B0ng kh\u00F4ng \u00E1p d\u1EE5ng cho c\u00E1c combo set s\u1EA3n ph\u1EA9m gi\u00E1 \u01B0u \u0111\u00E3i.  "))),
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("h2", null, "2/ Th\u1EDDi gian t\u1EB7ng gift card:"),
                react["createElement"]("div", { className: 'answer' },
                    react["createElement"]("p", null, "T\u1EEB ng\u00E0y 17/01/2019 \u0111\u1EBFn ng\u00E0y 27/01/2018. S\u1ED1 l\u01B0\u1EE3ng giftcard c\u00F3 h\u1EA1n. Ch\u01B0\u01A1ng tr\u00ECnh c\u00F3 th\u1EC3 k\u1EBFt th\u00FAc tr\u01B0\u1EDBc th\u1EDDi h\u1EA1n m\u00E0 kh\u00F4ng c\u1EA7n b\u00E1o tr\u01B0\u1EDBc. "))),
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("h2", null, "3/ C\u00E1ch s\u1EED d\u1EE5ng giftcard: "),
                react["createElement"]("div", { className: 'answer' },
                    react["createElement"]("p", null, "- Th\u1EDDi gian s\u1EED d\u1EE5ng giftcard: t\u1EEB ng\u00E0y 2/2/2019 - 10/2/2019 "),
                    react["createElement"]("p", null, "- M\u00E3 giftcard \u00E1p d\u1EE5ng cho to\u00E0n b\u1ED9 s\u1EA3n ph\u1EA9m c\u1EE7a c\u00E1c brand Halio, Okame, Lustre v\u00E0 Lixibox. "),
                    react["createElement"]("p", null, "- Qu\u00FD kh\u00E1ch nh\u1EADp m\u00E3 \u0111\u01B0\u1EE3c in tr\u00EAn m\u1ED7i giftcard t\u1EA1i b\u01B0\u1EDBc thanh to\u00E1n \u0111\u1EC3 \u0111\u01B0\u1EE3c gi\u1EA3m ti\u1EC1n cho \u0111\u01A1n h\u00E0ng k\u1EBF ti\u1EBFp. "),
                    react["createElement"]("p", null, "- \u00C1p d\u1EE5ng \u0111\u01B0\u1EE3c cho c\u00E1c s\u1EA3n ph\u1EA9m \u0111ang sale. Kh\u00F4ng \u00E1p d\u1EE5ng cho beauty box."),
                    react["createElement"]("p", null, "- M\u1ED7i m\u1ED9t giftcard ch\u1EC9 \u0111\u01B0\u1EE3c s\u1EED d\u1EE5ng 1 l\u1EA7n duy nh\u1EA5t v\u00E0 kh\u00F4ng \u0111\u01B0\u1EE3c c\u1ED9ng d\u1ED3n nhi\u1EC1u giftcard trong m\u1ED9t \u0111\u01A1n h\u00E0ng. "),
                    react["createElement"]("p", null, "- Giftcard kh\u00F4ng c\u00F3 gi\u00E1 tr\u1ECB quy \u0111\u1ED5i th\u00E0nh ti\u1EC1n m\u1EB7t v\u00E0 kh\u00F4ng ho\u00E0n tr\u1EA3 ti\u1EC1n d\u01B0 khi mua h\u00E0ng ho\u1EB7c \u0111\u1EC3 l\u1EA1i ti\u1EC1n d\u01B0 cho \u0111\u01A1n h\u00E0ng k\u1EBF ti\u1EBFp.   "),
                    react["createElement"]("p", null, "M\u1ECDi v\u1EA5n \u0111\u1EC1 li\u00EAn quan, m\u1EDDi Qu\u00FD kh\u00E1ch chat v\u1EDBi T\u01B0 v\u1EA5n vi\u00EAn / ho\u1EB7c Hotline 1800 2040 "),
                    react["createElement"]("p", null))),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_question_gift_card_2019 = (view_question_gift_card_2019_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1xdWVzdGlvbi1naWZ0LWNhcmQtMjAxOS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXctcXVlc3Rpb24tZ2lmdC1jYXJkLTIwMTkudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRS9CLE9BQU8sVUFBVSxNQUFNLHNCQUFzQixDQUFDO0FBQzlDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFL0IsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUV2QyxJQUFNLGFBQWEsR0FBRztJQUVwQixNQUFNLENBQUMsQ0FDTDtRQUNFLG9CQUFDLFVBQVU7WUFDVCw2QkFBSyxTQUFTLEVBQUUsY0FBYztnQkFDNUIsNEhBQTBEO2dCQUMxRCxxREFBc0I7Z0JBQ3RCLDZCQUFLLFNBQVMsRUFBRSxRQUFRO29CQUN0QixtUkFBZ0k7b0JBQ2hJLG9SQUFpSTtvQkFDakksb1JBQWlJO29CQUNqSSwwUkFBdUksQ0FDbkksQ0FDRjtZQUNOLDZCQUFLLFNBQVMsRUFBRSxjQUFjO2dCQUM1Qix5RUFBcUM7Z0JBQ3JDLDZCQUFLLFNBQVMsRUFBRSxRQUFRO29CQUN0QiwrU0FBNkksQ0FDekksQ0FDRjtZQUNOLDZCQUFLLFNBQVMsRUFBRSxjQUFjO2dCQUM1Qiw0RUFBbUM7Z0JBQ25DLDZCQUFLLFNBQVMsRUFBRSxRQUFRO29CQUN0QixzSEFBa0U7b0JBQ2xFLDhLQUFpRztvQkFDakcseVFBQWlIO29CQUNqSCxzS0FBK0U7b0JBQy9FLDZQQUFvSDtvQkFDcEgsK1NBQTZJO29CQUM3SSxxTEFBeUY7b0JBQ3pGLDhCQUFPLENBQ0gsQ0FDRjtZQUNOLG9CQUFDLEtBQUssSUFBQyxLQUFLLEVBQUUsWUFBWSxDQUFDLGdCQUFnQixHQUFJLENBQ3BDLENBQ1MsQ0FDekIsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsYUFBYSxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/info/about/view-makeover.tsx




var view_makeover_renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("p", null, "\u0110ang c\u1EADp nh\u1EADt...")),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_makeover = (view_makeover_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1tYWtlb3Zlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXctbWFrZW92ZXIudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFL0IsT0FBTyxVQUFVLE1BQU0sc0JBQXNCLENBQUM7QUFFOUMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUV2QyxJQUFNLGFBQWEsR0FBRztJQUVwQixNQUFNLENBQUMsQ0FDTDtRQUNFLG9CQUFDLFVBQVU7WUFDVCw2QkFBSyxTQUFTLEVBQUUsY0FBYztnQkFDNUIsaUVBQXVCLENBQ25CO1lBQ04sb0JBQUMsS0FBSyxJQUFDLEtBQUssRUFBRSxZQUFZLENBQUMsZ0JBQWdCLEdBQUksQ0FDcEMsQ0FDUyxDQUN6QixDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsZUFBZSxhQUFhLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/info/about/view-mask-bar.tsx




var view_mask_bar_renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("p", null, "\u0110ang c\u1EADp nh\u1EADt...")),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_mask_bar = (view_mask_bar_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1tYXNrLWJhci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXctbWFzay1iYXIudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFL0IsT0FBTyxVQUFVLE1BQU0sc0JBQXNCLENBQUM7QUFFOUMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUV2QyxJQUFNLGFBQWEsR0FBRztJQUVwQixNQUFNLENBQUMsQ0FDTDtRQUNFLG9CQUFDLFVBQVU7WUFDVCw2QkFBSyxTQUFTLEVBQUUsY0FBYztnQkFDNUIsaUVBQXVCLENBQ25CO1lBQ04sb0JBQUMsS0FBSyxJQUFDLEtBQUssRUFBRSxZQUFZLENBQUMsZ0JBQWdCLEdBQUksQ0FDcEMsQ0FDUyxDQUN6QixDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsZUFBZSxhQUFhLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/info/about/view-skin-test.tsx




var view_skin_test_renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("p", null, "\u0110ang c\u1EADp nh\u1EADt...")),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_skin_test = (view_skin_test_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1za2luLXRlc3QuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LXNraW4tdGVzdC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUUvQixPQUFPLFVBQVUsTUFBTSxzQkFBc0IsQ0FBQztBQUU5QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBRXZDLElBQU0sYUFBYSxHQUFHO0lBRXBCLE1BQU0sQ0FBQyxDQUNMO1FBQ0Usb0JBQUMsVUFBVTtZQUNULDZCQUFLLFNBQVMsRUFBRSxjQUFjO2dCQUM1QixpRUFBdUIsQ0FDbkI7WUFDTixvQkFBQyxLQUFLLElBQUMsS0FBSyxFQUFFLFlBQVksQ0FBQyxnQkFBZ0IsR0FBSSxDQUNwQyxDQUNTLENBQ3pCLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLGFBQWEsQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/info/about/view-recommend.tsx




var view_recommend_renderDesktop = function () {
    return (react["createElement"]("info-detail-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { className: 'info-content' },
                react["createElement"]("p", null, "\u0110ang c\u1EADp nh\u1EADt...")),
            react["createElement"](radium["Style"], { rules: style["a" /* INLINE_STYLE */].infoContentGroup }))));
};
/* harmony default export */ var view_recommend = (view_recommend_renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1yZWNvbW1lbmQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3LXJlY29tbWVuZC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUUvQixPQUFPLFVBQVUsTUFBTSxzQkFBc0IsQ0FBQztBQUU5QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBRXZDLElBQU0sYUFBYSxHQUFHO0lBRXBCLE1BQU0sQ0FBQyxDQUNMO1FBQ0Usb0JBQUMsVUFBVTtZQUNULDZCQUFLLFNBQVMsRUFBRSxjQUFjO2dCQUM1QixpRUFBdUIsQ0FDbkI7WUFDTixvQkFBQyxLQUFLLElBQUMsS0FBSyxFQUFFLFlBQVksQ0FBQyxnQkFBZ0IsR0FBSSxDQUNwQyxDQUNTLENBQ3pCLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLGFBQWEsQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/info/about/view.tsx























var renderView = function (props) {
    var location = props.location;
    var routeLink = location.pathname;
    switch (routeLink) {
        case routing["H" /* ROUTING_INFO_ABOUT_US */]:
            return Object(view_about_us["a" /* default */])();
        case routing["Da" /* ROUTING_INFO_TERM */]:
            return Object(view_term["a" /* default */])();
        case routing["S" /* ROUTING_INFO_PRIVACY */]:
            return view_privacy();
        case routing["K" /* ROUTING_INFO_CAREERS */]:
            return Object(view_about_us["a" /* default */])();
        case routing["N" /* ROUTING_INFO_GIVE_GIFT_CARD */]:
            return view_give_gift_card();
        case routing["I" /* ROUTING_INFO_BUY_ON_APP */]:
            return view_buy_on_app();
        case routing["J" /* ROUTING_INFO_BUY_ON_WEB */]:
            return view_buy_on_web();
        case routing["Ba" /* ROUTING_INFO_SHIPPING_FEE */]:
            return view_shipping_fee();
        case routing["M" /* ROUTING_INFO_DELIVERY_AND_PAYMENT */]:
            return view_delivery_and_payment();
        case routing["Z" /* ROUTING_INFO_RECEIVE_TIME */]:
            return view_receive_time();
        case routing["Y" /* ROUTING_INFO_RECEIVE_AND_REDEEM */]:
            return view_receive_and_redeem();
        case routing["P" /* ROUTING_INFO_GUARANTEE */]:
            return view_guarantee();
        case routing["L" /* ROUTING_INFO_CONTENT */]:
            return view_content();
        case routing["O" /* ROUTING_INFO_GIVE_GIFT_CARD_TIME */]:
            return view_give_gift_card_time();
        case routing["Ea" /* ROUTING_INFO_USE_CARD_TIME */]:
            return view_use_card_time();
        case routing["U" /* ROUTING_INFO_QUESTION_ABOUT_US */]:
            return view_question_about_us();
        case routing["X" /* ROUTING_INFO_QUESTION_RECEIVE_GIFT */]:
            return view_question_receive_gift();
        case routing["W" /* ROUTING_INFO_QUESTION_INVITE_FRIENDS_GET_REWARDS */]:
            return view_question_invite_friends_get_rewards();
        case routing["V" /* ROUTING_INFO_QUESTION_GIFT_CARD_2019 */]:
            return view_question_gift_card_2019();
        case routing["Q" /* ROUTING_INFO_MAKEOVER */]:
            return view_makeover();
        case routing["R" /* ROUTING_INFO_MASK_BAR */]:
            return view_mask_bar();
        case routing["Ca" /* ROUTING_INFO_SKIN_TEST */]:
            return view_skin_test();
        case routing["Aa" /* ROUTING_INFO_RECOMMEND */]:
            return view_recommend();
        default: return Object(view_about_us["a" /* default */])();
    }
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFDTCxxQkFBcUIsRUFDckIsaUJBQWlCLEVBQ2pCLG9CQUFvQixFQUNwQixvQkFBb0IsRUFDcEIsMkJBQTJCLEVBQzNCLHVCQUF1QixFQUN2Qix1QkFBdUIsRUFDdkIseUJBQXlCLEVBQ3pCLGlDQUFpQyxFQUNqQyx5QkFBeUIsRUFDekIsK0JBQStCLEVBQy9CLHNCQUFzQixFQUN0QixvQkFBb0IsRUFDcEIsZ0NBQWdDLEVBQ2hDLDBCQUEwQixFQUMxQiw4QkFBOEIsRUFDOUIsa0NBQWtDLEVBQ2xDLGdEQUFnRCxFQUNoRCxvQ0FBb0MsRUFDcEMscUJBQXFCLEVBQ3JCLHFCQUFxQixFQUNyQixzQkFBc0IsRUFDdEIsc0JBQXNCLEVBQ3ZCLE1BQU0sMkNBQTJDLENBQUM7QUFHbkQsT0FBTyxhQUFhLE1BQU0saUJBQWlCLENBQUM7QUFDNUMsT0FBTyxVQUFVLE1BQU0sYUFBYSxDQUFDO0FBQ3JDLE9BQU8sYUFBYSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sa0JBQWtCLE1BQU0sdUJBQXVCLENBQUM7QUFDdkQsT0FBTyxjQUFjLE1BQU0sbUJBQW1CLENBQUM7QUFDL0MsT0FBTyxjQUFjLE1BQU0sbUJBQW1CLENBQUM7QUFDL0MsT0FBTyxpQkFBaUIsTUFBTSxxQkFBcUIsQ0FBQztBQUNwRCxPQUFPLHdCQUF3QixNQUFNLDZCQUE2QixDQUFDO0FBQ25FLE9BQU8saUJBQWlCLE1BQU0scUJBQXFCLENBQUM7QUFDcEQsT0FBTyxzQkFBc0IsTUFBTSwyQkFBMkIsQ0FBQztBQUMvRCxPQUFPLGVBQWUsTUFBTSxrQkFBa0IsQ0FBQztBQUMvQyxPQUFPLGFBQWEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLHNCQUFzQixNQUFNLDRCQUE0QixDQUFDO0FBQ2hFLE9BQU8saUJBQWlCLE1BQU0sc0JBQXNCLENBQUM7QUFDckQsT0FBTyxxQkFBcUIsTUFBTSwwQkFBMEIsQ0FBQztBQUM3RCxPQUFPLHlCQUF5QixNQUFNLDhCQUE4QixDQUFDO0FBQ3JFLE9BQU8scUNBQXFDLE1BQU0sNENBQTRDLENBQUM7QUFDL0YsT0FBTywwQkFBMEIsTUFBTSxnQ0FBZ0MsQ0FBQztBQUN4RSxPQUFPLGNBQWMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLGFBQWEsTUFBTSxpQkFBaUIsQ0FBQztBQUM1QyxPQUFPLGNBQWMsTUFBTSxrQkFBa0IsQ0FBQztBQUM5QyxPQUFPLGVBQWUsTUFBTSxrQkFBa0IsQ0FBQztBQUcvQyxJQUFNLFVBQVUsR0FBRyxVQUFDLEtBQWE7SUFDdkIsSUFBQSx5QkFBUSxDQUFXO0lBQzNCLElBQU0sU0FBUyxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUM7SUFFcEMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUNsQixLQUFLLHFCQUFxQjtZQUN4QixNQUFNLENBQUMsYUFBYSxFQUFFLENBQUM7UUFFekIsS0FBSyxpQkFBaUI7WUFDcEIsTUFBTSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBRXRCLEtBQUssb0JBQW9CO1lBQ3ZCLE1BQU0sQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUV6QixLQUFLLG9CQUFvQjtZQUN2QixNQUFNLENBQUMsYUFBYSxFQUFFLENBQUM7UUFFekIsS0FBSywyQkFBMkI7WUFDOUIsTUFBTSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFFOUIsS0FBSyx1QkFBdUI7WUFDMUIsTUFBTSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRTFCLEtBQUssdUJBQXVCO1lBQzFCLE1BQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUUxQixLQUFLLHlCQUF5QjtZQUM1QixNQUFNLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUU3QixLQUFLLGlDQUFpQztZQUNwQyxNQUFNLENBQUMsd0JBQXdCLEVBQUUsQ0FBQztRQUVwQyxLQUFLLHlCQUF5QjtZQUM1QixNQUFNLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUU3QixLQUFLLCtCQUErQjtZQUNsQyxNQUFNLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUVsQyxLQUFLLHNCQUFzQjtZQUN6QixNQUFNLENBQUMsZUFBZSxFQUFFLENBQUM7UUFFM0IsS0FBSyxvQkFBb0I7WUFDdkIsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBRXpCLEtBQUssZ0NBQWdDO1lBQ25DLE1BQU0sQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBRWxDLEtBQUssMEJBQTBCO1lBQzdCLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBRTdCLEtBQUssOEJBQThCO1lBQ2pDLE1BQU0sQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1FBRWpDLEtBQUssa0NBQWtDO1lBQ3JDLE1BQU0sQ0FBQyx5QkFBeUIsRUFBRSxDQUFDO1FBRXJDLEtBQUssZ0RBQWdEO1lBQ25ELE1BQU0sQ0FBQyxxQ0FBcUMsRUFBRSxDQUFDO1FBRWpELEtBQUssb0NBQW9DO1lBQ3ZDLE1BQU0sQ0FBQywwQkFBMEIsRUFBRSxDQUFDO1FBRXRDLEtBQUsscUJBQXFCO1lBQ3hCLE1BQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUUxQixLQUFLLHFCQUFxQjtZQUN4QixNQUFNLENBQUMsYUFBYSxFQUFFLENBQUM7UUFFekIsS0FBSyxzQkFBc0I7WUFDekIsTUFBTSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRTFCLEtBQUssc0JBQXNCO1lBQ3pCLE1BQU0sQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUUzQixTQUFTLE1BQU0sQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUNsQyxDQUFDO0FBRUgsQ0FBQyxDQUFDO0FBRUYsZUFBZSxVQUFVLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/info/about/container.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var container_InfoAboutContainer = /** @class */ (function (_super) {
    __extends(InfoAboutContainer, _super);
    function InfoAboutContainer(props) {
        return _super.call(this, props) || this;
    }
    InfoAboutContainer.prototype.render = function () {
        return view(this.props);
    };
    ;
    InfoAboutContainer = __decorate([
        radium
    ], InfoAboutContainer);
    return InfoAboutContainer;
}(react["PureComponent"]));
/* harmony default export */ var container = (container_InfoAboutContainer);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLE9BQU8sQ0FBQztBQUN0QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUVqQyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFJaEM7SUFBaUMsc0NBQTBCO0lBQ3pELDRCQUFZLEtBQWE7ZUFDdkIsa0JBQU0sS0FBSyxDQUFDO0lBQ2QsQ0FBQztJQUVELG1DQUFNLEdBQU47UUFDRSxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBQUEsQ0FBQztJQVBFLGtCQUFrQjtRQUR2QixNQUFNO09BQ0Qsa0JBQWtCLENBUXZCO0lBQUQseUJBQUM7Q0FBQSxBQVJELENBQWlDLGFBQWEsR0FRN0M7QUFFRCxlQUFlLGtCQUFrQixDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/info/about/store.tsx
var connect = __webpack_require__(129).connect;

var mapStateToProps = function (state) { return ({}); };
var mapDispatchToProps = function (dispatch) { return ({}); };
/* harmony default export */ var store = __webpack_exports__["default"] = (connect(mapStateToProps, mapDispatchToProps)(container));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLGtCQUFrQixNQUFNLGFBQWEsQ0FBQztBQUU3QyxJQUFNLGVBQWUsR0FBRyxVQUFBLEtBQUssSUFBSSxPQUFBLENBQUMsRUFBRSxDQUFDLEVBQUosQ0FBSSxDQUFDO0FBRXRDLElBQU0sa0JBQWtCLEdBQUcsVUFBQSxRQUFRLElBQUksT0FBQSxDQUFDLEVBQUUsQ0FBQyxFQUFKLENBQUksQ0FBQztBQUU1QyxlQUFlLE9BQU8sQ0FDcEIsZUFBZSxFQUNmLGtCQUFrQixDQUNuQixDQUFDLGtCQUFrQixDQUFDLENBQUMifQ==

/***/ })

}]);