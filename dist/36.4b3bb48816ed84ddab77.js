(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[36],{

/***/ 745:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 100).then(__webpack_require__.bind(null, 755)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 858:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ../node_modules/react-router/index.js
var react_router = __webpack_require__(173);

// EXTERNAL MODULE: ./action/auth.ts + 1 modules
var auth = __webpack_require__(350);

// EXTERNAL MODULE: ./action/cart.ts + 1 modules
var cart = __webpack_require__(83);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./constants/application/global.ts
var global = __webpack_require__(24);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./utils/format.ts
var format = __webpack_require__(116);

// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// EXTERNAL MODULE: ./container/layout/wrap/index.tsx
var wrap = __webpack_require__(745);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./container/auth/connect-facebook/style.tsx

/* harmony default export */ var style = ({
    container: {
        height: 500
    },
    text: {
        paddingTop: 150,
        paddingBottom: 100,
        textAlign: 'center',
        fontSize: 22,
        fontFamily: variable["fontAvenirDemiBold"]
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUdwRCxlQUFlO0lBQ2IsU0FBUyxFQUFFO1FBQ1QsTUFBTSxFQUFFLEdBQUc7S0FDWjtJQUVELElBQUksRUFBRTtRQUNKLFVBQVUsRUFBRSxHQUFHO1FBQ2YsYUFBYSxFQUFFLEdBQUc7UUFDbEIsU0FBUyxFQUFFLFFBQVE7UUFDbkIsUUFBUSxFQUFFLEVBQUU7UUFDWixVQUFVLEVBQUUsUUFBUSxDQUFDLGtCQUFrQjtLQUN4QztDQUNGLENBQUMifQ==
// CONCATENATED MODULE: ./container/auth/connect-facebook/view.tsx



var renderView = function () { return (react["createElement"]("sign-in-container", { style: style.container },
    react["createElement"](wrap["a" /* default */], null,
        react["createElement"]("div", { style: style.text }, "Vui l\u00F2ng ch\u1EDD trong gi\u00E2y l\u00E1t...")))); };
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRS9CLE9BQU8sVUFBVSxNQUFNLG1CQUFtQixDQUFDO0FBRTNDLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QixJQUFNLFVBQVUsR0FBRyxjQUFNLE9BQUEsQ0FDdkIsMkNBQW1CLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUztJQUN2QyxvQkFBQyxVQUFVO1FBQ1QsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLHlEQUVsQixDQUNPLENBQ0ssQ0FDckIsRUFSd0IsQ0FReEIsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./container/auth/connect-facebook/container.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var container_ConnectFacebookContainer = /** @class */ (function (_super) {
    __extends(ConnectFacebookContainer, _super);
    function ConnectFacebookContainer(props) {
        var _this = _super.call(this, props) || this;
        window.startTime = new Date();
        return _this;
    }
    ConnectFacebookContainer.prototype.componentDidMount = function () {
        var _a = this.props, signInFBAction = _a.signInFBAction, hash = _a.location.hash, history = _a.history, userReferrerProfile = _a.userStore.userReferrerProfile;
        if (!hash) {
            history.push(routing["kb" /* ROUTING_SHOP_INDEX */]);
            return;
        }
        var referralCode = !Object(validate["j" /* isEmptyObject */])(userReferrerProfile) ? userReferrerProfile.referral_code : '';
        var accessToken = Object(format["e" /* getUrlParameter */])(hash.replace('#', '?'), 'access_token');
        signInFBAction({ facebookToken: accessToken, referralCode: referralCode });
    };
    ConnectFacebookContainer.prototype.handleSignInSuccess = function () {
        var _a = this.props, getCart = _a.getCart, history = _a.history;
        /** Default re-direct to home page */
        var targetRedirect = routing["kb" /* ROUTING_SHOP_INDEX */];
        /** if referrla exist -> re-direct to refferal link */
        var referralRedirectStorage = localStorage.getItem('referral-redirect');
        if (referralRedirectStorage) {
            targetRedirect = referralRedirectStorage;
            localStorage.removeItem('referral-redirect');
        }
        history.push(targetRedirect);
        getCart();
    };
    ConnectFacebookContainer.prototype.componentWillReceiveProps = function (nextProps) {
        var history = this.props.history;
        var _a = nextProps.authStore, signInStatus = _a.signInStatus, errorMessage = _a.errorMessage;
        switch (signInStatus) {
            case global["e" /* SIGN_IN_STATE */].LOGIN_SUCCESS:
                this.handleSignInSuccess();
                break;
            case global["e" /* SIGN_IN_STATE */].LOGIN_FAIL:
                history.push(routing["kb" /* ROUTING_SHOP_INDEX */]);
                break;
        }
    };
    ConnectFacebookContainer.prototype.render = function () {
        return view();
    };
    ConnectFacebookContainer = __decorate([
        radium
    ], ConnectFacebookContainer);
    return ConnectFacebookContainer;
}(react["Component"]));
;
/* harmony default export */ var container = (container_ConnectFacebookContainer);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFFakMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3RFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBRTVFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFFeEQsT0FBTyxVQUFVLE1BQU0sUUFBUSxDQUFDO0FBSWhDO0lBQXVDLDRDQU9oQztJQUNMLGtDQUFZLEtBQUs7UUFBakIsWUFDRSxrQkFBTSxLQUFLLENBQUMsU0FFYjtRQURDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQzs7SUFDaEMsQ0FBQztJQUVELG9EQUFpQixHQUFqQjtRQUNRLElBQUEsZUFBZ0csRUFBOUYsa0NBQWMsRUFBYyx1QkFBSSxFQUFJLG9CQUFPLEVBQWUsc0RBQW1CLENBQWtCO1FBQ3ZHLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQTtRQUFDLENBQUM7UUFFdkQsSUFBTSxZQUFZLEdBQUcsQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDbEcsSUFBTSxXQUFXLEdBQUcsZUFBZSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxFQUFFLGNBQWMsQ0FBQyxDQUFDO1FBQzVFLGNBQWMsQ0FBQyxFQUFFLGFBQWEsRUFBRSxXQUFXLEVBQUUsWUFBWSxjQUFBLEVBQUUsQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUFFRCxzREFBbUIsR0FBbkI7UUFDUSxJQUFBLGVBQWlDLEVBQS9CLG9CQUFPLEVBQUUsb0JBQU8sQ0FBZ0I7UUFFeEMscUNBQXFDO1FBQ3JDLElBQUksY0FBYyxHQUFHLGtCQUFrQixDQUFDO1FBRXhDLHNEQUFzRDtRQUN0RCxJQUFNLHVCQUF1QixHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUMxRSxFQUFFLENBQUMsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUM7WUFDNUIsY0FBYyxHQUFHLHVCQUF1QixDQUFDO1lBQ3pDLFlBQVksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUMvQyxDQUFDO1FBRUQsT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUU3QixPQUFPLEVBQUUsQ0FBQztJQUNaLENBQUM7SUFFRCw0REFBeUIsR0FBekIsVUFBMEIsU0FBUztRQUN6QixJQUFBLDRCQUFPLENBQWdCO1FBQ3ZCLElBQUEsd0JBQXlDLEVBQTVCLDhCQUFZLEVBQUUsOEJBQVksQ0FBaUI7UUFFaEUsTUFBTSxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUNyQixLQUFLLGFBQWEsQ0FBQyxhQUFhO2dCQUM5QixJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztnQkFDM0IsS0FBSyxDQUFDO1lBRVIsS0FBSyxhQUFhLENBQUMsVUFBVTtnQkFDM0IsT0FBTyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2dCQUVqQyxLQUFLLENBQUM7UUFDVixDQUFDO0lBQ0gsQ0FBQztJQUVELHlDQUFNLEdBQU47UUFDRSxNQUFNLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQTFERyx3QkFBd0I7UUFGN0IsTUFBTTtPQUVELHdCQUF3QixDQTJEN0I7SUFBRCwrQkFBQztDQUFBLEFBM0RELENBQXVDLEtBQUssQ0FBQyxTQUFTLEdBMkRyRDtBQUFBLENBQUM7QUFFRixlQUFlLHdCQUF3QixDQUFDIn0=
// CONCATENATED MODULE: ./container/auth/connect-facebook/store.tsx
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapStateToProps", function() { return mapStateToProps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapDispatchToProps", function() { return mapDispatchToProps; });
var connect = __webpack_require__(129).connect;




var mapStateToProps = function (state) { return ({
    authStore: state.auth,
    userStore: state.user
}); };
var mapDispatchToProps = function (dispatch) { return ({
    getCart: function () { return dispatch(Object(cart["t" /* getCartAction */])()); },
    signInFBAction: function (data) { return dispatch(Object(auth["i" /* signInFBAction */])(data)); },
}); };
/* harmony default export */ var store = __webpack_exports__["default"] = (Object(react_router["withRouter"])(connect(mapStateToProps, mapDispatchToProps)(container)));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUMvQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sY0FBYyxDQUFBO0FBRXpDLE9BQU8sRUFBZ0IsY0FBYyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDcEUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3JELE9BQU8sd0JBQXdCLE1BQU0sYUFBYSxDQUFDO0FBRW5ELE1BQU0sQ0FBQyxJQUFNLGVBQWUsR0FBRyxVQUFDLEtBQUssSUFBSyxPQUFBLENBQUM7SUFDekMsU0FBUyxFQUFFLEtBQUssQ0FBQyxJQUFJO0lBQ3JCLFNBQVMsRUFBRSxLQUFLLENBQUMsSUFBSTtDQUN0QixDQUFDLEVBSHdDLENBR3hDLENBQUM7QUFFSCxNQUFNLENBQUMsSUFBTSxrQkFBa0IsR0FBRyxVQUFDLFFBQVEsSUFBSyxPQUFBLENBQUM7SUFDL0MsT0FBTyxFQUFFLGNBQU0sT0FBQSxRQUFRLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBekIsQ0FBeUI7SUFDeEMsY0FBYyxFQUFFLFVBQUMsSUFBUyxJQUFLLE9BQUEsUUFBUSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUE5QixDQUE4QjtDQUM5RCxDQUFDLEVBSDhDLENBRzlDLENBQUM7QUFFSCxlQUFlLFVBQVUsQ0FDdkIsT0FBTyxDQUNMLGVBQWUsRUFDZixrQkFBa0IsQ0FDbkIsQ0FBQyx3QkFBd0IsQ0FBQyxDQUM1QixDQUFDIn0=

/***/ })

}]);