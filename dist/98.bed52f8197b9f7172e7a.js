(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[98],{

/***/ 857:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/uri.ts
var uri = __webpack_require__(347);

// CONCATENATED MODULE: ./container/landing-page/component/video/initialize.tsx

var video1 = 'https://upload.lixibox.com/videos/halio-1.mp4';
var video2 = 'https://upload.lixibox.com/videos/halio-2.mp4';
var video3 = 'https://upload.lixibox.com/videos/halio-3.mp4';
var video4 = 'https://upload.lixibox.com/videos/halio-4.mp4';
var video5 = 'https://upload.lixibox.com/videos/halio-5.mp4';
var thumbVideo1 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/halio-landing-page/thumb-1.png';
var thumbVideo2 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/halio-landing-page/thumb-2.png';
var thumbVideo3 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/halio-landing-page/thumb-3.png';
var thumbVideo4 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/halio-landing-page/thumb-4.png';
var thumbVideo5 = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/halio-landing-page/thumb-5.png';
var INITIAL_STATE = {
    videoList: [
        {
            video: video1,
            img: thumbVideo1,
            title: 'Kiểm chứng hiệu quả làm sạch của Máy rửa mặt HALIO'
        },
        {
            video: video2,
            img: thumbVideo2,
            title: 'Beauty Blogger nói gì về HALIO?'
        },
        {
            video: video3,
            img: thumbVideo3,
            title: 'Hướng dẫn sử dụng HALIO'
        },
        {
            video: video4,
            img: thumbVideo4,
            title: 'Vũ DINO - Đẹp trai mới có nhiều đứa yêu'
        },
        {
            video: video5,
            img: thumbVideo5,
            title: 'Vì sao MISOA chọn HALIO mà không phải máy rửa mặt khác'
        }
    ],
    autoPlayVideo: false,
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBRTFELElBQU0sTUFBTSxHQUFHLCtDQUErQyxDQUFDO0FBQy9ELElBQU0sTUFBTSxHQUFHLCtDQUErQyxDQUFDO0FBQy9ELElBQU0sTUFBTSxHQUFHLCtDQUErQyxDQUFDO0FBQy9ELElBQU0sTUFBTSxHQUFHLCtDQUErQyxDQUFDO0FBQy9ELElBQU0sTUFBTSxHQUFHLCtDQUErQyxDQUFDO0FBRS9ELElBQU0sV0FBVyxHQUFHLGlCQUFpQixHQUFHLCtDQUErQyxDQUFDO0FBQ3hGLElBQU0sV0FBVyxHQUFHLGlCQUFpQixHQUFHLCtDQUErQyxDQUFDO0FBQ3hGLElBQU0sV0FBVyxHQUFHLGlCQUFpQixHQUFHLCtDQUErQyxDQUFDO0FBQ3hGLElBQU0sV0FBVyxHQUFHLGlCQUFpQixHQUFHLCtDQUErQyxDQUFDO0FBQ3hGLElBQU0sV0FBVyxHQUFHLGlCQUFpQixHQUFHLCtDQUErQyxDQUFDO0FBRXhGLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixTQUFTLEVBQUU7UUFDVDtZQUNFLEtBQUssRUFBRSxNQUFNO1lBQ2IsR0FBRyxFQUFFLFdBQVc7WUFDaEIsS0FBSyxFQUFFLG9EQUFvRDtTQUM1RDtRQUNEO1lBQ0UsS0FBSyxFQUFFLE1BQU07WUFDYixHQUFHLEVBQUUsV0FBVztZQUNoQixLQUFLLEVBQUUsaUNBQWlDO1NBQ3pDO1FBQ0Q7WUFDRSxLQUFLLEVBQUUsTUFBTTtZQUNiLEdBQUcsRUFBRSxXQUFXO1lBQ2hCLEtBQUssRUFBRSx5QkFBeUI7U0FDakM7UUFDRDtZQUNFLEtBQUssRUFBRSxNQUFNO1lBQ2IsR0FBRyxFQUFFLFdBQVc7WUFDaEIsS0FBSyxFQUFFLHlDQUF5QztTQUNqRDtRQUNEO1lBQ0UsS0FBSyxFQUFFLE1BQU07WUFDYixHQUFHLEVBQUUsV0FBVztZQUNoQixLQUFLLEVBQUUsd0RBQXdEO1NBQ2hFO0tBQ0Y7SUFDRCxhQUFhLEVBQUUsS0FBSztDQUNYLENBQUMifQ==
// EXTERNAL MODULE: ./container/layout/wrap/index.tsx
var wrap = __webpack_require__(745);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./container/landing-page/component/video/style.tsx



var videoBg = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/halio-landing-page/cover-1.jpg';
/* harmony default export */ var style = ({
    video: {
        container: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    paddingBottom: 10,
                }],
            DESKTOP: [{
                    paddingTop: 60,
                    paddingBottom: 70,
                }],
            GENERAL: [{
                    position: variable["position"].relative
                }]
        }),
        bg: {
            backgroundImage: "url('" + videoBg + "')",
            backgroundSize: 'cover',
            position: variable["position"].absolute,
            top: 0,
            left: 0,
            zIndex: 0,
            width: '100%',
            height: '100%',
            filter: 'blur(50px)',
            opacity: .1
        },
        main: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{ marginBottom: 10, }],
            DESKTOP: [{
                    display: variable["display"].block,
                    margin: '0 auto 10px',
                    borderRadius: 30,
                    maxWidth: 840,
                    boxShadow: "0 0px 50px rgba(0,0,0,.1)"
                }],
            GENERAL: [{
                    width: '100%',
                    display: variable["display"].block,
                    transition: variable["transitionNormal"]
                }]
        }),
        mainTitle: {
            fontSize: 26,
            fontFamily: variable["fontAvenirDemiBold"],
            lineHeight: '40px',
            maxWidth: 840,
            textAlign: 'center',
            textTransform: 'uppercase',
            margin: '0 auto 30px',
            paddingLeft: 10,
            paddingRight: 10
        },
        smallTitle: {
            fontSize: 12,
            fontFamily: variable["fontAvenirMedium"],
            lineHeight: '20px',
            position: variable["position"].absolute,
            top: "calc(100% + 10px)",
            textAlign: 'center',
            textTransform: 'uppercase',
        },
        list: {
            container: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{ flexWrap: 'wrap' }],
                DESKTOP: [{ height: 120, }],
                GENERAL: [{
                        position: variable["position"].relative,
                        display: variable["display"].flex,
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }]
            }),
            item: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        width: 150,
                        height: 100,
                        marginLeft: 5,
                        marginRight: 5,
                        marginBottom: 75,
                    }],
                DESKTOP: [{
                        width: 185,
                        height: 120,
                        marginLeft: 15,
                        marginRight: 15,
                        borderRadius: 10,
                        boxShadow: "0 0px 30px rgba(0,0,0,.1)"
                    }],
                GENERAL: [{
                        backgroundSize: 'cover',
                        position: variable["position"].relative,
                        cursor: 'pointer'
                    }]
            }),
        },
        overlay: {
            position: variable["position"].absolute,
            display: variable["display"].flex,
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            background: variable["colorBlack01"],
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 10,
        },
        iconPlay: {
            width: 30,
            height: 40,
            borderTop: "20px solid transparent",
            borderBottom: "20px solid transparent",
            borderLeft: "30px solid " + variable["colorWhite"],
            boxSizing: 'border-box',
        },
        wrap: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{ height: 'auto' }],
            DESKTOP: [{ height: 472 }],
            GENERAL: [{
                    display: variable["display"].block,
                    margin: '0px auto 10px',
                    borderRadius: 30,
                    maxWidth: 840,
                    width: '100%',
                }]
        }),
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSw0QkFBNEIsQ0FBQztBQUN2RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDNUQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDMUQsSUFBTSxPQUFPLEdBQUcsaUJBQWlCLEdBQUcsK0NBQStDLENBQUM7QUFFcEYsZUFBZTtJQUNiLEtBQUssRUFBRTtRQUNMLFNBQVMsRUFBRSxZQUFZLENBQUM7WUFDdEIsTUFBTSxFQUFFLENBQUM7b0JBQ1AsYUFBYSxFQUFFLEVBQUU7aUJBQ2xCLENBQUM7WUFFRixPQUFPLEVBQUUsQ0FBQztvQkFDUixVQUFVLEVBQUUsRUFBRTtvQkFDZCxhQUFhLEVBQUUsRUFBRTtpQkFDbEIsQ0FBQztZQUVGLE9BQU8sRUFBRSxDQUFDO29CQUNSLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7aUJBQ3JDLENBQUM7U0FDSCxDQUFDO1FBRUYsRUFBRSxFQUFFO1lBQ0YsZUFBZSxFQUFFLFVBQVEsT0FBTyxPQUFJO1lBQ3BDLGNBQWMsRUFBRSxPQUFPO1lBQ3ZCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDcEMsR0FBRyxFQUFFLENBQUM7WUFDTixJQUFJLEVBQUUsQ0FBQztZQUNQLE1BQU0sRUFBRSxDQUFDO1lBQ1QsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsTUFBTTtZQUNkLE1BQU0sRUFBRSxZQUFZO1lBQ3BCLE9BQU8sRUFBRSxFQUFFO1NBQ1o7UUFFRCxJQUFJLEVBQUUsWUFBWSxDQUFDO1lBQ2pCLE1BQU0sRUFBRSxDQUFDLEVBQUUsWUFBWSxFQUFFLEVBQUUsR0FBRyxDQUFDO1lBRS9CLE9BQU8sRUFBRSxDQUFDO29CQUNSLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7b0JBQy9CLE1BQU0sRUFBRSxhQUFhO29CQUNyQixZQUFZLEVBQUUsRUFBRTtvQkFDaEIsUUFBUSxFQUFFLEdBQUc7b0JBQ2IsU0FBUyxFQUFFLDJCQUEyQjtpQkFDdkMsQ0FBQztZQUVGLE9BQU8sRUFBRSxDQUFDO29CQUNSLEtBQUssRUFBRSxNQUFNO29CQUNiLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7b0JBQy9CLFVBQVUsRUFBRSxRQUFRLENBQUMsZ0JBQWdCO2lCQUN0QyxDQUFDO1NBQ0gsQ0FBQztRQUVGLFNBQVMsRUFBRTtZQUNULFFBQVEsRUFBRSxFQUFFO1lBQ1osVUFBVSxFQUFFLFFBQVEsQ0FBQyxrQkFBa0I7WUFDdkMsVUFBVSxFQUFFLE1BQU07WUFDbEIsUUFBUSxFQUFFLEdBQUc7WUFDYixTQUFTLEVBQUUsUUFBUTtZQUNuQixhQUFhLEVBQUUsV0FBVztZQUMxQixNQUFNLEVBQUUsYUFBYTtZQUNyQixXQUFXLEVBQUUsRUFBRTtZQUNmLFlBQVksRUFBRSxFQUFFO1NBQ2pCO1FBRUQsVUFBVSxFQUFFO1lBQ1YsUUFBUSxFQUFFLEVBQUU7WUFDWixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtZQUNyQyxVQUFVLEVBQUUsTUFBTTtZQUNsQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1lBQ3BDLEdBQUcsRUFBRSxtQkFBbUI7WUFDeEIsU0FBUyxFQUFFLFFBQVE7WUFDbkIsYUFBYSxFQUFFLFdBQVc7U0FDM0I7UUFFRCxJQUFJLEVBQUU7WUFDSixTQUFTLEVBQUUsWUFBWSxDQUFDO2dCQUN0QixNQUFNLEVBQUUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsQ0FBQztnQkFFOUIsT0FBTyxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsR0FBRyxHQUFHLENBQUM7Z0JBRTNCLE9BQU8sRUFBRSxDQUFDO3dCQUNSLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7d0JBQ3BDLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7d0JBQzlCLEtBQUssRUFBRSxNQUFNO3dCQUNiLGNBQWMsRUFBRSxRQUFRO3dCQUN4QixVQUFVLEVBQUUsUUFBUTtxQkFDckIsQ0FBQzthQUNILENBQUM7WUFFRixJQUFJLEVBQUUsWUFBWSxDQUFDO2dCQUNqQixNQUFNLEVBQUUsQ0FBQzt3QkFDUCxLQUFLLEVBQUUsR0FBRzt3QkFDVixNQUFNLEVBQUUsR0FBRzt3QkFDWCxVQUFVLEVBQUUsQ0FBQzt3QkFDYixXQUFXLEVBQUUsQ0FBQzt3QkFDZCxZQUFZLEVBQUUsRUFBRTtxQkFDakIsQ0FBQztnQkFFRixPQUFPLEVBQUUsQ0FBQzt3QkFDUixLQUFLLEVBQUUsR0FBRzt3QkFDVixNQUFNLEVBQUUsR0FBRzt3QkFDWCxVQUFVLEVBQUUsRUFBRTt3QkFDZCxXQUFXLEVBQUUsRUFBRTt3QkFDZixZQUFZLEVBQUUsRUFBRTt3QkFDaEIsU0FBUyxFQUFFLDJCQUEyQjtxQkFDdkMsQ0FBQztnQkFFRixPQUFPLEVBQUUsQ0FBQzt3QkFDUixjQUFjLEVBQUUsT0FBTzt3QkFDdkIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTt3QkFDcEMsTUFBTSxFQUFFLFNBQVM7cUJBQ2xCLENBQUM7YUFDSCxDQUFDO1NBQ0g7UUFFRCxPQUFPLEVBQUU7WUFDUCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1lBQ3BDLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsR0FBRyxFQUFFLENBQUM7WUFDTixJQUFJLEVBQUUsQ0FBQztZQUNQLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLE1BQU07WUFDZCxVQUFVLEVBQUUsUUFBUSxDQUFDLFlBQVk7WUFDakMsY0FBYyxFQUFFLFFBQVE7WUFDeEIsVUFBVSxFQUFFLFFBQVE7WUFDcEIsWUFBWSxFQUFFLEVBQUU7U0FDakI7UUFFRCxRQUFRLEVBQUU7WUFDUixLQUFLLEVBQUUsRUFBRTtZQUNULE1BQU0sRUFBRSxFQUFFO1lBQ1YsU0FBUyxFQUFFLHdCQUF3QjtZQUNuQyxZQUFZLEVBQUUsd0JBQXdCO1lBQ3RDLFVBQVUsRUFBRSxnQkFBYyxRQUFRLENBQUMsVUFBWTtZQUMvQyxTQUFTLEVBQUUsWUFBWTtTQUN4QjtRQUVELElBQUksRUFBRSxZQUFZLENBQUM7WUFDakIsTUFBTSxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLENBQUM7WUFFNUIsT0FBTyxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLENBQUM7WUFFMUIsT0FBTyxFQUFFLENBQUM7b0JBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztvQkFDL0IsTUFBTSxFQUFFLGVBQWU7b0JBQ3ZCLFlBQVksRUFBRSxFQUFFO29CQUNoQixRQUFRLEVBQUUsR0FBRztvQkFDYixLQUFLLEVBQUUsTUFBTTtpQkFDZCxDQUFDO1NBQ0gsQ0FBQztLQUNIO0NBQ00sQ0FBQyJ9
// CONCATENATED MODULE: ./container/landing-page/component/video/view.tsx



function handleRenderVideo(item, index) {
    var _this = this;
    return (react["createElement"]("div", { key: "video-item-" + index, style: [style.video.list.item, { backgroundImage: "url(" + item.img + ")" }], onClick: function () { return _this.handleClickVideo(index + 1); } },
        react["createElement"]("div", { style: style.video.overlay },
            react["createElement"]("div", { style: style.video.iconPlay })),
        react["createElement"]("div", { style: style.video.smallTitle }, item.title)));
}
;
var renderVideo = function (_a) {
    var autoPlay = _a.autoPlay, videoList = _a.videoList, handleClickVideo = _a.handleClickVideo;
    var videoLength = videoList && videoList.length || 0;
    var subVideoList = Array.isArray(videoList)
        && videoLength > 0
        && videoList.slice(1, videoLength) || [];
    return (react["createElement"]("div", { style: style.video.container },
        react["createElement"]("div", { style: style.video.bg }),
        react["createElement"](wrap["a" /* default */], { style: { position: 'relative' } },
            react["createElement"]("div", { style: style.video.wrap },
                react["createElement"]("video", { id: 'main-video-halio', style: style.video.main, src: videoList[0].video, autoPlay: autoPlay })),
            react["createElement"]("div", { style: style.video.mainTitle }, videoList[0].title),
            react["createElement"]("div", { style: style.video.list.container }, subVideoList.map(handleRenderVideo, { handleClickVideo: handleClickVideo })))));
};
var renderView = function (_a) {
    var props = _a.props, state = _a.state, handleClickVideo = _a.handleClickVideo;
    return renderVideo({ autoPlay: state.autoPlayVideo, videoList: state.videoList, handleClickVideo: handleClickVideo });
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBRS9CLE9BQU8sVUFBVSxNQUFNLHNCQUFzQixDQUFDO0FBQzlDLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUU1QiwyQkFBMkIsSUFBSSxFQUFFLEtBQUs7SUFBdEMsaUJBT0M7SUFOQyxNQUFNLENBQUMsQ0FDTCw2QkFBSyxHQUFHLEVBQUUsZ0JBQWMsS0FBTyxFQUFFLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxFQUFFLGVBQWUsRUFBRSxTQUFPLElBQUksQ0FBQyxHQUFHLE1BQUcsRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLGNBQU0sT0FBQSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxFQUFoQyxDQUFnQztRQUN2Siw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPO1lBQUUsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFRLENBQU07UUFDL0UsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLLENBQUMsVUFBVSxJQUFHLElBQUksQ0FBQyxLQUFLLENBQU8sQ0FDbEQsQ0FDUCxDQUFBO0FBQ0gsQ0FBQztBQUFBLENBQUM7QUFFRixJQUFNLFdBQVcsR0FBRyxVQUFDLEVBQXlDO1FBQXZDLHNCQUFRLEVBQUUsd0JBQVMsRUFBRSxzQ0FBZ0I7SUFDMUQsSUFBTSxXQUFXLEdBQUcsU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDO0lBQ3ZELElBQU0sWUFBWSxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDO1dBQ3hDLFdBQVcsR0FBRyxDQUFDO1dBQ2YsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDO0lBRTNDLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSyxDQUFDLFNBQVM7UUFDL0IsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLLENBQUMsRUFBRSxHQUFRO1FBQ2xDLG9CQUFDLFVBQVUsSUFBQyxLQUFLLEVBQUUsRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFO1lBQ3pDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUk7Z0JBQzFCLCtCQUFPLEVBQUUsRUFBQyxrQkFBa0IsRUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsR0FBRyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsUUFBUSxFQUFFLFFBQVEsR0FFekYsQ0FDSjtZQUNOLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSyxDQUFDLFNBQVMsSUFBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFPO1lBQzdELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLElBQ25DLFlBQVksQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUUsRUFBRSxnQkFBZ0Isa0JBQUEsRUFBRSxDQUFDLENBQ3RELENBQ0ssQ0FDVCxDQUNQLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixJQUFNLFVBQVUsR0FBRyxVQUFDLEVBQWtDO1FBQWhDLGdCQUFLLEVBQUUsZ0JBQUssRUFBRSxzQ0FBZ0I7SUFDbEQsTUFBTSxDQUFDLFdBQVcsQ0FBQyxFQUFFLFFBQVEsRUFBRSxLQUFLLENBQUMsYUFBYSxFQUFFLFNBQVMsRUFBRSxLQUFLLENBQUMsU0FBUyxFQUFFLGdCQUFnQixrQkFBQSxFQUFFLENBQUMsQ0FBQztBQUN0RyxDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./container/landing-page/component/video/component.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var component_VideoComponent = /** @class */ (function (_super) {
    __extends(VideoComponent, _super);
    function VideoComponent(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    VideoComponent.prototype.handleClickVideo = function (index) {
        var videoList = this.state.videoList;
        var tmpVideoList = JSON.parse(JSON.stringify(videoList));
        var selectedVideo = tmpVideoList[index];
        var firstVideo = tmpVideoList[0];
        tmpVideoList.splice(index, 1);
        tmpVideoList.splice(0, 1);
        tmpVideoList.unshift(selectedVideo);
        tmpVideoList.splice(index, 0, firstVideo);
        this.setState({ videoList: tmpVideoList, autoPlayVideo: true });
    };
    VideoComponent.prototype.componentDidUpdate = function () {
        var videoMainElement = document.getElementById('main-video-halio');
        var att = document.createAttribute('controls');
        att.value = 'true';
        videoMainElement.setAttributeNode(att);
    };
    VideoComponent.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state,
            handleClickVideo: this.handleClickVideo.bind(this)
        };
        return view(args);
    };
    VideoComponent = __decorate([
        radium
    ], VideoComponent);
    return VideoComponent;
}(react["Component"]));
;
/* harmony default export */ var component = __webpack_exports__["default"] = (component_VideoComponent);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFLakMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUM3QyxPQUFPLFVBQVUsTUFBTSxRQUFRLENBQUM7QUFHaEM7SUFBNkIsa0NBQStCO0lBQzFELHdCQUFZLEtBQWE7UUFBekIsWUFDRSxrQkFBTSxLQUFLLENBQUMsU0FFYjtRQURDLEtBQUksQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDOztJQUM3QixDQUFDO0lBRUQseUNBQWdCLEdBQWhCLFVBQWlCLEtBQUs7UUFDWixJQUFBLGdDQUFTLENBQWdCO1FBQ2pDLElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ3pELElBQU0sYUFBYSxHQUFHLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMxQyxJQUFNLFVBQVUsR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbkMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDOUIsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDMUIsWUFBWSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNwQyxZQUFZLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFFMUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7SUFDbEUsQ0FBQztJQUVELDJDQUFrQixHQUFsQjtRQUNFLElBQU0sZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQ3JFLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDL0MsR0FBRyxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUM7UUFDbkIsZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVELCtCQUFNLEdBQU47UUFDRSxJQUFNLElBQUksR0FBRztZQUNYLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDbkQsQ0FBQztRQUVGLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDMUIsQ0FBQztJQWxDRyxjQUFjO1FBRG5CLE1BQU07T0FDRCxjQUFjLENBbUNuQjtJQUFELHFCQUFDO0NBQUEsQUFuQ0QsQ0FBNkIsS0FBSyxDQUFDLFNBQVMsR0FtQzNDO0FBQUEsQ0FBQztBQUVGLGVBQWUsY0FBYyxDQUFDIn0=

/***/ })

}]);