(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[58],{

/***/ 746:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 99).then(__webpack_require__.bind(null, 759)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 753:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 97).then(__webpack_require__.bind(null, 771)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 767:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KEY_WORD; });
var KEY_WORD = {
    TRACKING_CODE: 'tracking_code',
    CAMPAIGN_CODE: 'campaign_code',
    EXP_TRACKING_CODE: 'exp_tracking_code',
    RESET_PASSWORD_TOKEN: 'reset_password_token',
    UTM_ID: 'utm_id'
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2V5LXdvcmQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJrZXktd29yZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxNQUFNLENBQUMsSUFBTSxRQUFRLEdBQUc7SUFDdEIsYUFBYSxFQUFFLGVBQWU7SUFDOUIsYUFBYSxFQUFFLGVBQWU7SUFDOUIsaUJBQWlCLEVBQUUsbUJBQW1CO0lBQ3RDLG9CQUFvQixFQUFFLHNCQUFzQjtJQUM1QyxNQUFNLEVBQUUsUUFBUTtDQUNqQixDQUFDIn0=

/***/ }),

/***/ 798:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// CONCATENATED MODULE: ./api/brand.ts

var fetchBrandList = function () { return Object(restful_method["b" /* get */])({
    path: "/brands",
    description: 'Get Brand list',
    errorMesssage: "Can't get list brands. Please try again",
}); };
var fetchProductByBrandId = function (_a) {
    var id = _a.id, _b = _a.pl, pl = _b === void 0 ? '' : _b, _c = _a.ph, ph = _c === void 0 ? '' : _c, _d = _a.sort, sort = _d === void 0 ? 'newest' : _d, _e = _a.page, page = _e === void 0 ? 1 : _e, _f = _a.perPage, perPage = _f === void 0 ? 12 : _f;
    var query = "?page=" + page + "&per_page=" + perPage + "&pl=" + pl + "&ph=" + ph + "&sort=" + sort;
    return Object(restful_method["b" /* get */])({
        path: "/brands/" + id + query,
        description: 'Fetch product by brand id',
        errorMesssage: "Can't fetch product by brand id. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnJhbmQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJicmFuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFL0MsTUFBTSxDQUFDLElBQU0sY0FBYyxHQUFHLGNBQU0sT0FBQSxHQUFHLENBQUM7SUFDdEMsSUFBSSxFQUFFLFNBQVM7SUFDZixXQUFXLEVBQUUsZ0JBQWdCO0lBQzdCLGFBQWEsRUFBRSx5Q0FBeUM7Q0FDekQsQ0FBQyxFQUprQyxDQUlsQyxDQUFDO0FBR0gsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQUcsVUFBQyxFQU10QjtRQUxkLFVBQUUsRUFDRixVQUFPLEVBQVAsNEJBQU8sRUFDUCxVQUFPLEVBQVAsNEJBQU8sRUFDUCxZQUFlLEVBQWYsb0NBQWUsRUFDZixZQUFRLEVBQVIsNkJBQVEsRUFDUixlQUFZLEVBQVosaUNBQVk7SUFFWixJQUFNLEtBQUssR0FBRyxXQUFTLElBQUksa0JBQWEsT0FBTyxZQUFPLEVBQUUsWUFBTyxFQUFFLGNBQVMsSUFBTSxDQUFDO0lBRWpGLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDVCxJQUFJLEVBQUUsYUFBVyxFQUFFLEdBQUcsS0FBTztRQUM3QixXQUFXLEVBQUUsMkJBQTJCO1FBQ3hDLGFBQWEsRUFBRSxtREFBbUQ7S0FDbkUsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFBIn0=
// EXTERNAL MODULE: ./constants/api/brand.ts
var brand = __webpack_require__(113);

// CONCATENATED MODULE: ./action/brand.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fetchBrandListAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fetchProductByBrandIdAction; });


/** Fetch Brand List */
var fetchBrandListAction = function () { return function (dispatch, getState) {
    return dispatch({
        type: brand["a" /* FETCH_BRAND_LIST */],
        payload: { promise: fetchBrandList().then(function (res) { return res; }) },
    });
}; };
/** Fecth list all product by brand id */
var fetchProductByBrandIdAction = function (_a) {
    var id = _a.id, _b = _a.pl, pl = _b === void 0 ? '' : _b, _c = _a.ph, ph = _c === void 0 ? '' : _c, _d = _a.sort, sort = _d === void 0 ? '' : _d, _e = _a.page, page = _e === void 0 ? 1 : _e, _f = _a.perPage, perPage = _f === void 0 ? 12 : _f;
    return function (dispatch, getState) {
        return dispatch({
            type: brand["b" /* FETCH_PRODUCT_BY_BRAND_ID */],
            payload: { promise: fetchProductByBrandId({ id: id, page: page, perPage: perPage, pl: pl, ph: ph, sort: sort }).then(function (res) { return res; }) },
            meta: { id: id, page: page, perPage: perPage }
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnJhbmQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJicmFuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsY0FBYyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSx5QkFBeUIsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBRXJGLHVCQUF1QjtBQUN2QixNQUFNLENBQUMsSUFBTSxvQkFBb0IsR0FDL0IsY0FBTSxPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7SUFDdkIsT0FBQSxRQUFRLENBQUM7UUFDUCxJQUFJLEVBQUUsZ0JBQWdCO1FBQ3RCLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxjQUFjLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7S0FDeEQsQ0FBQztBQUhGLENBR0UsRUFKRSxDQUlGLENBQUM7QUFFUCx5Q0FBeUM7QUFDekMsTUFBTSxDQUFDLElBQU0sMkJBQTJCLEdBQUcsVUFBQyxFQU01QjtRQUxkLFVBQUUsRUFDRixVQUFPLEVBQVAsNEJBQU8sRUFDUCxVQUFPLEVBQVAsNEJBQU8sRUFDUCxZQUFTLEVBQVQsOEJBQVMsRUFDVCxZQUFRLEVBQVIsNkJBQVEsRUFDUixlQUFZLEVBQVosaUNBQVk7SUFDWixPQUFBLFVBQUMsUUFBUSxFQUFFLFFBQVE7UUFDakIsT0FBQSxRQUFRLENBQUM7WUFDUCxJQUFJLEVBQUUseUJBQXlCO1lBQy9CLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxxQkFBcUIsQ0FBQyxFQUFFLEVBQUUsSUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLEVBQUUsSUFBQSxFQUFFLEVBQUUsSUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEVBQUgsQ0FBRyxDQUFDLEVBQUU7WUFDakcsSUFBSSxFQUFFLEVBQUUsRUFBRSxJQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUU7U0FDNUIsQ0FBQztJQUpGLENBSUU7QUFMSixDQUtJLENBQUMifQ==

/***/ }),

/***/ 871:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/encode.ts
var encode = __webpack_require__(7);

// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// EXTERNAL MODULE: ./utils/format.ts
var format = __webpack_require__(116);

// EXTERNAL MODULE: ./constants/application/group-object-type.ts
var group_object_type = __webpack_require__(806);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./constants/application/key-word.ts
var key_word = __webpack_require__(767);

// EXTERNAL MODULE: ./action/meta.ts
var meta = __webpack_require__(754);

// EXTERNAL MODULE: ./action/brand.ts + 1 modules
var brand = __webpack_require__(798);

// EXTERNAL MODULE: ./action/tracking.ts + 1 modules
var tracking = __webpack_require__(777);

// CONCATENATED MODULE: ./container/app-shop/brand/store.tsx



var mapStateToProps = function (state) { return ({
    brandStore: state.brand,
    trackingStore: state.tracking
}); };
var mapDispatchToProps = function (dispatch) { return ({
    fetchProductByBrandIdAction: function (data) { return dispatch(Object(brand["b" /* fetchProductByBrandIdAction */])(data)); },
    trackingViewGroupAction: function (data) { return dispatch(Object(tracking["f" /* trackingViewGroupAction */])(data)); },
    saveProductTrackingAction: function (data) { return dispatch(Object(tracking["c" /* saveProductTrackingAction */])(data)); },
    updateMetaInfoAction: function (data) { return dispatch(Object(meta["a" /* updateMetaInfoAction */])(data)); }
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDNUQsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDcEUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLHlCQUF5QixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFJOUYsTUFBTSxDQUFDLElBQU0sZUFBZSxHQUFHLFVBQUMsS0FBSyxJQUFLLE9BQUEsQ0FBQztJQUN6QyxVQUFVLEVBQUUsS0FBSyxDQUFDLEtBQUs7SUFDdkIsYUFBYSxFQUFFLEtBQUssQ0FBQyxRQUFRO0NBQ25CLENBQUEsRUFIOEIsQ0FHOUIsQ0FBQztBQUViLE1BQU0sQ0FBQyxJQUFNLGtCQUFrQixHQUFHLFVBQUMsUUFBUSxJQUFLLE9BQUEsQ0FBQztJQUMvQywyQkFBMkIsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUEzQyxDQUEyQztJQUN2Rix1QkFBdUIsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUF2QyxDQUF1QztJQUMvRSx5QkFBeUIsRUFBRSxVQUFDLElBQVMsSUFBSyxPQUFBLFFBQVEsQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUF6QyxDQUF5QztJQUNuRixvQkFBb0IsRUFBRSxVQUFDLElBQUksSUFBSyxPQUFBLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFwQyxDQUFvQztDQUMxRCxDQUFBLEVBTG9DLENBS3BDLENBQUMifQ==
// CONCATENATED MODULE: ./container/app-shop/brand/initialize.tsx
var DEFAULT_PROPS = {
    type: 'full',
    column: 4,
    perPage: 24
};
var INITIAL_STATE = {
    urlList: [],
    page: 1,
    isSubCategoryOnTop: false,
    heightSubCategoryToTop: 0
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixJQUFJLEVBQUUsTUFBTTtJQUNaLE1BQU0sRUFBRSxDQUFDO0lBQ1QsT0FBTyxFQUFFLEVBQUU7Q0FDRixDQUFDO0FBRVosTUFBTSxDQUFDLElBQU0sYUFBYSxHQUFHO0lBQzNCLE9BQU8sRUFBRSxFQUFFO0lBQ1gsSUFBSSxFQUFFLENBQUM7SUFDUCxrQkFBa0IsRUFBRSxLQUFLO0lBQ3pCLHNCQUFzQixFQUFFLENBQUM7Q0FDaEIsQ0FBQyJ9
// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// EXTERNAL MODULE: ./container/layout/split/index.tsx
var split = __webpack_require__(753);

// EXTERNAL MODULE: ./components/general/pagination/index.tsx + 4 modules
var pagination = __webpack_require__(751);

// EXTERNAL MODULE: ./container/layout/main-block/index.tsx
var main_block = __webpack_require__(746);

// EXTERNAL MODULE: ./container/layout/wrap/index.tsx
var wrap = __webpack_require__(745);

// EXTERNAL MODULE: ./components/product/detail-item/index.tsx + 5 modules
var detail_item = __webpack_require__(780);

// EXTERNAL MODULE: ./components/ui/loading-placeholder/index.tsx + 4 modules
var loading_placeholder = __webpack_require__(750);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// EXTERNAL MODULE: ./style/media-queries.ts
var media_queries = __webpack_require__(76);

// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./container/app-shop/brand/style.tsx



/* harmony default export */ var style = ({
    container: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{ paddingTop: 0 }],
        DESKTOP: [{ paddingTop: 30 }],
        GENERAL: [{
                display: 'block',
                position: 'relative',
                zIndex: variable["zIndex5"],
            }]
    }),
    row: {
        display: variable["display"].flex,
        flexWrap: 'wrap',
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 5,
    },
    itemWrap: {
        padding: 5
    },
    column4: (style_a = {
            width: '50%'
        },
        style_a[media_queries["a" /* default */].tablet960] = {
            width: '25%',
        },
        style_a),
    column5: (style_b = {
            width: '50%'
        },
        style_b[media_queries["a" /* default */].tablet960] = {
            width: '20%',
        },
        style_b),
    customStyleLoading: {
        height: 400
    },
    placeholder: {
        width: '100%',
        paddingTop: 20,
        title: {
            background: variable["colorF0"],
            display: variable["display"].block,
            width: '50%',
            height: 40,
            margin: '0 auto 30px'
        },
        titleMobile: {
            margin: 0,
            textAlign: 'left'
        },
        control: {
            width: '100%',
            display: variable["display"].flex,
            justifyContent: 'space-between',
            paddingLeft: 10,
            paddingRight: 10,
            marginBottom: 20,
        },
        controlItem: {
            width: 150,
            height: 30,
            background: variable["colorF7"]
        },
        productList: {
            display: variable["display"].flex,
            flexWrap: 'wrap',
            paddingTop: 20,
        },
        productMobileItem: {
            minWidth: '50%',
            width: '50%',
        },
        productItem: {
            flex: 1,
            paddingLeft: 10,
            paddingRight: 10,
            marginBottom: 20,
            minWidth: '20%',
            width: '20%',
            image: {
                width: '100%',
                height: 'auto',
                paddingTop: '82%',
                marginBottom: 10,
            },
            text: {
                width: '94%',
                height: 25,
                marginBottom: 10,
            },
            lastText: {
                width: '65%',
                height: 25,
            }
        },
    },
    headerMenuContainer: {
        display: variable["display"].block,
        position: variable["position"].relative,
        height: 50,
        maxHeight: 50,
        background: variable["colorWhite08"],
        headerMenuWrap: {
            width: '100%',
            height: '100%',
            display: variable["display"].flex,
            position: variable["position"].relative,
        },
        headerMenu: {
            display: variable["display"].flex,
            alignItems: 'center',
            justifyContent: 'flex-start',
            fontFamily: variable["fontAvenirMedium"],
            borderBottom: "1px solid " + variable["colorBlack005"],
            background: variable["colorWhite08"],
            backdropFilter: "blur(10px)",
            WebkitBackdropFilter: "blur(10px)",
            height: '100%',
            width: '100%',
            position: variable["position"].absolute,
            top: 0,
            left: 0,
            isTop: {
                position: variable["position"].fixed,
                top: 0,
                left: 0,
                width: '100%',
                height: 50,
                zIndex: variable["zIndexMax"]
            },
            textBreadCrumb: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        fontSize: 15,
                        lineHeight: '50px',
                        height: '100%',
                        width: '100%',
                        paddingLeft: 10,
                        color: variable["colorBlack"],
                        fontFamily: variable["fontTrirong"],
                        display: variable["display"].inlineBlock
                    }],
                DESKTOP: [{
                        fontSize: 18,
                        height: 60,
                        lineHeight: '60px',
                        textTransform: 'capitalize'
                    }],
                GENERAL: [{
                        color: variable["colorBlack"],
                        cursor: 'pointer',
                    }]
            }),
            icon: function (isOpening) { return ({
                width: 50,
                height: 50,
                color: variable["colorBlack08"],
                transition: variable["transitionNormal"],
                transform: isOpening ? 'rotate(-180deg)' : 'rotate(0deg)'
            }); },
            inner: {
                width: 10,
            }
        }
    },
    subContent: {
        container: function (existedImage) { return Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    marginTop: 0,
                    marginBottom: 10,
                    paddingLeft: 10,
                    paddingRight: 10,
                    paddingTop: 10,
                    paddingBottom: 10,
                }],
            DESKTOP: [{ marginTop: 10, marginBottom: 30, }],
            GENERAL: [{
                    position: variable["position"].sticky,
                    top: 20,
                    width: '100%',
                    maxWidth: '100%'
                }]
        }); },
        wrap: {
            container: function (existedImage) { return ({
                position: variable["position"].relative,
                paddingTop: existedImage ? 20 : 10,
                boxShadow: variable["shadowBlurSort"],
                borderRadius: 5,
            }); },
            imgContainer: function (imgUrl) { return ({
                width: 200,
                margin: '0 auto',
                height: 'auto',
                borderRadius: '50%',
                zIndex: variable["zIndex9"],
                backgroundImage: "url(" + imgUrl + ")",
                backgroundSize: 'contain',
                backgroundPosition: 'center center',
                backgroundRepeat: 'no-repeat',
                backgroundColor: variable["colorWhite"],
                display: variable["display"].block,
                marginBottom: 20
            }); },
            content: {
                paddingLeft: 20,
                paddingRight: 20,
                paddingBottom: 20,
                brandName: {
                    fontSize: 22,
                    fontFamily: variable["fontTrirong"],
                    textAlign: 'center',
                    marginBottom: 10,
                    textTransform: 'uppercase',
                    wordBreak: 'break-all'
                },
                description: {
                    textAlign: 'justify',
                    fontSize: 16,
                    color: variable["colorBlack06"]
                }
            }
        }
    }
});
var style_a, style_b;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLGFBQWEsTUFBTSw4QkFBOEIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFekQsZUFBZTtJQUNiLFNBQVMsRUFBRSxZQUFZLENBQUM7UUFDdEIsTUFBTSxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsQ0FBQyxFQUFFLENBQUM7UUFDM0IsT0FBTyxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsRUFBRSxFQUFFLENBQUM7UUFDN0IsT0FBTyxFQUFFLENBQUM7Z0JBQ1IsT0FBTyxFQUFFLE9BQU87Z0JBQ2hCLFFBQVEsRUFBRSxVQUFVO2dCQUNwQixNQUFNLEVBQUUsUUFBUSxDQUFDLE9BQU87YUFDekIsQ0FBQztLQUNILENBQUM7SUFFRixHQUFHLEVBQUU7UUFDSCxPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1FBQzlCLFFBQVEsRUFBRSxNQUFNO1FBQ2hCLFdBQVcsRUFBRSxDQUFDO1FBQ2QsWUFBWSxFQUFFLENBQUM7UUFDZixhQUFhLEVBQUUsQ0FBQztLQUNqQjtJQUVELFFBQVEsRUFBRTtRQUNSLE9BQU8sRUFBRSxDQUFDO0tBQ1g7SUFFRCxPQUFPO1lBQ0wsS0FBSyxFQUFFLEtBQUs7O1FBRVosR0FBQyxhQUFhLENBQUMsU0FBUyxJQUFHO1lBQ3pCLEtBQUssRUFBRSxLQUFLO1NBQ2I7V0FDRjtJQUVELE9BQU87WUFDTCxLQUFLLEVBQUUsS0FBSzs7UUFFWixHQUFDLGFBQWEsQ0FBQyxTQUFTLElBQUc7WUFDekIsS0FBSyxFQUFFLEtBQUs7U0FDYjtXQUNGO0lBRUQsa0JBQWtCLEVBQUU7UUFDbEIsTUFBTSxFQUFFLEdBQUc7S0FDWjtJQUVELFdBQVcsRUFBRTtRQUNYLEtBQUssRUFBRSxNQUFNO1FBQ2IsVUFBVSxFQUFFLEVBQUU7UUFFZCxLQUFLLEVBQUU7WUFDTCxVQUFVLEVBQUUsUUFBUSxDQUFDLE9BQU87WUFDNUIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztZQUMvQixLQUFLLEVBQUUsS0FBSztZQUNaLE1BQU0sRUFBRSxFQUFFO1lBQ1YsTUFBTSxFQUFFLGFBQWE7U0FDdEI7UUFFRCxXQUFXLEVBQUU7WUFDWCxNQUFNLEVBQUUsQ0FBQztZQUNULFNBQVMsRUFBRSxNQUFNO1NBQ2xCO1FBRUQsT0FBTyxFQUFFO1lBQ1AsS0FBSyxFQUFFLE1BQU07WUFDYixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzlCLGNBQWMsRUFBRSxlQUFlO1lBQy9CLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLEVBQUU7WUFDaEIsWUFBWSxFQUFFLEVBQUU7U0FDakI7UUFFRCxXQUFXLEVBQUU7WUFDWCxLQUFLLEVBQUUsR0FBRztZQUNWLE1BQU0sRUFBRSxFQUFFO1lBQ1YsVUFBVSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1NBQzdCO1FBRUQsV0FBVyxFQUFFO1lBQ1gsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixRQUFRLEVBQUUsTUFBTTtZQUNoQixVQUFVLEVBQUUsRUFBRTtTQUNmO1FBRUQsaUJBQWlCLEVBQUU7WUFDakIsUUFBUSxFQUFFLEtBQUs7WUFDZixLQUFLLEVBQUUsS0FBSztTQUNiO1FBRUQsV0FBVyxFQUFFO1lBQ1gsSUFBSSxFQUFFLENBQUM7WUFDUCxXQUFXLEVBQUUsRUFBRTtZQUNmLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFFBQVEsRUFBRSxLQUFLO1lBQ2YsS0FBSyxFQUFFLEtBQUs7WUFFWixLQUFLLEVBQUU7Z0JBQ0wsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsTUFBTSxFQUFFLE1BQU07Z0JBQ2QsVUFBVSxFQUFFLEtBQUs7Z0JBQ2pCLFlBQVksRUFBRSxFQUFFO2FBQ2pCO1lBRUQsSUFBSSxFQUFFO2dCQUNKLEtBQUssRUFBRSxLQUFLO2dCQUNaLE1BQU0sRUFBRSxFQUFFO2dCQUNWLFlBQVksRUFBRSxFQUFFO2FBQ2pCO1lBRUQsUUFBUSxFQUFFO2dCQUNSLEtBQUssRUFBRSxLQUFLO2dCQUNaLE1BQU0sRUFBRSxFQUFFO2FBQ1g7U0FDRjtLQUNGO0lBRUQsbUJBQW1CLEVBQUU7UUFDbkIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztRQUMvQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1FBQ3BDLE1BQU0sRUFBRSxFQUFFO1FBQ1YsU0FBUyxFQUFFLEVBQUU7UUFDYixVQUFVLEVBQUUsUUFBUSxDQUFDLFlBQVk7UUFFakMsY0FBYyxFQUFFO1lBQ2QsS0FBSyxFQUFFLE1BQU07WUFDYixNQUFNLEVBQUUsTUFBTTtZQUNkLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUTtTQUNyQztRQUVELFVBQVUsRUFBRTtZQUNWLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsVUFBVSxFQUFFLFFBQVE7WUFDcEIsY0FBYyxFQUFFLFlBQVk7WUFDNUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxnQkFBZ0I7WUFDckMsWUFBWSxFQUFFLGVBQWEsUUFBUSxDQUFDLGFBQWU7WUFDbkQsVUFBVSxFQUFFLFFBQVEsQ0FBQyxZQUFZO1lBQ2pDLGNBQWMsRUFBRSxZQUFZO1lBQzVCLG9CQUFvQixFQUFFLFlBQVk7WUFDbEMsTUFBTSxFQUFFLE1BQU07WUFDZCxLQUFLLEVBQUUsTUFBTTtZQUNiLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDcEMsR0FBRyxFQUFFLENBQUM7WUFDTixJQUFJLEVBQUUsQ0FBQztZQUVQLEtBQUssRUFBRTtnQkFDTCxRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLO2dCQUNqQyxHQUFHLEVBQUUsQ0FBQztnQkFDTixJQUFJLEVBQUUsQ0FBQztnQkFDUCxLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsRUFBRTtnQkFDVixNQUFNLEVBQUUsUUFBUSxDQUFDLFNBQVM7YUFDM0I7WUFFRCxjQUFjLEVBQUUsWUFBWSxDQUFDO2dCQUMzQixNQUFNLEVBQUUsQ0FBQzt3QkFDUCxRQUFRLEVBQUUsRUFBRTt3QkFDWixVQUFVLEVBQUUsTUFBTTt3QkFDbEIsTUFBTSxFQUFFLE1BQU07d0JBQ2QsS0FBSyxFQUFFLE1BQU07d0JBQ2IsV0FBVyxFQUFFLEVBQUU7d0JBQ2YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO3dCQUMxQixVQUFVLEVBQUUsUUFBUSxDQUFDLFdBQVc7d0JBQ2hDLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLFdBQVc7cUJBQ3RDLENBQUM7Z0JBRUYsT0FBTyxFQUFFLENBQUM7d0JBQ1IsUUFBUSxFQUFFLEVBQUU7d0JBQ1osTUFBTSxFQUFFLEVBQUU7d0JBQ1YsVUFBVSxFQUFFLE1BQU07d0JBQ2xCLGFBQWEsRUFBRSxZQUFZO3FCQUM1QixDQUFDO2dCQUVGLE9BQU8sRUFBRSxDQUFDO3dCQUNSLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTt3QkFDMUIsTUFBTSxFQUFFLFNBQVM7cUJBQ2xCLENBQUM7YUFDSCxDQUFDO1lBRUYsSUFBSSxFQUFFLFVBQUMsU0FBUyxJQUFLLE9BQUEsQ0FBQztnQkFDcEIsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxZQUFZO2dCQUM1QixVQUFVLEVBQUUsUUFBUSxDQUFDLGdCQUFnQjtnQkFDckMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLGNBQWM7YUFDMUQsQ0FBQyxFQU5tQixDQU1uQjtZQUVGLEtBQUssRUFBRTtnQkFDTCxLQUFLLEVBQUUsRUFBRTthQUNWO1NBQ0Y7S0FDRjtJQUVELFVBQVUsRUFBRTtRQUNWLFNBQVMsRUFBRSxVQUFDLFlBQVksSUFBSyxPQUFBLFlBQVksQ0FBQztZQUN4QyxNQUFNLEVBQUUsQ0FBQztvQkFDUCxTQUFTLEVBQUUsQ0FBQztvQkFDWixZQUFZLEVBQUUsRUFBRTtvQkFDaEIsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLFVBQVUsRUFBRSxFQUFFO29CQUNkLGFBQWEsRUFBRSxFQUFFO2lCQUNsQixDQUFDO1lBQ0YsT0FBTyxFQUFFLENBQUMsRUFBRSxTQUFTLEVBQUUsRUFBRSxFQUFFLFlBQVksRUFBRSxFQUFFLEdBQUcsQ0FBQztZQUMvQyxPQUFPLEVBQUUsQ0FBQztvQkFDUixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxNQUFNO29CQUNsQyxHQUFHLEVBQUUsRUFBRTtvQkFDUCxLQUFLLEVBQUUsTUFBTTtvQkFDYixRQUFRLEVBQUUsTUFBTTtpQkFDakIsQ0FBQztTQUNILENBQUMsRUFoQjJCLENBZ0IzQjtRQUVGLElBQUksRUFBRTtZQUNKLFNBQVMsRUFBRSxVQUFDLFlBQVksSUFBSyxPQUFBLENBQUM7Z0JBQzVCLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3BDLFVBQVUsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDbEMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxjQUFjO2dCQUNsQyxZQUFZLEVBQUUsQ0FBQzthQUNoQixDQUFDLEVBTDJCLENBSzNCO1lBRUYsWUFBWSxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztnQkFDekIsS0FBSyxFQUFFLEdBQUc7Z0JBQ1YsTUFBTSxFQUFFLFFBQVE7Z0JBQ2hCLE1BQU0sRUFBRSxNQUFNO2dCQUNkLFlBQVksRUFBRSxLQUFLO2dCQUNuQixNQUFNLEVBQUUsUUFBUSxDQUFDLE9BQU87Z0JBQ3hCLGVBQWUsRUFBRSxTQUFPLE1BQU0sTUFBRztnQkFDakMsY0FBYyxFQUFFLFNBQVM7Z0JBQ3pCLGtCQUFrQixFQUFFLGVBQWU7Z0JBQ25DLGdCQUFnQixFQUFFLFdBQVc7Z0JBQzdCLGVBQWUsRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDcEMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztnQkFDL0IsWUFBWSxFQUFFLEVBQUU7YUFDakIsQ0FBQyxFQWJ3QixDQWF4QjtZQUVGLE9BQU8sRUFBRTtnQkFDUCxXQUFXLEVBQUUsRUFBRTtnQkFDZixZQUFZLEVBQUUsRUFBRTtnQkFDaEIsYUFBYSxFQUFFLEVBQUU7Z0JBRWpCLFNBQVMsRUFBRTtvQkFDVCxRQUFRLEVBQUUsRUFBRTtvQkFDWixVQUFVLEVBQUUsUUFBUSxDQUFDLFdBQVc7b0JBQ2hDLFNBQVMsRUFBRSxRQUFRO29CQUNuQixZQUFZLEVBQUUsRUFBRTtvQkFDaEIsYUFBYSxFQUFFLFdBQVc7b0JBQzFCLFNBQVMsRUFBRSxXQUFXO2lCQUN2QjtnQkFFRCxXQUFXLEVBQUU7b0JBQ1gsU0FBUyxFQUFFLFNBQVM7b0JBQ3BCLFFBQVEsRUFBRSxFQUFFO29CQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTtpQkFDN0I7YUFDRjtTQUNGO0tBRUY7Q0FDSyxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/brand/view.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};














var renderItemPlaceholder = function (item) { return (react["createElement"]("div", { style: [
        style.placeholder.productItem,
        'MOBILE' === window.DEVICE_VERSION && style.placeholder.productMobileItem
    ], key: item },
    react["createElement"](loading_placeholder["a" /* default */], { style: style.placeholder.productItem.image }),
    react["createElement"](loading_placeholder["a" /* default */], { style: style.placeholder.productItem.text }),
    react["createElement"](loading_placeholder["a" /* default */], { style: style.placeholder.productItem.text }),
    react["createElement"](loading_placeholder["a" /* default */], { style: style.placeholder.productItem.lastText }))); };
var renderLoadingPlaceholder = function () {
    var list = 'MOBILE' === window.DEVICE_VERSION ? [1, 2, 3, 4] : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
    return (react["createElement"]("div", { style: style.placeholder },
        react["createElement"](loading_placeholder["a" /* default */], { style: [style.placeholder.title, 'MOBILE' === window.DEVICE_VERSION && style.placeholder.titleMobile] }),
        react["createElement"]("div", { style: style.placeholder.productList }, Array.isArray(list) && list.map(renderItemPlaceholder))));
};
var renderHeader = function (_a) {
    var title = _a.title, _b = _a.isSubCategoryOnTop, isSubCategoryOnTop = _b === void 0 ? false : _b;
    var headerStyle = style.headerMenuContainer;
    var menuIconProps = {
        name: 'angle-down',
        innerStyle: headerStyle.headerMenu.inner,
        style: headerStyle.headerMenu.icon(false)
    };
    return (react["createElement"](react_router_dom["NavLink"], { to: routing["Qa" /* ROUTING_MOBILE_BRAND_PATH */], style: headerStyle },
        react["createElement"]("div", { style: [headerStyle.headerMenu, isSubCategoryOnTop && headerStyle.headerMenu.isTop], id: 'brand-detail-menu', className: 'backdrop-blur' },
            react["createElement"]("div", { style: headerStyle.headerMenuWrap },
                react["createElement"]("div", { style: headerStyle.headerMenu.textBreadCrumb }, title),
                react["createElement"](icon["a" /* default */], __assign({}, menuIconProps))))));
};
var renderContent = function (_a) {
    var list = _a.list, paginationProps = _a.paginationProps, type = _a.type, column = _a.column, trackingCode = _a.trackingCode;
    return typeof list === 'undefined' || list === null || list.length === 0
        ? renderLoadingPlaceholder()
        : (react["createElement"]("div", null,
            react["createElement"]("div", { style: style.row }, Array.isArray(list)
                && list.map(function (product) {
                    var productSlug = !!trackingCode ? product.slug + "?" + key_word["a" /* KEY_WORD */].TRACKING_CODE + "=" + trackingCode : "" + product.slug;
                    var productProps = { type: type, data: Object.assign({}, product, { slug: productSlug }), showQuickView: true };
                    return (react["createElement"]("div", { key: "brand-product-" + product.id, style: [style.itemWrap, style["column" + column]] },
                        react["createElement"](detail_item["a" /* default */], __assign({}, productProps))));
                })),
            react["createElement"](pagination["a" /* default */], __assign({}, paginationProps))));
};
var renderBrandContent = function (_a) {
    var name = _a.name, description = _a.description;
    return (react["createElement"]("div", { style: style.subContent.wrap.content },
        react["createElement"]("div", { style: style.subContent.wrap.content.brandName }, name),
        react["createElement"]("div", { style: style.subContent.wrap.content.description }, description)));
};
var renderSubContent = function (_a) {
    var brandDetail = _a.brandDetail;
    var existedImage = brandDetail
        && brandDetail.brand_image_url !== null
        && brandDetail.brand_image_url !== '/images/original/missing.png';
    return (react["createElement"]("div", { style: style.subContent.container(existedImage) },
        react["createElement"]("div", { style: style.subContent.wrap.container(existedImage) },
            existedImage && react["createElement"]("img", { style: style.subContent.wrap.imgContainer(), src: brandDetail && brandDetail.brand_image_url || '' }),
            renderBrandContent({ name: brandDetail && brandDetail.name || '', description: brandDetail && brandDetail.description || '' }))));
};
var renderView = function (_a) {
    var props = _a.props, state = _a.state;
    var _b = props, type = _b.type, column = _b.column, perPage = _b.perPage, brandStore = _b.brandStore, idBrand = _b.match.params.idBrand, viewGroupTrackingList = _b.trackingStore.viewGroupTrackingList;
    var _c = state, urlList = _c.urlList, page = _c.page, isSubCategoryOnTop = _c.isSubCategoryOnTop;
    var keyHashBrand = Object(encode["j" /* objectToHash */])({ id: idBrand, page: page, perPage: perPage });
    var productBrandList = brandStore.productByBrandId[keyHashBrand] || [];
    var brandBoxesList = productBrandList && productBrandList.boxes || [];
    var brandDetail = productBrandList && productBrandList.brand || {};
    var _urlList = 0 !== brandBoxesList.length ? urlList : [];
    var _d = 0 !== productBrandList.length && productBrandList.paging || { current_page: 0, per_page: 0, total_pages: 0 }, current_page = _d.current_page, per_page = _d.per_page, total_pages = _d.total_pages;
    ;
    var paginationProps = {
        current: current_page,
        per: per_page,
        total: total_pages,
        urlList: _urlList
    };
    var title = productBrandList && productBrandList.brand
        ? "Th\u01B0\u01A1ng hi\u1EC7u: #" + productBrandList.brand.name : '';
    var keyHashCode = Object(encode["k" /* stringToHash */])(idBrand);
    var trackingCode = viewGroupTrackingList
        && !Object(validate["l" /* isUndefined */])(viewGroupTrackingList[keyHashCode])
        && viewGroupTrackingList[keyHashCode].trackingCode || '';
    var mainBlockMobileProps = {
        showHeader: false,
        showViewMore: false,
        content: react["createElement"]("div", null,
            renderHeader({ title: title, isSubCategoryOnTop: isSubCategoryOnTop }),
            renderContent({ list: brandBoxesList, paginationProps: paginationProps, type: type, column: column, trackingCode: trackingCode })),
        style: {}
    };
    var mainBlockDesktopProps = {
        title: title,
        style: {},
        showHeader: true,
        showViewMore: false,
        content: renderContent({ list: brandBoxesList, paginationProps: paginationProps, type: type, column: column, trackingCode: trackingCode })
    };
    var switchVersion = {
        MOBILE: mainBlockMobileProps,
        DESKTOP: mainBlockDesktopProps
    };
    var mainBlockProps = switchVersion[window.DEVICE_VERSION];
    var splitLayoutProps = {
        subContainer: renderSubContent({ brandDetail: brandDetail }),
        mainContainer: react["createElement"](main_block["a" /* default */], __assign({}, mainBlockProps))
    };
    return (react["createElement"]("brand-container", { style: style.container },
        react["createElement"](wrap["a" /* default */], null, !!brandDetail.description
            ? react["createElement"](split["a" /* default */], __assign({}, splitLayoutProps))
            : react["createElement"](main_block["a" /* default */], __assign({}, mainBlockProps)))));
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNDLE9BQU8sSUFBSSxNQUFNLDZCQUE2QixDQUFDO0FBRS9DLE9BQU8sV0FBVyxNQUFNLG9CQUFvQixDQUFDO0FBQzdDLE9BQU8sVUFBVSxNQUFNLHdDQUF3QyxDQUFDO0FBQ2hFLE9BQU8sU0FBUyxNQUFNLHlCQUF5QixDQUFDO0FBQ2hELE9BQU8sVUFBVSxNQUFNLG1CQUFtQixDQUFDO0FBQzNDLE9BQU8saUJBQWlCLE1BQU0seUNBQXlDLENBQUM7QUFDeEUsT0FBTyxrQkFBa0IsTUFBTSw0Q0FBNEMsQ0FBQztBQUU1RSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDbkUsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDbkYsT0FBTyxFQUFFLFdBQVcsRUFBaUIsTUFBTSx5QkFBeUIsQ0FBQztBQUNyRSxPQUFPLEVBQUUsWUFBWSxFQUFFLFlBQVksRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBRW5FLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUk1QixNQUFNLENBQUMsSUFBTSxxQkFBcUIsR0FBRyxVQUFDLElBQUksSUFBSyxPQUFBLENBQzdDLDZCQUNFLEtBQUssRUFBRTtRQUNMLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVztRQUM3QixRQUFRLEtBQUssTUFBTSxDQUFDLGNBQWMsSUFBSSxLQUFLLENBQUMsV0FBVyxDQUFDLGlCQUFpQjtLQUMxRSxFQUNELEdBQUcsRUFBRSxJQUFJO0lBQ1Qsb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBSTtJQUNsRSxvQkFBQyxrQkFBa0IsSUFBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxHQUFJO0lBQ2pFLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLEdBQUk7SUFDakUsb0JBQUMsa0JBQWtCLElBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLFFBQVEsR0FBSSxDQUNqRSxDQUNQLEVBWjhDLENBWTlDLENBQUM7QUFFRixJQUFNLHdCQUF3QixHQUFHO0lBQy9CLElBQU0sSUFBSSxHQUFHLFFBQVEsS0FBSyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDckgsTUFBTSxDQUFDLENBQ0wsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXO1FBQzNCLG9CQUFDLGtCQUFrQixJQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLFFBQVEsS0FBSyxNQUFNLENBQUMsY0FBYyxJQUFJLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEdBQUk7UUFDN0gsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxJQUN0QyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FDbkQsQ0FDRixDQUNQLENBQUE7QUFDSCxDQUFDLENBQUM7QUFFRixJQUFNLFlBQVksR0FBRyxVQUFDLEVBQXFDO1FBQW5DLGdCQUFLLEVBQUUsMEJBQTBCLEVBQTFCLCtDQUEwQjtJQUN2RCxJQUFNLFdBQVcsR0FBRyxLQUFLLENBQUMsbUJBQW1CLENBQUM7SUFFOUMsSUFBTSxhQUFhLEdBQUc7UUFDcEIsSUFBSSxFQUFFLFlBQVk7UUFDbEIsVUFBVSxFQUFFLFdBQVcsQ0FBQyxVQUFVLENBQUMsS0FBSztRQUN4QyxLQUFLLEVBQUUsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO0tBQzFDLENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCxvQkFBQyxPQUFPLElBQUMsRUFBRSxFQUFFLHlCQUF5QixFQUFFLEtBQUssRUFBRSxXQUFXO1FBQ3hELDZCQUFLLEtBQUssRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEVBQUUsa0JBQWtCLElBQUksV0FBVyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLEVBQUUsbUJBQW1CLEVBQUUsU0FBUyxFQUFFLGVBQWU7WUFDM0ksNkJBQUssS0FBSyxFQUFFLFdBQVcsQ0FBQyxjQUFjO2dCQUNwQyw2QkFBSyxLQUFLLEVBQUUsV0FBVyxDQUFDLFVBQVUsQ0FBQyxjQUFjLElBQUcsS0FBSyxDQUFPO2dCQUNoRSxvQkFBQyxJQUFJLGVBQUssYUFBYSxFQUFJLENBQ3ZCLENBQ0YsQ0FDRSxDQUNYLENBQUE7QUFDSCxDQUFDLENBQUM7QUFFRixJQUFNLGFBQWEsR0FBRyxVQUFDLEVBQXFEO1FBQW5ELGNBQUksRUFBRSxvQ0FBZSxFQUFFLGNBQUksRUFBRSxrQkFBTSxFQUFFLDhCQUFZO0lBQ3hFLE1BQU0sQ0FBQyxPQUFPLElBQUksS0FBSyxXQUFXLElBQUksSUFBSSxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLENBQUM7UUFDdEUsQ0FBQyxDQUFDLHdCQUF3QixFQUFFO1FBQzVCLENBQUMsQ0FBQyxDQUNBO1lBQ0UsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxHQUFHLElBRWpCLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO21CQUNoQixJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUMsT0FBTztvQkFDbEIsSUFBTSxXQUFXLEdBQUcsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUksT0FBTyxDQUFDLElBQUksU0FBSSxRQUFRLENBQUMsYUFBYSxTQUFJLFlBQWMsQ0FBQyxDQUFDLENBQUMsS0FBRyxPQUFPLENBQUMsSUFBTSxDQUFDO29CQUNySCxJQUFNLFlBQVksR0FBRyxFQUFFLElBQUksTUFBQSxFQUFFLElBQUksRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxPQUFPLEVBQUUsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLENBQUMsRUFBRSxhQUFhLEVBQUUsSUFBSSxFQUFFLENBQUE7b0JBRTNHLE1BQU0sQ0FBQyxDQUNMLDZCQUNFLEdBQUcsRUFBRSxtQkFBaUIsT0FBTyxDQUFDLEVBQUksRUFDbEMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsV0FBUyxNQUFRLENBQUMsQ0FBQzt3QkFDakQsb0JBQUMsaUJBQWlCLGVBQUssWUFBWSxFQUFJLENBQ25DLENBQ1AsQ0FBQTtnQkFDSCxDQUFDLENBQUMsQ0FFQTtZQUNOLG9CQUFDLFVBQVUsZUFBSyxlQUFlLEVBQUksQ0FDL0IsQ0FDUCxDQUFBO0FBQ0wsQ0FBQyxDQUFDO0FBRUYsSUFBTSxrQkFBa0IsR0FBRyxVQUFDLEVBQXFCO1FBQW5CLGNBQUksRUFBRSw0QkFBVztJQUFPLE9BQUEsQ0FDcEQsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU87UUFDdkMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLElBQUcsSUFBSSxDQUFPO1FBQ2pFLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxJQUFHLFdBQVcsQ0FBTyxDQUN0RSxDQUNQO0FBTHFELENBS3JELENBQUM7QUFFRixJQUFNLGdCQUFnQixHQUFHLFVBQUMsRUFBZTtRQUFiLDRCQUFXO0lBQ3JDLElBQU0sWUFBWSxHQUFHLFdBQVc7V0FDM0IsV0FBVyxDQUFDLGVBQWUsS0FBSyxJQUFJO1dBQ3BDLFdBQVcsQ0FBQyxlQUFlLEtBQUssOEJBQThCLENBQUM7SUFFcEUsTUFBTSxDQUFDLENBQ0wsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQztRQUNsRCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQztZQUN0RCxZQUFZLElBQUksNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxFQUFFLEdBQUcsRUFBRSxXQUFXLElBQUksV0FBVyxDQUFDLGVBQWUsSUFBSSxFQUFFLEdBQUk7WUFDM0gsa0JBQWtCLENBQUMsRUFBRSxJQUFJLEVBQUUsV0FBVyxJQUFJLFdBQVcsQ0FBQyxJQUFJLElBQUksRUFBRSxFQUFFLFdBQVcsRUFBRSxXQUFXLElBQUksV0FBVyxDQUFDLFdBQVcsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUMzSCxDQUNGLENBQ1AsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLElBQU0sVUFBVSxHQUFHLFVBQUMsRUFBZ0I7UUFBZCxnQkFBSyxFQUFFLGdCQUFLO0lBQzFCLElBQUEsVUFPYSxFQU5qQixjQUFJLEVBQ0osa0JBQU0sRUFDTixvQkFBTyxFQUNQLDBCQUFVLEVBQ1MsaUNBQU8sRUFDVCw4REFBcUIsQ0FDcEI7SUFFZCxJQUFBLFVBQXVELEVBQXJELG9CQUFPLEVBQUUsY0FBSSxFQUFFLDBDQUFrQixDQUFxQjtJQUU5RCxJQUFNLFlBQVksR0FBRyxZQUFZLENBQUMsRUFBRSxFQUFFLEVBQUUsT0FBTyxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQztJQUVsRSxJQUFNLGdCQUFnQixHQUFHLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDekUsSUFBTSxjQUFjLEdBQUcsZ0JBQWdCLElBQUksZ0JBQWdCLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQztJQUN4RSxJQUFNLFdBQVcsR0FBRyxnQkFBZ0IsSUFBSSxnQkFBZ0IsQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDO0lBQ3JFLElBQU0sUUFBUSxHQUFHLENBQUMsS0FBSyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUV0RCxJQUFBLGlIQUFzSixFQUFwSiw4QkFBWSxFQUFFLHNCQUFRLEVBQUUsNEJBQVcsQ0FBa0g7SUFBQSxDQUFDO0lBQzlKLElBQU0sZUFBZSxHQUFHO1FBQ3RCLE9BQU8sRUFBRSxZQUFZO1FBQ3JCLEdBQUcsRUFBRSxRQUFRO1FBQ2IsS0FBSyxFQUFFLFdBQVc7UUFDbEIsT0FBTyxFQUFFLFFBQVE7S0FDbEIsQ0FBQztJQUVGLElBQU0sS0FBSyxHQUFHLGdCQUFnQixJQUFJLGdCQUFnQixDQUFDLEtBQUs7UUFDdEQsQ0FBQyxDQUFDLGtDQUFpQixnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsSUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFFeEQsSUFBTSxXQUFXLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzFDLElBQU0sWUFBWSxHQUFHLHFCQUFxQjtXQUNyQyxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsQ0FBQztXQUNoRCxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsQ0FBQyxZQUFZLElBQUksRUFBRSxDQUFDO0lBRTNELElBQU0sb0JBQW9CLEdBQUc7UUFDM0IsVUFBVSxFQUFFLEtBQUs7UUFDakIsWUFBWSxFQUFFLEtBQUs7UUFDbkIsT0FBTyxFQUNMO1lBQ0csWUFBWSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsa0JBQWtCLG9CQUFBLEVBQUUsQ0FBQztZQUMzQyxhQUFhLENBQUMsRUFBRSxJQUFJLEVBQUUsY0FBYyxFQUFFLGVBQWUsaUJBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxNQUFNLFFBQUEsRUFBRSxZQUFZLGNBQUEsRUFBRSxDQUFDLENBQ2pGO1FBQ1IsS0FBSyxFQUFFLEVBQUU7S0FDVixDQUFDO0lBRUYsSUFBTSxxQkFBcUIsR0FBRztRQUM1QixLQUFLLE9BQUE7UUFDTCxLQUFLLEVBQUUsRUFBRTtRQUNULFVBQVUsRUFBRSxJQUFJO1FBQ2hCLFlBQVksRUFBRSxLQUFLO1FBQ25CLE9BQU8sRUFBRSxhQUFhLENBQUMsRUFBRSxJQUFJLEVBQUUsY0FBYyxFQUFFLGVBQWUsaUJBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxNQUFNLFFBQUEsRUFBRSxZQUFZLGNBQUEsRUFBRSxDQUFDO0tBQzlGLENBQUM7SUFFRixJQUFNLGFBQWEsR0FBRztRQUNwQixNQUFNLEVBQUUsb0JBQW9CO1FBQzVCLE9BQU8sRUFBRSxxQkFBcUI7S0FDL0IsQ0FBQztJQUVGLElBQU0sY0FBYyxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUM7SUFFNUQsSUFBTSxnQkFBZ0IsR0FBRztRQUN2QixZQUFZLEVBQUUsZ0JBQWdCLENBQUMsRUFBRSxXQUFXLGFBQUEsRUFBRSxDQUFDO1FBQy9DLGFBQWEsRUFBRSxvQkFBQyxTQUFTLGVBQUssY0FBYyxFQUFjO0tBQzNELENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCx5Q0FBaUIsS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTO1FBQ3JDLG9CQUFDLFVBQVUsUUFFUCxDQUFDLENBQUMsV0FBVyxDQUFDLFdBQVc7WUFDdkIsQ0FBQyxDQUFDLG9CQUFDLFdBQVcsZUFBSyxnQkFBZ0IsRUFBSTtZQUN2QyxDQUFDLENBQUMsb0JBQUMsU0FBUyxlQUFLLGNBQWMsRUFBYyxDQUV0QyxDQUNHLENBQ25CLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixlQUFlLFVBQVUsQ0FBQyJ9
// CONCATENATED MODULE: ./container/app-shop/brand/container.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var connect = __webpack_require__(129).connect;









var container_BrandContainer = /** @class */ (function (_super) {
    __extends(BrandContainer, _super);
    function BrandContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    BrandContainer.prototype.init = function (props) {
        if (props === void 0) { props = this.props; }
        var brandStore = props.brandStore, viewGroupTrackingList = props.trackingStore.viewGroupTrackingList, fetchProductByBrandIdAction = props.fetchProductByBrandIdAction, idBrand = props.match.params.idBrand, trackingViewGroupAction = props.trackingViewGroupAction, location = props.location, perPage = props.perPage;
        var page = this.getPage();
        var params = { id: idBrand, page: page, perPage: perPage };
        var keyHashBrand = Object(encode["j" /* objectToHash */])(params);
        fetchProductByBrandIdAction(params);
        !Object(validate["l" /* isUndefined */])(brandStore.productByBrandId[keyHashBrand])
            && this.initPagination(props);
        var trackingCode = Object(format["e" /* getUrlParameter */])(location.search, key_word["a" /* KEY_WORD */].TRACKING_CODE);
        /** Tracking view group */
        var keyHashCode = Object(encode["k" /* stringToHash */])(idBrand);
        // Fisrt tracking
        trackingCode
            && 0 < trackingCode.length
            && Object(validate["l" /* isUndefined */])(viewGroupTrackingList[keyHashCode])
            && trackingViewGroupAction({ groupObjectType: group_object_type["a" /* GROUP_OBJECT_TYPE */].BRAND, groupObjectId: idBrand, campaignCode: trackingCode });
    };
    BrandContainer.prototype.initPagination = function (props) {
        if (props === void 0) { props = this.props; }
        var idBrand = props.match.params.idBrand, productByBrandId = props.brandStore.productByBrandId, perPage = props.perPage, location = props.location;
        var page = this.getPage();
        var params = { id: idBrand, page: page, perPage: perPage };
        var keyHash = Object(encode["j" /* objectToHash */])(params);
        var total_pages = (productByBrandId[keyHash]
            && productByBrandId[keyHash].paging || 0).total_pages;
        var urlList = [];
        for (var i = 1; i <= total_pages; i++) {
            urlList.push({
                number: i,
                title: i,
                link: routing["g" /* ROUTING_BRAND_DETAIL_PATH */] + "/" + idBrand + "?page=" + i
            });
        }
        this.setState({ urlList: urlList });
    };
    BrandContainer.prototype.getPage = function () {
        var page = Object(format["e" /* getUrlParameter */])(location.search, 'page') || 1;
        this.setState({ page: page });
        return page;
    };
    BrandContainer.prototype.componentDidMount = function () {
        this.init(this.props);
        window.addEventListener('scroll', this.handleScroll.bind(this));
    };
    BrandContainer.prototype.handleScroll = function () {
        var _a = this.state, isSubCategoryOnTop = _a.isSubCategoryOnTop, heightSubCategoryToTop = _a.heightSubCategoryToTop;
        var eleInfo = this.getPositionElementById('brand-detail-menu');
        eleInfo
            && eleInfo.top <= 0
            && !isSubCategoryOnTop
            && this.setState({ isSubCategoryOnTop: true, heightSubCategoryToTop: window.scrollY });
        heightSubCategoryToTop >= window.scrollY
            && isSubCategoryOnTop
            && this.setState({ isSubCategoryOnTop: false });
    };
    BrandContainer.prototype.getPositionElementById = function (elementId) {
        var el = document.getElementById(elementId);
        return el && el.getBoundingClientRect();
    };
    BrandContainer.prototype.componentWillUnmount = function () {
        window.addEventListener('scroll', function () { });
    };
    BrandContainer.prototype.componentWillReceiveProps = function (nextProps) {
        var _a = this.props, perPage = _a.perPage, location = _a.location, updateMetaInfoAction = _a.updateMetaInfoAction, idBrand = _a.match.params.idBrand, _b = _a.brandStore, isProductByBrandIdSuccess = _b.isProductByBrandIdSuccess, productByBrandId = _b.productByBrandId;
        if ((idBrand !== nextProps.match.params.idBrand)
            || (idBrand === nextProps.match.params.idBrand
                && location.search !== nextProps.location.search)) {
            this.init(nextProps);
        }
        !isProductByBrandIdSuccess
            && nextProps.brandStore.isProductByBrandIdSuccess
            && this.initPagination(nextProps);
        // Set meta for SEO
        var page = this.getPage();
        var params = { id: idBrand, page: page, perPage: perPage };
        var keyHash = Object(encode["j" /* objectToHash */])(params);
        ((Object(validate["l" /* isUndefined */])(productByBrandId[keyHash])
            && !Object(validate["l" /* isUndefined */])(nextProps.brandStore.productByBrandId[keyHash])) || !Object(validate["l" /* isUndefined */])(productByBrandId[keyHash])) && nextProps.brandStore.productByBrandId[keyHash]
            && updateMetaInfoAction({
                info: {
                    url: "http://lxbtest.tk/brand/" + nextProps.match.params.idBrand,
                    type: "article",
                    title: "Th\u01B0\u01A1ng hi\u1EC7u " + nextProps.brandStore.productByBrandId[keyHash].brand.name + " | Lixibox",
                    description: nextProps.brandStore.productByBrandId[keyHash].brand.description,
                    keyword: nextProps.brandStore.productByBrandId[keyHash].brand.name + ", lixibox, makeup, skincare, halio, okame",
                    image: 'https://assets.lixibox.com/assets/images/meta/cover.jpeg'
                }
            });
    };
    BrandContainer.prototype.render = function () {
        var args = {
            props: this.props,
            state: this.state
        };
        return view(args);
    };
    BrandContainer.defaultProps = DEFAULT_PROPS;
    BrandContainer = __decorate([
        radium
    ], BrandContainer);
    return BrandContainer;
}(react["Component"]));
;
/* harmony default export */ var container = __webpack_exports__["default"] = (connect(mapStateToProps, mapDispatchToProps)(container_BrandContainer));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFDakMsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLEVBQUUsWUFBWSxFQUFFLFlBQVksRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ25FLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDckYsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDbkYsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBR25FLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxlQUFlLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFDOUQsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDNUQsT0FBTyxVQUFVLE1BQU0sUUFBUSxDQUFDO0FBR2hDO0lBQ0Usa0NBQStCO0lBRS9CLHdCQUFZLEtBQWE7UUFBekIsWUFDRSxrQkFBTSxLQUFLLENBQUMsU0FFYjtRQURDLEtBQUksQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDOztJQUM3QixDQUFDO0lBRUQsNkJBQUksR0FBSixVQUFLLEtBQWtCO1FBQWxCLHNCQUFBLEVBQUEsUUFBUSxJQUFJLENBQUMsS0FBSztRQUVuQixJQUFBLDZCQUFVLEVBQ08saUVBQXFCLEVBQ3RDLCtEQUEyQixFQUNSLG9DQUFPLEVBQzFCLHVEQUF1QixFQUN2Qix5QkFBUSxFQUNSLHVCQUFPLENBQ0M7UUFFVixJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7UUFFNUIsSUFBTSxNQUFNLEdBQUcsRUFBRSxFQUFFLEVBQUUsT0FBTyxFQUFFLElBQUksTUFBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUM7UUFDOUMsSUFBTSxZQUFZLEdBQUcsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRTFDLDJCQUEyQixDQUFDLE1BQU0sQ0FBQyxDQUFBO1FBQ25DLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsQ0FBQztlQUNsRCxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRWhDLElBQU0sWUFBWSxHQUFHLGVBQWUsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUU5RSwwQkFBMEI7UUFDMUIsSUFBTSxXQUFXLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBRTFDLGlCQUFpQjtRQUNqQixZQUFZO2VBQ1AsQ0FBQyxHQUFHLFlBQVksQ0FBQyxNQUFNO2VBQ3ZCLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsQ0FBQztlQUMvQyx1QkFBdUIsQ0FBQyxFQUFFLGVBQWUsRUFBRSxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsYUFBYSxFQUFFLE9BQU8sRUFBRSxZQUFZLEVBQUUsWUFBWSxFQUFFLENBQUMsQ0FBQztJQUNqSSxDQUFDO0lBRUQsdUNBQWMsR0FBZCxVQUFlLEtBQWtCO1FBQWxCLHNCQUFBLEVBQUEsUUFBUSxJQUFJLENBQUMsS0FBSztRQUNKLElBQUEsb0NBQU8sRUFBb0Isb0RBQWdCLEVBQUksdUJBQU8sRUFBRSx5QkFBUSxDQUFXO1FBRXRHLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUM1QixJQUFNLE1BQU0sR0FBRyxFQUFFLEVBQUUsRUFBRSxPQUFPLEVBQUUsSUFBSSxNQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQztRQUM5QyxJQUFNLE9BQU8sR0FBRyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFN0IsSUFBQTtpRUFBVyxDQUV3QjtRQUUzQyxJQUFNLE9BQU8sR0FBZSxFQUFFLENBQUM7UUFFL0IsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxXQUFXLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUV0QyxPQUFPLENBQUMsSUFBSSxDQUFDO2dCQUNYLE1BQU0sRUFBRSxDQUFDO2dCQUNULEtBQUssRUFBRSxDQUFDO2dCQUNSLElBQUksRUFBSyx5QkFBeUIsU0FBSSxPQUFPLGNBQVMsQ0FBRzthQUMxRCxDQUFDLENBQUM7UUFDTCxDQUFDO1FBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBRUQsZ0NBQU8sR0FBUDtRQUNFLElBQU0sSUFBSSxHQUFHLGVBQWUsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDO1FBRXhCLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsMENBQWlCLEdBQWpCO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdEIsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7SUFFRCxxQ0FBWSxHQUFaO1FBQ1EsSUFBQSxlQUFxRSxFQUFuRSwwQ0FBa0IsRUFBRSxrREFBc0IsQ0FBMEI7UUFFNUUsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFFL0QsT0FBTztlQUNGLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQztlQUNoQixDQUFDLGtCQUFrQjtlQUNuQixJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsa0JBQWtCLEVBQUUsSUFBSSxFQUFFLHNCQUFzQixFQUFFLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1FBRXpGLHNCQUFzQixJQUFJLE1BQU0sQ0FBQyxPQUFPO2VBQ25DLGtCQUFrQjtlQUNsQixJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsa0JBQWtCLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsK0NBQXNCLEdBQXRCLFVBQXVCLFNBQVM7UUFDOUIsSUFBTSxFQUFFLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM5QyxNQUFNLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO0lBQzFDLENBQUM7SUFFRCw2Q0FBb0IsR0FBcEI7UUFDRSxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLGNBQVEsQ0FBQyxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVELGtEQUF5QixHQUF6QixVQUEwQixTQUFTO1FBQzNCLElBQUEsZUFNUSxFQUxaLG9CQUFPLEVBQ1Asc0JBQVEsRUFDUiw4Q0FBb0IsRUFDRCxpQ0FBTyxFQUMxQixrQkFBMkQsRUFBN0Msd0RBQXlCLEVBQUUsc0NBQWdCLENBQzVDO1FBRWYsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLEtBQUssU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDO2VBQzNDLENBQUMsT0FBTyxLQUFLLFNBQVMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU87bUJBQ3pDLFFBQVEsQ0FBQyxNQUFNLEtBQUssU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdEQsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN2QixDQUFDO1FBRUQsQ0FBQyx5QkFBeUI7ZUFDckIsU0FBUyxDQUFDLFVBQVUsQ0FBQyx5QkFBeUI7ZUFDOUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUdwQyxtQkFBbUI7UUFDbkIsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzVCLElBQU0sTUFBTSxHQUFHLEVBQUUsRUFBRSxFQUFFLE9BQU8sRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDO1FBQzlDLElBQU0sT0FBTyxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUVyQyxDQUNFLENBQ0UsV0FBVyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFDO2VBQ25DLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FDaEUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUM3QyxJQUFJLFNBQVMsQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDO2VBQzlDLG9CQUFvQixDQUFDO2dCQUN0QixJQUFJLEVBQUU7b0JBQ0osR0FBRyxFQUFFLG1DQUFpQyxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFTO29CQUN0RSxJQUFJLEVBQUUsU0FBUztvQkFDZixLQUFLLEVBQUUsZ0NBQWUsU0FBUyxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxlQUFZO29CQUMzRixXQUFXLEVBQUUsU0FBUyxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLENBQUMsV0FBVztvQkFDN0UsT0FBTyxFQUFLLFNBQVMsQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksOENBQTJDO29CQUNoSCxLQUFLLEVBQUUsMERBQTBEO2lCQUNsRTthQUNGLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwrQkFBTSxHQUFOO1FBQ0UsSUFBTSxJQUFJLEdBQUc7WUFDWCxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1NBQ2xCLENBQUE7UUFDRCxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFwSk0sMkJBQVksR0FBVyxhQUFhLENBQUM7SUFGeEMsY0FBYztRQURuQixNQUFNO09BQ0QsY0FBYyxDQXVKbkI7SUFBRCxxQkFBQztDQUFBLEFBdkpELENBQ0UsS0FBSyxDQUFDLFNBQVMsR0FzSmhCO0FBQUEsQ0FBQztBQUVGLGVBQWUsT0FBTyxDQUNwQixlQUFlLEVBQ2Ysa0JBQWtCLENBQ25CLENBQUMsY0FBYyxDQUFDLENBQUMifQ==

/***/ })

}]);