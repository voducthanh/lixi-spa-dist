(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[27],{

/***/ 745:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



/* harmony default export */ __webpack_exports__["a"] = (react_loadable__WEBPACK_IMPORTED_MODULE_1__({
    loader: function () { return __webpack_require__.e(/* import() */ 100).then(__webpack_require__.bind(null, 755)); },
    loading: function () { return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ui_lazy_loading__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null); }
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLFFBQVEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLFdBQVcsTUFBTSxxQ0FBcUMsQ0FBQztBQUU5RCxlQUFlLFFBQVEsQ0FBQztJQUN0QixNQUFNLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUI7SUFDbkMsT0FBTyxFQUFFLGNBQU0sT0FBQSxvQkFBQyxXQUFXLE9BQUcsRUFBZixDQUFlO0NBQy9CLENBQUMsQ0FBQyJ9

/***/ }),

/***/ 809:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./config/restful-method.ts
var restful_method = __webpack_require__(11);

// CONCATENATED MODULE: ./api/theme.ts

var fetchTheme = function () {
    return Object(restful_method["b" /* get */])({
        path: "/themes",
        description: 'Fetch list all themes | GET THEME',
        errorMesssage: "Can't fetch list all themes. Please try again",
    });
};
var fetchProductByThemeId = function (_a) {
    var id = _a.id, _b = _a.brands, brands = _b === void 0 ? '' : _b, _c = _a.bids, bids = _c === void 0 ? '' : _c, _d = _a.cids, cids = _d === void 0 ? '' : _d, _e = _a.pl, pl = _e === void 0 ? '' : _e, _f = _a.ph, ph = _f === void 0 ? '' : _f, _g = _a.sort, sort = _g === void 0 ? '' : _g, _h = _a.page, page = _h === void 0 ? 1 : _h, _j = _a.perPage, perPage = _j === void 0 ? 20 : _j;
    var query = "?page=" + page + "&per_page=" + perPage + "&brands=" + brands + "&bids=" + bids + "&cids=" + cids + "&pl=" + pl + "&ph=" + ph + "&sort=" + sort;
    return Object(restful_method["b" /* get */])({
        path: "/themes/" + id + query,
        description: 'Fetch product by theme id',
        errorMesssage: "Can't fetch product by theme id. Please try again",
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGhlbWUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0aGVtZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFL0MsTUFBTSxDQUFDLElBQU0sVUFBVSxHQUFHO0lBQ3hCLE9BQUEsR0FBRyxDQUFDO1FBQ0YsSUFBSSxFQUFFLFNBQVM7UUFDZixXQUFXLEVBQUUsbUNBQW1DO1FBQ2hELGFBQWEsRUFBRSwrQ0FBK0M7S0FDL0QsQ0FBQztBQUpGLENBSUUsQ0FBQztBQUdMLE1BQU0sQ0FBQyxJQUFNLHFCQUFxQixHQUFHLFVBQUMsRUFVckM7UUFUQyxVQUFFLEVBQ0YsY0FBVyxFQUFYLGdDQUFXLEVBQ1gsWUFBUyxFQUFULDhCQUFTLEVBQ1QsWUFBUyxFQUFULDhCQUFTLEVBQ1QsVUFBTyxFQUFQLDRCQUFPLEVBQ1AsVUFBTyxFQUFQLDRCQUFPLEVBQ1AsWUFBUyxFQUFULDhCQUFTLEVBQ1QsWUFBUSxFQUFSLDZCQUFRLEVBQ1IsZUFBWSxFQUFaLGlDQUFZO0lBR1osSUFBTSxLQUFLLEdBQUcsV0FBUyxJQUFJLGtCQUFhLE9BQU8sZ0JBQVcsTUFBTSxjQUFTLElBQUksY0FBUyxJQUFJLFlBQU8sRUFBRSxZQUFPLEVBQUUsY0FBUyxJQUFNLENBQUM7SUFDNUgsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNULElBQUksRUFBRSxhQUFXLEVBQUUsR0FBRyxLQUFPO1FBQzdCLFdBQVcsRUFBRSwyQkFBMkI7UUFDeEMsYUFBYSxFQUFFLG1EQUFtRDtLQUNuRSxDQUFDLENBQUE7QUFDSixDQUFDLENBQUMifQ==
// EXTERNAL MODULE: ./constants/api/theme.ts
var theme = __webpack_require__(166);

// CONCATENATED MODULE: ./action/theme.ts
/* unused harmony export fetchThemeAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fetchProductByThemeIdAction; });


/** Fecth list all themes */
var fetchThemeAction = function () {
    return function (dispatch, getState) {
        return dispatch({
            type: theme["b" /* FETCH_THEME */],
            payload: { promise: fetchTheme().then(function (res) { return res; }) },
        });
    };
};
/** Fecth list all product by theme id */
var fetchProductByThemeIdAction = function (_a) {
    var id = _a.id, _b = _a.brands, brands = _b === void 0 ? '' : _b, _c = _a.bids, bids = _c === void 0 ? '' : _c, _d = _a.cids, cids = _d === void 0 ? '' : _d, _e = _a.pl, pl = _e === void 0 ? '' : _e, _f = _a.ph, ph = _f === void 0 ? '' : _f, _g = _a.sort, sort = _g === void 0 ? '' : _g, _h = _a.page, page = _h === void 0 ? 1 : _h, _j = _a.perPage, perPage = _j === void 0 ? 20 : _j;
    return function (dispatch, getState) {
        return dispatch({
            type: theme["a" /* FETCH_PRODUCT_BY_THEME_ID */],
            payload: {
                promise: fetchProductByThemeId({
                    id: id,
                    brands: brands,
                    bids: bids,
                    cids: cids,
                    pl: pl,
                    ph: ph,
                    sort: sort,
                    page: page,
                    perPage: perPage
                }).then(function (res) { return res; })
            },
            meta: { id: id, page: page, perPage: perPage }
        });
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGhlbWUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0aGVtZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLHFCQUFxQixFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ2pFLE9BQU8sRUFBRSxXQUFXLEVBQUUseUJBQXlCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUdoRiw0QkFBNEI7QUFDNUIsTUFBTSxDQUFDLElBQU0sZ0JBQWdCLEdBQUc7SUFDOUIsT0FBQSxVQUFDLFFBQVEsRUFBRSxRQUFRO1FBQ2pCLE9BQUEsUUFBUSxDQUFDO1lBQ1AsSUFBSSxFQUFFLFdBQVc7WUFDakIsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUMsRUFBRTtTQUNwRCxDQUFDO0lBSEYsQ0FHRTtBQUpKLENBSUksQ0FBQztBQUVQLHlDQUF5QztBQUN6QyxNQUFNLENBQUMsSUFBTSwyQkFBMkIsR0FBRyxVQUFDLEVBVTNDO1FBVEMsVUFBRSxFQUNGLGNBQVcsRUFBWCxnQ0FBVyxFQUNYLFlBQVMsRUFBVCw4QkFBUyxFQUNULFlBQVMsRUFBVCw4QkFBUyxFQUNULFVBQU8sRUFBUCw0QkFBTyxFQUNQLFVBQU8sRUFBUCw0QkFBTyxFQUNQLFlBQVMsRUFBVCw4QkFBUyxFQUNULFlBQVEsRUFBUiw2QkFBUSxFQUNSLGVBQVksRUFBWixpQ0FBWTtJQUVaLE9BQUEsVUFBQyxRQUFRLEVBQUUsUUFBUTtRQUNqQixPQUFBLFFBQVEsQ0FBQztZQUNQLElBQUksRUFBRSx5QkFBeUI7WUFDL0IsT0FBTyxFQUFFO2dCQUNQLE9BQU8sRUFBRSxxQkFBcUIsQ0FBQztvQkFDN0IsRUFBRSxJQUFBO29CQUNGLE1BQU0sUUFBQTtvQkFDTixJQUFJLE1BQUE7b0JBQ0osSUFBSSxNQUFBO29CQUNKLEVBQUUsSUFBQTtvQkFDRixFQUFFLElBQUE7b0JBQ0YsSUFBSSxNQUFBO29CQUNKLElBQUksTUFBQTtvQkFDSixPQUFPLFNBQUE7aUJBQ1IsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLENBQUM7YUFDcEI7WUFDRCxJQUFJLEVBQUUsRUFBRSxFQUFFLElBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxPQUFPLFNBQUEsRUFBRTtTQUM1QixDQUFDO0lBaEJGLENBZ0JFO0FBakJKLENBaUJJLENBQUMifQ==

/***/ }),

/***/ 889:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ../node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ../node_modules/radium/index.js
var radium = __webpack_require__(92);

// EXTERNAL MODULE: ./utils/encode.ts
var encode = __webpack_require__(7);

// EXTERNAL MODULE: ./utils/validate.ts
var validate = __webpack_require__(9);

// EXTERNAL MODULE: ./action/theme.ts + 1 modules
var theme = __webpack_require__(809);

// CONCATENATED MODULE: ./container/app-shop/lixicoin/store.tsx

var mapStateToProps = function (state) { return ({
    router: state.router,
    themeStore: state.theme,
}); };
var mapDispatchToProps = function (dispatch) { return ({
    fetchProductByThemeIdAction: function (data) { return dispatch(Object(theme["a" /* fetchProductByThemeIdAction */])(data)); },
}); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFJcEUsTUFBTSxDQUFDLElBQU0sZUFBZSxHQUFHLFVBQUMsS0FBSyxJQUFLLE9BQUEsQ0FBQztJQUN6QyxNQUFNLEVBQUUsS0FBSyxDQUFDLE1BQU07SUFDcEIsVUFBVSxFQUFFLEtBQUssQ0FBQyxLQUFLO0NBQ2IsQ0FBQSxFQUg4QixDQUc5QixDQUFDO0FBRWIsTUFBTSxDQUFDLElBQU0sa0JBQWtCLEdBQUcsVUFBQyxRQUFRLElBQUssT0FBQSxDQUFDO0lBQy9DLDJCQUEyQixFQUFFLFVBQUMsSUFBUyxJQUFLLE9BQUEsUUFBUSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxDQUFDLEVBQTNDLENBQTJDO0NBQzdFLENBQUEsRUFGb0MsQ0FFcEMsQ0FBQyJ9
// EXTERNAL MODULE: ./utils/responsive.ts
var responsive = __webpack_require__(169);

// CONCATENATED MODULE: ./container/app-shop/lixicoin/initialize.tsx

var DEFAULT_PROPS = {
    themeName: 'redeem'
};
var INITIAL_STATE = {};
var promotionHeaderList = [
    { id: 1, title: '', silver: 'SILVER', gold: 'GOLD', diamond: 'DIAMOND' },
    Object(responsive["b" /* isDesktopVersion */])() ? { id: 2, title: '', silver: 'Free', gold: '10,000 lixicoin', diamond: '25,000 lixicoin' } : {},
];
var promotionList = [
    { id: 1, title: 'Hỗ trợ 50% phí giao hàng', silver: 'check', gold: 'check', diamond: 'check' },
    { id: 2, title: 'Quà tặng đặc biệt khi bạn mua box trong tuần sinh nhật', silver: 'check', gold: 'check', diamond: 'check' },
    { id: 3, title: 'Giảm giá đặc biệt trên một số mặt hàng', silver: 'check', gold: 'check', diamond: 'check' },
    { id: 4, title: 'Quà tặng tri ân khi đạt tiêu chuẩn thành viên', silver: 'error', gold: 'check', diamond: 'check' },
    { id: 5, title: 'Gói quà miễn phí (3 lần/ năm)', silver: 'error', gold: 'check', diamond: 'check' },
    { id: 6, title: 'Giảm giá đặc biệt tất cả mặt hàng (2 lần/ năm)', silver: 'error', gold: 'check', diamond: 'check' },
    { id: 7, title: 'Miễn phí giao hàng cho tất cả đơn hàng trong năm', silver: 'error', gold: 'error', diamond: 'check' },
    { id: 8, title: 'Quà tặng VIP cho mỗi đơn hàng trên 3,000,000 VND', silver: 'error', gold: 'error', diamond: 'check' },
    { id: 9, title: 'Trải nghiệm miễn phí sản phẩm và dịch vụ mới nhất từ Lixibox', silver: 'error', gold: 'error', diamond: 'check' },
    { id: 10, title: 'Tham gia buổi nói chuyện với các chuyên gia', silver: 'error', gold: 'error', diamond: 'check' },
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdGlhbGl6ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluaXRpYWxpemUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRzdELE1BQU0sQ0FBQyxJQUFNLGFBQWEsR0FBRztJQUMzQixTQUFTLEVBQUUsUUFBUTtDQUNWLENBQUM7QUFFWixNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUcsRUFBWSxDQUFDO0FBRTFDLE1BQU0sQ0FBQyxJQUFNLG1CQUFtQixHQUFHO0lBQ2pDLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFO0lBQ3hFLGdCQUFnQixFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLGlCQUFpQixFQUFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFO0NBQ3BILENBQUM7QUFFRixNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUc7SUFDM0IsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSwwQkFBMEIsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRTtJQUM5RixFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLHdEQUF3RCxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFO0lBQzVILEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsd0NBQXdDLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUU7SUFDNUcsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSwrQ0FBK0MsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRTtJQUNuSCxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLCtCQUErQixFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFO0lBQ25HLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsZ0RBQWdELEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUU7SUFDcEgsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxrREFBa0QsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRTtJQUN0SCxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLGtEQUFrRCxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFO0lBQ3RILEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsOERBQThELEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUU7SUFDbEksRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSw2Q0FBNkMsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRTtDQUNuSCxDQUFDIn0=
// EXTERNAL MODULE: ../node_modules/react-router-dom/index.js
var react_router_dom = __webpack_require__(214);

// EXTERNAL MODULE: ./container/layout/wrap/index.tsx
var wrap = __webpack_require__(745);

// EXTERNAL MODULE: ./components/ui/submit-button/index.tsx + 4 modules
var submit_button = __webpack_require__(748);

// EXTERNAL MODULE: ./components/ui/icon/index.tsx + 4 modules
var icon = __webpack_require__(346);

// EXTERNAL MODULE: ./components/product/slider/index.tsx + 6 modules
var slider = __webpack_require__(770);

// EXTERNAL MODULE: ./constants/application/routing.ts
var routing = __webpack_require__(2);

// EXTERNAL MODULE: ./utils/auth.ts + 1 modules
var auth = __webpack_require__(23);

// EXTERNAL MODULE: ./style/variable.ts
var variable = __webpack_require__(25);

// CONCATENATED MODULE: ./container/app-shop/lixicoin/style.tsx


/* harmony default export */ var lixicoin_style = ({
    shareGroup: function (imgUrl) {
        if (imgUrl === void 0) { imgUrl = ''; }
        return {
            textAlign: 'center',
            marginBottom: 30,
            borderBottom: "1px solid " + variable["colorBlack01"],
            height: 400,
            maxHeight: 400,
            display: variable["display"].flex,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundImage: "url(" + imgUrl + ")",
            backgroundSize: 'cover',
            backgroundPosition: 'center center',
            backgroundRepeat: 'no-repeat'
        };
    },
    title: {
        width: '70%',
        fontSize: 30,
        fontFamily: variable["fontAvenirDemiBold"],
        textShadow: "0px 2px 2px " + variable["colorWhite"]
    },
    button: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                width: '100%'
            }],
        DESKTOP: [{
                width: 350,
                maxWidth: 350
            }],
        GENERAL: [{}],
    }),
    link: Object(responsive["a" /* combineStyle */])({
        MOBILE: [{
                width: '90%'
            }],
        DESKTOP: [{}],
        GENERAL: [{}],
    }),
    infoLixiCoinGroup: {
        container: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    marginLeft: 20,
                    marginRight: 20
                }],
            DESKTOP: [{}],
            GENERAL: [{
                    borderBottom: "1px solid " + variable["colorBlack01"],
                    paddingBottom: 20,
                    marginBottom: 30
                }],
        }),
        titleGroup: {
            textAlign: 'center',
            marginBottom: 20,
            title: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        fontSize: 20
                    }],
                DESKTOP: [{
                        fontSize: 28
                    }],
                GENERAL: [{
                        fontFamily: variable["fontAvenirDemiBold"],
                        textTransform: 'uppercase',
                        textAlign: 'center',
                        marginBottom: 10
                    }],
            }),
            subTitle: {
                fontSize: 16,
                color: variable["colorBlack07"]
            }
        },
        promotionGroup: {
            display: variable["display"].flex,
            flexWrap: 'wrap',
            padding: '10px 0',
            title: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        fontSize: 14,
                        lineHeight: '20px'
                    }],
                DESKTOP: [{
                        fontSize: 18,
                        height: 30,
                        lineHeight: '30px'
                    }],
                GENERAL: [{
                        width: '55%',
                    }],
            }),
            info: {
                width: '15%',
                display: variable["display"].flex,
                justifyContent: 'center',
                alignItems: 'center',
            },
            infoHeader: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        fontSize: 10
                    }],
                DESKTOP: [{
                        fontSize: 20
                    }],
                GENERAL: [{
                        width: '15%',
                        display: variable["display"].flex,
                        justifyContent: 'center',
                        alignItems: 'center',
                        fontWeight: variable["fontAvenirDemiBold"],
                        fontFamily: variable["fontAvenirDemiBold"]
                    }]
            })
        }
    },
    earnLixiCoinGroup: {
        container: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    marginLeft: 20,
                    marginRight: 20,
                }],
            DESKTOP: [{
                    paddingBottom: 20
                }],
            GENERAL: [{
                    borderBottom: "1px solid " + variable["colorBlack01"]
                }],
        }),
        titleGroup: {
            textAlign: 'center',
            marginBottom: 20,
            title: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        fontSize: 20
                    }],
                DESKTOP: [{
                        fontSize: 28
                    }],
                GENERAL: [{
                        fontFamily: variable["fontAvenirDemiBold"],
                        textTransform: 'uppercase',
                        marginBottom: 10
                    }],
            }),
            subTitle: {
                fontSize: 16,
                color: variable["colorBlack07"]
            }
        },
        stepGroup: {
            container: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        flexDirection: 'column'
                    }],
                DESKTOP: [{
                        marginBottom: 20
                    }],
                GENERAL: [{
                        display: variable["display"].flex,
                        flexWrap: 'wrap',
                        justifyContent: 'space-between'
                    }],
            }),
            step: {
                container: Object(responsive["a" /* combineStyle */])({
                    MOBILE: [{
                            flexDirection: 'column',
                            width: "100%",
                            marginBottom: 20
                        }],
                    DESKTOP: [{
                            width: "calc(25% - 15px)"
                        }],
                    GENERAL: [{
                            display: variable["display"].flex,
                            flexDirection: 'column',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            padding: '20px 20px',
                            boxShadow: variable["shadowBlurSort"],
                            color: variable["colorBlack"],
                        }],
                }),
                title: {
                    height: 20,
                    lineHeight: '20px',
                    fontSize: 18,
                    padding: '10px 0',
                    marginBottom: 10
                },
                subTitle: {
                    fontSize: 14,
                    color: variable["colorBlack06"],
                    marginBottom: 10
                },
                cost: {
                    fontSize: 16,
                    color: variable["colorBlack08"],
                    marginBottom: 10
                },
                btn: {
                    color: variable["colorBlack"],
                    backgroundColor: variable["colorWhite"],
                    border: "1px solid " + variable["colorBlack"],
                    marginBottom: 0,
                    padding: '20px 20px'
                },
                icon: {
                    color: variable["colorBlack"]
                },
                innerIcon: {
                    width: 25
                },
                innerFbIcon: {
                    width: 15
                }
            }
        }
    },
    questionGroup: {
        container: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    marginLeft: 20,
                    marginRight: 20,
                    marginBottom: 20,
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingBottom: 20,
                    paddingTop: 30
                }],
            DESKTOP: [{
                    height: 200,
                    maxHeight: 200,
                }],
            GENERAL: [{
                    display: variable["display"].flex,
                    flexWrap: 'wrap',
                    justifyContent: 'space-between',
                    borderBottom: "1px solid " + variable["colorBlack01"],
                    marginBottom: 30
                }],
        }),
        question: {
            container: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        marginLeft: 20,
                        marginRight: 20,
                        marginBottom: 20,
                        flexDirection: 'column',
                        width: '100%',
                        textAlign: 'center'
                    }],
                DESKTOP: [{
                        width: 'calc(50% - 20px)'
                    }],
                GENERAL: [{
                        display: variable["display"].flex,
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                    }],
            }),
            title: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        fontSize: 20
                    }],
                DESKTOP: [{
                        fontSize: 28
                    }],
                GENERAL: [{
                        fontFamily: variable["fontAvenirDemiBold"],
                        textTransform: 'uppercase',
                        paddingTop: 5,
                        paddingRight: 5,
                        paddingBottom: 5,
                        paddingLeft: 5,
                        marginBottom: 10
                    }],
            }),
            subTitle: {
                fontSize: 16,
                color: variable["colorBlack07"],
                textTransform: 'uppercase'
            },
            button: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{}],
                DESKTOP: [{
                        maxWidth: 300,
                        width: 300
                    }],
                GENERAL: [{
                        marginTop: 20,
                        color: variable["colorBlack"],
                        backgroundColor: variable["colorWhite"],
                        border: "1px solid " + variable["colorBlack"]
                    }],
            })
        },
        contact: {
            container: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        width: '100%'
                    }],
                DESKTOP: [{
                        width: '50%'
                    }],
                GENERAL: [{
                        textAlign: 'center',
                        display: variable["display"].flex,
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                    }],
            }),
            icon: {
                color: variable["colorBlack"],
                width: 30,
                height: 30
            },
            innerIcon: {
                width: 30,
                height: 30
            },
            phone: {
                fontSize: 28,
                fontFamily: variable["fontAvenirDemiBold"],
                padding: 10
            },
            text: {
                fontSize: 16,
                color: variable["colorBlack07"]
            }
        }
    },
    redeemGroup: {
        borderBottom: "1px solid " + variable["colorBlack01"],
        marginBottom: 30,
        titleGroup: {
            textAlign: 'center',
            marginBottom: 20,
            title: {
                fontSize: 28,
                fontFamily: variable["fontAvenirDemiBold"],
                textTransform: 'uppercase'
            },
            subTitle: {
                fontSize: 18,
                color: variable["colorBlack07"]
            }
        }
    },
    giftGroup: {
        container: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    marginLeft: 20,
                    marginRight: 20,
                    flexDirection: 'column'
                }],
            DESKTOP: [{}],
            GENERAL: [{
                    marginBottom: 20
                }],
        }),
        title: Object(responsive["a" /* combineStyle */])({
            MOBILE: [{
                    fontSize: 20,
                    marginBottom: 20
                }],
            DESKTOP: [{
                    fontSize: 28,
                    marginBottom: 30
                }],
            GENERAL: [{
                    fontFamily: variable["fontAvenirDemiBold"],
                    textTransform: 'uppercase',
                    textAlign: 'center',
                }],
        }),
        stepGroup: {
            container: Object(responsive["a" /* combineStyle */])({
                MOBILE: [{
                        marginBottom: 20,
                        flexDirection: 'column'
                    }],
                DESKTOP: [{}],
                GENERAL: [{
                        display: variable["display"].flex,
                        flexWrap: 'wrap',
                        justifyContent: 'space-between',
                    }],
            }),
            step: {
                container: Object(responsive["a" /* combineStyle */])({
                    MOBILE: [{
                            flexDirection: 'column',
                            width: '100%',
                        }],
                    DESKTOP: [{
                            width: 'calc(25% - 15px)'
                        }],
                    GENERAL: [{
                            display: variable["display"].flex,
                            flexDirection: 'column',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                        }],
                }),
                btn: {
                    color: variable["colorBlack"],
                    backgroundColor: variable["colorWhite"],
                    border: "1px solid " + variable["colorBlack"],
                    marginBottom: 0,
                    padding: '20px 20px'
                }
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHlsZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxLQUFLLFFBQVEsTUFBTSx5QkFBeUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFekQsZUFBZTtJQUNiLFVBQVUsRUFBRSxVQUFDLE1BQVc7UUFBWCx1QkFBQSxFQUFBLFdBQVc7UUFDdEIsTUFBTSxDQUFDO1lBQ0wsU0FBUyxFQUFFLFFBQVE7WUFDbkIsWUFBWSxFQUFFLEVBQUU7WUFDaEIsWUFBWSxFQUFFLGVBQWEsUUFBUSxDQUFDLFlBQWM7WUFDbEQsTUFBTSxFQUFFLEdBQUc7WUFDWCxTQUFTLEVBQUUsR0FBRztZQUNkLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDOUIsYUFBYSxFQUFFLFFBQVE7WUFDdkIsY0FBYyxFQUFFLFFBQVE7WUFDeEIsVUFBVSxFQUFFLFFBQVE7WUFDcEIsZUFBZSxFQUFFLFNBQU8sTUFBTSxNQUFHO1lBQ2pDLGNBQWMsRUFBRSxPQUFPO1lBQ3ZCLGtCQUFrQixFQUFFLGVBQWU7WUFDbkMsZ0JBQWdCLEVBQUUsV0FBVztTQUM5QixDQUFBO0lBQ0gsQ0FBQztJQUVELEtBQUssRUFBRTtRQUNMLEtBQUssRUFBRSxLQUFLO1FBQ1osUUFBUSxFQUFFLEVBQUU7UUFDWixVQUFVLEVBQUUsUUFBUSxDQUFDLGtCQUFrQjtRQUN2QyxVQUFVLEVBQUUsaUJBQWUsUUFBUSxDQUFDLFVBQVk7S0FDakQ7SUFFRCxNQUFNLEVBQUUsWUFBWSxDQUFDO1FBQ25CLE1BQU0sRUFBRSxDQUFDO2dCQUNQLEtBQUssRUFBRSxNQUFNO2FBQ2QsQ0FBQztRQUVGLE9BQU8sRUFBRSxDQUFDO2dCQUNSLEtBQUssRUFBRSxHQUFHO2dCQUNWLFFBQVEsRUFBRSxHQUFHO2FBQ2QsQ0FBQztRQUVGLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQztLQUNkLENBQUM7SUFFRixJQUFJLEVBQUUsWUFBWSxDQUFDO1FBQ2pCLE1BQU0sRUFBRSxDQUFDO2dCQUNQLEtBQUssRUFBRSxLQUFLO2FBQ2IsQ0FBQztRQUVGLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQztRQUViLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQztLQUNkLENBQUM7SUFFRixpQkFBaUIsRUFBRTtRQUNqQixTQUFTLEVBQUUsWUFBWSxDQUFDO1lBQ3RCLE1BQU0sRUFBRSxDQUFDO29CQUNQLFVBQVUsRUFBRSxFQUFFO29CQUNkLFdBQVcsRUFBRSxFQUFFO2lCQUNoQixDQUFDO1lBRUYsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDO1lBRWIsT0FBTyxFQUFFLENBQUM7b0JBQ1IsWUFBWSxFQUFFLGVBQWEsUUFBUSxDQUFDLFlBQWM7b0JBQ2xELGFBQWEsRUFBRSxFQUFFO29CQUNqQixZQUFZLEVBQUUsRUFBRTtpQkFDakIsQ0FBQztTQUNILENBQUM7UUFFRixVQUFVLEVBQUU7WUFDVixTQUFTLEVBQUUsUUFBUTtZQUNuQixZQUFZLEVBQUUsRUFBRTtZQUVoQixLQUFLLEVBQUUsWUFBWSxDQUFDO2dCQUNsQixNQUFNLEVBQUUsQ0FBQzt3QkFDUCxRQUFRLEVBQUUsRUFBRTtxQkFDYixDQUFDO2dCQUVGLE9BQU8sRUFBRSxDQUFDO3dCQUNSLFFBQVEsRUFBRSxFQUFFO3FCQUNiLENBQUM7Z0JBRUYsT0FBTyxFQUFFLENBQUM7d0JBQ1IsVUFBVSxFQUFFLFFBQVEsQ0FBQyxrQkFBa0I7d0JBQ3ZDLGFBQWEsRUFBRSxXQUFXO3dCQUMxQixTQUFTLEVBQUUsUUFBUTt3QkFDbkIsWUFBWSxFQUFFLEVBQUU7cUJBQ2pCLENBQUM7YUFDSCxDQUFDO1lBRUYsUUFBUSxFQUFFO2dCQUNSLFFBQVEsRUFBRSxFQUFFO2dCQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTthQUM3QjtTQUNGO1FBRUQsY0FBYyxFQUFFO1lBQ2QsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM5QixRQUFRLEVBQUUsTUFBTTtZQUNoQixPQUFPLEVBQUUsUUFBUTtZQUVqQixLQUFLLEVBQUUsWUFBWSxDQUFDO2dCQUNsQixNQUFNLEVBQUUsQ0FBQzt3QkFDUCxRQUFRLEVBQUUsRUFBRTt3QkFDWixVQUFVLEVBQUUsTUFBTTtxQkFDbkIsQ0FBQztnQkFFRixPQUFPLEVBQUUsQ0FBQzt3QkFDUixRQUFRLEVBQUUsRUFBRTt3QkFDWixNQUFNLEVBQUUsRUFBRTt3QkFDVixVQUFVLEVBQUUsTUFBTTtxQkFDbkIsQ0FBQztnQkFFRixPQUFPLEVBQUUsQ0FBQzt3QkFDUixLQUFLLEVBQUUsS0FBSztxQkFDYixDQUFDO2FBQ0gsQ0FBQztZQUVGLElBQUksRUFBRTtnQkFDSixLQUFLLEVBQUUsS0FBSztnQkFDWixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM5QixjQUFjLEVBQUUsUUFBUTtnQkFDeEIsVUFBVSxFQUFFLFFBQVE7YUFDckI7WUFFRCxVQUFVLEVBQUUsWUFBWSxDQUFDO2dCQUN2QixNQUFNLEVBQUUsQ0FBQzt3QkFDUCxRQUFRLEVBQUUsRUFBRTtxQkFDYixDQUFDO2dCQUVGLE9BQU8sRUFBRSxDQUFDO3dCQUNSLFFBQVEsRUFBRSxFQUFFO3FCQUNiLENBQUM7Z0JBRUYsT0FBTyxFQUFFLENBQUM7d0JBQ1IsS0FBSyxFQUFFLEtBQUs7d0JBQ1osT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTt3QkFDOUIsY0FBYyxFQUFFLFFBQVE7d0JBQ3hCLFVBQVUsRUFBRSxRQUFRO3dCQUNwQixVQUFVLEVBQUUsUUFBUSxDQUFDLGtCQUFrQjt3QkFDdkMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxrQkFBa0I7cUJBQ3hDLENBQUM7YUFDSCxDQUFDO1NBQ0g7S0FDRjtJQUVELGlCQUFpQixFQUFFO1FBQ2pCLFNBQVMsRUFBRSxZQUFZLENBQUM7WUFDdEIsTUFBTSxFQUFFLENBQUM7b0JBQ1AsVUFBVSxFQUFFLEVBQUU7b0JBQ2QsV0FBVyxFQUFFLEVBQUU7aUJBQ2hCLENBQUM7WUFFRixPQUFPLEVBQUUsQ0FBQztvQkFDUixhQUFhLEVBQUUsRUFBRTtpQkFDbEIsQ0FBQztZQUVGLE9BQU8sRUFBRSxDQUFDO29CQUNSLFlBQVksRUFBRSxlQUFhLFFBQVEsQ0FBQyxZQUFjO2lCQUNuRCxDQUFDO1NBQ0gsQ0FBQztRQUVGLFVBQVUsRUFBRTtZQUNWLFNBQVMsRUFBRSxRQUFRO1lBQ25CLFlBQVksRUFBRSxFQUFFO1lBRWhCLEtBQUssRUFBRSxZQUFZLENBQUM7Z0JBQ2xCLE1BQU0sRUFBRSxDQUFDO3dCQUNQLFFBQVEsRUFBRSxFQUFFO3FCQUNiLENBQUM7Z0JBRUYsT0FBTyxFQUFFLENBQUM7d0JBQ1IsUUFBUSxFQUFFLEVBQUU7cUJBQ2IsQ0FBQztnQkFFRixPQUFPLEVBQUUsQ0FBQzt3QkFDUixVQUFVLEVBQUUsUUFBUSxDQUFDLGtCQUFrQjt3QkFDdkMsYUFBYSxFQUFFLFdBQVc7d0JBQzFCLFlBQVksRUFBRSxFQUFFO3FCQUNqQixDQUFDO2FBQ0gsQ0FBQztZQUVGLFFBQVEsRUFBRTtnQkFDUixRQUFRLEVBQUUsRUFBRTtnQkFDWixLQUFLLEVBQUUsUUFBUSxDQUFDLFlBQVk7YUFDN0I7U0FDRjtRQUVELFNBQVMsRUFBRTtZQUNULFNBQVMsRUFBRSxZQUFZLENBQUM7Z0JBQ3RCLE1BQU0sRUFBRSxDQUFDO3dCQUNQLGFBQWEsRUFBRSxRQUFRO3FCQUN4QixDQUFDO2dCQUVGLE9BQU8sRUFBRSxDQUFDO3dCQUNSLFlBQVksRUFBRSxFQUFFO3FCQUNqQixDQUFDO2dCQUVGLE9BQU8sRUFBRSxDQUFDO3dCQUNSLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7d0JBQzlCLFFBQVEsRUFBRSxNQUFNO3dCQUNoQixjQUFjLEVBQUUsZUFBZTtxQkFDaEMsQ0FBQzthQUNILENBQUM7WUFFRixJQUFJLEVBQUU7Z0JBQ0osU0FBUyxFQUFFLFlBQVksQ0FBQztvQkFDdEIsTUFBTSxFQUFFLENBQUM7NEJBQ1AsYUFBYSxFQUFFLFFBQVE7NEJBQ3ZCLEtBQUssRUFBRSxNQUFNOzRCQUNiLFlBQVksRUFBRSxFQUFFO3lCQUNqQixDQUFDO29CQUVGLE9BQU8sRUFBRSxDQUFDOzRCQUNSLEtBQUssRUFBRSxrQkFBa0I7eUJBQzFCLENBQUM7b0JBRUYsT0FBTyxFQUFFLENBQUM7NEJBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTs0QkFDOUIsYUFBYSxFQUFFLFFBQVE7NEJBQ3ZCLGNBQWMsRUFBRSxlQUFlOzRCQUMvQixVQUFVLEVBQUUsUUFBUTs0QkFDcEIsT0FBTyxFQUFFLFdBQVc7NEJBQ3BCLFNBQVMsRUFBRSxRQUFRLENBQUMsY0FBYzs0QkFDbEMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxVQUFVO3lCQUMzQixDQUFDO2lCQUNILENBQUM7Z0JBRUYsS0FBSyxFQUFFO29CQUNMLE1BQU0sRUFBRSxFQUFFO29CQUNWLFVBQVUsRUFBRSxNQUFNO29CQUNsQixRQUFRLEVBQUUsRUFBRTtvQkFDWixPQUFPLEVBQUUsUUFBUTtvQkFDakIsWUFBWSxFQUFFLEVBQUU7aUJBQ2pCO2dCQUVELFFBQVEsRUFBRTtvQkFDUixRQUFRLEVBQUUsRUFBRTtvQkFDWixLQUFLLEVBQUUsUUFBUSxDQUFDLFlBQVk7b0JBQzVCLFlBQVksRUFBRSxFQUFFO2lCQUNqQjtnQkFFRCxJQUFJLEVBQUU7b0JBQ0osUUFBUSxFQUFFLEVBQUU7b0JBQ1osS0FBSyxFQUFFLFFBQVEsQ0FBQyxZQUFZO29CQUM1QixZQUFZLEVBQUUsRUFBRTtpQkFDakI7Z0JBRUQsR0FBRyxFQUFFO29CQUNILEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDMUIsZUFBZSxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUNwQyxNQUFNLEVBQUUsZUFBYSxRQUFRLENBQUMsVUFBWTtvQkFDMUMsWUFBWSxFQUFFLENBQUM7b0JBQ2YsT0FBTyxFQUFFLFdBQVc7aUJBQ3JCO2dCQUVELElBQUksRUFBRTtvQkFDSixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7aUJBQzNCO2dCQUVELFNBQVMsRUFBRTtvQkFDVCxLQUFLLEVBQUUsRUFBRTtpQkFDVjtnQkFFRCxXQUFXLEVBQUU7b0JBQ1gsS0FBSyxFQUFFLEVBQUU7aUJBQ1Y7YUFDRjtTQUNGO0tBQ0Y7SUFFRCxhQUFhLEVBQUU7UUFDYixTQUFTLEVBQUUsWUFBWSxDQUFDO1lBQ3RCLE1BQU0sRUFBRSxDQUFDO29CQUNQLFVBQVUsRUFBRSxFQUFFO29CQUNkLFdBQVcsRUFBRSxFQUFFO29CQUNmLFlBQVksRUFBRSxFQUFFO29CQUNoQixhQUFhLEVBQUUsUUFBUTtvQkFDdkIsY0FBYyxFQUFFLFFBQVE7b0JBQ3hCLFVBQVUsRUFBRSxRQUFRO29CQUNwQixhQUFhLEVBQUUsRUFBRTtvQkFDakIsVUFBVSxFQUFFLEVBQUU7aUJBQ2YsQ0FBQztZQUVGLE9BQU8sRUFBRSxDQUFDO29CQUNSLE1BQU0sRUFBRSxHQUFHO29CQUNYLFNBQVMsRUFBRSxHQUFHO2lCQUNmLENBQUM7WUFFRixPQUFPLEVBQUUsQ0FBQztvQkFDUixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJO29CQUM5QixRQUFRLEVBQUUsTUFBTTtvQkFDaEIsY0FBYyxFQUFFLGVBQWU7b0JBQy9CLFlBQVksRUFBRSxlQUFhLFFBQVEsQ0FBQyxZQUFjO29CQUNsRCxZQUFZLEVBQUUsRUFBRTtpQkFDakIsQ0FBQztTQUNILENBQUM7UUFFRixRQUFRLEVBQUU7WUFDUixTQUFTLEVBQUUsWUFBWSxDQUFDO2dCQUN0QixNQUFNLEVBQUUsQ0FBQzt3QkFDUCxVQUFVLEVBQUUsRUFBRTt3QkFDZCxXQUFXLEVBQUUsRUFBRTt3QkFDZixZQUFZLEVBQUUsRUFBRTt3QkFDaEIsYUFBYSxFQUFFLFFBQVE7d0JBQ3ZCLEtBQUssRUFBRSxNQUFNO3dCQUNiLFNBQVMsRUFBRSxRQUFRO3FCQUNwQixDQUFDO2dCQUVGLE9BQU8sRUFBRSxDQUFDO3dCQUNSLEtBQUssRUFBRSxrQkFBa0I7cUJBQzFCLENBQUM7Z0JBRUYsT0FBTyxFQUFFLENBQUM7d0JBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSTt3QkFDOUIsYUFBYSxFQUFFLFFBQVE7d0JBQ3ZCLFVBQVUsRUFBRSxRQUFRO3dCQUNwQixjQUFjLEVBQUUsUUFBUTtxQkFDekIsQ0FBQzthQUNILENBQUM7WUFFRixLQUFLLEVBQUUsWUFBWSxDQUFDO2dCQUNsQixNQUFNLEVBQUUsQ0FBQzt3QkFDUCxRQUFRLEVBQUUsRUFBRTtxQkFDYixDQUFDO2dCQUVGLE9BQU8sRUFBRSxDQUFDO3dCQUNSLFFBQVEsRUFBRSxFQUFFO3FCQUNiLENBQUM7Z0JBRUYsT0FBTyxFQUFFLENBQUM7d0JBQ1IsVUFBVSxFQUFFLFFBQVEsQ0FBQyxrQkFBa0I7d0JBQ3ZDLGFBQWEsRUFBRSxXQUFXO3dCQUMxQixVQUFVLEVBQUUsQ0FBQzt3QkFDYixZQUFZLEVBQUUsQ0FBQzt3QkFDZixhQUFhLEVBQUUsQ0FBQzt3QkFDaEIsV0FBVyxFQUFFLENBQUM7d0JBQ2QsWUFBWSxFQUFFLEVBQUU7cUJBQ2pCLENBQUM7YUFDSCxDQUFDO1lBRUYsUUFBUSxFQUFFO2dCQUNSLFFBQVEsRUFBRSxFQUFFO2dCQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTtnQkFDNUIsYUFBYSxFQUFFLFdBQVc7YUFDM0I7WUFFRCxNQUFNLEVBQUUsWUFBWSxDQUFDO2dCQUNuQixNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUM7Z0JBRVosT0FBTyxFQUFFLENBQUM7d0JBQ1IsUUFBUSxFQUFFLEdBQUc7d0JBQ2IsS0FBSyxFQUFFLEdBQUc7cUJBQ1gsQ0FBQztnQkFFRixPQUFPLEVBQUUsQ0FBQzt3QkFDUixTQUFTLEVBQUUsRUFBRTt3QkFDYixLQUFLLEVBQUUsUUFBUSxDQUFDLFVBQVU7d0JBQzFCLGVBQWUsRUFBRSxRQUFRLENBQUMsVUFBVTt3QkFDcEMsTUFBTSxFQUFFLGVBQWEsUUFBUSxDQUFDLFVBQVk7cUJBQzNDLENBQUM7YUFDSCxDQUFDO1NBQ0g7UUFFRCxPQUFPLEVBQUU7WUFDUCxTQUFTLEVBQUUsWUFBWSxDQUFDO2dCQUN0QixNQUFNLEVBQUUsQ0FBQzt3QkFDUCxLQUFLLEVBQUUsTUFBTTtxQkFDZCxDQUFDO2dCQUVGLE9BQU8sRUFBRSxDQUFDO3dCQUNSLEtBQUssRUFBRSxLQUFLO3FCQUNiLENBQUM7Z0JBRUYsT0FBTyxFQUFFLENBQUM7d0JBQ1IsU0FBUyxFQUFFLFFBQVE7d0JBQ25CLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7d0JBQzlCLGFBQWEsRUFBRSxRQUFRO3dCQUN2QixVQUFVLEVBQUUsUUFBUTt3QkFDcEIsY0FBYyxFQUFFLFFBQVE7cUJBQ3pCLENBQUM7YUFDSCxDQUFDO1lBRUYsSUFBSSxFQUFFO2dCQUNKLEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtnQkFDMUIsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsTUFBTSxFQUFFLEVBQUU7YUFDWDtZQUVELFNBQVMsRUFBRTtnQkFDVCxLQUFLLEVBQUUsRUFBRTtnQkFDVCxNQUFNLEVBQUUsRUFBRTthQUNYO1lBRUQsS0FBSyxFQUFFO2dCQUNMLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFVBQVUsRUFBRSxRQUFRLENBQUMsa0JBQWtCO2dCQUN2QyxPQUFPLEVBQUUsRUFBRTthQUNaO1lBRUQsSUFBSSxFQUFFO2dCQUNKLFFBQVEsRUFBRSxFQUFFO2dCQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTthQUM3QjtTQUNGO0tBQ0Y7SUFFRCxXQUFXLEVBQUU7UUFDWCxZQUFZLEVBQUUsZUFBYSxRQUFRLENBQUMsWUFBYztRQUNsRCxZQUFZLEVBQUUsRUFBRTtRQUVoQixVQUFVLEVBQUU7WUFDVixTQUFTLEVBQUUsUUFBUTtZQUNuQixZQUFZLEVBQUUsRUFBRTtZQUVoQixLQUFLLEVBQUU7Z0JBQ0wsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osVUFBVSxFQUFFLFFBQVEsQ0FBQyxrQkFBa0I7Z0JBQ3ZDLGFBQWEsRUFBRSxXQUFXO2FBQzNCO1lBRUQsUUFBUSxFQUFFO2dCQUNSLFFBQVEsRUFBRSxFQUFFO2dCQUNaLEtBQUssRUFBRSxRQUFRLENBQUMsWUFBWTthQUM3QjtTQUNGO0tBQ0Y7SUFFRCxTQUFTLEVBQUU7UUFDVCxTQUFTLEVBQUUsWUFBWSxDQUFDO1lBQ3RCLE1BQU0sRUFBRSxDQUFDO29CQUNQLFVBQVUsRUFBRSxFQUFFO29CQUNkLFdBQVcsRUFBRSxFQUFFO29CQUNmLGFBQWEsRUFBRSxRQUFRO2lCQUN4QixDQUFDO1lBRUYsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDO1lBRWIsT0FBTyxFQUFFLENBQUM7b0JBQ1IsWUFBWSxFQUFFLEVBQUU7aUJBQ2pCLENBQUM7U0FDSCxDQUFDO1FBRUYsS0FBSyxFQUFFLFlBQVksQ0FBQztZQUNsQixNQUFNLEVBQUUsQ0FBQztvQkFDUCxRQUFRLEVBQUUsRUFBRTtvQkFDWixZQUFZLEVBQUUsRUFBRTtpQkFDakIsQ0FBQztZQUVGLE9BQU8sRUFBRSxDQUFDO29CQUNSLFFBQVEsRUFBRSxFQUFFO29CQUNaLFlBQVksRUFBRSxFQUFFO2lCQUNqQixDQUFDO1lBRUYsT0FBTyxFQUFFLENBQUM7b0JBQ1IsVUFBVSxFQUFFLFFBQVEsQ0FBQyxrQkFBa0I7b0JBQ3ZDLGFBQWEsRUFBRSxXQUFXO29CQUMxQixTQUFTLEVBQUUsUUFBUTtpQkFDcEIsQ0FBQztTQUNILENBQUM7UUFFRixTQUFTLEVBQUU7WUFDVCxTQUFTLEVBQUUsWUFBWSxDQUFDO2dCQUN0QixNQUFNLEVBQUUsQ0FBQzt3QkFDUCxZQUFZLEVBQUUsRUFBRTt3QkFDaEIsYUFBYSxFQUFFLFFBQVE7cUJBQ3hCLENBQUM7Z0JBRUYsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDO2dCQUViLE9BQU8sRUFBRSxDQUFDO3dCQUNSLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7d0JBQzlCLFFBQVEsRUFBRSxNQUFNO3dCQUNoQixjQUFjLEVBQUUsZUFBZTtxQkFDaEMsQ0FBQzthQUNILENBQUM7WUFFRixJQUFJLEVBQUU7Z0JBQ0osU0FBUyxFQUFFLFlBQVksQ0FBQztvQkFDdEIsTUFBTSxFQUFFLENBQUM7NEJBQ1AsYUFBYSxFQUFFLFFBQVE7NEJBQ3ZCLEtBQUssRUFBRSxNQUFNO3lCQUNkLENBQUM7b0JBRUYsT0FBTyxFQUFFLENBQUM7NEJBQ1IsS0FBSyxFQUFFLGtCQUFrQjt5QkFDMUIsQ0FBQztvQkFFRixPQUFPLEVBQUUsQ0FBQzs0QkFDUixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJOzRCQUM5QixhQUFhLEVBQUUsUUFBUTs0QkFDdkIsY0FBYyxFQUFFLGVBQWU7NEJBQy9CLFVBQVUsRUFBRSxRQUFRO3lCQUNyQixDQUFDO2lCQUNILENBQUM7Z0JBRUYsR0FBRyxFQUFFO29CQUNILEtBQUssRUFBRSxRQUFRLENBQUMsVUFBVTtvQkFDMUIsZUFBZSxFQUFFLFFBQVEsQ0FBQyxVQUFVO29CQUNwQyxNQUFNLEVBQUUsZUFBYSxRQUFRLENBQUMsVUFBWTtvQkFDMUMsWUFBWSxFQUFFLENBQUM7b0JBQ2YsT0FBTyxFQUFFLFdBQVc7aUJBQ3JCO2FBQ0Y7U0FDRjtLQUNGO0NBQ00sQ0FBQyJ9
// EXTERNAL MODULE: ./utils/uri.ts
var uri = __webpack_require__(347);

// CONCATENATED MODULE: ./container/app-shop/lixicoin/view-desktop.tsx
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};













var bannerImg = uri["a" /* CDN_ASSETS_PREFIX */] + '/assets/images/mobile/top-banner.jpg';
var promotionStyle = lixicoin_style.infoLixiCoinGroup.promotionGroup;
var renderText = function (_a) {
    var text = _a.text, style = _a.style;
    return react["createElement"]("div", { style: style }, text);
};
var renderIcon = function (iconName) {
    var iconProps = {
        name: iconName,
        style: { width: 20, height: 20, color: 'check' === iconName ? 'green' : 'red' }
    };
    return react["createElement"]("div", { style: promotionStyle.info },
        react["createElement"](icon["a" /* default */], __assign({}, iconProps)));
};
var renderHeader = function (item) {
    return Object(validate["j" /* isEmptyObject */])(item) ? null : (react["createElement"]("div", { key: "header-" + item.id, style: promotionStyle },
        renderText({ style: promotionStyle.title, text: item.title }),
        renderText({ style: promotionStyle.infoHeader, text: item.silver }),
        renderText({ style: promotionStyle.infoHeader, text: item.gold }),
        renderText({ style: promotionStyle.infoHeader, text: item.diamond })));
};
var renderBody = function (item) {
    return (react["createElement"]("div", { key: "body-" + item.id, style: promotionStyle },
        renderText({ style: promotionStyle.title, text: item.title }),
        renderIcon(item.silver),
        renderIcon(item.gold),
        renderIcon(item.diamond)));
};
var renderStepGroup = function (_a) {
    var iconName = _a.iconName, title = _a.title, subTitle = _a.subTitle, cost = _a.cost, buttonName = _a.buttonName, _b = _a.innerStyle, innerStyle = _b === void 0 ? {} : _b, _c = _a.link, link = _c === void 0 ? '' : _c;
    var iconProps = {
        name: iconName,
        style: lixicoin_style.earnLixiCoinGroup.stepGroup.step.icon,
        innerStyle: [lixicoin_style.earnLixiCoinGroup.stepGroup.step.innerIcon, innerStyle]
    };
    var buttonProps = {
        title: buttonName,
        style: lixicoin_style.earnLixiCoinGroup.stepGroup.step.btn
    };
    return (react["createElement"](react_router_dom["NavLink"], { to: link, style: lixicoin_style.earnLixiCoinGroup.stepGroup.step.container },
        react["createElement"](icon["a" /* default */], __assign({}, iconProps)),
        react["createElement"]("div", { style: lixicoin_style.earnLixiCoinGroup.stepGroup.step.title }, title),
        react["createElement"]("div", { style: lixicoin_style.earnLixiCoinGroup.stepGroup.step.subTitle }, subTitle),
        react["createElement"]("div", { style: lixicoin_style.earnLixiCoinGroup.stepGroup.step.cost }, cost),
        react["createElement"](submit_button["a" /* default */], __assign({}, buttonProps))));
};
var renderGiftGroup = function (_a) {
    var iconName = _a.iconName, buttonName = _a.buttonName, link = _a.link;
    var buttonProps = {
        title: buttonName,
        style: lixicoin_style.giftGroup.stepGroup.step.btn
    };
    return (react["createElement"](react_router_dom["NavLink"], { to: link, style: lixicoin_style.giftGroup.stepGroup.step.container },
        react["createElement"](submit_button["a" /* default */], __assign({}, buttonProps))));
};
var renderRedeemList = function (redeemList) {
    var redeemListProps = {
        title: 'Quà tặng hấp dẫn từ Lixibox',
        column: 4,
        viewMoreLink: routing["bb" /* ROUTING_REDEEM_PATH */],
        data: redeemList || [],
        isBuyByCoin: true,
        showCurrentPrice: true
    };
    return react["createElement"](slider["a" /* default */], __assign({}, redeemListProps));
};
var renderDesktop = function (props) {
    var _a = props, productByThemeId = _a.themeStore.productByThemeId, themeName = _a.themeName;
    var keyHashTheme = Object(encode["h" /* objectToHash */])({ id: themeName, page: 1, perPage: 20 });
    var redeemList = productByThemeId[keyHashTheme]
        && productByThemeId[keyHashTheme].boxes || [];
    var buttonProps = {
        title: 'Nhận ngay 100 lixicoin!',
        style: lixicoin_style.button
    };
    return (react["createElement"]("lixi-coin-container", null,
        react["createElement"](wrap["a" /* default */], null,
            react["createElement"]("div", { style: lixicoin_style.shareGroup(bannerImg) },
                react["createElement"]("div", { style: lixicoin_style.title }, "B\u1EA1n \u0111\u00E3 share clip, \u1EA3nh selfie \u0111\u1EADp h\u1ED9p Lixibox tr\u00EAn Facebook v\u00E0 Instagram?"),
                react["createElement"](react_router_dom["NavLink"], { to: routing["Ha" /* ROUTING_LOVES_NEW */], target: '_blank', style: lixicoin_style.link },
                    react["createElement"](submit_button["a" /* default */], __assign({}, buttonProps)))),
            react["createElement"]("div", { style: lixicoin_style.infoLixiCoinGroup.container },
                react["createElement"]("div", { style: lixicoin_style.infoLixiCoinGroup.titleGroup },
                    renderText({ style: lixicoin_style.infoLixiCoinGroup.titleGroup.title, text: 'Thêm lixicoin, thêm nhiều ưu đãi' }),
                    renderText({ style: lixicoin_style.infoLixiCoinGroup.titleGroup.subTitle, text: 'Lixicoin là chương trình tích lũy điểm cho khách hàng.' }),
                    renderText({ style: lixicoin_style.infoLixiCoinGroup.titleGroup.subTitle, text: 'Mỗi box bạn mua sẽ mang lại cho bạn nhiều ưu đãi hấp dẫn từ lixibox' })),
                react["createElement"]("div", null,
                    Array.isArray(promotionHeaderList)
                        && promotionHeaderList.map(function (item, index) { return renderHeader(item); }),
                    Array.isArray(promotionList)
                        && promotionList.map(function (item, index) { return renderBody(item); }))),
            react["createElement"]("div", { style: lixicoin_style.earnLixiCoinGroup.container },
                react["createElement"]("div", { style: lixicoin_style.earnLixiCoinGroup.titleGroup },
                    renderText({ style: lixicoin_style.earnLixiCoinGroup.titleGroup.title, text: 'LÀM SAO TÍCH LŨY THÊM LIXICOIN?' }),
                    renderText({ style: lixicoin_style.earnLixiCoinGroup.titleGroup.subTitle, text: 'Bạn có thể chọn 4 cách bên dưới' })),
                react["createElement"]("div", { style: lixicoin_style.earnLixiCoinGroup.stepGroup.container },
                    renderStepGroup({ iconName: 'cart', title: 'Mua box sản phẩm', subTitle: 'nhận ngay', cost: '1,000 = 1 LIXICOIN', buttonName: 'Tham gia mua hàng', link: routing["kb" /* ROUTING_SHOP_INDEX */] }),
                    renderStepGroup({ iconName: 'user-plus', title: 'Giới thiệu bạn', subTitle: 'nhận ngay', cost: '200 LIXICOIN / 1 LƯỢT MUA', buttonName: 'Giới thiệu bạn bè', link: '' }),
                    renderStepGroup({ iconName: 'facebook', title: 'Like, share trên FB', subTitle: 'nhận ngay', cost: '100 LIXICOINS', buttonName: 'Chia sẻ ngay', innerStyle: lixicoin_style.earnLixiCoinGroup.stepGroup.step.innerFbIcon, link: routing["Ha" /* ROUTING_LOVES_NEW */] }),
                    renderStepGroup({ iconName: 'message', title: 'Gửi bài đánh giá', subTitle: 'nhận ngay', cost: '10 LIXICOIN', buttonName: 'Đánh giá sản phẩm', link: routing["xb" /* ROUTING_USER_FEEDBACK */] }))),
            react["createElement"]("div", { style: lixicoin_style.questionGroup.container },
                react["createElement"]("div", { style: lixicoin_style.questionGroup.question.container },
                    react["createElement"]("div", { style: lixicoin_style.questionGroup.question.title }, "B\u1EA1n c\u00F3 \u0111i\u1EC1u mu\u1ED1n h\u1ECFi?"),
                    react["createElement"]("div", { style: lixicoin_style.questionGroup.question.subTitle }, "Xem c\u00E2u tr\u1EA3 l\u1EDDi t\u1EA1i \u0111\u00E2y \u0111\u1EC3 hi\u1EC3u TH\u00CAM v\u1EC1 lixibox!"),
                    react["createElement"](react_router_dom["NavLink"], { to: routing["U" /* ROUTING_INFO_QUESTION_ABOUT_US */] },
                        react["createElement"](submit_button["a" /* default */], { title: 'Xem FAQ', style: lixicoin_style.questionGroup.question.button }))),
                react["createElement"]("div", { style: lixicoin_style.questionGroup.contact.container },
                    react["createElement"](icon["a" /* default */], { name: 'call', style: lixicoin_style.questionGroup.contact.icon, innerStyle: lixicoin_style.questionGroup.contact.innerIcon }),
                    react["createElement"]("div", { style: lixicoin_style.questionGroup.contact.phone }, "1800 2040"),
                    react["createElement"]("div", { style: lixicoin_style.questionGroup.contact.text }, "LI\u00CAN H\u1EC6 \u0110\u1EC2 \u0110\u01AF\u1EE2C LIXIBOX T\u01AF V\u1EA4N"))),
            react["createElement"]("div", { style: lixicoin_style.redeemGroup }, renderRedeemList(redeemList)),
            react["createElement"]("div", { style: lixicoin_style.giftGroup.container },
                renderText({ style: lixicoin_style.giftGroup.title, text: 'CHỈ VỚI 4 BƯỚC ĐƠN GIẢN BÊN DƯỚI LÀ BẠN ĐÃ CÓ PHẦN QUÀ CỦA RIÊNG MÌNH' }),
                react["createElement"]("div", { style: lixicoin_style.giftGroup.stepGroup.container },
                    renderGiftGroup({ iconName: 'cart', buttonName: auth["a" /* auth */].loggedIn() ? 'Bạn đã đăng ký' : 'Bước 1. Đăng ký thành viên', link: auth["a" /* auth */].loggedIn() ? '' : routing["d" /* ROUTING_AUTH_SIGN_IN */] }),
                    renderGiftGroup({ iconName: 'gift', buttonName: 'Bước 2. Tham gia mua hàng', link: routing["kb" /* ROUTING_SHOP_INDEX */] }),
                    renderGiftGroup({ iconName: 'bell', buttonName: 'Bước 3. Tích lũy LixiCoin', link: routing["vb" /* ROUTING_USER */] }),
                    renderGiftGroup({ iconName: 'feed', buttonName: 'Bước 4. Đổi LixiCoin lấy quà', link: routing["h" /* ROUTING_CHECK_OUT */] }))))));
};
/* harmony default export */ var view_desktop = (renderDesktop);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1kZXNrdG9wLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1kZXNrdG9wLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU8sS0FBSyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQy9CLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUUzQyxPQUFPLFVBQVUsTUFBTSxtQkFBbUIsQ0FBQztBQUMzQyxPQUFPLE1BQU0sTUFBTSxzQ0FBc0MsQ0FBQztBQUMxRCxPQUFPLElBQUksTUFBTSw2QkFBNkIsQ0FBQztBQUMvQyxPQUFPLGFBQWEsTUFBTSxvQ0FBb0MsQ0FBQztBQUMvRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDckQsT0FBTyxFQUFlLGFBQWEsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ3JFLE9BQU8sRUFDTCxZQUFZLEVBQ1osaUJBQWlCLEVBQ2pCLGlCQUFpQixFQUNqQixrQkFBa0IsRUFDbEIsbUJBQW1CLEVBQ25CLHFCQUFxQixFQUNyQixvQkFBb0IsRUFDcEIsOEJBQThCLEVBQy9CLE1BQU0sd0NBQXdDLENBQUM7QUFDaEQsT0FBTyxFQUFFLElBQUksRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBRzNDLE9BQU8sS0FBSyxNQUFNLFNBQVMsQ0FBQztBQUM1QixPQUFPLEVBQUUsbUJBQW1CLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBRWxFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3ZELElBQU0sU0FBUyxHQUFHLGlCQUFpQixHQUFHLHNDQUFzQyxDQUFDO0FBQzdFLElBQU0sY0FBYyxHQUFHLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLENBQUM7QUFFOUQsSUFBTSxVQUFVLEdBQUcsVUFBQyxFQUFlO1FBQWIsY0FBSSxFQUFFLGdCQUFLO0lBQy9CLE1BQU0sQ0FBQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxJQUFHLElBQUksQ0FBTyxDQUFDO0FBQ3pDLENBQUMsQ0FBQztBQUVGLElBQU0sVUFBVSxHQUFHLFVBQUMsUUFBUTtJQUMxQixJQUFNLFNBQVMsR0FBRztRQUNoQixJQUFJLEVBQUUsUUFBUTtRQUNkLEtBQUssRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsT0FBTyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUU7S0FDaEYsQ0FBQztJQUNGLE1BQU0sQ0FBQyw2QkFBSyxLQUFLLEVBQUUsY0FBYyxDQUFDLElBQUk7UUFBRSxvQkFBQyxJQUFJLGVBQUssU0FBUyxFQUFTLENBQU0sQ0FBQztBQUM3RSxDQUFDLENBQUM7QUFFRixJQUFNLFlBQVksR0FBRyxVQUFDLElBQUk7SUFFeEIsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUNsQyw2QkFBSyxHQUFHLEVBQUUsWUFBVSxJQUFJLENBQUMsRUFBSSxFQUFFLEtBQUssRUFBRSxjQUFjO1FBQ2pELFVBQVUsQ0FBQyxFQUFFLEtBQUssRUFBRSxjQUFjLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDN0QsVUFBVSxDQUFDLEVBQUUsS0FBSyxFQUFFLGNBQWMsQ0FBQyxVQUFVLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNuRSxVQUFVLENBQUMsRUFBRSxLQUFLLEVBQUUsY0FBYyxDQUFDLFVBQVUsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2pFLFVBQVUsQ0FBQyxFQUFFLEtBQUssRUFBRSxjQUFjLENBQUMsVUFBVSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FDakUsQ0FDUCxDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUYsSUFBTSxVQUFVLEdBQUcsVUFBQyxJQUFJO0lBQ3RCLE1BQU0sQ0FBQyxDQUNMLDZCQUFLLEdBQUcsRUFBRSxVQUFRLElBQUksQ0FBQyxFQUFJLEVBQUUsS0FBSyxFQUFFLGNBQWM7UUFDL0MsVUFBVSxDQUFDLEVBQUUsS0FBSyxFQUFFLGNBQWMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM3RCxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN2QixVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUNyQixVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUNyQixDQUNQLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixJQUFNLGVBQWUsR0FBRyxVQUFDLEVBQTJFO1FBQXpFLHNCQUFRLEVBQUUsZ0JBQUssRUFBRSxzQkFBUSxFQUFFLGNBQUksRUFBRSwwQkFBVSxFQUFFLGtCQUFlLEVBQWYsb0NBQWUsRUFBRSxZQUFTLEVBQVQsOEJBQVM7SUFDaEcsSUFBTSxTQUFTLEdBQUc7UUFDaEIsSUFBSSxFQUFFLFFBQVE7UUFDZCxLQUFLLEVBQUUsS0FBSyxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSTtRQUNsRCxVQUFVLEVBQUUsQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsVUFBVSxDQUFDO0tBQzNFLENBQUM7SUFFRixJQUFNLFdBQVcsR0FBRztRQUNsQixLQUFLLEVBQUUsVUFBVTtRQUNqQixLQUFLLEVBQUUsS0FBSyxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRztLQUNsRCxDQUFDO0lBRUYsTUFBTSxDQUFDLENBQ0wsb0JBQUMsT0FBTyxJQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVM7UUFDeEUsb0JBQUMsSUFBSSxlQUFLLFNBQVMsRUFBUztRQUM1Qiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFHLEtBQUssQ0FBTztRQUN2RSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFHLFFBQVEsQ0FBTztRQUM3RSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFHLElBQUksQ0FBTztRQUNyRSxvQkFBQyxNQUFNLGVBQUssV0FBVyxFQUFXLENBQzFCLENBQ1gsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLElBQU0sZUFBZSxHQUFHLFVBQUMsRUFBOEI7UUFBNUIsc0JBQVEsRUFBRSwwQkFBVSxFQUFFLGNBQUk7SUFDbkQsSUFBTSxXQUFXLEdBQUc7UUFDbEIsS0FBSyxFQUFFLFVBQVU7UUFDakIsS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHO0tBQzFDLENBQUM7SUFFRixNQUFNLENBQUMsQ0FDTCxvQkFBQyxPQUFPLElBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVM7UUFDaEUsb0JBQUMsTUFBTSxlQUFLLFdBQVcsRUFBVyxDQUMxQixDQUNYLENBQUM7QUFDSixDQUFDLENBQUM7QUFFRixJQUFNLGdCQUFnQixHQUFHLFVBQUMsVUFBVTtJQUNsQyxJQUFNLGVBQWUsR0FBRztRQUN0QixLQUFLLEVBQUUsNkJBQTZCO1FBQ3BDLE1BQU0sRUFBRSxDQUFDO1FBQ1QsWUFBWSxFQUFFLG1CQUFtQjtRQUNqQyxJQUFJLEVBQUUsVUFBVSxJQUFJLEVBQUU7UUFDdEIsV0FBVyxFQUFFLElBQUk7UUFDakIsZ0JBQWdCLEVBQUUsSUFBSTtLQUN2QixDQUFDO0lBRUYsTUFBTSxDQUFDLG9CQUFDLGFBQWEsZUFBSyxlQUFlLEVBQUksQ0FBQztBQUNoRCxDQUFDLENBQUM7QUFFRixJQUFNLGFBQWEsR0FBRyxVQUFDLEtBQWE7SUFDNUIsSUFBQSxVQUFpRSxFQUFqRCxpREFBZ0IsRUFBSSx3QkFBUyxDQUFxQjtJQUN4RSxJQUFNLFlBQVksR0FBRyxZQUFZLENBQUMsRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFFM0UsSUFBTSxVQUFVLEdBQUcsZ0JBQWdCLENBQUMsWUFBWSxDQUFDO1dBQzVDLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUM7SUFFaEQsSUFBTSxXQUFXLEdBQUc7UUFDbEIsS0FBSyxFQUFFLHlCQUF5QjtRQUNoQyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU07S0FDcEIsQ0FBQztJQUVGLE1BQU0sQ0FBQyxDQUNMO1FBQ0Usb0JBQUMsVUFBVTtZQUNULDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQztnQkFDckMsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLLDZIQUFpRjtnQkFDeEcsb0JBQUMsT0FBTyxJQUFDLEVBQUUsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSTtvQkFDakUsb0JBQUMsTUFBTSxlQUFLLFdBQVcsRUFBVyxDQUMxQixDQUNOO1lBQ04sNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTO2dCQUMzQyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGlCQUFpQixDQUFDLFVBQVU7b0JBQzNDLFVBQVUsQ0FBQyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsa0NBQWtDLEVBQUUsQ0FBQztvQkFDekcsVUFBVSxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRSx3REFBd0QsRUFBRSxDQUFDO29CQUNsSSxVQUFVLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsSUFBSSxFQUFFLHFFQUFxRSxFQUFFLENBQUMsQ0FDNUk7Z0JBQ047b0JBR0ksS0FBSyxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQzsyQkFDL0IsbUJBQW1CLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBSSxFQUFFLEtBQUssSUFBSyxPQUFBLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBbEIsQ0FBa0IsQ0FBQztvQkFJL0QsS0FBSyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUM7MkJBQ3pCLGFBQWEsQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFJLEVBQUUsS0FBSyxJQUFLLE9BQUEsVUFBVSxDQUFDLElBQUksQ0FBQyxFQUFoQixDQUFnQixDQUFDLENBRXJELENBQ0Y7WUFDTiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGlCQUFpQixDQUFDLFNBQVM7Z0JBQzNDLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsaUJBQWlCLENBQUMsVUFBVTtvQkFDM0MsVUFBVSxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxpQ0FBaUMsRUFBRSxDQUFDO29CQUN4RyxVQUFVLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsSUFBSSxFQUFFLGlDQUFpQyxFQUFFLENBQUMsQ0FDeEc7Z0JBQ04sNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsU0FBUztvQkFDcEQsZUFBZSxDQUFDLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsa0JBQWtCLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUsb0JBQW9CLEVBQUUsVUFBVSxFQUFFLG1CQUFtQixFQUFFLElBQUksRUFBRSxrQkFBa0IsRUFBRSxDQUFDO29CQUM5SyxlQUFlLENBQUMsRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxnQkFBZ0IsRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRSwyQkFBMkIsRUFBRSxVQUFVLEVBQUUsbUJBQW1CLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxDQUFDO29CQUN4SyxlQUFlLENBQUMsRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRSxxQkFBcUIsRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRSxlQUFlLEVBQUUsVUFBVSxFQUFFLGNBQWMsRUFBRSxVQUFVLEVBQUUsS0FBSyxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksRUFBRSxpQkFBaUIsRUFBRSxDQUFDO29CQUMxTyxlQUFlLENBQUMsRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxrQkFBa0IsRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRSxhQUFhLEVBQUUsVUFBVSxFQUFFLG1CQUFtQixFQUFFLElBQUksRUFBRSxxQkFBcUIsRUFBRSxDQUFDLENBQzFLLENBQ0Y7WUFDTiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGFBQWEsQ0FBQyxTQUFTO2dCQUN2Qyw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsU0FBUztvQkFDaEQsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEtBQUssMERBQTZCO29CQUMzRSw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsUUFBUSw4R0FBd0Q7b0JBQ3pHLG9CQUFDLE9BQU8sSUFBQyxFQUFFLEVBQUUsOEJBQThCO3dCQUFFLG9CQUFDLE1BQU0sSUFBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQVcsQ0FBVSxDQUNsSTtnQkFDTiw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsU0FBUztvQkFDL0Msb0JBQUMsSUFBSSxJQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxVQUFVLEVBQUUsS0FBSyxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsU0FBUyxHQUFTO29CQUN2SCw2QkFBSyxLQUFLLEVBQUUsS0FBSyxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsS0FBSyxnQkFBaUI7b0JBQzlELDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxJQUFJLGtGQUFzQyxDQUM5RSxDQUNGO1lBQ04sNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLElBQzFCLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxDQUN6QjtZQUNOLDZCQUFLLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLFNBQVM7Z0JBQ2xDLFVBQVUsQ0FBQyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsdUVBQXVFLEVBQUUsQ0FBQztnQkFDNUgsNkJBQUssS0FBSyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLFNBQVM7b0JBQzVDLGVBQWUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLDRCQUE0QixFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztvQkFDdkssZUFBZSxDQUFDLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsMkJBQTJCLEVBQUUsSUFBSSxFQUFFLGtCQUFrQixFQUFFLENBQUM7b0JBQ3hHLGVBQWUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLDJCQUEyQixFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDbEcsZUFBZSxDQUFDLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsOEJBQThCLEVBQUUsSUFBSSxFQUFFLGlCQUFpQixFQUFFLENBQUMsQ0FDdkcsQ0FDRixDQUNLLENBQ08sQ0FDdkIsQ0FBQztBQUNKLENBQUMsQ0FBQztBQUVGLGVBQWUsYUFBYSxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/lixicoin/view.tsx

var renderView = function (props) {
    var switchView = {
        MOBILE: function () { return view_desktop(props); },
        DESKTOP: function () { return view_desktop(props); }
    };
    return switchView[window.DEVICE_VERSION]();
};
/* harmony default export */ var view = (renderView);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXcudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE9BQU8sYUFBYSxNQUFNLGdCQUFnQixDQUFDO0FBRzNDLElBQU0sVUFBVSxHQUFHLFVBQUMsS0FBYTtJQUMvQixJQUFNLFVBQVUsR0FBRztRQUNqQixNQUFNLEVBQUUsY0FBTSxPQUFBLGFBQWEsQ0FBQyxLQUFLLENBQUMsRUFBcEIsQ0FBb0I7UUFDbEMsT0FBTyxFQUFFLGNBQU0sT0FBQSxhQUFhLENBQUMsS0FBSyxDQUFDLEVBQXBCLENBQW9CO0tBQ3BDLENBQUM7SUFFRixNQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDO0FBQzdDLENBQUMsQ0FBQztBQUVGLGVBQWUsVUFBVSxDQUFDIn0=
// CONCATENATED MODULE: ./container/app-shop/lixicoin/container.tsx
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var connect = __webpack_require__(129).connect;





var container_LixiCoinContainer = /** @class */ (function (_super) {
    __extends(LixiCoinContainer, _super);
    function LixiCoinContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.state = INITIAL_STATE;
        return _this;
    }
    LixiCoinContainer.prototype.init = function () {
        var _a = this.props, themeStore = _a.themeStore, fetchProductByThemeIdAction = _a.fetchProductByThemeIdAction, themeName = _a.themeName;
        var param = { id: themeName, page: 1, perPage: 20 };
        var keyHashTheme = Object(encode["h" /* objectToHash */])(param);
        true === Object(validate["l" /* isUndefined */])(themeStore.productByThemeId[keyHashTheme]) && fetchProductByThemeIdAction(param);
    };
    LixiCoinContainer.prototype.componentWillMount = function () {
        this.init();
    };
    LixiCoinContainer.prototype.render = function () {
        return view(this.props);
    };
    LixiCoinContainer.defaultProps = DEFAULT_PROPS;
    LixiCoinContainer = __decorate([
        radium
    ], LixiCoinContainer);
    return LixiCoinContainer;
}(react["Component"]));
;
/* harmony default export */ var container = __webpack_exports__["default"] = (connect(mapStateToProps, mapDispatchToProps)(container_LixiCoinContainer));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFpbmVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxLQUFLLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDL0IsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFDakMsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUUvQyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDckQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBR3RELE9BQU8sRUFBRSxlQUFlLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFDOUQsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDNUQsT0FBTyxVQUFVLE1BQU0sUUFBUSxDQUFDO0FBR2hDO0lBQWdDLHFDQUErQjtJQUc3RCwyQkFBWSxLQUFhO1FBQXpCLFlBQ0Usa0JBQU0sS0FBSyxDQUFDLFNBRWI7UUFEQyxLQUFJLENBQUMsS0FBSyxHQUFHLGFBQXVCLENBQUM7O0lBQ3ZDLENBQUM7SUFFRCxnQ0FBSSxHQUFKO1FBQ1EsSUFBQSxlQUFtRSxFQUFqRSwwQkFBVSxFQUFFLDREQUEyQixFQUFFLHdCQUFTLENBQWdCO1FBQzFFLElBQU0sS0FBSyxHQUFHLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQztRQUN0RCxJQUFNLFlBQVksR0FBRyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFekMsSUFBSSxLQUFLLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSwyQkFBMkIsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN4RyxDQUFDO0lBRUQsOENBQWtCLEdBQWxCO1FBQ0UsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2QsQ0FBQztJQUVELGtDQUFNLEdBQU47UUFDRSxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBckJNLDhCQUFZLEdBQVcsYUFBYSxDQUFDO0lBRHhDLGlCQUFpQjtRQUR0QixNQUFNO09BQ0QsaUJBQWlCLENBdUJ0QjtJQUFELHdCQUFDO0NBQUEsQUF2QkQsQ0FBZ0MsS0FBSyxDQUFDLFNBQVMsR0F1QjlDO0FBQUEsQ0FBQztBQUVGLGVBQWUsT0FBTyxDQUNwQixlQUFlLEVBQ2Ysa0JBQWtCLENBQ25CLENBQUMsaUJBQWlCLENBQUMsQ0FBQyJ9

/***/ })

}]);