// server.js
const express        = require('express');
const MongoClient    = require('mongodb').MongoClient;
const bodyParser     = require('body-parser');
const app            = express();

const port = process.env.PORT || 8000;

app.get('/html', function(req, res) {
  res.sendFile(__dirname + '/dist/test.html');
});

app.get('*', function(req, res) {
  res.sendFile(__dirname + '/dist/index.html');
});

app.listen(port, () => {
  console.log('We are live on ' + port);
});